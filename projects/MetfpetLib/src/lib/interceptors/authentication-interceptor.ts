import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {
  
  constructor(private _authService: AuthService) {

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(this._setAuthorizationHandler(request));
  }

  private _setAuthorizationHandler(request: HttpRequest<any>) {
    var cloneRequestParams = {'setHeaders': {}};
    var token = this._authService.getAccessToken();
    if (token) {
      cloneRequestParams['setHeaders']['Authorization'] = 'Bearer ' + token;
    }
    return request.clone(cloneRequestParams);
  }
}