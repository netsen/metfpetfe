import { AbstractControl, FormGroup, FormControl, ValidatorFn, ValidationErrors } from '@angular/forms';

export function isNormalInteger(str) {
  return /^\+?(0|[1-9]\d*)$/.test(str);
}

export function isNormalFloating(str) {
  return /^\+?([0-9]*[,|.])?[0-9]+$/.test(str);
}

export function dateLessThan(dateField1: string, dateField2: string): ValidatorFn  {
  return (group: FormGroup): ValidatorFn  => {
    const date1 = group.controls[dateField1];
    const date2 = group.controls[dateField2];
    if ((date1.value !== null && date2.value !== null) && date1.value > date2.value) {
      date1.setErrors({mustLessThan: true});
      date2.setErrors({mustGreaterThan: true});
    } else {
      date1.setErrors(null);
      date2.setErrors(null);
    }
    return null;
  };
}

export function dateInFuture(dateField1: string): ValidatorFn  {
  return (group: FormGroup): ValidatorFn  => {
    const date1 = group.controls[dateField1];
    if ((date1.value !== null) && date1.value < new Date()) {
      date1.setErrors({mustBeInFuture: true});
    } else {
      date1.setErrors(null);
    }
    return null;
  };
}

export function patternValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }

    // test the value of the control against the regexp supplied
    const valid = regex.test(control.value);

    // if true, return no error (no error), else return error passed in the second parameter
    return valid ? null : error;
  };
}

export function mustEqual(fieldName: string): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    // control value (e.g. password)
    let secondValue = control.root.get(fieldName);

    // value not equal
    if (secondValue && control.value !== secondValue.value) {
      return { mustEqual: true }
    }
    return null;
  }
}

export function specicalCharacterValidator(control: FormControl): {[key: string]: any} {
  if (control.value && !/^[\w\s]+$/.test(control.value)) {
    return {containsSpecialCharacter: true};
  }
}

export function emailValidator(control: FormControl): {[key: string]: any} {
  var emailRegexp = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;  
  if (control.value && !emailRegexp.test(control.value)) {
    return {invalidEmail: true};
  }
}

export function passwordValidator(control: FormControl): {[key: string]: any} {
  if (control.value && !/^(((?=.*\d)(?=.*[a-z])(?=.*[A-Z])){3}|((?=.*\d)(?=.*[a-z])(?=.*[!"#$%&'()*+, \-./:;<=>?@ [\\\]^_`}|}~])){3}|((?=.*\d)(?=.*[a-z])(?=.*[\u0080-\uffff])){3}|((?=.*\d)(?=.*[A-Z])(?=.*[!"#$%&'()*+, \-./:;<=>?@ [\\\]^_`}|}~])){3}|((?=.*\d)(?=.*[A-Z])(?=.*[\u0080-\uffff])){3}|((?=.*\d)(?=.*[!"#$%&'()*+, \-./:;<=>?@ [\\\]^_`}|}~])(?=.*[\u0080-\uffff])){3}|((?=.*[a-z])(?=.*[A-Z])(?=.*[!"#$%&'()*+, \-./:;<=>?@ [\\\]^_`}|}~])){3}|((?=.*[a-z])(?=.*[A-Z])(?=.*[\u0080-\uffff])){3}|((?=.*[a-z])(?=.*[!"#$%&'()*+, \-./:;<=>?@ [\\\]^_`}|}~])(?=.*[\u0080-\uffff])){3}|((?=.*[A-Z])(?=.*[!"#$%&'()*+, \-./:;<=>?@ [\\\]^_`}|}~])(?=.*[\u0080-\uffff])){3}).{8,}$/.test(control.value)) {
    return {invalidPassword: true};
  }
}

export function validateForm(formGroup: FormGroup) {
  Object.keys(formGroup.controls).forEach(
    field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        validateForm(control);
      }
    }
  );
}
