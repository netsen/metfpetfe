import { Component } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';

@Component({
  templateUrl: './pwa-ios-prompt.component.html',
  styleUrls: ['./pwa-ios-prompt.component.scss']
})
export class PwaIosPromptComponent {

  constructor(
    private _bottomSheetRef: MatBottomSheetRef<PwaIosPromptComponent>
  ) {}

  public close() {
    this._bottomSheetRef.dismiss();
  }

}
