import { Component, Input, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AppSettings, Settings } from '../../../theme';
import { MenuService } from '../menu.service';
import { Menu } from '../menu.model';

@Component({
  selector: 'app-vertical-menu',
  templateUrl: './vertical-menu.component.html',
  styleUrls: ['./vertical-menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class VerticalMenuComponent {

  _menuItems: Array<Menu>;
  get menuItems(): Array<Menu> {
    return this._menuItems;
  }
  @Input() set menuItems(value: Array<Menu>) {
    this._menuItems = value;
  }

  @Input('menuParentId') menuParentId;
  parentMenu:Array<any>;
  public settings: Settings;
  
  constructor(
    public appSettings: AppSettings, 
    private _menuService: MenuService,
    private _router: Router
  ) {
    this.settings = this.appSettings.settings;
  }

  ngOnInit() {
    this.parentMenu = this.menuItems.filter(item => item.parentId == this.menuParentId);  
  }

  onClick(menuId) {
    this._menuService.toggleMenuItem(menuId);
    this._menuService.closeOtherSubMenus(this.menuItems, menuId);    
  }

}