import {
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation, 
  Input
} from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Store } from '@ngrx/store';
import { IdentityServiceAgent } from '../../services';
import { showError, showException, showSuccess } from '../../store/actions';

export interface RetrievePasswordEvent {
  identifier: string;
  email: string;
}

@Component({
  selector: 'app-retrieve-password',
  templateUrl: './retrieve-password.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RetrievePasswordComponent {

  @Input()  userType: string;
  loading: boolean;
  form: FormGroup;
  
  constructor(
    private _cd: ChangeDetectorRef,
    private _fb: FormBuilder,
    private _store: Store<any>,
    private _identityService: IdentityServiceAgent.HttpService
  ) {
    this.form = this._fb.group({
      'identifiant': [null, Validators.required],
      'emailOrPhone': [null, Validators.required],
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      this.loading = true;
      this._identityService.resetPassword(IdentityServiceAgent.ResetPasswordViewModel.fromJS({
          identifiant: this.form.get('identifiant').value,
          emailOrPhone: this.form.get('emailOrPhone').value,
          userType: this.userType
        }))
        .subscribe(
          () => this._store.dispatch(showSuccess({message: 'Un e-mail ou SMS vous a été envoyé avec votre nouveau mot de passe.'})),
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => {
          this.loading = false;
          this._cd.markForCheck();
        });
    }
  }
}