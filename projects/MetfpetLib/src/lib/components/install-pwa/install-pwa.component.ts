import { Component, ViewEncapsulation } from '@angular/core';
import { Platform } from '@angular/cdk/platform';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { PwaService } from './pwa.service';
import { PwaIosPromptComponent } from '../bottom-sheet';

@Component({
  selector: 'app-install-pwa',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './install-pwa.component.html',
})
export class InstallPwaComponent {

  private _isInStandaloneMode: boolean;

  constructor(
    private _bottomSheet: MatBottomSheet,
    private _platform: Platform,
    private _pwaService: PwaService) {
  }

  ngOnInit() {
    this._isInStandaloneMode = ('standalone' in window.navigator) && (window.navigator['standalone']);
  }

  public installPwa(): void {
    if (this._pwaService.deferredPrompt) {
      this._pwaService.deferredPrompt.prompt();
    } else {
      this._bottomSheet.dismiss();
      this._bottomSheet.open(PwaIosPromptComponent);
    }
  }

  public isInstallEnable(): boolean {
    return this._pwaService.deferredPrompt || 
      (this._platform.IOS && !this._isInStandaloneMode);
  }
}