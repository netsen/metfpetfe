import { ApplicationRef, Injectable, Injector } from '@angular/core';
import { concat, interval } from 'rxjs';
import { first } from 'rxjs/operators';
import { SwUpdate } from '@angular/service-worker';
import { DialogService } from '../dialogs';

@Injectable()
export class PwaService {

  public deferredPrompt: any;

  // Helper property to resolve the service dependency.
  private get _appRef(): ApplicationRef { 
    return this._injector.get(ApplicationRef); 
  }

  constructor(
    private _dialogService: DialogService,
    private _injector: Injector,
    private _swUpdate: SwUpdate
    ) {
  }

  public init() {
    console.log('init pwaService');
    if (this._swUpdate.isEnabled) {
      // Allow the app to stabilize first, before starting polling for updates with `interval()`.
      const appIsStable$ = this._appRef.isStable.pipe(first(isStable => isStable === true));
      const every30Min$ = interval(30 * 60 * 1000);
      const every30MinOnceAppIsStable$ = concat(appIsStable$, every30Min$);
      every30MinOnceAppIsStable$.subscribe(() => 
        this._swUpdate.checkForUpdate().then(() => console.log('checking for updates')));

      this._swUpdate.available.subscribe(event => {
        this._askUserToUpdate();
      });
    }
    
    window.addEventListener('beforeinstallprompt', event => {
      console.log('override beforeinstallprompt');
      event.preventDefault();
      this.deferredPrompt = event;
    })
  }

  private _askUserToUpdate() {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: 'Attention',
        message: 'Nouvelle version de l\'application est disponible, voulez-vous la mettre à jour?', 
        confirmBtnText: 'Oui'
      }

    }).afterClosed().subscribe(() => {
      this._swUpdate.activateUpdate().then(() => document.location.reload());
    });
  }
}