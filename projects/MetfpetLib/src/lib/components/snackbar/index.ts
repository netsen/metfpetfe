import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { SnackBarComponent } from './snackbar.component';

export * from './snackbar.component';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatSnackBarModule
  ],
  exports: [
  ],
  declarations: [
    SnackBarComponent
  ]
})
export class SnackBarModule {

}