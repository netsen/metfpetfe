import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ElementRef, 
  Inject,
  ViewChild,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MetfpetServiceAgent } from '../../services';
import { Diffusion } from '../../interfaces';
import * as htmlToImage from 'html-to-image';
import { toPng, toJpeg, toBlob, toPixelData, toSvg } from 'html-to-image';
import { hideLoading, showException, showLoading, showSuccess } from '../../store/actions';
import { Store } from '@ngrx/store';
import { jsPDF } from "jspdf";

@Component({
  templateUrl: './impression-document-preview.dialog.html',
  styleUrls: ['./impression-document-preview.dialog.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImpressionDocumentPreviewDialog {

  form: FormGroup;
  documentInfo: MetfpetServiceAgent.DocumentInfo;
  impressionDocumentId: string;
  documentHtmlContent: string;
  diffusion: number;
  impressionDocumentDto: MetfpetServiceAgent.ImpressionDocumentDTO;

  constructor(
    public dialogRef: MatDialogRef<ImpressionDocumentPreviewDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _formBuilder: FormBuilder,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
    private _store: Store<any>,
  ) {
    this.documentInfo = data.documentInfo;
    this.impressionDocumentId = data.impressionDocumentId;
    this.diffusion = data.diffusion;
    this.form = this._formBuilder.group({
      'id': null,
    });
  }

  ngOnInit() {
    if (this.impressionDocumentId) {
      this._metfpetService.getImpressionDocumentHtmlContent(this.impressionDocumentId).subscribe(
        content => {
          this.documentHtmlContent = content;
          this._cd.markForCheck();
        }
      );
    }
  }

  public onClose(): void {
    this.dialogRef.close(this.impressionDocumentDto);
  }

  public print(): void {
    this.printDocument(true);
  }

  public sendMail(): void {
    this.printDocument(false);
  }

  private updateDeliveryStatus(isPrinted: boolean): void {
    var request = new MetfpetServiceAgent.UpdateImpressionDocumentDeliveryStatus();
    request.id = this.impressionDocumentId;
    request.isPrinted = isPrinted;
    request.sentMail = !isPrinted;
    this._metfpetService.updateImpressionDocumentDeliveryStatus(request).subscribe(
      (result) => {
        this.impressionDocumentDto = result;
        this._store.dispatch(showSuccess({}));
      },
      (error) => this._store.dispatch(showException({error: error}))
    ).add(() => {
      this._store.dispatch(hideLoading());
    });
  }

  private printDocument(isPrinted: boolean): void {
    this._store.dispatch(showLoading());
    const savedfileName = this.documentInfo.etudiantName + '_document.pdf';
    const doc = new jsPDF({
      orientation: "landscape",
      unit: "mm",
      format: [85, 55]
    });
    var width = doc.internal.pageSize.getWidth();
    var height = doc.internal.pageSize.getHeight();

    htmlToImage.toJpeg(document.getElementById('card-front'), { backgroundColor: '#ffffff' })
      .then(function (dataUrl) {
        doc.addImage(dataUrl, "jpeg", -2, -3, width + 1, height + 1);
      })
      .then(() => {
        htmlToImage.toJpeg(document.getElementById('card-back'), { backgroundColor: '#ffffff' })
          .then(function (dataUrl) {
            doc.addPage();
            doc.addImage(dataUrl, "jpeg", -3.2, -3.8, width + 3, height + 4);
            if (isPrinted) {
              doc.output("dataurlnewwindow", {filename: savedfileName}).open();
            } else {
              doc.save(savedfileName);
            }
          }).then(() => { this.updateDeliveryStatus(isPrinted) });
      });
  }

  public allowSendEmail(): boolean {
    return this.diffusion === <any> Diffusion.Mail || this.diffusion === <any> Diffusion.ImprimeEtMail;
  }

  public allowPrint(): boolean {
    return this.diffusion === <any> Diffusion.Imprime || this.diffusion === <any> Diffusion.ImprimeEtMail;
  }
}