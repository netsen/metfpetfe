import { 
  ChangeDetectionStrategy,
  Component, 
  Inject,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MetfpetServiceAgent, PaymentServiceAgent } from '../../services';
import { Store } from '@ngrx/store';
import { hideLoading, showError, showException, showLoading } from '../../store/actions';
import { PAYMENT_ERROR } from '../../interfaces';

@Component({
  templateUrl: './impression-document-payment.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImpressionDocumentPaymentDialog {

  form: FormGroup;
  documentInfo: MetfpetServiceAgent.DocumentInfo;
  documentName: string;
  documentMontant: number;
  impressionDocumentId: string;
  paymentInProgress: boolean;
  
  constructor(
    public dialogRef: MatDialogRef<ImpressionDocumentPaymentDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _formBuilder: FormBuilder,
    private _paymentService: PaymentServiceAgent.HttpService,
    private _store: Store<any>
  ) {
    this.documentInfo = data.documentInfo;
    this.documentName = data.documentName;
    this.documentMontant = data.documentMontant;
    this.impressionDocumentId = data.impressionDocumentId;
    this.form = this._formBuilder.group({
      'id': null,
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public onProceed(): void {
    this.dialogRef.close();
  }

  public onPayWithOrange(): void {
    this.paymentInProgress = true;
    this._paymentService.createImpressionDocumentPayment(PaymentServiceAgent.CreateImpressionDocumentPayment.fromJS(
      {
        etudiantId: this.documentInfo.etudiantId,
        impressionDocumentId: this.impressionDocumentId,
      }
    ))
    .subscribe(
      (payment: PaymentServiceAgent.PaymentDTO) => {
        if (!payment) {
          this._store.dispatch(showError({message: PAYMENT_ERROR}));
          return;
        }
        this.paymentInProgress = false;
        if (payment.details) {
          window.open(payment.details, "_blank");
        }
        this.dialogRef.close({
          payment: payment,
        });
      },
      (error) => this._store.dispatch(showException({error: error}))
    );
  }
}