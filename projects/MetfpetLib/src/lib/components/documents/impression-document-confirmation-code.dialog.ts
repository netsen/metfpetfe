import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Inject,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MetfpetServiceAgent, PaymentServiceAgent } from '../../services';
import { validateForm } from '../../utils';
import { Store } from '@ngrx/store';
import { hideLoading, showException, showLoading, showSuccess } from '../../store/actions';

@Component({
  templateUrl: './impression-document-confirmation-code.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImpressionDocumentConfirmationCodeDialog {

  form: FormGroup;
  documentInfo: MetfpetServiceAgent.DocumentInfo;
  documentName: string;
  documentMontant: number;
  paymentId: string;

  constructor(
    public dialogRef: MatDialogRef<ImpressionDocumentConfirmationCodeDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _formBuilder: FormBuilder,
    private _paymentService: PaymentServiceAgent.HttpService,
    private _store: Store<any>,
    private _cd: ChangeDetectorRef,
  ) {
    this.documentInfo = data.documentInfo;
    this.documentName = data.documentName;
    this.documentMontant = data.documentMontant;
    this.paymentId = data.paymentId;
    this.form = this._formBuilder.group({
      'confirmationCode': [null, Validators.required],
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public onGetConfirmationCode(): void {
    this._store.dispatch(showLoading());
    this._paymentService.getConfirmationCode(this.paymentId).subscribe(
      (result) =>  {
        if (result) {
          this.form.get('confirmationCode').setValue(result);
          this._cd.markForCheck();
        }
      },
      error => this._store.dispatch(showException({error: error}))
    )
    .add(() => this._store.dispatch(hideLoading()));
  }

  public onProceed(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._paymentService.completeOrangePayment(PaymentServiceAgent.CompletePayment.fromJS(
        {
          notif_token: this.form.get('confirmationCode').value,
          paymentId: this.paymentId,
          paymentType: PaymentServiceAgent.PaymentType.ImpressionDocument
        }
      )).subscribe(
        () =>  {
          this.dialogRef.close(true);
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
    }
  }
}