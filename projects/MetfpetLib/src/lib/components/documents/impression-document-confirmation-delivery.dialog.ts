import { 
  ChangeDetectionStrategy,
  Component, 
  Inject,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './impression-document-confirmation-delivery.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImpressionDocumentConfirmationDeliveryDialog {

  documentName: string;
  diffusionName: string;

  constructor(
    public dialogRef: MatDialogRef<ImpressionDocumentConfirmationDeliveryDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.documentName = data.documentName;
    this.diffusionName = data.diffusionName;
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public onProceed(): void {
    this.dialogRef.close(true);
  }
}