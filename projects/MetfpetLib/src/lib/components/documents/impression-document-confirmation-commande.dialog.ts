import { 
  ChangeDetectionStrategy,
  Component, 
  Inject,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Commande, Diffusion } from '../../interfaces';

@Component({
  templateUrl: './impression-document-confirmation-commande.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImpressionDocumentConfirmationCommandeDialog {

  documentName: string;
  diffusionName: string;
  diffusion: number;
  email: string;

  constructor(
    public dialogRef: MatDialogRef<ImpressionDocumentConfirmationCommandeDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.documentName = data.documentName;
    this.diffusionName = data.diffusionName;
    this.diffusion = data.diffusion;
    this.email = data.email;
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public onProceed(): void {
    this.dialogRef.close(true);
  }

  public diffusionEmail(): boolean {
    return this.diffusion === <any> Diffusion.Mail || this.diffusion === <any> Diffusion.ImprimeEtMail;
  }

  public diffusionImprime(): boolean {
    return this.diffusion === <any> Diffusion.Imprime;
  }
  
  public diffusionImprimeEtMail(): boolean {
    return this.diffusion === <any> Diffusion.ImprimeEtMail;
  }

  public diffusionImprimeAndImprimeEtMail(): boolean { 
    return this.diffusionImprime() || this.diffusionImprimeEtMail();
  }
}