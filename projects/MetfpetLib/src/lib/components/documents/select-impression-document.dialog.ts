import { 
  ChangeDetectionStrategy,
  Component, 
  Inject,
} from '@angular/core';
import { 
  tap,
} from 'rxjs/operators/';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MetfpetServiceAgent } from '../../services';
import { validateForm } from '../../utils';
import { Store } from '@ngrx/store';
import { hideLoading, showException, showLoading, showSuccess } from '../../store/actions';

@Component({
  templateUrl: './select-impression-document.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectImpressionDocumentDialog {

  form: FormGroup;
  documentInfo: MetfpetServiceAgent.DocumentInfo;
  documents: Array<MetfpetServiceAgent.DocumentViewModel>;
  montant: number;

  constructor(
    public dialogRef: MatDialogRef<SelectImpressionDocumentDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.documentInfo = data.documentInfo;
    this.documents = data.documents;
    this.montant = 0;
    this.form = this._formBuilder.group({
      'documentId': [null, Validators.required],
    });
    this.form.get('documentId').valueChanges.pipe(
      tap(documentId => {
        if (documentId) {
          let selectedDocument = this.documents.find(v => v.id === documentId);
          this.montant = selectedDocument.montant;
        } else {
          this.montant = 0;
        }
      })
    ).subscribe();
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public onProceed(): void {
    validateForm(this.form);
    if (this.form.valid) {

      let selectedDocument = this.documents.find(v => v.id === this.form.get('documentId').value);
      
      this._store.dispatch(showLoading());
      this._metfpetService.createImpressionDocument(MetfpetServiceAgent.CreateImpressionDocument.fromJS({
          documentId : selectedDocument.id,
          name : selectedDocument.name,
          commande : selectedDocument.commande,
          etudiantId : this.documentInfo.etudiantId,
          inscriptionProgrammeId : this.documentInfo.inscriptionProgrammeId,
        }))
        .subscribe(result => {
            this.dialogRef.close({
              document: selectedDocument,
              impressionDocument: result
            });
          },
          (error) => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }
}