import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'document-viewer',
  templateUrl: './document-viewer.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DocumentViewerComponent {

  @Input()
  set documentContent(documentContent: string) {
    this.template = this.sanitizer.bypassSecurityTrustHtml(documentContent);
    this._cd.markForCheck();
  }

  template: SafeHtml;
  
  constructor(private sanitizer: DomSanitizer,
    private _cd: ChangeDetectorRef) { 
  }
}
