import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, Input } from '@angular/core';
import { Subscription, interval } from 'rxjs';

@Component({
  selector: 'app-content-header',
  templateUrl: './content-header.component.html',
  styleUrls: ['./content-header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContentHeaderComponent implements OnInit {

  @Input('icon') icon:any;
  @Input('customIcon') customIcon:any;
  @Input('title') title:any;
  @Input('desc') desc:any;
  @Input('hideBreadcrumb') hideBreadcrumb:boolean = false;
  @Input('hasBgImage') hasBgImage:boolean = false;
  @Input('class') class:any;
  @Input('layout') layout:string = 'row';
  private _subscription: Subscription;
  private _intervalTimer = interval(5000);
  index = 0;

  constructor(private _cd: ChangeDetectorRef) { 
  }

  ngOnInit() {
    this._subscription = this._intervalTimer.subscribe(() => {
      this.index = (this.index + 1) % 6;
      this._cd.markForCheck();
    });
  }

  ngOnDestroy() {
    if (this._subscription) {
      this._subscription.unsubscribe();
    }
  }
}
