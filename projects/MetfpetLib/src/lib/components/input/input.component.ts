import { 
  Component, 
  Directive,
  Host, 
  Optional, 
  SkipSelf,
  forwardRef, 
  Input,
  OnInit 
} from '@angular/core';
import { NG_VALUE_ACCESSOR, AbstractControl, ControlContainer, ControlValueAccessor } from '@angular/forms';

@Directive()
export class InputComponent implements ControlValueAccessor, OnInit {

  onChange: any = () => { };
  onTouched: any = () => { };
  outerFormControl: AbstractControl;
  @Input() placeholder: string;
  @Input() required: boolean;
  @Input() formControlName: string;
  @Input('value') innerValue: string;

  constructor(
    @Optional() @Host() @SkipSelf()
    protected controlContainer: ControlContainer) {

  }

  ngOnInit() {
    if (this.controlContainer && this.formControlName) {
      this.outerFormControl = this.controlContainer.control.get(this.formControlName);
      const self = this;
      const originalFn = this.outerFormControl.markAsTouched;
      this.outerFormControl.markAsTouched = function() {
        originalFn.apply(this, arguments);
        self.validate(this);
      }
    }
  }

  validate(control: AbstractControl) {
  }

  writeValue(value: string) {
    if (value !== this.innerValue) {
      this.innerValue = value;
    }
  }

  // registers 'fn' that will be fired when changes are made
  // this is how we emit the changes back to the form
  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }
}