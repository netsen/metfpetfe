export * from './confirm.dialog';
export * from './info.dialog';
export * from './modify-password.dialog';
export * from './payment-detail.dialog';
export * from './admission-payment-detail.dialog';
export * from './single-input-text.dialog';
export * from './dialog.service';