import { Injectable, Provider, SkipSelf, Optional, Type } from '@angular/core';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material/dialog';
import { AdmissionPaymentDetailDialog } from './admission-payment-detail.dialog';
import { ConfirmDialog } from './confirm.dialog';
import { InfoDialog } from './info.dialog';
import { PaymentDetailDialog } from './payment-detail.dialog';

export enum SeverityEnum {
  INFO = 'Info',
  WARN = 'Warn', 
  ERROR = 'Error'
}

export interface DialogData {
  title?: string;
  message?: string;
  footerMessage?: string;
  closeBtnText?: string;
}

export interface InfoDialogConfig extends MatDialogConfig {
  data?: InfoDialogData;
}

export interface InfoDialogData extends DialogData {
  severity: SeverityEnum;
  class?: string;
}

export interface ConfirmDialogConfig extends MatDialogConfig {
  data?: ConfirmDialogData;
}

export interface ConfirmDialogData extends DialogData {
  confirmBtnText?: string;
  cancelBtnText?: string;
}

export interface GenericDialogConfig extends MatDialogConfig {
  data?: any;
}

@Injectable({providedIn: 'root'})
export class DialogService {

  constructor(private _matDialog: MatDialog) {
  }

  public openInfoDialog(config: InfoDialogConfig): MatDialogRef<InfoDialog> {
    let dialogConfig: MatDialogConfig = this._createConfig(config);
    dialogConfig.panelClass = config.data.severity;
    if (!dialogConfig.width) {
      dialogConfig.width = '380px';
    }
    dialogConfig.data.title =
      dialogConfig.data.title ? dialogConfig.data.title :
      this.isInfo(config.data.severity) ? 'Message d\'information' :
          this.isWarn(config.data.severity) ? 'Attention' : 'Message d\'erreur';

    return this._matDialog.open(InfoDialog, dialogConfig);
  }

  public openConfirmDialog(config?: ConfirmDialogConfig): MatDialogRef<ConfirmDialog> {
    let dialogConfig: MatDialogConfig = this._createConfig(config);
    dialogConfig.panelClass = SeverityEnum.WARN;
    return this._matDialog.open(ConfirmDialog, dialogConfig);
  }

  public openPaymentDetailDialog(data: any) {
    return this.openDialog(
      PaymentDetailDialog,
      {
        width: '700px',
        data: data
      }
    );
  }

  public openAdmissionPaymentDetailDialog(data: any) {
    return this.openDialog(
      AdmissionPaymentDetailDialog,
      {
        width: '640px',
        data: data
      }
    );
  }

  public openDialog(component: Type<any>, config?: GenericDialogConfig) {
    let dialogConfig: MatDialogConfig = this._createConfig(config);
    return this._matDialog.open(component, dialogConfig);
  }

  private _createConfig(config: MatDialogConfig): MatDialogConfig {
    let dialogConfig: MatDialogConfig = new MatDialogConfig();
    if (!config) {
      config = {};
    } 
    if (!config.data) {
      config.data = {};
    }
    Object.assign(dialogConfig, config);
    return dialogConfig;
  }

  public isInfo(severity: SeverityEnum): boolean {
    return SeverityEnum.INFO == severity;
  }

  public isWarn(severity: SeverityEnum): boolean {
    return SeverityEnum.WARN == severity;
  }

  public isError(severity: SeverityEnum): boolean {
    return SeverityEnum.ERROR == severity;
  }
}

export function DIALOG_PROVIDER_FACTORY(
  parent: DialogService, dialog: MatDialog): DialogService {
  return parent || new DialogService(dialog);
}

export const DIALOG_PROVIDER: Provider = {
  // If there is already service available, use that. Otherwise, provide a new one.
  provide: DialogService,
  deps: [[new Optional(), new SkipSelf(), DialogService], MatDialog],
  useFactory: DIALOG_PROVIDER_FACTORY,
};