import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { mustEqual, passwordValidator, validateForm } from '../../utils';

@Component({
  selector: 'modify-password-dialog',
  templateUrl: './modify-password.dialog.html',
})
export class ModifyPasswordDialog {

  form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ModifyPasswordDialog>,
    private _formBuilder: FormBuilder
  ) {
    this.form = this._formBuilder.group({
      password: [null, Validators.required],
      newPassword: [null, Validators.compose([Validators.required, passwordValidator])],
      confirmPassword: [null, Validators.compose([Validators.required, mustEqual('newPassword')])],
    });
  }

  onConfirm(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this.dialogRef.close({
        password: this.form.get('password').value,
        newPassword: this.form.get('newPassword').value,
      });
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

}
