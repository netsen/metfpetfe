import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PaymentServiceAgent } from '../../services';
import { PaymentStatus } from '../../interfaces';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { hideLoading, showException, showLoading, showSuccess } from '../../store/actions';

@Component({
  templateUrl: './payment-detail.dialog.html',
})
export class PaymentDetailDialog {

  form: FormGroup;
  payment: PaymentServiceAgent.PaymentDTO;
  isEditing: boolean;
  updateTransaction: boolean;
  title: string;

  constructor(
    public dialogRef: MatDialogRef<PaymentDetailDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _paymentService: PaymentServiceAgent.HttpService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.payment = data.payment;
    this.isEditing = false;
    this.updateTransaction = false;
    this.form = this._formBuilder.group({
      transactionId: null,
    });
    this.form.patchValue({
      'transactionId': this.payment.transactionId,
    });
    if (this.payment.impressionDocumentId) {
      this.title = "Paiement - Document";
    } 
    else 
    {
      this.title = "Paiement - (Ré)-Inscription";
    }
  }

  onClose(): void {
    if (this.updateTransaction) {
      this.dialogRef.close("UpdateTransaction");
    }
    else {
      this.dialogRef.close();
    }
  }

  onConfirm(): void {
    this.dialogRef.close('TooglePayment');
  }

  isPaiementEffectue() {
    return this.payment && this.payment.status == <any>PaymentStatus.Success;
  }

  isPaiementEchoue() {
    return this.payment && this.payment.status == <any>PaymentStatus.Failed;
  }

  public get paymentDescription() {
    if (this.payment) {
      if (this.payment.inscriptionId) {
        return 'Inscription année ' + this.payment.anneeAcademiqueName;
      }
      if (this.payment.impressionDocumentId) {
        return this.payment.documentName;
      }
    }
    return null;
  }

  editTransaction(): void {
    this.isEditing = true;
  }

  saveTransaction(): void {
    this.payment.transactionId = this.form.get('transactionId').value;
    this._store.dispatch(showLoading());
    var updateTransactionPayment = new PaymentServiceAgent.UpdateTransactionPayment();
    updateTransactionPayment.transactionId = this.payment.transactionId;
    updateTransactionPayment.paymentId = this.payment.id;
    this._paymentService.updateTransactionPayment(updateTransactionPayment).subscribe(
      () => {
        this.updateTransaction = true;
        this._store.dispatch(showSuccess({}));
      },
      (error) => this._store.dispatch(showException({ error: error }))
    ).add(() => {
      this._store.dispatch(hideLoading());
    });
    this.isEditing = false;
  }
}
