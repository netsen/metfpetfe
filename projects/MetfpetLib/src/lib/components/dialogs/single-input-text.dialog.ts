import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './single-input-text.dialog.html',
})
export class SingleInputTextDialog {

  constructor(
    public dialogRef: MatDialogRef<SingleInputTextDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
  }

  onClose(): void {
    this.dialogRef.close();
  }

}