import { ChangeDetectorRef, Component, Inject, OnDestroy, OnInit, Optional } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { CommunicationServiceAgent } from '../../services';
import { showError, showSuccess } from '../../store/actions';

@Component({
  selector: 'app-confitmation-email',
  templateUrl: './confirmation-email.dialog.html',
  styleUrls: ['./confirmation-email.dialog.css']
})
export class ConfirmationEmailDialog implements OnInit, OnDestroy {
  form: FormGroup;
  expirationCode:Date;
  showResendButton: boolean;
  intervalId: any;
  timeRemaining: number = 0;
  loading: boolean;

  constructor(
    public dialogRef: MatDialogRef<ConfirmationEmailDialog>,
    private _communicationService: CommunicationServiceAgent.HttpService,
    private _cd:ChangeDetectorRef,
    private _store: Store<any>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.form = new FormGroup({
      char1: new FormControl(null, [Validators.required, Validators.maxLength(1)]),
      char2: new FormControl(null, [Validators.required, Validators.maxLength(1)]),
      char3: new FormControl(null, [Validators.required, Validators.maxLength(1)]),
      char4: new FormControl(null, [Validators.required, Validators.maxLength(1)]),
      char5: new FormControl(null, [Validators.required, Validators.maxLength(1)]),
      char6: new FormControl(null, [Validators.required, Validators.maxLength(1)]),
      char7: new FormControl(null, [Validators.required, Validators.maxLength(1)]),
      char8: new FormControl(null, [Validators.required, Validators.maxLength(1)]),
    })
  }

  ngOnInit(): void {
    this.sendEmailVerification();
  }

  ngOnDestroy(): void {
    clearInterval(this.intervalId);
  }

  onSubmit() {
    if (this.form.valid) {
      this.loading = true;
      const values = this.form.value;
      let code = Object.entries(values).map(v => (v[1])).join("");
      this._communicationService.confirmMail(CommunicationServiceAgent.ConfirmMailViewModel.fromJS({
        email: this.data.email,
        code
      })).subscribe({
        next: (res) => {
          if(res.isConfirmed){
            this._store.dispatch(showSuccess({message: res.message}));
            this.dialogRef.close(true);
          }else{
            this._store.dispatch(showError({message:res.message}));
          }
        },
        error: (error) => {
          if (error.status === 400) {
            this._store.dispatch(showError({ message: error.response }));
          }
          else {
            this._store.dispatch(showError({ message: "Erreur interne : " + error.response }));
          }
        }
      }).add(() => this.loading = false)
    }
  }

  sendEmailVerification() {
    this.showResendButton = false;
    this.loading = true;

    this._communicationService.sendMailVerificaiton(CommunicationServiceAgent.MailVerificationViewModel.fromJS({
      email:this.data.email, fullName:this.data.fullName
    }))
      .subscribe({
        next: (res) => {
          if (res.isValid) {
            this.expirationCode = res.expirationCode;
            
            this.timeRemaining = this.expirationCode.getTime() - (new Date().getTime());


            setTimeout(() =>{
              this.showResendButton = true;
            }, this.timeRemaining);
            
            this.startTimer();
            
          }else{
            this._store.dispatch(showError({ message: res.message }));
          }
        },
        error:(error) =>{
          if (error.status === 400) {
            this._store.dispatch(showError({ message: error.response }));
          }
          else {
            this._store.dispatch(showError({ message: "Erreur interne : " + error.response }));
          }
        }
      }).add(() => this.loading = false)
  }

  startTimer(){
    this.intervalId = setInterval(() => {
      this.timeRemaining -= 1000;
      if (this.timeRemaining <= 0) {
        clearInterval(this.intervalId);
      }
    }, 1000);
  }

  get time(){
    return this.timeRemaining;
  }

  onPaste(event: ClipboardEvent) {
    const clipboardData = event.clipboardData || window.Clipboard as any;
    const pastedText = clipboardData.getData('text');
    if (pastedText.length === 8) {
      event.preventDefault();

      for (let i = 0; i < 8; i++) {
        if (this.form.controls[`char${i + 1}`]) {
          this.form.controls[`char${i + 1}`].setValue(pastedText[i]);
        }
      }
    }
  }
}
