import {
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Component, 
  Input, 
  Output, 
  EventEmitter, 
} from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap, take } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { DemandeurStatus, IUser, RequirePayment, SUR_CONCOURS } from '../../interfaces';
import { MetfpetServiceAgent } from '../../services';
import { emailValidator, mustEqual, passwordValidator, validateForm } from '../../utils';
import { 
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  showException,
} from '../../store/actions';
import { DialogService, SeverityEnum } from '../dialogs';
import { selectLoggedInPerson } from '../../store/selectors';
import { Guid } from 'guid-typescript';

@Component({
  selector: 'app-manage-demandeur',
  templateUrl: './manage-demandeur.component.html',
  styleUrls: ['./manage-demandeur.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageDemandeurComponent {

  form: FormGroup;
  isCreation: boolean;
  private _model: MetfpetServiceAgent.DemandeurProfileViewModel;
  attachedDemandeurDocuments: Array<MetfpetServiceAgent.AttachedDemandeurDocumentDTO>;
  currentUser: IUser;
  demandeList: Array<MetfpetServiceAgent.DemandeViewModel>;

  get model(): MetfpetServiceAgent.DemandeurProfileViewModel {
    return this._model;
  }

  @Input()
  set demandeurId(demandeurId: string) {
    this.isCreation = !demandeurId;
    if (!demandeurId) return;
    
    this.loadingIndicator = true;
    this._metfpetService.getAttachedDemandeurDocuments(demandeurId).subscribe(data => {
      this.attachedDemandeurDocuments = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getAllDemandeByDemandeurId(demandeurId).subscribe(data => {
      this.demandeList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getDemandeurProfile(demandeurId)
      .subscribe(data => {
        this._model = data;
        this.onLoadedDemandeur.emit(this._model);
  
        this.form.controls['identifiant'].setValidators(
          Validators.compose([Validators.required, Validators.minLength(3)]));
  
        if (!this.isCreation) {
          this.form.controls['email'].setValidators(
            Validators.compose([Validators.required, emailValidator]));
        
          this.form.controls['phone'].setValidators(
            Validators.compose([Validators.required, Validators.pattern('^[0-9]{9}$')]));
        }
  
        this.form.patchValue({
          email                     : this._model.email,
          phone                     : this._model.phone,
          identifiant               : this._model.identifiant,
          name                      : this._model.name,
          firstName                 : this._model.firstName,
          dateNaissance             : this._model.dateNaissance,
          genreId                   : this._model.genreId,
        }, { emitEvent: false });
      })
      .add(() => {
        this.loadingIndicator = false;
        this._cd.markForCheck();
      });
  }

  genreList: Array<MetfpetServiceAgent.Genre>;
  @Input()  loadingIndicator: boolean;
  @Input()  fromComptesDemandeurs: boolean;
  @Output() onLoadedDemandeur = new EventEmitter<MetfpetServiceAgent.DemandeurProfileViewModel>();
  @Output() onSavedDemandeur = new EventEmitter<MetfpetServiceAgent.DemandeurProfileViewModel>();

  constructor(
    private _cd: ChangeDetectorRef,
    private _fb: FormBuilder, 
    private _location: Location,
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _router: Router,
  ) {
    this.form = this._fb.group({
      'identifiant': null,
      'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'firstName': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'email': [null, Validators.compose([Validators.required, emailValidator])],
      'phone': [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]{9}$")])],
      'genreId': [null, Validators.required],
      'dateNaissance': null,
    }); 
    this._store.pipe(select(selectLoggedInPerson), take(1)).subscribe(
      person =>  this.currentUser = person
    );
  }

  ngAfterViewInit() {
    this._metfpetService.getGenreList().subscribe(data => {
      this.genreList = data;
      this._cd.markForCheck();
    });
  }

  allowResetPassword() {
    return !this.isCreation;
  }

  public resetPassword(): void {
    if(!this.isCreation && this._model) {
      this._store.dispatch(showLoading());
      this._metfpetService.resetDemandeurPassword(this._model.id)
      .subscribe(
        success => {
          if (success) {
            this._store.dispatch(showSuccess({message: 'Le mot de passe a été réinitialisé'}));
          } else {
            this._store.dispatch(showError({message: 'Échec de la réinitialisation du mot de passe'}));
          }
        },
        (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public save(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._model = Object.assign({}, this._model, this.form.value);
        this._store.dispatch(showLoading());
        var saveDemandeur$: Observable<any>;
  
        if (this.isCreation) {
          var registerDemandeurViewModel = MetfpetServiceAgent.RegisterDemandeurViewModel.fromJS(this.form.value);
          saveDemandeur$ = this._metfpetService.registerDemandeur(registerDemandeurViewModel);
  
        } else {
          var demandeurProfileViewModel = MetfpetServiceAgent.DemandeurProfileViewModel.fromJS(this._model);
          saveDemandeur$ = this._metfpetService.updateDemandeurProfile(demandeurProfileViewModel);
        }
  
        saveDemandeur$
          .subscribe(
            () =>  {
              this._store.dispatch(showSuccess({}));
              this.onSavedDemandeur.emit(this._model);
            },
            error => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
    }
  }

  goBack() {
    this._location.back();
  }

  onRefuse() {
    this._dialogService.openInfoDialog({
      width: '600px',
      data: {
        title: 'Valider la conformité du dossier',
        severity: SeverityEnum.INFO, 
        closeBtnText: 'Je confirme que le dossier est refusé',
        message: `Veuillez confirmer votre action`,
        class: 'bg-warn'
      }
    })
    .afterClosed().subscribe(() => {
      this._store.dispatch(showLoading());
      let request = new MetfpetServiceAgent.ValidateDemandeurCompte();
      request.action = MetfpetServiceAgent.ValidateDemandeurCompteAction.Refuse;
      request.demandeurId = this._model.id;
      this._metfpetService.validateDemandeurCompte(request)
      .subscribe(
        () =>  {
          this._store.dispatch(showSuccess({}));
          this._location.back();
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
    });
  }

  onAccept() {
    this._dialogService.openInfoDialog({
      width: '600px',
      data: {
        title: 'Valider la conformité du dossier',
        severity: SeverityEnum.INFO, 
        closeBtnText: 'Je confirme que le dossier est valide et conforme',
        message: `Veuillez confirmer votre action`,
        class: 'bg-green'
      }
    })
    .afterClosed().subscribe(() => {
      this._store.dispatch(showLoading());
      let request = new MetfpetServiceAgent.ValidateDemandeurCompte();
      request.action = MetfpetServiceAgent.ValidateDemandeurCompteAction.Accept;
      request.demandeurId = this._model.id;
      this._metfpetService.validateDemandeurCompte(request)
      .subscribe(
        () =>  {
          this._store.dispatch(showSuccess({}));
          this._location.back();
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
    });
  }

  allowAcceptOrRefuse() {
    return !this.isCreation && this.fromComptesDemandeurs && this._model && 
      (this._model.demandeurStatus === <any> DemandeurStatus.CreationAnalyse || 
       this._model.demandeurStatus === <any> DemandeurStatus.ModificationAnalyse);
  }

  openAttachedDemandeurDocument(attachedDocument: MetfpetServiceAgent.AttachedDemandeurDocumentDTO) {
    window.open(attachedDocument.link, "_blank");
  }

  openDemandeDetail(row: MetfpetServiceAgent.DemandeViewModel) {
    this._router.navigate([`agrements/agrements/view/${row.id}`]);
  }

  openCertificateDocument(demande: MetfpetServiceAgent.DemandeViewModel): void {
    if (demande.certificateLink) {
      window.open(demande.certificateLink, "_blank");
    }
  }
}