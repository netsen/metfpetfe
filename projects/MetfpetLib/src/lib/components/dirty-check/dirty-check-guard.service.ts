import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable, of as observableOf } from 'rxjs';
import { switchMap, take, map } from 'rxjs/operators';
import { DialogService } from '../dialogs';

export interface DirtyComponent {
  isDirty$: Observable<boolean>;
}

@Injectable()
export class DirtyCheckGuardService implements CanDeactivate<DirtyComponent> {

  public constructor(private _dialogService: DialogService) {
    
  }

  canDeactivate(component: DirtyComponent) {
    return component.isDirty$.pipe(
      switchMap(dirty => {
        if (dirty === false) {
          return observableOf(true);
        }

        return this._dialogService.openConfirmDialog({
          width: '420px',
          data: {
            title: "Confirmation de fermeture", 
            message: " Toutes les données non sauvegardées seront perdues. Êtes-vous sûr de vouloir quitter cette page ?", 
            confirmBtnText: 'Oui'
          }
        }).afterClosed();
      }),
      take(1)
    );
  }
}