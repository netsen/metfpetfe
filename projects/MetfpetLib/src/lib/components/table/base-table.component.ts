import { ChangeDetectorRef, Directive, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators/';
import { Store, select } from '@ngrx/store';
import { BaseReactiveComponent } from './base-reactive.component';
import { MatMenu } from '@angular/material/menu';
import { Subject, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { PerfectScrollService } from './perfect-scroll.service';
import { saveSearchState } from '../../store/actions';
import { selectSearchState } from '../../store/selectors';

@Directive()
export abstract class BaseTableComponent extends BaseReactiveComponent {

  total: number;
  pageSize: number = 10;
  pageOffset: number = 0;
  pageRows = [];
  searchForm: FormGroup;
  loadingIndicator: boolean = true;
  sort: any;
  isFirstSearch: boolean = true;
  searchState: any;
  defaultSearchFormValue = {};
  @ViewChild('filterMenu') filterMenu: MatMenu;

  constructor(
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder, 
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService
  ) {
    super();
    this._createSearchForm();
  }

  ngOnInit() {
    this._store.pipe(select(selectSearchState)).subscribe(
      (searchState: any) => {
        if (searchState && searchState[this._router.url]) {
          let criteria = searchState[this._router.url];
          this.pageSize = criteria.pageSize;
          this.pageOffset = criteria.pageIndex;
          this.sort = criteria.sort;
          this.searchForm.patchValue({...criteria.filters}, {emitEvent: false});
          this._cd.markForCheck();
        }
      }
    );
  }

  // This method is to be overriden by the sub-class to build search form
  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({});
  }

  public onChangePage(pageInfo: {pageSize?: number, offset?: number}): void {
    this.isFirstSearch = false;
    this.pageSize = pageInfo.pageSize;
    this.pageOffset = pageInfo.offset;
    this.pageRows = [];
    const container = document.querySelector('#main');
    container.scrollTop = 0;

    let criteria = Object.assign({}, {
      pageSize: this.pageSize,
      pageIndex: this.pageOffset,
      filters: this.searchForm.value,
      sort: this._sortBy()
    });

    this.loadingIndicator = true;
    this._cd.markForCheck();
    this._search(criteria).pipe(takeUntil(this.ngDestroyed$))
      .subscribe(data => {
        if (Array.isArray(data)) {
          this.pageRows = data;
          this.total = data.length;
        } else {
          this.pageRows = data.results;
          this.total = data.totalCount;
        }
      })
      .add(() => {
        this.searchState = Object.assign({}, this.searchState);
        this.searchState[this._router.url] = criteria;
        this._store.dispatch(saveSearchState({searchState: this.searchState}));
        this.loadingIndicator = false;
        this._perfectScrollService.update();
        this._cd.markForCheck();
      });
  }

  public triggerSearch() {
    let offset = this.isFirstSearch ? this.pageOffset : 0;
    this.onChangePage({pageSize: this.pageSize, offset});
  }

  public resetSearchForm() {
    this.searchForm.reset(this.defaultSearchFormValue, {emitEvent: false});
    this.onChangePage({pageSize: this.pageSize, offset: 0});
  }

  // This method is to be overriden by the sub-class to fetch data
  protected abstract _search(criteria: any): Observable<any>;

  // This method is to be overriden by the sub-class to provide Sorting
  private _sortBy(): any {
    return this.sort ? {
      nameKey: this.sort.prop,
      isAscending: this.sort.dir === 'asc'
    } : null;
  }

  public onSort(event): any {
    this.sort = event.sorts[0];
    this.triggerSearch();
  }

  ngAfterViewInit() {
    if (this.filterMenu) {
      // Inject our custom logic of menu close
      (this.filterMenu as any).closed = this.filterMenu.close = this._configureMenuClose(this.filterMenu.close);
    }
  }

  private _configureMenuClose(old: MatMenu['close']): any {
    const upd = new EventEmitter();
    feed(upd.pipe(
      filter(event => {
        if (event === 'click') {
          // Ignore clicks inside the menu 
          return false;
        }
        return true;
      }),
    ), old);
    return upd;
  }
}

function feed<T>(from: any, to: Subject<T>): Subscription {
  return from.subscribe(
    data => to.next(data),
    err => to.error(err),
    () => to.complete(),
  );
}
