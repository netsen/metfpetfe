import { Injectable, QueryList } from '@angular/core';
import { PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';

@Injectable({providedIn: 'root'})
export class PerfectScrollService {

  private _pss: QueryList<PerfectScrollbarDirective>

  constructor() {}

  public setScrollbars(pss: QueryList<PerfectScrollbarDirective>) {
    this._pss = pss;
  }

  update() {
    if (this._pss) {
      this._pss.forEach(ps => {
        if (ps.elementRef.nativeElement.id == 'main'){
          ps.update();
        }
      });
    }
  }
}