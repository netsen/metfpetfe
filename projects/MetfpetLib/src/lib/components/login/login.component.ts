import { 
  ChangeDetectionStrategy,
  Component, 
  Input, 
  Output, 
  EventEmitter,
  TemplateRef,
  ViewEncapsulation
} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

export interface LoginEvent {
  identifier: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent {

  @Input() loading: boolean;
  @Input() sectionTemplate: TemplateRef<any>;
  @Input() userNameLabel: string = 'Identifiant';
  @Output() onSubmitLogin = new EventEmitter<LoginEvent>();
  public form: FormGroup;
  public error: string;
  hide = true;

  constructor(
    private _fb: FormBuilder,
    private _route: ActivatedRoute
  ) {
    this.form = this._fb.group({
      'identifier': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'password': [null, Validators.compose([Validators.required, Validators.minLength(6)])],
    });
  }

  ngOnInit() {
    this._route.queryParams.subscribe(params => {
      this.error = params['error'] === 'unauthorized' ? 'Votre session a été expirée' : null;
    });
  }

  public onSubmit(): void {
    if (this.form.valid) {
      this.loading = true;
      this.onSubmitLogin.emit({
        identifier: this.form.controls['identifier'].value,
        password: this.form.controls['password'].value,
      })
    }
  }

}