import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { IUser } from '../../interfaces';
import { Store } from '@ngrx/store';
import { AuthActions, UserActions } from '../../store/actions';
import { AuthorityEnum } from '../../interfaces';

@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class UserMenuComponent implements OnInit {
  
  @Input() user: IUser;
  userRole: string;
  
  constructor(
    private _store: Store<any>
    ) { 
  }

  ngOnInit() {
    this.userRole = "TODO";
  }

  openProfileDialog() {
    this._store.dispatch(UserActions.editProfile());
  }

  openModifyPasswordDialog() {
    this._store.dispatch(UserActions.modifyPassword());
  }

  disconnect() {
    this._store.dispatch(AuthActions.logout());
  }
}
