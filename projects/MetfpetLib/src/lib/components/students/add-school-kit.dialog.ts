import { ChangeDetectorRef, Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { MetfpetServiceAgent } from "../../services";
import { hideLoading, showError, showException, showLoading, showSuccess } from "../../store/actions";
import { Guid } from 'guid-typescript';

@Component({
  templateUrl: './add-school-kit.dialog.html',
})
export class StudentSchoolKitDialog {
  
  form: FormGroup;
  model: MetfpetServiceAgent.SchoolKitEtudiantDTO;
  etudiantId: string;
  schoolKitList: Array<MetfpetServiceAgent.SchoolKitRowViewModel>;

  constructor(
    public dialogRef: MatDialogRef<StudentSchoolKitDialog>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.etudiantId = data.etudiantId;
    this.form = this._formBuilder.group({
      'schoolKitId': [null, Validators.required]
    });
    this._metfpetService.getSchoolKitListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.schoolKitList = data.results;
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public onSubmit(){
    if(this.form.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.createSchoolKitEtudiant(MetfpetServiceAgent.SchoolKitEtudiantDTO.fromJS({
        id: Guid.EMPTY,
        schoolKitId: this.form.get('schoolKitId').value,
        etudiantId: this.etudiantId
      }))
        .subscribe(
          (result) => {
            this._store.dispatch(showSuccess({}));
            this.dialogRef.close(true);
          },
          (error) => this._store.dispatch(showError({message: error.message}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }
}
