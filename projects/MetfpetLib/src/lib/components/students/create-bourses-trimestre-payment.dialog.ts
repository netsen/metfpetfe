import { Component, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Store } from "@ngrx/store";
import { MetfpetServiceAgent } from "../../services";
import { validateForm } from '../../utils';

const EMPHASIS_CURRENT_STEP_CSS = "text-bold";
const BIOMETRICS_CSS_INIT = "payment-biometrics-circle initial";
const BIOMETRICS_CSS_PASSED = "payment-biometrics-circle passed";
const BIOMETRICS_CSS_FAILED = "payment-biometrics-circle error";

const CREATE_BOURSES_PAYMENT_STEP_1 = "Identification";
const CREATE_BOURSES_PAYMENT_STEP_2 = "Authentification";
const CREATE_BOURSES_PAYMENT_STEP_3 = "Paiement du montant";

const BIOMETRICS_AUTH_STATUS_INIT = "initial";
const BIOMETRICS_AUTH_STATUS_PASSED = "passed";
const BIOMETRICS_AUTH_STATUS_FAILED = "error";

const PAYMENT_STATUS_INIT = "initial";
const PAYMENT_STATUS_SUCCEEDED = "success";
const PAYMENT_STATUS_FAILED = "failed";

@Component({
  templateUrl: "./create-bourses-trimestre-payment.dialog.html",
  styleUrls: ["./create-bourses-trimestre-payment.dialog.scss"],
})
export class CreateBoursesTrimestrePaymentDialog {
  trimestreList: Array<MetfpetServiceAgent.BourseTrimestreDTO>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;

  currentStepName: string;
  currentBiometricsStatus: string;
  currentPaymentStatus: string;

  curInaNumber: string;
  curTrimestreId: string;
  curAnneeAcademiqueId: string;

  totalPaymentAmount: number;
  monthAndSessionText: string;
  paymentAvailability: string;
  isManualMode: boolean;
  hasChanges: boolean;
  keepDlgOpenedToPayAnotherOne: boolean;

  biometricStatus: string;

  title: string;
  form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<CreateBoursesTrimestrePaymentDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.title = "Paiement des bourses";
    this.loadAnneeAcademiqueList();
    this.loadTrimestreList();
    this.initFirstStep();
    this.parseInputData(data);
  }

  loadAnneeAcademiqueList() {
    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
    });
  }

  loadTrimestreList() {
    this._metfpetService
      .getBourseTrimestreList().subscribe((data) => {
        this.trimestreList = data.results;
      });
  }

  initFirstStep() {
    this.currentStepName = CREATE_BOURSES_PAYMENT_STEP_1;
    this.currentBiometricsStatus = BIOMETRICS_AUTH_STATUS_INIT;
    this.currentPaymentStatus = PAYMENT_STATUS_INIT;

    this.curInaNumber = "";
    this.monthAndSessionText = "";
    this.totalPaymentAmount = 0;

    this.form = this._formBuilder.group({
      inaNumber: null,
      trimestreId: null,
      anneeAcademiqueId: null,
    });
  }

  private parseInputData(data: any){
    this.isManualMode = true;
    if (data.curInaNumber) {
      this.curInaNumber = data.curInaNumber;
      this.isManualMode = false;
      this.form.patchValue({inaNumber: this.curInaNumber});
    }
  }

  getCurrentStepCss(step: string) {
    if (this.currentStepName === step) return EMPHASIS_CURRENT_STEP_CSS;
    return "";
  }

  getBiometricCircleCss() {
    if (this.currentBiometricsStatus === BIOMETRICS_AUTH_STATUS_INIT) {
      return BIOMETRICS_CSS_INIT;
    } else if (this.currentBiometricsStatus === BIOMETRICS_AUTH_STATUS_PASSED) {
      return BIOMETRICS_CSS_PASSED;
    } else if (this.currentBiometricsStatus === BIOMETRICS_AUTH_STATUS_FAILED) {
      return BIOMETRICS_CSS_FAILED;
    }
  }

  goToNextStep() {
    this.identifyIna();
  }

  private identifyIna() {
    let inaNo = this.form.controls.inaNumber.value;
    let trimestreId = this.form.controls.trimestreId.value;
    let anneeAcademiqueId = this.form.controls.anneeAcademiqueId.value;
    this._metfpetService
      .getTrimestrePaiementBourseInfo(
        MetfpetServiceAgent.TrimestrePaiementBourseRequest.fromJS({
          identifiantNationalEleve: inaNo,
          trimestreId: trimestreId,
          anneeAcademiqueId: anneeAcademiqueId,
        })
      )
      .subscribe((data) => {
        this.totalPaymentAmount = data.totalAmount;
        this.monthAndSessionText = data.paymentContent;
        this.biometricStatus = data.biometricStatus;
        this.curInaNumber = inaNo;
        this.curTrimestreId = trimestreId;
        this.curAnneeAcademiqueId = anneeAcademiqueId;

        if (this.biometricStatus === 'Non biomtérisé') 
        {
          this.biometricsAuthFailed();
        }
        else if (this.totalPaymentAmount > 0) 
        {
          this.identifyInaSucceeded();
        }
        else 
        {
          this.identifyInaFailed();
        }
      }, (error) => {
        this.identifyInaFailed();
      });
  }

  private getSelectedPaymentBourses() {
    this.biometricsAuth();
  }

  private identifyInaSucceeded() {
    this.currentStepName = CREATE_BOURSES_PAYMENT_STEP_2;
    this.currentBiometricsStatus = BIOMETRICS_AUTH_STATUS_INIT;
    this.currentPaymentStatus = PAYMENT_STATUS_INIT;

    setTimeout(() => {
      this.getSelectedPaymentBourses();
    }, 200);
  }

  private identifyInaFailed() {}

  biometricsAuth() {
    //TODO: implement this later
    //Currently we bypass the biometrics auth
    setTimeout(() => {
      this.biometricsAuthSucceeded();
    }, 500);
  }

  private biometricsAuthSucceeded() {
    this.currentStepName = CREATE_BOURSES_PAYMENT_STEP_3;
    this.currentBiometricsStatus = BIOMETRICS_AUTH_STATUS_PASSED;
    this.currentPaymentStatus = PAYMENT_STATUS_INIT;
  }

  private biometricsAuthFailed() {
    this.currentStepName = CREATE_BOURSES_PAYMENT_STEP_2;
    this.currentBiometricsStatus = BIOMETRICS_AUTH_STATUS_FAILED;
    this.currentPaymentStatus = PAYMENT_STATUS_INIT;
  }

  private paymentProcessingSucceeded() {
    this.currentStepName = CREATE_BOURSES_PAYMENT_STEP_3;
    this.currentPaymentStatus = PAYMENT_STATUS_SUCCEEDED;

    if (this.keepDlgOpenedToPayAnotherOne == false)
      this.onClose();
    else {
      this.title = "Paiement des bourses";
      this.initFirstStep();  
    }
  }

  private paymentProcessingFailed() {
    this.currentStepName = CREATE_BOURSES_PAYMENT_STEP_3;
    this.currentPaymentStatus = PAYMENT_STATUS_FAILED;
  }

  private executeTrimestrePaymentBourse() {
    this._metfpetService
      .executeTrimestrePaiementBourse(
        MetfpetServiceAgent.TrimestrePaiementBourseRequest.fromJS({
          identifiantNationalEleve: this.curInaNumber,
          trimestreId: this.curTrimestreId,
          anneeAcademiqueId: this.curAnneeAcademiqueId,
        })
      )
      .subscribe((data) => {
        if (data && data.length > 0) {
          this.monthAndSessionText =
            data.map((x) => x.moisAnneeName).join(", ") +
            "; " +
            data[0].etudiantBourseSessionName;
          this.hasChanges = true;
          this.paymentProcessingSucceeded();
        }
      }, (error) => {
        this.paymentProcessingFailed();
      });
  }

  

  private doPayment(){
    this.executeTrimestrePaymentBourse();
  }

  confirmPayment() {
    this.keepDlgOpenedToPayAnotherOne = false;
    this.doPayment();
  }

  confirmPaymentAndCreateNew() {
    this.keepDlgOpenedToPayAnotherOne = true;
    this.doPayment();
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
    }
  }

  onClose() {
    this._dialogRef.close(this.hasChanges);
  }
}
