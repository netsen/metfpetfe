import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { MetfpetServiceAgent } from '../../services';
import { validateForm } from '../../utils';
import { DialogService, SeverityEnum } from '../dialogs';
import { showSuccess, showError, showLoading, hideLoading, showException } from '../../store/actions';
import { SelectionType } from '@swimlane/ngx-datatable';
import { UpdateStudentBourseDialog } from './update-student-bourse.dialog';
import { HistoryBourseDialog } from './history-bourse.dialog';

@Component({
  templateUrl: './manage-student-bourse.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageStudentBourseDialog {

  studentId: string;
  etudiantBourses: Array<MetfpetServiceAgent.EtudiantBourseRowViewModel>;
  activeEtudiantBourses: Array<MetfpetServiceAgent.EtudiantBourseRowViewModel>;
  inactiveEtudiantBourses: Array<MetfpetServiceAgent.EtudiantBourseRowViewModel>;
  selectedActiveRows: Array<MetfpetServiceAgent.EtudiantBourseRowViewModel>;
  selectedInactiveRows: Array<MetfpetServiceAgent.EtudiantBourseRowViewModel>;
  SelectionType = SelectionType;
  etudiantBourseModifications: Array<MetfpetServiceAgent.EtudiantBourseModificationRowViewModel>;
  
  constructor(
    public dialogRef: MatDialogRef<ManageStudentBourseDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _cd: ChangeDetectorRef,
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.studentId = data.studentId;
    this.etudiantBourses = data.etudiantBourses;
    this.etudiantBourseModifications = data.etudiantBourseModifications;
    if (this.etudiantBourses) {
      this.activeEtudiantBourses = this.etudiantBourses.filter(x => x.etudiantBourseStatusName === 'Actif');
      this.inactiveEtudiantBourses = this.etudiantBourses.filter(x => x.etudiantBourseStatusName !== 'Actif');
    }
    this.selectedActiveRows = [];
    this.selectedInactiveRows = [];
  }

  private loadEtudiantBourses() {
    this._metfpetService.getEtudiantBourseList(this.studentId).subscribe(data => {
      this.etudiantBourses = data;
      if (this.etudiantBourses) {
        this.activeEtudiantBourses = this.etudiantBourses.filter(x => x.etudiantBourseStatusName === 'Actif');
        this.inactiveEtudiantBourses = this.etudiantBourses.filter(x => x.etudiantBourseStatusName !== 'Actif');
      }
      this._cd.markForCheck();
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  onSelectActive({ selected }) {
    this.selectedActiveRows.splice(0, this.selectedActiveRows.length);
    this.selectedActiveRows.push(...selected);
  }

  onSelectInactive({ selected }) {
    this.selectedInactiveRows.splice(0, this.selectedInactiveRows.length);
    this.selectedInactiveRows.push(...selected);
  }

  public deactiveBourses(): void {
    if (this.selectedActiveRows.length > 0) {
      this._dialogService.openDialog(
        UpdateStudentBourseDialog,
        {
          width: '600px',
          data: {
            ids: this.selectedActiveRows.filter(x => !!x.id).map(x => x.id),
            lossBourse: true,
          }
        }
      ).afterClosed().subscribe(data => {
        if (data) {
          this.loadEtudiantBourses();
        }
      });
    }
  }

  public reactiveBourses(): void {
    if (this.selectedInactiveRows.length > 0) {
      this._dialogService.openDialog(
        UpdateStudentBourseDialog,
        {
          width: '600px',
          data: {
            ids: this.selectedInactiveRows.filter(x => !!x.id).map(x => x.id),
            lossBourse: false,
          }
        }
      ).afterClosed().subscribe(data => {
        if (data) {
          this.loadEtudiantBourses();
        }
      });
    }
  }

  openEtudiantBourseModification(row: MetfpetServiceAgent.EtudiantBourseModificationRowViewModel) {
    this._dialogService.openDialog(
      HistoryBourseDialog,
      {
        width: '600px',
        data: {
          viewModel: row
        }
      }
    );
  }

  downloadHistories() {
    this._metfpetService.getEtudiantBourseModificationList(this.studentId).subscribe(data => {
      this.etudiantBourseModifications = data;
      this._cd.markForCheck();
    });
  }
}