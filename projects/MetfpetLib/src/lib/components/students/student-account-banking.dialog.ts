import { ChangeDetectorRef, Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { MetfpetServiceAgent } from "../../services";
import { hideLoading, showError, showException, showLoading, showSuccess } from "../../store/actions";
import { Guid } from 'guid-typescript';

@Component({
  selector: 'app-student-account-banking',
  templateUrl: './student-account-banking.dialog.html',
  styleUrls: ['./student-account-banking.dialog.css']
})
export class StudentAccountBankingComponent {
  
  form: FormGroup;
  model: MetfpetServiceAgent.EtudiantNSGBankingInfosDTO;

  constructor(
    public dialogRef: MatDialogRef<StudentAccountBankingComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) { 
    this.form = this._formBuilder.group({
      'id': Guid.EMPTY,
      'etudiantId': data.etudiantId,
      'emailCompte': [null],
      'identifiantCompte': [null, Validators.required],
      'numeroCompte': [null, Validators.required],
    });
    
    if (data.etudiantNSGBankingInfo) {
      this.form.patchValue({
        'id': data.etudiantNSGBankingInfo.id,
        'etudiantId': data.etudiantNSGBankingInfo.etudiantId,
        'emailCompte': data.etudiantNSGBankingInfo.emailCompte,
        'identifiantCompte': data.etudiantNSGBankingInfo.identifiantCompte,
        'numeroCompte': data.etudiantNSGBankingInfo.numeroCompte,
      });
    }
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public OnSubmit(){
    if(this.form.valid)
    {
      this._store.dispatch(showLoading());
      this._metfpetService.updateEtudiantNSGBankingInfos(MetfpetServiceAgent.EtudiantNSGBankingInfosDTO.fromJS(this.form.getRawValue()))
        .subscribe(
          (result) => {
            this._store.dispatch(showSuccess({}));
            this.dialogRef.close(true);
          },
          (error) => this._store.dispatch(showError({message: error.message}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }
}
