import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { MetfpetServiceAgent } from '../../services';
import { validateForm } from '../../utils';
import { DialogService, SeverityEnum } from '../dialogs';
import { showSuccess, showError, showLoading, hideLoading, showException } from '../../store/actions';
import { AdmissionInstitutionOptionValues } from '../../interfaces';

@Component({
  templateUrl: './manage-student-admission.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageStudentAdmissionDialog {

  form: FormGroup;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  currentSessionAdmission: MetfpetServiceAgent.SessionAdmissionDTO;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  isProgramRequired : boolean;
  allowChoose3Program: boolean;
  optionList = AdmissionInstitutionOptionValues;

  constructor(
    public dialogRef: MatDialogRef<ManageStudentAdmissionDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.student = data.student;
    this.currentSessionAdmission = data.currentSessionAdmission;
    this.isProgramRequired = false;
    this.allowChoose3Program = false;
    this.form = this._formBuilder.group({
      'id': null,
      'etudiantId': this.student.id,
      'sessionAdmissionId': this.currentSessionAdmission.id,
      'prefectureId': [null, Validators.required],
      'institutionId': [null, Validators.required],
      'option': [null, Validators.required],
      'programmeId': [null],
      'programme1Id': [null],
      'programme2Id': [null],
      'programme3Id': [null],
    });

    this.form.get('prefectureId').valueChanges.pipe(
      tap(prefectureId => {
        this.institutionList = [];
        this.programList = [];
        this.form.get('institutionId').setValue(null);
        this.form.get('programmeId').setValue(null);
        this.form.get('programme1Id').setValue(null);
        this.form.get('programme2Id').setValue(null);
        this.form.get('programme3Id').setValue(null);
        if (!prefectureId) {
          return;
        }
        this._loadInstitutions(prefectureId);
      })
    ).subscribe();

    this.form.get('institutionId').valueChanges.pipe(
      tap(institutionId => {
        this.programList = [];
        this.form.get('programmeId').setValue(null);
        this.form.get('programme1Id').setValue(null);
        this.form.get('programme2Id').setValue(null);
        this.form.get('programme3Id').setValue(null);
        if (!institutionId) {
          return;
        }
        this._loadPrograms(institutionId);
      })
    ).subscribe();
    
    this.form.get('option').valueChanges.pipe(
      tap(optionValue => {
        this.updateOptionValue(optionValue);
        this._cd.markForCheck(); ;
      })
    ).subscribe();
  }

  updateOptionValue(optionValue : number) {
    if(optionValue == 1 || optionValue == 2){
      this.isProgramRequired = false;
      this.allowChoose3Program = true;
    }
    else if(optionValue >= 3 && optionValue <= 9){
      this.isProgramRequired = true;
      this.allowChoose3Program = false;
    }
    else{
      this.isProgramRequired = false;
      this.allowChoose3Program = false;
    }
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cd.markForCheck(); 
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public onConfirm(): void {
    validateForm(this.form);
    if (this.form.valid) {
      var valid = true;
      if(this.isProgramRequired && !this.form.get('programmeId').value){
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.ERROR, 
            message: 'Veuillez sélectionner un programme'
          }
        });
        valid = false;
      }
      if (this.allowChoose3Program && 
          ((this.form.get('programme1Id').value && this.form.get('programme1Id').value === this.form.get('programme2Id').value) ||
           (this.form.get('programme1Id').value && this.form.get('programme1Id').value === this.form.get('programme3Id').value) ||
           (this.form.get('programme2Id').value && this.form.get('programme2Id').value === this.form.get('programme3Id').value)))
      {
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.ERROR, 
            message: 'Les programmes sont dupliqués'
          }
        });
        valid = false;
      }
      if(valid){
        this._store.dispatch(showLoading());
  
        this._metfpetService.createAdmissionInstitutionForAdmin(
          MetfpetServiceAgent.CreateAdmissionInstitutionForAdmin.fromJS(this.form.getRawValue()))
          .subscribe(
            (data : any) => {
              this._store.dispatch(showSuccess({}));
              this.dialogRef.close(data);
            },
            (error) => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    }
  }
  
  private _loadInstitutions(prefectureId: string) {
    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
         pageIndex: -1, filters: {prefecture: prefectureId}
      })).subscribe(data => {
        this.institutionList = data.results;
        this._cd.markForCheck();
      });
  }

  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
         pageIndex: -1, filters: {institution: institutionId}
      })).subscribe(data => {
        this.programList = data.results;
        this._cd.markForCheck();
      });
  }
}