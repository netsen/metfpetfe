import { 
  ChangeDetectionStrategy,
  Component, 
  Inject,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MetfpetServiceAgent } from '../../services';
import { AdmissionDecision, AdmissionDecisionValues } from '../../interfaces';

@Component({
  templateUrl: './admission-offer-decision-admin.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdmissionOfferDecisionAdminDialog {

  form: FormGroup;
  admissionInstitutionRowViewModel: MetfpetServiceAgent.AdmissionInstitutionRowViewModel;
  decisionList: Array<any>;
  attachedDocumentInfo: MetfpetServiceAgent.AttachedDocumentInfo;
  showAttachedDocument: boolean;
  
  constructor(
    public dialogRef: MatDialogRef<AdmissionOfferDecisionAdminDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _formBuilder: FormBuilder,
  ) {
    this.admissionInstitutionRowViewModel = data.admissionInstitutionRowViewModel;
    this.attachedDocumentInfo = data.attachedDocumentInfo;
    this.showAttachedDocument = this.admissionInstitutionRowViewModel.surDossier;
    this.decisionList = AdmissionDecisionValues;
    this.form = this._formBuilder.group({
      'id': null
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  private findDecisionName(decision: AdmissionDecision): string {
    let decisionObj =  this.decisionList.find(v => v.value === decision);
    if (decisionObj) {
      return decisionObj.name;
    }
    return '';
  }

  public isRefuseTheOffer(): boolean {
    return this.admissionInstitutionRowViewModel.decisionName === this.findDecisionName(AdmissionDecision.OffreRefusee);
  }

  public isAcceptTheOffer(): boolean {
    return this.admissionInstitutionRowViewModel.decisionName === this.findDecisionName(AdmissionDecision.OffreAcceptee);
  }

  public openActeNaissance(): void {
    if (this.attachedDocumentInfo.acteNaissanceLink) {
      window.open(this.attachedDocumentInfo.acteNaissanceLink, "_blank");
    }
  }

  public openPhoto(): void {
    if (this.attachedDocumentInfo.photoLink) {
      window.open(this.attachedDocumentInfo.photoLink, "_blank");
    }
  }

  public openLettre(): void {
    if (this.attachedDocumentInfo.lettreLink) {
      window.open(this.attachedDocumentInfo.lettreLink, "_blank");
    }
  }

  public openAttestation(): void {
    if (this.attachedDocumentInfo.attestationLink) {
      window.open(this.attachedDocumentInfo.attestationLink, "_blank");
    }
  }

  public openDiplomeCEE(): void {
    if (this.attachedDocumentInfo.diplomeCEELink) {
      window.open(this.attachedDocumentInfo.diplomeCEELink, "_blank");
    }
  }

  public openCertificatVM(): void {
    if (this.attachedDocumentInfo.certificatVMLink) {
      window.open(this.attachedDocumentInfo.certificatVMLink, "_blank");
    }
  }
}