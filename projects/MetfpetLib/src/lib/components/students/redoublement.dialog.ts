import { ChangeDetectorRef, Component, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Store } from "@ngrx/store";
import { MetfpetServiceAgent } from "../../services";
import { hideLoading, showException, showLoading, showSuccess } from "../../store/actions";
import { validateForm } from '../../utils';

const EMPHASIS_CURRENT_STEP_CSS = "text-bold";

const REDOUBLEMENT_STEP_1 = "Identification";
const REDOUBLEMENT_STEP_2 = "Confirmer le redoublement";

@Component({
  templateUrl: "./redoublement.dialog.html",
  styleUrls: ["./redoublement.dialog.scss"],
})
export class RedoublementDialog {
  currentStepName: string;
  curINA: number;
  inscription: MetfpetServiceAgent.InscriptionProgrammeDTO;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  title: string;
  form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<RedoublementDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _cd: ChangeDetectorRef,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.title = "Indiquer le redoublement";
    this.anneeAcademiqueList = data.anneeAcademiqueList;
    this.initFirstStep();
  }

  initFirstStep() {
    this.currentStepName = REDOUBLEMENT_STEP_1;
    this.curINA = null;
    this.form = this._formBuilder.group({
      ina: [null, Validators.required],
      periode: [null, Validators.required],
    });
  }

  getCurrentStepCss(step: string) {
    if (this.currentStepName === step)
    {
      return EMPHASIS_CURRENT_STEP_CSS;
    }
    return "";
  }

  goToNextStep() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());    
      let ina = this.form.controls.ina.value;
      this._metfpetService.identifyRedoublement(MetfpetServiceAgent.RedoublementRequest.fromJS(this.form.getRawValue()))
        .subscribe(
          (data : any) => {
            this.curINA = ina;
            this.inscription = data;
            this.currentStepName = REDOUBLEMENT_STEP_2;
            this._cd.markForCheck();
          },
          (error) => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onSave(needToCloseDialog: boolean) {
    this._store.dispatch(showLoading());

    this._metfpetService.redoublement(this.inscription)
      .subscribe(
            () => {
              this._store.dispatch(showSuccess({}));
              if (needToCloseDialog) {
                this._dialogRef.close(true);
              } else {
                this.initFirstStep();
              }
            },
            error => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
  }

  save() {
    this.onSave(true);
  }

  saveAndEnterNewCase() {
    this.onSave(false);
  }

  onClose() {
    this._dialogRef.close();
  }
}
