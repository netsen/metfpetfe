import {
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Component, 
  Input, 
  Output, 
  EventEmitter, 
} from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormArray} from '@angular/forms';
import { forkJoin, Observable } from 'rxjs';
import { tap, take, finalize } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { AdmissionDecision, AdmissionDecisionValues, AdmissionInstitutionStatus, AdmissionInstitutionStatusValues, CountryValues, FinalExamenStatusGlobal, ImpressionDocumentStatus, IUser, RequirePayment, SessionAdmissionStatus, SUR_CONCOURS, DiplomeStatus, ObservationTypeValues } from '../../interfaces';
import { AuthService, MetfpetServiceAgent } from '../../services';
import { emailValidator, validateForm } from '../../utils';
import { 
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  showException,
} from '../../store/actions/';
import { ManageStudentInscriptionDialog } from './manage-student-inscription.dialog';
import { DialogService, SeverityEnum } from '../dialogs';
import { selectLoggedInPerson } from '../../store/selectors';
import { AdmissionDecisionAdminDialog } from './admission-decision-admin.dialog';
import { Guid } from 'guid-typescript';
import { ManageStudentAdmissionDialog } from './manage-student-admission.dialog';
import { ImpressionDocumentConfirmationCodeDialog, ImpressionDocumentPaymentDialog, ImpressionDocumentPreviewDialog, SelectImpressionDocumentDialog } from '../documents';
import { ThrowStmt } from '@angular/compiler';
import { AdmissionOfferDecisionAdminDialog } from './admission-offer-decision-admin.dialog';
import { ManageUpdateStudentStatusDialog } from './manage-update-student-status.dialog';
import { ManageUpdateAdmissionDialog } from './manage-update-admission.dialog';
import { ManageStudentBourseDialog } from './manage-student-bourse.dialog';
import { CreateBoursesPaymentDialog } from './create-bourses-payment.dialog';
import { CreateBoursesTrimestrePaymentDialog } from './create-bourses-trimestre-payment.dialog';
import { GenerateStudentBourseManualDialog } from './generate-student-bourse-manual.dialog';
import { TelechargerLeReleveDeNotesDialog } from './telecharger-le-releve-de-notes.dialog';
import { AdmitGraduationDialog } from './admit-graduation.dialog';
import { StudentAccountBankingComponent } from './student-account-banking.dialog';
import { StudentConcoursDetailDialog } from './update-student-concours-detail.dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { StudentSchoolKitDialog } from './add-school-kit.dialog';
import { GenerateStudentReceiptFormDialog } from './generate-receipt-form.dialog';
import { StudentKitHistoryDialog } from './student-kit-history.dialog';
import { QuantityDecreaseReasonDialog } from './quantity-decrease-reason.dialog';

@Component({
  selector: 'app-manage-student',
  templateUrl: './manage-student.component.html',
  styleUrls: ['./manage-student.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageStudentComponent {

  form: FormGroup;
  isCreation: boolean;
  private _model: MetfpetServiceAgent.EtudiantProfileViewModel;
  currentUser: IUser;
  studenVerifiedPhoneNumber: string;

  get model(): MetfpetServiceAgent.EtudiantProfileViewModel {
    return this._model;
  }

  get resultatsMatieres(): Array<any> {
    if (this._model.optionTerminaleId) {
      return this._model.resultatsMatieresTerminale;
    }
    return this._model.resultatsMatieres;
  }
  
  get historiqueScolaireTitle(): string {
    if (this._model.optionTerminaleId) {
      return 'Résultats de Terminale';
    }
    return 'Résultats du BAC';
  }

  @Input()
  set studentId(studentId: string) {
    this.isCreation = !studentId;
    if (!studentId) return;
    
    this.loadingIndicator = true;
    this._metfpetService.getEtudiantDocumentInfo(studentId).subscribe(data => {
      this.documentInfo = data;
      this._cd.markForCheck();
    });
    this._metfpetService.getAvailableDocumentList(studentId).subscribe(data => {
      this.documents = data;
      this.allowRequestDocument = this.documents.length > 0;
      this._cd.markForCheck();
    });
    this.loadBourseData(studentId);
    this.loadEtudiantNSGBankingInfos(studentId);
    this.loadFinalExamenEtudiant(studentId);
    this.loadDiplome(studentId);
    this.loadConcoursEtudiant(studentId);
    this._metfpetService.getEtudiantProfile(studentId)
      .subscribe(data => {
        this._model = data;
        this.getVerifiedPhoneNumber(data.id);
        if(data && !data.photoPath){
          this._model.photoPath = "/assets/img/users/default-user.jpg";
        }
        this.setProfilePhoto();
        this.onLoadedStudent.emit(this._model);
  
        this.form.controls['identifiantNationalEleve'].setValidators(
          Validators.compose([Validators.required, Validators.minLength(3)]));
  
        if (!this.isCreation) {
          this.form.controls['email'].setValidators(
            Validators.compose([Validators.required, emailValidator]));
        
          this.form.controls['phone'].setValidators(
            Validators.compose([Validators.required, Validators.pattern('^[0-9]{9}$')]));
        }
  
        this.form.patchValue({
          email            : this._model.email,
          phone            : this._model.phone,
          identifiantNationalEleve: this._model.identifiantNationalEleve,
          name             : this._model.name,
          firstName        : this._model.firstName,
          dateNaissance    : this._model.dateNaissance,
          genreId          : this._model.genreId,
          prefectureId     : this._model.prefectureId,
          optionBACId      : this._model.optionBACId,
          optionBEPCId     : this._model.optionBEPCId,
          optionTerminaleId: this._model.optionTerminaleId,
          sessionBAC       : this._model.sessionBAC,
          sessionBEPC      : this._model.sessionBEPC,
          sessionTerminale : this._model.sessionTerminale,
          numeroPV         : this._model.numeroPV,
          lyceeId          : this._model.lyceeId,
          collegeId        : this._model.collegeId,
          lyceeTerminaleId : this._model.lyceeTerminaleId,
          centreExamenId   : this._model.centreExamenId,
          numeroPhoto      : this._model.numeroPhoto,
          lieuNaissance    : this._model.lieuNaissance,
          nomPere          : this._model.nomPere,
          nomMere          : this._model.nomMere,
          nationalite      : this._model.nationalite,
          remarque         : this._model.remarque,
          moyenneBAC       : this._model.moyenneBAC,
          moyenneBEPC      : this._model.moyenneBEPC,
          moyenneTerminale : this._model.moyenneTerminale,
          adresse          : this._model.adresse,
          ville            : this._model.ville,
          pays             : this._model.pays,
          moyenneGenerale  : this._model.moyenneGenerale,
          moyenneExamen    : this._model.moyenneExamen,
          rang             : this._model.rang,
          typeDiplomeSelection: this._model.niveauAccesCode
        }, { emitEvent: false });
        
        if (this._model.niveauAccesCode == "bac") {
          this.isBrevetSelected = false;
          this.isTerminaleSelected = false;
          this.isBacSelected = true;
          this.isPostSecondaireSelected = false;
          this.isPostPrimaireSelected = false;
          this.isAutreSelected = false;
        } else if (this._model.niveauAccesCode == "bepc") {
          this.isBrevetSelected = true;
          this.isTerminaleSelected = false;
          this.isBacSelected = false;
          this.isPostSecondaireSelected = false;
          this.isPostPrimaireSelected = false;
          this.isAutreSelected = false;
        } else if (this._model.niveauAccesCode == "terminale") {
          this.isBrevetSelected = false;
          this.isTerminaleSelected = true;
          this.isBacSelected = false;
          this.isPostSecondaireSelected = false;
          this.isPostPrimaireSelected = false;
          this.isAutreSelected = false;
        } else if (this._model.niveauAccesCode == "postSecondaire") {
          this.isBrevetSelected = false;
          this.isTerminaleSelected = false;
          this.isBacSelected = false;
          this.isPostSecondaireSelected = true;
          this.isPostPrimaireSelected = false;
          this.isAutreSelected = false;
        } else if (this._model.niveauAccesCode == "postPrimaire") {
          this.isBrevetSelected = false;
          this.isTerminaleSelected = false;
          this.isBacSelected = false;
          this.isPostSecondaireSelected = false;
          this.isPostPrimaireSelected = true;
          this.isAutreSelected = false;
        } else if (this._model.niveauAccesCode == "autre") {
          this.isBrevetSelected = false;
          this.isTerminaleSelected = false;
          this.isBacSelected = false;
          this.isPostSecondaireSelected = false;
          this.isPostPrimaireSelected = false;
          this.isAutreSelected = true;
        } 
        this.inscriptionsProgrammes = this._model.inscriptionsProgrammes;
        this.admissionInstitutions = this._model.admissionInstitutions;
        this.impressionDocuments = this._model.impressionDocuments;
      })
      .add(() => {
        this.loadingIndicator = false;
        this._cd.markForCheck();
      });
    this.loadDataForKitManagement(studentId);
  }

  documents: Array<MetfpetServiceAgent.DocumentViewModel>;
  documentInfo: MetfpetServiceAgent.DocumentInfo;
  optionsBACList: Array<MetfpetServiceAgent.OptionBACRowViewModel>;
  optionsBEPCList: Array<MetfpetServiceAgent.OptionBEPCRowViewModel>;
  optionsTerminaleList: Array<MetfpetServiceAgent.OptionTerminaleRowViewModel>;
  sessionsBACList: Array<any>;
  sessionsBEPCList: Array<any>;
  genreList: Array<MetfpetServiceAgent.Genre>;
  lyceesList: Array<MetfpetServiceAgent.LyceeRowViewModel>;
  collegeList: Array<MetfpetServiceAgent.CollegeRowViewModel>;
  centreExamenList: Array<MetfpetServiceAgent.CentreExamenRowViewModel>;
  inscriptionsProgrammes: Array<MetfpetServiceAgent.InscriptionProgrammeRowViewModel>;
  admissionInstitutions: Array<MetfpetServiceAgent.AdmissionInstitutionRowViewModel>;
  impressionDocuments: Array<MetfpetServiceAgent.ImpressionDocumentViewModel>;
  paiementBourses: Array<MetfpetServiceAgent.PaiementBourseRowViewModel>;
  etudiantBourses: Array<MetfpetServiceAgent.EtudiantBourseRowViewModel>;
  etudiantBourseModifications: Array<MetfpetServiceAgent.EtudiantBourseModificationRowViewModel>;
  touchPointHistories: Array<MetfpetServiceAgent.TouchPointHistoryViewModel>;
  activeBoursesNumber: number;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  currentSessionAdmission: MetfpetServiceAgent.SessionAdmissionDTO;
  currentAnneeAcademique: MetfpetServiceAgent.AnneeAcademiqueDTO;
  bourses: Array<MetfpetServiceAgent.BourseRowViewModel>;
  finalExamenEtudiantProfileViewModel: MetfpetServiceAgent.FinalExamenEtudiantProfileViewModel;
  finalExamenEtudiant: MetfpetServiceAgent.FinalExamenEtudiantViewModel;
  finalExamenEtudiantNotes: Array<MetfpetServiceAgent.FinalExamenEtudiantNoteDTO>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  diplomeList: Array<MetfpetServiceAgent.DiplomeViewModel>;
  concoursEtudiantList: Array<MetfpetServiceAgent.ConcoursEtudiantViewModel>;
  centreList: Array<MetfpetServiceAgent.CentreConcoursRowViewModel>;
  isBrevetSelected: boolean;
  isBacSelected: boolean;
  isTerminaleSelected: boolean;
  isPostSecondaireSelected: boolean;
  isPostPrimaireSelected: boolean;
  isAutreSelected: boolean;
  allowRequestDocument: boolean;
  isShowTheExamenResult: boolean;
  decisionList: Array<any>;
  countryList: Array<any>;
  etudiantNSGBankingInfo: MetfpetServiceAgent.EtudiantNSGBankingInfosDTO;
  profilePhoto: any;
  schoolKitForm: FormGroup;
  schoolKitList: Array<MetfpetServiceAgent.SchoolKitEtudiantDTO>;
  selectedKit: any;
  schoolSupplyKitPageOffset: number = 0;
  schoolSupplyKitTotal: number = 0;
  schoolSupplyKitPageSize: number = 10;
  schoolSupplyKitLoadingIndicator = false;
  schoolSupplyKitList: Array<MetfpetServiceAgent.SchoolSupplyKitViewModel>;
  readonly DEFAULT_QUANTITY: number = 1;
  lastInscription: any;
  observationTypeValues = ObservationTypeValues;
  schoolSupplyEtudiantPagedRows: any;
  @Input()  loadingIndicator: boolean;
  @Input()  isAdmin: boolean;
  @Output() onLoadedStudent = new EventEmitter<MetfpetServiceAgent.EtudiantProfileViewModel>();
  @Output() onSavedStudent = new EventEmitter<MetfpetServiceAgent.EtudiantProfileViewModel>();

  typeDiplomeList = [
    {id : "bac", name : "BAC"},
    {id : "bepc", name : "Brevet"},
    {id : "terminale", name : "Terminale"},
    {id : "postSecondaire", name : "Post-secondaire"},
    {id : "postPrimaire", name : "Post-primaire"},
    {id : "autre", name : "Autre"}
  ];

  constructor(
    private sanitizer: DomSanitizer,
    private _cd: ChangeDetectorRef,
    private _fb: FormBuilder, 
    private _location: Location,
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.isBrevetSelected = false;
    this.isBacSelected = true;
    this.form = this._fb.group({
      'identifiantNationalEleve': null,
      'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'firstName': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'email': [null, Validators.compose([Validators.required, emailValidator])],
      'dateNaissance': [null, Validators.required],
      'genreId': [null, Validators.required],
      'prefectureId': [null],
      'optionBACId': null,
      'optionBEPCId': null,
      'optionTerminaleId': null,
      'sessionBAC': null,
      'sessionBEPC': null,
      'sessionTerminale': null,
      'numeroPV': [null, Validators.compose([Validators.pattern("^[0-9]*$")])],
      'lyceeId': null,
      'collegeId': null,
      'lyceeTerminaleId': null,
      'centreExamenId': [null],    
      'phone': [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]{9}$")])],
      'numeroPhoto': null,
      'lieuNaissance': null,
      'nomPere': null,
      'nomMere': null,
      'nationalite': [null, Validators.required],
      'remarque': null,
      'moyenneBAC': null,
      'moyenneBEPC': null,
      'moyenneTerminale': null,
      'adresse': null,
      'ville': null,
      'pays': null,
      'moyenneGenerale': null,
      'moyenneExamen': null,
      'rang': null,
      'typeDiplomeSelection': ['bac'],
    });

    this.countryList = CountryValues
    this.inscriptionsProgrammes = [];
    this.admissionInstitutions = [];
    this.impressionDocuments = [];
    this.paiementBourses = [];
    this.decisionList = AdmissionDecisionValues;
    
    this._store.pipe(select(selectLoggedInPerson), take(1)).subscribe(
      person =>  this.currentUser = person
    );

    this.schoolKitForm = this._fb.group({
      'schoolSupplies': this._fb.array([])
    });
  }

  private setProfilePhoto() {
    if (this.model) {
      this.profilePhoto = this.model.hasBiometricPicture ? this.sanitizer.bypassSecurityTrustUrl(this.model.photoPath) : this.model.photoPath;
    }
    else
    {
      this.profilePhoto = "/assets/img/users/default-user.jpg";
    }
  }

  allowModifyProfilePhoto() {
    return !this.isExamAgent() && !this.isMinistre() && !this.isDirectionsNationales() && this.model && !this.model.hasBiometricPicture;
  }

  private loadFinalExamenEtudiant(studentId: string) {
    var getFinalExamenEtudiantProfile$: Observable<any>;
    var isUniversitaireUser: boolean;
    if (this._authService.getUserType() === 'superviseurUniversitaire' ||
        this._authService.getUserType() === 'agentUniversitaire' ||
        this._authService.getUserType() === 'administrateur') {
      getFinalExamenEtudiantProfile$ = this._metfpetService.getPublishedFinalExamenEtudiantProfile(studentId);
      isUniversitaireUser = true;
      this.isShowTheExamenResult = false;
    }
    else 
    {
      getFinalExamenEtudiantProfile$ = this._metfpetService.getFinalExamenEtudiantProfile(studentId);
      isUniversitaireUser = false;
      this.isShowTheExamenResult = true;
    }

    getFinalExamenEtudiantProfile$.subscribe(data => {
      this.finalExamenEtudiantProfileViewModel = data;
      if (this.finalExamenEtudiantProfileViewModel) {
        this.finalExamenEtudiant = this.finalExamenEtudiantProfileViewModel.finalExamenEtudiant;
        if (isUniversitaireUser && this.finalExamenEtudiant && 
           (this.finalExamenEtudiant.statusGlobal === <any> FinalExamenStatusGlobal.Admis || this.finalExamenEtudiant.statusGlobal === <any> FinalExamenStatusGlobal.NonAdmis)) 
        {
          this.isShowTheExamenResult = true;
        }
        this.finalExamenEtudiantNotes = this.finalExamenEtudiantProfileViewModel.finalExamenEtudiantNotes;
      }
      this._cd.markForCheck();
    });

    this._metfpetService.getFinalExamenAnneeAcademiqueListForTranscript(studentId).subscribe(data => {
      this.anneeAcademiqueList = data;
      this._cd.markForCheck();
    });
  }

  private loadBourseData(studentId: string) {
    this._metfpetService.getEtudiantPaiementBourseList(studentId).subscribe(data => {
      this.paiementBourses = data;
      this._cd.markForCheck();
    });
    this._metfpetService.getEtudiantBourseList(studentId).subscribe(data => {
      this.etudiantBourses = data;
      if (this.etudiantBourses) {
        this.activeBoursesNumber = this.etudiantBourses.filter(x => x.etudiantBourseStatusName === 'Actif').length;
      }
      this._cd.markForCheck();
    });
    this._metfpetService.getEtudiantBourseModificationList(studentId).subscribe(data => {
      this.etudiantBourseModifications = data;
      this._cd.markForCheck();
    });
    this._metfpetService.getTouchPointHistories(studentId).subscribe(data => {
      this.touchPointHistories = data;
      this._cd.markForCheck();
    });
  }

  private loadEtudiantNSGBankingInfos(studentId: string) {
    this._metfpetService.getEtudiantNSGBankingInfos(studentId).subscribe(etudiantNSGBankingInfo => {
      this.etudiantNSGBankingInfo = etudiantNSGBankingInfo;
      this._cd.markForCheck();
    });
  }

  private loadDiplome(studentId: string) {
    this._metfpetService.getDiplomeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {
        'etudiantId': studentId
       },
    })).subscribe(data => {
      this.diplomeList = data.results;
      this._cd.markForCheck();
    });
  }

  private loadConcoursEtudiant(studentId: string) {
    this._metfpetService.getConcoursEtudiantListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {
          'etudiant': studentId
        }
    })).subscribe(data => {
      this.concoursEtudiantList = data.results;
      this._cd.markForCheck();
    });
  }

  private loadDataForKitManagement(studentId: string) {
    this.schoolSupplyKitLoadingIndicator = true;
    this._metfpetService.getAllKitsOfEtudiant(studentId).subscribe(data => {
      this.schoolKitList = data;
      this.schoolKitList.unshift(MetfpetServiceAgent.SchoolKitEtudiantDTO.fromJS({
        id: Guid.EMPTY,
        kitName: 'Tous les kits',
        schoolKitId: null,
        etudiantId: studentId,
        allowDeleteKit: false
      }));
      this.selectedKit = this.schoolKitList[0];
      this.getSchoolSupplyKitListPage(studentId, -1);
      this._cd.markForCheck();
    });

    this._metfpetService.getInscriptionProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1,
      filters: {
        etudiant: studentId
      } 
    })).subscribe(data => {
      if (data.results.length > 0) {
        const parseDate = (dateStr) => {
          const [day, month, year] = dateStr.split("/").map(Number);
          return new Date(year, month - 1, day);
        };

        this.lastInscription = data.results.reduce((maxObj, currentObj) => {
          return parseDate(currentObj.date) > parseDate(maxObj.date) ? currentObj : maxObj;
        });
      }
      this._cd.markForCheck();
    });
  }

  ngAfterViewInit() {
    this._metfpetService.getAnneeAcademiqueAdmissionOuverte().subscribe((data : any) => {
      if (data) {
        this.currentAnneeAcademique = data;
        this._cd.markForCheck();
      }
    });

    this._metfpetService.getBourseListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.bourses = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getGenreList().subscribe(data => {
      this.genreList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getSessionBACList().subscribe(data => {
      this.sessionsBACList = data.map(x => {return {name: x}});
      this._cd.markForCheck();
    });

    this._metfpetService.getSessionBEPCList().subscribe(data => {
      this.sessionsBEPCList = data.map(x => {return {name: x}});
      this._cd.markForCheck();
    });

    this._metfpetService.getOptionBACListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.optionsBACList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getOptionBEPCListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.optionsBEPCList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getOptionTerminaleListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.optionsTerminaleList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getCentreExamenListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.centreExamenList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getLyceeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.lyceesList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getCollegeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.collegeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getCurrentSessionAdmission().subscribe(data => {
      this.currentSessionAdmission = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getCentreConcoursListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.centreList = data.results;
      this._cd.markForCheck();
    });
  }

  public onTypeDiplomeChange(): void {
    if (this.form.get('typeDiplomeSelection').value == "bepc") {
        this.isBrevetSelected = true;
        this.isBacSelected = false;
        this.isTerminaleSelected = false;
        this.isPostSecondaireSelected = false;
        this.isPostPrimaireSelected = false;
        this.isAutreSelected = false;
    }
    else if (this.form.get('typeDiplomeSelection').value == "bac") {
        this.isBrevetSelected = false;
        this.isBacSelected = true;
        this.isTerminaleSelected = false;
        this.isPostSecondaireSelected = false;
        this.isPostPrimaireSelected = false;
        this.isAutreSelected = false;
    }
    else if (this.form.get('typeDiplomeSelection').value == "terminale") {
        this.isBrevetSelected = false;
        this.isBacSelected = false;
        this.isTerminaleSelected = true;
        this.isPostSecondaireSelected = false;
        this.isPostPrimaireSelected = false;
        this.isAutreSelected = false;
    }
    else if (this.form.get('typeDiplomeSelection').value == "postSecondaire") {
        this.isBrevetSelected = false;
        this.isBacSelected = false;
        this.isTerminaleSelected = false;
        this.isPostSecondaireSelected = true;
        this.isPostPrimaireSelected = false;
        this.isAutreSelected = false;
    }
    else if (this.form.get('typeDiplomeSelection').value == "postPrimaire") {
        this.isBrevetSelected = false;
        this.isBacSelected = false;
        this.isTerminaleSelected = false;
        this.isPostSecondaireSelected = false;
        this.isPostPrimaireSelected = true;
        this.isAutreSelected = false;
    }
    else if (this.form.get('typeDiplomeSelection').value == "autre") {
      this.isBrevetSelected = false;
      this.isBacSelected = false;
      this.isTerminaleSelected = false;
      this.isPostSecondaireSelected = false;
      this.isPostPrimaireSelected = false;
      this.isAutreSelected = true;
  }
  }
  
  public isSessionExists() {
    return this.currentSessionAdmission && this.currentSessionAdmission.id !== Guid.EMPTY && this.currentSessionAdmission.status !== <any>SessionAdmissionStatus.Terminee;
  }

  public addAdmission(): void {
    this._dialogService.openDialog(
      ManageStudentAdmissionDialog,
      {
        width: '620px',
        data: {
          student: this._model,
          currentSessionAdmission: this.currentSessionAdmission,
        }
      }
    ).afterClosed().subscribe(data => {
      if (data) {
        this.admissionInstitutions = [...this.admissionInstitutions, 
          MetfpetServiceAgent.AdmissionInstitutionRowViewModel.fromJS(data)];
        this._cd.markForCheck();
      }
    })
  }

  public updateStudentStatus(): void {
    if(!this.isCreation && this._model && this.isAdmin) {
      this._dialogService.openDialog(
        ManageUpdateStudentStatusDialog,
        {
          width: '620px',
          data: {
            student: this._model,
          }
        }
      ).afterClosed().subscribe(data => {
        if (data) {
          this._cd.markForCheck();
        }
      })
    }
  }

  allowUpdateStatus(){
    return !this.isCreation && this.isAdmin && !this.isExamAgent() && !this.isMinistre() && !this.isDirectionsNationales();
  }

  allowResetPassword() {
    return !this.isCreation && !this.isExamAgent() && !this.isMinistre() && !this.isDirectionsNationales();
  }

  public resetPassword(): void {
    if(!this.isCreation && this._model) {
      this._store.dispatch(showLoading());
      this._metfpetService.resetEtudiantPassword(this._model.id)
      .subscribe(
        success => {
          if (success) {
            this._store.dispatch(showSuccess({message: 'Le mot de passe a été réinitialisé'}));
          } else {
            this._store.dispatch(showError({message: 'Échec de la réinitialisation du mot de passe'}));
          }
        },
        (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public activateOrDeactivateStudent(): void {
    if(!this.isCreation && this._model) {
      this._store.dispatch(showLoading());
      if (this.isActiveStudent()) 
      {
        this._metfpetService.deactivateEtudiant(this._model.id)
        .subscribe(
          success => {
            if (success) {
              this._store.dispatch(showSuccess({message: 'Désactiver l\'apprenant avec succès'}));
              this._model.userStatusName = 'Inactif';
              this._cd.markForCheck();
            } else {
              this._store.dispatch(showError({message: 'Échec de la désactivation de l\'apprenant'}));
            }
          },
          (error) => this._store.dispatch(showError({message: error.response}))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
      else
      {
        this._metfpetService.activateEtudiant(this._model.id)
        .subscribe(
          success => {
            if (success) {
              this._store.dispatch(showSuccess({message: 'Activer l\'apprenant avec succès'}));
              this._model.userStatusName = 'Actif';
              this._cd.markForCheck();
            } else {
              this._store.dispatch(showError({message: 'Échec de l\'activation de l\'apprenant'}));
            }
          },
          (error) => this._store.dispatch(showError({message: error.response}))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    }
  }
  
  isActiveStudent() {
    return !this.isCreation && this._model && this._model.userStatusName === 'Actif';
  }

  getActivateButtonText() {
    return this.isActiveStudent() ? 'Désactiver' : 'Activer';
  }

  allowSynchronizeWithIdentity() {
    return !this.isCreation && this.isAdmin && !this.isExamAgent() && !this.isDirectionsNationales() && this.model && !this._model.identityClientCode && !this.isMinistre();
  }

  public synchronizeWithIdentity(): void {
    if(!this.isCreation && this._model && this.isAdmin && !this._model.identityClientCode) {
      this._store.dispatch(showLoading());
      this._metfpetService.synchronizeEtudiantWithIdentity(this._model.id)
      .subscribe(
        clientCode => {
          if (clientCode) {
            this._model.identityClientCode = clientCode;
            this._cd.markForCheck();
            this._store.dispatch(showSuccess({}));
          } else {
            this._store.dispatch(showError({message: 'Échec de la synchroniser avec identity'}));
          }
        },
        (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  allowSynchronizeWithInserjeune() {
    return !this.isCreation && this.isAdmin && !this.isExamAgent() && !this.isDirectionsNationales() && this.model && this._model.identityClientCode && !this.isMinistre();
  }

  public synchronizeWithInserjeune(): void {
    if(!this.isCreation && this._model && this.isAdmin && this._model.identityClientCode) {
      this._store.dispatch(showLoading());
      this._metfpetService.synchronizeEtudiantWithInserjeune(this._model.id)
      .subscribe(() => {
          this._store.dispatch(showSuccess({}));
        },
        (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public save(): void {
    validateForm(this.form);
    if (this.form.valid) {
      var valid = true;


      if (this.form.get('typeDiplomeSelection').value == "bepc") {
        if(!this.form.get('sessionBEPC').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la session du BEPC'
            }
          });
          valid = false;
        }
      }
      else if (this.form.get('typeDiplomeSelection').value == "bac") {
        if(!this.form.get('sessionBAC').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la session du BAC'
            }
          });
          valid = false;
        }
      }
      else if (this.form.get('typeDiplomeSelection').value == "terminale") {
        if(!this.form.get('sessionTerminale').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la session de terminale'
            }
          });
          valid = false;
        }
      }

      if (valid && this.isCreation && !this.form.get('prefectureId').value) {
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.ERROR, 
            message: 'Veuillez saisir la préfecture de résidence'
          }
        });
        valid = false;
      }

      if (valid) {
        this._model = Object.assign({}, this._model, this.form.value);
        this._store.dispatch(showLoading());
        var saveStudent$: Observable<any>;
  
        if (this.isCreation) {
          var registerEtudiantViewModel = MetfpetServiceAgent.RegisterEtudiantViewModel.fromJS(this.form.value);
          registerEtudiantViewModel.niveauAccesCode = this.form.get('typeDiplomeSelection').value;
          saveStudent$ = this._metfpetService.registerEtudiant(registerEtudiantViewModel);
  
        } else {
          var etudiantProfileViewModel = MetfpetServiceAgent.EtudiantProfileViewModel.fromJS(this._model);
          etudiantProfileViewModel.niveauAccesCode = this.form.get('typeDiplomeSelection').value;
          saveStudent$ = this._metfpetService.updateEtudiantProfile(etudiantProfileViewModel);
        }
  
        saveStudent$
          .subscribe(
            () =>  {
              this._store.dispatch(showSuccess({}));
              this.onSavedStudent.emit(this._model);
            },
            error => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    }
  }

  public addInscription(): void {
    this._dialogService.openDialog(
      ManageStudentInscriptionDialog,
      {
        width: '620px',
        data: {
          student: this._model,
          inscriptionProgrammeId: null,
          institutionId: this.currentUser.institutionId,
        }
      }
    ).afterClosed().subscribe(data => {
      if (data) {
        this.inscriptionsProgrammes = [...this.inscriptionsProgrammes, 
          MetfpetServiceAgent.InscriptionProgrammeRowViewModel.fromJS(data)];
        this.loadBourseData(this._model.id);
        this._cd.markForCheck();
      }
    })
  }

  public editInscription(inscription: MetfpetServiceAgent.InscriptionProgrammeRowViewModel): void {
    this._dialogService.openDialog(
      ManageStudentInscriptionDialog,
      {
        width: '620px',
        data: {
          student: this._model,
          inscriptionProgrammeId: inscription.id,
          institutionId: this.currentUser.institutionId,
        }
      }
    ).afterClosed().subscribe(data => {
      if (data) {
        this.inscriptionsProgrammes = this.inscriptionsProgrammes.map(x => 
          x.id === inscription.id ? MetfpetServiceAgent.InscriptionProgrammeRowViewModel.fromJS(data) : x
        );
        this._cd.markForCheck();
      }
    });
  }

  public removeInscription(inscription: MetfpetServiceAgent.InscriptionProgrammeRowViewModel): void {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention", 
        message: "Voulez-vous définitivement supprimer l’inscription de cet apprenant? Attention cette action est irréversible", 
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());

        this._metfpetService.deleteInscriptionProgramme(inscription.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({message: 'L\'inscription a été supprimée'}));
              var index =  this.inscriptionsProgrammes.findIndex(x => x.id == inscription.id);
              this.inscriptionsProgrammes.splice(index, 1);
              this._cd.markForCheck();
            },
            (error) => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  private findDecisionName(decision: AdmissionDecision): string {
    let decisionObj =  this.decisionList.find(v => v.value === decision);
    if (decisionObj) {
      return decisionObj.name;
    }
    return '';
  }
  
  public allowChangeAdmissionDecision(row: MetfpetServiceAgent.AdmissionInstitutionRowViewModel): boolean {
    return (this.isAdmin || row.surDossier) && !this.isMinistre() && !this.isDirectionsNationales();
  }

  public changeAdmissionDecision(admissionInstitutionRowViewModel: MetfpetServiceAgent.AdmissionInstitutionRowViewModel): void {
    if (admissionInstitutionRowViewModel.decisionName === this.findDecisionName(AdmissionDecision.OffreRefusee) ||
        admissionInstitutionRowViewModel.decisionName === this.findDecisionName(AdmissionDecision.OffreAcceptee)) 
    {
      this._metfpetService.getAttachedDocumentInfo(admissionInstitutionRowViewModel.id).subscribe(
        attachedDocumentInfo => {
          this._dialogService.openDialog(
            AdmissionOfferDecisionAdminDialog,
            {
              width: '900px',
              data: {
                admissionInstitutionRowViewModel: admissionInstitutionRowViewModel,
                attachedDocumentInfo: attachedDocumentInfo
              }
            }
          ).afterClosed().subscribe(data => {
            if (data) {
              this._cd.markForCheck();
            }
          });
        });
    } 
    else 
    {
      this._dialogService.openDialog(
        AdmissionDecisionAdminDialog,
        {
          width: '1000px',
          data: {
            admissionInstitutionRowViewModel: admissionInstitutionRowViewModel,
            isAdmin: this.isAdmin
          }
        }
      ).afterClosed().subscribe(data => {
        if (data) {
          this._cd.markForCheck();
        }
      });
    }
  }
  
  public changeAdmissionStatus(admissionInstitutionRowViewModel: MetfpetServiceAgent.AdmissionInstitutionRowViewModel): void {
    this._dialogService.openDialog(
      ManageUpdateAdmissionDialog,
      {
        width: '620px',
        data: {
          admissionInstitutionRowViewModel: admissionInstitutionRowViewModel
        }
      }
    ).afterClosed().subscribe(data => {
      if (data) {
        admissionInstitutionRowViewModel.statusName = data.statusName;
        admissionInstitutionRowViewModel.decisionName = data.decisionName;
        this._cd.markForCheck();
      }
    });
  }

  goBack() {
    this._location.back();
  }

  getModalite(admissionInstitutionRowViewModel: MetfpetServiceAgent.AdmissionInstitutionRowViewModel) {
    return 'PDF';
  }

  getPV(admissionInstitutionRowViewModel: MetfpetServiceAgent.AdmissionInstitutionRowViewModel)
  {
    return admissionInstitutionRowViewModel.displayedNumeroPV;
  }

  private findStatusName(status: AdmissionInstitutionStatus): string {
    let statusObj =  AdmissionInstitutionStatusValues.find(v => v.value === status);
    if (statusObj) {
      return statusObj.name;
    }
    return '';
  }

  public showModalite(admissionInstitutionRowViewModel: MetfpetServiceAgent.AdmissionInstitutionRowViewModel) {
    if (admissionInstitutionRowViewModel.statusName !== this.findStatusName(AdmissionInstitutionStatus.APayer)) {
      if (admissionInstitutionRowViewModel.centreConcoursName && admissionInstitutionRowViewModel.centreConcoursName !== '') {
        this._dialogService.openInfoDialog({
          width: '600px',
          data: {
            title: 'Candidature enregistrée',
            severity: SeverityEnum.INFO, 
            closeBtnText: 'Terminer',
            message: `
              <p>Votre PV : ${admissionInstitutionRowViewModel.numeroPV}</p> 
              <p>Centre Concours : ${admissionInstitutionRowViewModel.centreConcoursName}</p> 
              <p>Votre inscription et vos choix ont été bien enregistrés. 
              Vous recevrez très prochaienement les modalités relatives aux concours par courriel, SMS et sur la plateforme. </p>
              <p>Vous y receverez aussi vos résultats de même que les offres d’admissions ou les lettres de refus.</p>
            `
          }
        })   
      } else {
        this._dialogService.openInfoDialog({
          width: '600px',
          data: {
            title: 'Candidature enregistrée',
            severity: SeverityEnum.INFO, 
            closeBtnText: 'Terminer',
            message: `
              <p>Votre inscription et vos choix ont été bien enregistrés. 
              Vous recevrez très prochaienement les modalités relatives aux concours par courriel, SMS et sur la plateforme. </p>
              <p>Vous y receverez aussi vos résultats de même que les offres d’admissions ou les lettres de refus.</p>
            `
          }
        })
      }  
    }
  }

  public generateDocument(): void {
    this._dialogService.openDialog(
      SelectImpressionDocumentDialog,
      {
        width: '680px',
        data: {
          documentInfo: this.documentInfo,
          documents: this.documents,
        }
      }
    ).afterClosed().subscribe(result => {
      if (result && result.document && result.impressionDocument) {
        let impressionDocument = result.impressionDocument;
        let document = result.document;
        if (document.requirePayment === <any> RequirePayment.Non) {
          this._dialogService.openDialog(
            ImpressionDocumentPreviewDialog,
            {
              width: '1200px',
              data: {
                documentInfo: this.documentInfo,
                impressionDocumentId: impressionDocument.id,
                diffusion: document.diffusion
              }
            }
          ).afterClosed().subscribe((impressionDocumentDto) => {
            if (impressionDocumentDto && impressionDocumentDto.status !== impressionDocument.status && 
                impressionDocumentDto.status === <any> ImpressionDocumentStatus.Delivree) {
              impressionDocument.status = MetfpetServiceAgent.ImpressionDocumentStatus.Delivree;
              impressionDocument.statusName = 'Délivré'; 
            }
            this.impressionDocuments = [...this.impressionDocuments, 
              MetfpetServiceAgent.ImpressionDocumentViewModel.fromJS(impressionDocument)];
            this._cd.markForCheck();
          });
        } else {
          this._dialogService.openDialog(
            ImpressionDocumentPaymentDialog,
            {
              width: '680px',
              data: {
                documentInfo: this.documentInfo,
                documentName: document.name,
                documentMontant: document.montant,
                impressionDocumentId: impressionDocument.id
              }
            }
          ).afterClosed().subscribe(paymentResult => {
            if (paymentResult) {
              this._dialogService.openDialog(
                ImpressionDocumentConfirmationCodeDialog,
                {
                  width: '680px',
                  data: {
                    documentInfo: this.documentInfo,
                    documentName: document.name,
                    documentMontant: document.montant,
                    paymentId: paymentResult.payment.id
                  }
                }
              ).afterClosed().subscribe(confirmationCodeResult => {
                if (confirmationCodeResult) {
                  impressionDocument.status = MetfpetServiceAgent.ImpressionDocumentStatus.Payee;
                  impressionDocument.statusName = 'Payé';
                  this._dialogService.openDialog(
                    ImpressionDocumentPreviewDialog,
                    {
                      width: '1200px',
                      data: {
                        documentInfo: this.documentInfo,
                        impressionDocumentId: impressionDocument.id,
                        diffusion: document.diffusion
                      }
                    }
                  ).afterClosed().subscribe((impressionDocumentDto) => {
                    if (impressionDocumentDto && impressionDocumentDto.status !== impressionDocument.status && 
                        impressionDocumentDto.status === <any> ImpressionDocumentStatus.Delivree) {
                      impressionDocument.status = MetfpetServiceAgent.ImpressionDocumentStatus.Delivree;
                      impressionDocument.statusName = 'Délivré'; 
                    }
                  });
                }
                this.impressionDocuments = [...this.impressionDocuments, 
                  MetfpetServiceAgent.ImpressionDocumentViewModel.fromJS(impressionDocument)];
                this._cd.markForCheck();
              });
            } else {
              this.impressionDocuments = [...this.impressionDocuments, 
                MetfpetServiceAgent.ImpressionDocumentViewModel.fromJS(impressionDocument)];
              this._cd.markForCheck();
            }
          });
        }
      } 
    });
  }

  public processDocument(row: MetfpetServiceAgent.ImpressionDocumentViewModel): void {
    let documentInfo = MetfpetServiceAgent.DocumentInfo.fromJS({
      etudiantId: row.etudiantId,
      identifiantNationalEleve: row.identifiantNationalEleve,
      etudiantName: row.etudiantName,
      etudiantStatusName: row.etudiantStatusName,
      matricule: row.matricule,
      programmeName: row.programmeName,
      institutionName: row.institutionName,
      anneeAcademiqueName: row.anneeAcademiqueName,
    });
    if (row.status === <any>ImpressionDocumentStatus.APayer) {
      this._dialogService.openDialog(
        ImpressionDocumentPaymentDialog,
        {
          width: '680px',
          data: {
            documentInfo: documentInfo,
            documentName: row.name,
            documentMontant: row.montant,
            impressionDocumentId: row.id
          }
        }
      ).afterClosed().subscribe(paymentResult => {
        if (paymentResult) {
          this._dialogService.openDialog(
            ImpressionDocumentConfirmationCodeDialog,
            {
              width: '680px',
              data: {
                documentInfo: documentInfo,
                documentName: row.name,
                documentMontant: row.montant,
                paymentId: paymentResult.payment.id
              }
            }
          ).afterClosed().subscribe(() => {
            row.status = MetfpetServiceAgent.ImpressionDocumentStatus.Payee;
            row.statusName = 'Payé';
            this._dialogService.openDialog(
              ImpressionDocumentPreviewDialog,
              {
                width: '1300px',
                data: {
                  documentInfo: documentInfo,
                  impressionDocumentId: row.id,
                  diffusion: row.diffusion
                }
              }
            ).afterClosed().subscribe((impressionDocumentDto) => {
              if (impressionDocumentDto && impressionDocumentDto.status !== row.status && 
                  impressionDocumentDto.status === <any> ImpressionDocumentStatus.Delivree) {
                row.status = MetfpetServiceAgent.ImpressionDocumentStatus.Delivree;
                row.statusName = 'Délivré'; 
              }
            });
          });
        }
      });
    } else if (row.status === <any>ImpressionDocumentStatus.Payee || row.status == <any>ImpressionDocumentStatus.Delivree) {
      this._dialogService.openDialog(
        ImpressionDocumentPreviewDialog,
        {
          width: '1300px',
          data: {
            documentInfo: documentInfo,
            impressionDocumentId: row.id,
            diffusion: row.diffusion
          }
        }
      ).afterClosed().subscribe((impressionDocumentDto) => {
        if (impressionDocumentDto && impressionDocumentDto.status !== row.status && 
            impressionDocumentDto.status === <any> ImpressionDocumentStatus.Delivree) {
          row.status = MetfpetServiceAgent.ImpressionDocumentStatus.Delivree;
          row.statusName = 'Délivré'; 
        }
      });
    }
  }

  public openFile(){
    document.getElementById('photoSelector').click();
  }

  public onFileUpload(event) {
    if (event.target.files && event.target.files.length > 0) {
      var size = event.target.files[0].size / 1024;
      if(size < 2048){
        let file : Blob = event.target.files[0];
        var fileReader = new FileReader();
        fileReader.onloadend = function () {
          
        }
        fileReader.readAsBinaryString(file);
        
        let fileParameter = 
        {
          data : file,
          fileName : event.target.fileName
        };
          
        this._metfpetService.uploadEtudiantPhoto(fileParameter, this._model.id)
          .subscribe(
            (result) => {
              this._model.photoPath = result;
              this.profilePhoto = this._model.photoPath;
              this._cd.markForCheck();
              this._store.dispatch(showSuccess({}));
            },
            (error) => this._store.dispatch(showError({message: error.message}))
          );
      }
      else{
        this._dialogService.openInfoDialog({
          width: '600px',
          data: {
            title: 'Fichier trop volumineux',
            severity: SeverityEnum.ERROR, 
            closeBtnText: 'OK',
            message: `
              <p>Le fichier sélectionné est trop volumineux pour être téléversé.</p>
              <p>Veuillez réduire sa taille.</p>
            `
          }
        });
      }
    }
  }

  payEtudiantBourse(row: MetfpetServiceAgent.PaiementBourseRowViewModel) {
    this._dialogService.openDialog(
      CreateBoursesPaymentDialog,
      {
        width: '1000px',
        data: {
          paymentBoursesData: row,
        }
      }
    ).afterClosed().subscribe(() => {
      this.loadBourseData(this.model.id);
    })
  }

  payEtudiantBourseTrimestre() {
    this._dialogService.openDialog(
      CreateBoursesTrimestrePaymentDialog,
      {
        width: '1000px',
        data: {
          curInaNumber: this._model.identifiantNationalEleve
        }
      }
    ).afterClosed().subscribe(() => {
      this.loadBourseData(this.model.id);
    })
  }

  manageBourses() 
  {
    this._dialogService.openDialog(
      ManageStudentBourseDialog,
      {
        width: '1000px',
        data: {
          studentId: this.model.id,
          etudiantBourses: this.etudiantBourses,
          etudiantBourseModifications: this.etudiantBourseModifications
        }
      }
    ).afterClosed().subscribe(() => {
      this.loadBourseData(this.model.id);
    })
  }

  generateBourseManual() {
    this._dialogService.openDialog(
      GenerateStudentBourseManualDialog,
      {
        width: '1000px',
        data: {
          etudiantId: this.model.id,
          bourses: this.bourses,
          anneeAcademiqueId: this.currentAnneeAcademique.id
        }
      }
    ).afterClosed().subscribe(() => {
      this.loadBourseData(this.model.id);
    })
  }

  public isAdminUser() {
    return this._authService.getUserType() == 'administrateurMinisteriel' || this._authService.getUserType() == 'administrateur'; 
  }

  public isMinisterielUser() {
    return this._authService.getUserType() == 'administrateurMinisteriel'
    || this._authService.getUserType() == 'superviseurMinisteriel'
    || this._authService.getUserType() == 'agentMinisteriel'
    || this._authService.getUserType() == 'administrateur';
  }

  public isAdminAndSuperviseurMinisterielUser() {
    return this._authService.getUserType() == 'administrateurMinisteriel'
    || this._authService.getUserType() == 'administrateur'
    || this._authService.getUserType() == 'superviseurMinisteriel';
  }

  public isUniversitaireUser() {
    return this._authService.getUserType() == 'superviseurUniversitaire'
    || this._authService.getUserType() == 'agentUniversitaire';
  }

  public isAllowedExam() {
    return this._authService.getUserType() == 'administrateurMinisteriel'
    || this._authService.getUserType() == 'administrateur'
    || this._authService.getUserType() == 'superviseurUniversitaire'
    || this.isExamRole()
    || this.isMinistre();
  }

  public isExamRole() {
    return this._authService.getUserType() == 'examSupervisorMinisteriel'
        || this._authService.getUserType() == 'examAgentMinisteriel';
  }

  public isExamAgent() {
    return this._authService.getUserType() == 'examAgentMinisteriel';
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }

  public isReadOnly() {
    return this._authService.getUserType() !== 'administrateurMinisteriel' &&
           this._authService.getUserType() !== 'administrateur';
  }

  public isDiplomeInformationsReadOnly() {
    return !this.allowToModifyDiplomeInformations();
  }

  public allowToModifyDiplomeInformations() {
    return this._authService.getUserType() == 'administrateurMinisteriel' &&
           (this._authService.getIdentifiant() == 'metfpet' ||
            this._authService.getIdentifiant() == 'hdiallo' ||
            this._authService.getIdentifiant() == 'FRCA0001' ||
            this._authService.getIdentifiant() == 'VITR0001' ||
            this._authService.getIdentifiant() == 'ALDI0001' ||
            this._authService.getIdentifiant() == 'Jkourouma' ||
            this._authService.getIdentifiant() == 'MOSACAMARA' ||
            this._authService.getIdentifiant() == 'THMODIABY' ||
            this._authService.getIdentifiant() == 'ILICISSE' ||
            this._authService.getIdentifiant() == 'mfCamara' ||
            this._authService.getIdentifiant() == 'SCONDE' ||
            this._authService.getIdentifiant() == 'FIDEL' ||
            this._authService.getIdentifiant() == 'bouba');
  }

  public allowToShowTheBourseHistory() {
    return this._authService.getUserType() == 'administrateurMinisteriel' &&
           (this._authService.getIdentifiant() == 'FRCA0001' ||
            this._authService.getIdentifiant() == 'VITR0001');
  }

  public isDirectionsNationales() {
    return this._authService.getUserType() == 'directionsNationales';
  }

  public isAdministrateur() {
    return this._authService.getUserType() === 'administrateur';
  }
  
  public allowToShowKitManagement() {
    return (this.isAdmin || this._authService.getUserType() == 'superviseurUniversitaire') && !this.isExamRole();
  }

  allowDownloadTranscript() {
    return !this.isMinistre() && !this.isDirectionsNationales() && !this.isUniversitaireUser() &&
      (!this.isAdministrateur() || (this.isAdministrateur() && this.finalExamenEtudiant && this.finalExamenEtudiant.isPublished && this.finalExamenEtudiant.transcriptUrl));
  }

  downloadTranscript() {
    if (this.isUniversitaireUser() || this.isAdministrateur()) {
      if (this.finalExamenEtudiant && this.finalExamenEtudiant.isPublished && this.finalExamenEtudiant.transcriptUrl)
        {
          window.open(this.finalExamenEtudiant.transcriptUrl, "_blank");
        }
    }
    else
    {
      this._dialogService.openDialog(
        TelechargerLeReleveDeNotesDialog,
        {
          width: '650px',
          data: {
            etudiantId: this.model.id,
            anneeAcademiqueList: this.anneeAcademiqueList,
          }
        }
      );
    }
  }

  actualizeDiplome(row: MetfpetServiceAgent.DiplomeViewModel) {
    this._store.dispatch(showLoading());
    this._metfpetService.actualizeDiplome(row.id)
      .subscribe(
        () => {
          this._store.dispatch(showSuccess({}));
          this.loadDiplome(this.model.id);
        },
        (error) => this._store.dispatch(showError({message: error.response}))
    )
    .add(() => this._store.dispatch(hideLoading()));
  }

  processDiplome(row: MetfpetServiceAgent.DiplomeViewModel) {
    if (row.status === <any>DiplomeStatus.Inscrit) {
      this._dialogService.openDialog(
        AdmitGraduationDialog,
        {
          width: '400px',
          data: {
            diplomeId: row.id,
          }
        }
      ).afterClosed().subscribe(() => {
        this.loadDiplome(this.model.id);
      })
    }
    else if (row.status === <any>DiplomeStatus.AdmissibleDiplomation || row.status === <any>DiplomeStatus.Diplome) {
      this._dialogService.openConfirmDialog({
        width: '400px',
        data: {
          title: "Confirmation d’action", 
          message: "Veuillez confirmer l’action : " + row.actionName, 
          confirmBtnText: 'Confirmer'
        }
      }).afterClosed().subscribe(confirmed => {
        if (confirmed) {
          if (row.status === <any>DiplomeStatus.AdmissibleDiplomation) 
          {
            row.status = <any>DiplomeStatus.Diplome;
            this._store.dispatch(showLoading());
            this._metfpetService.updateDiplome(row)
              .subscribe(
                () => {
                  this._store.dispatch(showSuccess({}));
                  this.loadDiplome(this.model.id);
                },
                (error) => this._store.dispatch(showError({message: error.response}))
            )
            .add(() => this._store.dispatch(hideLoading()));
          }
          else
          {
            this._store.dispatch(showLoading());
            var diplomeIds = [];
            diplomeIds.push(row.id);
            this._metfpetService.generateDiplomas(diplomeIds)
              .subscribe(
                () => {
                  this._store.dispatch(showSuccess({}));
                  this.loadDiplome(this.model.id);
                },
                (error) => this._store.dispatch(showError({message: error.response}))
            )
            .add(() => this._store.dispatch(hideLoading()));
          }
        }
      });
    }
    else if (row.status === <any>DiplomeStatus.DiplomeGenere)
    {
      window.open(row.diplomaUrl, "_blank");
    }
  }

  updateAccount() {
    this._dialogService.openDialog(
      StudentAccountBankingComponent,
      {
        width: '620px',
        data: {
          etudiantId: this._model.id,
          etudiantNSGBankingInfo: this.etudiantNSGBankingInfo
        }
      }
    ).afterClosed().subscribe(result => {
      if(result){
        this.loadEtudiantNSGBankingInfos(this._model.id);
      }
    });
  }

  openStudentConcoursDetail(row, isCompetitionResult) {
    this._dialogService.openDialog(
      StudentConcoursDetailDialog,
      {
        width: '650px',
        data: {
          id: row.id,
          isCompetitionResult: isCompetitionResult,
          centreList: this.centreList,
          isReadOnly: row.isPublished || this.isDiplomeInformationsReadOnly(),
          isAdmin: true,
          isUniversitaireUser: this.isUniversitaireUser(),
          isMinistre: this.isMinistre(),
        }
      }
    ).afterClosed().subscribe(() => {
      this.loadConcoursEtudiant(this.model.id);
    })
  }

  allowOverWritingBiometry(): boolean {
    return this._model?.biometricStatus === "Biométrisé" && this._authService.getUserType() == 'administrateurMinisteriel' &&
      (this._authService.getIdentifiant() == 'metfpet' ||
      this._authService.getIdentifiant() == 'FRCA0001' ||
      this._authService.getIdentifiant() == 'VITR0001' ||
      this._authService.getIdentifiant() == 'ALDI0001' ||
      this._authService.getIdentifiant() == 'ABYA0001' ||
      this._authService.getIdentifiant() == 'Jkourouma' ||
      this._authService.getIdentifiant() == 'SCONDE' ||
      this._authService.getIdentifiant() == 'MOSACAMARA' ||
      this._authService.getIdentifiant() == 'THMODIABY');
  }
  
  overWriteBiometry() {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention",
        message: "Voulez-vous définitivement supprimer les données biométriques de cet apprenant ?",
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe((confirmed) => {
      if (confirmed) {
        if (this._model?.biometricStatus === "Biométrisé") {
          this._store.dispatch(showLoading());
          this._metfpetService.overWriteStudentBiometry(MetfpetServiceAgent.OverWriteBiometryDTO.fromJS({ identifiantNationalEleve: this._model?.identifiantNationalEleve }))
            .subscribe({
              next: (result) => {
                if (result.message !== "Error") {
                  this._store.dispatch(showSuccess({}));
                  this._store.dispatch(hideLoading());
                } else {
                  this._store.dispatch(showError({ message: 'Erreur de suppression' }));
                  this._store.dispatch(hideLoading());
                }
              },
              error: (error) => {
                this._store.dispatch(showError({ message: 'Erreur d\'écrasement de données biométriques' }));
              }
            })
        }
      }
    })
  }

  getVerifiedPhoneNumber(studentId: string){
    this._metfpetService.getStudentVerifiedPhoneNumber(studentId).subscribe((data)=>{
      this.studenVerifiedPhoneNumber = data?.phoneNumber ? data?.phoneNumber : "Numéro non vérifié";
      this._cd.markForCheck();
    })
  }

  addSchoolKit(): void {
    if (!this._model) return;
    this._dialogService.openDialog(
      StudentSchoolKitDialog, {
        width: '550px',
        data: {
          etudiantId: this._model.id,
        }
      }
    ).afterClosed().subscribe(data => {
      if (data) {
        this.loadDataForKitManagement(this._model.id);
      }
    })
  }

  get schoolSupplies() {
    return this.schoolKitForm.controls['schoolSupplies'] as FormArray;
  }

  onKitMenuClick(item: any): void {
    this.selectedKit = item;
    this.schoolSupplyKitPageOffset = 0;
    this.getSchoolSupplyKitListPage(this.model.id, -1);
  }

  getSchoolSupplyKitListPage(studentId, pageIndex) {
    if (!studentId) return;
    this.schoolSupplyKitLoadingIndicator = true;
    this._metfpetService.getSchoolSupplyKitListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: pageIndex,
      pageSize: this.schoolSupplyKitPageSize,
      filters: {
        schoolKitId: this.selectedKit?.schoolKitId,
        etudiantId: studentId,
        hasAllStudentKit: !this.selectedKit?.schoolKitId
      }
    })).subscribe(data => {
      this.schoolSupplyKitList = data.results;
      this.schoolSupplyKitTotal = data.totalCount;
      this.showSchoolSupplies(data.results);
      this.schoolSupplyEtudiantPagedRows = this.schoolSupplies.controls.slice(this.schoolSupplyKitPageOffset * this.schoolSupplyKitPageSize, (this.schoolSupplyKitPageOffset + 1) * this.schoolSupplyKitPageSize);
    })
    .add(() => {
      this.schoolSupplyKitLoadingIndicator = false;
      this._cd.markForCheck();
    });
  }

  showSchoolSupplies(data: any) {
    this.schoolSupplies.clear();
    data.forEach(item => {
      this.schoolSupplies.push(this._fb.group({
        id: item.id,
        name: item.name,
        etudiantId: item.etudiantId,
        observationType: item.observationType,
        originalObservationType: item.observationType,
        schoolSupplyId: item.schoolSupplyId,
        schoolKitId: item.schoolKitId,
        schoolSupplyKitId: item.id,
        hasReception: !!item.hasReception,
        originalHasReception: !!item.hasReception,
        dateOfFirstReception: item.dateOfFirstReception,
        originalDateOfFirstReception: item.dateOfFirstReception,
        hasDefaultDate: item.dateOfFirstReception == null,
        quantity: item.quantity,
        originalQuantity: item.quantity,
        comment: item.comment,
        originalComment: item.comment,
        hasReceiptForm: item.hasReceiptForm
      }));
    });
    this.schoolSupplies.controls = [...this.schoolSupplies.controls];
  }

  onChangePageSchoolSupplyKitTable(event) {
    this.schoolSupplyKitPageOffset = event.offset;
    this.schoolSupplyEtudiantPagedRows = this.schoolSupplies.controls.slice(this.schoolSupplyKitPageOffset * this.schoolSupplyKitPageSize, (this.schoolSupplyKitPageOffset + 1) * this.schoolSupplyKitPageSize);
  }

  onChangePageMaterial(event) {
    this.schoolSupplyKitPageOffset = event.pageIndex;
    this.schoolSupplyEtudiantPagedRows = this.schoolSupplies.controls.slice(this.schoolSupplyKitPageOffset * this.schoolSupplyKitPageSize, (this.schoolSupplyKitPageOffset + 1) * this.schoolSupplyKitPageSize);
  }

  decreaseSupplyQuantity(index) {
    const currentValue = this.schoolSupplies.at(index).get('quantity')?.value || this.DEFAULT_QUANTITY;
    if (currentValue < this.DEFAULT_QUANTITY) return;
    this.schoolSupplies.at(index).get('quantity').setValue(currentValue - 1);
  }

  increaseSupplyQuantity(index) {
    const currentValue = this.schoolSupplies.at(index).get('quantity')?.value || this.DEFAULT_QUANTITY;
    this.schoolSupplies.at(index).get('quantity').setValue(currentValue + 1);
  }

  addReason(index) {
    let hasReceiptForm = this.schoolSupplies.at(index).get('hasReceiptForm').value;
    let schoolSupplyKitId = this.schoolSupplies.at(index).get('id').value;
    let newQuantity = this.schoolSupplies.at(index).get('quantity').value;
    let observationType = this.schoolSupplies.at(index).get('observationType').value;
    let hasReception = this.schoolSupplies.at(index).get('hasReception').value;
    let dateOfFirstReception = this.schoolSupplies.at(index).get('dateOfFirstReception').value;
    let comment = this.schoolSupplies.at(index).get('comment').value;
    let etudiantId = this.schoolSupplies.at(index).get('etudiantId').value;

    let schoolSupplyKit = this.schoolSupplyKitList.find(x => x.id === schoolSupplyKitId);
    if (etudiantId && schoolSupplyKit && newQuantity < schoolSupplyKit.quantity) {
      var dialogRef = this._dialogService.openDialog(QuantityDecreaseReasonDialog, {
        width: '700px',
        data: {}
      })
      .afterClosed().subscribe(data => {
        if (data) {
          this.updateSupplyEtudiant(MetfpetServiceAgent.SchoolSupplyEtudiantDTO.fromJS({
            etudiantId: this._model.id,
            observationType: observationType,
            schoolSupplyKitId: schoolSupplyKitId,
            hasReception: hasReception,
            dateOfFirstReception: dateOfFirstReception,
            quantity: newQuantity,
            comment: comment,
            hasReceiptForm: hasReceiptForm,
            reason: MetfpetServiceAgent.SchoolSupplyEtudiantReasonDTO.fromJS({
              reasonId: data.reasonId,
              comment: data.comment,
            })
          }));
        }
      });
    } else {
      this.updateSupplyEtudiant(MetfpetServiceAgent.SchoolSupplyEtudiantDTO.fromJS({
        etudiantId: this._model.id,
        observationType: observationType,
        schoolSupplyKitId: schoolSupplyKitId,
        hasReception: hasReception,
        dateOfFirstReception: dateOfFirstReception,
        quantity: newQuantity,
        comment: comment,
        hasReceiptForm: hasReceiptForm,
        reason: null
      }));
    }
  }

  updateSupplyEtudiant(data) {
    this._store.dispatch(showLoading());
    this._metfpetService.createOrUpdateSchoolSupplyEtudiant(data)
      .subscribe(
        (data) => {
          if (data) {
            let schoolSupplyKit = this.schoolSupplyKitList.find(x => x.id === data.schoolSupplyKitId);
            schoolSupplyKit.quantity = data.quantity;
          }
          this._store.dispatch(showSuccess({}));
        },
        (error) => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
  }

  generateReceiptFormKit() {
    if (!this.model) return;
    var dialogRef = this._dialogService.openDialog(GenerateStudentReceiptFormDialog, {
      width: '900px',
      data: {
        etudiantId: this.model.id
      }
    })
    .afterClosed().subscribe((saveSuccess) => {});
  }

  openHistory() {
    if (!this.model) return;
    var dialogRef = this._dialogService.openDialog(StudentKitHistoryDialog, {
      width: '900px',
      data: {
        etudiantId: this.model.id
      }
    })
    .afterClosed().subscribe((saveSuccess) => {});
  }

  onHasReceptionChange(index) {
    const currentValue = this.schoolSupplies.at(index).get('hasReception')?.value;
    const hasDefaultDate = this.schoolSupplies.at(index).get('hasDefaultDate').value;
    if (hasDefaultDate) {
      this.schoolSupplies.at(index).get('dateOfFirstReception').setValue(currentValue ? new Date() : null);
    }
  }

  onCheckboxAllChange(event) {
    this.schoolSupplies.controls.forEach(group => {
      group.get('hasReception')?.setValue(event.checked);
      let hasDefaultDate = group.get('hasDefaultDate').value;
      if (hasDefaultDate) {
        group.get('dateOfFirstReception').setValue(event.checked ? new Date() : null);
      }
    });
  }

  isCheckedAll() {
    return this.schoolSupplies?.length > 0 && this.schoolSupplies?.controls.every(group => group.get('hasReception').value === true);
  }

  saveAll() {
    if (!this.model) return;
    const validRows = this.schoolSupplies.controls.filter(group => {
      return (group.get('hasReception').value !== group.get('originalHasReception').value) ||
              (group.get('dateOfFirstReception').value !== group.get('originalDateOfFirstReception').value) ||
              (!group.get('etudiantId').value && (group.get('quantity').value !== group.get('originalQuantity').value)) ||
              (group.get('observationType').value !== group.get('originalObservationType').value) ||
              (group.get('comment').value !== group.get('originalComment').value)
    });
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention",
        message: `Voulez-vous mettre à jour ${validRows.length} lignes ?`,
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        const chunks = this.chunkData(validRows, 10);
        forkJoin(
          chunks.map(chunk => this._metfpetService.createOrUpdateEtudiantAllSchoolSupplies(chunk))
        ).pipe(finalize(() => this._store.dispatch(hideLoading()))
        ).subscribe({
          next: () => {},
          error: (error) => {
            this._store.dispatch(showException({error: error}));
          },
          complete: () => {
            this._store.dispatch(showSuccess({}));
            this.getSchoolSupplyKitListPage(this.model.id, -1);
          }
        });
      }
    });
  }

  chunkData(data, chunkSize: number) {
    const chunks = [];
    for (let i = 0; i < data.length; i += chunkSize) {
      const chunkValues = data.slice(i, i + chunkSize).map(item => Object.assign({
        ...item.value,
        etudiantId: this.model.id
      }));
      chunks.push(chunkValues);
    }
    return chunks;
  }

  deleteKit(selectedItem) {
    if (!this.model) return;
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention",
        message: 'Voulez-vous supprimer ce kit scolaire ?',
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteSchoolKitOfEtudiant(selectedItem.id)
          .subscribe(() => {
            this.loadDataForKitManagement(this.model.id);
            this._store.dispatch(showSuccess({}));
          },
            (error) => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }
}