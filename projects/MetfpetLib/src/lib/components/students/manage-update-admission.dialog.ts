import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { MetfpetServiceAgent } from '../../services';
import { validateForm } from '../../utils';
import { DialogService, SeverityEnum } from '../dialogs';
import { showSuccess, showError, showLoading, hideLoading, showException } from '../../store/actions';
import { AdmissionInstitutionOptionValues } from '../../interfaces';

@Component({
  templateUrl: './manage-update-admission.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageUpdateAdmissionDialog {

  form: FormGroup;
  admissionInstitutionRowViewModel: MetfpetServiceAgent.AdmissionInstitutionRowViewModel;
  optionList = AdmissionInstitutionOptionValues;

  constructor(
    public dialogRef: MatDialogRef<ManageUpdateAdmissionDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.admissionInstitutionRowViewModel = data.admissionInstitutionRowViewModel;
    this.form = this._formBuilder.group({
      'id': this.admissionInstitutionRowViewModel.id,
      'option': [null, Validators.required],
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public onConfirm(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
  
      this._metfpetService.updateAdmissionInstitutionForAdmin(
        MetfpetServiceAgent.UpdateAdmissionInstitution.fromJS(this.form.getRawValue()))
        .subscribe(
          (data : any) => {
            this._store.dispatch(showSuccess({}));
            this.dialogRef.close(data);
          },
          (error) => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }
}