import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AuthService, MetfpetServiceAgent } from '../../services';
import { validateForm, specicalCharacterValidator } from '../../utils';
import { showSuccess, showError, showLoading, hideLoading, showException } from '../../store/actions';
import { InscriptionProgrammeStatus, InscriptionProgrammeStatusValues, InscriptionProgrammeStatusWithoutAPayerValues, Regime, RegimeValues } from '../../interfaces';

@Component({
  templateUrl: './manage-student-inscription.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageStudentInscriptionDialog {

  form: FormGroup;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  institutionId: string;
  inscriptionProgrammeId: string;
  inscriptionProgramme: MetfpetServiceAgent.InscriptionProgrammeDTO;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  facultyList: Array<MetfpetServiceAgent.FaculteRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  typeFormationList: Array<MetfpetServiceAgent.TypeFormation>;
  statusList = InscriptionProgrammeStatusValues;
  regimeList = RegimeValues;

  constructor(
    public dialogRef: MatDialogRef<ManageStudentInscriptionDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.student = data.student;
    this.institutionId = data.institutionId;
    this.inscriptionProgrammeId = data.inscriptionProgrammeId;
    this.inscriptionProgramme = new MetfpetServiceAgent.InscriptionProgrammeDTO();
    this.form = this._formBuilder.group({
      'id': null,
      'etudiantId': this.student.id,
      'matricule': [null],
      'anneeAcademiqueId': [null, Validators.required],
      'institutionId': [null, Validators.required],
      'faculteId': [null, Validators.required],
      'programmeId': [null, Validators.required],
      'niveauEtudeId': [null, Validators.required],
      'typeFormationId': [null, Validators.required],
      'moyenne': [null],
      'status': [null, Validators.required],
      'regime': [<any> Regime.Externat, Validators.required],
    });

    this.form.get('institutionId').valueChanges.pipe(
      tap(institutionId => {
        this.programList = [];
        this.facultyList = [];
        this.form.get('faculteId').setValue(null);
        this.form.get('programmeId').setValue(null);
        if (!institutionId) {
          return;
        }
        this._loadFaculties(institutionId);
      })
    ).subscribe();

    this.form.get('faculteId').valueChanges.pipe(
      tap(faculteId => {
        this.programList = [];
        this.form.get('programmeId').setValue(null);
        let institutionId = this.form.get('institutionId').value;
        if (!institutionId || !faculteId) {
          return;
        }
        this._loadPrograms(faculteId);
      })
    ).subscribe();
  }

  ngOnInit() {
    if (this.inscriptionProgrammeId) {
      this._metfpetService.getInscriptionProgramme(this.inscriptionProgrammeId).subscribe(
        inscriptionProgramme => {
          this.inscriptionProgramme = inscriptionProgramme;
          this.form.patchValue({
            'id': this.inscriptionProgramme.id,
            'matricule': this.inscriptionProgramme.matricule,
            'anneeAcademiqueId': this.inscriptionProgramme.anneeAcademiqueId,
            'institutionId': this.inscriptionProgramme.institutionId,
            'faculteId': this.inscriptionProgramme.faculteId,
            'programmeId': this.inscriptionProgramme.programmeId,
            'niveauEtudeId': this.inscriptionProgramme.niveauEtudeId,
            'typeFormationId': this.inscriptionProgramme.typeFormationId,
            'moyenne': this.inscriptionProgramme.moyenne,
            'status': this.inscriptionProgramme.status,
            'regime': this.inscriptionProgramme.regime,
          });

          if (this.isUniversitaireUser() && !this.isAPayerStatut())
          {
            this.statusList = InscriptionProgrammeStatusWithoutAPayerValues;
          }
          this._cd.markForCheck();
        }
      );
    } else {
      this.form.patchValue({
        'institutionId': this.institutionId,
      });
    }
  }

  ngAfterViewInit() {
    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getTypeFormationList().subscribe(data => {
      this.typeFormationList = data;
      this._cd.markForCheck();
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public onConfirm(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());

      if (!this.inscriptionProgrammeId) {
        this._metfpetService.createInscriptionProgramme(
          MetfpetServiceAgent.CreateInscriptionProgramme.fromJS(this.form.getRawValue()))
          .subscribe(
            (data : any) => {
              this._store.dispatch(showSuccess({}));
              this.dialogRef.close(data);
            },
            (error) => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));

      } else {
        this._metfpetService.updateInscriptionProgramme(
          MetfpetServiceAgent.InscriptionProgrammeDTO.fromJS(Object.assign({}, this.inscriptionProgramme, this.form.getRawValue())))
          .subscribe(
            (data : any) => {
              this._store.dispatch(showSuccess({}));
              this.dialogRef.close(data);
            },
            (error) => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    }
  }
  
  private _loadFaculties(institutionId: string) {
    this._metfpetService.getFaculteListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
         pageIndex: -1, filters: {institution: institutionId}
      })).subscribe(data => {
        this.facultyList = data.results;
        this._cd.markForCheck();
      });
  }

  private _loadPrograms(faculteId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
         pageIndex: -1, filters: {faculte: faculteId}
      })).subscribe(data => {
        this.programList = data.results;
        this._cd.markForCheck();
      });
  }

  blockEditStatut(): boolean {
    return this.inscriptionProgrammeId && !this.allowToModifyStatus();
  }
  
  isAPayerStatut() {
    return this.inscriptionProgramme && this.inscriptionProgramme.status == <any>InscriptionProgrammeStatus.APayer;
  }

  isUniversitaireUser() {
    return this._authService.getUserType() == 'superviseurUniversitaire'
    || this._authService.getUserType() == 'agentUniversitaire';
  }

  public allowToModifyStatus() {
    return (this._authService.getUserType() == 'administrateurMinisteriel' &&
           (this._authService.getIdentifiant() == 'metfpet' ||
            this._authService.getIdentifiant() == 'FRCA0001' ||
            this._authService.getIdentifiant() == 'VITR0001' ||
            this._authService.getIdentifiant() == 'ALDI0001' ||
            this._authService.getIdentifiant() == 'Jkourouma' ||
            this._authService.getIdentifiant() == 'MOSACAMARA' ||
            this._authService.getIdentifiant() == 'THMODIABY')) || this.isUniversitaireUser();
  }
}