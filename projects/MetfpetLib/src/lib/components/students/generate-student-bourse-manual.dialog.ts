import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { MetfpetServiceAgent } from '../../services';
import { validateForm } from '../../utils';
import { DialogService, SeverityEnum } from '../dialogs';
import { showSuccess, showError, showLoading, hideLoading, showException } from '../../store/actions';
import { PaiementBourseStatusValues, StudentStatusValues } from '../../interfaces';

@Component({
  templateUrl: './generate-student-bourse-manual.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GenerateStudentBourseManualDialog {

  form: FormGroup;
  statusList = PaiementBourseStatusValues;
  model: MetfpetServiceAgent.EtudiantBourseManualGenerationDTO;
  bourses: Array<MetfpetServiceAgent.BourseRowViewModel>;

  constructor(
    public dialogRef: MatDialogRef<GenerateStudentBourseManualDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.bourses = data.bourses;
    this.form = this._formBuilder.group({
      etudiantId: data.etudiantId,
      anneeAcademiqueId: data.anneeAcademiqueId,
      bourseId: [null, Validators.required],
      isJanvier: false,
      isFevrier: false,
      isMars: false,
      isAvril: false,
      isMai: false,
      isJuin: false,
      isJuillet: false,
      isAout: false,
      isSeptembre: false,
      isOctobre: false,
      isNovembre: false,
      isDecembre: false,
      janvierStatus: null,
      fevrierStatus: null,
      marsStatus: null,
      avrilStatus: null,
      maiStatus: null,
      juinStatus: null,
      juilletStatus: null,
      aoutStatus: null,
      septembreStatus: null,
      octobreStatus: null,
      novembreStatus: null,
      decembreStatus: null
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public onConfirm(): void {
    validateForm(this.form);
    if (this.form.valid) {
      var valid = true;
      let infos = [];
      if (this.form.get('isJanvier').value) {
        let janvier = new MetfpetServiceAgent.PaiementBourseInfo();
        janvier.mois = MetfpetServiceAgent.MoisAnnee.Janvier;
        if(!this.form.get('janvierStatus').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la janvier statut'
            }
          });
          valid = false;
        } else {
          janvier.status = this.form.get('janvierStatus').value;
        }
        infos.push(janvier);
      }

      if (this.form.get('isFevrier').value) {
        let fevrier = new MetfpetServiceAgent.PaiementBourseInfo();
        fevrier.mois = MetfpetServiceAgent.MoisAnnee.Fevrier;
        if(!this.form.get('fevrierStatus').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la février statut'
            }
          });
          valid = false;
        } else {
          fevrier.status = this.form.get('fevrierStatus').value;
        }
        infos.push(fevrier);
      }

      if (this.form.get('isMars').value) {
        let mars = new MetfpetServiceAgent.PaiementBourseInfo();
        mars.mois = MetfpetServiceAgent.MoisAnnee.Mars;
        if(!this.form.get('marsStatus').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la mars statut'
            }
          });
          valid = false;
        } else {
          mars.status = this.form.get('marsStatus').value;
        }
        infos.push(mars);
      }

      if (this.form.get('isAvril').value) {
        let avril = new MetfpetServiceAgent.PaiementBourseInfo();
        avril.mois = MetfpetServiceAgent.MoisAnnee.Avril;
        if(!this.form.get('avrilStatus').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la avril statut'
            }
          });
          valid = false;
        } else {
          avril.status = this.form.get('avrilStatus').value;
        }
        infos.push(avril);
      }

      if (this.form.get('isMai').value) {
        let mai = new MetfpetServiceAgent.PaiementBourseInfo();
        mai.mois = MetfpetServiceAgent.MoisAnnee.Mai;
        if(!this.form.get('maiStatus').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la mai statut'
            }
          });
          valid = false;
        } else {
          mai.status = this.form.get('maiStatus').value;
        }
        infos.push(mai);
      }

      if (this.form.get('isJuin').value) {
        let juin = new MetfpetServiceAgent.PaiementBourseInfo();
        juin.mois = MetfpetServiceAgent.MoisAnnee.Juin;
        if(!this.form.get('juinStatus').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la juin statut'
            }
          });
          valid = false;
        } else {
          juin.status = this.form.get('juinStatus').value;
        }
        infos.push(juin);
      }

      if (this.form.get('isJuillet').value) {
        let juillet = new MetfpetServiceAgent.PaiementBourseInfo();
        juillet.mois = MetfpetServiceAgent.MoisAnnee.Juillet;
        if(!this.form.get('juilletStatus').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la juillet statut'
            }
          });
          valid = false;
        } else {
          juillet.status = this.form.get('juilletStatus').value;
        }
        infos.push(juillet);
      }

      if (this.form.get('isAout').value) {
        let aout = new MetfpetServiceAgent.PaiementBourseInfo();
        aout.mois = MetfpetServiceAgent.MoisAnnee.Aout;
        if(!this.form.get('aoutStatus').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la aout statut'
            }
          });
          valid = false;
        } else {
          aout.status = this.form.get('aoutStatus').value;
        }
        infos.push(aout);
      }

      if (this.form.get('isSeptembre').value) {
        let septembre = new MetfpetServiceAgent.PaiementBourseInfo();
        septembre.mois = MetfpetServiceAgent.MoisAnnee.Septembre;
        if(!this.form.get('septembreStatus').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la septembre statut'
            }
          });
          valid = false;
        } else {
          septembre.status = this.form.get('septembreStatus').value;
        }
        infos.push(septembre);
      }

      if (this.form.get('isOctobre').value) {
        let octobre = new MetfpetServiceAgent.PaiementBourseInfo();
        octobre.mois = MetfpetServiceAgent.MoisAnnee.Octobre;
        if(!this.form.get('octobreStatus').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la octobre statut'
            }
          });
          valid = false;
        } else {
          octobre.status = this.form.get('octobreStatus').value;
        }
        infos.push(octobre);
      }

      if (this.form.get('isNovembre').value) {
        let novembre = new MetfpetServiceAgent.PaiementBourseInfo();
        novembre.mois = MetfpetServiceAgent.MoisAnnee.Novembre;
        if(!this.form.get('novembreStatus').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la novembre statut'
            }
          });
          valid = false;
        } else {
          novembre.status = this.form.get('novembreStatus').value;
        }
        infos.push(novembre);
      }

      if (this.form.get('isDecembre').value) {
        let decembre = new MetfpetServiceAgent.PaiementBourseInfo();
        decembre.mois = MetfpetServiceAgent.MoisAnnee.Decembre;
        if(!this.form.get('decembreStatus').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la décembre statut'
            }
          });
          valid = false;
        } else {
          decembre.status = this.form.get('decembreStatus').value;
        }
        infos.push(decembre);
      }
      
      if (valid) {
        this._store.dispatch(showLoading());
      
        this.model = MetfpetServiceAgent.EtudiantBourseManualGenerationDTO.fromJS(Object.assign(this.form.getRawValue(), { paiementBourseInfos: infos}));
        this._metfpetService.generateBourseManual(this.model)
          .subscribe(
            (data : any) => {
              this._store.dispatch(showSuccess({}));
              this.dialogRef.close(data);
            },
            (error) => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
      
    }
  }
}