import { ChangeDetectorRef, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { combineLatest, fromEvent, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith } from 'rxjs/operators';
import { MetfpetServiceAgent } from '../../services';
import { BaseTableComponent, PerfectScrollService } from '../table';

@Component({
  templateUrl: './student-kit-history.dialog.html',
})
export class StudentKitHistoryDialog extends BaseTableComponent {
  
  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstName', {static: true}) searchFirstName: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;
  etudiantId: string;

  constructor(
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    protected _perfectScrollService: PerfectScrollService,
    private _route: ActivatedRoute,
    private _matDialogRef: MatDialogRef<StudentKitHistoryDialog>, 
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.etudiantId = data.etudiantId;
   }

  ngOnInit() {
    super.ngOnInit();

    combineLatest(
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstName.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
    )
    .subscribe(([
      eventSearchNationalStudentIdentifier,
      eventSearchName, 
      eventFirstname,
    ]) => {
      this.searchForm.patchValue({
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      identifiantNationalEleve: null,
      name: null,
      firstName: null,
      from: null,
      to: null,
      etudiantId: this.etudiantId,
      isKitHistory: true
    };
    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    criteria.filters.etudiantId = this.etudiantId;
    return this._metfpetService.getUserActivityListPage(criteria);
  }

  onClose() {
    this._matDialogRef.close()
  }
}
