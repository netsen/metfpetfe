import { ChangeDetectorRef, Component, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Store } from "@ngrx/store";
import { MetfpetServiceAgent } from "../../services";
import { hideLoading, showException, showLoading, showSuccess } from "../../store/actions";
import { validateForm } from '../../utils';

@Component({
  templateUrl: './admit-graduation.dialog.html',
})
export class AdmitGraduationDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.DiplomeDTO;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<AdmitGraduationDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.form = this._formBuilder.group({
      moyenne: [null, Validators.compose([Validators.required])],
    });

    this.title = 'Admettre à la diplomation';

    this._metfpetService.getDiplome(data.diplomeId).subscribe((data) => {
        this.model = data;
        this.form.patchValue({
          moyenne: this.model.moyenne,
        });
      }
    );
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.updateDiplome(MetfpetServiceAgent.DiplomeDTO.fromJS(
        Object.assign(this.model, this.form.value)))
        .subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}