import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { MetfpetServiceAgent } from '../../services';
import { validateForm } from '../../utils';
import { showSuccess, showLoading, hideLoading, showException } from '../../store/actions';
import { EtudiantBourseStatus } from '../../interfaces/metfpet.model';

@Component({
  templateUrl: './update-student-bourse.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpdateStudentBourseDialog {

  form: FormGroup;
  title: string;
  motifText: string;
  raisonStatutBourses: Array<MetfpetServiceAgent.RaisonStatutBourseDTO>;
  ids: Array<string>;
  
  constructor(
    public dialogRef: MatDialogRef<UpdateStudentBourseDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.ids = data.ids;
    if (data.lossBourse) {
      this.title = 'Perte de bourse';
      this.motifText = 'Nom du motif de perte de la bourse';
    } else {
      this.title = 'Réactiver la/les bourse(s)';
      this.motifText = 'Motif de réactivation de bourse(s)';
    }
    this.form = this._formBuilder.group({
      'raisonStatutBourseId': null,
      'status': data.lossBourse ? EtudiantBourseStatus.Inactive : EtudiantBourseStatus.Active,
      'commentaires': null
    });
  }

  ngAfterViewInit() {
    this._metfpetService.getRaisonStatutBourseListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.raisonStatutBourses = data.results;
      this._cd.markForCheck();
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public onConfirm(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      
      this._metfpetService.updateEtudiantBourseStatus(
        MetfpetServiceAgent.UpdateEtudiantBourseStatusRequest.fromJS(Object.assign(this.form.getRawValue(), { ids: this.ids})))
        .subscribe(
          (data : any) => {
            this._store.dispatch(showSuccess({}));
            this.dialogRef.close(data);
          },
          (error) => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }
}