import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AuthService } from '../../services';
import { MetfpetServiceAgent } from '../../services';
import { validateForm, specicalCharacterValidator } from '../../utils';
import { showSuccess, showError, showLoading, hideLoading, showException } from '../../store/actions';
import { AdmissionDecision, AdmissionInstitutionStatus, AttachedDocumentAssociatedType, AttachedDocumentStatus } from '../../interfaces/metfpet.model';
import { Guid } from 'guid-typescript';
import { ThrowStmt } from '@angular/compiler';
import { DialogService, SeverityEnum } from '../dialogs';

@Component({
  templateUrl: './admission-decision-admin.dialog.html',
  styleUrls: ['./admission-decision-admin.dialog.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdmissionDecisionAdminDialog {

  form: FormGroup;
  admissionInstitutionRowViewModel: MetfpetServiceAgent.AdmissionInstitutionRowViewModel;
  admissionInstitutionDTO: MetfpetServiceAgent.AdmissionInstitutionDTO;
  admissionInstitutionId: string;
  choixAdmissionInstitutionDTOs: Array<MetfpetServiceAgent.ChoixAdmissionInstitutionDTO>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  attachedDocumentInfo: MetfpetServiceAgent.AttachedDocumentInfo;
  showAttachedDocument: boolean = false;
  acteNaissanceChecked: boolean = true;
  photoChecked: boolean = true;
  lettreChecked: boolean = true;
  attestationChecked: boolean = true;
  diplomeCEEChecked: boolean = true;
  certificatVMChecked: boolean = true;
  acteNaissanceImport: boolean = true;
  photoImport: boolean = true;
  lettreImport: boolean = true;
  attestationImport: boolean = true;
  diplomeCEEImport: boolean = true;
  certificatVMImport: boolean = true;
  isPublique: boolean = true;
  isAdmin: boolean;

  constructor(
    public dialogRef: MatDialogRef<AdmissionDecisionAdminDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _authService: AuthService,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.admissionInstitutionRowViewModel = data.admissionInstitutionRowViewModel;
    this.admissionInstitutionId = data.admissionInstitutionRowViewModel.id;
    this.isAdmin = data.isAdmin;
    this.form = this._formBuilder.group({
      'id': null,
      'programmeId': [this.admissionInstitutionRowViewModel.programmeId]
    });
  }

  ngOnInit() {
    if(this._authService.getUserStatusEtablissementCode() != 'publique' || this.admissionInstitutionRowViewModel.surDossier){
      this.isPublique = false;
    }
    if (this.admissionInstitutionId) {
      this._metfpetService.getAdmissionInstitution(this.admissionInstitutionId).subscribe(
        admissionInstitution => {
          this.admissionInstitutionDTO = admissionInstitution;
          this.choixAdmissionInstitutionDTOs = this.admissionInstitutionDTO.choixAdmissionInstitutions;
          this.form.patchValue({
            'id': this.admissionInstitutionDTO.id,
            'programmeId': this.mapProgrammeId(),
          });

          this._cd.markForCheck();
        }
      );
      this.loadPrograms(this.admissionInstitutionRowViewModel.institutionId);
      this._metfpetService.getAttachedDocumentInfo(this.admissionInstitutionId).subscribe(
        attachedDocumentInfo => {
          this.showAttachedDocument = this.admissionInstitutionRowViewModel.surDossier;
          this.updateAttachedDocumentInfo(attachedDocumentInfo);
        }
      );
    }
  }
  
  private updateAttachedDocumentInfo(attachedDocumentInfo: MetfpetServiceAgent.AttachedDocumentInfo) {
    this.attachedDocumentInfo = attachedDocumentInfo;
    this.acteNaissanceChecked = this.attachedDocumentInfo.acteNaissanceStatus === <any>AttachedDocumentStatus.Valide;
    this.photoChecked = this.attachedDocumentInfo.photoStatus === <any>AttachedDocumentStatus.Valide;
    this.lettreChecked = this.attachedDocumentInfo.lettreStatus === <any>AttachedDocumentStatus.Valide;
    this.attestationChecked = this.attachedDocumentInfo.attestationStatus === <any>AttachedDocumentStatus.Valide;
    this.diplomeCEEChecked = this.attachedDocumentInfo.diplomeCEEStatus === <any>AttachedDocumentStatus.Valide;
    this.certificatVMChecked = this.attachedDocumentInfo.certificatVMStatus === <any>AttachedDocumentStatus.Valide;
    if (this.attachedDocumentInfo.acteNaissanceFileName) {
      this.acteNaissanceImport = false;
    }
    if (this.attachedDocumentInfo.photoLink) {
      this.photoImport = false;
    }
    if (this.attachedDocumentInfo.lettreLink) {
      this.lettreImport = false;
    }
    if (this.attachedDocumentInfo.attestationLink) {
      this.attestationImport = false;
    }
    if (this.attachedDocumentInfo.diplomeCEELink) {
      this.diplomeCEEImport = false;
    }
    if (this.attachedDocumentInfo.certificatVMLink) {
      this.certificatVMImport = false;
    }
    this._cd.markForCheck();
  }

  private mapProgrammeId(): any {
    if (this.admissionInstitutionDTO.decision === <any>AdmissionDecision.NonAdmis) {
      return -1;
    } else if (this.admissionInstitutionDTO.decision === <any>AdmissionDecision.InvalidDocuments) {
      return -2;
    }
    return this.admissionInstitutionDTO.programmeId;
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public openActeNaissance(): void {
    window.open(this.attachedDocumentInfo.acteNaissanceLink, "_blank");
  }

  public changeActeNaissanceChecked($event): void {
    this._store.dispatch(showLoading());
    this._metfpetService.toogleAttachedDocumentStatus(this.attachedDocumentInfo.acteNaissanceId) 
      .subscribe(
      (attachedDocument : any) => {
        this._store.dispatch(showSuccess({}));
        if (attachedDocument) {
          this.attachedDocumentInfo.acteNaissanceStatus = attachedDocument.statusName;
          this.acteNaissanceChecked = this.attachedDocumentInfo.acteNaissanceStatus === <any> AttachedDocumentStatus.Valide;
          this._cd.markForCheck();
        }
      },
      (error) => this._store.dispatch(showException({error: error}))
    )
    .add(() => this._store.dispatch(hideLoading()));
  }

  public openPhoto(): void {
    window.open(this.attachedDocumentInfo.photoLink, "_blank");
  }

  public changePhotoChecked($event): void {
    this._store.dispatch(showLoading());
    this._metfpetService.toogleAttachedDocumentStatus(this.attachedDocumentInfo.photoId) 
      .subscribe(
      (attachedDocument : any) => {
        this._store.dispatch(showSuccess({}));
        if (attachedDocument) {
          this.attachedDocumentInfo.photoStatus = attachedDocument.statusName;
          this.photoChecked = this.attachedDocumentInfo.photoStatus === <any> AttachedDocumentStatus.Valide;
          this._cd.markForCheck();
        }
      },
      (error) => this._store.dispatch(showException({error: error}))
    )
    .add(() => this._store.dispatch(hideLoading()));
  }

  public openLettre(): void {
    window.open(this.attachedDocumentInfo.lettreLink, "_blank");
  }

  public changeLettreChecked($event): void {
    this._store.dispatch(showLoading());
    this._metfpetService.toogleAttachedDocumentStatus(this.attachedDocumentInfo.lettreId) 
      .subscribe(
      (attachedDocument : any) => {
        this._store.dispatch(showSuccess({}));
        if (attachedDocument) {
          this.attachedDocumentInfo.lettreStatus = attachedDocument.statusName;
          this.lettreChecked = this.attachedDocumentInfo.lettreStatus === <any> AttachedDocumentStatus.Valide;
          this._cd.markForCheck();
        }
      },
      (error) => this._store.dispatch(showException({error: error}))
    )
    .add(() => this._store.dispatch(hideLoading()));
  }

  public openAttestation(): void {
    window.open(this.attachedDocumentInfo.attestationLink, "_blank");
  }

  public changeAttestationChecked($event): void {
    this._store.dispatch(showLoading());
    this._metfpetService.toogleAttachedDocumentStatus(this.attachedDocumentInfo.attestationId) 
      .subscribe(
      (attachedDocument : any) => {
        this._store.dispatch(showSuccess({}));
        if (attachedDocument) {
          this.attachedDocumentInfo.attestationStatus = attachedDocument.statusName;
          this.attestationChecked = this.attachedDocumentInfo.attestationStatus === <any> AttachedDocumentStatus.Valide;
          this._cd.markForCheck();
        }
      },
      (error) => this._store.dispatch(showException({error: error}))
    )
    .add(() => this._store.dispatch(hideLoading()));
  }

  public openDiplomeCEE(): void {
    window.open(this.attachedDocumentInfo.diplomeCEELink, "_blank");
  }

  public changeDiplomeCEEChecked($event): void {
    this._store.dispatch(showLoading());
    this._metfpetService.toogleAttachedDocumentStatus(this.attachedDocumentInfo.diplomeCEEId) 
      .subscribe(
      (attachedDocument : any) => {
        this._store.dispatch(showSuccess({}));
        if (attachedDocument) {
          this.attachedDocumentInfo.diplomeCEEStatus = attachedDocument.statusName;
          this.diplomeCEEChecked = this.attachedDocumentInfo.diplomeCEEStatus === <any> AttachedDocumentStatus.Valide;
          this._cd.markForCheck();
        }
      },
      (error) => this._store.dispatch(showException({error: error}))
    )
    .add(() => this._store.dispatch(hideLoading()));
  }

  public openCertificatVM(): void {
    window.open(this.attachedDocumentInfo.certificatVMLink, "_blank");
  }

  public changeCertificatVMChecked($event): void {
    this._store.dispatch(showLoading());
    this._metfpetService.toogleAttachedDocumentStatus(this.attachedDocumentInfo.certificatVMId) 
      .subscribe(
      (attachedDocument : any) => {
        this._store.dispatch(showSuccess({}));
        if (attachedDocument) {
          this.attachedDocumentInfo.certificatVMStatus = attachedDocument.statusName;
          this.certificatVMChecked = this.attachedDocumentInfo.certificatVMStatus === <any> AttachedDocumentStatus.Valide;
          this._cd.markForCheck();
        }
      },
      (error) => this._store.dispatch(showException({error: error}))
    )
    .add(() => this._store.dispatch(hideLoading()));
  }

  public onConfirm(): void {
    validateForm(this.form);
    if (this.form.valid) {
      
      let programmeId = this.form.get('programmeId').value;
      if (programmeId === -1) {
        this.admissionInstitutionDTO.decision = MetfpetServiceAgent.AdmissionDecision.NonAdmis;
        if (this.admissionInstitutionDTO.status == <any>AdmissionInstitutionStatus.AnalyseEnCours) {
          this.admissionInstitutionDTO.status = MetfpetServiceAgent.AdmissionInstitutionStatus.ResultatEnregistre;
        }
        this.admissionInstitutionDTO.programmeId = null;
        this.admissionInstitutionDTO.faculteId = null;
      } if (programmeId === -2)  {
        this.admissionInstitutionDTO.decision = MetfpetServiceAgent.AdmissionDecision.InvalidDocuments;
        this.admissionInstitutionDTO.programmeId = null;
        this.admissionInstitutionDTO.faculteId = null;
      } else {
        let programme = this.programList.find(v => v.id === programmeId);
        if (programme) {
          if (!this.isAdmin && this.admissionInstitutionRowViewModel.surDossier && programme.typeAdmissionName !== 'Etude de dossier/Tests') {
            this._dialogService.openInfoDialog({
              data: {
                severity: SeverityEnum.ERROR, 
                message: 'Vous n\'êtes pas autorisé à donner une décision pour ce programme'
              }
            });
            return;
          }
          this.admissionInstitutionDTO.decision = MetfpetServiceAgent.AdmissionDecision.Admis;
          if (this.admissionInstitutionDTO.status == <any>AdmissionInstitutionStatus.AnalyseEnCours) {
            this.admissionInstitutionDTO.status = MetfpetServiceAgent.AdmissionInstitutionStatus.ResultatEnregistre;
          }
          this.admissionInstitutionDTO.programmeId = programme.id;
          this.admissionInstitutionDTO.faculteId = programme.faculteId;
        }
      }

      this._store.dispatch(showLoading());

      this._metfpetService.updateAdmissionInstitution(this.admissionInstitutionDTO)
        .subscribe(
          (data : any) => {
            this._store.dispatch(showSuccess({}));
            this.dialogRef.close(data);
          },
          (error) => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  private loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
         pageIndex: -1, filters: {institution: institutionId}
      })).subscribe(data => {
        this.programList = data.results;
        this._cd.markForCheck();
      });
  }

  private onFileUpload(event: any, associatedType: AttachedDocumentAssociatedType) {
    if (event.target.files && event.target.files.length > 0) {
      this._store.dispatch(showLoading());

      let file : Blob = event.target.files[0];
      var fileReader = new FileReader();
      fileReader.onloadend = function () {
        
      }
      fileReader.readAsBinaryString(file);
      
      let fileParameter = 
      {
        data : file,
        fileName : event.target.fileName
      };
      
      this._metfpetService.attachDocument(fileParameter, this.admissionInstitutionId, associatedType)
        .subscribe(
          () => {
            this._metfpetService.getAttachedDocumentInfo(this.admissionInstitutionId).subscribe(
              attachedDocumentInfo => {
                this.updateAttachedDocumentInfo(attachedDocumentInfo);
                this._store.dispatch(showSuccess({}));
              }
            ).add(() => this._store.dispatch(hideLoading()));
          },
          (error) => this._store.dispatch(showError({message: error.message}))
        );
    }
  }

  public uploadActeNaissance(): void {
    document.getElementById('acteNaissanceSelector').click();
  }

  public onActeNaissanceFileUpload(event): void {
    this.onFileUpload(event, AttachedDocumentAssociatedType.ActeNaissance);
  }

  public uploadPhoto(): void {
    document.getElementById('photoSelector').click();
  }

  public onPhotoFileUpload(event): void {
    this.onFileUpload(event, AttachedDocumentAssociatedType.Photo);
  }

  public uploadLettre(): void {
    document.getElementById('lettreSelector').click();
  }

  public onLettreFileUpload(event): void {
    this.onFileUpload(event, AttachedDocumentAssociatedType.Lettre);
  }

  public uploadAttestation(): void {
    document.getElementById('attestationSelector').click();
  }

  public onAttestationFileUpload(event): void {
    this.onFileUpload(event, AttachedDocumentAssociatedType.Attestation);
  }

  public uploadDiplomeCEE(): void {
    document.getElementById('diplomeCEESelector').click();
  }

  public onDiplomeCEEFileUpload(event): void {
    this.onFileUpload(event, AttachedDocumentAssociatedType.DiplomeCEE);
  }

  public uploadCertificatVM(): void {
    document.getElementById('certificatVMSelector').click();
  }

  public onCertificatVMFileUpload(event): void {
    this.onFileUpload(event, AttachedDocumentAssociatedType.CertificatVM);
  }
}