import { ChangeDetectorRef, Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { SelectionType } from '@swimlane/ngx-datatable';
import { AuthService, MetfpetServiceAgent } from '../../services';
import { hideLoading, showError, showLoading, showSuccess } from '../../store/actions';
import { ReceiptFormType } from '../../interfaces';

@Component({
  templateUrl: './generate-receipt-form.dialog.html',
})
export class GenerateStudentReceiptFormDialog {

  readonly DEFAULT_QUANTITY: number = 1;
  readonly tabs = [
    { id: 1 },
    { id: 2 }
  ];
  currentTab: number = this.tabs[0].id;
  form: FormGroup;
  error: string;
  loadingIndicator = true;
  etudiantId: string;
  SelectionType = SelectionType;
  schoolSupplyList: any;
  schoolSupplyPagedRows: any;
  schoolSupplyPageOffset = 0;
  schoolSupplyTotal = 0;
  receiptFormFileName: string;

  receiptFormList: Array<MetfpetServiceAgent.ReceiptFormRowViewModel>;
  receiptFormPageOffset = 0;
  receiptFormTotal = 0;
  receiptFormPageSize = 10;

  constructor(
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _matDialogRef: MatDialogRef<GenerateStudentReceiptFormDialog>, 
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.etudiantId = data.etudiantId;
   }

  ngOnInit(): void {
    this.loadDataOfReceiptGeneration();
  }

  loadDataOfReceiptGeneration() {
    this.loadingIndicator = true;
    this.schoolSupplyPagedRows = [];
    this.schoolSupplyPageOffset = 0;
    this.schoolSupplyTotal = 0;
    this.receiptFormFileName = null;
    this._metfpetService.getSchoolSupplyEtudiantListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1,
      filters: {
        etudiantId: this.etudiantId,
        hasReception: true,
        hasQuantity: true
      } 
    })).subscribe(data => {
      this.schoolSupplyList = data.results;
      this.schoolSupplyTotal = data.totalCount;
      this.loadingIndicator = false;
      this.schoolSupplyPagedRows = this.schoolSupplyList.slice(this.schoolSupplyPageOffset * 10, (this.schoolSupplyPageOffset + 1) * 10);
      this._cd.markForCheck();
    });
  }

  loadDataOfReceiptUpload() {
    this.receiptFormList = [];
    this.receiptFormPageOffset = 0;
    this.receiptFormTotal = 0;
    this.getReceiptFormList();
  }

  setTab(tabId) {
    this.currentTab = tabId;
    if (this.currentTab === this.tabs[0].id) {
      this.loadDataOfReceiptGeneration();
    } else if (this.currentTab === this.tabs[1].id) {
      this.loadDataOfReceiptUpload();
    }
  }

  getTabClasses(tabId) {
    return [this.currentTab === tabId ? 'bg-green-dark' : 'bg-white color-green-dark', 'w-100'];
  }

  onChangeSchoolSupplyTable(event) {
    this.schoolSupplyPageOffset = event.offset;
    this.schoolSupplyPagedRows = this.schoolSupplyList.slice(this.schoolSupplyPageOffset * 10, (this.schoolSupplyPageOffset + 1) * 10);
  }

  onChangeSchoolSupplyMaterialTable(event) {
    this.schoolSupplyPageOffset = event.pageIndex;
    this.schoolSupplyPagedRows = this.schoolSupplyList.slice(this.schoolSupplyPageOffset * 10, (this.schoolSupplyPageOffset + 1) * 10);
  }

  decreaseSupplyQuantity(row) {
    let schoolSupply = this.schoolSupplyList.find(x => x.id === row.id);
    if (schoolSupply.quantity >= this.DEFAULT_QUANTITY) {
      schoolSupply.quantity -= 1;
      if (schoolSupply.quantity === 0) {
        schoolSupply.checked = false;
      }
      this._cd.markForCheck();
    }
  }

  increaseSupplyQuantity(row) {
    let schoolSupply = this.schoolSupplyList.find(x => x.id === row.id);
    if (schoolSupply.quantity < schoolSupply.originalQuantity) {
      schoolSupply.quantity += 1;
      this._cd.markForCheck();
    }
  }

  onCheckboxAllChange(event) {
    if (event.checked) {
      this.schoolSupplyList.forEach(obj => {
        obj.checked = obj.quantity > 0;
      });
    } else {
      this.schoolSupplyList.forEach(obj => {
        obj.checked = false;
      });
    }
  }

  isCheckedAll() {
    return this.schoolSupplyList?.every(obj => obj.checked === true);
  }

  disableCheckedAll() {
    return this.schoolSupplyList?.some(obj => obj.quantity === 0);
  }

  onCheckboxChange(row) {
    let schoolSupply = this.schoolSupplyList.find(x => x.id === row.id);
    schoolSupply.checked = !schoolSupply.checked;
    this._cd.markForCheck();
  }

  allowGenerateReceiptForm() {
    return this.schoolSupplyList?.some(obj => obj.checked && obj.quantity > 0);
  }

  generateReceiptFormKit() {
    this._store.dispatch(showLoading());
    this._metfpetService.generateReceiptFormKit(MetfpetServiceAgent.GenerateReceiptFormKitDTO.fromJS({
      etudiantId: this.etudiantId,
      schoolSuppliesEtudiant: this.schoolSupplyList.filter(obj => obj.checked && obj.quantity > 0)
    }))
      .subscribe(
        (fileName) => {
          this.receiptFormFileName = fileName;
          this._cd.markForCheck();
          this._store.dispatch(showSuccess({}));
        },
        (error) => this._store.dispatch(showError({message: error.message}))
      )
      .add(() => this._store.dispatch(hideLoading()));
  }

  openReceiptForm() {
    if (this.receiptFormFileName) {
      window.open(this.receiptFormFileName, "_blank");
    }
  }

  uploadFile() {
    document.getElementById('attachedFileSelector').click();
  }

  onFileUpload(event) {
    if (event.target.files && event.target.files.length > 0) {
      this._store.dispatch(showLoading());

      let file : Blob = event.target.files[0];
      let uploadedFileName : string = event.target.files[0].name;
      var fileReader = new FileReader();
      fileReader.onloadend = function () {
        
      }
      fileReader.readAsBinaryString(file);
      
      let fileParameter = 
      {
        data : file,
        fileName : uploadedFileName
      };

      this._metfpetService.uploadReceiptFormKit(fileParameter, this.etudiantId, uploadedFileName)
        .subscribe(fileLink => {
          if (fileLink) {
            this.getReceiptFormList()
            this._cd.markForCheck();
            this._store.dispatch(showSuccess({}));
          }
        },
        (error) => this._store.dispatch(showError({message: error.message}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  getReceiptFormList() {
    this.loadingIndicator = true;
    this._metfpetService.getReceiptFormListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: this.receiptFormPageOffset,
      pageSize: this.receiptFormPageSize,
      filters: {
        etudiantId: this.etudiantId,
        type: ReceiptFormType.Upload
      }
    })).subscribe(data => {
      this.receiptFormList = data.results;
      this.receiptFormTotal = data.totalCount;
    })
    .add(() => {
      this.loadingIndicator = false;
      this._cd.markForCheck();
    });
  }

  onChangeReceiptFormTable(event) {
    this.receiptFormPageOffset = event.offset;
    this.getReceiptFormList();
  }

  onChangeReceiptFormMaterialTable(event) {
    this.receiptFormPageOffset = event.pageIndex;
    this.getReceiptFormList();
  }

  openFile(link) {
    if (link) {
      window.open(link, "_blank");
    }
  }

  onClose() {
    this._matDialogRef.close()
  }
}
