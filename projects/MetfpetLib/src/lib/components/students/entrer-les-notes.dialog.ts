import { ChangeDetectorRef, Component, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Store } from "@ngrx/store";
import { MetfpetServiceAgent } from "../../services";
import { hideLoading, showException, showLoading, showSuccess } from "../../store/actions";
import { validateForm } from '../../utils';
import { FinalExamenStatusGlobal, TypeExamen } from "../../interfaces";

const EMPHASIS_CURRENT_STEP_CSS = "text-bold";

const CREATE_BOURSES_PAYMENT_STEP_1 = "Identification";
const CREATE_BOURSES_PAYMENT_STEP_2 = "Entrée des notes";

@Component({
  templateUrl: "./entrer-les-notes.dialog.html",
  styleUrls: ["./entrer-les-notes.dialog.scss"],
})
export class EntrerLesNotesDialog {
  currentStepName: string;
  curPVNumber: number;
  finalExamenEtudiantNotes: Array<MetfpetServiceAgent.FinalExamenEtudiantNoteDTO>;
  title: string;
  form: FormGroup;
  showSaveAndEnterNewCaseBtn: boolean;
  typeExamenName: string;
  statusResult: Array<any>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<EntrerLesNotesDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _cd: ChangeDetectorRef,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.title = "Entrer les notes";
    this.statusResult = [
      {value : FinalExamenStatusGlobal.Admis, name : 'Admis'},
      {value : FinalExamenStatusGlobal.NonAdmis, name : 'Non admis'}
    ];
    this.initFirstStep();
    this.showSaveAndEnterNewCaseBtn = true;
    if (data.numeroPV) {
      this.showSaveAndEnterNewCaseBtn = false;
      this.form.patchValue({
        pvNumber: data.numeroPV
      });
      this.goToNextStep();
    }
  }

  initFirstStep() {
    this.currentStepName = CREATE_BOURSES_PAYMENT_STEP_1;
    this.curPVNumber = null;
    this.finalExamenEtudiantNotes = [];
    this.form = this._formBuilder.group({
      pvNumber: null,
      finalExamenEtudiantId: null,
      notes: this._formBuilder.array([]),
    });
  }

  get notes() {
    return this.form.controls['notes'] as FormArray;
  }

  buildNoteForm() {
    if (this.finalExamenEtudiantNotes) {
      for (var finalExamenEtudiantNote of this.finalExamenEtudiantNotes) {
        this.notes.push(this._formBuilder.group({
          id: finalExamenEtudiantNote.id,
          finalExamenEtudiantId: finalExamenEtudiantNote.finalExamenEtudiantId,
          matiereName: finalExamenEtudiantNote.matiereName,
          moyenne: finalExamenEtudiantNote.moyenne
        }));
      }
    }
  }

  getCurrentStepCss(step: string) {
    if (this.currentStepName === step)
    {
      return EMPHASIS_CURRENT_STEP_CSS;
    }
    return "";
  }

  goToNextStep() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());    
      let pvNumber = this.form.controls.pvNumber.value;
      this._metfpetService.identifyNumeroPV(pvNumber)
        .subscribe(
          (data : MetfpetServiceAgent.FinalExamenEtudiantResult) => {
            this.curPVNumber = pvNumber;
            this.typeExamenName = data.typeExamenName;
            this.finalExamenEtudiantNotes = data.finalExamenEtudiantNoteDTOs;
            this.buildNoteForm();
            this.currentStepName = CREATE_BOURSES_PAYMENT_STEP_2;
            this._cd.markForCheck();
          },
          (error) => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onSave(needToCloseDialog: boolean) {
    this._store.dispatch(showLoading());

    this.finalExamenEtudiantNotes = this.notes.value.map(x => MetfpetServiceAgent.FinalExamenEtudiantNoteDTO.fromJS(x));
    
    this._metfpetService.enterGrades(this.finalExamenEtudiantNotes)
    .subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            if (needToCloseDialog) {
              this._dialogRef.close(true);
            } else {
              this.initFirstStep();
            }
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
  }

  save() {
    this.onSave(true);
  }

  saveAndEnterNewCase() {
    this.onSave(false);
  }

  onClose() {
    this._dialogRef.close();
  }
}
