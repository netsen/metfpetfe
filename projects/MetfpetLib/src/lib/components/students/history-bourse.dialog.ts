import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { MetfpetServiceAgent } from '../../services';

@Component({
  templateUrl: './history-bourse.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HistoryBourseDialog {

  viewModel: MetfpetServiceAgent.EtudiantBourseModificationRowViewModel;
  model: MetfpetServiceAgent.EtudiantBourseModificationDTO;
  
  constructor(
    public dialogRef: MatDialogRef<HistoryBourseDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.viewModel = data.viewModel;
    _metfpetService.getEtudiantBourseModification(this.viewModel.id).subscribe(data => {
      this.model = data;
      this._cd.markForCheck();
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }
}