import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { MetfpetServiceAgent } from '../../services';
import { validateForm } from '../../utils';

@Component({
  templateUrl: './quantity-decrease-reason.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuantityDecreaseReasonDialog {

  form: FormGroup;
  reasonList: Array<MetfpetServiceAgent.ReasonRowViewModel>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<QuantityDecreaseReasonDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.form = this._formBuilder.group({
      reasonId: [null, Validators.required],
      comment: null
    });

    this._metfpetService.getReasonListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.reasonList = data.results;
    });
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._dialogRef.close(this.form.value);
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}