import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { MetfpetServiceAgent } from "../../services";
import { hideLoading, showException, showLoading, showSuccess } from "../../store/actions";
import { validateForm } from '../../utils';

@Component({
  templateUrl: './telecharger-le-releve-de-notes.dialog.html',
})
export class TelechargerLeReleveDeNotesDialog {

  form: FormGroup;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  etudiantId: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<TelechargerLeReleveDeNotesDialog>,
    protected _cdRef: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.anneeAcademiqueList = data.anneeAcademiqueList;
    this.etudiantId = data.etudiantId;
    this.form = this._formBuilder.group({
      anneeAcademiqueId: [null, Validators.required],
    });
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.generateTranscript(MetfpetServiceAgent.GenerateTranscriptRequest.fromJS({
        anneeAcademiqueId: this.form.get('anneeAcademiqueId').value,
        etudiantId: this.etudiantId
      }))
        .subscribe(
          (data) => {
            if (data) {
              window.open(data, "_blank");
            }
            this._dialogRef.close();
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}