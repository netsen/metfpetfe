import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Store } from '@ngrx/store';
import { MetfpetServiceAgent } from '../../services';
import { validateForm } from '../../utils';
import { showSuccess, showError, showLoading, hideLoading, showException } from '../../store/actions';
import { MoyenneValues } from '../../interfaces';

@Component({
  templateUrl: './update-student-concours-detail.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentConcoursDetailDialog {

  form: FormGroup;
  _model: MetfpetServiceAgent.ConcoursEtudiantDTO;
  centreList: Array<MetfpetServiceAgent.CentreConcoursRowViewModel>;
  isReadOnly: boolean;
  isAdmin: boolean;
  isCompetitionResult: boolean;
  isUniversitaireUser: boolean;
  isMinistre: boolean;
  title: string;
  moyenneValues = MoyenneValues;

  constructor(
    public dialogRef: MatDialogRef<StudentConcoursDetailDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.centreList = data.centreList;
    this.isReadOnly = data.isReadOnly;
    this.isAdmin = data.isAdmin;
    this.isCompetitionResult = data.isCompetitionResult;
    this.isUniversitaireUser= data.isUniversitaireUser;
    this.isMinistre = data.isMinistre;
    this.title = this.isCompetitionResult ? 'Résultats du concours' : 'Modalités du concours';
    this.form = this._formBuilder.group({
      'centreConcoursId': [null, Validators.required],
      'concoursEtudiantNotes': this._formBuilder.array([])
    });
    if (data.id) {
      this._metfpetService.getConcoursEtudiantDetail(data.id).subscribe((data)=>{
        this._model = data;
        this.form.patchValue({
          centreConcoursId: this._model.centreConcoursId,
        })
        if (this._model.concoursEtudiantNotes) {
          for (let item of this._model.concoursEtudiantNotes) {
            this.concoursEtudiantNotes.push(this._formBuilder.group({
              id: item.id,
              concoursEtudiantId: item.concoursEtudiantId,
              concoursMatiereId: item.concoursMatiereId,
              moyenne: item.moyenne,
              matiereName: item.matiereName,
              matiereCoefficient: item.matiereCoefficient
            }));
          }
        }
        this._cd.markForCheck();
      })
    }
  }

  get concoursEtudiantNotes() {
    return this.form.controls['concoursEtudiantNotes'] as FormArray;
  }

  getMatiereNameAndCoefficientText(form: FormGroup) {
    return form.value?.matiereName + ' (' + form.value?.matiereCoefficient + ')';
  }

  isPublished(): boolean {
    return this._model && (this._model.isPublished || this.isAdmin);
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());

      this._model = Object.assign({}, this._model, this.form.value);
      this._model.concoursEtudiantNotes = this.concoursEtudiantNotes.value
        .map(x => MetfpetServiceAgent.ConcoursEtudiantNoteDTO.fromJS(x));
  
      this._metfpetService.updateConcoursEtudiantDetail(MetfpetServiceAgent.ConcoursEtudiantDTO.fromJS(this._model))
        .subscribe(
          (data : any) => {
            this._store.dispatch(showSuccess({}));
            this.dialogRef.close(data);
          },
          (error) => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public onClose(): void {
    this.dialogRef.close();
  }
}