import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppSettings, Settings } from '../../../theme';

@Component({
  selector: 'app-error',
  templateUrl: './page-error.component.html'
})
export class PageErrorComponent {

  public settings: Settings;

  constructor(public appSettings: AppSettings, public router: Router) {
    this.settings = this.appSettings.settings; 
  }

  ngAfterViewInit() {
    setTimeout(() => this.settings.loadingSpinner = false);
  }

  goHome(): void {
    this.router.navigate(['/']);
  }
}