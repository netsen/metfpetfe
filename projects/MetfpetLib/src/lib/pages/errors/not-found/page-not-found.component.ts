import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { AppSettings, Settings } from '../../../theme';

@Component({
  selector: 'app-not-found',
  templateUrl: './page-not-found.component.html'
})
export class PageNotFoundComponent{
  public form:FormGroup;
  public settings: Settings;
  constructor(
    public appSettings:AppSettings, 
    public router:Router, 
    public fb: FormBuilder) {
    this.settings = this.appSettings.settings; 
  }

  ngOnInit(){
    this.form = this.fb.group({
      'param': [null, Validators.required]
    });
  }

  ngAfterViewInit() {
    setTimeout(() => this.settings.loadingSpinner = false);
  }

  public onSubmit(values:Object):void {
    if (this.form.valid) {
      this.router.navigate(['/search', values['param'] ]);
    }
  }

  public goHome(): void {
    this.router.navigate(['/']);
  }

}