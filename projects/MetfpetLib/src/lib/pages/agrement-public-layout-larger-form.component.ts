import { Component } from '@angular/core';
import { AppSettings } from '../theme';

@Component({
  templateUrl: './agrement-public-layout-larger-form.component.html',
  styleUrls: ['./agrement-public-layout-larger-form.component.scss']
})
export class AgrementPublicLayoutLargerFormComponent {
  
  constructor(private _app: AppSettings) { 
    this._app.settings.overflowYScroll = true;
  }

  ngOnDestroy() {
    this._app.settings.overflowYScroll = false;
  }
}
