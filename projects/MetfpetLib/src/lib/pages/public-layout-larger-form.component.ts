import { Component } from '@angular/core';
import { AppSettings } from '../theme';

@Component({
  templateUrl: './public-layout-larger-form.component.html',
  styleUrls: ['./public-layout-larger-form.component.scss']
})
export class PublicLayoutLargerFormComponent {
  
  constructor(private _app: AppSettings) { 
    this._app.settings.overflowYScroll = true;
  }

  ngOnDestroy() {
    this._app.settings.overflowYScroll = false;
  }
}
