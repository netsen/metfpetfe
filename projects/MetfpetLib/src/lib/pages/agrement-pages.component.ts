import { Component, OnInit, ViewChild, HostListener, ViewChildren, QueryList } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { PerfectScrollbarDirective, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { Store, select } from '@ngrx/store';
import { AppSettings, Settings } from '../theme';
import { IAppState } from '../interfaces';
import { MenuService } from '../components/menu';
import { PerfectScrollService } from '../components/table';
import { rotate } from '../utils';
import { selectLoading, selectLoggedInPerson } from '../store/selectors';

@Component({
  selector: 'agrement-app-pages',
  templateUrl: './agrement-pages.component.html',
  styleUrls: ['./agrement-pages.component.scss'],
  animations: [ rotate ],
})
export class AgrementPagesComponent implements OnInit { 
  @ViewChild('sidenav') sidenav:any;  
  @ViewChild('backToTop') backToTop:any;  
  @ViewChildren(PerfectScrollbarDirective) pss: QueryList<PerfectScrollbarDirective>;
  public optionsPsConfig: PerfectScrollbarConfigInterface = {};
  public settings:Settings;
  public showSidenav:boolean = false;
  public showInfoContent:boolean = false;
  public toggleSearchBar:boolean = false;
  private defaultMenu:string; //declared for return default menu when window resized 
  public menus = ['Verticale', 'Horizontale'];
  public menuOption:string;
  public menuTypes = ['default', 'compact', 'mini'];
  public menuTypeOption:string;
  public loading: boolean;
  public user: any;

  constructor(
    public appSettings:AppSettings, 
    public router:Router,
    private _store: Store<IAppState>,
    private _menuService: MenuService,
    private _perfectScrollService: PerfectScrollService
  ){
    this.settings = this.appSettings.settings;
    this.settings = this.appSettings.settings;
    this._store.pipe(select(selectLoading)).subscribe(
      (loading: boolean) => setTimeout(() => this.loading = loading)
    );

    this._store.pipe(select(selectLoggedInPerson)).subscribe(
      (person: any) => this.user = person
    );
  }
  
  ngOnInit() {
    this.optionsPsConfig.wheelPropagation = false;
    if(window.innerWidth <= 960){
      this.settings.menu = 'vertical';
      this.settings.sidenavIsOpened = false;
      this.settings.sidenavIsPinned = false;
    }
    this.menuOption = this.settings.menu; 
    this.menuTypeOption = this.settings.menuType; 
    this.defaultMenu = this.settings.menu;
  }

  ngAfterViewInit(){
    setTimeout(() => { this.settings.loadingSpinner = false }, 300); 
    this.backToTop.nativeElement.style.display = 'none'; 
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.scrollToTop();
      } 
      if(window.innerWidth <= 960){
        this.sidenav.close(); 
      }                
    });
    if(this.settings.menu == 'vertical')
      this._menuService.expandActiveSubMenu(this._menuService.getVerticalMenuItems());
    this._perfectScrollService.setScrollbars(this.pss);
  }

  public toggleSidenav(){
    this.sidenav.toggle();
  }
  
  public closeInfoContent(showInfoContent){
    this.showInfoContent = !showInfoContent;
  }

  @HostListener('window:resize')
  public onWindowResize():void {
    if(window.innerWidth <= 960){
      this.settings.sidenavIsOpened = false;
      this.settings.sidenavIsPinned = false;
      this.settings.menu = 'vertical'
    }
    else{
      (this.defaultMenu == 'horizontal') ? this.settings.menu = 'horizontal' : this.settings.menu = 'vertical'
      this.settings.sidenavIsOpened = true;
      this.settings.sidenavIsPinned = true;
    }
  }

  public onPsScrollY(event){   
    (event.target.scrollTop > 300) ? this.backToTop.nativeElement.style.display = 'flex' : this.backToTop.nativeElement.style.display = 'none';
  }

  public scrollToTop() {
    this.pss.forEach(ps => {
      if(ps.elementRef.nativeElement.id == 'main'){
        ps.scrollToTop(0,250);
        ps.update();
      }
    });
  }
  
  public closeSubMenus(){
    if(this.settings.menu == 'vertical'){
      this._menuService.closeAllSubMenus();
    }      
  }
}