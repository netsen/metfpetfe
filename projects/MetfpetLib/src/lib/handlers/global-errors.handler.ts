import { ErrorHandler, Injectable, Injector, NgZone } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../services';

@Injectable()
export class GlobalErrorsHandler implements ErrorHandler {

  private _authService: AuthService;
  private _router: Router;

  // Because the GlobalErrorsHandler is created before the providers, we’ll have to use the Injector to get them.
  constructor(private _injector: Injector, private _zone: NgZone) {
    setTimeout(() => {
      this._authService = this._injector.get(AuthService);
      this._router = this._injector.get(Router);
    });
  }

  handleError(error: Error | HttpErrorResponse) {    
    // Always log the error
    // TODO: TAQ create a logging and notification service later
    console.log(error);

    if (error instanceof HttpErrorResponse) {    
      // Server error happened    
      if (!navigator.onLine) {
        // No Internet connection TODO
        return;
      }

      if (error.status === 401) {
        this._authService.signOut();
        return this._zone.run(() => this._router.navigate(['/login'], { queryParams: { error: 'unauthorized'}}));
      }

      if (error.status === 404) {
        return this._zone.run(() => this._router.navigate(['/pagenotfound']));
      }
      
    } 

    this._zone.run(() => this._router.navigate(['/error']));
  }
}