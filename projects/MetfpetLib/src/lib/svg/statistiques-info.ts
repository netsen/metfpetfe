export const appStatistiquesInfoIcon = {
    data: `<svg width="35" height="35" viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <rect width="35" height="35" fill="url(#pattern0_5333_18)"/>
    <defs>
    <pattern id="pattern0_5333_18" patternContentUnits="objectBoundingBox" width="1" height="1">
    <use xlink:href="#image0_5333_18" transform="scale(0.0111111)"/>
    </pattern>
    <image id="image0_5333_18" width="90" height="90" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABaCAYAAAA4qEECAAAACXBIWXMAAAsTAAALEwEAmpwYAAAEvklEQVR4nO2dSW9cRRDHS0qAoLBc2SRAwBdgOQECIUCCIAGHAYlFIiw+GEbx9P/fnuPjxiIIkSW+RgLhACIfwBAFBRIOXEAc2I3jcMKWjB+qpJEsa8YMTL+ufm/6L9XFnvFU/abda3U9kaKioqKioqKioqKioqLJ1Ov19gC4C8BLJN8G8AHJMyS/BXAOwEawc/oz/Z2+BsBb+h7v/Z36Nyb8uNnS4uLiDSQXAHxE8g+S9TQG4DzJ4865Q4PB4HqZZQ0Gg8sBPA/gBIC/poW7i22S/JTkc1VV7ZNZ0fz8/BXa0gD82CDccS39NwCvD4fDq6WrmpubuwSAJ7maGvAIWwUA9Um6JO/9vSTPZgB4Zwv/huSD0nZpn0jyfZJb1lB3sS0AS/1+/zJpo4bD4U0kP8sAZD1h6/5iMBjcKm2S/jvGmKYxPezzAB6QNojkkwD+tIbG/w97w3v/tOQsHcgbnhPXiWBrDK9IjnLOPREWB+agGAk2gKckJ2m/RnLdGg7jw94g+bDkIO/9bW0c+Pjf9k1uMYWsc0+dFlnDYPN2ynSeHRYj1hDqRC17yQSy9/4+gxXfMsnDwZYTf/aWc+4eiw2irxMGuQbgkRFf9qOhD03lx5mqqvYmAx124ZK1JoyAvM2XA4l9GSSB7L2/MvFW5/K/+QTg84T+rOieeuOgSS6mbEEk35kA9LuJWzUaP34i+UtuoHlxcEzp00+NHovpGV/igOoMu44L5px7tknQJwxA1zq7GOeT9/4xC59IftJYSoDVzhwu7hMfGAU58fRuu20uLCxcGx205l0YBVRvA65dxGEA75E8mYE/r0UHHZJbTANjZgbgaFTImmKlqzPrwJifrUVNP9NcuAyCqnM0AHdEA+2ce9k6IGZqAA5GA61ZndYBMVMD8EZM0B9aB8RZGBABfGUdEDM1AKejgSb5vXVAMt43a9jfxQRtngEq+YJeiQY6HLsX0BzJYL2AZstAl66DabqOMhgyzWCYw/ROxjeCTk3vzBcski/oo51agku+oOMtwcMt1gKaIxm80KltUhkja7+897d3auNf8gQdd+M/BHS8gGazR1kqvVZcQHNni341Omjv/XWWd1RkjAy/fGVxTXTQISitElBA80K38bF0LCWszrRFP9MYaE3ssyj/wMxAA/hZEz6lS0nozBA0Sdco5AB6v24NzipoAL8nSUQPsDGroJ1zhySV9MIMyS9TB0l7O5u8ao1eBcu84Ekd2bb0yp9YSC85ZgCgTmRHxEp6bVev72YAoW7YTlZVdalYSi+kG2bd14kuk94sOQjA/W2uOsMxpvkszrmHJCeRfLxrhVG89z3JUVoepwulfkhuak645Cwt+dPybmQ9uxI/u5X+aekAuabjjbRJAG40qKlRT2GnzEv6TFkKaCnzFaT6dsR8nhxruY4M0sp2WvDpbumSqqraq0VFLLZYR9iK7sIlrSaTWgD2hxP1HwwA/6qFuvv9/lUyK6qqap+WYtAqAQ0vdDb1IFXP+Gaq9PwoaZUAvcBO8lh4GsW0fa/+jWOad9FYSkDb1ev19mg+m/f+RZJvajaQ5iCHR4Gs/vN4kHALQR8ZclpfE157UN9bHg9SVFRUVFRUVFRUVFQkk+tvTMRj9IMSERwAAAAASUVORK5CYII="/>
    </defs>
    </svg>
    `,
    name: 'statistiques-info'
};