export const appBackIcon = {
    data: `<svg fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 22"><path d="M17.026 22C27.983 10.579 14.7 1.135 6.642 8.691l2.464 2.352H0V2.096l2.232 2.229C17.026-8.878 33.742 11.376 17.026 22z"/></svg>`,
    name: 'back'
};