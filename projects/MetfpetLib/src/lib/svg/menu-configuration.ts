export const appMenuConfigurationIcon = {
    data: `<svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><path d="M6.25 1h17.5C26.65 1 29 3.35 29 6.25v17.5c0 2.9-2.35 5.25-5.25 5.25H6.25A5.25 5.25 0 011 23.75V6.25C1 3.35 3.35 1 6.25 1z" stroke="#0046BA" stroke-width="2"/><path d="M21.875 5.625h-2.5v3.516h2.5V5.625zM21.875 15h2.5v-3.516h-7.5V15h2.5v9.375h2.5V15z" fill="#24C021"/><path d="M10.625 5.625h-2.5V15h2.5V5.625zM10.625 20.86h2.5v-3.516h-7.5v3.515h2.5v3.516h2.5v-3.516z" fill="#BA3232"/></svg>`,
    name: 'menu-configuration'
};