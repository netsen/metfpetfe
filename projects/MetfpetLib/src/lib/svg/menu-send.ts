export const appMenuSendIcon = {
    data: `<svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 17"><path d="M0 8.5l7.792 2.196 4.958-5.738-5.777 4.018-3.054-.851 10.88-5.44-2.814 10.32-2.656-2.365-1.537 2.072v-.545l-1.417-.397V17l3.168-4.272 3.207 2.855L17 0 0 8.5z" fill="#2E9ABC"/></svg>`,
    name: 'menu-send'
};