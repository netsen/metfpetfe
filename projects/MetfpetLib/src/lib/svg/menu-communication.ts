export const appMenuCommunicationIcon = {
    data: `<svg fill="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 38 38"><path d="M19 3.167c8.73 0 15.833 7.103 15.833 15.833S27.731 34.833 19 34.833c-8.73 0-15.833-7.102-15.833-15.833C3.167 10.27 10.27 3.167 19 3.167zM19 0C8.507 0 0 8.507 0 19s8.507 19 19 19 19-8.507 19-19S29.493 0 19 0zm0 19.87l-9.487-7.235h18.973L19 19.871zm0 2.04l-9.5-7.33v10.722h19V14.58L19 21.91z" fill="#DB6813"/></svg>`,
    name: 'menu-communication'
};