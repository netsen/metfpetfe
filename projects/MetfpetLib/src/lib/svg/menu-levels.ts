export const appMenuLevelsIcon = {
    data: `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><circle cx="51.952" cy="101.644" r="51.952" fill="#fe585d"/><circle cx="51.952" cy="255.956" r="51.952" fill="#aac1ce"/><circle cx="51.952" cy="410.356" r="51.952" fill="#25b6d2"/><g fill="#8ba0ae"><path d="M160 85.636h352v32H160zM160 239.396h352v32H160zM160 393.156h352v32H160z"/></g></svg>`,
    name: 'menu-levels'
};