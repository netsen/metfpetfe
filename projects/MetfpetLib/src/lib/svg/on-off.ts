export const appOnOffIcon = {
    data: `<svg height="512" viewBox="0 0 24 24" width="512" xmlns="http://www.w3.org/2000/svg"><g id="on"><path d="m16 6h-8a6 6 0 0 0 0 12h8a6 6 0 0 0 0-12zm-12 6a4 4 0 1 1 4 4 4 4 0 0 1 -4-4zm14 .5a1 1 0 0 1 -2 0v-1a1 1 0 0 1 2 0z"/></g></svg>`,
    name: 'on-off'
};