import { formatDate } from '@angular/common';
import { Injectable } from '@angular/core';
import * as JSZip from 'jszip';
import * as ExcelJS from "exceljs/dist/exceljs.min.js";
import * as FileSaver from 'file-saver';
import { MetfpetServiceAgent } from './metfpet-http.service';
import { FinalExamenStatistiquesRowType } from '../interfaces';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const XLSX_EXTENSION = '.xlsx';
const XLS_EXTENSION = '.xls';
const CSV_EXTENSION = '.csv';
const CSV_TYPE = 'text/plain;charset=utf-8';
const GUPOL_LOGO = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAABbCAMAAAGJ9HgcAAAKQ2lDQ1BJQ0MgcHJvZmlsZQAAeNqdU3dYk/cWPt/3ZQ9WQtjwsZdsgQAiI6wIyBBZohCSAGGEEBJAxYWIClYUFRGcSFXEgtUKSJ2I4qAouGdBiohai1VcOO4f3Ke1fXrv7e371/u855zn/M55zw+AERImkeaiagA5UoU8Otgfj09IxMm9gAIVSOAEIBDmy8JnBcUAAPADeXh+dLA//AGvbwACAHDVLiQSx+H/g7pQJlcAIJEA4CIS5wsBkFIAyC5UyBQAyBgAsFOzZAoAlAAAbHl8QiIAqg0A7PRJPgUA2KmT3BcA2KIcqQgAjQEAmShHJAJAuwBgVYFSLALAwgCgrEAiLgTArgGAWbYyRwKAvQUAdo5YkA9AYACAmUIszAAgOAIAQx4TzQMgTAOgMNK/4KlfcIW4SAEAwMuVzZdL0jMUuJXQGnfy8ODiIeLCbLFCYRcpEGYJ5CKcl5sjE0jnA0zODAAAGvnRwf44P5Dn5uTh5mbnbO/0xaL+a/BvIj4h8d/+vIwCBAAQTs/v2l/l5dYDcMcBsHW/a6lbANpWAGjf+V0z2wmgWgrQevmLeTj8QB6eoVDIPB0cCgsL7SViob0w44s+/zPhb+CLfvb8QB7+23rwAHGaQJmtwKOD/XFhbnauUo7nywRCMW735yP+x4V//Y4p0eI0sVwsFYrxWIm4UCJNx3m5UpFEIcmV4hLpfzLxH5b9CZN3DQCshk/ATrYHtctswH7uAQKLDljSdgBAfvMtjBoLkQAQZzQyefcAAJO/+Y9AKwEAzZek4wAAvOgYXKiUF0zGCAAARKCBKrBBBwzBFKzADpzBHbzAFwJhBkRADCTAPBBCBuSAHAqhGJZBGVTAOtgEtbADGqARmuEQtMExOA3n4BJcgetwFwZgGJ7CGLyGCQRByAgTYSE6iBFijtgizggXmY4EImFINJKApCDpiBRRIsXIcqQCqUJqkV1II/ItchQ5jVxA+pDbyCAyivyKvEcxlIGyUQPUAnVAuagfGorGoHPRdDQPXYCWomvRGrQePYC2oqfRS+h1dAB9io5jgNExDmaM2WFcjIdFYIlYGibHFmPlWDVWjzVjHVg3dhUbwJ5h7wgkAouAE+wIXoQQwmyCkJBHWExYQ6gl7CO0EroIVwmDhDHCJyKTqE+0JXoS+cR4YjqxkFhGrCbuIR4hniVeJw4TX5NIJA7JkuROCiElkDJJC0lrSNtILaRTpD7SEGmcTCbrkG3J3uQIsoCsIJeRt5APkE+S+8nD5LcUOsWI4kwJoiRSpJQSSjVlP+UEpZ8yQpmgqlHNqZ7UCKqIOp9aSW2gdlAvU4epEzR1miXNmxZDy6Qto9XQmmlnafdoL+l0ugndgx5Fl9CX0mvoB+nn6YP0dwwNhg2Dx0hiKBlrGXsZpxi3GS+ZTKYF05eZyFQw1zIbmWeYD5hvVVgq9ip8FZHKEpU6lVaVfpXnqlRVc1U/1XmqC1SrVQ+rXlZ9pkZVs1DjqQnUFqvVqR1Vu6k2rs5Sd1KPUM9RX6O+X/2C+mMNsoaFRqCGSKNUY7fGGY0hFsYyZfFYQtZyVgPrLGuYTWJbsvnsTHYF+xt2L3tMU0NzqmasZpFmneZxzQEOxrHg8DnZnErOIc4NznstAy0/LbHWaq1mrX6tN9p62r7aYu1y7Rbt69rvdXCdQJ0snfU6bTr3dQm6NrpRuoW623XP6j7TY+t56Qn1yvUO6d3RR/Vt9KP1F+rv1u/RHzcwNAg2kBlsMThj8MyQY+hrmGm40fCE4agRy2i6kcRoo9FJoye4Ju6HZ+M1eBc+ZqxvHGKsNN5l3Gs8YWJpMtukxKTF5L4pzZRrmma60bTTdMzMyCzcrNisyeyOOdWca55hvtm82/yNhaVFnMVKizaLx5balnzLBZZNlvesmFY+VnlW9VbXrEnWXOss623WV2xQG1ebDJs6m8u2qK2brcR2m23fFOIUjynSKfVTbtox7PzsCuya7AbtOfZh9iX2bfbPHcwcEh3WO3Q7fHJ0dcx2bHC866ThNMOpxKnD6VdnG2ehc53zNRemS5DLEpd2lxdTbaeKp26fesuV5RruutK10/Wjm7ub3K3ZbdTdzD3Ffav7TS6bG8ldwz3vQfTw91jicczjnaebp8LzkOcvXnZeWV77vR5Ps5wmntYwbcjbxFvgvct7YDo+PWX6zukDPsY+Ap96n4e+pr4i3z2+I37Wfpl+B/ye+zv6y/2P+L/hefIW8U4FYAHBAeUBvYEagbMDawMfBJkEpQc1BY0FuwYvDD4VQgwJDVkfcpNvwBfyG/ljM9xnLJrRFcoInRVaG/owzCZMHtYRjobPCN8Qfm+m+UzpzLYIiOBHbIi4H2kZmRf5fRQpKjKqLupRtFN0cXT3LNas5Fn7Z72O8Y+pjLk722q2cnZnrGpsUmxj7Ju4gLiquIF4h/hF8ZcSdBMkCe2J5MTYxD2J43MC52yaM5zkmlSWdGOu5dyiuRfm6c7Lnnc8WTVZkHw4hZgSl7I/5YMgQlAvGE/lp25NHRPyhJuFT0W+oo2iUbG3uEo8kuadVpX2ON07fUP6aIZPRnXGMwlPUit5kRmSuSPzTVZE1t6sz9lx2S05lJyUnKNSDWmWtCvXMLcot09mKyuTDeR55m3KG5OHyvfkI/lz89sVbIVM0aO0Uq5QDhZML6greFsYW3i4SL1IWtQz32b+6vkjC4IWfL2QsFC4sLPYuHhZ8eAiv0W7FiOLUxd3LjFdUrpkeGnw0n3LaMuylv1Q4lhSVfJqedzyjlKD0qWlQyuCVzSVqZTJy26u9Fq5YxVhlWRV72qX1VtWfyoXlV+scKyorviwRrjm4ldOX9V89Xlt2treSrfK7etI66Trbqz3Wb+vSr1qQdXQhvANrRvxjeUbX21K3nShemr1js20zcrNAzVhNe1bzLas2/KhNqP2ep1/XctW/a2rt77ZJtrWv913e/MOgx0VO97vlOy8tSt4V2u9RX31btLugt2PGmIbur/mft24R3dPxZ6Pe6V7B/ZF7+tqdG9s3K+/v7IJbVI2jR5IOnDlm4Bv2pvtmne1cFoqDsJB5cEn36Z8e+NQ6KHOw9zDzd+Zf7f1COtIeSvSOr91rC2jbaA9ob3v6IyjnR1eHUe+t/9+7zHjY3XHNY9XnqCdKD3x+eSCk+OnZKeenU4/PdSZ3Hn3TPyZa11RXb1nQ8+ePxd07ky3X/fJ897nj13wvHD0Ivdi2yW3S609rj1HfnD94UivW2/rZffL7Vc8rnT0Tes70e/Tf/pqwNVz1/jXLl2feb3vxuwbt24m3Ry4Jbr1+Hb27Rd3Cu5M3F16j3iv/L7a/eoH+g/qf7T+sWXAbeD4YMBgz8NZD+8OCYee/pT/04fh0kfMR9UjRiONj50fHxsNGr3yZM6T4aeypxPPyn5W/3nrc6vn3/3i+0vPWPzY8Av5i8+/rnmp83Lvq6mvOscjxx+8znk98ab8rc7bfe+477rfx70fmSj8QP5Q89H6Y8en0E/3Pud8/vwv94Tz+4A5JREAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAADJGlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNy4xLWMwMDAgNzkuZGFiYWNiYiwgMjAyMS8wNC8xNC0wMDozOTo0NCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIDIzLjAgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkU4MDUyOEJEQ0M3RjExRUM5NkE5OEVDMTAyNEI5OUJCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkU4MDUyOEJFQ0M3RjExRUM5NkE5OEVDMTAyNEI5OUJCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RTgwNTI4QkJDQzdGMTFFQzk2QTk4RUMxMDI0Qjk5QkIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RTgwNTI4QkNDQzdGMTFFQzk2QTk4RUMxMDI0Qjk5QkIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5YkB7DAAABiVBMVEVISEguhOD////wgScAAAD6iBfwgifwgyfxhibxhybxhSbyiyXyiiX0lCPzjiTzjyT0liLyjCUgKzLzkCTxiCb1miH0kyPxhCbyiSX2oR/1mCL0lyL1myH2oh7zkST0lSP1mSH2niD2oB/3ox71nCD2nx/3pR3zkiPyjSX2nSDxhCf3pB7yiCb0kiP0lSLyiSb3ph3zjSXzjST1nCH3pB31nSDzkSMvLy/1lyK56/T2nyBvqur1mSLyjST2ox5XX2Xh7vtEkeMPDw9iouhKSkr2oh/U5vn97OL97uJBQUH+9Ozb6vr0o2vE3Pbyiib+9e7+8umSuOzW5/nL4Pf0kyb4sD5rqOm2zvJbn+f97dyszvP73MHZ5fj6z6a0zfH5zbDw9fxUXGH7zYGdvu3728GPve75zaf83KX+8eCDtu1FkuP96c70nUmgy9NCkOMjLTTp8PtBj+P0pWs+juJ5sOuoxe/2pCc7ieGkyfJsqeknMjj//v71mCHzkTSMtOu11PT3sm7W4/cAAABFb4PWAAAAg3RSTlP/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////ACKHqaAAAA0YSURBVHjaYmgiCjCASUailDESVMkAUqAqzc/ZVAXkMDAABcAkBIHYUBZQmaIAHwdzE1gZA1QKJMGArIyxSZtJnomJiRkqBTUFSRFUmTKTmDtEGcRGqL/Q3SYrqssjDrYU6iCEOkZGRoRPFeQk2OFugyiDeR6hjBHZaJhXsQcvC1GxQBAABBADKaoYCaqCeIWRoFkyYtziQGVIMYkIF1jMNcmKqvNoMTehxHcTUrxDghoYlxLAaGJkQFLIgGowSJUFkwgbwqwm7OEKTECccFVNMBvgiZQRGknyhsBkBnM9NGkwwmMSogoaCIwQ2yDOJjdUiQAAAUipkhyEYRiYpaJFrAKxiaUgQdmEEB/gAVy5cOMveTlO4jhuJU7poWnddOQZT4bvktIkXRHrI1PR/CnPrFuX00EHlpGvo7PdvfYSSoZND2uIud5Nhrd+tz1refFqe4UI59/QgMJ3WgirWpy35eYxL3Kt/eQoBOjfBla0GBJx0weGV7VXPaW+CrAALGN0BO+NacypRo7QxvOwsnIBRdeXrHUkKEVsxEgykB0WVlz0hIQFuY4g10XrF+olmnpAyVs3YsmIFdMa5liOT/ciN5YgYhnKyHCK+XH405fHtyj4kOpV85aJxv8JQHu5pSAMA1E0mQRBUUFUUEE/VEQEH4jrcDNuQGblNjGdR1qhaO2fpF5mkrnnpgbbe/7jx+Cetnq0TUK6SYJTe2IUv60rzNRpdb1Tpzkn8CMnUHMiBGTw9n4Wh9WicKFBbXT2NUrXU+gXf15vF4Px6NwrMvxBA89a9BsrsKGVssfNW2vS6zhZFysypDItsj5xYvPmRNByYr/0q7V18d0o1XVZAjyLMO0Wlx5HyYWmkqYZHtVKvNV43C1hCjAEOIS6vLd8WGJL5DmqRTpHGxENcALog6hLpUbdJenD3eQ2L0fCSd6bHK8EQpvmO2GQWahGgvY+z5kgZtlwpFXxEI+EQ8RaLaxoJUVVV3iFRiIZ3GRDIbVsqZW515AdjzGGIqjVYXEMmbIA7q62LoWeTEukdpOPi7iT/jfkKK761riKtk3e//q8BGDGXLYShoEwnIHKHS8VEBBULqJcRLwdlONS17p24QO49wHMk5ukt8wkVY6iJYuepintx8zkz9+s8ll/jfUGAL8O3uqxFJCbJBcuLgiOruvqV5LDknPROoFAun1IDEtQ3SmX2jraP6wdbJ0W65Wcb6M0FWKRbUUqZBkI3UjU0a/Rp6H7iB/sLm4EVvlsd3OnWM/n9rIZ5BXx0ykIFnXjnNmwDFvAaBI5H7e7nX61uV0+EVilSb4gsNJPmnwbWORF2iYK/gUZQZAYHyVRUQ16jePOVfV2KnJ4WSxVcgVBlfbdgJlEW+RQTJbB0lgeaBK9yTa7FljDqLRUDn2sYOUjLyRVY5QJs92G8ZkllZxplTMbCyPXF/arrBzYufBNWWmdAmcBCMtSVej72VrK5ohZjzzEcpSPGPQeU6qV1XEivWGA5TjOf6qE50devA2AnrCXIofTWip1IeVB0YXRAvgyI6Z8eHqs74KRXb5AvgPHFC15TF9v2o3FsHWv5EEvrbS2Xchiyob0mFYcrzYseA+xiEGjcipVC8vDSKt4jvQzXoWivr5mmdEKkGi0bB+yXMhDM5KHUQbH6vtoIXigX8igJxHgY5lo+VepPMjJ4ISjMcsFQytReGKtm6i24Pva4uDv28gmcyiwniMPTJNo03wSvFXtRoSuBvkH7y8hCaVYnM5Ff3jjZy3OBkqQ+Zwn6U/j3SlfE3dq5m7tsJJunwKwaz6vUUNBHH8vhIpFbVxaQWgptFAED1u0VUTF1l4KnsWDFw8evAgq3hQP85eb9yN58+bHS9oGsoXdw7K7bJIveTPz5vuZrGXdXNbvmUNLycSVta8zmLAhWa/tBDB7clnt8i0mQThTkiT//q43+zNV1myrPtN2ajubLM8Dz6yCIOZiEO2Fv9ev4pfGm/173SRnkbtqyZsCa7P0v2VelhlPoBaj9jNZwN385yBrwbtTeiIzjEZUWYKdxN2pv1mEQUBO4KQzKRbbCJhEk2V0Wf7Kjw+cyWgevdh69jCYjLz7F70z65+HbQhGA9SVc1kfA4Nw3sf7V4dGBFkCY5GNGmimjRIL1o5nHSlCI72sTBe/usKRDA1jw4kNl8VDPuAtkdgAy0QEkgTfX0jCq8aWv66jbqc7fxsMA3P/yuNEC/KRd0tIXyrrSISBMnaTUBUvA1psiTmawQRkKI4O9xiESGiEYjcpxYr1QQGpXCnBbh4kta/Gv//Z/tWBJMhA0rAsvW4pUcAWNMwn37v6fhz4lp+gbr2tqu1W0waCgTkNNCIU1So7B+Uatk5I14Z9x6FTL2u/lfXE36nEt+owjOO7G5fF57/KbkmPwUuRwvn4ZC8s4n7Vve5sdIs4A6RMRPeglXLZeHTa1ocqjy2wQsIZDkvMVLKiLi/LJWJVtYm4vLsph7yKToHJdPn7ibJSxLewy7KMBsavT5X6QAqEkvtcpjvmB25vrygrLuRh7B/up/4hyRKqgikg3fa//xB/ZLLws4jAAVG2+eD+IY1W6OaT7ST0+RnAD6SBKgt/LMqyvax+aodlwThZGdEty/oCOV8WFzFcWJ/4XF+WGFs9Ax+U5X74drr7JvYPS9Q/EHZa2O7Gy+qSqLyI8dfnfSIuY/8A15aF8i09WYgCaoysDx0AJ20NOaRQt/gHKzF3JmuoQAQ/Bq4+PIj14WvA8jXNxKykS1jeUASbpgV0qFGWhW9hrA/dsKBmIc9kieV0IrMPcG5lGG5LsoCbxWnRyPkgGlFlZbvijaYYBZAUwv/nKoCkTN5LWL3hyoXtmtGVnPmsAfj4UdR6uHJrZP0XoJ3z6o0aCALwOrFSSL8UEQgcl4QQQgqQIiAEEE1CggcQQkKCB0CAEAjeKA8IaX85a3ttb5mZHRNHCTnvQ3Iue2d/N21nxndEL6uB1UWwImM0sIKsjJcPG1jE2M1YtVryW0t2q2xVhKXGG9nAghYVtrHKi8Qta0nqnd5lsAw7bjG53cqfRTXPk91j9gUuVHc4ADSlluwG5RQgqjjWZXRnPN+ZHHsJ7M/089jj8mGleQcH1vS5U6fH10dmLpydHBu9MjE0fHJjymbVglZvwhvwUfYUiR2B94PtFV6ikLw0uG5uhVPx9RJW5+LiXAqrrWEtJ7BWBwcHdGtEDsyXLCGIaxJMWOAzDEgTvEArg2FYfl3IuwJB+UEpv8+f72wbsCZzWFODAwrWSvaMDeYSBffWOVOwI0Kgc6rCEoGvS1Cx+plZxaqzuDk3nWnhWiJYVyeGhoY3Mlj9/X15WhyyWNT3LDiag3TqSDB17Os2A5b/qS4gcz6xrFlIYRlauFaYrFUF64SGVdZjMckCttxyX2gKlrYm1LsCLIlJVtAb6ldbC7dcWK7JSmD19uK04O8cpsCEJUnhDUxiSZYkzCzoDWXSYKMEa3b+RmdbaeFNBUsJ1k6ihbbJKsvELG+Id2sEphwULJ4ngWEVd7uUwjIEywwcUpNVaqFFi+MNq7kigVGpqoYht+AgAS7O+ugXxROAS1uXFkAtBEyWQcsWLfRyqvg8OCpjGXjJ2IA/CP4qhL8ezGApVg+Kfp8EVrGhtbCn6LdxaBnPVwWkgVZQhswFQgdGIBK2WL7MOcbmbQprtqcc6yPGhoJlbNmKaL4d4lMIxSGeRMWljhn6hk16MCo27u5u8RGXlwpYSguTfx9SWEoLUz5JK16PpYUaVqwXSgQsKmynHCgRydL+vkI8R8eAFqy4WBIqk5UEDob4tE3JWk3/Jr4QkqwS1jFdSGtY+lZTWCpwKPGMt2c8LXwMwLIl67jVn2Cb9TELHBIUReCQbIzu5YFDJZvFdYi0HwDTae8OI0Xz1faGLqy2hrVcBg4VvCHXT5GG2cEV+aU5+xuPipa1CFjoR8Zh942AXZCQ/8qPKS1Mo6xtfK2zYpl3XpwViEvpMMHtpvW64RiwjB90cw+Dy2PSIhTnpRGpXutcAzMOK74vRCN4IoKGTiXEkCqQsGBF9cGSdnrGDd/3siSpF76H1oZhWPfxbIwfmCPFgTCscmpNsPITuRkHRtahomQJOhTbFywZWf8w2wd3KlD5LAVrM4eVZBzcJGlfgNW/2iw63ezDAoXkFQJLmj8xuk/Jss7NBKs0WXDGIZwprRMWYLOM+/4BujsJGq8aYOl30eWKz4AWprBWcljlzE9VcvA8X4hWgkwNeQoFBxHIyCkP12SzjEKYC2vKCByKFY6a8IhR3aley0ALQaBhQfb5DKIa4iyjbmjtNQKHwmRZJ+jzaW+IHmImvOp5nKr2irTEfhf+y+/XyJE47A3xQ6waoTj0VSLR65A07Ed/eL0OAQMfgiU5C57/q4sGPu+Au2iOBCU6AaJu/YnLLjNNcVzu+okXo7uiMQQb98zmmqbzjx67pmQ1sMKW3Hj5rIFFjvdNH3zzhEUD62DGXyn6SxE0JImeAAAAAElFTkSuQmCC';

export enum ImportExportType {
  Xlsx,
  Xls,
  Cvs
}

@Injectable()
export class ExportService {

  private getCurrentDate(): string {
    return formatDate(new Date(), 'yyyy_MM_dd', 'en-US');
  }

  private getCurrentDateAndTime(): string {
    return formatDate(new Date(), 'yyyy_MM_dd_HH_mm_ss', 'en-US');
  }
  
  private getCurrentYear(): string {
    return new Date().getFullYear().toString();
  }

  public exportStudentsOfCurrentInstitutionToExcel(students: Array<any>, fields: any) {
    let fileName = 'Exportation_Apprenants';
    if (students && students.length > 0 && students[0].Institution) {
      fileName = fileName + "_" + students[0].Institution;
    }
    fileName = fileName + "_" + this.getCurrentDate();
    
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });
    let worksheet = workbook.addWorksheet(fileName);
    let columns = [{ header: 'INA', key: 'identifiantNationalEleve', width: 20 }];
    this._addCommonConditionalColumns(columns, fields);
    
    worksheet.columns = columns;
    worksheet.addRows(students);
    this._addLogoAndCopyrightText(students.length, worksheet, logo);
    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportResultatsAdmissions(admissions: object[], type: ImportExportType) {
    let fileName = "demandes_admission" + new  Date().getTime();
    switch(type) {
      case ImportExportType.Xlsx:
        this._exportResultatsAdmissions(admissions, true, fileName);
        break;
      case ImportExportType.Xls:
        this._exportResultatsAdmissions(admissions, false, fileName);
        break;
      case ImportExportType.Cvs:
        this.exportToCsv(admissions, fileName, 
          ["etudiantName", "identifiantNationalEleve", "anneeAcademiqueName", "institutionName", "programmeName", "numeroPV", "decisionName"], 
          ["Nom et Prénom", "INA", "Année", "Institution", "Programme", "PV", "Statut", "Décision"]);
        break;
    }
  }

  private _exportResultatsAdmissions(admissions: object[], isXlsx: boolean, fileName: string) {
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet('Demandes d’admission');
    let columns = [];
    columns.push({ header: 'Nom et Prénom', key: 'etudiantName', width: 25 });
    columns.push({ header: 'INA', key: 'identifiantNationalEleve', width: 20 });
    columns.push({ header: 'Année', key: 'anneeAcademiqueName', width: 10 });
    columns.push({ header: 'Institution', key: 'institutionName', width: 30 });
    columns.push({ header: 'Programme', key: 'programmeName', width: 30 });
    columns.push({ header: 'PV', key: 'numeroPV', width: 10 });
    columns.push({ header: 'Décision', key: 'decisionName', width: 25 });
    worksheet.columns = columns;
    worksheet.addRows(admissions);
    this._addLogoAndCopyrightText(admissions.length, worksheet, logo);
    if (isXlsx) {
      workbook.xlsx.writeBuffer().then((buffer) => {
        this._saveAs(buffer, `${fileName}${XLSX_EXTENSION}`, EXCEL_TYPE);
      });
    } else {
      workbook.xlsx.writeBuffer().then((buffer) => {
        this._saveAs(buffer, `${fileName}${XLS_EXTENSION}`, EXCEL_TYPE);
      });
    }
  }

  public exportAdmissionApprenants(admissions: any) {
    let fileName = 'Apprenant_uniques_des_admissions' + '_' + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet(fileName);
    let columns = [];
    columns.push({ header: 'Année', key: 'anneeAcademiqueName', width: 10 });
    columns.push({ header: 'Numéro PV Concours', key: 'displayedNumeroPV', width: 20 });
    columns.push({ header: 'Identifiant National Apprenant', key: 'identifiantNationalEleve', width: 20 });
    columns.push({ header: 'Prénoms', key: 'etudiantFirstName', width: 25 });
    columns.push({ header: 'Nom', key: 'etudiantName', width: 25 });
    columns.push({ header: 'Numéro PV Apprenant', key: 'etudiantNumeroPV', width: 20 });
    columns.push({ header: 'Date Naissance', key: 'etudiantDateNaissance', width: 25 });
    columns.push({ header: 'Lieu de naissance', key: 'lieuNaissance', width: 30 });
    columns.push({ header: 'Sexe', key: 'etudiantGenre', width: 25 });
    columns.push({ header: 'Prefecture de Résidence', key: 'etudiantPrefecture', width: 30 });
    columns.push({ header: 'Nom du père', key: 'nomPere', width: 30 });
    columns.push({ header: 'Nom de la mère', key: 'nomMere', width: 30 });
    columns.push({ header: 'Email', key: 'email', width: 30 });
    columns.push({ header: 'Numéro de téléphone', key: 'phone', width: 30 });
    columns.push({ header: 'Statut biométrique', key: 'biometricStatus', width: 20 });

    worksheet.columns = columns;
    worksheet.addRows(admissions);
    this._addLogoAndCopyrightText(admissions.length, worksheet, logo);
    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportAdmissions(admissions: any, fields: any, fileName: string) {
    fileName = fileName + '_' + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet(fileName);

    let columns = [];

    if (fields.exportAnnee) {
      columns.push({ header: 'Année', key: 'anneeAcademiqueName', width: 10 });
    }
    if (fields.exportPv) {
      columns.push({ header: 'Numéro PV Concours', key: 'displayedNumeroPV', width: 20 });
    }
    if (fields.exportINA) {
      columns.push({ header: 'Identifiant National Apprenant', key: 'identifiantNationalEleve', width: 20 });
    }
    if (fields.exportPrenom) {
      columns.push({ header: 'Prénoms', key: 'etudiantFirstName', width: 25 });
    }
    if (fields.exportNom) {
      columns.push({ header: 'Nom', key: 'etudiantName', width: 25 });
    }
    if (fields.exportPvEtudiant) {
      columns.push({ header: 'Numéro PV Apprenant', key: 'etudiantNumeroPV', width: 20 });
    }
    if (fields.exportDateNaissance) {
      columns.push({ header: 'Date Naissance', key: 'etudiantDateNaissance', width: 25 });
    }
    if (fields.exportLieuNaissance) {
      columns.push({ header: 'Lieu de naissance', key: 'lieuNaissance', width: 30 });
    }
    if (fields.exportSexe) {
      columns.push({ header: 'Sexe', key: 'etudiantGenre', width: 25 });
    }
    if (fields.exportPrefectureDeResidence) {
      columns.push({ header: 'Prefecture de Résidence', key: 'etudiantPrefecture', width: 30 });
    }
    if (fields.exportNomPere) {
      columns.push({ header: 'Nom du père', key: 'nomPere', width: 30 });
    }
    if (fields.exportNomMere) {
      columns.push({ header: 'Nom de la mère', key: 'nomMere', width: 30 });
    }
    if (fields.exportEmail) {
      columns.push({ header: 'Email', key: 'email', width: 30 });
    }
    if (fields.exportPhone) {
      columns.push({ header: 'Numéro de téléphone', key: 'phone', width: 30 });
    }
    if (fields.exportBiometricStatus) {
      columns.push({ header: 'Statut biométrique', key: 'biometricStatus', width: 20 });
    }
    if (fields.exportEtudiantLevel) {
      columns.push({ header: 'Type de diplôme', key: 'etudiantLevel', width: 20 });
    }
    if (fields.exportOrginialSchool) {
      columns.push({ header: 'Ecole d’origine', key: 'orginialSchool', width: 30 });
    }
    if (fields.exportCentreConcours) {
      columns.push({ header: 'Centre de concours', key: 'centreConcoursName', width: 30 });
    }
    if (fields.exportChoix) {
      columns.push({ header: 'Choix', key: 'choixAdmissionInstitution', width: 30 });
    }
    if (fields.exportInstitution) {
      columns.push({ header: 'Institution', key: 'institutionName', width: 30 });
    }
    columns.push({ header: 'Statut de l\'institution', key: 'statusEtablissement', width: 30 });
    if (fields.exportFaculte) {
      columns.push({ header: 'Département', key: 'faculteName', width: 30 });
    }
    if (fields.exportProgramme) {
      columns.push({ header: 'Programme', key: 'programmeName', width: 30 });
    }
    if (fields.exportTypeAdmissionName) {
      columns.push({ header: 'Type d\'admission', key: 'typeAdmissionName', width: 30 });
    }
    if (fields.exportNiveauAccesName) {
      columns.push({ header: 'Institution Niveau accès', key: 'niveauAccesName', width: 30 });
    }
    if (fields.exportDecision) {
      columns.push({ header: 'Décision', key: 'decisionName', width: 25 });
    }
    if (fields.exportConcoursStatutGlobal)
    {
      columns.push({ header: 'Concours Statut global', key: 'concoursStatutGlobal', width: 25 });
    }
    if (fields.exportConcoursNotesMoyenne)
    {
      columns.push({ header: 'Concours Notes moyenne', key: 'concoursNotesMoyenne', width: 25 });
    }

    worksheet.columns = columns;
    worksheet.addRows(admissions);
    this._addLogoAndCopyrightText(admissions.length, worksheet, logo);
    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportInstitutions(institutions: any, fields: any) {
    let fileName = "Institutions_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet(fileName);

    let columns = [];

    if (fields.exportNom) {
      columns.push({ header: 'Nom', key: 'name', width: 20 });
    }

    if (fields.exportDescription) {
      columns.push({ header: 'Description', key: 'description', width: 20 });
    }

    if (fields.exportPrefecture) {
      columns.push({ header: 'Préfecture', key: 'prefectureName', width: 20 });
    }

    if (fields.exportTypeInstitution) {
      columns.push({ header: 'Type d\'institution', key: 'typeInstitutionName', width: 20 });
    }

    if (fields.exportStatusEtablissement) {
      columns.push({ header: 'Statut de l\'institution', key: 'statusEtablissementName', width: 20 });
    }

    if (fields.exportStatut) {
      columns.push({ header: 'Statut actif', key: 'statusName', width: 20 });
    }

    worksheet.columns = columns;
    worksheet.addRows(institutions);
    this._addLogoAndCopyrightText(institutions.length, worksheet, logo); 
    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportProgrammes(programmes: any, fields: any) {
    let fileName = "Programmes_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet(fileName);

    let columns = [];

    if (fields.exportNom) {
      columns.push({ header: 'Nom', key: 'name', width: 30 });
    }

    if (fields.exportDescription) {
      columns.push({ header: 'Description', key: 'description', width: 30 });
    }

    if (fields.exportDuree) {
      columns.push({ header: 'Durée en années', key: 'duree', width: 20 });
    }

    if (fields.exportInstitution) {
      columns.push({ header: 'Institution', key: 'institutionName', width: 30 });
    }

    if (fields.exportFaculte) {
      columns.push({ header: 'Département', key: 'faculteName', width: 20 });
    }

    if (fields.exportTypeProgramme) {
      columns.push({ header: 'Type Programme', key: 'typeProgrammeName', width: 20 });
    }

    if (fields.exportTypeAdmission) {
      columns.push({ header: 'Type d\'admission', key: 'typeAdmissionName', width: 20 });
    }

    if (fields.exportTypeDiplome) {
      columns.push({ header: 'Type de diplôme', key: 'typeDiplomeName', width: 20 });
    }

    if (fields.exportNiveauAcces) {
      columns.push({ header: 'Niveau accès', key: 'niveauAccesName', width: 20 });
    }

    if (fields.exportTypeExamen) {
      columns.push({ header: 'Type d’examen', key: 'typeExamenName', width: 20 });
    }

    if (fields.exportMatieresExamen) {
      columns.push({ header: 'Matières examen de sortie', key: 'matieresExamen', width: 30 });
    }

    if (fields.exportStatut) {
      columns.push({ header: 'Statut actif', key: 'statusName', width: 20 });
    }

    worksheet.columns = columns;
    worksheet.addRows(programmes);
    this._addLogoAndCopyrightText(programmes.length, worksheet, logo); 
    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportFinalExamenMatieres(matieres: any) {
    let fileName = "FinalExamenMatieres_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });
    let worksheet = workbook.addWorksheet(fileName);
    let columns = [];
    columns.push({ header: 'Programme', key: 'programme', width: 25});
    columns.push({ header: 'Institution', key: 'institution', width: 25 });
    columns.push({ header: 'Sujet1', key: 'subject1', width: 25 });
    columns.push({ header: 'Sujet2', key: 'subject2', width: 25 });
    columns.push({ header: 'Sujet3', key: 'subject3', width: 25 });
    columns.push({ header: 'Sujet4', key: 'subject4', width: 25 });
    columns.push({ header: 'Sujet5', key: 'subject5', width: 25 });
    columns.push({ header: 'Sujet6', key: 'subject6', width: 25 });
    columns.push({ header: 'Sujet7', key: 'subject7', width: 25 });
    columns.push({ header: 'Sujet8', key: 'subject8', width: 25 });
    columns.push({ header: 'Sujet9', key: 'subject9', width: 25 });
    columns.push({ header: 'Sujet10', key: 'subject10', width: 25 });
    columns.push({ header: 'Sujet11', key: 'subject11', width: 25 });
    columns.push({ header: 'Sujet12', key: 'subject12', width: 25 });
    columns.push({ header: 'Sujet13', key: 'subject13', width: 25 });
    columns.push({ header: 'Sujet14', key: 'subject14', width: 25 });
    columns.push({ header: 'Sujet15', key: 'subject15', width: 25 });

    worksheet.columns = columns;
    worksheet.addRows(matieres);
    this._addLogoAndCopyrightText(matieres.length, worksheet, logo); 
    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportCentreConcours(centreConcours: any, fields: any) {
    let fileName = "CentreConcours_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet(fileName);

    let columns = [];

    if (fields.exportNom) {
      columns.push({ header: 'Nom', key: 'name', width: 20 });
    }

    if (fields.exportNiveauAcces) {
      columns.push({ header: 'Niveau accès', key: 'niveauAccesName', width: 20 });
    }

    if (fields.exportRegion) {
      columns.push({ header: 'Région', key: 'regionName', width: 20 });
    }
    
    if (fields.exportPrefecture) {
      columns.push({ header: 'Préfecture', key: 'prefectureName', width: 20 });
    }

    if (fields.exportProgramme) {
      columns.push({ header: "Programmes d'Institutions", key: 'programName', width: 30 });
    }

    if (fields.exportStatut) {
      columns.push({ header: 'Statut', key: 'statusName', width: 20 });
    }

    worksheet.columns = columns;
    worksheet.addRows(centreConcours);
    this._addLogoAndCopyrightText(centreConcours.length, worksheet, logo); 
    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportBoursePayments(payments: any) {
    let fileName = "Paiements_Bourses_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });
    let worksheet = workbook.addWorksheet(fileName);
    let columns = [];
    columns.push({ header: 'Identifiant National Apprenant', key: 'etudiantINA', width: 25});
    columns.push({ header: 'Prénom', key: 'etudiantFirstName', width: 25 });
    columns.push({ header: 'Nom', key: 'etudiantName', width: 25 });
    columns.push({ header: 'Email', key: 'email', width: 25 });
    columns.push({ header: 'Numéro de téléphone', key: 'phone', width: 25 });
    columns.push({ header: 'Date Naissance', key: 'etudiantDateNaissance', width: 25 });
    columns.push({ header: 'Sexe', key: 'etudiantGenre', width: 25 });
    columns.push({ header: 'Préfecture', key: 'etudiantInstitutionPrefectureName', width: 25 });
    columns.push({ header: 'Institution', key: 'etudiantInstitutionName', width: 25 });
    columns.push({ header: 'Session', key: 'etudiantBourseSessionName', width: 25 });
    columns.push({ header: 'Mois', key: 'moisAnneeName', width: 25 });
    columns.push({ header: 'Montant', key: 'montant', width: 25 });
    columns.push({ header: 'Statut', key: 'statusName', width: 25 });
    columns.push({ header: 'Date de paiement', key: 'datePaiement', width: 25 });
    columns.push({ header: 'Nº téléphone authentifié', key: 'authenticatedPhoneNumber', width: 25 });
    columns.push({ header: 'Comptes Écobank', key: 'ecobankAcountNumber', width: 25 });
    columns.push({ header: 'Redoublant ?', key: 'redoublant', width: 25 });

    worksheet.columns = columns;
    worksheet.addRows(payments);
    this._addLogoAndCopyrightText(payments.length, worksheet, logo); 
    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportPayments(payments: any, fields: any, fileNamePrefix: string) {
    let fileName = fileNamePrefix + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet(fileName);

    let columns = [];

    if (fields.exportEtudiantNom) {
      columns.push({ header: 'Nom et Prénom', key: 'etudiantName', width: 25 });
    }

    if (fields.exportINA) {
      columns.push({ header: 'Identifiant National Apprenant', key: 'identifiantNationalEleve', width: 30 });
    }

    if (fields.exportOperateur) {
      columns.push({ header: 'Opérateur', key: 'operateur', width: 20 });
    }

    if (fields.exportStatut) {
      columns.push({ header: 'Statut', key: 'statusName', width: 22 });
    }

    if (fields.exportDateTransaction) {
      columns.push({ header: 'Date de transaction', key: 'transactionTime', width: 22 });
    }

    if (fields.exportMontant) {
      columns.push({ header: 'Montant', key: 'amount', width: 20 });
    }

    if (fields.exportNumeroTransaction) {
      columns.push({ header: 'Numéro de transaction', key: 'transactionId', width: 25 });
    }

    if (fields.exportConfirmationCode) {
      columns.push({ header: 'Code de confirmation', key: 'notificationToken', width: 40 });
    }

    if (fields.exportInstitution) {
      columns.push({ header: "Institution", key: 'institutionName', width: 30 });
    }

    if (fields.exportStatusEtablissement) {
      columns.push({ header: "Statut de l'institution", key: 'institutionStatusEtablissement', width: 40 });
    }

    worksheet.columns = columns;
    worksheet.addRows(payments);
    this._addLogoAndCopyrightText(payments.length, worksheet, logo); 
    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }
  
  public exportInscriptionProgrammes(inscriptionProgrammes: object[], fields: any) {
    let fileName = "Inscription_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet(fileName);

    let columns = [];

    if (fields.exportIna) {
      columns.push({ header: 'INA', key: 'identifiantNationalEleve', width: 20 });
    }

    if (fields.exportEtudiantNom) {
      columns.push({ header: 'Nom et Prénom', key: 'etudiantName', width: 25 });
    }

    if (fields.exportMatricule) {
      columns.push({ header: 'Matricule', key: 'matricule', width: 20 });
    }

    if (fields.exportPeriode) {
      columns.push({ header: 'Période', key: 'anneeAcademiqueName', width: 20 });
    }

    if (fields.exportProgramme) {
      columns.push({ header: 'Programme', key: 'programmeName', width: 20 });
    }

    if (fields.exportNiveau) {
      columns.push({ header: 'Niveau', key: 'niveauEtudeName', width: 20 });
    }

    if (fields.exportType) {
      columns.push({ header: 'Type', key: 'typeName', width: 20 });
    }

    if (fields.exportFormation) {
      columns.push({ header: 'Formation', key: 'typeFormationName', width: 20 });
    }

    if (fields.exportStatut) {
      columns.push({ header: 'Statut', key: 'statusName', width: 20 });
    }

    if (fields.exportNomPere) {
      columns.push({ header: 'Nom du père', key: 'nomPere', width: 20 });
    }

    if (fields.exportNomMere) {
      columns.push({ header: 'Nom de la mère', key: 'nomMere', width: 20 });
    }

    if (fields.exportPhone) {
      columns.push({ header: 'Numéro de téléphone', key: 'phone', width: 20 });
    }

    if (fields.exportEmail) {
      columns.push({ header: 'Email', key: 'email', width: 20 });
    }

    if (fields.exportGenre) {
      columns.push({ header: 'Sexe', key: 'genre', width: 20 });
    }

    worksheet.columns = columns;
    worksheet.addRows(inscriptionProgrammes);
    this._addLogoAndCopyrightText(inscriptionProgrammes.length, worksheet, logo); 
    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportInscriptions(inscriptions: object[], type: ImportExportType) {
    let fileName = "inscriptions" + new  Date().getTime();
    switch(type) {
      case ImportExportType.Xlsx:
        this._exportInscriptions(inscriptions, true, fileName);
        break;
      case ImportExportType.Xls:
        this._exportInscriptions(inscriptions, false, fileName);
        break;
      case ImportExportType.Cvs:
        this.exportToCsv(inscriptions, fileName, 
          ["identifiantNationalEleve", "matricule", "etudiantName", "institutionName", "localisation", "programmeName", "anneeAcademiqueName", "niveauEtudeName", "frais", "statusName"], 
          ["INA", "Matricule", "Prénom & Nom", "Institution", "Localisation", "Programme", "Période", "Niveau", "Frais", "Statut"]);
        break;
    }
  }

  public exportUserActivities(inscriptions: object[]) {
    let fileName = "Logs" + new  Date().getTime();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet('Journaux d’évènements');
    let columns = [];
    columns.push({ header: 'INA', key: 'identifiantNationalEleve', width: 20 });
    columns.push({ header: 'Date', key: 'actionDate', width: 20 });
    columns.push({ header: 'Type d’action', key: 'actionType', width: 50 });
    columns.push({ header: 'Fait par', key: 'userName', width: 20 });
    columns.push({ header: 'Description de l’action', key: 'details', width: 50 });
    worksheet.columns = columns;
    worksheet.addRows(inscriptions);
    this._addLogoAndCopyrightText(inscriptions.length, worksheet, logo);
    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAs(buffer, `${fileName}${XLSX_EXTENSION}`, EXCEL_TYPE);
    });
  }

  private _exportInscriptions(inscriptions: object[], isXlsx: boolean, fileName: string) {
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet('Demandes d’admission');
    let columns = [];
    columns.push({ header: 'INA', key: 'identifiantNationalEleve', width: 20 });
    columns.push({ header: 'PV', key: 'numeroPV', width: 20 });
    columns.push({ header: 'Matricule', key: 'matricule', width: 15 });
    columns.push({ header: 'Prénom & Nom', key: 'etudiantName', width: 25 });
    columns.push({ header: 'Institution', key: 'institutionName', width: 30 });
    columns.push({ header: 'Localisation', key: 'localisation', width: 30 });
    columns.push({ header: 'Programme', key: 'programmeName', width: 30 });
    columns.push({ header: 'Période', key: 'anneeAcademiqueName', width: 10 });
    columns.push({ header: 'Niveau', key: 'niveauEtudeName', width: 20 });
    columns.push({ header: 'Frais', key: 'frais', width: 10 });
    columns.push({ header: 'Statut', key: 'statusName', width: 25 });
    columns.push({ header: 'Email', key: 'email', width: 25 });
    columns.push({ header: 'Numéro de téléphone', key: 'phone', width: 25 });
    columns.push({ header: 'Date Naissance', key: 'dateNaissance', width: 25 });
    columns.push({ header: 'Lieu de naissance', key: 'lieuNaissance', width: 30 });
    columns.push({ header: 'Nom du père', key: 'nomPere', width: 30 });
    columns.push({ header: 'Nom de la mère', key: 'nomMere', width: 30 });
    columns.push({ header: 'Sexe', key: 'genre', width: 20 });
    columns.push({ header: 'Statut biométrique', key: 'biometricStatus', width: 20 });
    columns.push({ header: 'Régime', key: 'regime', width: 20 });
    worksheet.columns = columns;
    worksheet.addRows(inscriptions);
    this._addLogoAndCopyrightText(inscriptions.length, worksheet, logo);
    if (isXlsx) {
      workbook.xlsx.writeBuffer().then((buffer) => {
        this._saveAs(buffer, `${fileName}${XLSX_EXTENSION}`, EXCEL_TYPE);
      });
    } else {
      workbook.xlsx.writeBuffer().then((buffer) => {
        this._saveAs(buffer, `${fileName}${XLS_EXTENSION}`, EXCEL_TYPE);
      });
    }
  }

  public exportLesTemplatesDeNotes(data: MetfpetServiceAgent.ExportFinalExamenMatiereInfo) {
    let fileName = 'Exportation_TemplatesDeNotes_' + data.programmeName + "_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();

    let worksheet = workbook.addWorksheet(data.programmeName);
    let columns = [];
    columns.push({ header: 'PV', key: 'numeroPV', width: 20 });
    columns.push({ header: 'INA', key: 'identifiantNationalEleve', width: 20 });
    columns.push({ header: 'Prénom & Nom', key: 'etudiantName', width: 30 });
    for (let matiere of data.finalExamenMatieres) {
      columns.push({ header: matiere.name, key: matiere.name, width: 20 });
    }
    columns.push({ header: 'Institution', key: 'institutionName', width: 30 });
    columns.push({ header: 'Programme', key: 'programmeName', width: 30 });
    worksheet.columns = columns;
    worksheet.addRows(data.finalExamenEtudiantInfos);

    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public async exportAllLesTemplatesDeNotes(data: Array<MetfpetServiceAgent.ExportFinalExamenMatiereInfo>) {
    const zip = new JSZip();  
    const fileZipName =  'Exportation_TemplatesDeNotes_Tous' + "_" + this.getCurrentDate();

    for (let matiereInfo of data) {
      let fileName = 'Exportation_TemplatesDeNotes_' + matiereInfo.programmeName + "_" + this.getCurrentDate() + ".xlsx";
      let workbook = new ExcelJS.Workbook();

      let worksheet = workbook.addWorksheet(matiereInfo.programmeName);
      let columns = [];
      columns.push({ header: 'PV', key: 'numeroPV', width: 20 });
      columns.push({ header: 'INA', key: 'identifiantNationalEleve', width: 20 });
      columns.push({ header: 'Prénom & Nom', key: 'etudiantName', width: 30 });
      for (let matiere of matiereInfo.finalExamenMatieres) {
        columns.push({ header: matiere.name, key: matiere.name, width: 20 });
      }
      columns.push({ header: 'Institution', key: 'institutionName', width: 30 });
      columns.push({ header: 'Programme', key: 'programmeName', width: 30 });
      worksheet.columns = columns;
      worksheet.addRows(matiereInfo.finalExamenEtudiantInfos);

      let buffer = await workbook.xlsx.writeBuffer();
      let dataFile: Blob = new Blob([buffer], {type: EXCEL_TYPE});
      zip.file(fileName, dataFile);
    }

    zip.generateAsync({ type: 'blob' }).then((content) => {
      if (content) {
        FileSaver.saveAs(content, fileZipName);
      }
    });
  }

  public exportConcoursLesTemplatesDeNotes(data: MetfpetServiceAgent.ExportConcoursMatiereInfo) {
    let fileName = 'Exportation_TemplatesDeNotes_' + data.concoursName + "_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();

    let worksheet = workbook.addWorksheet(data.concoursName);
    let columns = [];
    columns.push({ header: 'INA', key: 'identifiantNationalEleve', width: 20 });
    columns.push({ header: 'PV', key: 'numeroPV', width: 20 });
    columns.push({ header: 'Nom du concours', key: 'concoursName', width: 30 });
    for (let matiere of data.concoursMatieres) {
      columns.push({ header: matiere.name, key: matiere.name, width: 20 });
    }
    worksheet.columns = columns;
    worksheet.addRows(data.concoursEtudiantInfos);

    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportListeDesConcoursCandidats(data: any) {
    let fileName = "ListeDesConcoursCandidats" + "_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet('Liste des candidats');
    let columns = [];
    columns.push({ header: 'INA', key: 'identifiantNationalEleve', width: 20 });
    columns.push({ header: 'Prénom', key: 'etudiantFirstName', width: 20 });
    columns.push({ header: 'Nom', key: 'etudiantName', width: 20 });
    columns.push({ header: 'Sexe', key: 'etudiantGenre', width: 10 });
    columns.push({ header: 'Date de naissance', key: 'etudiantDateNaissance', width: 20 });
    columns.push({ header: 'Lieu de naissance', key: 'lieuNaissance', width: 20 });
    columns.push({ header: 'Nom du père', key: 'nomPere', width: 30 });
    columns.push({ header: 'Nom de la mère', key: 'nomMere', width: 30 });
    columns.push({ header: 'Nom examen', key: 'concoursName', width: 30 });
    columns.push({ header: 'Période', key: 'anneeAcademiqueName', width: 10 });
    columns.push({ header: 'Niveau', key: 'niveauAcces', width: 20 });
    columns.push({ header: 'PV', key: 'numeroPV', width: 10 });
    columns.push({ header: 'Centre', key: 'centreConcoursName', width: 25 });
    worksheet.columns = columns;
    worksheet.addRows(data);
    this._addLogoAndCopyrightText(data.length, worksheet, logo);

    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public async exportAllConcoursLesTemplatesDeNotes(data: Array<MetfpetServiceAgent.ExportConcoursMatiereInfo>) {
    const zip = new JSZip();  
    const fileZipName =  'Exportation_TemplatesDeNotes_Tous' + "_" + this.getCurrentDate();

    for (let item of data) {
      let fileName = 'Exportation_TemplatesDeNotes_' + item.concoursName + "_" + this.getCurrentDate() + ".xlsx";
      let workbook = new ExcelJS.Workbook();
      let worksheet = workbook.addWorksheet(item.concoursName);
      let columns = [];
      columns.push({ header: 'INA', key: 'identifiantNationalEleve', width: 20 });
      columns.push({ header: 'PV', key: 'numeroPV', width: 20 });
      columns.push({ header: 'Nom du concours', key: 'concoursName', width: 30 });
      for (let matiere of item.concoursMatieres) {
        columns.push({ header: matiere.name, key: matiere.name, width: 20 });
      }
      worksheet.columns = columns;
      worksheet.addRows(item.concoursEtudiantInfos);

      let buffer = await workbook.xlsx.writeBuffer();
      let dataFile: Blob = new Blob([buffer], {type: EXCEL_TYPE});
      zip.file(fileName, dataFile); 
    }

    zip.generateAsync({ type: 'blob' }).then((content) => {  
      if (content) {  
        FileSaver.saveAs(content, fileZipName);  
      }  
    }); 
  }

  public exportListeDesProposerOffres(data: any) {
    let fileName = "ListeDesProposerOffres" + "_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet('Liste des proposer offres');
    let columns = [];
    columns.push({ header: 'Année', key: 'anneeAcademiqueName', width: 20 });
    columns.push({ header: 'Numéro PV', key: 'numeroPV', width: 10 });
    columns.push({ header: 'Identifiant National Apprenant', key: 'identifiantNationalEtudiant', width: 10 });
    columns.push({ header: 'Prénoms', key: 'firstName', width: 20 });
    columns.push({ header: 'Nom', key: 'name', width: 20 });
    columns.push({ header: 'Date Naissance', key: 'dateNaissance', width: 20 });
    columns.push({ header: 'Sexe', key: 'genre', width: 20 });
    columns.push({ header: 'Centre Concours', key: 'centreConcoursName', width: 25 });
    columns.push({ header: 'Nom examen', key: 'concoursName', width: 30 });
    columns.push({ header: 'Notes moyenne', key: 'moyenneTotal', width: 10 });
    columns.push({ header: 'Institution', key: 'institution', width: 30 });
    columns.push({ header: 'Programme', key: 'programme', width: 30 });
    columns.push({ header: 'Décision', key: 'decision', width: 10 });
    worksheet.columns = columns;
    worksheet.addRows(data);
    this._addLogoAndCopyrightText(data.length, worksheet, logo);

    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportListeDesEnFilAttente(data: any) {
    let fileName = "ListeDesEnFilAttente" + "_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet('En fil d’attente');
    let columns = [];
    columns.push({ header: 'INA', key: 'identifiantNationalEleve', width: 20 });
    columns.push({ header: 'Prénom', key: 'etudiantFirstName', width: 20 });
    columns.push({ header: 'Nom', key: 'etudiantName', width: 20 });
    columns.push({ header: 'Nom examen', key: 'concoursName', width: 30 });
    columns.push({ header: 'Période', key: 'anneeAcademiqueName', width: 10 });
    columns.push({ header: 'Niveau', key: 'niveauAcces', width: 20 });
    columns.push({ header: 'PV', key: 'numeroPV', width: 10 });
    columns.push({ header: 'Centre', key: 'centreConcoursName', width: 25 });
    columns.push({ header: 'Notes moyenne', key: 'moyenneTotal', width: 25 });
    worksheet.columns = columns;
    worksheet.addRows(data);
    this._addLogoAndCopyrightText(data.length, worksheet, logo);

    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportListeDesFinalExamenCandidats(data: any) {
    let fileName = "ListeDesFinalExamenCandidats" + "_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet('Liste des candidats');
    let columns = [];
    columns.push({ header: 'INA', key: 'identifiantNationalEleve', width: 20 });
    columns.push({ header: 'Prénom', key: 'etudiantFirstName', width: 20 });
    columns.push({ header: 'Nom', key: 'etudiantName', width: 20 });
    columns.push({ header: 'Sexe', key: 'genreName', width: 20 });
    columns.push({ header: 'Date de naissance', key: 'dateNaissance', width: 20 });
    columns.push({ header: 'Lieu', key: 'lieuNaissance', width: 20 });
    columns.push({ header: 'Nom du père', key: 'nomPere', width: 20 });
    columns.push({ header: 'Nom de la mère', key: 'nomMere', width: 20 });
    columns.push({ header: 'Institution', key: 'institutionName', width: 30 });
    columns.push({ header: 'Programme', key: 'programmeName', width: 30 });
    columns.push({ header: 'Type d\'examen', key: 'typeExamenName', width: 20 });
    columns.push({ header: 'Période', key: 'anneeAcademiqueName', width: 10 });
    columns.push({ header: 'Niveau', key: 'niveauEtudeName', width: 20 });
    columns.push({ header: 'PV', key: 'numeroPV', width: 10 });
    columns.push({ header: 'Centre', key: 'finalExamenCentreName', width: 25 });
    columns.push({ header: 'Email', key: 'etudiantEmail', width: 25 });
    columns.push({ header: 'Phone', key: 'etudiantPhone', width: 25 });
    worksheet.columns = columns;
    worksheet.addRows(data);
    this._addLogoAndCopyrightText(data.length, worksheet, logo);

    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportSurLesResultats(data: any) {
    let fileName = "SurLesResultats" + "_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet('Rapport sur les résultats');
    let columns = [];
    columns.push({ header: 'INA', key: 'identifiantNationalEleve', width: 20 });
    columns.push({ header: 'Prénom', key: 'etudiantFirstName', width: 20 });
    columns.push({ header: 'Nom', key: 'etudiantName', width: 20 });
    columns.push({ header: 'Sexe', key: 'genreName', width: 20 });
    columns.push({ header: 'Institution', key: 'institutionName', width: 30 });
    columns.push({ header: 'Statut d’institution', key: 'statusEtablissementName', width: 20 });
    columns.push({ header: 'Programme', key: 'programmeName', width: 30 });
    columns.push({ header: 'Type de diplôme', key: 'typeDiplomeName', width: 20 });
    columns.push({ header: 'Période', key: 'anneeAcademiqueName', width: 10 });
    columns.push({ header: 'Niveau', key: 'niveauEtudeName', width: 20 });
    columns.push({ header: 'PV', key: 'numeroPV', width: 10 });
    columns.push({ header: 'Centre', key: 'finalExamenCentreName', width: 25 });
    columns.push({ header: 'Notes moyenne', key: 'displayedMoyenneTotal', width: 25 });
    columns.push({ header: 'Statut résultats', key: 'statusResultName', width: 25 });
    columns.push({ header: 'Statut global', key: 'statusGlobalName', width: 25 });
    worksheet.columns = columns;
    worksheet.addRows(data);
    this._addLogoAndCopyrightText(data.length, worksheet, logo);

    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportListeNotesMoyenne(data: any) {
    let fileName = "ListeNotesMoyenneParConcours" + "_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet('Liste Notes moyenne');
    let columns = [];
    columns.push({ header: 'INA', key: 'identifiantNationalEleve', width: 20 });
    columns.push({ header: 'Prénom', key: 'etudiantFirstName', width: 20 });
    columns.push({ header: 'Nom', key: 'etudiantName', width: 20 });
    columns.push({ header: 'Nom examen', key: 'concoursName', width: 30 });
    columns.push({ header: 'Période', key: 'anneeAcademiqueName', width: 10 });
    columns.push({ header: 'Niveau', key: 'niveauAcces', width: 20 });
    columns.push({ header: 'PV', key: 'numeroPV', width: 10 });
    columns.push({ header: 'Centre', key: 'centreConcoursName', width: 25 });
    columns.push({ header: 'Notes moyenne', key: 'moyenneTotal', width: 25 });
    columns.push({ header: 'Résultat', key: 'resultName', width: 25 });
    worksheet.columns = columns;
    worksheet.addRows(data);

    this._addLogoAndCopyrightText(data.length, worksheet, logo);

    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportListeDesInstitutions(data: any) {
    let fileName = "ListeDesInstitutionsEtProgrammes" + "_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet('Liste des institutions et programmes avec statut de quotas');
    let columns = [];
    columns.push({ header: 'Institution', key: 'institutionName', width: 20 });
    columns.push({ header: 'Préfecture', key: 'prefectureName', width: 20 });
    columns.push({ header: "Statut de l'institution", key: 'statusEtablissementName', width: 20 });
    columns.push({ header: 'Programme', key: 'name', width: 30 });
    columns.push({ header: "Type d'admission", key: 'typeAdmissionName', width: 10 });
    columns.push({ header: 'Niveau accès', key: 'niveauAccesName', width: 20 });
    columns.push({ header: 'Nombre de places limite', key: 'limitePlace', width: 10 });
    columns.push({ header: 'Nombre Places Occupées', key: 'nombrePlacesOccupees', width: 25 });
    worksheet.columns = columns;
    worksheet.addRows(data);

    this._addLogoAndCopyrightText(data.length, worksheet, logo);

    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  private writeReportCellValue(worksheet: ExcelJS.Worksheet, cellPosition: string, value: string, 
    hasBorder: boolean, isBold: boolean, isAlignLeft: boolean, isLargeSize: boolean): any {
    var cell = worksheet.getCell(cellPosition);
    cell.value = value;
    if (isBold)
    {
      cell.font = {
        bold: true
      };
    }
    if (isAlignLeft)
    {
      cell.alignment = { vertical: 'middle', horizontal: 'left' };
    }
    else
    {
      cell.alignment = { vertical: 'middle', horizontal: 'center' };
    }
    if (hasBorder)
    {
      cell.border = {
        top: {style:'thin'},
        left: {style:'thin'},
        bottom: {style:'thin'},
        right: {style:'thin'}
      };
    }
    if (isLargeSize)
    {
      cell.font.size = 12;
    }
    return cell;
  }

  public exportFinalExamenStatistiques(rows: Array<MetfpetServiceAgent.FinalExamenStatistiquesInfo>, isBeforePublish: boolean) {
    let fileName = "STATISTIQUE DES EXAMENS DE SORTIE SESSION " + this.getCurrentYear() + "_" + this.getCurrentDate();
    if (isBeforePublish)
    {
      fileName = fileName + '(pré publication)';
    }
    let workbook = new ExcelJS.Workbook();
    let worksheet = workbook.addWorksheet('VRAIE STATISTIQUE');

    // header
    worksheet.getCell('A1').value = 'MET-FP';
    worksheet.getCell('A2').value = 'SECS-P';
    worksheet.getCell('J1').value = 'REPUBLIQUE DE GUINEE';
    worksheet.getCell('J2').value = 'Travail-Justice-Solidarité';

    const programNameColumn = worksheet.getColumn('B');
    programNameColumn.width = 50;

    worksheet.mergeCells('A4:L4');
    var title = 'STATISTIQUE DES CANDIDATS DE L\'EXAMEN DE SORTIE SESSION ' + this.getCurrentYear() + ' PAR OPTION ET PAR FILIERES DE FORMATION';
    this.writeReportCellValue(worksheet, 'A4', title, false, true, false, false);

    // Table header
    worksheet.mergeCells('A6:A8');
    this.writeReportCellValue(worksheet, 'A6', 'N°', true, true, false, false);
    worksheet.mergeCells('B6:B8');
    this.writeReportCellValue(worksheet, 'B6', 'OPTIONS', true, true, false, false);
    worksheet.mergeCells('C6:F6');
    this.writeReportCellValue(worksheet, 'C6', 'Candidats', true, true, false, false);
    worksheet.mergeCells('G6:J6');
    this.writeReportCellValue(worksheet, 'G6', 'Admis', true, true, false, false);
    worksheet.mergeCells('K6:L6');
    this.writeReportCellValue(worksheet, 'K6', '% Admis', true, true, false, false);
    worksheet.mergeCells('C7:D7');
    this.writeReportCellValue(worksheet, 'C7', 'Publiques', true, true, false, false);
    worksheet.mergeCells('E7:F7');
    this.writeReportCellValue(worksheet, 'E7', 'Privées', true, true, false, false);
    worksheet.mergeCells('G7:H7');
    this.writeReportCellValue(worksheet, 'G7', 'Publiques', true, true, false, false);
    worksheet.mergeCells('I7:J7');
    this.writeReportCellValue(worksheet, 'I7', 'Privées', true, true, false, false);
    worksheet.mergeCells('K7:K8');
    this.writeReportCellValue(worksheet, 'K7', 'Publiques', true, true, false, false);
    worksheet.mergeCells('L7:L8');
    this.writeReportCellValue(worksheet, 'L7', 'Privées', true, true, false, false);

    this.writeReportCellValue(worksheet, 'C8', 'T', true, true, false, false);
    this.writeReportCellValue(worksheet, 'D8', 'F', true, true, false, false);
    this.writeReportCellValue(worksheet, 'E8', 'T', true, true, false, false);
    this.writeReportCellValue(worksheet, 'F8', 'F', true, true, false, false);
    this.writeReportCellValue(worksheet, 'G8', 'T', true, true, false, false);
    this.writeReportCellValue(worksheet, 'H8', 'F', true, true, false, false);
    this.writeReportCellValue(worksheet, 'I8', 'T', true, true, false, false);
    this.writeReportCellValue(worksheet, 'J8', 'F', true, true, false, false);

    let rowIndex = 9;
    for (let row of rows)
    {
      let isBold : boolean = row.rowType === <any> FinalExamenStatistiquesRowType.TypeDiplome || row.rowType === <any> FinalExamenStatistiquesRowType.Total;
      let isLargeSize: boolean = row.rowType === <any> FinalExamenStatistiquesRowType.Total;
      this.writeReportCellValue(worksheet, 'A' + rowIndex, row.orderNumber, true, isBold, false, isLargeSize);
      this.writeReportCellValue(worksheet, 'B' + rowIndex, row.programName, true, isBold, true, isLargeSize);
      worksheet.getCell('B' + rowIndex).alignment = { wrapText: true };
      this.writeReportCellValue(worksheet, 'C' + rowIndex, row.candidatePublicTotal, true, isBold, false, isLargeSize);
      this.writeReportCellValue(worksheet, 'D' + rowIndex, row.candidatePublicFemale, true, isBold, false, isLargeSize);
      this.writeReportCellValue(worksheet, 'E' + rowIndex, row.candidatePrivateTotal, true, isBold, false, isLargeSize);
      this.writeReportCellValue(worksheet, 'F' + rowIndex, row.candidatePrivateFemale, true, isBold, false, isLargeSize);
      this.writeReportCellValue(worksheet, 'G' + rowIndex, row.admisPublicTotal, true, isBold, false, isLargeSize);
      this.writeReportCellValue(worksheet, 'H' + rowIndex, row.admisPublicFemale, true, isBold, false, isLargeSize);
      this.writeReportCellValue(worksheet, 'I' + rowIndex, row.admisPrivateTotal, true, isBold, false, isLargeSize);
      this.writeReportCellValue(worksheet, 'J' + rowIndex, row.admisPrivateFemale, true, isBold, false, isLargeSize);
      this.writeReportCellValue(worksheet, 'K' + rowIndex, row.admisPercentagePublic, true, true, false, isLargeSize);
      this.writeReportCellValue(worksheet, 'L' + rowIndex, row.admisPercentagePrivate, true, true, false, isLargeSize);
      rowIndex++;
    }

    rowIndex = rowIndex + 4;
    this.writeReportCellValue(worksheet, 'I' + rowIndex, 'LE SERVICE EXAMENS', false, true, true, false);

    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportToCsv(rows: object[], fileName: string, columns?: string[], header?: string[]): string {
    if (!rows || !rows.length) {
      return;
    }
    const separator = ',';
    const csvContent =
      header.join(separator) +
      '\n' +
      rows.map(row => {
        return columns.map(k => {
          let cell = row[k] === null || row[k] === undefined ? '' : row[k];
          cell = cell instanceof Date
            ? cell.toLocaleString()
            : cell.toString().replace(/"/g, '""');
          if (cell.search(/("|,|\n)/g) >= 0) {
            cell = `"${cell}"`;
          }
          return cell;
        }).join(separator);
      }).join('\n');
    this._saveAs(csvContent, `${fileName}${CSV_EXTENSION}`, CSV_TYPE);
  }

  private _saveAsExcel(buffer: any, fileName: string) {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName);
  }

  private _saveAs(buffer: any, fileName: string, fileType: string) {
    const data: Blob = new Blob([buffer], {type: fileType});
    FileSaver.saveAs(data, fileName);
  }

  private _addLogoAndCopyrightText(numRows: number, worksheet: ExcelJS.Worksheet, logo: any) {
    let copyrightTextPos = numRows + 3;
    let logoPos1 = copyrightTextPos + 1;
    let logoPos2 = logoPos1 + 4; 
    var cell = worksheet.getCell('E'+copyrightTextPos);
    cell.value = 'Copyright © METFP Tous droits réservés';
    cell.font = {
      bold: true
    };
    worksheet.addImage(logo, 'E'+logoPos1+':F'+logoPos2);
  }

  private _addCommonConditionalColumns(columns, fields) {
    if (fields.exportNom) {
      columns.push({ header: 'Nom', key: 'name', width: 25 });
    }

    if (fields.exportPrenom) {
      columns.push({ header: 'Prénom', key: 'firstName', width: 25 });
    }

    if (fields.exportEmail) {
      columns.push({ header: 'Email', key: 'email', width: 30 });
    }

    if (fields.exportNumeroTelephone) {
      columns.push({ header: 'Numéro de téléphone', key: 'phone', width: 18 });
    }

    if (fields.exportDateNaissance) {
      columns.push({ header: 'Date de naissance', key: 'dateNaissance', width: 18 });
    }

    if (fields.exportSexe) {
      columns.push({ header: 'Sexe', key: 'genreName', width: 10 });
    }

    if (fields.exportPv) {
      columns.push({ header: 'PV', key: 'numeroPV', width: 10 });
    }

    if (fields.exportSession) {
      columns.push({ header: 'Session BAC', key: 'sessionBAC', width: 10 });
      columns.push({ header: 'Session BEPC', key: 'sessionBEPC', width: 10 });
      columns.push({ header: 'Session Terminale', key: 'sessionTerminale', width: 10 });
    }

    if (fields.exportOption) {
      columns.push({ header: 'Option BAC', key: 'optionBACName', width: 20 });
      columns.push({ header: 'Option BEPC', key: 'optionBEPCName', width: 20 });
      columns.push({ header: 'Option Terminale', key: 'optionTerminaleName', width: 20 });
      columns.push({ header: 'Option Post-primaire', key: 'optionPostPrimaireName', width: 20 });
      columns.push({ header: 'Option Post-secondaire', key: 'optionPostSecondaireName', width: 20 });
      columns.push({ header: 'Option Autre', key: 'optionAutreName', width: 20 });
    }

    if (fields.exportCentreExamen) {
      columns.push({ header: 'Centre d\'examen', key: 'centreExamenName', width: 30 });
    }

    if (fields.exportLycee) {
      columns.push({ header: 'Lycée BAC', key: 'lyceeName', width: 30 });
      columns.push({ header: 'Collège', key: 'collegeName', width: 30 });
      columns.push({ header: 'Lycée Terminale', key: 'lyceeTerminaleName', width: 30 });
    }

    if (fields.exportMoyenne) {
      columns.push({ header: 'Moyenne BAC', key: 'moyenneBAC', width: 20 });
      columns.push({ header: 'Moyenne BEPC', key: 'moyenneBEPC', width: 20 });
      columns.push({ header: 'Moyenne Terminale', key: 'moyenneTerminale', width: 20 });
    }

    if (fields.exportMatricule) {
      columns.push({ header: 'Matricule', key: 'Matricule', width: 20 });
    }

    if (fields.exportLieuNaissance) {
      columns.push({ header: 'Lieu de naissance', key: 'lieuNaissance', width: 20 });
    }

    if (fields.exportNomPere) {
      columns.push({ header: 'Nom du père', key: 'nomPere', width: 30 });
    }

    if (fields.exportNomMere) {
      columns.push({ header: 'Nom de la mère', key: 'nomMere', width: 30 });
    }

    if (fields.exportBiometricStatus) {
      columns.push({ header: 'Statut biométrique', key: 'biometricStatus', width: 20 });
    }
  }

  public exportDemandeurs(demandeurs: any, fields: any, fileNamePrefix: string) {
    let fileName = fileNamePrefix + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet(fileName);

    let columns = [];

    if (fields.identifiant) {
      columns.push({ header: fields.identifiant.header, key: fields.identifiant.key, width: fields.identifiant.width });
    }

    if (fields.name) {
      columns.push({ header: fields.name.header, key: fields.name.key, width: fields.name.width });
    }
    
    if (fields.firstName) {
      columns.push({ header: fields.firstName.header, key: fields.firstName.key, width: fields.firstName.width });
    }
    
    if (fields.email) {
      columns.push({ header: fields.email.header, key: fields.email.key, width: fields.email.width });
    }
    
    if (fields.demandeDate) {
      columns.push({ header: fields.demandeDate.header, key: fields.demandeDate.key, width: fields.demandeDate.width });
    }
    
    if (fields.demandeurStatusName) {
      columns.push({ header: fields.demandeurStatusName.header, key: fields.demandeurStatusName.key, width: fields.demandeurStatusName.width });
    }

    worksheet.columns = columns;
    worksheet.addRows(demandeurs);
    this._addLogoAndCopyrightText(demandeurs.length, worksheet, logo); 
    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportDemandes(demandes: any, fields: any, fileNamePrefix: string) {
    let fileName = fileNamePrefix + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });

    let worksheet = workbook.addWorksheet(fileName);

    let columns = [];

    if (fields.typeDemandeName) {
      columns.push({ header: fields.typeDemandeName.header, key: fields.typeDemandeName.key, width: fields.typeDemandeName.width });
    }

    if (fields.agrementationName) {
      columns.push({ header: fields.agrementationName.header, key: fields.agrementationName.key, width: fields.agrementationName.width });
    }

    if (fields.numero) {
      columns.push({ header: fields.numero.header, key: fields.numero.key, width: fields.numero.width });
    }

    if (fields.demandeurName) {
      columns.push({ header: fields.demandeurName.header, key: fields.demandeurName.key, width: fields.demandeurName.width });
    }
    
    if (fields.demandeurFirstName) {
      columns.push({ header: fields.demandeurFirstName.header, key: fields.demandeurFirstName.key, width: fields.demandeurFirstName.width });
    }
    
    if (fields.creationTime) {
      columns.push({ header: fields.creationTime.header, key: fields.creationTime.key, width: fields.creationTime.width });
    }
    
    if (fields.status) {
      columns.push({ header: fields.status.header, key: fields.status.key, width: fields.status.width });
    }

    worksheet.columns = columns;
    worksheet.addRows(demandes);
    this._addLogoAndCopyrightText(demandes.length, worksheet, logo); 
    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }

  public exportRapportCompletSurLesConcours(items: any) {
    let fileName = "RapportCompletSurLesConcours_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let logo = workbook.addImage({
      base64: GUPOL_LOGO,
      extension: 'png',
    });
    let worksheet = workbook.addWorksheet('Rapport complet sur les concours');
    let columns = [];
    columns.push({ header: 'Année', key: 'annee', width: 25});
    columns.push({ header: 'Numéro PV Concours', key: 'numeroPV', width: 25 });
    columns.push({ header: 'Identifiant National Apprenant', key: 'identifiantNationalEleve', width: 25 });
    columns.push({ header: 'Prénoms', key: 'etudiantFirstName', width: 25 });
    columns.push({ header: 'Nom', key: 'etudiantName', width: 25 });
    columns.push({ header: 'Date Naissance', key: 'etudiantDateNaissance', width: 25 });
    columns.push({ header: 'Sexe', key: 'etudiantGenre', width: 25 });
    columns.push({ header: 'Institution', key: 'institutionName', width: 25 });
    columns.push({ header: 'Statut de l\'institution', key: 'statusEtablissement', width: 25 });
    columns.push({ header: 'Département', key: 'faculteName', width: 25 });
    columns.push({ header: 'Nom du concours', key: 'concoursName', width: 25 });
    columns.push({ header: 'Centre de concours', key: 'centreConcoursName', width: 25 });
    columns.push({ header: 'Programme', key: 'programmeName', width: 25 });
    columns.push({ header: 'Statut des résultats', key: 'statusResultName', width: 25 });
    columns.push({ header: 'Décision', key: 'decisionName', width: 25 });
    columns.push({ header: 'Concours Statut global', key: 'statusGlobalName', width: 25 });
    columns.push({ header: 'Concours Notes moyenne', key: 'moyenneTotal', width: 25 });

    worksheet.columns = columns;
    worksheet.addRows(items);
    this._addLogoAndCopyrightText(items.length, worksheet, logo); 
    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }
  
  public downloadTemplate(templateName : string, headers : { name: string, value: string }[]) {
    let fileName = templateName + "_" + this.getCurrentDate();
    let workbook = new ExcelJS.Workbook();
    let worksheet = workbook.addWorksheet(templateName);
    
    let columns = [];

    headers.forEach(data => {
      columns.push({ header: data.name, key: data.value, width: 20 });
    });

    worksheet.columns = columns;
    workbook.xlsx.writeBuffer().then((buffer) => {
      this._saveAsExcel(buffer, fileName);
    });
  }
}