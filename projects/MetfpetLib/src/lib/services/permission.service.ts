import { Injectable } from '@angular/core';
import { NgxPermissionsService } from 'ngx-permissions';
import { MenuService, Menu } from '../components/menu';
import { AuthorityEnum } from '../interfaces';

export const RoutesByPermissionsConfiguration = {
  ['admission']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.AgentMinisteriel, AuthorityEnum.Ministre
    ],
  
  ['admission.session']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.AgentMinisteriel
    ],

  ['activities']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur
    ],
  
  ['statistiques']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.DirectionsNationales
    ],

  ['communication']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel
    ],

  ['configuration']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.DirectionsNationales, AuthorityEnum.ExamSupervisorMinisteriel, AuthorityEnum.AgrementSupervisorMinisteriel, AuthorityEnum.Ministre
    ],
    
  ['cursus']: 
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.AgentMinisteriel, AuthorityEnum.ExamSupervisorMinisteriel, AuthorityEnum.ExamAgentMinisteriel, AuthorityEnum.Ministre, AuthorityEnum.DirectionsNationales
    ],
  
  ['inscription']: 
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.AgentMinisteriel, AuthorityEnum.Ministre, AuthorityEnum.DirectionsNationales
    ],
  
  ['examen']: 
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.ExamSupervisorMinisteriel, AuthorityEnum.ExamAgentMinisteriel, AuthorityEnum.Ministre
    ],

  ['admin.examen']: 
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.ExamSupervisorMinisteriel
    ],

  ['document']: 
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.AgentMinisteriel, AuthorityEnum.Ministre
    ],

  ['export-import']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel
    ],

  ['import']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel
    ],

  ['export']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel
    ],

  ['diplomes']: 
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.AgentMinisteriel, AuthorityEnum.Ministre
    ],
  
  ['generate.diplomes']: 
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.AgentMinisteriel
    ],

  ['payments']: 
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.AgentMinisteriel, AuthorityEnum.DirectionsNationales, AuthorityEnum.AgrementSupervisorMinisteriel, AuthorityEnum.AgrementAgentMinisteriel, AuthorityEnum.Ministre
    ],
  
  ['students']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.AgentMinisteriel, AuthorityEnum.DirectionsNationales, AuthorityEnum.ExamSupervisorMinisteriel, AuthorityEnum.ExamAgentMinisteriel, AuthorityEnum.Ministre
    ],
  
  ['institutions']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.DirectionsNationales, AuthorityEnum.Ministre
    ],
  
  ['admin.institutions']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.Ministre
    ],

  ['lycees']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.Ministre
    ],
  
  ['admissions']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.AgentMinisteriel, AuthorityEnum.Ministre
    ],

  ['session']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.Ministre
    ],

  ['users']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.Ministre
    ],
  
  ['bourses']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.AgentMinisteriel, AuthorityEnum.Ministre, AuthorityEnum.DirectionsNationales
    ],
  
  ['generate.bourses']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur
    ],
  
  ['report.bourses']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.AgentMinisteriel, AuthorityEnum.Ministre
    ],
  
  ['config.bourses']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.AgentMinisteriel, AuthorityEnum.Ministre
    ],

  ['agrementation']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.AgrementSupervisorMinisteriel, AuthorityEnum.AgrementAgentMinisteriel, AuthorityEnum.Ministre
    ],
  
  ['admin.agrementation']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.AgrementSupervisorMinisteriel
    ],

  ['frais']:
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.SuperviseurMinisteriel, AuthorityEnum.AgentMinisteriel, AuthorityEnum.Ministre
    ],

  ['admin.kits']: 
    [
      AuthorityEnum.AdministrateurMinisteriel, AuthorityEnum.Administrateur, AuthorityEnum.ExamSupervisorMinisteriel, AuthorityEnum.ExamAgentMinisteriel, AuthorityEnum.Ministre
    ],

  ['student.admission']:
    [
      AuthorityEnum.Student
    ],
  
  ['student.dossier']:
    [
      AuthorityEnum.Student
    ],
  
  ['student.cursus']: 
    [
      AuthorityEnum.Student
    ],

  ['student.document']: 
    [
      AuthorityEnum.Student
    ],

  ['student.bourses']: 
    [
      AuthorityEnum.Student
    ],

  ['student.examens']: 
    [
      AuthorityEnum.Student
    ],

  ['student.diplomes']: 
    [
      AuthorityEnum.Student
    ],

  ['institution.admissions']: 
    [
      AuthorityEnum.SuperviseurUniversitaire
    ],

  ['institution.students']: 
    [
      AuthorityEnum.SuperviseurUniversitaire, AuthorityEnum.AgentUniversitaire
    ],

  ['institution.inscriptions']: 
    [
      AuthorityEnum.SuperviseurUniversitaire, AuthorityEnum.AgentUniversitaire
    ],
  
  ['institution.examens']: 
    [
      AuthorityEnum.SuperviseurUniversitaire
    ],

  ['institution.documents']: 
    [
      AuthorityEnum.SuperviseurUniversitaire, AuthorityEnum.AgentUniversitaire
    ],
  
  ['institution.bourses']: 
    [
      AuthorityEnum.SuperviseurUniversitaire
    ],

  ['institution.employees']: 
    [
      AuthorityEnum.SuperviseurUniversitaire
    ],

  ['demandeur.demandes']:
    [
      AuthorityEnum.Demandeur
    ],
  
  ['demandeur.dossier']:
    [
      AuthorityEnum.Demandeur
    ],   
};

export const MenusWithPermission = [
  new Menu (1,   'Apprenants', null, null, 'apprenant', null, true, 0, RoutesByPermissionsConfiguration['students']),
  new Menu (11,   'Dossier Apprenant', 'students', null, 'menu-dossier-students', null, false, 1, RoutesByPermissionsConfiguration['students']),
  new Menu (12,   'Admission', 'admission', null, 'menu-admission', null, false, 1, RoutesByPermissionsConfiguration['admission']),
  new Menu (13,   'Cursus', 'cursus/inscriptions', null, 'menu-management', null, true, 1, RoutesByPermissionsConfiguration['cursus']),
  new Menu (130,  'Inscription et réinscriptions', 'cursus/inscriptions', null, 'menu-cursus-inscription', null, false, 13, RoutesByPermissionsConfiguration['inscription']),
  new Menu (16,  'Examens de sortie', null, null, 'menu-cursus-examens', null, true, 1, RoutesByPermissionsConfiguration['examen']),
  new Menu (161,  'Gestion des examens', 'cursus/examens', null, 'menu-examen-operation', null, false, 16, RoutesByPermissionsConfiguration['examen']),
  new Menu (162,  'Opération - examens', 'cursus/gestions', null, 'menu-cursus-gestion', null, false, 16, RoutesByPermissionsConfiguration['admin.examen']),
  new Menu (17,  'Concours d’entrée', null, null, 'concours-entree', null, true, 1, RoutesByPermissionsConfiguration['examen']),
  new Menu (171,  'Gestion des concours', 'concours', null, 'gestion-concours', null, false, 17, RoutesByPermissionsConfiguration['examen']),
  new Menu (172,  'Opération - Concours', 'concours/operation', null, 'menu-cursus-gestion', null, false, 17, RoutesByPermissionsConfiguration['admin.examen']),
  new Menu (173,  'Export résultats', 'export', null, 'menu-export-import', null, false, 17, RoutesByPermissionsConfiguration['admin.examen']),
  new Menu (14,   'Documents officiels', null, null, 'menu-documents', null, true, 1, RoutesByPermissionsConfiguration['document']),
  new Menu (314,   'Documents', 'document/impressionDocuments', null, 'agrementation-document', null, false, 14, RoutesByPermissionsConfiguration['document']),
  new Menu (315,   'Génération document', 'document/operationDocuments', null, 'menu-generation-list-diplome', null, false, 14, RoutesByPermissionsConfiguration['document']),
  new Menu (18,   'Diplomes et équivalences', 'diplomes', null, 'diplome-equivalence', null, true, 1, RoutesByPermissionsConfiguration['diplomes']),
  new Menu (181,  'Diplomes', 'diplomes', null, 'menu-diplome', null, false, 18, RoutesByPermissionsConfiguration['diplomes']),
  new Menu (182,  'Génération liste diplômés', 'diplomes/generation-list-diplome', null, 'menu-generation-list-diplome', null, false, 18, RoutesByPermissionsConfiguration['generate.diplomes']),
  new Menu (15,   'Bourses', 'bourses', null, 'menu-bourse', null, true, 1, RoutesByPermissionsConfiguration['bourses']),
  new Menu (150,  'Paiement des bourses', 'bourses/payments', null, 'menu-bourse-payment', null, false, 15, RoutesByPermissionsConfiguration['bourses']),
  new Menu (151,  'Déterminer bourses', 'bourses/determiner', null, 'menu-bourse-determiner', null, false, 15, RoutesByPermissionsConfiguration['generate.bourses']),
  new Menu (152,  'Rapports', 'bourses/rapports', null, 'menu-bourse-rapport', null, false, 15, RoutesByPermissionsConfiguration['report.bourses']),
  new Menu (2,    'Agréments', null, null, 'agrementation', null, true, 0, RoutesByPermissionsConfiguration['agrementation']),
  new Menu (21,   'Dossier demandeur', 'agrementation-dossiers', null, 'menu-dossier-students', null, false, 2, RoutesByPermissionsConfiguration['agrementation']),
  new Menu (22,   'Agrément', 'agrements', null, 'agrement', null, false, 2, RoutesByPermissionsConfiguration['agrementation']),
  new Menu (5,   'Journaux d’évènements', 'activities', null, 'menu-track', null, false, 0, RoutesByPermissionsConfiguration['activities']),
  new Menu (51,   'Statistiques et rapports', 'statistiques', null, 'menu-report', null, true, 0, RoutesByPermissionsConfiguration['statistiques']),
  new Menu (511,  'Paiement des bourses', 'statistiques/bourses', null, 'menu-bourse-payment', null, false, 51, RoutesByPermissionsConfiguration['statistiques']),
  new Menu (512,  'Apprenants paiements', 'statistiques/paiements', null, 'statistiques-apprenant-paiement', null, false, 51, RoutesByPermissionsConfiguration['statistiques']),
  new Menu (513,  'Suivi des effectifs', 'statistiques/admissions', null, 'statistiques-apprenant', null, false, 51, RoutesByPermissionsConfiguration['statistiques']),
  new Menu (514,  'Kits scolaires', 'statistiques/school-kits', null, 'menu-school-kit', null, false, 51, RoutesByPermissionsConfiguration['statistiques']),
  new Menu (6,   'Communication', 'communication', null, 'menu-communication', null, true, 0, RoutesByPermissionsConfiguration['communication']),
  new Menu (60,  'Envoyer un message', 'communication', null, 'menu-send', null, false, 6, RoutesByPermissionsConfiguration['communication']),
  new Menu (61,  'Paramètres', 'communication/parameters', null, 'menu-parameters', null, false, 6, RoutesByPermissionsConfiguration['communication']),
  new Menu (62,  'Domaine de courriers', 'communication/mailDomains', null, 'menu-communication', null, false, 6, RoutesByPermissionsConfiguration['communication']),
  new Menu (7,   'Paiements', 'payments', null, 'menu-payment', null, false, 0, RoutesByPermissionsConfiguration['payments']),
  new Menu (8,   'Personnels', 'users/ministry', null, 'menu-target', null, false, 0, RoutesByPermissionsConfiguration['users']),
  new Menu (9,   'Configuration', 'configuration', null, 'menu-configuration', null, true, 0, RoutesByPermissionsConfiguration['configuration']),
  new Menu (91,  'Institutions', null, null, 'menu-institution', null, true, 9, RoutesByPermissionsConfiguration['institutions']),
  new Menu (910, 'Paramètres', 'institutions', null, 'menu-parameters-1', null, false, 91, RoutesByPermissionsConfiguration['institutions']),
  new Menu (911, 'Frais d’institutions', 'institutions/frais-institutions', null, 'menu-frais', null, false, 91, RoutesByPermissionsConfiguration['admin.institutions']),
  new Menu (912, 'Configurations en attente', 'institutions/configurations-attente', null, 'menu-configurations-attente', null, false, 91, RoutesByPermissionsConfiguration['admin.institutions']),
  new Menu (92,  'Admissions', null, null, 'menu-admission', null, true, 9, RoutesByPermissionsConfiguration['admissions']),
  new Menu (921,  'Session en cours', 'admission/session', null, 'menu-session', null, false, 92, RoutesByPermissionsConfiguration['admission.session']),
  new Menu (922,  'Lycées et écoles', 'lycees', null, 'menu-school', null, false, 92, RoutesByPermissionsConfiguration['lycees']),
  new Menu (93,   'Import', null, null, 'menu-export-import', null, true, 9, RoutesByPermissionsConfiguration['export-import']),
  new Menu (931,  'Import admission', 'import-students', null, 'menu-export-import', null, false, 93, RoutesByPermissionsConfiguration['import']),
  new Menu (932,  'Import résultats', 'import-results', null, 'menu-export-import', null, false, 93, RoutesByPermissionsConfiguration['import']),
  new Menu (933,  'Import ré-inscription', 'import-inscriptions', null, 'menu-export-import', null, false, 93, RoutesByPermissionsConfiguration['import']),
  new Menu (934,  'Import personnels', 'import-users', null, 'menu-export-import', null, false, 93, RoutesByPermissionsConfiguration['import']),
  new Menu (935,  'Import candidats libres', 'import-candidats-libres', null, 'menu-export-import', null, false, 93, RoutesByPermissionsConfiguration['import']),
  new Menu (936,  'Import redoublants', 'import-redoublants', null, 'menu-export-import', null, false, 93, RoutesByPermissionsConfiguration['import']),
  new Menu (937,  'Import diplôme', 'import-diplome', null, 'menu-export-import', null, false, 93, RoutesByPermissionsConfiguration['import']),
  new Menu (938,  'Synchroniser Inserjeune', 'sync-inserjeune', null, 'menu-export-import', null, false, 93, RoutesByPermissionsConfiguration['import']),
  new Menu (939,  'Importer emails apprenants', 'import-email', null, 'menu-export-import', null, false, 93, RoutesByPermissionsConfiguration['import']),
  new Menu (9391, 'Import données de profil', 'import-student-profile', null, 'menu-export-import', null, false, 93, RoutesByPermissionsConfiguration['import']),
  new Menu (9392, 'Actualiser inscriptions', 'actualiser-inscriptions', null, 'menu-export-import', null, false, 93, RoutesByPermissionsConfiguration['import']),
  new Menu (9393, 'Comptes Écobank', 'import-comptes-ecobank', null, 'menu-export-import', null, false, 93, RoutesByPermissionsConfiguration['import']),
  new Menu (9394, 'Validation de bourses', 'import-bourses', null, 'menu-export-import', null, false, 93, RoutesByPermissionsConfiguration['import']),
  new Menu (94,   'Documents officiels', 'document/list', null, 'menu-documents', null, false, 9, RoutesByPermissionsConfiguration['document']),
  new Menu (95,   'Bourses', 'bourses/list', null, 'menu-bourse', null, false, 9, RoutesByPermissionsConfiguration['config.bourses']),
  new Menu (96,   'Examens de sortie', 'cursus/configurations', null, 'menu-cursus-examens', null, false, 9, RoutesByPermissionsConfiguration['admin.examen']),
  new Menu (961,  'Concours d’entrée', 'concours/configurations', null, 'concours-entree', null, false, 9, RoutesByPermissionsConfiguration['admin.examen']),
  new Menu (97,   'Agréments', null, null, 'agrementation', null, true, 9, RoutesByPermissionsConfiguration['admin.agrementation']),
  new Menu (971,  'Agrément', 'agrementation-configuration', null, 'agrement', null, false, 97, RoutesByPermissionsConfiguration['admin.agrementation']),
  new Menu (972,  'Frais', 'frais/frais-agrementations', null, 'menu-frais', null, false, 97, RoutesByPermissionsConfiguration['admin.agrementation']),
  new Menu (973,  'Import', 'agrementation-configuration/import', null, 'menu-export-import', null, false, 97, RoutesByPermissionsConfiguration['admin.agrementation']),
  new Menu (974,  'Modèles de documents', 'agrementation-configuration/document-template', null, 'agrementation-document', null, false, 97, RoutesByPermissionsConfiguration['admin.agrementation']),
  new Menu (98,  'Kits scolaires', 'school-kits', null, 'menu-school-kit', null, false, 9, RoutesByPermissionsConfiguration['admin.kits']),
  
  // for institution
  new Menu (800, 'Admission', 'admissions', null, 'menu-admission', null, false, 0, RoutesByPermissionsConfiguration['institution.admissions']),
  new Menu (801, 'Dossier Apprenant', 'students', null, 'menu-dossier-students', null, false, 0, RoutesByPermissionsConfiguration['institution.students']),
  new Menu (802, 'Inscriptions', 'inscriptions', null, 'menu-clipboard', null, false, 0, RoutesByPermissionsConfiguration['institution.inscriptions']),
  //new Menu (803, 'Documents officiels', 'documents', null, 'menu-documents', null, false, 0, RoutesByPermissionsConfiguration['institution.documents']),
  new Menu (804, 'Bourses', 'bourses', null, 'menu-bourse', null, false, 0, RoutesByPermissionsConfiguration['institution.bourses']),
  new Menu (805, 'Personnels', 'users', null, 'menu-target', null, false, 0, RoutesByPermissionsConfiguration['institution.employees']),
  new Menu (806, 'Examens de sortie', 'examens', null, 'menu-cursus-examens', null, false, 0, RoutesByPermissionsConfiguration['institution.examens']),

  // for student
  new Menu (900, 'Admission', 'admission', null, 'menu-admission', null, false, 0, RoutesByPermissionsConfiguration['student.admission']),
  new Menu (901, 'Dossier apprenant', 'dossier', null, 'menu-dossier-students', null, false, 0, RoutesByPermissionsConfiguration['student.dossier']),
  new Menu (902, 'Cursus', 'cursus/inscriptions', null, 'menu-management', null, false, 0, RoutesByPermissionsConfiguration['student.cursus']),
  //new Menu (903, 'Documents officiels', 'document', null, 'menu-documents', null, false, 0, RoutesByPermissionsConfiguration['student.document']),
  new Menu (904, 'Examens de sortie', 'examens', null, 'menu-cursus-examens', null, false, 0, RoutesByPermissionsConfiguration['student.examens']),
  new Menu (905, 'Diplômes', 'diplomes', null, 'menu-diplome', null, false, 0, RoutesByPermissionsConfiguration['student.diplomes']),
  new Menu (906, 'Bourses', 'bourses', null, 'menu-bourse', null, false, 0, RoutesByPermissionsConfiguration['student.bourses']),

  // for demandeur
  new Menu (900, 'Agrémentation', 'demandes', null, 'agrement', null, false, 0, RoutesByPermissionsConfiguration['demandeur.demandes']),
  new Menu (901, 'Mon profil', 'dossier', null, 'menu-profil', null, false, 0, RoutesByPermissionsConfiguration['demandeur.dossier']),
];

@Injectable()
export class PermissionService {

  constructor(
    private _menuService: MenuService,
    private _ngxPermissionsService: NgxPermissionsService
  ) {
    this._menuService.loadMenus(MenusWithPermission);
  }

  public loadPermissions(userType: string) {
    this._ngxPermissionsService.loadPermissions([userType]);
  }
}