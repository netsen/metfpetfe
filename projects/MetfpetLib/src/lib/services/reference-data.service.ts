import { Injectable } from '@angular/core';

@Injectable()
export class ReferenceDataService {

  public fetchListDistricts(onLoadFunction): void {
    const req = new XMLHttpRequest();
    req.open('GET', 'assets/data/district.json');
    req.onload = () => onLoadFunction(JSON.parse(req.response));
    req.send();
  }
}