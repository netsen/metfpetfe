import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthRoleGuardService implements CanActivate {

  constructor(private _router: Router, private _authService: AuthService) {}

  canActivate() {
    if  (!this._authService.isAuthenticated()) {
      this._router.navigate([this._authService.signOut()]);
      return false;
    }
    return true;
  }

}