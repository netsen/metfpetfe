import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators/';
import { Store } from '@ngrx/store';
import { UserIdleService } from 'angular-user-idle';
import { MatDialog } from '@angular/material/dialog';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { IAppState, LoginTypeEnum } from '../interfaces/';
import { IdentityServiceAgent } from './identity-http.service';
import { PermissionService } from './permission.service';
import { StorageService } from './storage.service';
import { loginSuccess } from '../store/actions/auth.actions';

const AuthorizationData = 'authorizationData';

@Injectable({providedIn: 'root'})
export class AuthService {

  private _onTimeStartSubscription = new Subscription();
  private _onTimoutSubscription = new Subscription();
  private _authorizationData: any;

  constructor(
    private _matDialog: MatDialog,
    private _router: Router,
    private _store: Store<IAppState>,
    private _userIdleService: UserIdleService,
    private _identityService: IdentityServiceAgent.HttpService,
    private _permissionService: PermissionService,
    private _storageService: StorageService
  ) {
    this._onTimeStartSubscription = this._userIdleService.onTimerStart().subscribe();
    this._onTimoutSubscription = this._userIdleService.onTimeout().subscribe(() => {
      if (this.getAccessToken() == undefined) {
        return;
      }
      this._matDialog.closeAll();
      this._router.navigate([this.signOut()]);
    });

    if (this.isAuthenticated()) {
      this._userIdleService.startWatching();
      this._emitLoginSuccess();
    }
  }

  ngOnDestroy() {
    this._onTimeStartSubscription.unsubscribe();
    this._onTimoutSubscription.unsubscribe();
  }

  public login(username: string, password: string, loginType: LoginTypeEnum) {
    var login$: Observable<any>;

    switch (loginType) {
      case LoginTypeEnum.Ministeriel: {
        login$ = this._identityService.loginEmployeMinisteriel(IdentityServiceAgent.LoginInfo.fromJS({
          identifiant: username,
          password: password
        }));
        break;
      }
      
      case LoginTypeEnum.Institution: {
        login$ = this._identityService.loginEmployeUniversitaire(IdentityServiceAgent.LoginInfo.fromJS({
          identifiant: username,
          password: password
        }));
        break;
      }
      
      case LoginTypeEnum.Student: {
        login$ = this._identityService.login(IdentityServiceAgent.LoginInfo.fromJS({
          identifiant: username,
          password: password
        }));
        break;
      }

      case LoginTypeEnum.Demandeur: {
        login$ = this._identityService.loginDemandeur(IdentityServiceAgent.LoginInfo.fromJS({
          identifiant: username,
          password: password
        }));
        break;
      }
    }

    return login$.pipe(
      map((authResult: any) => this._storeAuthResult(authResult))
    );
  }

  private _storeAuthResult(authResult: any) {
    if (!authResult) {
      throw new Error('Nom d\'utilisateur ou mot de passe invalide');
    }

    this._resetAuthData();
    this._storageService.store(AuthorizationData, authResult);
    this._emitLoginSuccess();
    this._userIdleService.resetTimer();
    this._userIdleService.startWatching();
    return authResult;
  }

  private _resetAuthData() {
    this._storageService.remove(AuthorizationData);
    this._authorizationData = null;
  }

  private _emitLoginSuccess() {
    this._permissionService.loadPermissions(this.getAuthorizationData().type);
    this._store.dispatch(loginSuccess(
      {
        user: {
          id: this.getAuthorizationData().id,
          name: this.getAuthorizationData().name,
          firstName: this.getAuthorizationData().firstName,
          identifiant: this.getAuthorizationData().identifiant,
          type: this.getAuthorizationData().type,
          institutionId: this.getAuthorizationData().institutionId,
          statusEtablissementCode: this.getAuthorizationData().statusEtablissementCode,
          employeRoleId: this.getAuthorizationData().employeRoleId,
          employeRole: this.getAuthorizationData().employeRole,
        }
      }
    ));
  }

  public signOut(): string {
    this._resetAuthData();
    this._userIdleService.stopWatching();
    return 'login';
  }

  public isAuthenticated(): boolean {
    return this.getAccessToken() && moment().isBefore(this.getExpirationTime());;
  }

  public getExpirationTime() {
    let expirationTime = +this.getAuthorizationData().expirationTime;
    let date = new Date(0); // The 0 here is the key, which sets the date to the epoch
    date.setUTCSeconds(expirationTime);
    return moment(date)
  }

  public getAuthorizationData() {
    this._authorizationData = this._storageService.retrieve(AuthorizationData);
    return this._authorizationData;
  }

  public getAccessToken() {
    return this.getAuthorizationData() ? this.getAuthorizationData().token : null;
  }

  public getUserId() {
    return this.getAuthorizationData() ? this.getAuthorizationData().id : null;
  }

  public getUserType() {
    return this.getAuthorizationData() ? this.getAuthorizationData().type : null;
  }

  public getUserStatusEtablissementCode() {
    return this.getAuthorizationData() ? this.getAuthorizationData().statusEtablissementCode : null;
  }

  public getEmployeRole() {
    return this.getAuthorizationData() ? this.getAuthorizationData().employeRole : null;
  }

  public getEmployeRoleId() {
    return this.getAuthorizationData() ? this.getAuthorizationData().employeRoleId : null;
  }

  public getIdentifiant() {
    return this.getAuthorizationData() ? this.getAuthorizationData().identifiant : null;
  }
}
