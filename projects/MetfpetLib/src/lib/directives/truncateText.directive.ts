import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[truncateText]',
})
export class TruncateTextDirective {
  @Input() truncateTextMaxLength : number = 10;
  @Input() bgColor : string = '#C4C4C4';
  text: string;

  constructor( private element: ElementRef) {}

  ngAfterViewInit() {
    this.text = this.element.nativeElement.textContent;
    this.element.nativeElement.innerText = this.text.length > this.truncateTextMaxLength ? this.text.substring(0, this.truncateTextMaxLength ) + '...' : this.text
  }

  @HostListener('mouseenter') onMouseEnter(){
    this.element.nativeElement.innerText = this.text;
    this.element.nativeElement.style.backgroundColor = this.bgColor;
    this.element.nativeElement.style.padding = '4px';
    this.element.nativeElement.style.borderRadius = '5px';
    this.element.nativeElement.style.width = 'auto';
    this.element.nativeElement.style.maxWidth  = '150px';
    this.element.nativeElement.style.zIndex = 100;
  }

  @HostListener('mouseleave') onMouseLeave(){
    this.element.nativeElement.innerText = this.text.length > this.truncateTextMaxLength ? this.text.substring(0, this.truncateTextMaxLength) + '...' : this.text;
    this.element.nativeElement.style.backgroundColor = '';
    this.element.nativeElement.style.width = 'auto';
    this.element.nativeElement.style.maxWidth  = '150px';
  }
}
  