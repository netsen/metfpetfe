import { MetfpetServiceAgent } from "../services";

export const PAYMENT_ERROR = 'Votre paiement a échoué. Veuillez contacter le support client.';
export const SUR_CONCOURS = 'Sur concours';

export enum StudentStatus {
  EnAdmission = 1,
  AInscrire = 2,
  Inscrit = 3,
  AReInscrire = 4,
  Termine = 5,
  Enregistre = 6,
  Diplome = 7,
}

export const StudentStatusValues = [
  {value : StudentStatus.EnAdmission, name : "En admission"},
  {value : StudentStatus.AInscrire, name : "À inscrire"},
  {value : StudentStatus.Inscrit, name : "Inscrit"},
  {value : StudentStatus.AReInscrire, name : "À (ré)inscrire"},
  {value : StudentStatus.Termine, name : "Terminé"},
  {value : StudentStatus.Enregistre, name : "Enregistré"},
  {value : StudentStatus.Diplome, name : "Diplomé"},
];

export const StudentStatusMap = StudentStatusValues.reduce((acc, cur) => {
  acc[cur.value] = cur.name;
  return acc;
}, {});

export function CanInscrire(status: StudentStatus) {
  switch(status) {
    case StudentStatus.AInscrire:
    case StudentStatus.AReInscrire:
    case StudentStatus.Inscrit:
      return true;
    default:
      return false;
  }
}

export enum InscriptionProgrammeStatus {
  APayer = 1,
  EnValidation = 2,
  Inscrit = 3,
  Redoublement = 4,
  CandidatLibre = 5,
}

export const InscriptionProgrammeStatusValues = [
  {value : InscriptionProgrammeStatus.APayer, name : 'À payer'},
  {value : InscriptionProgrammeStatus.EnValidation, name : 'En validation'},
  {value : InscriptionProgrammeStatus.Inscrit, name : 'Inscrit'},
  {value : InscriptionProgrammeStatus.Redoublement, name : 'Redoublement'},
  {value : InscriptionProgrammeStatus.CandidatLibre, name : 'Candidats libre'},
];

export const InscriptionProgrammeStatusWithoutAPayerValues = [
  {value : InscriptionProgrammeStatus.EnValidation, name : 'En validation'},
  {value : InscriptionProgrammeStatus.Inscrit, name : 'Inscrit'},
  {value : InscriptionProgrammeStatus.Redoublement, name : 'Redoublement'},
  {value : InscriptionProgrammeStatus.CandidatLibre, name : 'Candidats libre'},
];

export const InscriptionProgrammeStatusMap = InscriptionProgrammeStatusValues.reduce((acc, cur) => {
  acc[cur.value] = cur.name;
  return acc;
}, {});

export enum EmployeeAccess {
  //Aucun
  Aucun = 0,

  //Administrateur
  SuperAdministrator = 1,

  //Superviseur
  Supervisor = 2,

  //Agent
  Agent = 3,

  //Exam supervisor
  ExamSupervisor = 4,

  //Exam Agent
  ExamAgent = 5,

  // Agrement supervisor
  AgrementSupervisor = 6,

  // Agrement Agent
  AgrementAgent = 7,

  // Directions Nationales
  DirectionsNationales = 8,

  // Ministre
  Ministre = 9,

  // Super Administrator
  Administrator = 10,
}

export const MinistryEmployeeAccessValues = [
  {value : EmployeeAccess.Aucun, name : 'Aucun'},
  {value : EmployeeAccess.SuperAdministrator, name : 'Super administrateur'},
  {value : EmployeeAccess.Administrator, name : 'Administrateur'},
  {value : EmployeeAccess.Supervisor, name : 'Superviseur ParcoursPro'},
  {value : EmployeeAccess.Agent, name : 'Agent ParcoursPro'},
  {value : EmployeeAccess.ExamSupervisor, name : 'Superviseur examens'},
  {value : EmployeeAccess.ExamAgent, name : 'Agent examens'},
  {value : EmployeeAccess.AgrementSupervisor, name : 'Superviseur agrément'},
  {value : EmployeeAccess.AgrementAgent, name : 'Agent agrément'},
  {value : EmployeeAccess.DirectionsNationales, name : 'Directions Nationales'},
  {value : EmployeeAccess.Ministre, name : 'Ministre / SG'},
];

export const MinistryEmployeeAccessValuesWithoutSuperAdmin = [
  {value : EmployeeAccess.Aucun, name : 'Aucun'},
  {value : EmployeeAccess.Administrator, name : 'Administrateur'},
  {value : EmployeeAccess.Supervisor, name : 'Superviseur ParcoursPro'},
  {value : EmployeeAccess.Agent, name : 'Agent ParcoursPro'},
  {value : EmployeeAccess.ExamSupervisor, name : 'Superviseur examens'},
  {value : EmployeeAccess.ExamAgent, name : 'Agent examens'},
  {value : EmployeeAccess.AgrementSupervisor, name : 'Superviseur agrément'},
  {value : EmployeeAccess.AgrementAgent, name : 'Agent agrément'},
  {value : EmployeeAccess.DirectionsNationales, name : 'Directions Nationales'},
  {value : EmployeeAccess.Ministre, name : 'Ministre / SG'},
];

export const RestrictedMinistryEmployeeAccessValues = [
  {value : EmployeeAccess.Aucun, name : 'Aucun'},
  {value : EmployeeAccess.Supervisor, name : 'Superviseur ParcoursPro'},
  {value : EmployeeAccess.Agent, name : 'Agent ParcoursPro'},
  {value : EmployeeAccess.ExamSupervisor, name : 'Superviseur examens'},
  {value : EmployeeAccess.ExamAgent, name : 'Agent examens'},
  {value : EmployeeAccess.AgrementSupervisor, name : 'Superviseur agrément'},
  {value : EmployeeAccess.AgrementAgent, name : 'Agent agrément'},
  {value : EmployeeAccess.DirectionsNationales, name : 'Directions Nationales'},
  {value : EmployeeAccess.Ministre, name : 'Ministre / SG'},
];

export const MinistryEmployeeAccessMap = MinistryEmployeeAccessValues.reduce((acc, cur) => {
  acc[cur.value] = cur.name;
  return acc;
}, {});

export const InstitutionEmployeeAccessValues = [
  {value : EmployeeAccess.Aucun, name : 'Aucun'},
  {value : EmployeeAccess.Supervisor, name : 'Superviseur'},
  {value : EmployeeAccess.Agent, name : 'Agent'}
];

export const InstitutionEmployeeAccessMap = InstitutionEmployeeAccessValues.reduce((acc, cur) => {
  acc[cur.value] = cur.name;
  return acc;
}, {});

export enum LyceeStatus {
  Active = 1, 
  Inactive = 2
}

export enum CollegeStatus {
  Active = 1, 
  Inactive = 2
}

export enum SessionAdmissionStatus {
  ADemarrer = 1,
  EnCours = 2,
  Suspendue = 3,
  AExecuter = 4,
  ANotifier = 5,
  AConfirmer = 6,
  Terminee = 7
}

export const SessionAdmissionStatusValues = [
  {value : SessionAdmissionStatus.ADemarrer, name : 'À Démarrer'},
  {value : SessionAdmissionStatus.EnCours, name : 'En Cours'},
  {value : SessionAdmissionStatus.Suspendue, name : 'Suspendue'},
  {value : SessionAdmissionStatus.AExecuter, name : 'À Exécuter'},
  {value : SessionAdmissionStatus.ANotifier, name : 'À Notifier'},
  {value : SessionAdmissionStatus.AConfirmer, name : 'À Confirmer'},
  {value : SessionAdmissionStatus.Terminee, name : 'Terminée'},
];

export enum AdmissionInstitutionOption {
  APayer = 1,
  AnalyseEnCours = 2,
  Admis = 3,
  NonAdmis = 4,
  ResultatAdmisEmis = 5,
  ResultatNonAdmisEmis = 6,
  Accepte = 7,
  Refuse = 8,
  OffreSansReponse = 9,
  Archive = 10,
}

export const AdmissionInstitutionOptionValues = [
  {value : AdmissionInstitutionOption.APayer, name : 'À Payer'},
  {value : AdmissionInstitutionOption.AnalyseEnCours, name : 'Analyse En Cours'},
  {value : AdmissionInstitutionOption.Admis, name : 'Admis - Résultat Enregistré'},
  {value : AdmissionInstitutionOption.NonAdmis, name : 'Non admis - Résultat Enregistré'},
  {value : AdmissionInstitutionOption.ResultatAdmisEmis, name : 'Admis - Résultat Émis'},
  {value : AdmissionInstitutionOption.ResultatNonAdmisEmis, name : 'Non admis - Résultat Émis'},
  {value : AdmissionInstitutionOption.Accepte, name : 'Accepté'},
  {value : AdmissionInstitutionOption.Refuse, name : 'Refusé'},
  {value : AdmissionInstitutionOption.OffreSansReponse, name : 'Offre Sans Réponse'},
  {value : AdmissionInstitutionOption.Archive, name : 'Archivé'}
];

export enum AdmissionInstitutionStatus {
  APayer = 1,
  AnalyseEnCours = 2,
  ResultatEnregistre = 3,
  ResultatEmis = 4,
  Accepte = 5,
  Refuse = 6,
  OffreSansReponse = 7,
  Archive = 8,
}

export const AdmissionInstitutionStatusValues = [
  {value : AdmissionInstitutionStatus.APayer, name : 'À Payer'},
  {value : AdmissionInstitutionStatus.AnalyseEnCours, name : 'Analyse En Cours'},
  {value : AdmissionInstitutionStatus.ResultatEnregistre, name : 'Résultat Enregistré'},
  {value : AdmissionInstitutionStatus.ResultatEmis, name : 'Résultat Émis'},
  {value : AdmissionInstitutionStatus.Accepte, name : 'Accepté'},
  {value : AdmissionInstitutionStatus.Refuse, name : 'Refusé'},
  {value : AdmissionInstitutionStatus.OffreSansReponse, name : 'Offre Sans Réponse'},
  {value : AdmissionInstitutionStatus.Archive, name : 'Archivé'}
];

export enum AdmissionDecision {
  APayer = 1,
  Admis = 2,
  NonAdmis = 3,
  OffreAcceptee = 4,
  OffreRefusee = 5,
  OffreSansReponse = 6,
  NonComplete = 7,
  DejaAdmis = 8,
  AnalyseEnCours = 9,
  InvalidDocuments = 10,
  Archive = 11,
}

export const AdmissionDecisionValues = [
  {value : AdmissionDecision.APayer, name : 'À Payer'},
  {value : AdmissionDecision.Admis, name : 'Admis'},
  {value : AdmissionDecision.NonAdmis, name : 'Non Admis'},
  {value : AdmissionDecision.OffreAcceptee, name : 'Offre Acceptée'},
  {value : AdmissionDecision.OffreRefusee, name : 'Offre Refusée'},
  {value : AdmissionDecision.OffreSansReponse, name : 'Offre Sans Réponse'},
  {value : AdmissionDecision.NonComplete, name : 'Non Complété'},
  {value : AdmissionDecision.DejaAdmis, name : 'Déjà Admis'},
  {value : AdmissionDecision.AnalyseEnCours, name : 'Analyse En Cours'},
  {value : AdmissionDecision.InvalidDocuments, name : 'Documents Invalide'},
  {value : AdmissionDecision.Archive, name : 'Archivé'},
];

export enum ExamCenterStatus {
  Active = 1, 
  Inactive = 2
}

export enum CompetitionCenterStatus {
  Active = 1, 
  Inactive = 2
}

export enum InstitutionStatus {
  Active = 1, 
  Inactive = 2
}

export enum FacultyStatus {
  Active = 1, 
  Inactive = 2
}

export enum ProgramStatus {
  Active = 1, 
  Inactive = 2
}

export enum MatiereBACStatus {
  Active = 1, 
  Inactive = 2
}

export enum PaymentOperator {
  OrangeMoney = 'Orange', 
}

export enum PaymentStatus {
  Initiated = 1, // waiting for user entry
  Pending = 2,   // user has clicked on “Confirmer”, the transaction is in progress on Orange side
  Success = 3,   // payment is done
  Failed = 4,    // payment has failed
  Inconnu = 5
}

export const PaymentStatusFilterValues = [
  {code: PaymentStatus.Initiated, name: 'Non payé'},
  {code: PaymentStatus.Success, name: 'Paiement effectué'},
  {code: PaymentStatus.Failed, name: 'Paiement échoué'}
];

export const PaymentStatusValues = [
  {code: PaymentStatus.Initiated, name: 'INITIÉ'},
  {code: PaymentStatus.Pending, name: 'EN COURS'},
  {code: PaymentStatus.Success, name: 'SUCCÈS'},
  {code: PaymentStatus.Failed, name: 'ÉCHOUÉ'}
];

export const PaymentOperatorsFilterValues = [
  {code: PaymentOperator.OrangeMoney, name: PaymentOperator.OrangeMoney},
];

export enum TemplateType {
  All = 0,
  Email = 1,
  Sms = 2,
}

export const TemplateTypeValues = [
  {value : TemplateType.All, name : 'Les deux'},
  {value : TemplateType.Email, name : 'Email'},
  {value : TemplateType.Sms, name : 'Sms'},
];

export const TemplateTypeMap = TemplateTypeValues.reduce((acc, cur) => {
  acc[cur.value] = cur.name;
  return acc;
}, {});

export interface IUser {
  id?: string;
  identifiant?: string;
  name?: string;
  firstName?: string;
  email?: string;
  photo?: string;
  type?: string; 
  institutionId?: string;
  statusEtablissementCode?: string;
  employeRoleId?: string;
  employeRole?: string;
}

export interface IAppState {
  loggedInPerson?: IUser;
  loading?: boolean;
  student?: MetfpetServiceAgent.EtudiantProfileViewModel,
  searchState?: any;
  demandeur?: MetfpetServiceAgent.DemandeurProfileViewModel;
}

export enum AuthorityEnum {
  AdministrateurMinisteriel = 'administrateurMinisteriel',
  SuperviseurMinisteriel = 'superviseurMinisteriel',
  AgentMinisteriel = 'agentMinisteriel',
  DirectionsNationales = 'directionsNationales',
  Ministre = 'ministre',
  Administrateur = 'administrateur',
  ExamSupervisorMinisteriel = 'examSupervisorMinisteriel',
  ExamAgentMinisteriel = 'examAgentMinisteriel',
  AgrementSupervisorMinisteriel = 'agrementSupervisorMinisteriel',
  AgrementAgentMinisteriel = 'agrementAgentMinisteriel',
  SuperviseurUniversitaire = 'superviseurUniversitaire',
  AgentUniversitaire = 'agentUniversitaire',
  Student = 'etudiant',
  Demandeur = 'demandeur',
}

export enum LoginTypeEnum {
  Ministeriel = 'ministeriel',
  Institution = 'institution',
  Student = 'student',
  Demandeur = 'demandeur'
}

export enum DocumentStatus {
  Active = 1, 
  Inactive = 2
}

export enum AssociatedType
{
    Cursus = 1,
    Admission = 2
}

export const AssociatedTypeValues = [
  {value : AssociatedType.Cursus, name : 'Cursus'},
  {value : AssociatedType.Admission, name : 'Admission'},
];

export enum RequirePayment
{
    Oui = 1,
    Non = 2
}

export const RequirePaymentValues = [
  {value : RequirePayment.Oui, name : 'Oui'},
  {value : RequirePayment.Non, name : 'Non'},
];

export enum Commande
{
    Unique = 1,
    Multiple = 2
}

export const CommandeValues = [
  {value : Commande.Unique, name : 'Commande unique seulement'},
  {value : Commande.Multiple, name : 'Commande multiple autorisée'},
];

export enum Diffusion
{
    Imprime = 1,
    Mail = 2,
    ImprimeEtMail = 3
}

export const DiffusionValues = [
  {value : Diffusion.Imprime, name : 'Imprimé'},
  {value : Diffusion.Mail, name : 'Mail'},
  {value : Diffusion.ImprimeEtMail, name : 'Mail et imprimé'},
];

export enum DocumentTemplateStatus
{
    Disponible = 1,
    Indisponible = 2
}

export const DocumentTemplateStatusValues = [
  {value : DocumentTemplateStatus.Disponible, name : 'Disponible'},
  {value : DocumentTemplateStatus.Indisponible, name : 'Indisponible'},
];

export const BiometricStatusValues = [
  {value : 'Non biomtérisé', name : 'Non biomtérisé'},
  {value : 'Biométrisé', name : 'Biométrisé'},
];

export enum ImpressionDocumentStatus
{
    APayer = 1,
    Payee = 2,
    Delivree = 3
};

export enum AttachedDocumentAssociatedType
{
    ActeNaissance = 1,
    Photo = 2,
    Lettre = 3,
    Attestation = 4,
    DiplomeCEE = 5,
    CertificatVM = 6,
};

export enum AttachedDocumentStatus {
  Valide = 'Valide',
  Invalide = 'Invalide'
};

export const addPostOptions = (optionsList: Array<MetfpetServiceAgent.OptionRowViewModel>) => {
  let postPrimaire = new MetfpetServiceAgent.OptionRowViewModel();
  postPrimaire.id = '-1';
  postPrimaire.displayedName = 'Post-primaire';
  optionsList.push(postPrimaire);
  let postSecondaire = new MetfpetServiceAgent.OptionRowViewModel();
  postSecondaire.id = '-2';
  postSecondaire.displayedName = 'Post-secondaire';
  optionsList.push(postSecondaire);
  let autre = new MetfpetServiceAgent.OptionRowViewModel();
  autre.id = '-3';
  autre.displayedName = 'Autre';
  optionsList.push(autre);
  return optionsList;
};

export enum TypeApprenant {
  Guinneen = 1,
  Etranger = 2
};

export const TypeApprenantValues = [
  {value : TypeApprenant.Guinneen, name : 'Guinéen'},
  {value : TypeApprenant.Etranger, name : 'Étranger'},
];

export enum TypeBourse {
  Cummulative = 1,
  NonCummulative = 2
};

export const TypeBourseValues = [
  {value : TypeBourse.Cummulative, name : 'Oui'},
  {value : TypeBourse.NonCummulative, name : 'Non'},
];

export enum BourseStatus {
    Active = 1,
    Inactive = 2
};

export enum  ProgrammesType
{
    Tous = 1,
    Specifique = 2
};

export const ProgrammesTypeValues = [
  {value : ProgrammesType.Tous, name : 'Tous les programmes'},
  {value : ProgrammesType.Specifique, name : 'Programmes spécifiques seulement'},
];

export enum RaisonStatutBourseType {
    Automatique = 1,
    Manuel = 2,
    ManuelEtAutomatique = 3
};

export const RaisonStatutBourseTypeValues = [
  {value : RaisonStatutBourseType.Automatique, name : 'Automatique'},
  {value : RaisonStatutBourseType.Manuel, name : 'Manuel'},
  {value : RaisonStatutBourseType.ManuelEtAutomatique, name : 'Manuel et automatique'},
];

export enum RaisonStatutBourseStatus {
    Active = 1,
    Inactive = 2
};

export enum EtudiantBourseStatus {
    Active = 1,
    Inactive = 2
};

export enum PaiementBourseStatus {
    Payee = 1,
    APayer = 2,
    AVenir = 3,
    NonAdmissible = 4,
    Archive = 5
};

export const PaiementBourseStatusValues = [
  {value : PaiementBourseStatus.Payee, name : 'Payé'},
  {value : PaiementBourseStatus.APayer, name : 'A payer'},
  {value : PaiementBourseStatus.AVenir, name : 'A venir'},
  {value : PaiementBourseStatus.NonAdmissible, name : 'Non admissible'},
  {value : PaiementBourseStatus.Archive, name : 'Archivé'},
];

export enum MoisAnnee {
    Janvier = 1,
    Fevrier = 2,
    Mars = 3,
    Avril = 4,
    Mai = 5,
    Juin = 6,
    Juillet = 7,
    Aout = 8,
    Septembre = 9,
    Octobre = 10,
    Novembre = 11,
    Decembre = 12
};

export const MoisAnneeValues = [
  {value : MoisAnnee.Janvier, name : 'Janvier'},
  {value : MoisAnnee.Fevrier, name : 'Février'},
  {value : MoisAnnee.Mars, name : 'Mars'},
  {value : MoisAnnee.Avril, name : 'Avril'},
  {value : MoisAnnee.Mai, name : 'Mai'},
  {value : MoisAnnee.Juin, name : 'Juin'},
  {value : MoisAnnee.Juillet, name : 'Juillet'},
  {value : MoisAnnee.Aout, name : 'Août'},
  {value : MoisAnnee.Septembre, name : 'Septembre'},
  {value : MoisAnnee.Octobre, name : 'Octobre'},
  {value : MoisAnnee.Novembre, name : 'Novembre'},
  {value : MoisAnnee.Decembre, name : 'Décembre'},
];

export const CountryValues = [
  { value: "AF", name: "Afghanistan" },
  { value: "ZA", name: "Afrique du Sud" },
  { value: "AL", name: "Albanie" },
  { value: "DZ", name: "Algérie" },
  { value: "DE", name: "Allemagne" },
  { value: "AD", name: "Andorre" },
  { value: "AO", name: "Angola" },
  { value: "AI", name: "Anguilla" },
  { value: "AQ", name: "Antarctique" },
  { value: "AG", name: "Antigua-et-Barbuda" },
  { value: "AN", name: "Antilles Néerlandaises" },
  { value: "SA", name: "Arabie Saoudite" },
  { value: "AR", name: "Argentine" },
  { value: "AM", name: "Arménie" },
  { value: "AW", name: "Aruba" },
  { value: "AU", name: "Australie" },
  { value: "AT", name: "Autriche" },
  { value: "AZ", name: "Azerbaïdjan" },
  { value: "BS", name: "Bahamas" },
  { value: "BH", name: "Bahreïn" },
  { value: "BD", name: "Bangladesh" },
  { value: "BB", name: "Barbade" },
  { value: "BY", name: "Bélarus" },
  { value: "BE", name: "Belgique" },
  { value: "BZ", name: "Belize" },
  { value: "BJ", name: "Bénin" },
  { value: "BM", name: "Bermudes" },
  { value: "BT", name: "Bhoutan" },
  { value: "BO", name: "Bolivie" },
  { value: "BA", name: "Bosnie-Herzégovine" },
  { value: "BW", name: "Botswana" },
  { value: "BR", name: "Brésil" },
  { value: "BN", name: "Brunéi Darussalam" },
  { value: "BG", name: "Bulgarie" },
  { value: "BF", name: "Burkina Faso" },
  { value: "BI", name: "Burundi" },
  { value: "KH", name: "Cambodge" },
  { value: "CM", name: "Cameroun" },
  { value: "CA", name: "Canada" },
  { value: "CV", name: "Cap-vert" },
  { value: "CL", name: "Chili" },
  { value: "CN", name: "Chine" },
  { value: "CY", name: "Chypre" },
  { value: "CO", name: "Colombie" },
  { value: "KM", name: "Comores" },
  { value: "CR", name: "Costa Rica" },
  { value: "CI", name: "Côte d'Ivoire" },
  { value: "HR", name: "Croatie" },
  { value: "CU", name: "Cuba" },
  { value: "DK", name: "Danemark" },
  { value: "DJ", name: "Djibouti" },
  { value: "DM", name: "Dominique" },
  { value: "EG", name: "Égypte" },
  { value: "SV", name: "El Salvador" },
  { value: "AE", name: "Émirats Arabes Unis" },
  { value: "EC", name: "Équateur" },
  { value: "ER", name: "Érythrée" },
  { value: "ES", name: "Espagne" },
  { value: "EE", name: "Estonie" },
  { value: "FM", name: "États Fédérés de Micronésie" },
  { value: "US", name: "États-Unis" },
  { value: "ET", name: "Éthiopie" },
  { value: "RU", name: "Fédération de Russie" },
  { value: "FJ", name: "Fidji" },
  { value: "FI", name: "Finlande" },
  { value: "FR", name: "France" },
  { value: "GA", name: "Gabon" },
  { value: "GM", name: "Gambie" },
  { value: "GE", name: "Géorgie" },
  { value: "GS", name: "Géorgie du Sud et les Îles Sandwich du Sud" },
  { value: "GH", name: "Ghana" },
  { value: "GI", name: "Gibraltar" },
  { value: "GR", name: "Grèce" },
  { value: "GD", name: "Grenade" },
  { value: "GL", name: "Groenland" },
  { value: "GP", name: "Guadeloupe" },
  { value: "GU", name: "Guam" },
  { value: "GT", name: "Guatemala" },
  { value: "GN", name: "Guinée" },
  { value: "GQ", name: "Guinée Équatoriale" },
  { value: "GW", name: "Guinée-Bissau" },
  { value: "GY", name: "Guyana" },
  { value: "GF", name: "Guyane Française" },
  { value: "HT", name: "Haïti" },
  { value: "HN", name: "Honduras" },
  { value: "HK", name: "Hong-Kong" },
  { value: "HU", name: "Hongrie" },
  { value: "BV", name: "Île Bouvet" },
  { value: "CX", name: "Île Christmas" },
  { value: "IM", name: "Île de Man" },
  { value: "NF", name: "Île Norfolk" },
  { value: "FK", name: "Îles (malvinas) Falkland" },
  { value: "AX", name: "Îles Aland" },
  { value: "KY", name: "Îles Caïmanes" },
  { value: "CC", name: "Îles Cocos (Keeling)" },
  { value: "CK", name: "Îles Cook" },
  { value: "FO", name: "Îles Féroé" },
  { value: "HM", name: "Îles Heard et Mcdonald" },
  { value: "MP", name: "Îles Mariannes du Nord" },
  { value: "MH", name: "Îles Marshall" },
  { value: "UM", name: "Îles Mineures Éloignées des États-Unis" },
  { value: "SB", name: "Îles Salomon" },
  { value: "TC", name: "Îles Turks et Caïques" },
  { value: "VG", name: "Îles Vierges Britanniques" },
  { value: "VI", name: "Îles Vierges des États-Unis" },
  { value: "IN", name: "Inde" },
  { value: "ID", name: "Indonésie" },
  { value: "IQ", name: "Iraq" },
  { value: "IE", name: "Irlande" },
  { value: "IS", name: "Islande" },
  { value: "IL", name: "Israël" },
  { value: "IT", name: "Italie" },
  { value: "LY", name: "Jamahiriya Arabe Libyenne" },
  { value: "JM", name: "Jamaïque" },
  { value: "JP", name: "Japon" },
  { value: "JO", name: "Jordanie" },
  { value: "KZ", name: "Kazakhstan" },
  { value: "KE", name: "Kenya" },
  { value: "KG", name: "Kirghizistan" },
  { value: "KI", name: "Kiribati" },
  { value: "KW", name: "Koweït" },
  { value: "LS", name: "Lesotho" },
  { value: "LV", name: "Lettonie" },
  { value: "MK", name: "L'ex-République Yougoslave de Macédoine" },
  { value: "LB", name: "Liban" },
  { value: "LR", name: "Libéria" },
  { value: "LI", name: "Liechtenstein" },
  { value: "LT", name: "Lituanie" },
  { value: "LU", name: "Luxembourg" },
  { value: "MO", name: "Macao" },
  { value: "MG", name: "Madagascar" },
  { value: "MY", name: "Malaisie" },
  { value: "MW", name: "Malawi" },
  { value: "MV", name: "Maldives" },
  { value: "ML", name: "Mali" },
  { value: "MT", name: "Malte" },
  { value: "MA", name: "Maroc" },
  { value: "MQ", name: "Martinique" },
  { value: "MU", name: "Maurice" },
  { value: "MR", name: "Mauritanie" },
  { value: "YT", name: "Mayotte" },
  { value: "MX", name: "Mexique" },
  { value: "MC", name: "Monaco" },
  { value: "MN", name: "Mongolie" },
  { value: "MS", name: "Montserrat" },
  { value: "MZ", name: "Mozambique" },
  { value: "MM", name: "Myanmar" },
  { value: "NA", name: "Namibie" },
  { value: "NR", name: "Nauru" },
  { value: "NP", name: "Népal" },
  { value: "NI", name: "Nicaragua" },
  { value: "NE", name: "Niger" },
  { value: "NG", name: "Nigéria" },
  { value: "NU", name: "Niué" },
  { value: "NO", name: "Norvège" },
  { value: "NC", name: "Nouvelle-Calédonie" },
  { value: "NZ", name: "Nouvelle-Zélande" },
  { value: "OM", name: "Oman" },
  { value: "UG", name: "Ouganda" },
  { value: "UZ", name: "Ouzbékistan" },
  { value: "PK", name: "Pakistan" },
  { value: "PW", name: "Palaos" },
  { value: "PA", name: "Panama" },
  { value: "PG", name: "Papouasie-Nouvelle-Guinée" },
  { value: "PY", name: "Paraguay" },
  { value: "NL", name: "Pays-Bas" },
  { value: "PE", name: "Pérou" },
  { value: "PH", name: "Philippines" },
  { value: "PN", name: "Pitcairn" },
  { value: "PL", name: "Pologne" },
  { value: "PF", name: "Polynésie Française" },
  { value: "PR", name: "Porto Rico" },
  { value: "PT", name: "Portugal" },
  { value: "QA", name: "Qatar" },
  { value: "SY", name: "République Arabe Syrienne" },
  { value: "CF", name: "République Centrafricaine" },
  { value: "KR", name: "République de Corée" },
  { value: "MD", name: "République de Moldova" },
  { value: "CD", name: "République Démocratique du Congo" },
  { value: "LA", name: "République Démocratique Populaire Lao" },
  { value: "DO", name: "République Dominicaine" },
  { value: "CG", name: "République du Congo" },
  { value: "IR", name: "République Islamique d'Iran" },
  { value: "KP", name: "République Populaire Démocratique de Corée" },
  { value: "CZ", name: "République Tchèque" },
  { value: "TZ", name: "République-Unie de Tanzanie" },
  { value: "RE", name: "Réunion" },
  { value: "RO", name: "Roumanie" },
  { value: "GB", name: "Royaume-Uni" },
  { value: "RW", name: "Rwanda" },
  { value: "EH", name: "Sahara Occidental" },
  { value: "SH", name: "Sainte-Hélène" },
  { value: "LC", name: "Sainte-Lucie" },
  { value: "KN", name: "Saint-Kitts-et-Nevis" },
  { value: "SM", name: "Saint-Marin" },
  { value: "PM", name: "Saint-Pierre-et-Miquelon" },
  { value: "VC", name: "Saint-Vincent-et-les Grenadines" },
  { value: "WS", name: "Samoa" },
  { value: "AS", name: "Samoa Américaines" },
  { value: "ST", name: "Sao Tomé-et-Principe" },
  { value: "SN", name: "Sénégal" },
  { value: "CS", name: "Serbie-et-Monténégro" },
  { value: "SC", name: "Seychelles" },
  { value: "SL", name: "Sierra Leone" },
  { value: "SG", name: "Singapour" },
  { value: "SK", name: "Slovaquie" },
  { value: "SI", name: "Slovénie" },
  { value: "SO", name: "Somalie" },
  { value: "SD", name: "Soudan" },
  { value: "LK", name: "Sri Lanka" },
  { value: "SE", name: "Suède" },
  { value: "CH", name: "Suisse" },
  { value: "SR", name: "Suriname" },
  { value: "SJ", name: "Svalbard et Île Jan Mayen" },
  { value: "SZ", name: "Swaziland" },
  { value: "TJ", name: "Tadjikistan" },
  { value: "TW", name: "Taïwan" },
  { value: "TD", name: "Tchad" },
  { value: "TF", name: "Terres Australes Françaises" },
  { value: "IO", name: "Territoire Britannique de l'Océan Indien" },
  { value: "TH", name: "Thaïlande" },
  { value: "TL", name: "Timor-Leste" },
  { value: "TG", name: "Togo" },
  { value: "TK", name: "Tokelau" },
  { value: "TO", name: "Tonga" },
  { value: "TT", name: "Trinité-et-Tobago" },
  { value: "TN", name: "Tunisie" },
  { value: "TM", name: "Turkménistan" },
  { value: "TR", name: "Turquie" },
  { value: "TV", name: "Tuvalu" },
  { value: "UA", name: "Ukraine" },
  { value: "UY", name: "Uruguay" },
  { value: "VU", name: "Vanuatu" },
  { value: "VE", name: "Venezuela" },
  { value: "VN", name: "Viet Nam" },
  { value: "WF", name: "Wallis et Futuna" },
  { value: "YE", name: "Yémen" },
  { value: "ZM", name: "Zambie" },
  { value: "ZW", name: "Zimbabwe" },
];

export enum ChampType {
  ShortText = 1,
  LongText = 2,
  CheckBox = 3,
  RadioButton = 4,
  DropdownList = 5
};

export const ChampTypeValues = [
  {value : ChampType.ShortText, name : 'Champs de texte court'},
  {value : ChampType.LongText, name : 'Champs de texte long'},
  {value : ChampType.CheckBox, name : 'Cases à cocher'},
  {value : ChampType.RadioButton, name : 'Boutons radio'},
  {value : ChampType.DropdownList, name : 'Liste déroulante'},
];

export enum ChampStatus {
  Active = 1, 
  Inactive = 2
};

export enum ValeurStatus {
  Active = 1, 
  Inactive = 2
};
export enum DemandeStatus {
  APayer = 1,
  AnalyseEnCours = 2,
  AnnuleModification = 3,
  CorrectionsNecessaire = 4,
  Delivre = 5,
  Refuse = 6,
  Suspendu = 7,
  Annule = 8,
  Reactive = 9,
};

export enum DTemplateStatus {
  Active = 1,
  Inactive = 2
}

export enum DemandeurStatus {
  CreationEnAttenteDesDocuments = 0,
  CreationAnalyse = 1,
  ModificationAnalyse = 2,
  BiometrieAPrendre = 3,
  Approuve = 4,
  Refuse = 5
};

export enum TypeDemandeStatus {
  Active = 1, 
  Inactive = 2
};

export enum ReferenceTemplateStatus {
  Oui = 1,
  Non = 2
};

export enum TarificationStatus {
  Active = 1, 
  Inactive = 2
};

export enum AgrementationStatus {
  Active = 1, 
  Inactive = 2
};

export enum FormulaireSectionChampStatus{
    Active = 1,
    Inactive = 2
}

export enum TimeUnits {
  Second = 1,
  Minute = 2,
  Hour = 3,
  Day = 4,
  Week = 5,
  Month = 6,
  Year = 7
};

export const TimeUnitsValues = [
  {value : TimeUnits.Day, name : 'Jours'},
  {value : TimeUnits.Week, name : 'Semaines'},
  {value : TimeUnits.Month, name : 'Mois'},
  {value : TimeUnits.Year, name : 'Années'},
];

export enum PersonnelDocumentStatus {
  Active = 1, 
  Inactive = 2
};

export enum PersonnelMemberStatus {
  Active = 1, 
  Inactive = 2
};

export enum PersonnelType {
  Administratif = 1,
  Formateur = 2
};

export const DemandeurStatusValues = [
{value : DemandeurStatus.CreationEnAttenteDesDocuments, name : 'Création - En attente des documents'},
{value : DemandeurStatus.CreationAnalyse, name : 'Création - Analyse'},
{value : DemandeurStatus.ModificationAnalyse, name : 'Modification - Analyse'},
{value : DemandeurStatus.BiometrieAPrendre, name : 'Biométrie à prendre'},
{value : DemandeurStatus.Approuve, name : 'Approuvé'},
{value : DemandeurStatus.Refuse, name : 'Refusé'},
];

export enum DocumentSectionStatus
{
    Active = 1,
    Inactive = 2
}

export function GetDemandeStatusEquivalence(demandeStatus: DemandeStatus){
  switch (demandeStatus) {
    case DemandeStatus.AnnuleModification:
      return "Annulé - modification"
    case DemandeStatus.AnalyseEnCours:
      return "Analyse en cours";
    case DemandeStatus.APayer:
      return "A payer"
    case DemandeStatus.Delivre:
      return "Delivré"
    case DemandeStatus.Refuse:
      return "Refusé";
    case DemandeStatus.CorrectionsNecessaire:
      return "Correction necessaire";
    case DemandeStatus.Annule:
      return "Annulé";
    case DemandeStatus.Reactive:
      return "Réactivé";
    case DemandeStatus.Suspendu:
      return "Suspendu";
    default:
      return "Inconnu";
  }
};

export enum DemandeProcessStepStatus {
  AnalyseEnCours = 1,
  CorrectionsNecessaire = 2,
  Valide = 3,
  Refuse = 4
};

export enum DemandePersonnelMemberStatus {
  Active = 1, 
  Inactive = 2
};

export const DemandeStatusValues = [
  {value : DemandeStatus.APayer, name : GetDemandeStatusEquivalence(DemandeStatus.APayer)},
  {value : DemandeStatus.AnalyseEnCours, name : GetDemandeStatusEquivalence(DemandeStatus.AnalyseEnCours)},
  {value : DemandeStatus.AnnuleModification, name : GetDemandeStatusEquivalence(DemandeStatus.AnnuleModification)},
  {value : DemandeStatus.CorrectionsNecessaire, name : GetDemandeStatusEquivalence(DemandeStatus.CorrectionsNecessaire)},
  {value : DemandeStatus.Delivre, name : GetDemandeStatusEquivalence(DemandeStatus.Delivre)},
  {value : DemandeStatus.Refuse, name : GetDemandeStatusEquivalence(DemandeStatus.Refuse)},
  {value : DemandeStatus.Annule, name : GetDemandeStatusEquivalence(DemandeStatus.Annule)},
  {value : DemandeStatus.Reactive, name : GetDemandeStatusEquivalence(DemandeStatus.Reactive)},
  {value : DemandeStatus.Suspendu, name : GetDemandeStatusEquivalence(DemandeStatus.Suspendu)},
];

export const TypeDataFormation = {
  Langues : 'Langue',
  NiveauEtude : 'Niveau d’étude',
  NiveauAcces : 'Niveau d’accès',
  TypeAdmission : 'Type d’admission',
  TypeDiplome : 'Type de diplôme',
  TypeDiscipline : 'Type de discipline',
  TypeProgramme : 'Type de filière'
};

export enum EmployeRoleStatus{
  Active = 1,
  Inactive = 2
}

export enum ProgrammeStatus
{
    Active = 1,
    Inactive = 2
}

export enum DisciplineStatus
{
    Active = 1,
    Inactive = 2
}

export enum FinalExamenStatusResult {
  EnDerniereAnnee = 1,
  AEntrer = 2,
  ACompleter = 3,
  Complet = 4,
  Redoublant = 5
};

export const FinalExamenStatusResultValues = [
  {value : FinalExamenStatusResult.EnDerniereAnnee, name : 'En dernière année'},
  {value : FinalExamenStatusResult.AEntrer, name : 'A entrer'},
  {value : FinalExamenStatusResult.ACompleter, name : 'A compléter'},
  {value : FinalExamenStatusResult.Complet, name : 'Complet'},
  {value : FinalExamenStatusResult.Redoublant, name : 'Redoublant'},
];

export enum FinalExamenStatusGlobal
{
    EnDerniereAnnee = 1,
    ResultatAEntrer = 2,
    APublier = 3,
    Admis = 4,
    NonAdmis = 5,
    Redoublant = 6,
};

export const FinalExamenStatusGlobalValues = [
  {value : FinalExamenStatusGlobal.EnDerniereAnnee, name : 'En dernière année'},
  {value : FinalExamenStatusGlobal.ResultatAEntrer, name : 'Résultat à entrer'},
  {value : FinalExamenStatusGlobal.APublier, name : 'A publier'},
  {value : FinalExamenStatusGlobal.Admis, name : 'Admis'},
  {value : FinalExamenStatusGlobal.NonAdmis, name : 'Non admis'},
  {value : FinalExamenStatusGlobal.Redoublant, name : 'Redoublant'},
];

export enum FinalExamenCentreStatus {
  Active = 1,
  Inactive = 2
};

export enum FinalExamenRapportType
{
    RelevesDeNotes = 1,
    ListeDesCandidats = 2,
    ListeDesRedoublants = 3,
    ListeDesAdmis = 4,
    SurLesResultats = 5,
    NationalProgramme = 6,
    ListeDesCandidatsPdf = 7,
    AdmisParProgramme = 8,
    Statistiques = 9,
    StatistiquesBeforePublish = 10,
    ListeDesCandidatsByProgrammeAndInstitutionPdf = 11
};

export const FinalExamenRapportTypeValues = [
  {value : FinalExamenRapportType.RelevesDeNotes, name : 'Relevés de notes apprenant et par institution'},
  {value : FinalExamenRapportType.ListeDesCandidats, name : 'Liste des candidats par centre d’examen'},
  {value : FinalExamenRapportType.ListeDesRedoublants, name : 'Liste des redoublants par ordre et institution'},
  {value : FinalExamenRapportType.ListeDesAdmis, name : 'Liste des admis par ordre et institution'},
  {value : FinalExamenRapportType.SurLesResultats, name : 'Rapport sur les résultats'},
  {value : FinalExamenRapportType.NationalProgramme, name : 'Liste des admis par ordre et programmes (National)'},
  {value : FinalExamenRapportType.ListeDesCandidatsPdf, name : 'Liste d’affichage des candidats aux examens (PDF)'},
  {value : FinalExamenRapportType.AdmisParProgramme, name : 'Notes de service - admis par programme'},
  {value : FinalExamenRapportType.Statistiques, name : 'Statistique complète - par option et filières de formation'},
  {value : FinalExamenRapportType.StatistiquesBeforePublish, name : 'Statistique complète - par option et filières de formation (pré publication)'},
  {value : FinalExamenRapportType.ListeDesCandidatsByProgrammeAndInstitutionPdf, name : 'Liste d’affichage des candidats aux examens par programme et institution (PDF)'},
];

export enum DiplomeStatus
{
    Inscrit = 1,
    AdmissibleDiplomation = 2,
    Diplome = 3,
    DiplomeGenere = 4,
    Archive = 5
}

export const DiplomeStatusValues = [
  {value : DiplomeStatus.Inscrit, name : "Inscrit"},
  {value : DiplomeStatus.AdmissibleDiplomation, name : "Diplôme admissible"},
  {value : DiplomeStatus.Diplome, name : "Diplomé"},
  {value : DiplomeStatus.DiplomeGenere, name : "Diplôme généré"}, 
  {value : DiplomeStatus.Archive, name : "Archivé"}, 
]

export enum TypeDiplomeStatus {
  Active = 1, 
  Inactive = 2
}

export enum TypeExamen {
    General = 1,
    APC = 2,
}

export const TypeExamenValues = [
  {value : TypeExamen.General, name : 'Général'},
  {value : TypeExamen.APC, name : 'APC'},
];

export enum TypeNote {
  Sur20 = 1,
  Sur100 = 2,
}

export const TypeNoteValues = [
  {value : TypeNote.Sur20, name : 'Sur 20'},
  {value : TypeNote.Sur100, name : 'Sur 100'},
];

export enum MessageType {
  ExamenCandidature = 1,
  ExamenResultats = 2
};

export enum ConcoursMessageType {
  ConcoursCandidature = 1,
  ConcoursResultats = 2
};

export enum ConcoursStatusGlobal {
  ResultatAEntrer = 1,
  AOrienter = 2,
  EnFilAttente = 3,
  OffreEnvoyee = 4,
  NonAdmis = 5,
  OffreRefusee = 6,
  OffreAcceptee = 7
};

export const ConcoursStatusGlobalValues = [
  {value : ConcoursStatusGlobal.ResultatAEntrer, name : 'Résultat à entrer'},
  {value : ConcoursStatusGlobal.AOrienter, name : 'A orienter'},
  {value : ConcoursStatusGlobal.EnFilAttente, name : 'En fil d’attente'},
  {value : ConcoursStatusGlobal.OffreEnvoyee, name : 'Offre envoyée'},
  {value : ConcoursStatusGlobal.NonAdmis, name : 'Non admis'},
  {value : ConcoursStatusGlobal.OffreRefusee, name : 'Offre refusée'},
  {value : ConcoursStatusGlobal.OffreAcceptee, name : 'Offre acceptée'},
];

export enum ConcoursStatusResult {
  EnDerniereAnnee = 1,
  AEntrer = 2,
  ACompleter = 3,
  Complet = 4,
};

export const ConcoursStatusResultValues = [
  {value : ConcoursStatusResult.EnDerniereAnnee, name : 'En dernière année'},
  {value : ConcoursStatusResult.AEntrer, name : 'A entrer'},
  {value : ConcoursStatusResult.ACompleter, name : 'A compléter'},
  {value : ConcoursStatusResult.Complet, name : 'Complet'},
];

export enum ConcoursRapportType
{
  ListeDesCandidats = 1,
  ListeDesEnFilAttente = 2,
  OffreAccepteeParConcours = 3,
  NotesMoyenne = 4,
  ListeDesInstitutions = 5,
  ListeDesAdmissions = 6,
  RapportCompletSurLesConcours = 7
};

export const ConcoursRapportTypeValues = [
  {value : ConcoursRapportType.ListeDesCandidats, name : 'Candidats par centre de concours et type de concours'},
  {value : ConcoursRapportType.ListeDesEnFilAttente, name : 'En fil d’attente par centre de concours et type de concours'},
  {value : ConcoursRapportType.OffreAccepteeParConcours, name : 'Offre acceptée par concours'},
  {value : ConcoursRapportType.NotesMoyenne, name : 'Notes moyenne résultats par concours'},
  {value : ConcoursRapportType.ListeDesInstitutions, name : 'Liste des institutions et programmes avec statut de quotas'},
  {value : ConcoursRapportType.ListeDesAdmissions, name : 'Liste des admissions'},
  {value : ConcoursRapportType.RapportCompletSurLesConcours, name : 'Rapport complet sur les concours'},
];

export const SUCCESS_MOYENNE_MAX = 20;

export const MoyenneValues = Array.from({ length: SUCCESS_MOYENNE_MAX + 1}, (_, i) => ({
    value: i,
    name: i.toString() + '/' + SUCCESS_MOYENNE_MAX
}));

export enum ConcoursStatus {
  Active = 1, 
  Inactive = 2
}

export enum ActivityAreaStatus {
  Active = 1, 
  Inactive = 2
}

export enum PaymentReceiptStatus {
  AAjouter = 1,
  Enregister = 2
}

export enum StudentYear {
  Tous = 1,
  LastYear = 2,
  Other = 3,
};

export const StudentYearValues = [
  {value : StudentYear.Tous, name : 'Tous les apprenants'},
  {value : StudentYear.LastYear, name : 'En dernière année'},
  {value : StudentYear.Other, name : 'Autre que dernière année'},
];

export enum FinalExamenStatistiquesRowType
{
    Program = 1,
    TypeDiplome = 2,
    Total = 3,
};

export enum OtpStatus 
{
  NotVerified = 1,
  HasStatusAPayerAndNotVerified = 2,
  isPending = 3,
  isVerified = 4,
  NoNeedOtpVerification = 5
}

export enum SchoolKitStatus {
  Active = 1,
  Inactive = 2
};

export enum ReceiptFormType {
  Upload = 1,
  Generate = 2
};

export enum SchoolSupplyStatus {
  Active = 1,
  Inactive = 2
};

export enum ReasonStatus {
  Active = 1,
  Inactive = 2
};

export enum ObservationType {
  Good = 1,
  Medium = 2,
  Bad = 3
};

export const ObservationTypeValues = [
  {value : ObservationType.Good, name : 'Bon état'},
  {value : ObservationType.Medium, name : 'État moyen'},
  {value : ObservationType.Bad, name : 'Mauvais état'}
];

export enum RedoublementStatus {
  Redoublement = 1,
  NonRedoublant = 2,
  Tous = 3,
};

export const RedoublementStatusValues = [
  {value : RedoublementStatus.Redoublement, name : 'Redoublement'},
  {value : RedoublementStatus.NonRedoublant, name : 'Non redoublant'},
  {value : RedoublementStatus.Tous, name : 'Tous'},
];

export enum Regime {
    Internat = 1,
    Externat = 2
};

export const RegimeValues = [
  {value : Regime.Internat, name : 'Internat'},
  {value : Regime.Externat, name : 'Externat'}
];
