import { Injectable } from '@angular/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { Moment } from 'moment';
import * as moment from 'moment';

@Injectable()
export class CustomDateAdapter extends MomentDateAdapter {

  public dateFormat = 'DD/MM/YYYY';

  parse(value: any, parseFormat: string): Moment | null {
    let dateFormat: string | string[] = this.dateFormat || parseFormat;
    if (value && typeof value == 'string') {
      if (moment(value, dateFormat).format(dateFormat) !== value) {
        return null;
      }
      return moment.utc(value, dateFormat).locale(this.locale);
    }
    return value ? moment(value).locale(this.locale) : null;
  }

  format(date: Moment, displayFormat: string): string {
    let dateFormat: string | string[] = this.dateFormat || displayFormat;
    return super.format(date.utc(), dateFormat);
  }

  createDate(year: number, month: number, date: number): Moment {
    // Moment.js will create an invalid date if any of the components are out of bounds, but we
    // explicitly check each case so we can throw more descriptive errors.
    if (month < 0 || month > 11) {
      throw Error(`Invalid month index "${month}". Month index has to be between 0 and 11.`);
    }

    if (date < 1) {
      throw Error(`Invalid date "${date}". Date has to be greater than 0.`);
    }

    let result = moment.utc({ year, month, date }).locale(this.locale);

    // If the result isn't valid, the date must have been out of bounds for this month.
    if (!result.isValid()) {
      throw Error(`Invalid date "${date}" for month with index "${month}".`);
    }

    return result;
  }

}