import { createReducer, on } from '@ngrx/store';
import { AuthActions } from '../actions';

export const authReducer = createReducer(
  null,

  on(AuthActions.loginSuccess, (state, action) => {
    return action.user;
  }),

  on(AuthActions.getLoggedInPerson, (state, action) => {
    return state.loggedInPerson;
  })
);