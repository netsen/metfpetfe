import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AuthService } from '../../services/';
import { logout } from '../actions/';

@Injectable()
export class AuthEffects {

  constructor(
    private _actions$: Actions,
    private _authService: AuthService,
    private _router: Router
  ) {

  }

  logout$ = createEffect(() =>
    this._actions$.pipe(
      ofType(logout),
      tap(() => {
        this._router.navigate([this._authService.signOut()]);
      })
    ), { dispatch: false }
  );

}