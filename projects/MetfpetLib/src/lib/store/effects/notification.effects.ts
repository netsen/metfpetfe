import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { MatSnackBar} from '@angular/material/snack-bar';
import { SnackBarComponent } from '../../components/snackbar';
import { NotificationActions } from '../actions';

@Injectable()
export class NotificationEffects {

  private _commonConfig = {
    duration: 3000,
    verticalPosition: 'top' as const
  };

  constructor(
    private _actions$: Actions,
    private _matSnackBar: MatSnackBar
  ) { }

  showSuccess$ = createEffect(() =>
    this._actions$.pipe(
      ofType(NotificationActions.showSuccess),
      map((action: any) => action.message),
      tap((message: string) => 
        this._matSnackBar.openFromComponent(SnackBarComponent, Object.assign({}, this._commonConfig, {
          data: {'message': message ? message : 'Modifications enregistrées avec succès', 'className': 'success'}
        }))
      )
    ), {dispatch: false}
  );

  showError$ = createEffect(() =>
    this._actions$.pipe(
      ofType(NotificationActions.showError),
      map((action: any) => action.message),
      tap((message: string) => 
        this._matSnackBar.openFromComponent(SnackBarComponent, Object.assign({}, this._commonConfig, {
          duration: 3000,
          data: {'message': message ? message : 'Erreur de serveur', 'className': 'error'}
        }))
      )
    ), {dispatch: false}
  );

  showException$ = createEffect(() =>
    this._actions$.pipe(
      ofType(NotificationActions.showException),
      map((action: any) => action.error),
      tap((error: any) => 
        this._matSnackBar.openFromComponent(SnackBarComponent, Object.assign({}, this._commonConfig, {
          duration: 3000,
          data: {'message': error ? (error.isApiException ? (JSON.parse(error.response).Message ? JSON.parse(error.response).Message : error.response) : error) : 'Erreur de serveur', 'className': 'error'}
        }))
      )
    ), {dispatch: false}
  );
}
