import { createAction, props } from '@ngrx/store';
import { ICredentials } from '../../interfaces';

export const editProfile = createAction(
  '[User] EditProfile',
);

export const modifyPassword = createAction(
  '[User] ModifyPassword',
);

