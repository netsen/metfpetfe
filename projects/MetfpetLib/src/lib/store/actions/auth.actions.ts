import { createAction, props } from '@ngrx/store';
import { ICredentials, IUser } from '../../interfaces';

export const login = createAction(
  '[Authentication] User login',
  props<{credentials: ICredentials}>()
);

export const loginSuccess = createAction(
  '[Authentication] User login success',
  props<{user: IUser}>()
);

export const loginFailure = createAction(
  '[Authentication] User login failure',
  props<{error: any}>()
);

export const logout = createAction(
  '[Authentication] User logout'
);

export const getLoggedInPerson = createAction(
  '[Authentication] getLoggedInPerson'
);
