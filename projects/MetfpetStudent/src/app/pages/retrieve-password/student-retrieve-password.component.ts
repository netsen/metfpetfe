import {
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';

@Component({
  templateUrl: './student-retrieve-password.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentRetrievePasswordComponent {

  userType: string;

  constructor(
    private _cd: ChangeDetectorRef,
    private _fb: FormBuilder,
    private _store: Store<any>,
  ) {
    this.userType = "etudiant";
  }
}