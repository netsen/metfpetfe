import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Component, 
  OnInit,
} from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import {
  emailValidator,
  AppSettings,
  Settings,
  IAppState,
  validateForm,
  showSuccess,
  showError,
  MetfpetServiceAgent,
  showException,
  DialogService,
  SeverityEnum,
  FinalExamenStatusGlobal
} from 'MetfpetLib';
import { Store, select } from '@ngrx/store';
import { selectCurrentStudent } from '../../shared/store/selectors';

@Component({
  templateUrl: './student-examens.component.html', 
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentExamensComponent implements OnInit {

  form: FormGroup;
  settings: Settings;
  title: string;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  finalExamenEtudiantProfileViewModel: MetfpetServiceAgent.FinalExamenEtudiantProfileViewModel;
  finalExamenEtudiant: MetfpetServiceAgent.FinalExamenEtudiantViewModel;
  finalExamenEtudiantNotes: Array<MetfpetServiceAgent.FinalExamenEtudiantNoteDTO>;
  isShowTheExamenResult: boolean;
  loadingIndicator: boolean;
  
  constructor(
    public appSettings: AppSettings, 
    private _formBuilder: FormBuilder, 
    private _cd: ChangeDetectorRef,
    private _store: Store<IAppState>,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings; 
    this.form = this._formBuilder.group({
    });
  }

  ngOnInit() {
    this._store.pipe(select(selectCurrentStudent)).subscribe(data => {
      this.student = Object.assign({}, data);
      if (!data) return;
      this.title = "Examen de sortie";
      this.loadFinalExamenEtudiant(this.student.id);
    });
  }

  private loadFinalExamenEtudiant(studentId: string) {
    this.isShowTheExamenResult = false;
    this._metfpetService.getPublishedFinalExamenEtudiantProfile(studentId).subscribe(data => {
      this.finalExamenEtudiantProfileViewModel = data;
      if (this.finalExamenEtudiantProfileViewModel) {
        this.finalExamenEtudiant = this.finalExamenEtudiantProfileViewModel.finalExamenEtudiant;
        // if (this.finalExamenEtudiant && (this.finalExamenEtudiant.statusGlobal === <any> FinalExamenStatusGlobal.Admis || this.finalExamenEtudiant.statusGlobal === <any> FinalExamenStatusGlobal.NonAdmis)) 
        // {
        //   this.isShowTheExamenResult = true;
        // }
        //this.finalExamenEtudiantNotes = this.finalExamenEtudiantProfileViewModel.finalExamenEtudiantNotes;
        this.finalExamenEtudiantNotes = [];
      }
      this._cd.markForCheck();
    });
  }

  ngAfterViewInit() {
    setTimeout(() => this.settings.loadingSpinner = false);
  }
  
  allowDownloadTranscript() {
    //return this.finalExamenEtudiant && this.finalExamenEtudiant.isPublished && this.finalExamenEtudiant.transcriptUrl;
    return false;
  }

  printResults(): void {
    // if (this.finalExamenEtudiant && this.finalExamenEtudiant.isPublished && this.finalExamenEtudiant.transcriptUrl) {
    //   window.open(this.finalExamenEtudiant.transcriptUrl, "_blank");
    // }
  }
}