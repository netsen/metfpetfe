import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import {
  MetfpetServiceAgent
} from 'MetfpetLib';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  templateUrl: './student-resultats-du-concours.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentResultatsDuConcoursDialog {

  form: FormGroup;
  _model: MetfpetServiceAgent.ConcoursEtudiantDTO;

  constructor(
    public dialogRef: MatDialogRef<StudentResultatsDuConcoursDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _formBuilder: FormBuilder,
    private _cd: ChangeDetectorRef,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.form = this._formBuilder.group({
      'id': null
    });
    if (data.id) {
      this._metfpetService.getConcoursEtudiantDetail(data.id).subscribe((data)=>{
        this._model = data;
        this._cd.markForCheck();
      })
    }
  }

  public onClose(): void {
    this.dialogRef.close();
  }
}