import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { DatePipe } from '@angular/common';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import {
  DialogService,
  SeverityEnum,
  showSuccess,
  showError,
  showLoading, 
  hideLoading,
  validateForm,
  MetfpetServiceAgent,
  PaymentServiceAgent,
  PAYMENT_ERROR,
  showException,
} from 'MetfpetLib';

@Component({
  templateUrl: './student-admission-payment-confirmation-code.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentAdmissionPaymentConfirmationCodeDialog {

  form: FormGroup;
  cart: MetfpetServiceAgent.SelectedProgrammeInfo;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  payment: PaymentServiceAgent.AdmissionPaymentDTO;

  constructor(
    public dialogRef: MatDialogRef<StudentAdmissionPaymentConfirmationCodeDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _cd: ChangeDetectorRef,
    private _datePipe: DatePipe, 
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _paymentService: PaymentServiceAgent.HttpService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.student = data.student;
    this.cart = data.cart;
    this.payment = data.payment;
    this.form = this._formBuilder.group({
      'confirmationCode': [null, Validators.required],
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onGetConfirmationCode(): void {
    this._store.dispatch(showLoading());
    this._paymentService.getConfirmationCode(this.payment.id).subscribe(
      (result) =>  {
        if (result) {
          this.form.get('confirmationCode').setValue(result);
          this._cd.markForCheck();
        }
      },
      error => this._store.dispatch(showException({error: error}))
    )
    .add(() => this._store.dispatch(hideLoading()));
  }

  onCompletePayment() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._paymentService.completeOrangePayment(PaymentServiceAgent.CompletePayment.fromJS(
        {
          notif_token: this.form.get('confirmationCode').value,
          paymentId: this.payment.id,
          paymentType: PaymentServiceAgent.PaymentType.Admission
        }
      )).subscribe(
        () =>  {
          this.dialogRef.close(true);
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
    }
  }
}