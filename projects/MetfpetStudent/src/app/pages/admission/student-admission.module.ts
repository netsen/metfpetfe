import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { StudentAdmissionTabComponent } from './student-admission-tab.component';
import { StudentAdmissionManageComponent } from './student-admission-manage.component';
import { StudentAdmissionParticipateComponent } from './student-admission-participate.component';
import { StudentAdmissionSelectProgramsComponent } from './student-admission-select-programs.component';
import { StudentAdmissionSelectProgramsDialog } from './student-admission-select-programs.dialog';
import { StudentAdmissionPaymentDialog } from './student-admission-payment.dialog';
import { StudentAdmissionDecisionDialog } from './student-admission-decision.dialog';
import { StudentAdmissionDecisionConfirmDialog } from './student-admission-decision-confirm.dialog';
import { StudentAdmissionPaymentConfirmationCodeDialog } from './student-admission-payment-confirmation-code.dialog';
import { StudentTransmissionDocumentDialog } from './student-transmission-document.dialog';
import { StudentAdmissionInvalidDocumentsDecisionDialog } from './student-admission-invalid-documents-decision.dialog';
import { StudentConcoursComponent } from './student-concours.component';
import { StudentResultatsDuConcoursDialog } from './student-resultats-du-concours.dialog';

export const routes = [
  {
    path: '',
    component: StudentAdmissionTabComponent,
    children: [
      {
        path: '',
        redirectTo: 'participate',
        pathMatch: 'full'
      },
      { 
        path: 'participate', 
        component: StudentAdmissionParticipateComponent, 
        data: { breadcrumb: 'Participer à la session d’admission' }
      },
      { 
        path: 'concours', 
        component: StudentConcoursComponent, 
        data: { breadcrumb: 'Concours d’entrée' }
      },
      { 
        path: 'manage', 
        component: StudentAdmissionManageComponent, 
        data: { breadcrumb: 'Suivi de mes demandes' }
      },
    ]
  },
  {
    path: 'select-programs/:type', 
    component: StudentAdmissionSelectProgramsComponent, 
    data: { breadcrumb: 'Sélectionner les programmes' }
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    StudentAdmissionTabComponent,
    StudentAdmissionManageComponent,
    StudentAdmissionParticipateComponent,
    StudentAdmissionSelectProgramsComponent,
    StudentAdmissionSelectProgramsDialog,
    StudentAdmissionDecisionDialog,
    StudentAdmissionPaymentDialog,
    StudentAdmissionDecisionConfirmDialog,
    StudentAdmissionPaymentConfirmationCodeDialog,
    StudentTransmissionDocumentDialog,
    StudentAdmissionInvalidDocumentsDecisionDialog,
    StudentConcoursComponent,
    StudentResultatsDuConcoursDialog
  ]
})
export class StudentAdmissionModule {}
