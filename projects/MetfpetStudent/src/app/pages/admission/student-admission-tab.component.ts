import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

export const Tabs = [
  { 
    routerLink: '/' + UiPath.student.admission.participate, 
    label: 'Participer à la session d’admission'
  },
  {
    routerLink: '/' + UiPath.student.admission.concours, 
    label: 'Concours d’entrée'
  },
  {
    routerLink: '/' + UiPath.student.admission.manage, 
    label: 'Suivi de mes demandes'
  },
];

@Component({
  selector: 'app-root',
  templateUrl: './student-admission-tab.component.html',
})
export class StudentAdmissionTabComponent {
  title = 'Admission - Participer à la session d’admission';
  navs = Tabs;
  routerEventSubscription: Subscription;

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
        let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
        if (selectedTab) {
          this.title = 'Admission - ' + selectedTab.label;
        }
    });
  }

  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}