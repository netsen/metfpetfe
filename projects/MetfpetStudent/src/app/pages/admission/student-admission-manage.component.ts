import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef,
  Component} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { startWith, tap } from 'rxjs/operators/';
import { Store, select } from '@ngrx/store';
import { selectCurrentStudent } from '../../shared/store/selectors/';
import {
  AppSettings,
  BaseTableComponent,
  MetfpetServiceAgent,
  PerfectScrollService,
  AdmissionInstitutionStatusValues,
  DialogService,
  showLoading,
  showSuccess,
  showError,
  hideLoading,
  IAppState,
  AdmissionDecision,
  AdmissionDecisionValues,
  SUR_CONCOURS,
  showException,
  SeverityEnum,
  AdmissionInstitutionStatus
} from 'MetfpetLib';
import { StudentAdmissionDecisionDialog } from './student-admission-decision.dialog';
import { StudentAdmissionDecisionConfirmDialog } from './student-admission-decision-confirm.dialog';
import { Guid } from 'guid-typescript';
import { StudentAdmissionInvalidDocumentsDecisionDialog } from './student-admission-invalid-documents-decision.dialog';
import { StudentActions } from '../../shared/store/actions';
import { StudentAdmissionPaymentDialog } from './student-admission-payment.dialog';
import { StudentAdmissionPaymentConfirmationCodeDialog } from './student-admission-payment-confirmation-code.dialog';

@Component({
  templateUrl: './student-admission-manage.component.html',
  styleUrls: ['./student-admission-manage.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StudentAdmissionManageComponent extends BaseTableComponent {

  regionList: Array<MetfpetServiceAgent.Region>;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  anneeList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  typeAdmissionList: Array<MetfpetServiceAgent.TypeAdmission>;
  statusList: Array<any>;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  currentSession: MetfpetServiceAgent.SessionAdmissionDTO;
  toPayProgrammeInfo: MetfpetServiceAgent.ToPayProgrammeInfo;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<IAppState>,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialogService: DialogService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.sort = {prop: 'etablissement', dir: 'asc'};
    this.statusList = AdmissionInstitutionStatusValues;
  }

  ngOnInit() {
    this._store.pipe(select(selectCurrentStudent)).subscribe(student => {
      if (student) {
        setTimeout(() => {
          this.student = student;
          this.searchForm.get('etudiant').setValue(student.id);
          this.triggerSearch();
          this._getToPayProgrammeInfo();
        })
      }
    });
    
    this._metfpetService.getCurrentSessionAdmission().subscribe(
      (data: MetfpetServiceAgent.SessionAdmissionDTO) => {
        this.currentSession = data;
      }
    );

    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cd.markForCheck(); 
    });

    this._metfpetService.getRegionList().subscribe(data => {
      this.regionList = data;
      this._cd.markForCheck(); 
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getTypeAdmissionList().subscribe(data => {
      this.typeAdmissionList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeList = data.results;
      this._cd.markForCheck();
    });

    this._loadPrograms(null, null);

    this.searchForm.get('institution').valueChanges.pipe(
      startWith(this.searchForm.get('institution').value),
      tap(institution => {
        this.programList = [];
        if (!this.isFirstSearch) {
          this.searchForm.get('programme').setValue(null);
        }
        if (!institution) {
          this._loadPrograms(null, null);
          return;
        }
        this._loadPrograms(institution, null);
      })
    ).subscribe();

    combineLatest([
      this.searchForm.get('region').valueChanges
        .pipe(
          startWith(this.searchForm.get('region').value)
        ),
      this.searchForm.get('prefecture').valueChanges
        .pipe(
          startWith(this.searchForm.get('prefecture').value)
        ),
      this.searchForm.get('institution').valueChanges
        .pipe(
          startWith(this.searchForm.get('institution').value)
        ),
      this.searchForm.get('programme').valueChanges
        .pipe(
          startWith(this.searchForm.get('programme').value)
        ),
      this.searchForm.get('typeAdmission').valueChanges
        .pipe(
          startWith(this.searchForm.get('typeAdmission').value)
        ),
      this.searchForm.get('annee').valueChanges
        .pipe(
          startWith(this.searchForm.get('annee').value)
        ),
      this.searchForm.get('status').valueChanges
        .pipe(
          startWith(this.searchForm.get('status').value)
        ),
      this.searchForm.get('etudiant').valueChanges
        .pipe(
          startWith(this.searchForm.get('etudiant').value)
        ),
      this.searchForm.get('fromSuiviDeMesDemandes').valueChanges
        .pipe(
          startWith(this.searchForm.get('fromSuiviDeMesDemandes').value)
        ),
    ])
    .subscribe((
      [
        region,
        prefecture,
        institution,
        programme,
        typeAdmission,
        annee,
        status,
        etudiant,
        fromSuiviDeMesDemandes
      ]) => {
      this.searchForm.patchValue({
        region,
        prefecture,
        institution,
        programme,
        typeAdmission,
        annee,
        status,
        etudiant,
        fromSuiviDeMesDemandes
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  private _getToPayProgrammeInfo() {
    this._metfpetService.getToPayProgrammeInfo(this.student.id).subscribe(
      data => {
        this.toPayProgrammeInfo = data;
        this._cd.markForCheck();
      }
    );
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      region: null,
      prefecture: null,
      institution: null,
      programme: null,
      typeAdmission: null,
      annee: null,
      status: null,
      etudiant: Guid.EMPTY,
      fromSuiviDeMesDemandes: true
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getAdmissionInstitutionListPage(criteria);
  }

  private _loadPrograms(institutionId: string, faculteId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {'institution': institutionId, 'faculte': faculteId}
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });
  }

  private findDecisionName(decision: AdmissionDecision): string {
    let decisionObj =  AdmissionDecisionValues.find(v => v.value === decision);
    if (decisionObj) {
      return decisionObj.name;
    }
    return '';
  }

  private findStatus(statusName: string): AdmissionInstitutionStatus {
    let statusObj = AdmissionInstitutionStatusValues.find(v => v.name === statusName);
    if (statusObj) {
      return statusObj.value;
    }
    return AdmissionInstitutionStatus.AnalyseEnCours;
  }

  public allowAcceptOffer(admissionInstitutionRowViewModel: MetfpetServiceAgent.AdmissionInstitutionRowViewModel): boolean {
    if (admissionInstitutionRowViewModel.decisionName === this.findDecisionName(AdmissionDecision.InvalidDocuments)) {
      return true;
    }
    let status = this.findStatus(admissionInstitutionRowViewModel.statusNameForSuiviDeMesDemandes);
    if (status === AdmissionInstitutionStatus.APayer || 
        status === AdmissionInstitutionStatus.AnalyseEnCours ||
        admissionInstitutionRowViewModel.statusName === this.findStatusName(AdmissionInstitutionStatus.APayer) ||
        admissionInstitutionRowViewModel.statusName === this.findStatusName(AdmissionInstitutionStatus.AnalyseEnCours)) {
      return false;
    }
    let offerAccepted = this.findDecisionName(AdmissionDecision.OffreAcceptee);
    let accepttedOne = this.pageRows.find(x => x.decisionName === offerAccepted);
    if (accepttedOne) {
      return admissionInstitutionRowViewModel.decisionName === offerAccepted || 
        admissionInstitutionRowViewModel.decisionName === this.findDecisionName(AdmissionDecision.OffreRefusee);
    }
    return admissionInstitutionRowViewModel.decisionName === offerAccepted || 
      admissionInstitutionRowViewModel.decisionName === this.findDecisionName(AdmissionDecision.OffreRefusee) ||
      admissionInstitutionRowViewModel.decisionName === this.findDecisionName(AdmissionDecision.Admis);
  }

  getPV(admissionInstitutionRowViewModel: MetfpetServiceAgent.AdmissionInstitutionRowViewModel)
  {
    return admissionInstitutionRowViewModel.displayedNumeroPV;
  }

  getModalite(admissionInstitutionRowViewModel: MetfpetServiceAgent.AdmissionInstitutionRowViewModel) {
    return 'PDF';
  }

  private findStatusName(status: AdmissionInstitutionStatus): string {
    let statusObj =  AdmissionInstitutionStatusValues.find(v => v.value === status);
    if (statusObj) {
      return statusObj.name;
    }
    return '';
  }

  public showModalite(admissionInstitutionRowViewModel: MetfpetServiceAgent.AdmissionInstitutionRowViewModel) {
    if (admissionInstitutionRowViewModel.statusName !== this.findStatusName(AdmissionInstitutionStatus.APayer)) {
      if (admissionInstitutionRowViewModel.centreConcoursName && admissionInstitutionRowViewModel.centreConcoursName !== '') {
        this._dialogService.openInfoDialog({
          width: '600px',
          data: {
            title: 'Candidature enregistrée',
            severity: SeverityEnum.INFO, 
            closeBtnText: 'Terminer',
            message: `
              <p>Votre PV : ${admissionInstitutionRowViewModel.numeroPV}</p> 
              <p>Centre Concours : ${admissionInstitutionRowViewModel.centreConcoursName}</p> 
              <p>Votre inscription et vos choix ont été bien enregistrés. 
              Vous recevrez très prochaienement les modalités relatives aux concours par courriel, SMS et sur la plateforme. </p>
              <p>Vous y receverez aussi vos résultats de même que les offres d’admissions ou les lettres de refus.</p>
            `
          }
        })
      } else {
        this._dialogService.openInfoDialog({
          width: '600px',
          data: {
            title: 'Candidature enregistrée',
            severity: SeverityEnum.INFO, 
            closeBtnText: 'Terminer',
            message: `
              <p>Votre inscription et vos choix ont été bien enregistrés. 
              Vous recevrez très prochaienement les modalités relatives aux concours par courriel, SMS et sur la plateforme. </p>
              <p>Vous y receverez aussi vos résultats de même que les offres d’admissions ou les lettres de refus.</p>
            `
          }
        })
      }   
    }
  }

  public open(admissionInstitutionRowViewModel: MetfpetServiceAgent.AdmissionInstitutionRowViewModel): void {
    if (admissionInstitutionRowViewModel.decisionName === this.findDecisionName(AdmissionDecision.InvalidDocuments)) {
      this._metfpetService.getAttachedDocumentInfo(admissionInstitutionRowViewModel.id).subscribe(
        attachedDocumentInfo => {
          this._dialogService.openDialog(
            StudentAdmissionInvalidDocumentsDecisionDialog,
            {
              width: '800px',
              data: {
                admissionInstitutionRowViewModel: admissionInstitutionRowViewModel,
                attachedDocumentInfo: attachedDocumentInfo
              }
            }
          ).afterClosed().subscribe(confirmed => {
            if (confirmed) {
              this._store.dispatch(showLoading());
              this._metfpetService.getAdmissionInstitution(admissionInstitutionRowViewModel.id).subscribe(
                admissionInstitution => {
                    admissionInstitution.decision = MetfpetServiceAgent.AdmissionDecision.AnalyseEnCours;
  
                    this._metfpetService.updateAdmissionInstitution(admissionInstitution)
                      .subscribe(
                        () => {
                          this._store.dispatch(showSuccess({}));
                          this.triggerSearch();
                        },
                        (error) => this._store.dispatch(showException({error: error}))
                      )
                      .add(() => this._store.dispatch(hideLoading()));
              });
            }
          });
        }
      );
    } else if (this.allowAcceptOffer(admissionInstitutionRowViewModel)) {
      this._dialogService.openDialog(
        StudentAdmissionDecisionDialog,
        {
          width: '620px',
          data: {
            admissionInstitutionRowViewModel: admissionInstitutionRowViewModel,
            currentSession: this.currentSession
          }
        }
      ).afterClosed().subscribe(data => {
        if (data) {
          let accept = data === "Accept";
          this._dialogService.openDialog(
            StudentAdmissionDecisionConfirmDialog,
            {
              width: '620px',
              data: {
                accept: accept
              }
            }
          ).afterClosed().subscribe(confirmed => {
            if (confirmed) {
              this._store.dispatch(showLoading());
              this._metfpetService.getAdmissionInstitution(admissionInstitutionRowViewModel.id).subscribe(
                admissionInstitution => {
                    if (accept) {
                      admissionInstitution.decision = MetfpetServiceAgent.AdmissionDecision.OffreAcceptee;
                      admissionInstitution.status = MetfpetServiceAgent.AdmissionInstitutionStatus.Accepte;
                    } else {
                      admissionInstitution.decision = MetfpetServiceAgent.AdmissionDecision.OffreRefusee;
                      admissionInstitution.status = MetfpetServiceAgent.AdmissionInstitutionStatus.Refuse;
                    }
  
                    this._metfpetService.updateAdmissionInstitution(admissionInstitution)
                      .subscribe(
                        () => {
                          this._store.dispatch(showSuccess({}));
                          this._store.dispatch(StudentActions.getCurrentStudent());
                          this.triggerSearch();
                        },
                        (error) => this._store.dispatch(showException({error: error}))
                      )
                      .add(() => this._store.dispatch(hideLoading()));
              });
            }            
          });
          this._cd.markForCheck();
        }
      });
    }
  }

  payAdmission() 
  {
    if (this.allowToPayAdmission()) 
    {
      this._dialogService.openDialog(
        StudentAdmissionPaymentDialog,
        {
          width: '600px',
          data: {
            student: this.student,
            currentSession: this.toPayProgrammeInfo.sessionAdmission,
            cart: this.toPayProgrammeInfo.selectedProgrammeInfo
          }
        }
      ).afterClosed().subscribe(payment => {
        if (payment) {
          this._dialogService.openDialog(
            StudentAdmissionPaymentConfirmationCodeDialog,
            {
              width: '600px',
              data: {
                student: this.student,
                cart: this.toPayProgrammeInfo.selectedProgrammeInfo,
                payment: payment
              }
            }
          ).afterClosed().subscribe(result => {
            if (result) {
              let centreConcoursName = this.toPayProgrammeInfo.selectedProgrammeInfo.admissionInstitutions[0].admissionInstitution.centreConcoursListName;
              if (centreConcoursName && centreConcoursName !== '') {
                this._dialogService.openInfoDialog({
                  width: '600px',
                  data: {
                    title: 'Candidature enregistrée',
                    severity: SeverityEnum.INFO,
                    closeBtnText: 'Terminer',
                    message: `
                            <p>Votre PV : ${this.toPayProgrammeInfo.selectedProgrammeInfo.admissionInstitutions[0].admissionInstitution.numeroPV}</p> 
                            <p>Centre Concours : ${centreConcoursName}</p> 
                            <p>Votre inscription et vos choix ont été bien enregistrés. 
                            Vous recevrez très prochaienement les modalités relatives aux concours par courriel, SMS et sur la plateforme. </p>
                            <p>Vous y receverez aussi vos résultats de même que les offres d’admissions ou les lettres de refus.</p>
                          `
                  }
                }).afterClosed().subscribe(() => {
                  this.triggerSearch();
                  this._getToPayProgrammeInfo();
                });
              } else {
                this._dialogService.openInfoDialog({
                  width: '600px',
                  data: {
                    title: 'Candidature enregistrée',
                    severity: SeverityEnum.INFO,
                    closeBtnText: 'Terminer',
                    message: `
                            <p>Votre inscription et vos choix ont été bien enregistrés. 
                            Vous recevrez très prochaienement les modalités relatives aux concours par courriel, SMS et sur la plateforme. </p>
                            <p>Vous y receverez aussi vos résultats de même que les offres d’admissions ou les lettres de refus.</p>
                          `
                  }
                }).afterClosed().subscribe(() => {
                  this.triggerSearch();
                  this._getToPayProgrammeInfo();
                });
              }
            }
          });
        }
      });    
    }
  }

  allowToPayAdmission() {
    return this.toPayProgrammeInfo && this.toPayProgrammeInfo.allowToPay;
  }
}