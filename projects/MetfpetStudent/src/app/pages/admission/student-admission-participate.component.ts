import { ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Guid } from 'guid-typescript';
import { MetfpetServiceAgent, SessionAdmissionStatus, StudentStatus } from 'MetfpetLib';
import { selectCurrentStudent } from '../../shared/store/selectors';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './student-admission-participate.component.html',
})
export class StudentAdmissionParticipateComponent {

  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  currentSession: MetfpetServiceAgent.SessionAdmissionDTO;
  admissionParticiperInfo: MetfpetServiceAgent.AdmissionParticiperInfo;

  constructor(
    protected _cd: ChangeDetectorRef,
    private _router: Router,
    protected _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
  }

  ngOnInit() {
    this._metfpetService.getCurrentSessionAdmission().subscribe(
      (data: MetfpetServiceAgent.SessionAdmissionDTO) => {
        this.currentSession = data;
        this._cd.markForCheck();
      }
    );
    this._store.pipe(select(selectCurrentStudent)).subscribe(student => {
      if (student) {
        setTimeout(() => {
          this.student = student;
          if (this.student)
          {
            this.getAdmissionParticiperInfo();
          }
          this._cd.markForCheck();
        })
      }
    });
  }

  getAdmissionParticiperInfo() {
    this._metfpetService.getAdmissionParticiperInfo(this.student.id).subscribe(
      (data: MetfpetServiceAgent.AdmissionParticiperInfo) => {
        this.admissionParticiperInfo = data;
        this._cd.markForCheck();
      }
    );
  }

  allowParticiperPrivate() {
    return this.student && (this.student.etudiantStatus === <any> StudentStatus.EnAdmission || 
                            this.student.etudiantStatus === <any> StudentStatus.Diplome) && 
           this.currentSession.status !== <any> SessionAdmissionStatus.ADemarrer && 
           this.admissionParticiperInfo && this.admissionParticiperInfo.allowToParticiperPrivate;
  }

  onClickParticiperPrivate() {
    this._router.navigate([`${UiPath.student.admission.selectPrograms}/private`]);
  }

  allowParticiperPublic() {
    return this.student && (this.student.etudiantStatus === <any> StudentStatus.EnAdmission || 
                            this.student.etudiantStatus === <any> StudentStatus.Diplome) && 
           this.currentSession.status !== <any> SessionAdmissionStatus.ADemarrer &&
           this.admissionParticiperInfo && this.admissionParticiperInfo.allowToParticiperPublic;
  }

  onClickParticiperPublic() {
    this._router.navigate([`${UiPath.student.admission.selectPrograms}/public`]);
  }

  allowParticiperSante() {
    return this.student && (this.student.etudiantStatus === <any> StudentStatus.EnAdmission || 
                            this.student.etudiantStatus === <any> StudentStatus.Diplome) && 
           this.currentSession.status !== <any> SessionAdmissionStatus.ADemarrer &&
           this.admissionParticiperInfo && this.admissionParticiperInfo.allowToParticiperSante;
  }

  onClickParticiperSante() {
    this._router.navigate([`${UiPath.student.admission.selectPrograms}/sante`]);
  }

  get isSessionExists(): boolean {
    return this.currentSession && this.currentSession.id !== Guid.EMPTY;
  }
}