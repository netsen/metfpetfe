import { 
  ChangeDetectionStrategy,
  Component, 
  Inject,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  AdmissionDecision,
  AdmissionDecisionValues,
  MetfpetServiceAgent,
  SessionAdmissionStatus,
} from 'MetfpetLib';
import { Router } from '@angular/router';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './student-admission-decision.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentAdmissionDecisionDialog {

  form: FormGroup;
  admissionInstitutionRowViewModel: MetfpetServiceAgent.AdmissionInstitutionRowViewModel;
  currentSession: MetfpetServiceAgent.SessionAdmissionDTO;
  decisionList: Array<any>;
  
  constructor(
    public dialogRef: MatDialogRef<StudentAdmissionDecisionDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _formBuilder: FormBuilder,
    private _router: Router
  ) {
    this.admissionInstitutionRowViewModel = data.admissionInstitutionRowViewModel;
    this.currentSession = data.currentSession;
    this.decisionList = AdmissionDecisionValues;
    this.form = this._formBuilder.group({
      'id': null
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public onDeclineTheOffer(): void {
    this.dialogRef.close("Decline");
  }

  public onAcceptTheOffer(): void {
    this.dialogRef.close("Accept");
  }

  public onRegister(): void {
    this.onClose();
    this._router.navigate([UiPath.student.cursus.inscriptions]);
  }

  private findDecisionName(decision: AdmissionDecision): string {
    let decisionObj =  this.decisionList.find(v => v.value === decision);
    if (decisionObj) {
      return decisionObj.name;
    }
    return '';
  }

  public isRefuseTheOffer(): boolean {
    return this.admissionInstitutionRowViewModel.decisionName === this.findDecisionName(AdmissionDecision.OffreRefusee);
  }

  public isAcceptTheOffer(): boolean {
    return this.admissionInstitutionRowViewModel.decisionName === this.findDecisionName(AdmissionDecision.OffreAcceptee);
  }

  public isAdmis(): boolean {
    return this.admissionInstitutionRowViewModel.decisionName === this.findDecisionName(AdmissionDecision.Admis);
  }

  public allowSinscrire(): boolean {
    return this.currentSession.id !== this.admissionInstitutionRowViewModel.sessionAdmissionId || 
           this.currentSession.status === <any>SessionAdmissionStatus.Terminee ||
           this.currentSession.status === <any>SessionAdmissionStatus.AConfirmer;
  }
}