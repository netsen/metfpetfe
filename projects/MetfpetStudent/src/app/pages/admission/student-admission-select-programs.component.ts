import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { startWith, tap } from 'rxjs/operators/';
import { Store, select } from '@ngrx/store';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import {
  BaseTableComponent,
  DialogService,
  MetfpetServiceAgent,
  PerfectScrollService,
  showLoading,
  hideLoading,
  showSuccess,
  showError,
  SeverityEnum,
  AdmissionInstitutionStatus,
  SUR_CONCOURS,
  TypeExamenValues,
} from 'MetfpetLib';
import { selectCurrentStudent } from '../../shared/store/selectors';
import { StudentAdmissionSelectProgramsDialog } from './student-admission-select-programs.dialog';
import { StudentAdmissionPaymentDialog } from './student-admission-payment.dialog';
import { StudentAdmissionPaymentConfirmationCodeDialog } from './student-admission-payment-confirmation-code.dialog';
import { Guid } from 'guid-typescript';
import { StudentTransmissionDocumentDialog } from './student-transmission-document.dialog';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './student-admission-select-programs.component.html',
  styleUrls: ['./student-admission-select-programs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentAdmissionSelectProgramsComponent extends BaseTableComponent {

  regionList: Array<MetfpetServiceAgent.Region>;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  typeAdmissionList: Array<MetfpetServiceAgent.TypeAdmission>;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  currentSession: MetfpetServiceAgent.SessionAdmissionDTO;
  cart: MetfpetServiceAgent.SelectedProgrammeInfo;
  typeExamesList: Array<any>;
  title = 'Programme';
  paid = false;
  isPrivate: boolean;
  isSante: boolean;
  admissionParticiperInfo: MetfpetServiceAgent.AdmissionParticiperInfo;

  constructor(
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _route: ActivatedRoute,
    private _metfpetService: MetfpetServiceAgent.HttpService

  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.sort = { prop: 'institution.name', dir: 'asc' };
    this.typeExamesList = TypeExamenValues;
  }

  ngOnInit() {
    this._route.params.subscribe(params => 
    {
        this.isPrivate = params['type'] == 'private';
        this.isSante = params['type'] == 'sante';
        this.searchForm.get('isPrivate').setValue(this.isPrivate);
        this.searchForm.get('isSante').setValue(this.isSante);
        this._cd.markForCheck();
    });

    this._metfpetService.getTypeAdmissionList().subscribe(data => {
      this.typeAdmissionList = data;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getCurrentSessionAdmission().subscribe(
      (data: MetfpetServiceAgent.SessionAdmissionDTO) => {
        this.currentSession = data;
      }
    );

    this._store.pipe(select(selectCurrentStudent)).subscribe(student => {
      if (student) {
        setTimeout(() => {
          this.student = student;
          this.searchForm.get('etudiant').setValue(student.id);
          this.triggerSearch();

          this._loadSelectedPrograms();
        })
      }
    });

    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getRegionList().subscribe(data => {
      this.regionList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });

    this._loadPrograms(null, null);

    this.searchForm.get('institution').valueChanges.pipe(
      startWith(this.searchForm.get('institution').value),
      tap(institution => {
        this.programList = [];
        if (!this.isFirstSearch) {
          this.searchForm.get('programme').setValue(null);
        }
        if (!institution) {
          this._loadPrograms(null, null);
          return;
        }
        this._loadPrograms(institution, null);
      })
    ).subscribe();

    combineLatest([
      this.searchForm.get('region').valueChanges
        .pipe(
          startWith(this.searchForm.get('region').value)
        ),
      this.searchForm.get('prefecture').valueChanges
        .pipe(
          startWith(this.searchForm.get('prefecture').value)
        ),
      this.searchForm.get('institution').valueChanges
        .pipe(
          startWith(this.searchForm.get('institution').value)
        ),
      this.searchForm.get('programme').valueChanges
        .pipe(
          startWith(this.searchForm.get('programme').value)
        ),
      this.searchForm.get('typeAdmission').valueChanges
        .pipe(
          startWith(this.searchForm.get('typeAdmission').value)
        ),
      this.searchForm.get('etudiant').valueChanges
        .pipe(
          startWith(this.searchForm.get('etudiant').value)
        ),
    ])
      .subscribe((
        [
          region,
          prefecture,
          institution,
          programme,
          typeAdmission,
          etudiant
        ]) => {
        this.searchForm.patchValue({
          region,
          prefecture,
          institution,
          programme,
          typeAdmission,
          etudiant
        }, { emitEvent: false });
        this.triggerSearch();
      });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      region: null,
      prefecture: null,
      institution: null,
      programme: null,
      typeAdmission: null,
      etudiant: Guid.EMPTY,
      hasPlace: true,
      isPrivate: this.isPrivate,
      isSante: this.isSante
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  private _loadPrograms(institutionId: string, faculteId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { 'institution': institutionId, 'faculte': faculteId }
    })).subscribe(data => {
      let programs: Array<MetfpetServiceAgent.ProgrammeRowViewModel> = [];
      for (let program of data.results) {
        let index = programs.findIndex(v => v.name == program.name);
        if (index < 0) {
          programs.push(program);
        }
      }
      this.programList = programs;
      this._cd.markForCheck();
    });
  }

  private _loadSelectedPrograms() {
    this._metfpetService.getSelectedProgrammeList(MetfpetServiceAgent.GetSelectedProgrammesRequest.fromJS({
      etudiantId: this.student.id,
      isPrivate: this.isPrivate,
      isSante: this.isSante
    })).subscribe(
      data => {
        this.cart = data;
        if (data && data.admissionInstitutions) {
          let paidAdmissionInstitution = data.admissionInstitutions.find(v => v.admissionInstitution.status !== <any>AdmissionInstitutionStatus.APayer);
          if (paidAdmissionInstitution) {
            this.paid = true;
          }
        }
        this._cd.markForCheck();
      }
    );
    this.getAdmissionParticiperInfo();
  }

  getAdmissionParticiperInfo() {
    this._metfpetService.getAdmissionParticiperInfo(this.student.id).subscribe(
      (data: MetfpetServiceAgent.AdmissionParticiperInfo) => {
        this.admissionParticiperInfo = data;
        this._cd.markForCheck();
      }
    );
  }

  allowToSelectTheProgram() {
    if (this.admissionParticiperInfo)
    {
      if (this.isPrivate)
      {
        return this.admissionParticiperInfo.allowToChoosePrivateProgram;
      }
      else if (this.isSante)
      {
        return this.admissionParticiperInfo.allowToChooseSante;
      }
      else
      {
        return this.admissionParticiperInfo.allowToChoosePublicProgram;
      }
    }
    return true;
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getAvailableProgrammeList(criteria);
  }

  enablePayButton() {
    return this.cart && this.cart.totalFrais && this.cart.totalFrais > 0;
  }

  getProgramNames(row: MetfpetServiceAgent.ProgrammeInfo) {
    return row.programmes.map(x => x.nameWithTypeAdmission).join('\n');
  }

  enableUpdateOrDelete(dto: MetfpetServiceAgent.AdmissionInstitutionInfo) {
    return dto.admissionInstitution.status === <any>AdmissionInstitutionStatus.APayer;
  }

  addToCart(row: MetfpetServiceAgent.ProgrammeInfo) {
    this._dialogService.openDialog(
      StudentAdmissionSelectProgramsDialog,
      {
        width: '600px',
        data: {
          student: this.student,
          currentSession: this.currentSession,
          institution: row.institution,
          availablePrograms: row.programmes,
        }
      }
    ).afterClosed().subscribe(result => {
      if (result) {
        this._loadSelectedPrograms();
      }
    });
  }

  modifyAdmissionChoice(dto: MetfpetServiceAgent.AdmissionInstitutionInfo) {
    var programInfo = this.pageRows.find(x => x.institution.id === dto.institution.id);

    this._dialogService.openDialog(
      StudentAdmissionSelectProgramsDialog,
      {
        width: '600px',
        data: {
          student: this.student,
          currentSession: this.currentSession,
          institution: dto.institution,
          availablePrograms: programInfo.programmes,
          currentAdmissionInstitution: dto.admissionInstitution,
        }
      }
    ).afterClosed().subscribe(result => {
      if (result) {
        this._loadSelectedPrograms();
      }
    });
  }

  removeFromCart(admissionInstitutionChoiceId: string) {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention",
        message: "Voulez-vous définitivement retirer ce choix ?",
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteAdmissionInstitutionChoice(admissionInstitutionChoiceId)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({ message: 'Le choix a été retiré' }));
              this._loadSelectedPrograms();
            },
            (error) => this._store.dispatch(showError({ message: error.response }))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  payAndComplete() {
    this._dialogService.openConfirmDialog({
      width: '600px',
      data: {
        title: 'Attention',
        confirmBtnText: 'Payer',
        message: `
          <p>En cliquant sur payer, vous soumettez vos choix.
          Après avoir payé, vous ne pourrez plus effectuer de modifications dans vos choix.
          Nous vous recommandons de sélectionner au minimum 3 choix.</p> 

          <p>Vous comprenez que vous payez pour les frais d’administration. 
          Payer ne vous garantie pas d’être accepté dans le programme.</p>
        `
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        if (this.cart.surDossierAdmissionInstitutions) {
          let attachedDocumentInfo = new MetfpetServiceAgent.AttachedDocumentInfo();
          this._metfpetService.getAttachedDocumentInfo(this.cart.surDossierAdmissionInstitutions).subscribe(
            result => {
              attachedDocumentInfo = result;
            }
          ).add(() => {
            this._dialogService.openDialog(
              StudentTransmissionDocumentDialog,
              {
                width: '800px',
                data: {
                  surDossierAdmissionInstitutions: this.cart.surDossierAdmissionInstitutions,
                  attachedDocumentInfo: attachedDocumentInfo
                }
              }
            ).afterClosed().subscribe(confirmed => {
              if (confirmed) {
                this.processAdmissionPayment();
              }
            });
          });
        } else {
          this.processAdmissionPayment();
        }
      }
    });
  }

  private processAdmissionPayment() {
    this._dialogService.openDialog(
      StudentAdmissionPaymentDialog,
      {
        width: '600px',
        data: {
          student: this.student,
          currentSession: this.currentSession,
          cart: this.cart
        }
      }
    ).afterClosed().subscribe(payment => {
      if (payment) {
        this._dialogService.openDialog(
          StudentAdmissionPaymentConfirmationCodeDialog,
          {
            width: '600px',
            data: {
              student: this.student,
              cart: this.cart,
              payment: payment
            }
          }
        ).afterClosed().subscribe(result => {
          if (result) {
            let centreConcoursName = this.cart.admissionInstitutions[0].admissionInstitution.centreConcoursListName;
            if (centreConcoursName && centreConcoursName !== '') {
              this._dialogService.openInfoDialog({
                width: '600px',
                data: {
                  title: 'Candidature enregistrée',
                  severity: SeverityEnum.INFO,
                  closeBtnText: 'Terminer',
                  message: `
                          <p>Votre PV : ${this.cart.admissionInstitutions[0].admissionInstitution.numeroPV}</p> 
                          <p>Centre Concours : ${centreConcoursName}</p> 
                          <p>Votre inscription et vos choix ont été bien enregistrés. 
                          Vous recevrez très prochaienement les modalités relatives aux concours par courriel, SMS et sur la plateforme. </p>
                          <p>Vous y receverez aussi vos résultats de même que les offres d’admissions ou les lettres de refus.</p>
                        `
                }
              }).afterClosed().subscribe(() => {
                this.paid = true;
                this.triggerSearch();
                this._loadSelectedPrograms();
                this._router.navigate([UiPath.student.admission.manage]);
              });
            } else {
              this._dialogService.openInfoDialog({
                width: '600px',
                data: {
                  title: 'Candidature enregistrée',
                  severity: SeverityEnum.INFO,
                  closeBtnText: 'Terminer',
                  message: `
                          <p>Votre inscription et vos choix ont été bien enregistrés. 
                          Vous recevrez très prochaienement les modalités relatives aux concours par courriel, SMS et sur la plateforme. </p>
                          <p>Vous y receverez aussi vos résultats de même que les offres d’admissions ou les lettres de refus.</p>
                        `
                }
              }).afterClosed().subscribe(() => {
                this.paid = true;
                this.triggerSearch();
                this._router.navigate([UiPath.student.admission.manage]);
              });
            }
          }
        });
      }
    });
  }

  getDiplomeTypeName(programmes: MetfpetServiceAgent.ProgrammeDTO[]){
    return programmes.map(x => x.typeDiplomeName).join('\n');
  }

  getTypeExamenName(programmes: MetfpetServiceAgent.ProgrammeDTO[]) {
    return programmes.map(x => x.typeExamenName).join('\n');
  }
}