import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { 
  MetfpetServiceAgent,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  showException,
  validateForm,
} from 'MetfpetLib';

@Component({
  templateUrl: './student-admission-select-programs.dialog.html',
})
export class StudentAdmissionSelectProgramsDialog {

  form: FormGroup;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  currentSession: MetfpetServiceAgent.SessionAdmissionDTO;
  institution: MetfpetServiceAgent.InstitutionInfo;
  availablePrograms: Array<MetfpetServiceAgent.ProgrammeDTO>;
  currentAdmissionInstitution: MetfpetServiceAgent.AdmissionInstitutionDTO;
  error: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<StudentAdmissionSelectProgramsDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.student = this.data.student;
    this.currentSession = this.data.currentSession;
    this.institution = this.data.institution;
    this.availablePrograms = this.data.availablePrograms;
    this.currentAdmissionInstitution = this.data.currentAdmissionInstitution;
    this.form = this._formBuilder.group({
      programs: this._formBuilder.array([]),
    });

    if (!this.currentAdmissionInstitution) {
      for (let i = 1; i <= 3; i++) {
        if (i <= this.availablePrograms.length && this.institution.statusEtablissementCode === "publique") {
            this.programs.push(this._formBuilder.group({
              programmeId: [null, Validators.required], 
              position: i, 
            }));
          }
          else
          {
            this.programs.push(this._formBuilder.group({
              programmeId: null, 
              position: i, 
            }));
          }
      }
    } else {
      for (var item of this.currentAdmissionInstitution.choixAdmissionInstitutions) {
        if (this.institution.statusEtablissementCode === "publique")
        {
          this.programs.push(this._formBuilder.group({
            programmeId: [item.programmeId, Validators.required], 
            position: item.position, 
          }));
        }
        else
        {
          this.programs.push(this._formBuilder.group({
            programmeId: item.programmeId, 
            position: item.position, 
          }));
        }
      }

      for (let i = this.currentAdmissionInstitution.choixAdmissionInstitutions.length + 1; i <= 3; i++) {
        if (i <= this.availablePrograms.length && this.institution.statusEtablissementCode === "publique") {
          this.programs.push(this._formBuilder.group({
            programmeId: [null, Validators.required], 
            position: i, 
          }));
        }
        else
        {
          this.programs.push(this._formBuilder.group({
            programmeId: null, 
            position: i, 
          }));
        }
      }
    }
  }

  get programs() {
    return this.form.controls['programs'] as FormArray;
  }

  onClose(): void {
    this._dialogRef.close();
  }

  onConfirm() {
    validateForm(this.form);
    if(this.form.invalid) return;
    this.error = null;

    if (this._validateChoices()) {
      if (!this.currentAdmissionInstitution) {
        this._createAdmissionInstitution();
      } else {
        this._updateAdmissionInstitution();
      }
    }
  }

  private _createAdmissionInstitution() {
    var dto = new MetfpetServiceAgent.CreateAdmissionInstitution();
      dto.etudiantId = this.student.id;
      dto.institutionId = this.institution.id;
      dto.sessionAdmissionId = this.currentSession.id;
      dto.createChoixAdmissionInstitutions = this.programs.value
        .filter(x => !!x.programmeId)
        .map(x => MetfpetServiceAgent.CreateChoixAdmissionInstitution.fromJS(x));

      this._store.dispatch(showLoading());

      this._metfpetService.createAdmissionInstitution(dto)
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
  }

  private _updateAdmissionInstitution() {
    this.currentAdmissionInstitution.changeChoixAdmissionInstitution = true;
    this.currentAdmissionInstitution.choixAdmissionInstitutions = 
      this.programs.value
        .filter(x => !!x.programmeId)
        .map(x => MetfpetServiceAgent.ChoixAdmissionInstitutionDTO.fromJS(x));

    this._store.dispatch(showLoading());
    this._metfpetService.updateAdmissionInstitution(this.currentAdmissionInstitution)
      .subscribe(
        () => {
          this._store.dispatch(showSuccess({}));
          this._dialogRef.close(true);
        },
        (error) => this._store.dispatch(showError({message: error.response}))
    )
    .add(() => this._store.dispatch(hideLoading()));
  }

  private _validateChoices() {
    var choices = this.programs.value.filter(x => !!x.programmeId).map(x => x.programmeId);

    if (choices.length == 0) {
      this.error = 'Au moins 1 programme est requis';
    }

    if (new Set(choices).size !== choices.length) {
      this.error = 'Programme déjà ajouté dans le panier';
    }

    return !this.error;
  }


}