import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Inject,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  AttachedDocumentAssociatedType,
  hideLoading,
  MetfpetServiceAgent, showError, showLoading, showSuccess,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';

export const NO_FILE_SELECT = 'aucun fichier sélectionné';
export const NORMAL_CSS = 'row mb-2';
export const ERROR_CSS = 'row mb-2 warn-color';

@Component({
  templateUrl: './student-transmission-document.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentTransmissionDocumentDialog {

  form: FormGroup;
  acteNaissanceFileName: string = NO_FILE_SELECT;
  acteNaissanceCssClass: string = ERROR_CSS;
  photoFileName: string = NO_FILE_SELECT;
  photoCssClass: string = ERROR_CSS;
  lettreFileName: string = NO_FILE_SELECT;
  lettreCssClass: string = ERROR_CSS;
  attestationFileName: string = NO_FILE_SELECT;
  attestationCssClass: string = ERROR_CSS;
  diplomeCEEFileName: string = NO_FILE_SELECT;
  diplomeCEECssClass: string = ERROR_CSS;
  certificatVMFileName: string = NO_FILE_SELECT;
  certificatVMCssClass: string = ERROR_CSS;
  surDossierAdmissionInstitutions: string;
  attachedDocumentInfo: MetfpetServiceAgent.AttachedDocumentInfo;

  constructor(
    public dialogRef: MatDialogRef<StudentTransmissionDocumentDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _formBuilder: FormBuilder,
    protected _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    protected _cd: ChangeDetectorRef,
  ) {
    this.surDossierAdmissionInstitutions = data.surDossierAdmissionInstitutions;
    this.attachedDocumentInfo = data.attachedDocumentInfo;
    if (this.attachedDocumentInfo) {
      if (this.attachedDocumentInfo.acteNaissanceFileName) {
        this.acteNaissanceFileName = this.attachedDocumentInfo.acteNaissanceFileName;
        this.acteNaissanceCssClass = NORMAL_CSS;
      }
      if (this.attachedDocumentInfo.photoFileName) {
        this.photoFileName = this.attachedDocumentInfo.photoFileName;
        this.photoCssClass = NORMAL_CSS;
      }
      if (this.attachedDocumentInfo.lettreFileName) {
        this.lettreFileName = this.attachedDocumentInfo.lettreFileName;
        this.lettreCssClass = NORMAL_CSS;
      }
      if (this.attachedDocumentInfo.attestationFileName) {
        this.attestationFileName = this.attachedDocumentInfo.attestationFileName;
        this.attestationCssClass = NORMAL_CSS;
      }
      if (this.attachedDocumentInfo.diplomeCEEFileName) {
        this.diplomeCEEFileName = this.attachedDocumentInfo.diplomeCEEFileName;
        this.diplomeCEECssClass = NORMAL_CSS;
      }
      if (this.attachedDocumentInfo.certificatVMFileName) {
        this.certificatVMFileName = this.attachedDocumentInfo.certificatVMFileName;
        this.certificatVMCssClass = NORMAL_CSS;
      }
    }
    this.form = this._formBuilder.group({
      'id': null
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public onSubmitLater(): void {
    this.dialogRef.close(true);
  }

  public onContinue(): void {
    if (this.allowToGoNext()) {
      this.dialogRef.close(true);
    }
  }

  public allowToGoNext() {
    return this.acteNaissanceFileName !== NO_FILE_SELECT &&
      this.photoFileName !== NO_FILE_SELECT &&
      this.lettreFileName !== NO_FILE_SELECT &&
      this.attestationFileName !== NO_FILE_SELECT &&
      this.diplomeCEEFileName !== NO_FILE_SELECT &&
      this.certificatVMFileName !== NO_FILE_SELECT;
  }

  private updateFileName(fileName: string, associatedType: AttachedDocumentAssociatedType) {
    switch (associatedType) {
      case AttachedDocumentAssociatedType.ActeNaissance:
        this.acteNaissanceFileName = fileName;
        this.acteNaissanceCssClass = NORMAL_CSS;
        break;
      case AttachedDocumentAssociatedType.Photo:
        this.photoFileName = fileName;
        this.photoCssClass = NORMAL_CSS;
        break;
      case AttachedDocumentAssociatedType.Lettre:
        this.lettreFileName = fileName;
        this.lettreCssClass = NORMAL_CSS;
        break
      case AttachedDocumentAssociatedType.Attestation:
        this.attestationFileName = fileName;
        this.attestationCssClass = NORMAL_CSS;
        break
      case AttachedDocumentAssociatedType.DiplomeCEE:
        this.diplomeCEEFileName = fileName;
        this.diplomeCEECssClass = NORMAL_CSS;
        break;
      case AttachedDocumentAssociatedType.CertificatVM:
        this.certificatVMFileName = fileName;
        this.certificatVMCssClass = NORMAL_CSS;
        break;
    }
  }

  private onFileUpload(event: any, associatedType: AttachedDocumentAssociatedType) {
    if (event.target.files && event.target.files.length > 0) {
      this._store.dispatch(showLoading());

      let file : Blob = event.target.files[0];
      var fileReader = new FileReader();
      fileReader.onloadend = function () {
        
      }
      fileReader.readAsBinaryString(file);
      
      let fileParameter = 
      {
        data : file,
        fileName : event.target.fileName
      };
      
      this._metfpetService.attachDocument(fileParameter, this.surDossierAdmissionInstitutions, associatedType)
        .subscribe(
          (fileName) => {
            this.updateFileName(fileName, associatedType);
            this._cd.markForCheck();
            this._store.dispatch(showSuccess({}));
          },
          (error) => this._store.dispatch(showError({message: error.message}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public uploadActeNaissance(): void {
    document.getElementById('acteNaissanceSelector').click();
  }

  public onActeNaissanceFileUpload(event): void {
    this.onFileUpload(event, AttachedDocumentAssociatedType.ActeNaissance);
  }

  public uploadPhoto(): void {
    document.getElementById('photoSelector').click();
  }

  public onPhotoFileUpload(event): void {
    this.onFileUpload(event, AttachedDocumentAssociatedType.Photo);
  }

  public uploadLettre(): void {
    document.getElementById('lettreSelector').click();
  }

  public onLettreFileUpload(event): void {
    this.onFileUpload(event, AttachedDocumentAssociatedType.Lettre);
  }

  public uploadAttestation(): void {
    document.getElementById('attestationSelector').click();
  }

  public onAttestationFileUpload(event): void {
    this.onFileUpload(event, AttachedDocumentAssociatedType.Attestation);
  }

  public uploadDiplomeCEE(): void {
    document.getElementById('diplomeCEESelector').click();
  }

  public onDiplomeCEEFileUpload(event): void {
    this.onFileUpload(event, AttachedDocumentAssociatedType.DiplomeCEE);
  }

  public uploadCertificatVM(): void {
    document.getElementById('certificatVMSelector').click();
  }

  public onCertificatVMFileUpload(event): void {
    this.onFileUpload(event, AttachedDocumentAssociatedType.CertificatVM);
  }
}