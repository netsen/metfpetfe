import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './student-admission-decision-confirm.dialog.html',
})
export class StudentAdmissionDecisionConfirmDialog {

  constructor(
    public dialogRef: MatDialogRef<StudentAdmissionDecisionConfirmDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { 
  }

  onClose(): void {
    this.dialogRef.close();
  }

}