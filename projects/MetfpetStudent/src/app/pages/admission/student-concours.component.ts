import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, of as observableOf } from 'rxjs';
import {
  AppSettings,
  Settings,
  DialogService, 
  MetfpetServiceAgent,
  PerfectScrollService,
  BaseTableComponent,
  StudentConcoursDetailDialog,
} from 'MetfpetLib';
import { selectCurrentStudent } from '../../shared/store/selectors';
import { StudentActions } from '../../shared/store/actions';
import { Tabs } from './student-admission-tab.component';
import { StudentResultatsDuConcoursDialog } from './student-resultats-du-concours.dialog';

@Component({
  templateUrl: './student-concours.component.html', 
  styleUrls: ['./student-concours.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentConcoursComponent extends BaseTableComponent {

  settings: Settings;
  title: string;
  navs = Tabs;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  centreList: Array<MetfpetServiceAgent.CentreConcoursRowViewModel>;
  
  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Concours d’entrée';
    this.sort = {prop: 'concoursName', dir: 'desc'};
  }

  ngAfterViewInit(): void {
    this._store.pipe(select(selectCurrentStudent)).subscribe(student => {
      if (student) {
        setTimeout(() => {
          this.student = student;
          this.searchForm.get('etudiant').setValue(student.id);
          this.triggerSearch();
        })
      }
    });

    this._metfpetService.getCentreConcoursListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.centreList = data.results;
      this._cd.markForCheck();
    });
  }
  
  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      etudiant: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getConcoursEtudiantListPage(criteria);
  }

  getResultatValue(row: MetfpetServiceAgent.ConcoursEtudiantViewModel) {
    return row.isPublished ? row.resultName : '-';
  }
  
  getMoyenneTotal(row: MetfpetServiceAgent.ConcoursEtudiantViewModel) {
    return row.isPublished && row.moyenneTotal ? row.moyenneTotal : "-";
  }

  viewTheResultText(row: MetfpetServiceAgent.ConcoursEtudiantViewModel) {
    return row.isPublished ? 'Ouvrir' : '-';
  }

  viewTheResult(row: MetfpetServiceAgent.ConcoursEtudiantViewModel) {
    if (row.isPublished)
    {
      this._dialogService.openDialog(
        StudentResultatsDuConcoursDialog,
        {
          width: '650px',
          data: {
            id: row.id
          }
        }
      );
    }
  }

  openStudentModalitesConcours(row: MetfpetServiceAgent.ConcoursEtudiantViewModel) {
    this._dialogService.openDialog(
      StudentConcoursDetailDialog,
      {
        width: '650px',
        data: {
          id: row.id,
          centreList: this.centreList,
          isCompetitionResult: false,
          isReadOnly: true,
          isAdmin: false,
          isUniversitaireUser: false,
          isMinistre: false,
        }
      }
    );
  }
}