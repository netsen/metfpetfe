import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { DatePipe } from '@angular/common';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import {
  DialogService,
  SeverityEnum,
  showSuccess,
  showError,
  showLoading, 
  hideLoading,
  validateForm,
  MetfpetServiceAgent,
  PaymentServiceAgent,
  PAYMENT_ERROR,
} from 'MetfpetLib';

@Component({
  templateUrl: './student-admission-payment.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentAdmissionPaymentDialog {

  form: FormGroup;
  paymentInProgress: boolean;
  cart: MetfpetServiceAgent.SelectedProgrammeInfo;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  currentSession: MetfpetServiceAgent.SessionAdmissionDTO;

  constructor(
    public dialogRef: MatDialogRef<StudentAdmissionPaymentDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _cd: ChangeDetectorRef,
    private _datePipe: DatePipe, 
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _paymentService: PaymentServiceAgent.HttpService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.student = data.student;
    this.currentSession = data.currentSession;
    this.cart = data.cart;
    this.form = this._formBuilder.group({
      'id': null,
      'etudiantId': this.student.id,
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onPayWithOrange() {
    this.paymentInProgress = true;
    this._paymentService.createAdmissionPayment(PaymentServiceAgent.CreateAdmissionPayment.fromJS(
      {
        etudiantId: this.student.id,
        sessionAdmissionId: this.currentSession.id,
        orderId: this._datePipe.transform(new Date(), "yyyyMMddHmmss"),
        selectedAdmissionInstitutionIds: this.cart.admissionInstitutions.map(x => x.id)
      }
    ))
    .subscribe(
      (payment: PaymentServiceAgent.AdmissionPaymentDTO) => {
        if (!payment) {
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: PAYMENT_ERROR
            }
          });
        } else {
          if (payment.details) {
            window.open(payment.details, "_blank");
          }
          this.dialogRef.close(payment);
        }
      },
      error => 
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.ERROR, 
            message: PAYMENT_ERROR + ` (Erreur: ${error})`
          }
        })
    );
  }
}