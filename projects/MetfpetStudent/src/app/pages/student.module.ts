import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { 
  AuthRoleGuardService, 
  PagesComponent, 
  PublicLayoutComponent, 
  PublicLayoutLargerFormComponent,
} from 'MetfpetLib';
import { SharedModule } from '../shared/shared.module';
import { StudentHomeComponent } from './home/student-home.component';
import { StudentProfileDialog } from './profile/student-profile.dialog';
import { OtpDialog } from './otp/otp.dialog';

export const routes = [
  { 
    path: 'login', 
    component: PublicLayoutComponent,
    loadChildren: () => import('./login/student-login.module').then(m => m.StudentLoginModule),
  },
  { 
    path: 'faq', 
    loadChildren: () => import('./faq/faq.module').then(m => m.FaqModule),
    data: { breadcrumb: 'Faq' }, 
  },
  { 
    path: 'retrieve-password', 
    component: PublicLayoutComponent,
    loadChildren: () => import('./retrieve-password/student-retrieve-password.module').then(m => m.StudentRetrievePasswordModule),
  },
  { 
    path: 'inscription', 
    component: PublicLayoutLargerFormComponent,
    loadChildren: () => import('./inscription/student-inscription.module').then(m => m.StudentInscriptionModule),
  },
  { 
    path: 'reinscription', 
    component: PublicLayoutLargerFormComponent,
    loadChildren: () => import('./reinscription/student-reinscription.module').then(m => m.StudentReinscriptionModule),
  },
  {
    path: '',
    component: PagesComponent,
    canActivate: [ AuthRoleGuardService ],
    children: [
      {
        path: '',
        component: StudentHomeComponent,
      },
      { 
        path: 'admission', 
        loadChildren: () => import('./admission/student-admission.module').then(m => m.StudentAdmissionModule),
        data: { breadcrumb: 'Admission' }, 
      },
      { 
        path: 'dossier', 
        loadChildren: () => import('./dossier/student-dossier.module').then(m => m.StudentDossierModule),
        data: { breadcrumb: 'Dossier d\'apprenant' }, 
      },
      { 
        path: 'cursus', 
        loadChildren: () => import('./cursus/student-cursus.module').then(m => m.StudentCursusModule),
        data: { breadcrumb: 'Cursus' }, 
      },
      { 
        path: 'document', 
        loadChildren: () => import('./document/student-document.module').then(m => m.StudentDocumentModule),
        data: { breadcrumb: 'Document' }, 
      },
      { 
        path: 'bourses', 
        loadChildren: () => import('./bourses/student-bourses.module').then(m => m.StudentBoursesModule),
        data: { breadcrumb: 'Bourses' }, 
      },
      { 
        path: 'examens', 
        loadChildren: () => import('./examens/student-examens.module').then(m => m.StudentExamensModule),
        data: { breadcrumb: 'Examen de sortie' }, 
      },
      { 
        path: 'diplomes', 
        loadChildren: () => import('./diplomes/student-diplomes.module').then(m => m.StudentDiplomesModule),
        data: { breadcrumb: 'Diplomes' }, 
      },
    ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule, 
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    StudentHomeComponent,
    StudentProfileDialog,
    OtpDialog
  ],
  entryComponents: [
    StudentProfileDialog
  ]
})
export class StudentModule { }