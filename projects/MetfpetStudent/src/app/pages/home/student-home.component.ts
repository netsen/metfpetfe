import { ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppSettings, AuthService, DialogService, MetfpetServiceAgent, OtpStatus, Settings } from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { CreateSmartPayAccountDialog } from '../bourses/create-smartpay-account.dialog';
import { Guid } from 'guid-typescript';
import { OtpDialog } from '../otp/otp.dialog';
import { select, Store } from '@ngrx/store';
import { selectCurrentStudent } from '../../shared/store/selectors';
import { MatDialog } from '@angular/material/dialog';

@Component({
  templateUrl: './student-home.component.html',
})
export class StudentHomeComponent {
  otpStatusResult: MetfpetServiceAgent.OtpStatus
  public settings: Settings;
  public title: string;
  isChecking: boolean = false;
  
  constructor(
    public appSettings: AppSettings, 
    private _router: Router,
    private _authService: AuthService,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _store: Store<any>,
    private dialog: MatDialog,
  ) {
    this.settings = this.appSettings.settings;
    this.title = 'Bienvenue sur votre espace d\'apprenant';
  }

  ngAfterViewInit(){
    this.openOtpDialog();
  }

    // this._metfpetService.hasInscriptionPublicInstitutionButNoSmartAccount(this._authService.getUserId()).subscribe(studentId => {
    //   if (studentId !== Guid.EMPTY)
    //   {
    //     this._dialogService.openDialog(CreateSmartPayAccountDialog, {
    //       width: '800px',
    //       data: {
    //         studentId: studentId,
    //       }
    //     }).afterClosed().subscribe(result => {
    //       if (!result) {
    //         this._router.navigate(['/login']);
    //       }
    //     });
    //   }
    // });

  openOtpDialog() {
    this._store.pipe(select(selectCurrentStudent)).subscribe(currentStudent => {
      this._metfpetService.getAnneeAcademiqueAdmissionOuverte().subscribe((currentAnneeAcademique: any) => {
        if (currentAnneeAcademique && currentStudent) {
          this._metfpetService.checkStudentOtpStatus(MetfpetServiceAgent.StudentOtpStatusDTO.fromJS({ etudiantId: currentStudent.id, currentAnneeAcademiqueId: currentAnneeAcademique.id })).subscribe((status) => {
            setTimeout(() => this.settings.loadingSpinner = false);
            if (this.isChecking) 
            {
              return;
            }
            if ((<any>status === OtpStatus.isPending) || (<any>status === OtpStatus.NotVerified)) {
              if(this.dialog.openDialogs.length) return;
              this.isChecking = true;
              this._dialogService.openDialog(
                OtpDialog, {
                data: {
                  status: <any>status,
                  etudiantId: currentStudent?.id,
                  currentAnneeAcademiqueId: currentAnneeAcademique.id,
                  isTouchPoint: false
                }
              }
              ).afterClosed().subscribe((disconnect) => {
                if (disconnect) this.disconnect()
              })
            }
            else 
            {
              this.notifyDisponibleBourse(currentStudent);
            }
          })
        }
      })
    });
  }

  notifyDisponibleBourse(currentStudent: any) {
    if (currentStudent)
    {
      this._metfpetService.getToPayEtudiantBourseAmount(currentStudent.id).subscribe((amount: any) => {
        if (amount > 0)
        {
          this.isChecking = true;
          this._dialogService.openConfirmDialog({
            width: '800px',
            data: {
              title: "Votre bourse est disponible",
              message:  `
                <p>Vous pouvez dès à présent retirer votre bourse depuis ParcoursPro.</p> 
                <p>Pour ce faire, rendez-vous dans l’onglet bourse de votre compte et sélectionnez votre mode de retrait.</p>
              `,
              confirmBtnText: 'Retirer ma bourse',
              cancelBtnText: 'Revenir à l’écran d’accueil'
            }
          }).afterClosed().subscribe(confirmed => {
            if (confirmed) {
              this._router.navigate([UiPath.student.bourses.bourses]);
            }
          });
        }
      });
    } 
  }

  disconnect() {
    this._router.navigate(['/login']);
  }

  goToStudentDossiers(){
    this._router.navigate([UiPath.student.dossier]);
  }

  goToAdmission(){
    this._router.navigate([UiPath.student.admission.participate]);
  }

  goToCursus() {
    this._router.navigate([UiPath.student.cursus.inscriptions]);
  }

  goToDocument() {
    this._router.navigate([UiPath.student.document.impressionDocuments]);
  }

  goToBourses() {
    this._router.navigate([UiPath.student.bourses.bourses]);
  }
}
