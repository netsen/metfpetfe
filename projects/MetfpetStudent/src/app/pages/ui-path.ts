export const UiPath = {
  error               : 'error',
  login               : 'login',
  student: {
    admission: {
      participate     : 'admission/participate',
      concours        : 'admission/concours',
      selectPrograms  : 'admission/select-programs',
      manage          : 'admission/manage',
    },
    bourses: {
      bourses: 'bourses',
      retirerMesBourses: 'bourses/retirer-mes-bourses',
      historiqueDeMesBourses: 'bourses/historique-de-mes-bourses'
    },
    cursus: {
      inscriptions    : 'cursus/inscriptions',
    },
    document: {
      impressionDocuments : 'document/impressionDocuments'
    },
    dossier           : 'dossier'
  }
};