import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Store } from '@ngrx/store';
import {
  AppSettings,
  Settings,
  emailValidator, 
  mustEqual,  
  passwordValidator, 
  validateForm,
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  ConfirmationEmailDialog,
} from 'MetfpetLib';

@Component({
  templateUrl: './student-reinscription.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentReinscriptionComponent {

  form: FormGroup;
  settings: Settings;
  loading: boolean;

  constructor(
    public appSettings: AppSettings, 
    private _cdRef: ChangeDetectorRef,
    private _fb: FormBuilder, 
    private _router: Router,
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {

    this.settings = this.appSettings.settings; 
    this.form = this._fb.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'firstName': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'email': [null, Validators.compose([Validators.required, emailValidator])],
      'phone': [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]{9}$")])],
      'password': [null, Validators.compose([Validators.required, passwordValidator])],
      'confirmPassword': [null, Validators.compose([Validators.required, mustEqual('password')])],
      'identifiantNationalEleve': [null, Validators.required],
    });
  }
  
  public validateINA(): void {
    if (this.form.get('identifiantNationalEleve').value) {
      this.loading = true;
      this._metfpetService.validateINA(this.form.get('identifiantNationalEleve').value)
        .subscribe((etudiant) => {
          if (etudiant) {
            this.form.patchValue({
              name      : etudiant.name,
              firstName : etudiant.firstName,
            });
      
            this._cdRef.markForCheck();
          }
        }, 
        error => {
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Invalide INA'
            }
          });
        })
        .add(() => {
          this.loading = false;
          this._cdRef.markForCheck();
        });
    } else {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.ERROR, 
          message: 'Veuillez saisir la INA'
        }
      });
    }
  }

  openMailConfirmation() {
    return this._dialogService.openDialog(ConfirmationEmailDialog, {
      width: '800px',
      data: {
        email: this.form.value.email,
        fullName: this.form.value.firstName + " "+ this.form.value.name
      }
    })
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this.openMailConfirmation().afterClosed()
        .subscribe((res) => {
          if (res) {
            this.loading = true;
            var etudiantInfo = MetfpetServiceAgent.EtudiantInfo.fromJS(this.form.value);
            etudiantInfo.niveauAccesCode = 'autre';
            this._metfpetService.validateEtudiantInfo(etudiantInfo)
              .subscribe(
                () =>  {
                  this._dialogService.openInfoDialog({
                    width: '600px',
                    data: {
                      title: 'Enregistrement effectué',
                      severity: SeverityEnum.INFO, 
                      closeBtnText: 'Terminer',
                      message: `
                        <p>Votre enregistrement sur la plateforme est effectué. </p> 
                        <p>Vous venez de recevoir par E-Mail et SMS votre INA et votre mot de passe. 
                        Vous pouvez désormais vous connecter à la plateforme au moyen des ces identifiants. </p>
                      `
                    }
                  })
                  .afterClosed().subscribe(() => this._router.navigate(['/login']));
                },
                error => {
                  this._dialogService.openInfoDialog({
                    width: '600px',
                    data: {
                      title: 'Erreur d’enregistrement',
                      severity: SeverityEnum.ERROR, 
                      closeBtnText: 'Terminer',
                      message: `
                        <p>Nous n’avons pas pu vous identifier dans la base de données.</p> 
                        <p>Veuillez vérifier vos informations et effectuer une nouvelle tentative. 
                        Si le problème persiste, veuillez contacter l’assistance.</p>
                      `
                    }
                  });
                }
              )
              .add(() => {
                this.loading = false;
                this._cdRef.markForCheck();
              });
          }
      });
    } else {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.ERROR, 
          message: 'Veuillez compléter correctement les informations soulignées en rouge'
        }
      });
    }
  }
}