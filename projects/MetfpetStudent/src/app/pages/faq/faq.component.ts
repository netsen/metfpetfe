import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AppSettings, Settings } from 'MetfpetLib';
@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements AfterViewInit {
  public settings: Settings;
  panelOpenState: boolean = false;
  expansionPanels: HTMLElement;
  panelStates: boolean[] = [false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false];
  constructor(
    public appSettings: AppSettings,
  ) {
    this.settings = this.appSettings.settings;
  }
  ngAfterViewInit() {
    setTimeout(() => this.settings.loadingSpinner = false);
  }
  togglePanel(index: number): void {
    this.panelStates[index] = !this.panelStates[index];
  }
}