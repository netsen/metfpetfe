import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqComponent } from './faq.component';
import { RouterModule } from '@angular/router';
export const routes = [
  { path: '', component: FaqComponent, pathMatch: 'full' }
];
@NgModule({
  declarations: [FaqComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class FaqModule { }