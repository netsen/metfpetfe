import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Component, 
  OnInit,
} from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import {
  AppSettings,
  Settings,
  IAppState,
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  DialogService,
} from 'MetfpetLib';
import { Store, select } from '@ngrx/store';
import { selectCurrentStudent } from '../../shared/store/selectors';

@Component({
  templateUrl: './student-diplomes.component.html', 
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentDiplomesComponent implements OnInit {

  form: FormGroup;
  settings: Settings;
  title: string;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  diplomeList: Array<MetfpetServiceAgent.DiplomeViewModel>;
  loadingIndicator: boolean;
  
  constructor(
    public appSettings: AppSettings, 
    private _formBuilder: FormBuilder, 
    private _cd: ChangeDetectorRef,
    private _store: Store<IAppState>,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings;
  }

  ngOnInit() {
    this._store.pipe(select(selectCurrentStudent)).subscribe(data => {
      this.student = Object.assign({}, data);
      if (!data) return;
      this.title = "Diplômes";
      this.loadDiplomes(this.student.id);
    });
  }

  private loadDiplomes(studentId: string) {
    this._metfpetService.getDiplomeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {
        'etudiantId': studentId
       },
    })).subscribe(data => {
      this.diplomeList = data.results;
      this._cd.markForCheck();
    });
  }
}