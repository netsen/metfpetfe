import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { hideLoading, MetfpetServiceAgent, OtpStatus, showError, showLoading, showSuccess, validateForm } from 'MetfpetLib';

export function numberOnlyValidator(control: FormControl): ValidationErrors | null {
  const value: string = control.value;
  const regExp = new RegExp(/^\d{0,10}$/);
  return regExp.test(value) ? null : { 'numberOnly': true };
}

@Component({
  selector: 'app-otp',
  templateUrl: './otp.dialog.html',
  styleUrls: ["./otp.dialog.css"],
})
export class OtpDialog {
  otpStatus = OtpStatus;
  loading: boolean = false;
  displayForm1: boolean = true;
  form1: FormGroup;
  form2: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _formBuilder: FormBuilder,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _store: Store<any>,
    private _dialogRef: MatDialogRef<OtpDialog>,
    private _cd: ChangeDetectorRef,
  ) {
    this.form1 = this._formBuilder.group({
      phoneNumber: [null, [Validators.required, numberOnlyValidator]]
    })

    this.form2 = this._formBuilder.group({
      phoneNumber: [null, [Validators.required, numberOnlyValidator]],
      otp: [null, [Validators.required, numberOnlyValidator]],
    })
  }

  onSubmitSendOTP() {
    validateForm(this.form1);
    if (this.form1.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.sendStudentOtpCode(MetfpetServiceAgent.SendOtpDTO.fromJS({ ...this.form1.value, ...{ etudiantId: this.data.etudiantId } })).subscribe(
        (res) => {
          if(res?.hasOwnProperty("error")){
            this._store.dispatch(showError({ message: res.error }));
            this.loading = false;
            this._store.dispatch(hideLoading());
            return
          }
          this.loading = false;
          this._store.dispatch(hideLoading());
          this._store.dispatch(showSuccess({ message: "SMS envoyé avec succès. Veillez vérifier vos messages" }));
          this.data.status = this.otpStatus.isPending;
          var phone_number = this.form1.get("phoneNumber").value;
          this.form2.get("phoneNumber").patchValue(phone_number);
          this.displayForm1 = false;
          this._cd.markForCheck();
        },
        (err) => {
          this._store.dispatch(showError({ message: "Erreur d'envoie SMS" }));
          this._store.dispatch(hideLoading());
        }
      )
    }
  }

  onSubmitVerifyOTP() {
    validateForm(this.form2);
    if (this.form2.valid) {
      this._store.dispatch(showLoading());
      var data = {
        ...this.form2.value,
        studentOtpStatusDTO: {
          etudiantId: this.data.etudiantId,
          currentAnneeAcademiqueId: this.data.currentAnneeAcademiqueId
        },
        isTouchPoint: this.data.isTouchPoint,
      }

      this._metfpetService.verifyStudentOtpCode(MetfpetServiceAgent.VerifyOtpDTO.fromJS(data)).subscribe(
        () => {
          this.loading = false;
          this._store.dispatch(hideLoading());
          this._store.dispatch(showSuccess({ message: "Vérification effectuée avec succes" }));
          this.data.status = this.otpStatus.isPending;
          this._cd.markForCheck();
          this.close(false);
        },
        (err) => {
          this._store.dispatch(showError({ message: "Erreur de vérification" }));
          this._store.dispatch(hideLoading());
        }
      )
    }
  }

  close = (disconnect = true) => { this._dialogRef.close(disconnect) }

  goBack(){
    this.displayForm1 = true;
    this._cd.markForCheck();
  }

}

