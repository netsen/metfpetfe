import {
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, of as observableOf } from 'rxjs';
import {
  AppSettings,
  Settings,
  InscriptionProgrammeStatus,
  InscriptionProgrammeStatusMap,
  DialogService,
  MetfpetServiceAgent,
  PerfectScrollService,
  BaseTableComponent,
  CanInscrire,
  showLoading,
  hideLoading,
  showSuccess,
  showError,
  showException,
  StudentStatus,
  OtpStatus,
} from 'MetfpetLib';
import { selectCurrentStudent } from '../../shared/store/selectors/';
import { Tabs } from './student-cursus-tab.component';
import { StudentInscriptionDialog } from './student-inscription.dialog';
import { StudentActions } from '../../shared/store/actions/';
import { OtpDialog } from '../otp/otp.dialog';

@Component({
  templateUrl: './student-inscriptions.component.html',
  styleUrls: ['./student-inscriptions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentInscriptionsComponent extends BaseTableComponent {

  settings: Settings;
  title: string;
  navs = Tabs;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  inscriptionProgrammeStatusMap = InscriptionProgrammeStatusMap;
  anneeAcademiqueOuverte: MetfpetServiceAgent.AnneeAcademiqueDTO;
  addInscriptionButtonText: string = 'Faire une inscription administrative';
  allowToCreateInscription: boolean;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Cursus';
    this.sort = { prop: 'matricule', dir: 'desc' };
  }

  ngAfterViewInit(): void {
    this._store.pipe(select(selectCurrentStudent)).subscribe(student => {
      if (student) {
        setTimeout(() => {
          this.student = student;
          if (this.student.etudiantStatus === <any>StudentStatus.AInscrire) {
            this.addInscriptionButtonText = 'Faire une inscription administrative';
          } else {
            this.addInscriptionButtonText = 'Faire une réinscription administrative';
          }
          this.searchForm.get('etudiant').setValue(student.id);
          this.checkIfAllowToCreateInscription();
          this.triggerSearch();
        })
      }
    });

    this._metfpetService.getAnneeAcademiqueInscriptionOuverte().subscribe(anneeAcademique => {
      this.anneeAcademiqueOuverte = anneeAcademique;
      this._cd.markForCheck();
    });
  }

  checkIfAllowToCreateInscription() {
    this._metfpetService.allowToCreateInscription(this.student.id).subscribe(result => {
      this.allowToCreateInscription = result;
      this._cd.markForCheck();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      etudiant: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return observableOf(this.student.inscriptionsProgrammes);
  }

  addInscription() {
    this._dialogService.openDialog(
      StudentInscriptionDialog,
      {
        width: '620px',
        data: {
          student: this.student,
          inscriptionProgrammeId: null,
          anneeAcademiqueOuverte: this.anneeAcademiqueOuverte,
        }
      }
    ).afterClosed().subscribe(result => {
      if (result) {
        this._store.dispatch(StudentActions.getCurrentStudent());
      }
    })
  }

  editInscription(inscription: MetfpetServiceAgent.InscriptionProgrammeRowViewModel): void {
    this._dialogService.openDialog(
      StudentInscriptionDialog,
      {
        width: '620px',
        data: {
          student: this.student,
          inscriptionProgrammeId: inscription.id
        }
      }
    ).afterClosed().subscribe(result => {
      if (result) {
        this._store.dispatch(StudentActions.getCurrentStudent());
      }
    });
  }

  removeInscription(inscription: MetfpetServiceAgent.InscriptionProgrammeRowViewModel): void {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention",
        message: "Voulez-vous définitivement supprimer l’inscription? Attention cette action est irréversible",
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());

        this._metfpetService.deleteInscriptionProgramme(inscription.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({ message: 'L\'inscription a été supprimée' }));
              this._store.dispatch(StudentActions.getCurrentStudent());
            },
            (error) => this._store.dispatch(showException({ error: error }))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  isInscriptionStatusApayer(inscription: MetfpetServiceAgent.InscriptionProgrammeRowViewModel) {
    return inscription && inscription.statusName == <any>this.inscriptionProgrammeStatusMap[InscriptionProgrammeStatus.APayer];
  }

  get canInscrire(): boolean {
    return this.student && CanInscrire(<any>this.student.etudiantStatus)
      && this.anneeAcademiqueOuverte != null && this.allowToCreateInscription;
  }
}