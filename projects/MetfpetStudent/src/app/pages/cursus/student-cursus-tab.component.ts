import { Component } from '@angular/core';
import { UiPath } from '../ui-path';

export const Tabs = [
  { 
    routerLink: '/' + UiPath.student.cursus.inscriptions, 
    label: 'Inscription & Réinscription administrative' 
  },
];

@Component({
  templateUrl: './student-cursus-tab.component.html',
})
export class StudentCursusTabComponent {

  navs = Tabs;
  title: string;

  constructor() {
    this.title = "Cursus - Inscription & Réinscription administrative";
  }
}
