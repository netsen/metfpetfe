import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { StudentCursusTabComponent } from './student-cursus-tab.component';
import { StudentInscriptionsComponent } from './student-inscriptions.component';
import { StudentInscriptionDialog } from './student-inscription.dialog';

export const routes = [
  { 
    path: '', 
    component: StudentCursusTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'inscriptions',
          pathMatch: 'full'
        },
        { 
          path: 'inscriptions', 
          component: StudentInscriptionsComponent, 
          data: { breadcrumb: 'Inscription / Réinscription' }
        },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    StudentCursusTabComponent,
    StudentInscriptionsComponent,
    StudentInscriptionDialog,
  ],
  entryComponents: [
    StudentInscriptionDialog
  ]
})
export class StudentCursusModule {}
