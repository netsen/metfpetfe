import {
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { DatePipe } from '@angular/common';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import {
  specicalCharacterValidator,
  InscriptionProgrammeStatus,
  StudentAccountBankingComponent,
  DialogService,
  SeverityEnum,
  showSuccess,
  showError,
  showException,
  showLoading,
  hideLoading,
  validateForm,
  MetfpetServiceAgent,
  PaymentServiceAgent,
  PAYMENT_ERROR,
  StudentStatus,
  OtpStatus,
  RegimeValues,
  Regime,
} from 'MetfpetLib';
import { LinkForCycloRegistration } from '../bourses/home-student-bourses.component';
import { RtlScrollAxisType } from '@angular/cdk/platform';
import { selectCurrentStudent } from '../../shared/store/selectors';
import { OtpDialog } from '../otp/otp.dialog';

type OtpDataDialog = {
  status : MetfpetServiceAgent.OtpStatus,
  etudiantId: string,
  currentSessionId:string,
}

@Component({
  templateUrl: './student-inscription.dialog.html',
  styleUrls: ['./student-inscription.dialog.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentInscriptionDialog {

  form: FormGroup;
  form1: FormGroup;
  title: string;
  step: string;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  proposedInscription: MetfpetServiceAgent.InscriptionProgrammeDTO;
  anneeAcademiqueOuverte: MetfpetServiceAgent.AnneeAcademiqueDTO;
  inscriptionProgrammeId: string;
  inscriptionProgramme: MetfpetServiceAgent.InscriptionProgrammeDTO;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  facultyList: Array<MetfpetServiceAgent.FaculteRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  typeFormationList: Array<MetfpetServiceAgent.TypeFormation>;
  montant: number;
  paymentInProgress: boolean;
  showOrangeMoney: boolean = true;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  payment: PaymentServiceAgent.PaymentDTO;
  titlePrefix: string;
  payLaterButtonText: string = 'Payer plus tard';
  etudiantNSGBankingInfo: MetfpetServiceAgent.EtudiantNSGBankingInfosDTO;
  loading: boolean = false;
  otpDataDialog : OtpDataDialog;
  disabledPayerButton : boolean = true;
  regimeList = RegimeValues;

  constructor(
    public dialogRef: MatDialogRef<StudentInscriptionDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _cd: ChangeDetectorRef,
    private _datePipe: DatePipe,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _paymentService: PaymentServiceAgent.HttpService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialog : MatDialog,
  ) {
    this.step = 'stepInscription';
    this.student = data.student;
    if (this.student.etudiantStatus === <any>StudentStatus.AInscrire) {
      this.titlePrefix = 'Inscription';
    } else {
      this.titlePrefix = 'Réinscription';
    }
    this.inscriptionProgrammeId = data.inscriptionProgrammeId;
    this.inscriptionProgramme = new MetfpetServiceAgent.InscriptionProgrammeDTO();
    this.anneeAcademiqueOuverte = data.anneeAcademiqueOuverte;
    this.form = this._formBuilder.group({
      'id': null,
      'etudiantId': this.student.id,
      'matricule': [null],
      'anneeAcademiqueId': [null, Validators.required],
      'institutionId': [null, Validators.required],
      'faculteId': [null, Validators.required],
      'programmeId': [null, Validators.required],
      'niveauEtudeId': [null, Validators.required],
      'typeFormationId': [null, Validators.required],
      'regime': [<any> Regime.Externat, Validators.required],
    });
    this.form1 = this._formBuilder.group({
      'confirmationCode': [null, Validators.required],
    });

    this.form.get('institutionId').valueChanges.pipe(
      tap(institutionId => {
        this.programList = [];
        this.facultyList = [];
        this.form.get('faculteId').setValue(null);
        this.form.get('programmeId').setValue(null);
        if (!institutionId) {
          return;
        }
        this._loadFaculties(institutionId);
      })
    ).subscribe();

    this.form.get('faculteId').valueChanges.pipe(
      tap(faculteId => {
        this.programList = [];
        this.form.get('programmeId').setValue(null);
        let institutionId = this.form.get('institutionId').value;
        if (!institutionId || !faculteId) {
          return;
        }
        this._loadPrograms(faculteId);
      })
    ).subscribe();
  }

  isShownCompteNSG() {
    // let institutionId = this.form.get('institutionId').value;
    // if (institutionId && this.institutionList) 
    // {
    //   let institution = this.institutionList.find(x => x.id == institutionId);
    //   return institution && institution.statusEtablissementName == 'Publique';
    // }
    return false;
  }

  isShownCompteNSGButton() {
    return this.etudiantNSGBankingInfo && !this.etudiantNSGBankingInfo.identifiantCompte;
  }

  ngOnInit() {
    if (this.inscriptionProgrammeId) {
      this._metfpetService.getInscriptionProgramme(this.inscriptionProgrammeId).subscribe(
        inscriptionProgramme => {
          this.inscriptionProgramme = inscriptionProgramme;
          this.title = this.inscriptionProgramme.typeName + ' administrative';
          this.form.patchValue({
            'id': this.inscriptionProgramme.id,
            'matricule': this.inscriptionProgramme.matricule,
            'anneeAcademiqueId': this.inscriptionProgramme.anneeAcademiqueId,
            'institutionId': this.inscriptionProgramme.institutionId,
            'faculteId': this.inscriptionProgramme.faculteId,
            'programmeId': this.inscriptionProgramme.programmeId,
            'niveauEtudeId': this.inscriptionProgramme.niveauEtudeId,
            'typeFormationId': this.inscriptionProgramme.typeFormationId,
            'regime': this.inscriptionProgramme.regime,
          });

          this._cd.markForCheck();
        }
      );
    } else {
      this.title = this.titlePrefix + ' administrative';
    }
    this.otpVerification()
  }

  ngAfterViewInit() {
    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getTypeFormationList().subscribe(data => {
      this.typeFormationList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getProposedInscriptionProgramme(this.student.id).subscribe(proposedInscription => {
      this.proposedInscription = proposedInscription;
      if (this.proposedInscription && !this.inscriptionProgrammeId) {
        this.form.patchValue({
          'institutionId': this.proposedInscription.institutionId,
          'faculteId': this.proposedInscription.faculteId,
          'programmeId': this.proposedInscription.programmeId,
          'anneeAcademiqueId': this.proposedInscription.anneeAcademiqueId,
          'niveauEtudeId': this.proposedInscription.niveauEtudeId
        });
      }
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    this.loadEtudiantNSGBankingInfo();
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onFinish(): void {
    this.dialogRef.close(true);
  }

  onPayer() {
    validateForm(this.form);
    if (this.form.valid) {
      if (this.isShownCompteNSG() && (!this.etudiantNSGBankingInfo || !this.etudiantNSGBankingInfo.identifiantCompte)) {
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.ERROR,
            message: 'Veuillez créer et renseigner votre compte SmartPay'
          }
        });
        return;
      }
      if (this.inscriptionProgrammeId) {
        this.changeToPaymentStep(this.inscriptionProgramme.frais);
      } else {
        this._store.dispatch(showLoading());
        this._metfpetService.getInscriptionTarification(
          MetfpetServiceAgent.InscriptionTarificationRequest.fromJS({
            etudiantId: this.student.id,
            niveauEtudeId: this.form.get('niveauEtudeId').value,
            programmeId: this.form.get('programmeId').value,
          }))
          .subscribe(
            montant => {
              this.changeToPaymentStep(montant);
            },
            (error) => this._store.dispatch(showException({ error: error }))
          )
          .add(() => {
            this._store.dispatch(hideLoading());
            this._cd.markForCheck();
          });
      }

    }
  }

  private changeToPaymentStep(montant: number) {
    this.montant = montant;
    if (this.montant === 0) {
      this.showOrangeMoney = false;
      this.payLaterButtonText = 'Terminer';
    }
    this.step = 'stepPayment';
    this.title = 'Mode de paiement';
  }

  onBack() {
    this.step = 'stepInscription';
    this.title = this.titlePrefix + ' administrative';
  }

  onPayWithOrange() {
    if (this.inscriptionProgrammeId) {
      this._updateInscription({ isPayLater: false });
    } else {
      this._createInscription({ isPayLater: false });
    }
  }

  public get isCreateNew() {
    return !this.inscriptionProgrammeId;
  }

  public get isInscriptionStatusApayer() {
    return !this.inscriptionProgrammeId ||
      this.inscriptionProgramme.status == <any>InscriptionProgrammeStatus.APayer;
  }

  onPayLater() {
    if (this.inscriptionProgrammeId) {
      this._updateInscription({ isPayLater: true });
    } else {
      this._createInscription({ isPayLater: true });
    }
  }

  onGetConfirmationCode() {
    this._store.dispatch(showLoading());
    this._paymentService.getConfirmationCode(this.payment.id).subscribe(
      (result) => {
        if (result) {
          this.form1.get('confirmationCode').setValue(result);
          this._cd.markForCheck();
        }
      },
      error => this._store.dispatch(showException({ error: error }))
    )
      .add(() => this._store.dispatch(hideLoading()));
  }

  onCompletePayment() {
    validateForm(this.form1);
    if (this.form1.valid) {
      this._store.dispatch(showLoading());
      this._paymentService.completeOrangePayment(PaymentServiceAgent.CompletePayment.fromJS(
        {
          notif_token: this.form1.get('confirmationCode').value,
          paymentId: this.payment.id,
          paymentType: PaymentServiceAgent.PaymentType.Inscription
        }
      )).subscribe(
        () => {
          this.step = 'stepFinishPayment';
          this.title = this.titlePrefix + ' administrative terminée';
          this._cd.markForCheck();
        },
        error => this._store.dispatch(showException({ error: error }))
      )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  private _createInscription(option: { isPayLater: boolean }) {
    validateForm(this.form);
    if (this.form.valid) {
      if (option.isPayLater) {
        this._store.dispatch(showLoading());
      } else {
        this.paymentInProgress = true;
      }

      let createInscriptionProgramme = MetfpetServiceAgent.CreateInscriptionProgramme.fromJS(this.form.getRawValue());
      if (this.montant > 0) {
        createInscriptionProgramme.status = MetfpetServiceAgent.InscriptionProgrammeStatus.APayer;
      } else {
        createInscriptionProgramme.status = MetfpetServiceAgent.InscriptionProgrammeStatus.EnValidation;
      }
      this._metfpetService.createInscriptionProgramme(createInscriptionProgramme)
        .subscribe(
          (inscriptionProgramme: MetfpetServiceAgent.InscriptionProgrammeDTO) => {
            if (option.isPayLater) {
              this._store.dispatch(showSuccess({}));
              this.dialogRef.close(inscriptionProgramme);
            } else {
              this._payWithOrange(inscriptionProgramme);
            }
          },
          (error) => this._store.dispatch(showException({ error: error }))
        )
        .add(() => {
          if (option.isPayLater) {
            this._store.dispatch(hideLoading());
          }
        });
    }
  }

  private _updateInscription(option: { isPayLater: boolean }) {
    validateForm(this.form);
    if (this.form.valid) {
      if (option.isPayLater) {
        this._store.dispatch(showLoading());
      } else {
        this.paymentInProgress = true;
      }

      this._metfpetService.updateInscriptionProgramme(
        MetfpetServiceAgent.InscriptionProgrammeDTO.fromJS(Object.assign({}, this.inscriptionProgramme, this.form.getRawValue())))
        .subscribe(
          (inscriptionProgramme: MetfpetServiceAgent.InscriptionProgrammeDTO) => {
            if (option.isPayLater) {
              this._store.dispatch(showSuccess({}));
              this.dialogRef.close(inscriptionProgramme);
            } else {
              this._payWithOrange(inscriptionProgramme);
            }
          },
          (error) => this._store.dispatch(showException({ error: error }))
        )
        .add(() => {
          if (option.isPayLater) {
            this._store.dispatch(hideLoading());
          }
        });
    }
  }

  private _payWithOrange(inscriptionProgramme: MetfpetServiceAgent.InscriptionProgrammeDTO) {
    this._paymentService.createPayment(PaymentServiceAgent.CreatePayment.fromJS(
      {
        etudiantId: this.student.id,
        inscriptionProgammeId: inscriptionProgramme.id,
        orderId: this._datePipe.transform(new Date(), "yyyyMMddHmmss"),
      }
    ))
      .subscribe(
        (payment: PaymentServiceAgent.PaymentDTO) => {
          if (!payment) {
            this._dialogService.openInfoDialog({
              data: {
                severity: SeverityEnum.ERROR,
                message: PAYMENT_ERROR
              }
            });

            return;
          }
          this.payment = payment;
          this.step = 'stepCompletePayment';
          this.title = 'Mode de paiement';
          this.paymentInProgress = false;
          this._cd.markForCheck();
          if (payment.details) {
            window.open(payment.details, "_blank");
          }
        },
        error =>
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR,
              message: PAYMENT_ERROR + ` (Erreur: ${error})`
            }
          })
      );
  }

  private _loadFaculties(institutionId: string) {
    this._metfpetService.getFaculteListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { institution: institutionId }
    })).subscribe(data => {
      this.facultyList = data.results;
      this._cd.markForCheck();
    });
  }

  private _loadPrograms(faculteId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { faculte: faculteId }
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });
  }

  redirectToCyclo() {
    window.open(LinkForCycloRegistration, '_blank');
  }

  openDialog() {
    this._dialogService.openDialog(
      StudentAccountBankingComponent,
      {
        width: '620px',
        data: {
          etudiantId: this.student.id,
        }
      }
    ).afterClosed().subscribe(result => {
      if (result) {
        this.loadEtudiantNSGBankingInfo();
      }
    });
  }

  private loadEtudiantNSGBankingInfo() {
    this._metfpetService.getEtudiantNSGBankingInfos(this.student.id).subscribe(etudiantNSGBankingInfo => {
      this.etudiantNSGBankingInfo = etudiantNSGBankingInfo;
      this._cd.markForCheck();
    });
  }

  checkOtpBeforePayer(){
    this.step = 'stepInscription';
    if(!this.otpDataDialog) return;
    if ((<any>this.otpDataDialog.status === OtpStatus.NotVerified) || (<any>this.otpDataDialog.status === OtpStatus.HasStatusAPayerAndNotVerified)) {
      this._dialog.closeAll();
      this._dialogService.openDialog(
        OtpDialog, {
        data: {
          status: <any>this.otpDataDialog.status,
          etudiantId: this.otpDataDialog.etudiantId,
          currentSessionId: this.otpDataDialog.currentSessionId,
          isTouchPoint: false
        }
      })
    } else {
      this.onPayer();
      this.loading = false;
    }
  }

  otpVerification() {
    this.loading = true;
    this._store.pipe(select(selectCurrentStudent)).subscribe(currentStudent => {
      this._metfpetService.getAnneeAcademiqueAdmissionOuverte().subscribe((currentSession: any) => {
        if (currentSession && currentStudent) {
          this._metfpetService.checkStudentOtpStatus(MetfpetServiceAgent.StudentOtpStatusDTO.fromJS({ etudiantId: currentStudent.id, currentSessionId: currentSession.id })).subscribe((status) => {
            this.loading = false;
            this.disabledPayerButton = false;
            console.log(status)
            this.otpDataDialog = {status : status, etudiantId : currentStudent.id, currentSessionId : currentSession.id}
            this._cd.markForCheck();
          })
        }
      })
    });
  }
}