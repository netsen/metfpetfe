import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Component, 
  OnInit,
} from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import {
  emailValidator,
  AppSettings,
  Settings,
  IAppState,
  validateForm,
  showSuccess,
  showError,
  MetfpetServiceAgent,
  showException,
  DialogService,
  SeverityEnum
} from 'MetfpetLib';
import { Store, select } from '@ngrx/store';
import { selectCurrentStudent } from '../../shared/store/selectors/';
import * as StudentActions from '../../shared/store/actions/student.actions';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  templateUrl: './student-dossier.component.html', 
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentDossierComponent implements OnInit {

  form: FormGroup;
  settings: Settings;
  title: string;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  programsChoice: Array<any>;
  resultsList : Array<any> = [];
  loadingIndicator: boolean;
  loading: boolean;
  profilePhoto: any;

  constructor(
    public appSettings: AppSettings, 
    private _formBuilder: FormBuilder, 
    private _cd: ChangeDetectorRef,
    private _store: Store<IAppState>,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private sanitizer: DomSanitizer,
  ) {
    this.settings = this.appSettings.settings; 
    this.form = this._formBuilder.group({
      'email': [null, Validators.compose([Validators.required, emailValidator])],
      'phone': [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]{9}$")])],
    });
  }

  ngOnInit() {
    this._store.pipe(select(selectCurrentStudent)).subscribe(data => {
      this.student = Object.assign({}, data);
      this.setProfilePhoto();
      if (!data) return;
      if(!data.photoPath){
        this.student.photoPath = "/assets/img/users/default-user.jpg";
      }
      this.title = "Dossier apprenant - Profil";

      this.form.patchValue({
        email : this.student.email,
        phone : this.student.phone,
      });

      this._cd.markForCheck();
    });
  }

  private setProfilePhoto() {
    if (this.student) {
      this.profilePhoto = this.student.hasBiometricPicture ? this.sanitizer.bypassSecurityTrustUrl(this.student.photoPath) : this.student.photoPath;
    }
    else
    {
      this.profilePhoto = "/assets/img/users/default-user.jpg";
    }
  }

  ngAfterViewInit() {
    setTimeout(() => this.settings.loadingSpinner = false);
  }
  
  onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this.loading = true;
      this._cd.markForCheck();
      
      this.student = Object.assign(this.student, this.form.value);
      this._metfpetService.updateEtudiantProfile(MetfpetServiceAgent.EtudiantProfileViewModel.fromJS(
        Object.assign({}, this.student, this.form.value)
      )).subscribe(
        () =>  {
          this._store.dispatch(showSuccess({}));
          this._store.dispatch(StudentActions.getCurrentStudentSuccess({student: this.student}));
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => {
        this.loading = false;
        this._cd.markForCheck();
      });
    }
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    if (tabChangeEvent.index === 0) {
      this.title = "Dossier apprenant - Profil";
    } else {
      this.title = "Dossier apprenant - Historique scolaire";
    }
  }

  public openFile(){
    document.getElementById('photoSelector').click();
  }

  public onFileUpload(event) {
    if (event.target.files && event.target.files.length > 0) {
      var size = event.target.files[0].size / 1024;

      if(size < 2048){
        let file : Blob = event.target.files[0];
        var fileReader = new FileReader();
        fileReader.onloadend = function () {
          
        }
        fileReader.readAsBinaryString(file);
        
        let fileParameter = 
        {
          data : file,
          fileName : event.target.fileName
        };
          
        this._metfpetService.uploadEtudiantPhoto(fileParameter, this.student.id)
          .subscribe(
            (result) => {
              this.student.photoPath = result;
              this.profilePhoto = this.student.photoPath;
              this._cd.markForCheck();
              this._store.dispatch(showSuccess({}));
              this._store.dispatch(StudentActions.getCurrentStudentSuccess({student: this.student}));
            },
            (error) => this._store.dispatch(showError({message: error.message}))
          );
      }
      else
      {
        this._dialogService.openInfoDialog({
          width: '600px',
          data: {
            title: 'Fichier trop volumineux',
            severity: SeverityEnum.ERROR, 
            closeBtnText: 'OK',
            message: `
              <p>Le fichier sélectionné est trop volumineux pour être téléversé.</p>
              <p>Veuillez réduire sa taille.</p>
            `
          }
        });
      }
    }
  }
}