import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, of as observableOf } from 'rxjs';
import {
  AppSettings,
  Settings,
  DialogService, 
  MetfpetServiceAgent,
  PerfectScrollService,
  BaseTableComponent,
  CanInscrire,
  showLoading,
  hideLoading,
  showSuccess,
  showError,
  showException,
  SelectImpressionDocumentDialog,
  ImpressionDocumentPaymentDialog,
  ImpressionDocumentConfirmationCodeDialog,
  ImpressionDocumentConfirmationCommandeDialog,
  ImpressionDocumentStatus,
  ImpressionDocumentConfirmationDeliveryDialog,
  RequirePayment,
} from 'MetfpetLib';
import { selectCurrentStudent } from '../../shared/store/selectors';
import { Tabs } from './student-document-tab.component';
import { StudentActions } from '../../shared/store/actions';

@Component({
  templateUrl: './student-impression-documents.component.html', 
  styleUrls: ['./student-impression-documents.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentImpressionDocumentsComponent extends BaseTableComponent {

  settings: Settings;
  title: string;
  navs = Tabs;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  documentInfo: MetfpetServiceAgent.DocumentInfo;
  documents: Array<MetfpetServiceAgent.DocumentViewModel>;
  allowRequestDocument: boolean;
  
  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Documents officiels';
    this.sort = {prop: 'matricule', dir: 'desc'};
  }

  ngAfterViewInit(): void {
    this._store.pipe(select(selectCurrentStudent)).subscribe(student => {
      if (student) {
        setTimeout(() => {
          this.student = student;
          this.searchForm.get('etudiant').setValue(student.id);
          this.loadDocumentInfo();
          this.triggerSearch();
        })
      }
    });
  }
  
  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      etudiant: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return observableOf(this.student.impressionDocuments);
  }

  public loadDocumentInfo(): void {
    this._metfpetService.getEtudiantDocumentInfo(this.student.id).subscribe(data => {
      this.documentInfo = data;
      this._cd.markForCheck();
    });
    this._metfpetService.getAvailableDocumentList(this.student.id).subscribe(data => {
      this.documents = data;
      this.allowRequestDocument = this.documents.length > 0;
      this._cd.markForCheck();
    });
  }

  public generateDocument(): void {
    this._dialogService.openDialog(
      SelectImpressionDocumentDialog,
      {
        width: '680px',
        data: {
          documentInfo: this.documentInfo,
          documents: this.documents,
        }
      }
    ).afterClosed().subscribe(result => {
      if (result && result.document && result.impressionDocument) {
        if (result.document.requirePayment === <any> RequirePayment.Non) {
          this._dialogService.openDialog(
            ImpressionDocumentConfirmationCommandeDialog,
            {
              width: '760px',
              data: {
                documentName: result.document.name,
                diffusionName: result.document.diffusionName,
                diffusion: result.document.diffusion,
                email: this.documentInfo.email
              }
            }
          ).afterClosed().subscribe(() => {
            this._store.dispatch(StudentActions.getCurrentStudent());
          });
        } else {
          this._dialogService.openDialog(
            ImpressionDocumentPaymentDialog,
            {
              width: '680px',
              data: {
                documentInfo: this.documentInfo,
                documentName: result.document.name,
                documentMontant: result.document.montant,
                impressionDocumentId: result.impressionDocument.id
              }
            }
          ).afterClosed().subscribe(paymentResult => {
            if (paymentResult) {
              this._dialogService.openDialog(
                ImpressionDocumentConfirmationCodeDialog,
                {
                  width: '680px',
                  data: {
                    documentInfo: this.documentInfo,
                    documentName: result.document.name,
                    documentMontant: result.document.montant,
                    paymentId: paymentResult.payment.id
                  }
                }
              ).afterClosed().subscribe(confirmationCodeResult => {
                if (confirmationCodeResult) {
                  this._dialogService.openDialog(
                    ImpressionDocumentConfirmationCommandeDialog,
                    {
                      width: '760px',
                      data: {
                        documentName: result.document.name,
                        diffusionName: result.document.diffusionName,
                        diffusion: result.document.diffusion,
                        email: this.documentInfo.email
                      }
                    }
                  ).afterClosed().subscribe(() => {
                    this._store.dispatch(StudentActions.getCurrentStudent());
                  });
                } else {
                  this._store.dispatch(StudentActions.getCurrentStudent());
                }
              });
            } else {
              this._store.dispatch(StudentActions.getCurrentStudent());
            }
          });
        }
      } 
    });
  }

  public processDocument(row: MetfpetServiceAgent.ImpressionDocumentViewModel): void {
    if (row.status === <any> ImpressionDocumentStatus.APayer) {
        let document = this.documents.find(x => x.id === row.documentId);
        if (document) {
          this._dialogService.openDialog(
            ImpressionDocumentPaymentDialog,
            {
              width: '680px',
              data: {
                documentInfo: this.documentInfo,
                documentName: document.name,
                documentMontant: document.montant,
                impressionDocumentId: row.id
              }
            }
          ).afterClosed().subscribe(paymentResult => {
            if (paymentResult) {
              this._dialogService.openDialog(
                ImpressionDocumentConfirmationCodeDialog,
                {
                  width: '680px',
                  data: {
                    documentInfo: this.documentInfo,
                    documentName: document.name,
                    documentMontant: document.montant,
                    paymentId: paymentResult.payment.id
                  }
                }
              ).afterClosed().subscribe(confirmationCodeResult => {
                if (confirmationCodeResult) {
                  this._dialogService.openDialog(
                    ImpressionDocumentConfirmationCommandeDialog,
                    {
                      width: '760px',
                      data: {
                        documentName: document.name,
                        diffusionName: document.diffusionName,
                        diffusion: document.diffusion,
                        email: this.documentInfo.email
                      }
                    }
                  ).afterClosed().subscribe(() => {
                    this._store.dispatch(StudentActions.getCurrentStudent());
                  });
                } else {
                  this._store.dispatch(StudentActions.getCurrentStudent());
                }
              });
            } else {
              this._store.dispatch(StudentActions.getCurrentStudent());
            }
          });
        }
    } else if (row.status === <any> ImpressionDocumentStatus.Payee) {
      this._dialogService.openDialog(
        ImpressionDocumentConfirmationCommandeDialog,
        {
          width: '760px',
          data: {
            documentName: row.name,
            diffusionName: row.diffusionName,
            diffusion: row.diffusion,
            email: this.documentInfo.email
          }
        }
      );
    } else if (row.status === <any> ImpressionDocumentStatus.Delivree) {
      this._dialogService.openDialog(
        ImpressionDocumentConfirmationDeliveryDialog,
        {
          width: '760px',
          data: {
            documentName: row.name,
            diffusionName: row.diffusionName
          }
        }
      );
    }
  }
}