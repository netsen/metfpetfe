import { Component } from '@angular/core';
import { UiPath } from '../ui-path';

export const Tabs = [
  { 
    routerLink: '/' + UiPath.student.document.impressionDocuments, 
    label: 'Documents officiels' 
  },
];

@Component({
  templateUrl: './student-document-tab.component.html',
})
export class StudentDocumentTabComponent {

  navs = Tabs;
  title: string;

  constructor() {
    this.title = "Documents officiels";
  }
}
