import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { StudentImpressionDocumentsComponent } from './student-impression-documents.component';
import { StudentDocumentTabComponent } from './student-document-tab.component';

export const routes = [
  { 
    path: '', 
    component: StudentDocumentTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'impressionDocuments',
          pathMatch: 'full'
        },
        { 
          path: 'impressionDocuments', 
          component: StudentImpressionDocumentsComponent, 
          data: { breadcrumb: 'Documents officiels' }
        },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    StudentDocumentTabComponent,
    StudentImpressionDocumentsComponent,
  ],
  entryComponents: [
  ]
})
export class StudentDocumentModule {}
