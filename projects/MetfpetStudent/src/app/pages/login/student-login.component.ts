import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { 
  AppSettings,
  Settings,
  LoginTypeEnum, 
  AuthService,
  showError,
  showException,
  MetfpetServiceAgent,
  DialogService,
} from 'MetfpetLib';
import { StudentActions } from '../../shared/store/actions/';
import { TechnicalMaintenanceDialog } from './technical-maintenance.dialog';

@Component({
  templateUrl: './student-login.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentLoginComponent {

  error: string;
  settings: Settings;
  loading: boolean;
  private _recaptchaSubscription: Subscription;
  plateformeInscriptionConfigDTO: MetfpetServiceAgent.PlateformeInscriptionConfigDTO;

  constructor(
    public appSettings: AppSettings, 
    private _cd: ChangeDetectorRef,
    private _router: Router,
    private _store: Store<any>,
    private _authService: AuthService,
    private _recaptchaV3Service: ReCaptchaV3Service,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialogService: DialogService,
  ) {
    this.settings = this.appSettings.settings;
  }

  ngOnInit() {
    this.loading = true;
    this._metfpetService.getPlateformeInscriptionConfigs().subscribe(data => 
      {
        if (data)
        {
          let technicalPage = data.find(x => x.name === 'Plateforme en cours de maintenance');
          if (technicalPage && technicalPage.isShown)
          {
            this.showTechnicalMaintenanceDialog();
          }
          this.plateformeInscriptionConfigDTO = data.find(x => x.name === 'S’inscrire sur la plateforme');
          this._cd.markForCheck(); 
        }
      })
      .add(() => {
        this.loading = false;
        this._cd.markForCheck();
      });
  }

  private showTechnicalMaintenanceDialog() {
    this._dialogService.openDialog(
      TechnicalMaintenanceDialog,
      {
        width: '700px',
        data: {}
      }
    ).afterClosed().subscribe(() => {
      this.showTechnicalMaintenanceDialog();
    });
  }

  showPlateformeInscriptionLink() {
    return this.plateformeInscriptionConfigDTO && this.plateformeInscriptionConfigDTO.isShown;
  }

  ngOnDestroy() {
    if (this._recaptchaSubscription) {
      this._recaptchaSubscription.unsubscribe();
    }
  }

  public onSubmit(event): void {
    this.loading = true;
    if (this._recaptchaSubscription) {
      this._recaptchaSubscription.unsubscribe();
    }

    this._recaptchaSubscription = this._recaptchaV3Service.execute('studentLogin')
      .pipe(
        switchMap(() => this._authService.login(event.identifier, event.password, LoginTypeEnum.Student))
      )
      .subscribe(
        () =>  {
          this._router.navigate(['/']);
          this._store.dispatch(StudentActions.getCurrentStudent());
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => {
        this.loading = false;
        this._cd.markForCheck();
      });
  }
}