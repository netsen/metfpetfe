import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './technical-maintenance.dialog.html',
})
export class TechnicalMaintenanceDialog {

  constructor(
    public dialogRef: MatDialogRef<TechnicalMaintenanceDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onClose(): void {
    this.dialogRef.close();
  }
}