import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { RECAPTCHA_V3_SITE_KEY, RecaptchaV3Module } from 'ng-recaptcha';
import { environment } from '../../../environments/environment';
import { StudentLoginComponent } from './student-login.component';
import { TechnicalMaintenanceDialog } from './technical-maintenance.dialog';

export const routes = [
  { path: '', component: StudentLoginComponent, pathMatch: 'full' },
  { path: 'login', component: StudentLoginComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule, 
    ReactiveFormsModule,
    SharedModule,
    RecaptchaV3Module
  ],
  declarations: [
    StudentLoginComponent,
    TechnicalMaintenanceDialog
  ],
  providers: [
    { provide: RECAPTCHA_V3_SITE_KEY, useValue: environment.recaptchaPublicKey },
  ]
})
export class StudentLoginModule { }