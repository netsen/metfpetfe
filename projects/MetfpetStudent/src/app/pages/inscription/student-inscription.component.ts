import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { tap } from 'rxjs/operators/';
import { Store } from '@ngrx/store';
import {
  AppSettings,
  Settings,
  emailValidator, 
  mustEqual,  
  passwordValidator, 
  validateForm,
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  ConfirmationEmailDialog,
} from 'MetfpetLib';

@Component({
  templateUrl: './student-inscription.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentInscriptionComponent {

  form: FormGroup;
  settings: Settings;
  loading: boolean;
  isBrevetSelected: boolean;
  isBacSelected: boolean;
  isTerminaleSelected: boolean;
  isPostSecondaireSelected: boolean;
  isPostPrimaireSelected: boolean;
  genreList: Array<MetfpetServiceAgent.Genre>;
  centreExamenList: Array<MetfpetServiceAgent.CentreExamenRowViewModel>;
  optionsBEPCList: Array<MetfpetServiceAgent.OptionBEPCRowViewModel>;
  optionsBACList: Array<MetfpetServiceAgent.OptionBACRowViewModel>;
  optionsTerminaleList: Array<MetfpetServiceAgent.OptionTerminaleRowViewModel>;
  collegesList: Array<MetfpetServiceAgent.CollegeRowViewModel>;
  lyceesList: Array<MetfpetServiceAgent.LyceeRowViewModel>;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  sessionsBEPCList: Array<any>;
  sessionsBACList: Array<any>;
  
  typeDiplomeList = [
    {id : "bepc", name : "Brevet"},
    {id : "bac", name : "BAC"},
    {id : "terminale", name : "Terminale"},
    //{id : "postSecondaire", name : "Post-secondaire"},
    //{id : "postPrimaire", name : "Post-primaire"},
  ];
  
  constructor(
    public appSettings: AppSettings, 
    private _cdRef: ChangeDetectorRef,
    private _fb: FormBuilder, 
    private _router: Router,
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {

    this.settings = this.appSettings.settings; 
    this.isBrevetSelected = true;
    this.isBacSelected = false;
    this.isTerminaleSelected = false;
    this.isPostSecondaireSelected = false;
    this.isPostPrimaireSelected = false;
    this.form = this._fb.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'firstName': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'email': [null, Validators.compose([Validators.required, emailValidator])],
      'phone': [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]{9}$")])],
      'genreId': [null, Validators.required],
      'dateNaissance': [null, Validators.required],
      'typeDiplomeSelection': ['bepc'],
      'optionBEPCId': [null],
      'sessionBEPC': [null],
      'optionBACId': [null],
      'sessionBAC': [null],
      'sessionTerminale': [null],
      'optionTerminaleId': [null],
      'collegeId': [null],
      'lyceeId': [null],
      'centreExamenId': null,
      'numeroPV': [null, Validators.pattern("^[0-9]*$")],
      'prefectureId': [null],
      'prefectureResidenceId': [null, Validators.required],
      'password': [null, Validators.compose([Validators.required, passwordValidator])],
      'confirmPassword': [null, Validators.compose([Validators.required, mustEqual('password')])],
    });
  }

  ngOnInit() {
    this.form.get('prefectureId').valueChanges.pipe(
      tap(prefectureId => {
        if (!prefectureId) {
          return;
        }

        this.form.get('collegeId').setValue(null);
        this.form.get('lyceeId').setValue(null);
        this.loading = true;

        this._metfpetService.getCollegeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
           pageIndex: -1, filters: {prefectureId}, sort: {nameKey:'Name'}
        })).subscribe(data => {
          this.collegesList = data.results;
          this.loading = false;
          this._cdRef.markForCheck();
        });

        this._metfpetService.getLyceeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
           pageIndex: -1, filters: {prefectureId}, sort: {nameKey:'Name'}
        })).subscribe(data => {
          this.lyceesList = data.results;
          this.loading = false;
          this._cdRef.markForCheck();
        });
      })
    ).subscribe();
  }

  ngAfterViewInit() {
    this.loading = true;

    this._metfpetService.getGenreList().subscribe(data => this.genreList = data);

    this._metfpetService.getPrefectureList().subscribe(data => this.prefectureList = data);

    this._metfpetService.getCentreExamenListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.centreExamenList = data.results;
      this._cdRef.markForCheck();
    });

    this._metfpetService.getOptionBEPCListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.optionsBEPCList = data.results;
      this._cdRef.markForCheck();
    });

    this._metfpetService.getOptionBACListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.optionsBACList = data.results;
      this._cdRef.markForCheck();
    });
    
    this._metfpetService.getOptionTerminaleListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.optionsTerminaleList = data.results;
      this._cdRef.markForCheck();
    });

    this._metfpetService.getSessionBEPCList().subscribe(data => {
      this.sessionsBEPCList = data.map(x => {return {name: x}});
      this._cdRef.markForCheck();
    });
    
    this._metfpetService.getSessionBACList().subscribe(data => {
      this.sessionsBACList = data.map(x => {return {name: x}});
      this._cdRef.markForCheck();
    });

    this._metfpetService.getCollegeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.collegesList = data.results;
      this.loading = false;
      this._cdRef.markForCheck();
    });

    this._metfpetService.getLyceeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.lyceesList = data.results;
      this.loading = false;
      this._cdRef.markForCheck();
    });

    setTimeout(() => this.settings.loadingSpinner = false);
  }
  
  public onTypeDiplomeChange(): void {
    if (this.form.get('typeDiplomeSelection').value == "bepc") {
        this.isBrevetSelected = true;
        this.isBacSelected = false;
        this.isTerminaleSelected = false;
        this.isPostSecondaireSelected = false;
        this.isPostPrimaireSelected = false;
    }
    else if (this.form.get('typeDiplomeSelection').value == "bac") {
        this.isBrevetSelected = false;
        this.isBacSelected = true;
        this.isTerminaleSelected = false;
        this.isPostSecondaireSelected = false;
        this.isPostPrimaireSelected = false;
    }
    else if (this.form.get('typeDiplomeSelection').value == "terminale") {
        this.isBrevetSelected = false;
        this.isBacSelected = false;
        this.isTerminaleSelected = true;
        this.isPostSecondaireSelected = false;
        this.isPostPrimaireSelected = false;
    }
    else if (this.form.get('typeDiplomeSelection').value == "postSecondaire") {
        this.isBrevetSelected = false;
        this.isBacSelected = false;
        this.isTerminaleSelected = false;
        this.isPostSecondaireSelected = true;
        this.isPostPrimaireSelected = false;
    }
    else if (this.form.get('typeDiplomeSelection').value == "postPrimaire") {
        this.isBrevetSelected = false;
        this.isBacSelected = false;
        this.isTerminaleSelected = false;
        this.isPostSecondaireSelected = false;
        this.isPostPrimaireSelected = true;
    }
  }

  openMailConfirmation() {
    return this._dialogService.openDialog(ConfirmationEmailDialog, {
      width: '800px',
      data: {
        email: this.form.value.email,
        fullName: this.form.value.firstName + " "+ this.form.value.name
      }
    })
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      var valid = true;
      if (this.form.get('typeDiplomeSelection').value == "bepc") {
        if(!this.form.get('sessionBEPC').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la session du BEPC'
            }
          });
          valid = false;
        }
        if(!this.form.get('numeroPV').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir le numéro de PV'
            }
          });
          valid = false;
        }
        if(!this.form.get('prefectureId').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez sélectionner la préfecture d\'origine'
            }
          });
          valid = false;
        }
      }
      else if (this.form.get('typeDiplomeSelection').value == "bac") {
        if(!this.form.get('sessionBAC').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la session du BAC'
            }
          });
          valid = false;
        }
        if(!this.form.get('numeroPV').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir le numéro de PV'
            }
          });
          valid = false;
        }
        if(!this.form.get('prefectureId').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez sélectionner la préfecture d\'origine'
            }
          });
          valid = false;
        }
      }
      else if (this.form.get('typeDiplomeSelection').value == "terminale") {
        if(!this.form.get('sessionTerminale').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir la session de terminale'
            }
          });
          valid = false;
        }
        if(!this.form.get('numeroPV').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez saisir le numéro de PV'
            }
          });
          valid = false;
        }
        if(!this.form.get('prefectureId').value){
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: 'Veuillez sélectionner la préfecture d\'origine'
            }
          });
          valid = false;
        }
      }
  
      if(valid){
        this.openMailConfirmation().afterClosed()
        .subscribe((res) => {
          if (res) {
            this.loading = true;
            var etudiantInfo = MetfpetServiceAgent.EtudiantInfo.fromJS(this.form.value);
            etudiantInfo.niveauAccesCode = this.form.get('typeDiplomeSelection').value;
            this._metfpetService.validateEtudiantInfo(etudiantInfo)
              .subscribe(
                () =>  {
                  this._dialogService.openInfoDialog({
                    width: '600px',
                    data: {
                      title: 'Enregistrement effectué',
                      severity: SeverityEnum.INFO, 
                      closeBtnText: 'Terminer',
                      message: `
                        <p>Votre enregistrement sur la plateforme est effectué. </p> 
                        <p>Vous venez de recevoir par E-Mail et SMS votre INA et votre mot de passe. 
                        Vous pouvez désormais vous connecter à la plateforme au moyen des ces identifiants. </p>
                      `
                    }
                  })
                  .afterClosed().subscribe(() => this._router.navigate(['/login']));
                },
                error => {
                  this._dialogService.openInfoDialog({
                    width: '600px',
                    data: {
                      title: 'Erreur d’enregistrement',
                      severity: SeverityEnum.ERROR, 
                      closeBtnText: 'Terminer',
                      message: `
                        <p>Nous n’avons pas pu vous identifier dans la base de données.</p> 
                        <p>Veuillez vérifier vos informations et effectuer une nouvelle tentative. 
                        Si le problème persiste, veuillez contacter l’assistance.</p>
                      `
                    }
                  });
                }
              )
              .add(() => {
                this.loading = false;
                this._cdRef.markForCheck();
              });
              }
        });
      }
    } else {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.ERROR, 
          message: 'Veuillez compléter correctement les informations soulignées en rouge'
        }
      });
    }
  }
}