import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogService, hideLoading, MetfpetServiceAgent, showException, showLoading } from 'MetfpetLib';
import {  SuccessRetirerDialog } from './result-retirer/success-retirer.dialog';
import { FailureRetirerDialog } from './result-retirer/failure-retirer.dialog';
import { Store } from '@ngrx/store';


@Component({
  templateUrl: './retirer-via-ecobank-express.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RetirerViaEcobankExpressDialog {

  montant: number;
  etudiantEcobank: MetfpetServiceAgent.EtudiantEcobankDTO;
  isProcessing: boolean;

  constructor(
    public dialogRef: MatDialogRef<RetirerViaEcobankExpressDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _cd: ChangeDetectorRef,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialogService: DialogService,
    private _store: Store<any>,
  ) {
    this.montant = data.montant;
    this.etudiantEcobank = data.etudiantEcobank;
    this.isProcessing = false;
  }

  ngOnInit() {}

  withDraw() {
    this.isProcessing = true;
    this._cd.markForCheck();
    this._store.dispatch(showLoading());
    this._metfpetService.withdrawBourseFromEcobank(this.etudiantEcobank.etudiantId).subscribe((status: any) => {
      if (status) {
        this._store.dispatch(hideLoading());
        this.dialogRef.close(true);
        this._dialogService.openDialog(SuccessRetirerDialog, {
          width: '800px',
          data: {},
          panelClass: 'success-retirer-dialog-container',
        })
      }
      this._cd.markForCheck();
    },
    error => this._store.dispatch(showException({error: error}))).add(() => 
      {
        this._store.dispatch(hideLoading());
        this.isProcessing = false;
        this._cd.markForCheck();
      });
  }

  public onClose(): void {
    this.dialogRef.close();
  }
}