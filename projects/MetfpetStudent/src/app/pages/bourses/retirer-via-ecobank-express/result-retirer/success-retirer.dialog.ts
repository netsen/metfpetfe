import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MetfpetServiceAgent } from 'MetfpetLib';

@Component({
  templateUrl: './success-retirer.dialog.html',
  styleUrls: ['./success-retirer.dialog.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SuccessRetirerDialog {

  constructor(
    public dialogRef: MatDialogRef<SuccessRetirerDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _cd: ChangeDetectorRef,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) { }
  public onClose(): void {
    this.dialogRef.close(true);
  }
}