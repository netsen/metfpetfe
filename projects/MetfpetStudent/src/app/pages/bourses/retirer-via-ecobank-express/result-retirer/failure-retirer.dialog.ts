import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MetfpetServiceAgent } from 'MetfpetLib';


@Component({
  templateUrl: './failure-retirer.dialog.html',
  styleUrls: ['./failure-retirer.dialog.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FailureRetirerDialog {

  constructor(
    public dialogRef: MatDialogRef<FailureRetirerDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _cd: ChangeDetectorRef,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {}

  public onClose(): void {
    this.dialogRef.close();
  }
}