import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { StudentBoursesComponent } from './student-bourses.component';
import { StudentMyBoursesDialog } from './student-mybourses.dialog';
import { HomeStudentBoursesComponent } from './home-student-bourses.component';
import { CreateSmartPayAccountDialog } from './create-smartpay-account.dialog';
import { StudentBoursesTabComponent } from './student-bourses-tab.component';
import { StudentRetirerMesBoursesComponent } from './student-retirer-mes-bourses.component';
import { RetirerViaEcobankExpressDialog } from './retirer-via-ecobank-express/retirer-via-ecobank-express.dialog';
import { SuccessRetirerDialog } from './retirer-via-ecobank-express/result-retirer/success-retirer.dialog';
import { FailureRetirerDialog } from './retirer-via-ecobank-express/result-retirer/failure-retirer.dialog';
import { RetirerViaTouchPointDialog } from './retirer-via-touch-point/retirer-via-touch-point.dialog';
import { ConfirmationViaTouchPoinDialog } from './retirer-via-touch-point/confirmation-via-touch-point.dialog';
import { AucuneBourseDispoiableDialog } from './aucune-bourse-disponible/aucune-bourse-disponible.dialog';

export const routes = [
  //{ path: '', component: StudentBoursesComponent, pathMatch: 'full' }
  //{ path: '', component: HomeStudentBoursesComponent, pathMatch: 'full' },
  //{ path: 'list', component: StudentBoursesComponent, pathMatch: 'full' }
  { 
    path: '', 
    component: StudentBoursesTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'retirer-mes-bourses',
          pathMatch: 'full'
        },
        { 
          path: 'retirer-mes-bourses', 
          component: StudentRetirerMesBoursesComponent, 
          data: { breadcrumb: 'Retirer mes bourses' }
        },
        { 
          path: 'historique-de-mes-bourses', 
          component: StudentBoursesComponent, 
          data: { breadcrumb: 'Historique de mes bourses' }
        },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    StudentBoursesComponent,
    StudentMyBoursesDialog,
    HomeStudentBoursesComponent,
    CreateSmartPayAccountDialog,
    StudentBoursesTabComponent,
    StudentRetirerMesBoursesComponent,
    RetirerViaEcobankExpressDialog,
    SuccessRetirerDialog,
    FailureRetirerDialog,
    RetirerViaTouchPointDialog,
    ConfirmationViaTouchPoinDialog,
    AucuneBourseDispoiableDialog
  ]
})
export class StudentBoursesModule {}
