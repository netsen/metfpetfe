import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeStudentBoursesComponent } from './home-student-bourses.component';

describe('HomeStudentBoursesComponent', () => {
  let component: HomeStudentBoursesComponent;
  let fixture: ComponentFixture<HomeStudentBoursesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeStudentBoursesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeStudentBoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
