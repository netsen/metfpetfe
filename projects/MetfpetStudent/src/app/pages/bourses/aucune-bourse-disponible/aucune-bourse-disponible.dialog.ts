import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MetfpetServiceAgent } from 'MetfpetLib';

@Component({
  templateUrl: './aucune-bourse-disponible.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AucuneBourseDispoiableDialog {

  constructor(
    public dialogRef: MatDialogRef<AucuneBourseDispoiableDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _cd: ChangeDetectorRef,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) { }
  public onClose(): void {
    this.dialogRef.close();
  }
}