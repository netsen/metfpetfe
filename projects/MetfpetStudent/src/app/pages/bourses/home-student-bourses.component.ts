import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {
  AppSettings,
  Settings,
  IAppState,
  MetfpetServiceAgent,
  DialogService,
  StudentAccountBankingComponent,
} from 'MetfpetLib';
import { Router } from '@angular/router';
import { selectCurrentStudent } from '../../shared/store/selectors';
import { Store, select } from '@ngrx/store';

export const LinkForCycloRegistration = "https://smartpay.parcoursproguinee.org/classic#users.users.public-registration!groupId=-7535492664248752493";

@Component({
  selector: 'app-home-student-bourses',
  templateUrl: './home-student-bourses.component.html',
  styleUrls: ['./home-student-bourses.component.css']
})
export class HomeStudentBoursesComponent implements OnInit {

  settings: Settings;
  title: string;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;

  constructor(
    private _dialog:MatDialog,
    private _router:Router,
    protected _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialogService: DialogService,
  ) 
  { 
    this.title = "Bourses";
  }

  ngOnInit(): void {
    this._store.pipe(select(selectCurrentStudent)).subscribe(data => {
      this.student = Object.assign({}, data);
      if (!data) return;
      this._metfpetService.getEtudiantNSGBankingInfos(this.student.id).subscribe(etudiantNSGBankingInfo => {
        if (etudiantNSGBankingInfo) {
          this._router.navigateByUrl("/bourses/list");
        }
      },
      (error) => {}
      );
    });
  }

  redirectToCyclo() {
    window.open(LinkForCycloRegistration, '_blank');
  }

  openDialog(){
    this._dialogService.openDialog(
      StudentAccountBankingComponent,
      {
        width: '620px',
        data: {
          etudiantId: this.student.id,
        }
      }
    ).afterClosed().subscribe(result => {
      if(result){
        this._router.navigateByUrl("/bourses/list");
      }
    });
  }

}
