import {
    ChangeDetectorRef,
    ChangeDetectionStrategy,
    Component,
    Inject,
    ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MetfpetServiceAgent } from 'MetfpetLib';

@Component({
    templateUrl: './confirmation-via-touch-point.dialog.html',
    styleUrls: ['./confirmation-via-touch-point.dialog.scss'],
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmationViaTouchPoinDialog {

    constructor(
        public dialogRef: MatDialogRef<ConfirmationViaTouchPoinDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private _cd: ChangeDetectorRef,
        private _metfpetService: MetfpetServiceAgent.HttpService
    ) { }
    public onClose(): void {
        this.dialogRef.close(true);
    }
}