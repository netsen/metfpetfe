import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogService, hideLoading, MetfpetServiceAgent, OtpStatus, showException, showLoading } from 'MetfpetLib';
import { SuccessRetirerDialog } from '../retirer-via-ecobank-express/result-retirer/success-retirer.dialog';
import { ConfirmationViaTouchPoinDialog } from './confirmation-via-touch-point.dialog';
import { select, Store } from '@ngrx/store';
import { OtpDialog } from '../../otp/otp.dialog';
import { selectCurrentStudent } from '../../../shared/store/selectors';


@Component({
  templateUrl: './retirer-via-touch-point.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RetirerViaTouchPointDialog {

  montant: number;
  etudiantEcobank: MetfpetServiceAgent.EtudiantEcobankDTO;
  step: string;
  cashoutPhoneNumbers: [];
  cashoutPhoneNumber: string;
  isProcessing: boolean;
  
  constructor(
    public dialogRef: MatDialogRef<RetirerViaTouchPointDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _cd: ChangeDetectorRef,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialogService: DialogService,
    private _store: Store<any>,
  ) {
    this.step = 'stepSelectOptionWithDraw';
    this.montant = data.montant;
    this.etudiantEcobank = data.etudiantEcobank;
    const phoneNumbers = data.etudiantEcobank.cashoutPhoneNumbers
      ? data.etudiantEcobank.cashoutPhoneNumbers.split(", ")
      : [];
    phoneNumbers.push(data.etudiantEcobank.phoneNumber);
    this.cashoutPhoneNumbers = phoneNumbers;
    this.isProcessing = false;
  }

  ngOnInit() {}

  withDraw() {
    this.isProcessing = true;
    this._cd.markForCheck();
    this._store.dispatch(showLoading());
    this._metfpetService.withdrawBourseFromTouchPoint(MetfpetServiceAgent.TouchPointDTO.fromJS(Object.assign({
      etudiantId: this.etudiantEcobank.etudiantId,
      cashoutPhoneNumber: this.cashoutPhoneNumber
    }))).subscribe((status: any) => {
      if (status) {
        this._store.dispatch(hideLoading());
        this.dialogRef.close(true);
        this._dialogService.openDialog(ConfirmationViaTouchPoinDialog, {
          width: '800px',
          data: {},
          panelClass: 'confirmation-retirer-dialog-container',
        })
      }
      this._cd.markForCheck();
    },
    error => this._store.dispatch(showException({error: error}))).add(() => 
      {
        this._store.dispatch(hideLoading());
        this.isProcessing = false;
        this._cd.markForCheck();
      });
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  select(item) {
    this.cashoutPhoneNumber = item;
    this.step ="stepWithDraw"
  }

  openOtpDialog() {
    this._store.dispatch(showLoading());
    this._store.pipe(select(selectCurrentStudent)).subscribe(currentStudent => {
      this._metfpetService.getAnneeAcademiqueAdmissionOuverte().subscribe((currentAnneeAcademique: any) => {
        if (currentAnneeAcademique && currentStudent) {
          this._metfpetService.checkStudentOtpStatus(MetfpetServiceAgent.StudentOtpStatusDTO.fromJS({ etudiantId: currentStudent.id, currentAnneeAcademiqueId: currentAnneeAcademique.id })).subscribe((status) => {
            if ((<any>status === OtpStatus.isVerified)) {
              this._store.dispatch(hideLoading());
              this._dialogService.openDialog(
                OtpDialog, {
                data: {
                  status: <any>status,
                  etudiantId: currentStudent?.id,
                  currentAnneeAcademiqueId: currentAnneeAcademique.id,
                  isTouchPoint: true
                }
              }).afterClosed().subscribe((success) => {
                if (!success) {
                  this._metfpetService.getStudentVerifiedPhoneNumber(this.data.studentId).subscribe((etudiantEcobank: any) => {
                    this.etudiantEcobank = etudiantEcobank;
                    const phoneNumbers = etudiantEcobank.cashoutPhoneNumbers
                      ? etudiantEcobank.cashoutPhoneNumbers.split(", ")
                      : [];
                    phoneNumbers.push(etudiantEcobank.phoneNumber);
                    this.cashoutPhoneNumbers = phoneNumbers;
                    this.cashoutPhoneNumbers = etudiantEcobank.cashoutPhoneNumbers?.split(", ");
                    this._cd.markForCheck();
                  });
                };
              })
            }
          })
        }
      })
    });
  }
}