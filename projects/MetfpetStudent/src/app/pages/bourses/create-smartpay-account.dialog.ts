import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogService, StudentAccountBankingComponent } from 'MetfpetLib';
import { LinkForCycloRegistration } from './home-student-bourses.component';


@Component({
  templateUrl: './create-smartpay-account.dialog.html',
  styleUrls: ['./create-smartpay-account.dialog.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateSmartPayAccountDialog {

  studentId: string;
  closeBtnText: string;
  hasSmartPayAccount: boolean;

  constructor(
    public dialogRef: MatDialogRef<CreateSmartPayAccountDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
  ) {
    this.closeBtnText = 'Se déconnecter';
    this.hasSmartPayAccount = false;
    this.studentId = data.studentId;
  }
  
  redirectToCyclo() {
    window.open(LinkForCycloRegistration, '_blank');
  }

  openDialog(){
    this._dialogService.openDialog(
      StudentAccountBankingComponent,
      {
        width: '620px',
        data: {
          etudiantId: this.studentId,
        }
      }
    ).afterClosed().subscribe(result => {
      if(result) {
        this.hasSmartPayAccount = true;
        this.closeBtnText = 'Connecter';
        this._cd.markForCheck();
      }
    });
  }

  public onClose(): void {
    this.dialogRef.close(this.hasSmartPayAccount);
  }
}