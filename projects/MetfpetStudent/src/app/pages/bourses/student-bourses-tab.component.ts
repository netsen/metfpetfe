import { Component } from '@angular/core';
import { UiPath } from '../ui-path';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

export const Tabs = [
  { 
    routerLink: '/' + UiPath.student.bourses.retirerMesBourses, 
    label: 'Retirer mes bourses' 
  },
  { 
    routerLink: '/' + UiPath.student.bourses.historiqueDeMesBourses, 
    label: 'Historique de mes bourses' 
  },
];

@Component({
  templateUrl: './student-bourses-tab.component.html',
})
export class StudentBoursesTabComponent {
  
  navs = Tabs;
  title: string;
  routerEventSubscription: Subscription;
  
  constructor(private router: Router) {
    this.title = 'Bourses - Retirer mes bourses';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = 'Bourses - ' + selectedTab.label;
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
