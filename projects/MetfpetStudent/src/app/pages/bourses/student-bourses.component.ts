import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Component, 
  OnInit,
} from '@angular/core';
import {
  AppSettings,
  Settings,
  IAppState,
  MetfpetServiceAgent,
  DialogService,
  PerfectScrollService,
  TypeBourse,
  StudentAccountBankingComponent,
} from 'MetfpetLib';
import { Store, select } from '@ngrx/store';
import { selectCurrentStudent } from '../../shared/store/selectors';
import { StudentMyBoursesDialog } from './student-mybourses.dialog';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Tabs } from './student-bourses-tab.component';

@Component({
  templateUrl: './student-bourses.component.html', 
  styleUrls: ['./student-bourses.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class StudentBoursesComponent {
  
  settings: Settings;
  title: string;
  navs = Tabs;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  paiementBourses: Array<MetfpetServiceAgent.PaiementBourseRowViewModel>;
  activeBoursesNumber: number;
  inactiveBoursesNumber: number;
  etudiantBourseModifications: Array<MetfpetServiceAgent.EtudiantBourseModificationRowViewModel>;
  bourseStatus: string = 'Actif';
  bourseReason: string = 'Aucun motif de perte de bourse';

  bourse : Array<{
    typeBourse: string,
    session : string,
    trimestre : string,
    montant: number,
    statut: string
  }>
  
  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<IAppState>,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialogService: DialogService
  ) {
    this.settings = this.appSettings.settings; 
    this.paiementBourses = [];
    this.bourse = [{
      typeBourse : "Entiere",
      session : "2023",
      trimestre : "3e",
      montant: 20000000,
      statut : "Actif"
    }]
  }

  private loadBoursesData(studentId: string) {
    this._metfpetService.getEtudiantPaiementBourseList(studentId).subscribe(data => {
      this.paiementBourses = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getEtudiantBourseList(studentId).subscribe(data => {
      if (data) {
        this.activeBoursesNumber = data.filter(x => x.etudiantBourseStatusName === 'Actif').length;
        this.inactiveBoursesNumber = data.filter(x => x.etudiantBourseStatusName === 'Inactif').length;
        if (this.inactiveBoursesNumber > 0) {
          this.bourseStatus = 'Inactif';
        } else if (this.activeBoursesNumber > 0) {
          this.bourseStatus = 'Actif';
        } else {
          this.bourseStatus = 'Aucun';
          this.bourseReason = '';
        }
      }
      this._cd.markForCheck();
    });

    this._metfpetService.getEtudiantBourseModificationList(studentId).subscribe(data => {
      this.etudiantBourseModifications = data;
      if (this.etudiantBourseModifications) {
        var lostBourseReason = this.etudiantBourseModifications.filter(x => x.typeName == 'Perte de bourse(s)');
        if (lostBourseReason.length > 0) {
          this.bourseReason = '';
          for (var item of lostBourseReason) {
            this.bourseReason = this.bourseReason + item.raisonStatutBourseName + ',';
          }
          this.bourseReason = this.bourseReason.slice(0, -1);
        }
      }
      this._cd.markForCheck();
    });
  }

  ngOnInit() {
    this._store.pipe(select(selectCurrentStudent)).subscribe(data => {
      this.student = Object.assign({}, data);
      if (!data) return;

      this.title = "Bourses";
      this.loadBoursesData(this.student.id);
      this._cd.markForCheck();
    });
  }

  ngAfterViewInit() {
    setTimeout(() => this.settings.loadingSpinner = false);
  }
}