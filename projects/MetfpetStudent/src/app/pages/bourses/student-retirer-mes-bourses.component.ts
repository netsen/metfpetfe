import {
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, of as observableOf } from 'rxjs';
import {
  AppSettings,
  Settings,
  DialogService,
  MetfpetServiceAgent,
  PerfectScrollService,
  showLoading,
  hideLoading,
  showSuccess,
  showError,
  showException,
  StudentStatus,
  OtpStatus,
} from 'MetfpetLib';
import { selectCurrentStudent } from '../../shared/store/selectors';
import { Tabs } from './student-bourses-tab.component';
import { RetirerViaEcobankExpressDialog } from './retirer-via-ecobank-express/retirer-via-ecobank-express.dialog';
import { RetirerViaTouchPointDialog } from './retirer-via-touch-point/retirer-via-touch-point.dialog';
import { AucuneBourseDispoiableDialog } from './aucune-bourse-disponible/aucune-bourse-disponible.dialog';
import { FailureRetirerDialog } from './retirer-via-ecobank-express/result-retirer/failure-retirer.dialog';

@Component({
  templateUrl: './student-retirer-mes-bourses.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentRetirerMesBoursesComponent {

  settings: Settings;
  title: string;
  navs = Tabs;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  montant: number;
  etudiantEcobank: MetfpetServiceAgent.EtudiantEcobankDTO;
  
  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings;
    this.title = 'Bourses';
  }

  ngAfterViewInit(): void {
    this._store.pipe(select(selectCurrentStudent)).subscribe(student => {
      if (student) {
        setTimeout(() => {
          this.student = student;
          this.getToPayEtudiantBourseAmount();
          this._metfpetService.getStudentVerifiedPhoneNumber(student.id).subscribe((etudiantEcobank: any) => {
            this.etudiantEcobank = etudiantEcobank;
            this._cd.markForCheck();
          });
        })
      }
    });
  }

  private getToPayEtudiantBourseAmount() {
    this._metfpetService.getToPayEtudiantBourseAmount(this.student.id).subscribe((montant: any) => {
      this.montant = montant;
      this._cd.markForCheck();
    });
  }

  checkAucuneBourseDispoiable(): boolean {
    if (this.montant == 0) {
      this._dialogService.openDialog(AucuneBourseDispoiableDialog, {
        width: '800px',
        data: {}
      });
      return true;
    }
    return false;
  }

  retirerViaEcobankXpress() {
    if (this.checkAucuneBourseDispoiable()) return;

    if (!this.etudiantEcobank || !this.etudiantEcobank.ecobankAcountNumber) {
      this._dialogService.openDialog(FailureRetirerDialog, {
        width: '800px',
        data: {},
        panelClass: 'failure-retirer-dialog-container',
      })
      return;
    }

    this._dialogService.openDialog(RetirerViaEcobankExpressDialog, {
      width: '800px',
      data: {
        montant: this.montant,
        etudiantEcobank: this.etudiantEcobank
      }
    }).afterClosed().subscribe((result) => {
      if (result)
      {
        this.getToPayEtudiantBourseAmount();
      }
    });
  }

  retirerViaTouchPoint() {
    if (this.checkAucuneBourseDispoiable()) return;
    if (!this.etudiantEcobank?.phoneNumber) return;

    this._dialogService.openDialog(RetirerViaTouchPointDialog, {
      width: '800px',
      data: {
        montant: this.montant,
        etudiantEcobank: this.etudiantEcobank,
        studentId: this.student.id
      }
    }).afterClosed().subscribe((result) => {
      if (result)
      {
        this.getToPayEtudiantBourseAmount();
      }
    });
  }
}