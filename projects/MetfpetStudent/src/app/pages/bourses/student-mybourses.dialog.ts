import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MetfpetServiceAgent } from 'MetfpetLib';
import { SelectionType } from '@swimlane/ngx-datatable';


@Component({
  templateUrl: './student-mybourses.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentMyBoursesDialog {

  studentId: string;
  etudiantBourses: Array<MetfpetServiceAgent.EtudiantBourseRowViewModel>;
  SelectionType = SelectionType;
  etudiantBourseModifications: Array<MetfpetServiceAgent.EtudiantBourseModificationRowViewModel>;
  
  constructor(
    public dialogRef: MatDialogRef<StudentMyBoursesDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _cd: ChangeDetectorRef,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.studentId = data.studentId;
    this.etudiantBourses = [];
  }

  ngOnInit() {
    this.loadEtudiantBourses();
  }

  private loadEtudiantBourses() {
    this._metfpetService.getEtudiantBourseList(this.studentId).subscribe(data => {
      this.etudiantBourses = data;
      this._cd.markForCheck();
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }
  
}