import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Component, 
  ViewEncapsulation
} from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { 
  IAppState, 
  emailValidator,
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import { Store, select } from '@ngrx/store';
import { selectCurrentStudent } from '../../shared/store/selectors/';
import * as StudentActions from '../../shared/store/actions/student.actions';

@Component({
  templateUrl: './student-profile.dialog.html', 
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StudentProfileDialog {

  form: FormGroup;
  title: string;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  
  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogRef: MatDialogRef<StudentProfileDialog>,
    private _formBuilder: FormBuilder, 
    private _store: Store<IAppState>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.form = this._formBuilder.group({
      'email': [null, Validators.compose([Validators.required, emailValidator])],
      'phone': [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]{9}$")])],
    });
  }

  ngOnInit() {
    this._store.pipe(select(selectCurrentStudent)).subscribe(data => { 
      this.student = data;
      if (!data) return;
      this.title = this.student.firstName + ' ' + this.student.name;

      this.form.patchValue({
        email: this.student.email,
        phone: this.student.phone,
      });

      this._cd.markForCheck();
    })
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this.student = Object.assign({}, this.student, this.form.value);

      this._metfpetService.updateEtudiantProfile(MetfpetServiceAgent.EtudiantProfileViewModel.fromJS(
        Object.assign({}, this.student, this.form.value)
      )).subscribe(
        () =>  {
          this._store.dispatch(showSuccess({}));
          this._store.dispatch(StudentActions.getCurrentStudentSuccess({student: this.student}));
          this._dialogRef.close(true);
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public onClose(): void {
    this._dialogRef.close();
  }
}