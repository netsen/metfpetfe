import { createReducer, on } from '@ngrx/store';
import { StudentActions } from '../actions/';

export const studentReducer = createReducer(
  null,

  on(StudentActions.getCurrentStudentSuccess, (state, action) => {
    return action.student;
  })
);