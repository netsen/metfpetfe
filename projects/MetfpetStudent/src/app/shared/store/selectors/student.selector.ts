import { IAppState } from 'MetfpetLib';

export const selectCurrentStudent = (state: IAppState) => 
  (state && state.student) ? state.student : null;
