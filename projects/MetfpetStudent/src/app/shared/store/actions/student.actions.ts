import { createAction, props } from '@ngrx/store';
import { MetfpetServiceAgent } from 'MetfpetLib';

export const getCurrentStudent = createAction(
  '[Student] GetCurrentStudent'
);

export const getCurrentStudentSuccess = createAction(
  '[Student] GetCurrentStudentSuccess',
  props<{student: MetfpetServiceAgent.EtudiantProfileViewModel}>()
);

