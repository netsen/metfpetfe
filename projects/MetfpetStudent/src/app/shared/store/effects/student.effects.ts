import { Injectable } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { StudentActions } from '../actions/';
import { AuthService, MetfpetServiceAgent } from 'MetfpetLib';

@Injectable()
export class StudentEffects {

  constructor(
    private _actions$: Actions,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
  }

  getCurrentStudent$ = createEffect(() => 
    this._actions$.pipe(
      ofType(StudentActions.getCurrentStudent),
      switchMap(() => 
        this._metfpetService.getEtudiantProfile(this._authService.getUserId())
          .pipe(
            map((student: MetfpetServiceAgent.EtudiantProfileViewModel) => StudentActions.getCurrentStudentSuccess({student}))
          )
      )
    ), { dispatch: true }
  );
}