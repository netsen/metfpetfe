export const environment = {
    production: true,
    recaptchaPublicKey: '6LcV_cgfAAAAAKdVd1Pqq8HH4dFEcBWDCYjrpEnR',
    apiEndPoint: 'https://api.parcoursproguinee.org',
    enableQueueIt: false,
};
