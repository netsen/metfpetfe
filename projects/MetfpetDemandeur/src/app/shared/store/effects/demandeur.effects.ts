import { Injectable } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { DemandeurActions } from '../actions';
import { AuthService, MetfpetServiceAgent } from 'MetfpetLib';

@Injectable()
export class DemandeurEffects {

  constructor(
    private _actions$: Actions,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
  }

  getCurrentDemandeur$ = createEffect(() => 
    this._actions$.pipe(
      ofType(DemandeurActions.getCurrentDemandeur),
      switchMap(() => 
        this._metfpetService.getDemandeurProfile(this._authService.getUserId())
          .pipe(
            map((demandeur: MetfpetServiceAgent.DemandeurProfileViewModel) => DemandeurActions.getCurrentDemandeurSuccess({demandeur}))
          )
      )
    ), { dispatch: true }
  );
}