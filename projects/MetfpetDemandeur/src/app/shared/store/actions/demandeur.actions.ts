import { createAction, props } from '@ngrx/store';
import { MetfpetServiceAgent } from 'MetfpetLib';

export const getCurrentDemandeur = createAction(
  '[Demandeur] GetCurrentDemandeur'
);

export const getCurrentDemandeurSuccess = createAction(
  '[Demandeur] GetCurrentDemandeurSuccess',
  props<{demandeur: MetfpetServiceAgent.DemandeurProfileViewModel}>()
);

