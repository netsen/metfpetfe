import { Action, ActionReducer, ActionReducerMap, INIT, MetaReducer } from '@ngrx/store';
import { IAppState, authReducer, loadingReducer, logout } from 'MetfpetLib';
import { demandeurReducer } from './demandeur.reducer';

export const rootReducer: ActionReducerMap<IAppState> = {
  loggedInPerson: authReducer,
  loading: loadingReducer,
  demandeur: demandeurReducer,
};

export function clearState(reducer: ActionReducer<IAppState>): ActionReducer<IAppState> {
  return function(state: IAppState, action: Action): IAppState {
    if (action != null && action.type === logout.type) {
      return reducer(undefined, {type: INIT});
    }
    return reducer(state, action);
  };
}
export const metaReducers: MetaReducer<IAppState>[] = [clearState];