import { createReducer, on } from '@ngrx/store';
import { DemandeurActions } from '../actions';

export const demandeurReducer = createReducer(
  null,

  on(DemandeurActions.getCurrentDemandeurSuccess, (state, action) => {
    return action.demandeur;
  })
);