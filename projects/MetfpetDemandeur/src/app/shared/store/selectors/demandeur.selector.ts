import { IAppState } from 'MetfpetLib';

export const selectCurrentDemandeur = (state: IAppState) => 
  (state && state.demandeur) ? state.demandeur : null;
