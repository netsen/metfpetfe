import { Routes, RouterModule, PreloadAllModules  } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { UiPath } from './pages/ui-path';
import {
  PageErrorComponent, 
  PageNotFoundComponent,
} from 'MetfpetLib';

export const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/demandeur.module').then(m => m.DemandeurModule),
  },
  { 
    path: UiPath.error, 
    component: PageErrorComponent 
  },
  { 
    path: '**', 
    component: PageNotFoundComponent 
  }
];

declare module '@angular/core' {
  interface ModuleWithProviders<T = any> {
  ngModule: Type<T>;
  providers?: Provider[];
  }
}

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, {
   preloadingStrategy: PreloadAllModules,  // <- comment this line for activate lazy load
   // useHash: true
});