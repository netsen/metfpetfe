import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Store } from '@ngrx/store';
import {
  AppSettings,
  Settings,
  emailValidator, 
  mustEqual,  
  passwordValidator, 
  validateForm,
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
} from 'MetfpetLib';

@Component({
  templateUrl: './demandeur-register.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DemandeurRegisterComponent {

  form: FormGroup;
  settings: Settings;
  loading: boolean;
  genreList: Array<MetfpetServiceAgent.Genre>;

  constructor(
    public appSettings: AppSettings, 
    private _cdRef: ChangeDetectorRef,
    private _fb: FormBuilder, 
    private _router: Router,
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {

    this.settings = this.appSettings.settings; 
    this.form = this._fb.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'firstName': [null, Validators.compose([Validators.minLength(2)])],
      'email': [null, Validators.compose([Validators.required, emailValidator])],
      'phone': [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]{9}$")])],
      'password': [null, Validators.compose([Validators.required, passwordValidator])],
      'confirmPassword': [null, Validators.compose([Validators.required, mustEqual('password')])],
      'genreId': null,
      'dateNaissance': null,
    });
  }
  
  ngAfterViewInit() {
    this.loading = true;
    this._metfpetService.getGenreList().subscribe(data => {
      this.genreList = data;
      this.loading = false;
      this._cdRef.markForCheck();
    });
    setTimeout(() => this.settings.loadingSpinner = false);
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      var valid = true;
      if(!this.form.get('firstName').value){
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.ERROR, 
            message: 'Veuillez saisir la prénom'
          }
        });
        valid = false;
      }
      if(!this.form.get('genreId').value){
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.ERROR, 
            message: 'Veuillez saisir la sexe'
          }
        });
        valid = false;
      }
      if (valid) {
        this.loading = true;
        var demandeurInfo = MetfpetServiceAgent.RegisterDemandeurViewModel.fromJS(this.form.value);
        this._metfpetService.registerDemandeur(demandeurInfo)
          .subscribe(
            () =>  {
              this._dialogService.openInfoDialog({
                width: '600px',
                data: {
                  title: 'Enregistrement effectué',
                  severity: SeverityEnum.INFO, 
                  closeBtnText: 'Terminer',
                  message: `
                    <p>Votre enregistrement sur la plateforme est effectué. </p> 
                    <p>Vous venez de recevoir par E-Mail et SMS votre identifiant et votre mot de passe. 
                    Vous pouvez désormais vous connecter à la plateforme au moyen des ces identifiants. </p>
                  `
                }
              })
              .afterClosed().subscribe(() => this._router.navigate(['/login']));
            },
            error => {
              if (error && error.isApiException && JSON.parse(error.response).Message)
              {
                this._dialogService.openInfoDialog({
                  width: '400px',
                  data: {
                    title: 'Erreur d’enregistrement',
                    severity: SeverityEnum.ERROR, 
                    closeBtnText: 'Terminer',
                    message: JSON.parse(error.response).Message
                  }
                });
              } 
              else
              {
                this._dialogService.openInfoDialog({
                  width: '600px',
                  data: {
                    title: 'Erreur d’enregistrement',
                    severity: SeverityEnum.ERROR, 
                    closeBtnText: 'Terminer',
                    message: `
                      <p>Nous n’avons pas pu vous identifier dans la base de données.</p> 
                      <p>Veuillez vérifier vos informations et effectuer une nouvelle tentative. 
                      Si le problème persiste, veuillez contacter l’assistance.</p>
                    `
                  }
                });
              }
            }
          )
          .add(() => {
            this.loading = false;
            this._cdRef.markForCheck();
          });
      } else {
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.ERROR, 
            message: 'Veuillez compléter correctement les informations soulignées en rouge'
          }
        });
      }
    }
  }
}