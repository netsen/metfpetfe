import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Component, 
  OnInit,
} from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import {
  emailValidator,
  AppSettings,
  Settings,
  IAppState,
  validateForm,
  showSuccess,
  showError,
  MetfpetServiceAgent,
  showException,
  SeverityEnum,
  DialogService,
} from 'MetfpetLib';
import { Store, select } from '@ngrx/store';
import { selectCurrentDemandeur } from '../../shared/store/selectors';
import * as StudentActions from '../../shared/store/actions/demandeur.actions';
import { MatTabChangeEvent } from '@angular/material/tabs';

@Component({
  templateUrl: './demandeur-dossier.component.html', 
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DemandeurDossierComponent implements OnInit {

  form: FormGroup;
  settings: Settings;
  title: string;
  demandeur: MetfpetServiceAgent.DemandeurProfileViewModel;
  attachedTypeDemandeurDocuments: Array<any>;
  genreList: Array<MetfpetServiceAgent.Genre>;
  loadingIndicator: boolean;
  loading: boolean;
  
  constructor(
    public appSettings: AppSettings, 
    private _formBuilder: FormBuilder, 
    private _cd: ChangeDetectorRef,
    private _store: Store<IAppState>,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings; 
    this.form = this._formBuilder.group({
      'identifiant': null,
      'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'firstName': [null, Validators.compose([Validators.minLength(2)])],
      'email': [null, Validators.compose([Validators.required, emailValidator])],
      'phone': [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]{9}$")])],
      'genreId': null,
      'dateNaissance': null,
    });
  }

  ngOnInit() {
    this._store.pipe(select(selectCurrentDemandeur)).subscribe(data => {
      this.demandeur = Object.assign({}, data);
      if (!data) return;
      this.title = "Mon profil";

      this._metfpetService.getAttachedDemandeurDocuments(this.demandeur.id).subscribe(data => {
        this.attachedTypeDemandeurDocuments = data;
        this._cd.markForCheck();
      });

      this.form.patchValue({
        email                     : this.demandeur.email,
        phone                     : this.demandeur.phone,
        identifiant               : this.demandeur.identifiant,
        name                      : this.demandeur.name,
        firstName                 : this.demandeur.firstName,
        dateNaissance             : this.demandeur.dateNaissance,
        genreId                   : this.demandeur.genreId,
      }, { emitEvent: false });
      
      this._cd.markForCheck();
    });
  }

  ngAfterViewInit() {
    this._metfpetService.getGenreList().subscribe(data => {
      this.genreList = data;
      this._cd.markForCheck();
    });
    setTimeout(() => this.settings.loadingSpinner = false);
  }
  
  openAttachedTypeDemandeurDocument(attachedDocument: any) {
    window.open(attachedDocument.link, "_blank");
  }
  
  onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      var valid = true;
      if(!this.form.get('firstName').value){
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.ERROR, 
            message: 'Veuillez saisir la prénom'
          }
        });
        valid = false;
      }
      if(!this.form.get('genreId').value){
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.ERROR, 
            message: 'Veuillez saisir la sexe'
          }
        });
        valid = false;
      }

      if (valid) {
        this.loading = true;
        this._cd.markForCheck();
        
        this.demandeur = Object.assign(this.demandeur, this.form.value);
        this.demandeur.isModificationRequest = true;
        this._metfpetService.updateDemandeurProfile(MetfpetServiceAgent.DemandeurProfileViewModel.fromJS(
          Object.assign({}, this.demandeur, this.form.value)
        )).subscribe(
          () =>  {
            this._store.dispatch(StudentActions.getCurrentDemandeurSuccess({demandeur: this.demandeur}));
            this._dialogService.openInfoDialog({
              width: '600px',
              data: {
                title: 'Demande de modification d’un compte',
                severity: SeverityEnum.INFO, 
                closeBtnText: 'Je confirme avoir lu et compris',
                message: `
                  <p>Afin de pouvoir valider vos modifications, le Ministère de la Santé doit préalablement les approuver. </p> 
                  <p>Vous receverez un message une fois vos modifications approuvées. </p>
                `
              }
            });
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => {
          this.loading = false;
          this._cd.markForCheck();
        });
      }
    }
  }
}