import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { 
  AppSettings,
  Settings,
  LoginTypeEnum, 
  AuthService,
  showError,
  showException,
} from 'MetfpetLib';
import { DemandeurActions } from '../../shared/store/actions';

@Component({
  templateUrl: './demandeur-login.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DemandeurLoginComponent {

  error: string;
  settings: Settings;
  loading: boolean;
  private _recaptchaSubscription: Subscription;

  constructor(
    public appSettings: AppSettings, 
    private _cd: ChangeDetectorRef,
    private _router: Router,
    private _store: Store<any>,
    private _authService: AuthService,
    private _recaptchaV3Service: ReCaptchaV3Service
  ) {
    this.settings = this.appSettings.settings;
  }

  ngOnDestroy() {
    if (this._recaptchaSubscription) {
      this._recaptchaSubscription.unsubscribe();
    }
  }

  public onSubmit(event): void {
    this.loading = true;
    if (this._recaptchaSubscription) {
      this._recaptchaSubscription.unsubscribe();
    }

    this._recaptchaSubscription = this._recaptchaV3Service.execute('studentLogin')
      .pipe(
        switchMap(() => this._authService.login(event.identifier, event.password, LoginTypeEnum.Demandeur))
      )
      .subscribe(
        () =>  {
          this._store.dispatch(DemandeurActions.getCurrentDemandeur());
          this._router.navigate(['/']);
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => {
        this.loading = false;
        this._cd.markForCheck();
      });
  }
}