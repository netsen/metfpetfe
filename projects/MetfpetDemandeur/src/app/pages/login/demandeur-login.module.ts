import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { RECAPTCHA_V3_SITE_KEY, RecaptchaV3Module } from 'ng-recaptcha';
import { environment } from '../../../environments/environment';
import { DemandeurLoginComponent } from './demandeur-login.component';

export const routes = [
  { path: '', component: DemandeurLoginComponent, pathMatch: 'full' },
  { path: 'login', component: DemandeurLoginComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule, 
    ReactiveFormsModule,
    SharedModule,
    RecaptchaV3Module
  ],
  declarations: [
    DemandeurLoginComponent
  ],
  providers: [
    { provide: RECAPTCHA_V3_SITE_KEY, useValue: environment.recaptchaPublicKey },
  ]
})
export class DemandeurLoginModule { }