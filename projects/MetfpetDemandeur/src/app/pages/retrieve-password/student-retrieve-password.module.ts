import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { DemandeurRetrievePasswordComponent } from './demandeur-retrieve-password.component';

export const routes = [
  { path: '', component: DemandeurRetrievePasswordComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule, 
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    DemandeurRetrievePasswordComponent
  ]
})
export class StudentRetrievePasswordModule { }