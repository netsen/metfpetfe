import { ChangeDetectorRef, Component, OnInit, } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  MinistryEmployeeAccessMap,
  MetfpetServiceAgent,
  PerfectScrollService,
  MinistryEmployeeAccessValues,
  BiometricStatusValues,
  DemandeStatus,
  GetDemandeStatusEquivalence,
} from 'MetfpetLib';
import { DemandeDialog } from './demande-dialog.component';
import { selectCurrentDemandeur } from '../../shared/store/selectors';

@Component({
  selector: 'app-demandes',
  templateUrl: './demandes.component.html',
  styleUrls: ['./demandes.component.css']
})
export class DemandesComponent extends BaseTableComponent implements OnInit {

  etablissementDeSoin: string = "Etablissements de soins";
  etablissementPharmaceutique: string = "Etablissements pharmaceutiques et paramédicaux";
  modificationAUnDossier: string = "Modification à un dossier";
  title: string;
  clickedButton: string;
  clickValue: MetfpetServiceAgent.TypeDemandeViewModel;
  settings: Settings;
  accessMap = MinistryEmployeeAccessMap;
  accessList = MinistryEmployeeAccessValues;
  biometricList = BiometricStatusValues;
  testData: any;
  testDataShown: any;
  cardNumber: number;
  iTestData: number;
  limitItem: number;
  screeSize: number;
  hideLeftArrow: boolean;
  hideRightArrow: boolean;
  loading: boolean;
  emptyArray = [];
  agrementationlist: Array<MetfpetServiceAgent.AgrementationViewModel>;
  typeDemandesList: Array<MetfpetServiceAgent.TypeDemandeViewModel>;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  demandeList: Array<MetfpetServiceAgent.DemandeViewModel>
  agrementPrealableList: MetfpetServiceAgent.AgrementPrealableViewModel;
  demandeur: MetfpetServiceAgent.DemandeurProfileViewModel;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _route: ActivatedRoute,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _matDialog: MatDialog,
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Demandes';
    this.loading = true;
    this.sort = { prop: 'name', dir: 'desc' };
  }

  ngOnInit(): void {
    this.screeSize = window.innerWidth;
    this.hideRightArrow = false;
    this.hideLeftArrow = false;
    this.loading = true;
    this.cardNumber = 4;
    this.iTestData = 0;
    this._createSearchForm();
    this._getAllDemandeList();

    this._metfpetService.getTypeDemandeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {isActive:+true}
    })).subscribe((data) => {
      this.typeDemandesList = data.results.sort().reverse();
      this.isActive(this.typeDemandesList[0]);
      this.onClick(this.typeDemandesList[0]);
      this.searchForm.patchValue({
        typeDemandeName: this.typeDemandesList[0].name
      }, { emitEvent: false })
      this.triggerSearch();
    })
  }

  logEvent(event: any, isHover: boolean) {
    let className = 'typeDemandehover';
    let lisclass = event.target.classList.value;
    if (isHover) {
      if (event.target.innerText.length >= 50) {
        if (!event.target.classList.value.includes(className)) {
          event.target.setAttribute("class", lisclass + ' ' + className);
        }
      }
    } else {
      if (lisclass.includes(className)) {
        event.target.setAttribute("class", lisclass.replace(className, ''));
      }
    }
  }

  protected _createSearchForm() {
    this._store.pipe(select(selectCurrentDemandeur)).subscribe((data) => {
      this.demandeur = data;
    })
    this.defaultSearchFormValue = {
      typeDemandeName: '',
      demandeurId: this.demandeur.id
    };
    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);

  }

  protected _search(criteria: any): Observable<any> {
    this._searchAgrementationList(criteria.filters)
    return this._metfpetService.getDemandeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
    return null;
  }

  protected _searchAgrementationList(filter: any) {
    this._metfpetService.getAgrementationListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: filter
    })).subscribe((data) => {
      this.agrementationlist = data.results;
      if (this.agrementationlist.length < this.cardNumber) {
        this.hideRightArrow = true;
        this.hideLeftArrow = true;
      }
      this.loading = false;
      this.iTestData = 0;
      this.setShownData();
      this._cd.markForCheck();
    });
  }

  protected _getAllDemandeList() {
    this._store.pipe(select(selectCurrentDemandeur)).subscribe((data) => {
      if (!data) {
        return;
      }
      var demandeur = data
      this._metfpetService.getDemandeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: { demandeurId: demandeur.id }
      })).subscribe((data) => {
        this.demandeList = data.results
      })
    });
  }

  changes($event) {
    let filter = $event.target.value
    this.searchForm.patchValue({
      typeDemandeName: filter
    }, { emitEvent: false })
    this.triggerSearch()
  }

  onClick(value: MetfpetServiceAgent.TypeDemandeViewModel) {
    this.clickedButton = value.name;
    this.clickValue = value;
  }

  isActive(value: MetfpetServiceAgent.TypeDemandeViewModel) {
    return this.clickValue === value;
  }

  open(id: string) {
    this._router.navigate([`view/${id}`], { relativeTo: this._route });
  }

  public openDocument(demande: MetfpetServiceAgent.DemandeViewModel): void {
    if (demande.certificateLink) {
      window.open(demande.certificateLink, "_blank");
    }
  }

  openAddDemande(agrementation: any) {
    if (agrementation.hasAgrementPrealable) {
      this._metfpetService.getAgrementPrealable(agrementation.id)
        .subscribe((data) => {
          this.agrementPrealableList = data;
          if (this.demandeList !== null && this.demandeur !== null) {
            var approuvedDemande = [];
            this.agrementPrealableList.targetAgrementations.filter((x) => {
              var demandes = this.demandeList.filter((y) => {
                return y.agrementationId == x.id && (y.status == GetDemandeStatusEquivalence(DemandeStatus.Delivre) || y.status == GetDemandeStatusEquivalence(DemandeStatus.Reactive));
              });
              approuvedDemande = approuvedDemande.concat(demandes);
            })
            if (agrementation.isModification) {
              var openModificationDemandes = this.demandeList.filter((x) => {
                return x.isModification && x.status !== GetDemandeStatusEquivalence(DemandeStatus.Delivre) && x.status !== GetDemandeStatusEquivalence(DemandeStatus.Refuse) && x.status !== GetDemandeStatusEquivalence(DemandeStatus.Reactive);
              });

              approuvedDemande = approuvedDemande.filter((x) => {
                return openModificationDemandes.findIndex(k => k.agrementPrealable == x.numero) == -1;
              });
            }
            var dialogRef = this._matDialog.open(DemandeDialog,
              {
                width: "550px",
                data: {
                  agrementation: agrementation,
                  approuvedDemandeList: approuvedDemande,
                  demandeurId: this.demandeur.id,
                  isModification: agrementation.isModification
                }
              }
            ).afterClosed().subscribe((saveSuccess) => {
              if (saveSuccess) {
                this.triggerSearch();
              }
            })
          }
        })
    }
    else {
      var dialogRef = this._matDialog.open(DemandeDialog,
        {
          data: {
            agrementation: agrementation,
            demandeurId: this.demandeur.id
          }
        }
      ).afterClosed().subscribe((saveSuccess) => {
        if (saveSuccess) {
          this.triggerSearch();
        }
      })
    }
  }

  //*************Caroussel algo*******************

  setShownData() {
    if (this.screeSize < 1600) {
      this.cardNumber = 3
    }
    if (this.screeSize < 990) {
      this.cardNumber = 2
    }
    if (this.screeSize < 480) {
      this.cardNumber = 1
    }

    this.testDataShown = this.agrementationlist.slice(this.iTestData * this.cardNumber, (this.iTestData + 1) * this.cardNumber);
    this._cd.markForCheck();
  }

  next() {
    if (((this.iTestData + 1) * this.cardNumber) < this.agrementationlist.length) {
      this.hideLeftArrow = false;
      this.iTestData = this.iTestData + 1;
      this.setShownData();
      this.limitItem = this.limitItem - ((this.iTestData + 1) * this.cardNumber);
      if (this.limitItem <= 0) {
        this.hideRightArrow = true;
        this.limitItem = 0
      }
    } else {
      this.hideRightArrow = true;
    }
  }

  preview() {
    if (this.iTestData != 0) {
      this.hideRightArrow = false;
      this.iTestData = this.iTestData - 1;
      this.setShownData();
      this.limitItem = this.limitItem + ((this.iTestData + 1) * this.cardNumber);
      if (this.limitItem >= this.agrementationlist.length) {
        this.hideLeftArrow = true;
      }
    } else {
      this.hideLeftArrow = true;
    }
  }

  scrollLeft() {
    const container = document.querySelector('#typeDemande');
    container.scrollLeft -= 300;
  }

  scrollRight() {
    const container = document.querySelector('#typeDemande');
    container.scrollLeft += 300;
  }
}
