import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  OnInit
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { 
  AppSettings,
  Settings,
  MetfpetServiceAgent,
  PerfectScrollService,
  DialogService,
  showLoading,
  showSuccess,
  showError,
  hideLoading,
  validateForm,
  showException,
  DemandeStatus,
  SeverityEnum,
  ChampType,
  PersonnelType,
} from 'MetfpetLib';
import { MatDialog } from '@angular/material/dialog';
import { Location } from '@angular/common';
import { UiPath } from '../ui-path';
import { DemandePaymentDialog } from './demande-payment.dialog';
import { DemandePaymentConfirmationCodeDialog } from './demande-payment-confirmation-code.dialog';
import { CommunicationNoteDialogComponent } from './communication-note-dialog.component';
import { DemandePersonnelMemberDialogComponent } from './demande-personnel-member-dialog.component';
import { Observable } from 'rxjs';
import { DemandeProgrammeDialogComponent } from './demande-programme-dialog.component';
import { DemandeCommunicationReadonly } from './demande-communication-readonly.dialog.component';

export const NORMAL_CSS = 'row mb-2';
export const ERROR_CSS = 'row mb-2 warn-color';

@Component({
  templateUrl: './demande-detail.component.html',
  styleUrls: ['./demande-detail.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DemandeDetailComponent implements OnInit {
  
  form: FormGroup;
  demandeInstitutionForm: FormGroup;
  canUserEdit: boolean = false;
  canUserEditDocumentSection: boolean = false;
  canUserEditInstitution: boolean = false;
  settings: Settings;
  title: string;
  demandeProcessStepData: any[];
  firstStepTitle: string;
  enAttente : boolean;
  allStep: number[];
  repeatedStep: number[];
  noRepeatedStep: number[];
  lastStep: any;
  showDeleteButton: boolean = false;
  userCanEdit : boolean = true;
  agrementPrealableNumbers: string [];
  agrementPrealableTitle: string;
  communicationNoteList: Array<MetfpetServiceAgent.CommunicationNoteViewModel>;
  demandeId: string;
  public _model: MetfpetServiceAgent.DemandeDTO;
  demandeInstitutionModel: MetfpetServiceAgent.DemandeInstitutionDTO;
  agrementation: MetfpetServiceAgent.AgrementationDTO;
  tarification: MetfpetServiceAgent.AgrementationTarificationDTO;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  currentProcessStep: MetfpetServiceAgent.DemandeProcessStepDTO;
  attachedDocumentSections: Array<MetfpetServiceAgent.AttachedDocumentSectionListDTO>;
  formulaireSectionChampValeurs: Array<MetfpetServiceAgent.FormulaireSectionChampValeurListDTO>;
  saveFormulaireSectionChampValeurs: Array<MetfpetServiceAgent.FormulaireSectionChampValeurDTO>;
  personnelMembersOfAgrementation: Array<MetfpetServiceAgent.PersonnelMemberDTO>;
  demandePersonnelList: Array<MetfpetServiceAgent.DemandePersonnelMemberDTO>;
  administratifDemandePersonnelMemberList: Array<MetfpetServiceAgent.DemandePersonnelMemberDTO>;
  administratifPersonnelMemberList: Array<MetfpetServiceAgent.PersonnelMemberDTO>;
  formateurDemandePersonnelMemberList: Array<MetfpetServiceAgent.DemandePersonnelMemberDTO>;
  formateurPersonnelMemberList: Array<MetfpetServiceAgent.PersonnelMemberDTO>;
  demandeProgrammeList: Array<MetfpetServiceAgent.DemandeProgrammeDTO>;
  demandeProgrammeListView: Array<MetfpetServiceAgent.DemandeProgrammeViewModel>;
  demandeDisciplineList: Array<MetfpetServiceAgent.DemandeDisciplineDTO>;
  niveauAccesList: Array<MetfpetServiceAgent.NiveauAccesViewModel>;
  typeAdmissionList: Array<MetfpetServiceAgent.TypeAdmissionViewModel>;
  typeProgrammeList: Array<MetfpetServiceAgent.TypeProgrammeViewModel>;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  typeDisciplineList: Array<MetfpetServiceAgent.TypeDisciplineViewModel>;
  typeDiplomeList: Array<MetfpetServiceAgent.TypeDiplomeViewModel>;
  langueList: Array<MetfpetServiceAgent.LangueViewModel>;
  demandeFaculteList: Array<MetfpetServiceAgent.DemandeFaculteViewModel>;
  typeInstitutionList: Array<MetfpetServiceAgent.TypeInstitutionViewModel>;
  prefecturesList: Array<MetfpetServiceAgent.PrefectureViewModel>;
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissementViewModel>;
  requiredAdministratifPosteListString: string;
  requiredPosteListString: string;
  isSelectedAdministratifPersonnelTab = true;
  demandeurRoleId: string;
  loadingIndicator : boolean;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    private _location: Location,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    private _route: ActivatedRoute, 
    protected _store: Store<any>,
    private _dialog: MatDialog,
    private _dialogService: DialogService,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings;
    this.saveFormulaireSectionChampValeurs = [];
    this.title = 'Demandes';
  }

  ngOnInit() {
    this._metfpetService.getPrefectureList().subscribe((data)=>{
      this.prefectureList = data;
      this._cd.markForCheck();
    });

    this.demandeInstitutionForm = this._formBuilder.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'description': [null],
      'typeInstitutionId': [null, Validators.required],
      'prefectureId': [null, Validators.required],
      'statusEtablissementId': [null, Validators.required],
    });

    this._route.params.subscribe(params => {
      this.demandeId = params['id'];
      if (this.demandeId) {
        this._metfpetService.getDemande(this.demandeId).subscribe(data => {
          this._model = data;
          this.loadAgrementation();
          this.loadAgrementPrealableNumber(this._model);
          this.hidenDeleteDemandButton();
          this.loadTarification();
          this.disbleFileInput();
          this.loadDemandeProcessSteps();
          this.loadAttachedDocumentSections();
          this.loadFormulaireSectionChampValeurs();
          this.loadPersonnelMembersOfAgrementation(this._model.agrementationId);
          this.loadAdministratifPersonnelMembers(this._model.agrementationId);
          this.loadFormateurPersonnelMembers(this._model.agrementationId);
          this.loadInstitutionOfDemande();
          this.loadDemandeProgrammeListView();
          this.loadDemandeFacultes();
          this._cd.markForCheck();
        });
        // this.loadDemandePersonnelList();
        this.loadAdministratifDemandePersonnelMembers();
        this.loadFormateurDemandePersonnelMembers();
        this.getDemandeurRoleId();
        this.loadProgrammesOfDemande();
        this.loadDisciplinesOfDemande();
        this.loadNiveauAccess();
        this.loadTypeAdmissions();
        this.loadTypeProgrammes();
        this.loadTypeDisciplines();
        this.loadNiveauEtudes();
        this.loadTypeDiplomes();
        this.loadLangues();
        this.loadTypeInstitutions();
        this.loadPrefectures();
        this.loadStatusEtablissements()
      } 
    });

    
  }

  disbleFileInput(){
    this.canUserEdit = this._model.status === (<any>DemandeStatus.APayer || <any>DemandeStatus.CorrectionsNecessaire);
    this.canUserEditDocumentSection = this._model.status === <any>DemandeStatus.APayer || this._model.status === <any>DemandeStatus.CorrectionsNecessaire;
    this.canUserEditInstitution = this._model.status === <any>DemandeStatus.CorrectionsNecessaire || this._model.status === <any>DemandeStatus.APayer;
    if (!this.canUserEditInstitution) {
      this.demandeInstitutionForm.controls['name'].disable();
      this.demandeInstitutionForm.controls['description'].disable();
    }
  }

  filtrerElementsRepetes(element, index, liste) {
    return index !== liste.indexOf(element) && index === liste.lastIndexOf(element);
  }

  circleByStep(step: number){
    return this.demandeProcessStepData.filter(x => x.step === step).reverse();
  }

  circleWidth(step: number){
    return this.repeatedStep.includes(step) ? '35px' : '40px';
  }

  circleHeight(step: number){
    return this.repeatedStep.includes(step) ? '35px' : '40px';
  }

  circleBgColor(process : any){
    if(this._model.status == <any>DemandeStatus.Delivre || this._model.status == <any>DemandeStatus.Reactive){
      return 'valide';
    }

    if(process.step === this._model.currentProcessStep && process.status === <any>DemandeStatus.AnalyseEnCours){
      return 'enCours';
    }

    if (process.step <= this._model.currentProcessStep && !this.enAttente) {
      switch (process.status) {
        case 1:
          return 'analyseEnCours';
        case 2:
          return 'correctionsNecessaire';
        case 3:
          return 'valide';
        case 4:
          return 'refuse';
        default:
          return 'analyseEnCours';
      }
    }else{
      return 'analyseEnCours';
    }
  }

  stepIcon(process : any){
    if(this._model.status === <any>DemandeStatus.Delivre){
      return 'valide';
    }

    if(process.step === this._model.currentProcessStep && process.status === <any>DemandeStatus.AnalyseEnCours){
      return 'enAttente';
    }

    if  (process.step <=  this._model.currentProcessStep && !this.enAttente) {
      switch (process.status) {
        case 1:
          return '';
        case 2:
          return 'correctionsNecessaire';
        case 3:
          return 'valide';
        case 4:
          return 'refuse';
        default:
          return '';
      }
    }else{
      return '';
    }
  }

  isVerticalCircle(step){
   return this.repeatedStep.includes(step) ;
  }

  loadAgrementation() {
    this._metfpetService.getAgrementation(this._model.agrementationId).subscribe((data) => {
      this.agrementation = data;
    });
  }

  loadDemandeProcessSteps() {
    this._metfpetService.getDemandeProcessSteps(
      MetfpetServiceAgent.DemandeDetailRequest.fromJS(
        Object.assign(
          {
            demandeId: this._model.id,
            agrementationId: this._model.agrementationId
          })
      )
    ).subscribe((data) => {
        this.demandeProcessStepData = data.sort((a, b) => (a.step > b.step) ? 1 : -1);
        this.lastStep = this.demandeProcessStepData.find(x => x.step === Math.max(...this.demandeProcessStepData.map(x => x.step)));
        this.demandeProcessStepData.pop();
        this.allStep = this.demandeProcessStepData.map(x => x.step);
        this.repeatedStep = this.allStep.filter(this.filtrerElementsRepetes);
        this.noRepeatedStep = this.allStep.reduce((acc, step) => {
          if (!acc.includes(step)) {
            acc.push(step);
          }
          return acc.sort();
        }, []);
      })
      if(this._model.status == 1){
        this.enAttente = true;
        this.firstStepTitle = 'En attente';
      }else{
        this.firstStepTitle = 'Dossier soumis';
        this.enAttente = false;
      }
    }

  hidenDeleteDemandButton(){
    this.showDeleteButton = this._model.status === <any>DemandeStatus.APayer;
  }

  loadAgrementPrealableNumber(model :MetfpetServiceAgent.DemandeDTO){
    if(model.agrementPrealable){
      this.agrementPrealableNumbers = model.agrementPrealable.split(",");
      if (model.isModification) {
        this.agrementPrealableTitle = "Agrément à modifier";
      }else{
        this.agrementPrealableTitle = "Agrément préalable";
      }
    }
  }

  loadFormulaireSectionChampValeurs() {
    this._metfpetService.getFormulaireSectionChampValeurs(MetfpetServiceAgent.DemandeDetailRequest.fromJS(
      Object.assign( 
        {
          demandeId: this._model.id,
          agrementationId: this._model.agrementationId
        })
    )).subscribe((data)=>{
      this.formulaireSectionChampValeurs = data;
      this.buildFormulaire();
      this._cd.markForCheck();
    });
  }

  loadPersonnelMembersOfAgrementation(agrementationId: string) {
    this._metfpetService.getPersonnelMembersOfAgrementation(agrementationId).subscribe(data => {
      this.personnelMembersOfAgrementation = data;
      this.requiredPosteListString = this.personnelMembersOfAgrementation.map(x => x.poste).join(", ");
      this._cd.markForCheck();
    });
  }

  buildFormulaire() {
    const group: any = {};
    for (var section of this.formulaireSectionChampValeurs) {
      for (var champ of section.formulaireSectionChampValeurs) {
        this.saveFormulaireSectionChampValeurs.push(champ);
        group[champ.formControlName] = new FormControl(champ.valeur || '');
      }
    }
    this.form = this._formBuilder.group(group);
  }

  getDemandeurRoleId(){
    this.loadingIndicator = true;
    this._metfpetService.getEmployeRoleWithDemandeurListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { 
        status : 1,
        name : 'Demandeur'
       }
    })).subscribe((res)=>{
      this.demandeurRoleId = res?.results[0]?.id
      this.loadCommunicationNotes(this.demandeurRoleId)
    })
    
  }

  loadCommunicationNotes(demandeurRoleId : string) {
    this._metfpetService.getAllCommunicationNoteByDemadeId(this.demandeId).subscribe(data => {
      this.communicationNoteList = data.filter(x => x.employeRoleIds.includes(this.demandeurRoleId) || x.employeRoleIds.length === 0);
      this._cd.markForCheck();
      console.log(data)
      this.loadingIndicator = false;
    });
  }

    openReadMessageDialog(communicationNote: MetfpetServiceAgent.CommunicationNoteViewModel){
      this._dialog.open(DemandeCommunicationReadonly,{
        width: '600px',
        data : {
          messageContent : communicationNote.message, 
          messageSubject : communicationNote.object,
          attachedFileLink : communicationNote.attachedFileLink,
          attachedFileName : communicationNote.attachedFileName
        }
      })
    }

  truncateMessage(message: string, length: number = 25): string {
    if (!message) {
      return '';
    }
    return message.length > length ? message.substring(0, length) + '...' : message;
  }

  loadAttachedDocumentSections() {
    this._metfpetService.getAttachedDocumentSections(MetfpetServiceAgent.DemandeDetailRequest.fromJS(
      Object.assign( 
        {
          demandeId: this._model.id,
          agrementationId: this._model.agrementationId
        })
    )).subscribe((data)=>{
      this.attachedDocumentSections = data.filter(x => x.section?.employeRoleName == 'Demandeur');
      this._cd.markForCheck();
    });
  }

  loadTarification() {
    this._metfpetService.getTarificationForAgrementation(this._model.agrementationId).subscribe((data)=>{
      this.tarification = data;
    });
  }

  loadDemandePersonnelList() {
    this._metfpetService.getPersonnelMembersOfDemande(this.demandeId).subscribe(data => {
      this.demandePersonnelList = data;
      this._cd.markForCheck();
    });
  }

  loadAdministratifPersonnelMembers(agrementationId: string) {
    this._metfpetService.getPersonnelMemberListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {
        'agrementationId': agrementationId,
        'personnelType': PersonnelType.Administratif
      }
    })).subscribe(data => {
      this.administratifPersonnelMemberList = data.results;
      this.requiredAdministratifPosteListString = this.administratifPersonnelMemberList.map(x => x.poste).join(", ");
      this._cd.markForCheck();
    });
  }

  loadAdministratifDemandePersonnelMembers() {
    this._metfpetService.getDemandePersonnelMemberListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {
        'demandeId': this.demandeId,
        'personnelType': PersonnelType.Administratif
      }
    })).subscribe(data => {
      this.administratifDemandePersonnelMemberList = data.results;
      this._cd.markForCheck();
    });
  }

  loadFormateurPersonnelMembers(agrementationId: string) {
    this._metfpetService.getPersonnelMemberListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {
        'agrementationId': agrementationId,
        'personnelType': PersonnelType.Formateur
      }
    })).subscribe(data => {
      this.formateurPersonnelMemberList = data.results;
      this._cd.markForCheck();
    });
  }

  loadFormateurDemandePersonnelMembers() {
    this._metfpetService.getDemandePersonnelMemberListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {
        'demandeId': this.demandeId,
        'personnelType': PersonnelType.Formateur
      }
    })).subscribe(data => {
      this.formateurDemandePersonnelMemberList = data.results;
      this._cd.markForCheck();
    });
  }

  loadProgrammesOfDemande() {
    this._metfpetService.getProgrammesOfDemande(this.demandeId).subscribe(data => {
      this.demandeProgrammeList = data;
      this._cd.markForCheck();
    });
  }
  loadDisciplinesOfDemande() {
    this._metfpetService.getDisciplinesOfDemande(this.demandeId).subscribe(data => {
      this.demandeDisciplineList = data;
      this._cd.markForCheck();
    });
  }

  loadNiveauAccess() {
    this._metfpetService.getNiveauAccesListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauAccesList = data.results;
      this._cd.markForCheck();
    });
  }

  loadTypeAdmissions() {
    this._metfpetService.getTypeAdmissionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.typeAdmissionList = data.results;
      this._cd.markForCheck();
    });
  }

  loadTypeProgrammes() {
    this._metfpetService.getTypeProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.typeProgrammeList = data.results;
      this._cd.markForCheck();
    });
  }

  loadTypeDisciplines() {
    this._metfpetService.getTypeDisciplineListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.typeDisciplineList = data.results;
      this._cd.markForCheck();
    });
  }

  loadNiveauEtudes() {
    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });
  }

  loadTypeDiplomes() {
    this._metfpetService.getTypeDiplomeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.typeDiplomeList = data.results;
      this._cd.markForCheck();
    });
  }

  loadLangues() {
    this._metfpetService.getLangueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.langueList = data.results;
      this._cd.markForCheck();
    });
  }

  loadDemandeFacultes() {
    this._metfpetService.getDemandeFaculteListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {
        demandeId: this.demandeId
      }
    })).subscribe(data => {
      this.demandeFaculteList = data.results;
      this._cd.markForCheck();
    });
  }

  loadDemandeProgrammeListView() {
    this._metfpetService.getProgrammesOfDemandeListPage(this.demandeId).subscribe(data => {
      this.demandeProgrammeListView = data;
      this._cd.markForCheck();
    });
  }

  loadTypeInstitutions() {
    this._metfpetService.getTypeInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.typeInstitutionList = data.results;
      this._cd.markForCheck();
    });
  }

  loadPrefectures() {
    this._metfpetService.getPrefectureListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.prefecturesList = data.results;
      this._cd.markForCheck();
    });
  }

  loadStatusEtablissements() {
    this._metfpetService.getStatusEtablissementListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.statusEtablissementList = data.results;
      this._cd.markForCheck();
    });
  }

  loadInstitutionOfDemande() {
    this._metfpetService.getInstitutionOfDemande(this.demandeId).subscribe(data => {
      if (data) {
        this.demandeInstitutionModel = data;
        this.demandeInstitutionForm.patchValue({
          name: this.demandeInstitutionModel.name,
          description: this.demandeInstitutionModel.description,
          typeInstitutionId: this.demandeInstitutionModel.typeInstitutionId,
          prefectureId: this.demandeInstitutionModel.prefectureId,
          statusEtablissementId: this.demandeInstitutionModel.statusEtablissementId,
        });
        this._cd.markForCheck();
      }
    });
  }

  isShortText(champ: MetfpetServiceAgent.FormulaireSectionChampValeurDTO) {
    return champ.champType === <any> ChampType.ShortText;
  }

  isLongText(champ: MetfpetServiceAgent.FormulaireSectionChampValeurDTO) {
    return champ.champType === <any> ChampType.LongText;
  }

  isCheckBox(champ: MetfpetServiceAgent.FormulaireSectionChampValeurDTO) {
    return champ.champType === <any> ChampType.CheckBox;
  }

  isRadioButton(champ: MetfpetServiceAgent.FormulaireSectionChampValeurDTO) {
    return champ.champType === <any> ChampType.RadioButton;
  }

  isDropdownList(champ: MetfpetServiceAgent.FormulaireSectionChampValeurDTO) {
    return champ.champType === <any> ChampType.DropdownList;
  }

  onSave(isSubmit: boolean, needToPay: boolean) {
    if (this.allowSubmit()) {
      validateForm(this.form);
      validateForm(this.demandeInstitutionForm);
      if (this.form.valid && this.demandeInstitutionForm.valid) {
        this._model = Object.assign({}, this._model, this.form.value);
          if (this.demandeInstitutionForm.valid) { 
            this.demandeInstitutionModel = Object.assign({},
              this.demandeInstitutionModel,
              this.demandeInstitutionForm.value, {
                demandeId: this._model.id
              }
            );
          }
          this._store.dispatch(showLoading());
          for (var item of this.saveFormulaireSectionChampValeurs) {
            item.valeur = this.form.get(item.formControlName).value;
          }
          this._metfpetService.updateDemandeDetail(MetfpetServiceAgent.DemandeSaveInfo.fromJS(Object.assign( 
            {
              demande: this._model,
              demandeInstitution: this.demandeInstitutionModel,
              formulaireSectionChampValeurs: this.saveFormulaireSectionChampValeurs,
              isSubmit: isSubmit
            }))).subscribe(
              () => {
                if (needToPay) {
                  this._store.dispatch(hideLoading());
                  this.payDemande();
                } else {
                  this._store.dispatch(showSuccess({}));
                  this._router.navigate([UiPath.demandeur.demandes.list]);
                }
              },
              error => this._store.dispatch(showException({error: error}))
            )
            .add(() => this._store.dispatch(hideLoading()));   
      }
    }
  }

  allowSubmit(): boolean  {
    return this._model.status === <any>DemandeStatus.APayer || this._model.status === <any>DemandeStatus.CorrectionsNecessaire;
  }

  onSubmit() {
    if (this._model.status === <any>DemandeStatus.APayer) {
      this.onSave(true, this.agrementation.requirePayment);
    } else if (this._model.status === <any>DemandeStatus.CorrectionsNecessaire) {
      this.onSave(true, false);
    }
  }

  private payDemande() {
    this._dialogService.openConfirmDialog({
      width: '700px',
      data: {
        title: 'Confirmation',
        confirmBtnText: 'Payer',
        message: `
            <p>En payant, vous soumettez votre dossier. Après avoir payer, vous ne pourrez plus effectuer de modifications dans votre dossier.</p> 
  
            <p>Vous confirmez par la présente vous confirmez la <b>validité des renseignements</b> soumis.</p>

            <p>Vous confirmez aussi que <b>votre dossier est complet</b>. Tous dossier incomplet sera refusé.</p>

            <p>Vous comprenez que votre dossier sera soumis au Ministère qui se réserve le droit d’accepter ou de refuser votre demande.</p>
          `
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this.processPayment();
      }
    });
  }

  onDelete() {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention", 
        message: "Voulez-vous définitivement supprimer ce demande ?", 
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteDemande(this._model.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({message: 'Le demande a été supprimée'}));
              this._router.navigate([UiPath.demandeur.demandes]);
            },
            (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  goBack() {
    this._location.back();
  }

  private processPayment() {
    this._dialogService.openDialog(
      DemandePaymentDialog,
      {
        width: '600px',
        data: {
          demande: this._model,
          tarification: this.tarification
        }
      }
    ).afterClosed().subscribe(payment => {
      if (payment) {
        this._dialogService.openDialog(
          DemandePaymentConfirmationCodeDialog,
          {
            width: '600px',
            data: {
              tarification: this.tarification,
              payment: payment
            }
          }
        ).afterClosed().subscribe(result => {
          if (result) {
            this._dialogService.openInfoDialog({
              width: '700px',
              data: {
                title: 'Enregistrement de la demande',
                severity: SeverityEnum.INFO,
                closeBtnText: 'Terminer',
                message: `
                        <p>Votre demande à bien été enregistrée.</p>
                        <p>Lorsque celle-ci aura été traitée ou en cas de corrections à effectuer, vous receverez une notification au courriel lié à votre profil : info@etablissement.gn</p>
                        <p>Vous pourriez aussi recevoir une convocation avec un agent du ministère afin de vérifier vos documents. Dans ce cas, vous receverz toutes les informations nécessaires dans une communication liée à votre demande.</p>
                      `
              }
            }).afterClosed().subscribe(() => {
              this._router.navigate([UiPath.demandeur.demandes]);
            });
          }
        });
      }
    });
  }

  public showAttachedDocumentSectionWarning() {
    if (this.attachedDocumentSections && this.attachedDocumentSections.length > 0) {
      for (var item of this.attachedDocumentSections) {
        if (item.attachedDocumentSections) {
          for (var ad of item.attachedDocumentSections) {
            if (!ad.link) {
              return true;
            }
          }
        }
      }
      return false;
    }
    return true;
  }

  public getCss(attachedDocument: MetfpetServiceAgent.AttachedDocumentSectionDTO) {
    return attachedDocument.link ? NORMAL_CSS : ERROR_CSS;
  }

  public getSelectorId(attachedDocument: MetfpetServiceAgent.AttachedDocumentSectionDTO) {
    return 'selector' + attachedDocument.id;
  }
  
  private replaceAttachedDocumentSectionDTO(item: MetfpetServiceAgent.AttachedDocumentSectionListDTO, data: MetfpetServiceAgent.AttachedDocumentSectionDTO) : MetfpetServiceAgent.AttachedDocumentSectionListDTO {
    item.attachedDocumentSections = item.attachedDocumentSections.map(x =>
      x.id == data.id ? MetfpetServiceAgent.AttachedDocumentSectionDTO.fromJS(data) : x);
    return item;
  }

  public onFileUpload(event: any, attachedDocument: MetfpetServiceAgent.AttachedDocumentSectionDTO) {
    if (event.target.files && event.target.files.length > 0) {
      this._store.dispatch(showLoading());

      let file : Blob = event.target.files[0];
      let uploadedFileName : string = event.target.files[0].name;
      var fileReader = new FileReader();
      fileReader.onloadend = function () {
        
      }
      fileReader.readAsBinaryString(file);
      
      let fileParameter = 
      {
        data : file,
        fileName : uploadedFileName
      };
      
      this._metfpetService.uploadAttachedDocumentSection(fileParameter, attachedDocument.id)
        .subscribe(
          (data) => {
            this.attachedDocumentSections = this.attachedDocumentSections.map(x => this.replaceAttachedDocumentSectionDTO(x, data));
            this._cd.markForCheck();
            this._store.dispatch(showSuccess({}));
          },
          (error) => this._store.dispatch(showError({message: error.message}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public uploadDocument(attachedDocument: MetfpetServiceAgent.AttachedDocumentSectionDTO): void {
    document.getElementById('selector' + attachedDocument.id).click();
  }

  public openDocument(attachedDocument: MetfpetServiceAgent.AttachedDocumentSectionDTO): void {
    window.open(attachedDocument.link, "_blank");
  }
  
  public isValidToDownloadModel(attachedDocument: MetfpetServiceAgent.AttachedDocumentSectionDTO){
    return attachedDocument.modelLink && attachedDocument.modelLink.length > 0;
  }

  public openDocumentModel(attachedDocument: MetfpetServiceAgent.AttachedDocumentSectionDTO): void {
    window.open(attachedDocument.modelLink, "_blank");
  }

  createCommunicationNote(id: string) {
    var dialogref = this._dialog.open(CommunicationNoteDialogComponent,
      {
        data: {
          id: id,
          demandeId: this.demandeId,
          demandeurId: this._model.demandeurId,
          demandeurRoleId : this.demandeurRoleId,
        }
      })
      .afterClosed().subscribe((saveSuccess) => {
        if (saveSuccess) {
          this.loadCommunicationNotes(this.demandeurRoleId);
        }
      })
  }

  editPersonnelDetails(row: MetfpetServiceAgent.DemandePersonnelMemberDTO){
    let isAdministratif = row.personnelType == <any>PersonnelType.Administratif;
    var dialogref = this._dialog.open(DemandePersonnelMemberDialogComponent,
      {
        data: {
          id: row.id,
          demandeId: this.demandeId,
          personnelMemberList: this.personnelMembersOfAgrementation,
          programmeList: this.demandeProgrammeList,
          disciplineList: this.demandeDisciplineList,
          isAdministratif: isAdministratif,
        }
      })
      .afterClosed().subscribe((saveSuccess) => {
        if (saveSuccess) {
          this.reloadDemandePersonnelMembers(isAdministratif);
        }
      })
  }

  deletePersonnelMember(row: MetfpetServiceAgent.DemandePersonnelMemberDTO){
    let isAdministratif = row.personnelType == <any>PersonnelType.Administratif;
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention", 
        message: "Voulez-vous supprimer définitivement ce membre ?", 
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteDemandePersonnelMember(row.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({message: 'Le membre a été supprimé'}));
              this.reloadDemandePersonnelMembers(isAdministratif);
            },
            (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  createDemandePersonnelMember(isMandatoryMember: boolean, isAdministratif: boolean) {
    var dialogref = this._dialog.open(DemandePersonnelMemberDialogComponent,
      {
        data: {
          demandeId: this.demandeId,
          personnelMemberList: isAdministratif ? this.administratifPersonnelMemberList : this.formateurPersonnelMemberList,
          programmeList: this.demandeProgrammeList,
          disciplineList: this.demandeDisciplineList,
          isMandatoryMember: isMandatoryMember,
          isAdministratif: isAdministratif,
        }
      })
      .afterClosed().subscribe((saveSuccess) => {
        if (saveSuccess) {
          this.reloadDemandePersonnelMembers(isAdministratif);
        }
      })
  }

  reloadDemandePersonnelMembers(isAdministratif: boolean) {
    if (isAdministratif) {
      this.loadAdministratifDemandePersonnelMembers();
    }
    else {
      this.loadFormateurDemandePersonnelMembers();
    }
  }

  changePersonnelTab(isAdministratif: boolean) {
    this.isSelectedAdministratifPersonnelTab = isAdministratif;
  }

  createDemandeProgramme() {
    var dialogref = this._dialog.open(DemandeProgrammeDialogComponent,
      {
        data: {
          demandeId: this.demandeId,
          niveauAccesList: this.niveauAccesList,
          typeAdmissionList: this.typeAdmissionList,
          typeProgrammeList: this.typeProgrammeList,
          typeDisciplineList: this.typeDisciplineList,
          niveauEtudeList: this.niveauEtudeList,
          typeDiplomeList: this.typeDiplomeList,
          langueList: this.langueList,
          demandeFaculteList: this.demandeFaculteList,
          isCenter: this.agrementation.isCenter,
        }
      })
      .afterClosed().subscribe((saveSuccess) => {
        if (saveSuccess) {
          this.loadDemandeProgrammeListView();
        }
      })
  }

  editDemandeProgramme(row: MetfpetServiceAgent.DemandeProgrammeViewModel) {
    var dialogref = this._dialog.open(DemandeProgrammeDialogComponent,
      {
        data: {
          id: row.id,
          demandeId: this.demandeId,
          niveauAccesList: this.niveauAccesList,
          typeAdmissionList: this.typeAdmissionList,
          typeProgrammeList: this.typeProgrammeList,
          typeDisciplineList: this.typeDisciplineList,
          niveauEtudeList: this.niveauEtudeList,
          typeDiplomeList: this.typeDiplomeList,
          langueList: this.langueList,
          demandeFaculteList: this.demandeFaculteList,
          isCenter: this.agrementation.isCenter,
        }
      })
      .afterClosed().subscribe((saveSuccess) => {
        if (saveSuccess) {
          this.loadDemandeProgrammeListView();
        }
      })
  }

  deleteDemandeProgramme(row: MetfpetServiceAgent.DemandeProgrammeViewModel){
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention", 
        message: "Voulez-vous supprimer définitivement ce membre ?", 
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteDemandeProgramme(row.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({message: 'Le membre a été supprimé'}));
              this.loadDemandeProgrammeListView();
            },
            (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  saveDemandeInstitution() {
    validateForm(this.demandeInstitutionForm);

    if (this.demandeInstitutionForm.valid) {
      this._store.dispatch(showLoading());

      this.demandeInstitutionModel = Object.assign({},
        this.demandeInstitutionModel,
        this.demandeInstitutionForm.value,
      );

      var saveDemandeInstitution$: Observable<MetfpetServiceAgent.DemandeInstitutionDTO>;

      if (this.demandeInstitutionModel.id) {
        saveDemandeInstitution$ = this._metfpetService.updateDemandeInstitution(MetfpetServiceAgent.DemandeInstitutionDTO.fromJS(this.demandeInstitutionModel));
      } else {
        this.demandeInstitutionModel.demandeId = this._model.id;
        saveDemandeInstitution$ = this._metfpetService.createDemandeInstitution(MetfpetServiceAgent.DemandeInstitutionDTO.fromJS(this.demandeInstitutionModel));
      }

      saveDemandeInstitution$.subscribe(
        () => {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([UiPath.demandeur.demandes.list]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }
}
