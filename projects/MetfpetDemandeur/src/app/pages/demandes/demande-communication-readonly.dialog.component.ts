import { 
  Component, 
	Inject, OnInit
} from '@angular/core';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-demande-communication-readonly.dialog',
  templateUrl: './demande-communication-readonly.dialog.component.html',
  styleUrls: ['./demande-communication-readonly.dialog.component.css']
})
export class DemandeCommunicationReadonly implements OnInit {
  messageContent: string;
  messageSubject: string;
  title: string;
  attachedFileLink: string;
  attachedFileName: string;

  constructor(
    private _matDialogRef: MatDialogRef<DemandeCommunicationReadonly>, 
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 
    this.title = "Message"
  }

  ngOnInit(): void {
    this.messageContent = this.data.messageContent;
    this.messageSubject = this.data.messageSubject;
    this.attachedFileLink = this.data.attachedFileLink;
    this.attachedFileName = this.data.attachedFileName;
  }

  onClose() {
    this._matDialogRef.close()
  }

}
