import { 
  ChangeDetectorRef, 
  ChangeDetectionStrategy,
  Component, 
  Inject,
  ViewEncapsulation,
  OnInit,
} from '@angular/core';
import { DatePipe } from '@angular/common';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import {
  DialogService,
  SeverityEnum,
  showSuccess,
  showError,
  showLoading, 
  hideLoading,
  validateForm,
  PaymentServiceAgent,
  PAYMENT_ERROR,
  MetfpetServiceAgent,
} from 'MetfpetLib';

@Component({
  templateUrl: './demande-payment.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DemandePaymentDialog {

  form: FormGroup;
  demande: MetfpetServiceAgent.DemandeDTO;
  tarification: MetfpetServiceAgent.AgrementationTarificationDTO;
  paymentInProgress: boolean;

  constructor(
    public dialogRef: MatDialogRef<DemandePaymentDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _cd: ChangeDetectorRef,
    private _datePipe: DatePipe, 
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _paymentService: PaymentServiceAgent.HttpService
  ) {
    this.demande = data.demande;
    this.tarification = data.tarification;
    this.form = this._formBuilder.group({
      'id': null,
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onPayWithOrange() {
    this.paymentInProgress = true;
    this._paymentService.createAgrementationPayment(PaymentServiceAgent.CreateAgrementationPayment.fromJS(
      {
        demandeId: this.demande.id,
        demandeurId: this.demande.demandeurId,
        agrementationId: this.demande.agrementationId,
      }
    ))
    .subscribe(
      (payment: PaymentServiceAgent.AgrementationPaymentDTO) => {
        if (!payment) {
          this._dialogService.openInfoDialog({
            data: {
              severity: SeverityEnum.ERROR, 
              message: PAYMENT_ERROR
            }
          });
        } else {
          if (payment.details) {
            window.open(payment.details, "_blank");
          }
          this.dialogRef.close(payment);
        }
      },
      error => 
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.ERROR, 
            message: PAYMENT_ERROR + ` (Erreur: ${error})`
          }
        })
    );
  }
}