import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Component, 
  ViewEncapsulation,
  Inject, OnInit
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { 
  validateForm,
  showSuccess,
  MetfpetServiceAgent,
  showLoading,
  showException,
  showError,
  hideLoading,
  emailValidator,
  ProgrammeStatus,
  DisciplineStatus,
} from 'MetfpetLib';
import { Observable } from 'rxjs';

export const NORMAL_CSS = 'row mb-2';
export const ERROR_CSS = 'row mb-2 warn-color';
@Component({
  templateUrl: './demande-programme-dialog.component.html',
  styleUrls: ['./demande-detail.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DemandeProgrammeDialogComponent {

  form: FormGroup;
  title: string; 
  loading: boolean;
  isCreation: boolean;
  isCenter: boolean;
  model: MetfpetServiceAgent.DemandeProgrammeDTO;
  niveauAccesList: Array<MetfpetServiceAgent.NiveauAccesViewModel>;
  typeAdmissionList: Array<MetfpetServiceAgent.TypeAdmissionViewModel>;
  typeProgrammeList: Array<MetfpetServiceAgent.TypeProgrammeViewModel>;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  filteredNiveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  typeDisciplineList: Array<MetfpetServiceAgent.TypeDisciplineViewModel>;
  typeDiplomeList: Array<MetfpetServiceAgent.TypeDiplomeViewModel>;
  langueList: Array<MetfpetServiceAgent.LangueViewModel>;
  demandeFaculteList: Array<MetfpetServiceAgent.DemandeFaculteViewModel>;

  constructor(
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _matDialogRef: MatDialogRef<DemandeProgrammeDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.niveauAccesList = data.niveauAccesList;
    this.typeAdmissionList = data.typeAdmissionList;
    this.typeProgrammeList = data.typeProgrammeList;
    this.niveauEtudeList = data.niveauEtudeList;
    this.typeDisciplineList = data.typeDisciplineList;
    this.typeDiplomeList = data.typeDiplomeList;
    this.langueList = data.langueList;
    this.demandeFaculteList = data.demandeFaculteList;
    this.isCenter = data.isCenter;

    if (this.isCenter)
    {
      this.form = this._formBuilder.group({
        'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
        'demandeFaculteId': [null, Validators.required],
        'typeProgrammeId': [null, Validators.required],
        'typeDiplomeId': [null, Validators.required],
        'langueId': [null, Validators.required],
        'status': true,
      });
    }
    else
    {
      this.form = this._formBuilder.group({
        'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
        'demandeFaculteId': [null, Validators.required],
        'niveauAccesId': [null, Validators.required],
        'typeAdmissionId': [null, Validators.required],
        'duree': [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])],
        'moyenneSeuilMin': [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])],
        'typeProgrammeId': [null, Validators.required],
        'typeDiplomeId': [null, Validators.required],
        'langueId': [null, Validators.required],
        'status': true,
        'demandeDisciplines': this._formBuilder.array([]),
      });
    }
    
    if (data.id) {
      this.isCreation = false;
      this.title = 'Filière de formation';
      this._metfpetService.getDemandeProgramme(data.id).subscribe((data) => {
        this.model = data;
        this.filteredNiveauEtudeList = this.niveauEtudeList.filter(x => x.typeProgrammeId == this.model.typeProgrammeId);
        if (this.isCenter)
        {
          this.form.patchValue({
            name: this.model.name,
            demandeFaculteId: this.model.demandeFaculteId,
            typeProgrammeId: this.model.typeProgrammeId,
            typeDiplomeId: this.model.typeDiplomeId,
            langueId: this.model.langueId,
            status: this.model.status === <any>ProgrammeStatus.Active
          });
        }
        else
        {
          this.form.patchValue({
            name: this.model.name,
            demandeFaculteId: this.model.demandeFaculteId,
            niveauAccesId: this.model.niveauAccesId,
            typeAdmissionId: this.model.typeAdmissionId,
            duree: this.model.duree,
            typeProgrammeId: this.model.typeProgrammeId,
            moyenneSeuilMin: this.model.moyenneSeuilMin,
            typeDiplomeId: this.model.typeDiplomeId,
            langueId: this.model.langueId,
            status: this.model.status === <any>ProgrammeStatus.Active
          });
  
          this.model.demandeDisciplines.forEach((item, index) => {
            this.addDemandeDiscipline();
            this.demandeDisciplines.controls[index].patchValue({
              id: item.id,
              demandeProgrammeId: item.demandeProgrammeId,
              name: item.name,
              niveauEtudeId: item.niveauEtudeId,
              typeDisciplineId: item.typeDisciplineId,
              status: item.status === <any>DisciplineStatus.Active
            })
          });
        }
      });
    } else {
      this.isCreation = true;
      this.title = 'Filière de formation';
      this.model = data;
    }
  }

  get demandeDisciplines() {
    return this.form.controls['demandeDisciplines'] as FormArray;
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (!this.isCenter)
    {
      for (let control of this.demandeDisciplines.controls) {
        validateForm(control as FormGroup);
      } 
    }

    if (this.form.valid) {
      this._store.dispatch(showLoading());

      this.model = Object.assign({},
        this.model,
        this.form.value,
      );

      var saveDemandeProgramme$: Observable<MetfpetServiceAgent.DemandeProgrammeDTO>;

      if (this.isCreation) {
        saveDemandeProgramme$ = this._metfpetService.createDemandeProgramme(MetfpetServiceAgent.DemandeProgrammeDTO.fromJS(this.model));
      } else {
        saveDemandeProgramme$ = this._metfpetService.updateDemandeProgramme(MetfpetServiceAgent.DemandeProgrammeDTO.fromJS(this.model));
      }

      saveDemandeProgramme$.subscribe(
        () => {
            this._store.dispatch(showSuccess({}));
            this._matDialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  addDemandeDiscipline() {
    this.demandeDisciplines.push(this._formBuilder.group({
      'id': null,
      'demandeProgrammeId': null,
      'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'niveauEtudeId': [null, Validators.required],
      'typeDisciplineId': [null, Validators.required],
      'status': true,
    }));
    this._cd.markForCheck();
  }

  delete(i: number) {
    this.demandeDisciplines.removeAt(i);
    this._cd.markForCheck();
  }

  changeTypeProgramme(typeProgrammeIds: any) {
    this.filteredNiveauEtudeList = this.niveauEtudeList.filter(x => typeProgrammeIds.includes(x.typeProgrammeId))
  }

  onClose() {
    this._matDialogRef.close();
  }
}
