import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  DemandeStatus,
  MetfpetServiceAgent,
  hideLoading,
  IAppState,
  showException,
  showLoading,
  showSuccess,
  validateForm,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-nouvelle-demande',
  templateUrl: './demande-dialog.component.html',
  styleUrls: ['./demandes.component.css']
})
export class DemandeDialog {
  form: FormGroup;
  agrementPrealable: string;

  constructor(
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _router: Router,
    private _formBuilder: FormBuilder,
    private _store: Store<IAppState>,
    private _dialogRef: MatDialogRef<DemandeDialog>,
    protected _cd: ChangeDetectorRef,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 
    this.form = this._formBuilder.group({
      demande: [null, Validators.required]
    })
    this._cd.markForCheck()
  }
  
  public onSubmit(): void {
    if(this.data.agrementation.hasAgrementPrealable){
      validateForm(this.form)
      if(this.form.valid){
        if (this.data.agrementation.isModification) {
          this.agrementPrealable = this.form.get('demande').value;
        }
        else
        {
          this.agrementPrealable = this.form.get('demande').value.join(",");
        }
      }else{ return null }
    }
    else{this.agrementPrealable = null}
    this._store.dispatch(showLoading())
    var saveDemande$: Observable<MetfpetServiceAgent.DemandeDTO>
    saveDemande$ = this._metfpetService.createDemande(MetfpetServiceAgent.DemandeDTO.fromJS(
      {
        typeDemandeId     : this.data.agrementation.typeDemandeId,
        agrementationId   : this.data.agrementation.id,
        demandeurId       : this.data.demandeurId,
        isModification    : this.data.isModification,
        status            : DemandeStatus.APayer,
        agrementPrealable : this.agrementPrealable,
      }))
    saveDemande$.subscribe(() => {
        this._store.dispatch(showSuccess({}));
        this._dialogRef.close(true)
      },
      error => this._store.dispatch(showException({ error: error })))
      .add(() => {
        this._store.dispatch(hideLoading())
      });
  }

  onClose() {
    this._dialogRef.close();
  }
}
