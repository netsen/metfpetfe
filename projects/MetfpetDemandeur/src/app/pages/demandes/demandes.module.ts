import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DemandesComponent } from './demandes.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { DemandeDetailComponent } from './demande-detail.component';
import { DemandeDialog } from './demande-dialog.component';
import { DemandePaymentDialog } from './demande-payment.dialog';
import { DemandePaymentConfirmationCodeDialog } from './demande-payment-confirmation-code.dialog';
import { CommunicationNoteDialogComponent } from './communication-note-dialog.component';
import { DemandePersonnelMemberDialogComponent } from './demande-personnel-member-dialog.component';
import { DemandeProgrammeDialogComponent } from './demande-programme-dialog.component';
import { DemandeCommunicationReadonly } from './demande-communication-readonly.dialog.component';

const routes = [
  {
    path : '',
    component: DemandesComponent,
    pathMatch: 'full'
  },
  { 
    path: 'view/:id', 
    component: DemandeDetailComponent, 
    data: { breadcrumb: 'Demandes' }  
  },
]

@NgModule({
  declarations: [
    DemandesComponent,
    DemandeDetailComponent,
    DemandeDialog,
    DemandePaymentDialog,
    DemandePaymentConfirmationCodeDialog,
    CommunicationNoteDialogComponent,
    DemandePersonnelMemberDialogComponent,
    DemandeProgrammeDialogComponent,
    DemandeCommunicationReadonly
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,

  ]
})
export class DemandesModule { }
