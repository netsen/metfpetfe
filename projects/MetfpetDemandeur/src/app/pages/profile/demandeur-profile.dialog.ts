import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Component, 
  ViewEncapsulation
} from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { 
  IAppState, 
  emailValidator,
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import { Store, select } from '@ngrx/store';
import { selectCurrentDemandeur } from '../../shared/store/selectors';
import * as DemandeurActions from '../../shared/store/actions/demandeur.actions';

@Component({
  templateUrl: './demandeur-profile.dialog.html', 
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DemandeurProfileDialog {

  form: FormGroup;
  title: string;
  demandeur: MetfpetServiceAgent.DemandeurProfileViewModel;
  
  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogRef: MatDialogRef<DemandeurProfileDialog>,
    private _formBuilder: FormBuilder, 
    private _store: Store<IAppState>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.form = this._formBuilder.group({
      'email': [null, Validators.compose([Validators.required, emailValidator])],
      'phone': [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]{9}$")])],
    });
  }

  ngOnInit() {
    this._store.pipe(select(selectCurrentDemandeur)).subscribe(data => { 
      this.demandeur = data;
      if (!data) return;
      this.title = this.demandeur.firstName + ' ' + this.demandeur.name;

      this.form.patchValue({
        email: this.demandeur.email,
        phone: this.demandeur.phone,
      });

      this._cd.markForCheck();
    })
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this.demandeur = Object.assign({}, this.demandeur, this.form.value);

      this._metfpetService.updateDemandeurProfile(MetfpetServiceAgent.DemandeurProfileViewModel.fromJS(
        Object.assign({}, this.demandeur, this.form.value)
      )).subscribe(
        () =>  {
          this._store.dispatch(showSuccess({}));
          this._store.dispatch(DemandeurActions.getCurrentDemandeurSuccess({demandeur: this.demandeur}));
          this._dialogRef.close(true);
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public onClose(): void {
    this._dialogRef.close();
  }
}