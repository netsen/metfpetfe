import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  AgrementPagesComponent,
  AgrementPublicLayoutComponent,
  AgrementPublicLayoutLargerFormComponent,
  AuthRoleGuardService,
  PagesComponent,
  PublicLayoutComponent,
  PublicLayoutLargerFormComponent,
} from 'MetfpetLib';
import { SharedModule } from '../shared/shared.module';
import { DemandeurHomeComponent } from './home/demandeur-home.component';
import { DemandeurProfileDialog } from './profile/demandeur-profile.dialog';
import { AttachAccountDocumentDialog } from './account-document/attach-account-document.dialog';

export const routes = [
  {
    path: 'login',
    component: AgrementPublicLayoutComponent,
    loadChildren: () => import('./login/demandeur-login.module').then(m => m.DemandeurLoginModule),
  },
  {
    path: 'retrieve-password',
    component: AgrementPublicLayoutComponent,
    loadChildren: () => import('./retrieve-password/student-retrieve-password.module').then(m => m.StudentRetrievePasswordModule),
  },
  {
    path: 'register',
    component: AgrementPublicLayoutLargerFormComponent,
    loadChildren: () => import('./register/demandeur-register.module').then(m => m.DemandeurRegisterModule),
  },
  {
    path: '',
    component: AgrementPagesComponent,
    canActivate: [AuthRoleGuardService],
    children: [
      {
        path: '',
        component: DemandeurHomeComponent,
      },
      {
        path: 'dossier',
        loadChildren: () => import('./dossier/demandeur-dossier.module').then(m => m.DemandeurDossierModule),
        data: { breadcrumb: 'Dossier d\'demandeur' },
      },
      {
        path: 'demandes',
        loadChildren: () => import('./demandes/demandes.module').then(m => m.DemandesModule),
        data: { breadcrumb: 'Demandes' },
      },

    ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    DemandeurHomeComponent,
    DemandeurProfileDialog,
    AttachAccountDocumentDialog
  ],
  entryComponents: [
    DemandeurProfileDialog,
    AttachAccountDocumentDialog
  ]
})
export class DemandeurModule { }