import { ChangeDetectorRef, Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { AppSettings, DemandeurStatus, DialogService, MetfpetServiceAgent, IAppState, Settings, SeverityEnum } from 'MetfpetLib';
import { selectCurrentDemandeur } from '../../shared/store/selectors';
import { AttachAccountDocumentDialog } from '../account-document/attach-account-document.dialog';
import { UiPath } from '../ui-path';

@Component({
  templateUrl:'./demandeur-home.component.html',
})
export class DemandeurHomeComponent {

  public settings: Settings;
  public title: string;
  demandeur: MetfpetServiceAgent.DemandeurProfileViewModel;
  isChecking: boolean = false;

  constructor(
    public appSettings: AppSettings, 
    private _router: Router,
    private _cd: ChangeDetectorRef,
    private _store: Store<IAppState>,
    private _dialogService: DialogService,
    private _dialog: MatDialog,
    private _metfpetService: MetfpetServiceAgent.HttpService,
  ) {
    this.settings = this.appSettings.settings;
    this.title = 'Bienvenue sur votre espace d\'demandeur';
  }

  ngAfterViewInit() {
    this._store.pipe(select(selectCurrentDemandeur)).subscribe(data => {
      if (!data) return;
      this.demandeur = Object.assign({}, data);
      if (this.demandeur.demandeurStatus !== <any>DemandeurStatus.Approuve && !this.isChecking) {
        this.isChecking = true;
        this._metfpetService.getAttachedDemandeurDocuments(this.demandeur.id).subscribe(data => {
            if (data && !this.showUploadDocument(data)) {
              this._dialog.open(AttachAccountDocumentDialog, {
                width: '800px',
                data: {
                  demandeur: this.demandeur,
                  attachedDemandeurDocumentDtos: data
                }
              }).afterClosed().subscribe(result => {
                if (result) {
                  this.showAuthorisePopup();
                } else {
                  this._router.navigate(['/login']);
                }
              });
            } else {
              this.showAuthorisePopup();
            }
          }
        );
      }
    });
  }
  
  showAuthorisePopup() {
    this._dialogService.openInfoDialog({
      width: '600px',
      data: {
        title: 'Autorisation d’utilisation de la plateforme',
        severity: SeverityEnum.INFO, 
        closeBtnText: 'Je confirme avoir lu et compris',
        message: `
          <p>Afin de pouvoir vous connecter à votre compte, le Ministère de l’Enseignement Technique et de la Formation Professionnelle doit préalablement approuver votre demande. </p> 
          <p>Vous receverez un message une fois votre demande approuvée et pourrez vous connecter à l’aide de vos identifiants. </p>
        `
      }
    })
    .afterClosed().subscribe(() => this._router.navigate(['/login']));
  }

  showUploadDocument(attachedDemandeurDocumentDtos: Array<MetfpetServiceAgent.AttachedDemandeurDocumentDTO>): boolean {
    if (attachedDemandeurDocumentDtos) {
      var filterMandatoryDocuments = attachedDemandeurDocumentDtos.filter(x => x.isMandatory);
      var uploadedDocuments = filterMandatoryDocuments.filter(x => !!x.link).map(x => x.link);
      return new Set(uploadedDocuments).size === filterMandatoryDocuments.length;
    }
    return false;
  }

  goToDemandeurDossier(){
    this._router.navigate([UiPath.demandeur.dossier]);
  }
  goToDemandes(){
    this._router.navigate([UiPath.demandeur.demandes.list]);
  }
}
