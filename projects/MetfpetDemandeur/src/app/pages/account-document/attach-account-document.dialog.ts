import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Inject,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  CommunicationServiceAgent,
  MetfpetServiceAgent,
  hideLoading,
  showError, showLoading, showSuccess, TemplateType,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';

export const NO_FILE_SELECT = 'aucun fichier sélectionné';
export const NORMAL_CSS = 'row mb-2';
export const ERROR_CSS = 'row mb-2 warn-color';

@Component({
  templateUrl: './attach-account-document.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AttachAccountDocumentDialog {

  form: FormGroup;
  demandeur: MetfpetServiceAgent.DemandeurProfileViewModel;
  attachedDemandeurDocumentDtos: Array<any>;

  constructor(
    private _dialogRef: MatDialogRef<AttachAccountDocumentDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _formBuilder: FormBuilder,
    protected _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _communicationService: CommunicationServiceAgent.HttpService,
    protected _cd: ChangeDetectorRef,
  ) {
    this.demandeur = data.demandeur;
    this.attachedDemandeurDocumentDtos = data.attachedDemandeurDocumentDtos;
    this.form = this._formBuilder.group({
      'id': null
    });
  }

  public getCss(attachedDocument: any) {
    return attachedDocument.link || !attachedDocument.isMandatory ? NORMAL_CSS : ERROR_CSS;
  }

  public onClose(): void {
    this._dialogRef.close();
  }

  public onContinue(): void {
    if (this.allowToGoNext()) {
      let sendRequest = new CommunicationServiceAgent.SendManyCommunicationsRequestDTO();
      sendRequest.mode = <any>TemplateType.All;
      sendRequest.userIds = [ this.demandeur.userId ];
      this._store.dispatch(showLoading());
      this._communicationService.sendManyCommunications(sendRequest, "documentationSubmissionAgrementation")
      .subscribe(
        () => {
          this._dialogRef.close(true);
        },
        (error) => this._store.dispatch(showError({ message: error.response }))
      )
      .add(() => {
        this._store.dispatch(hideLoading());
      });
    }
  }

  public allowToGoNext() {
    if (this.attachedDemandeurDocumentDtos) {
      var filterMandatoryDocuments = this.attachedDemandeurDocumentDtos.filter(x => x.isMandatory);
      var uploadedDocuments = filterMandatoryDocuments.filter(x => !!x.link).map(x => x.link);
      return new Set(uploadedDocuments).size === filterMandatoryDocuments.length;
    }
    return false;
  }

  public getSelectorId(attachedDocument: any) {
    return 'selector' + attachedDocument.orderNumber;
  }

  public getRequiredCharacter(attachedDocument: any) {
    return attachedDocument.isMandatory ? '*' : '';
  }

  public onFileUpload(event: any, attachedDocument: any) {
    if (event.target.files && event.target.files.length > 0) {
      this._store.dispatch(showLoading());

      let file : Blob = event.target.files[0];
      let uploadedFileName : string = event.target.files[0].name;
      var fileReader = new FileReader();
      fileReader.onloadend = function () {
        
      }
      fileReader.readAsBinaryString(file);
      
      let fileParameter = 
      {
        data : file,
        fileName : uploadedFileName
      };
      
      this._metfpetService.uploadAttachedDemandeurDocument(fileParameter, attachedDocument.id)
        .subscribe(
          (data) => {
            this.attachedDemandeurDocumentDtos = this.attachedDemandeurDocumentDtos.map(x => 
              x.id === attachedDocument.id ? MetfpetServiceAgent.AttachedDemandeurDocumentDTO.fromJS(data) : x
            );
            this._cd.markForCheck();
            this._store.dispatch(showSuccess({}));
          },
          (error) => this._store.dispatch(showError({message: error.message}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public uploadDocument(attachedDocument: any): void {
    document.getElementById('selector' + attachedDocument.orderNumber).click();
  }
  
  public openDocument(attachedDocument: any): void {
    window.open(attachedDocument.link, "_blank");
  }

  public isValidToDownloadModel(attachedDocument: any){
    return attachedDocument.modelLink && attachedDocument.modelLink.length > 0;
  }

  public openDocumentModel(attachedDocument: any): void {
    window.open(attachedDocument.modelLink, "_blank");
  }
}