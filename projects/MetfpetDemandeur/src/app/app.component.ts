import { Component, Renderer2, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Store } from '@ngrx/store';
import { environment } from '../environments/environment';
import { AppSettings, Settings, AuthService } from 'MetfpetLib';
import { DemandeurActions } from './shared/store/actions/';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
})
export class AppComponent {
  
  title = 'Metfpet Demandeur';
  settings: Settings;
  private _isReadyToShowPage: boolean = !environment.enableQueueIt;

  constructor(
    public appSettings: AppSettings,
    @Inject(DOCUMENT) private _document: Document,
    private _renderer2: Renderer2, 
    private _store: Store<any>,
    private _authService: AuthService
  ) {
    this.settings = this.appSettings.settings;
  }

  public get isReadyToShowPage() {
    return this._isReadyToShowPage;
  }

  ngOnInit() {
    if (environment.enableQueueIt) {
      const self = this;
      window.addEventListener('queuePassed', function(e) {
        self._isReadyToShowPage = true;
      }, false);

      let script = this._renderer2.createElement('script');
      script.type = `text/javascript`;
      script.src = `//static.queue-it.net/script/queueclient.min.js`;
      this._renderer2.appendChild(this._document.body, script);

      script = this._renderer2.createElement('script');
      script.type = `text/javascript`;
      script.src = `//static.queue-it.net/script/queueconfigloader.min.js`;
      script.setAttribute('data-queueit-c', `netsengroup`);
      this._renderer2.appendChild(this._document.body, script);
    }

    if (this._authService.isAuthenticated()) {
      this._store.dispatch(DemandeurActions.getCurrentDemandeur());
    }
  }

}
