export const environment = {
    production: true,
    recaptchaPublicKey: '6LeJdFQjAAAAAG9kJHvySKiXaZ2eeB083pAZF1rB',
    apiEndPoint: 'https://apimetfp.netsengroup.ca:8076',
};
