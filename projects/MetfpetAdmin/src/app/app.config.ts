import { InjectionToken } from "@angular/core";
import { environment } from '../environments/environment';

export interface IAppConfig {
  apiEndPoint: string,
  routes: any,
}

export const APP_DI_CONFIG = {
  apiEndPoint        : environment.apiEndPoint,
  routes: {
  },
}

export let APP_CONFIG = new InjectionToken<IAppConfig>('app.config');
