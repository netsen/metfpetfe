import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import {
  AppSettings,
  Settings, 
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  DocumentStatus,
  AssociatedTypeValues,
  InscriptionProgrammeStatusValues,
  RequirePaymentValues,
  CommandeValues,
  DiffusionValues,
  DocumentTemplateStatusValues,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './admin-document.component.html',
  styleUrls: ['./admin-document.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminDocumentComponent implements OnInit {

  form: FormGroup;
  settings: Settings;
  title: string;
  isCreation: boolean = true;
  private _model: MetfpetServiceAgent.DocumentDTO;
  associatedTypeList: Array<any>;
  associatedTypeStatusList: Array<any>;
  requirePaymentList: Array<any>;
  commandeList: Array<any>;
  diffusionList: Array<any>;
  documentTemplateStatusList: Array<any>;
  htmlTemplate: string = '';
  
  constructor(
    public appSettings: AppSettings,
    private _activatedRoute: ActivatedRoute, 
    private _cdRef: ChangeDetectorRef,
    private _fb: FormBuilder, 
    private _location: Location,
    private _router: Router,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
  ) {

    this.settings = this.appSettings.settings; 
    this.associatedTypeList = AssociatedTypeValues;
    this.associatedTypeStatusList = InscriptionProgrammeStatusValues;
    this.requirePaymentList = RequirePaymentValues;
    this.commandeList = CommandeValues;
    this.diffusionList = DiffusionValues;
    this.documentTemplateStatusList = DocumentTemplateStatusValues;
    this.form = this._fb.group({
      'status': [null],
      'name': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'associatedType': [null, Validators.required],
      'associatedTypeStatus': [null, Validators.required],
      'requirePayment': [null, Validators.required],
      'commande': [null, Validators.required],
      'diffusion': [null, Validators.required],
      'templateStatus': [null, Validators.required],
      'montant' : [null]
    });
  }

  ngOnInit() {
        
    this._activatedRoute.params.subscribe(params => {
      let documentId = params['id'];
      this.isCreation = !documentId;

      if (!this.isCreation) {
        this._metfpetService.getDocument(documentId).subscribe(data => {
          this._model = data;
          this.title = this._model.name;
          this.form.controls['name'].setValue(this._model.name);
          this.form.controls['associatedType'].setValue(this._model.associatedType);
          this.form.controls['associatedTypeStatus'].setValue(this._model.associatedTypeStatus);
          this.form.controls['requirePayment'].setValue(this._model.requirePayment);
          this.form.controls['commande'].setValue(this._model.commande);
          this.form.controls['templateStatus'].setValue(this._model.templateStatus); 
          this.form.controls['diffusion'].setValue(this._model.diffusion); 
          this.form.controls['montant'].setValue(this._model.montant); 
          this.form.controls['status'].setValue(this._model.status === <any>DocumentStatus.Active);
          this.htmlTemplate = data.htmlTemplate;
          this._cdRef.markForCheck();
        });
      }
    });
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveDocument$: Observable<MetfpetServiceAgent.DocumentDTO>;

      if (this.isCreation) {
        saveDocument$= this._metfpetService.createDocument(MetfpetServiceAgent.DocumentDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: DocumentStatus.Active }
          )
        ));
      } else {
        saveDocument$ = this._metfpetService.updateDocument(MetfpetServiceAgent.DocumentDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: this.form.get('status').value ? DocumentStatus.Active : DocumentStatus.Inactive },
          )
        ));
      }

      saveDocument$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([`${UiPath.admin.document.list}`]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  goBack() {
    this._location.back();
  }

}