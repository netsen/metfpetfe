import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewChild,
  ElementRef
} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators/';
import { Store } from '@ngrx/store';
import {
  AppSettings,
  Settings,
  AuthorityEnum,
  StudentStatusValues,
  BaseTableComponent,
  PerfectScrollService,
  AuthService,
  DialogService,
  ExportService,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  ImpressionDocumentStatus,
  ImpressionDocumentPaymentDialog,
  ImpressionDocumentConfirmationCodeDialog,
  ImpressionDocumentPreviewDialog,
  showSuccess,
  showError,
} from 'MetfpetLib';
import { AdminImpressionDocumentsDialog } from './admin-impression-documents.dialog';

enum display {
  form = 1,
  cardLoaded = 2,
  cardGeneration = 3,
  download = 4, 
  removeCard = 5
}

@Component({
  templateUrl: './admin-impression-documents.component.html',
  styleUrls: ['./admin-impression-documents.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminImpressionDocumentsComponent extends BaseTableComponent {

  @ViewChild('searchName', { static: true }) searchName: ElementRef;
  @ViewChild('searchFirstname', { static: true }) searchFirstname: ElementRef;
  @ViewChild('searchMatricule', { static: true }) searchMatricule: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', { static: true }) searchNationalStudentIdentifier: ElementRef;

  settings: Settings;
  title: string;
  selectedRows: any[] = [];
  showMultipleSelect: boolean;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  statusList: Array<any>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  genreList: Array<MetfpetServiceAgent.Genre>;
  documentList: Array<MetfpetServiceAgent.DocumentViewModel>;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _authService: AuthService,
    private _dialogService: DialogService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Documents officiels';
    this.statusList = StudentStatusValues;
    this.sort = { prop: 'identifiantNationalEleve', dir: 'asc' };
    this.showMultipleSelect = false;
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getGenreList().subscribe(data => {
      this.genreList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getDocumentListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { 'isActive': '1' }
    })).subscribe(data => {
      this.documentList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    })

    this.searchForm.get('institution').valueChanges.pipe(
      startWith(this.searchForm.get('institution').value),
      tap(institutionId => {
        this.programList = [];
        if (!this.isFirstSearch) {
          this.searchForm.get('programme').setValue(null);
        }
        if (!institutionId) {
          return;
        }
        this._loadPrograms(institutionId);
      })
    ).subscribe();

    combineLatest([
      fromEvent(this.searchMatricule.nativeElement, 'keyup')
        .pipe(
          debounceTime(800),
          distinctUntilChanged(),
          startWith({
            target: { value: this.searchForm.get('matricule').value }
          })
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement, 'keyup')
        .pipe(
          debounceTime(800),
          distinctUntilChanged(),
          startWith({
            target: { value: this.searchForm.get('identifiantNationalEleve').value }
          })
        ),
      fromEvent(this.searchName.nativeElement, 'keyup')
        .pipe(
          debounceTime(800),
          distinctUntilChanged(),
          startWith({
            target: { value: this.searchForm.get('name').value }
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement, 'keyup')
        .pipe(
          debounceTime(800),
          distinctUntilChanged(),
          startWith({
            target: { value: this.searchForm.get('firstName').value }
          })
        ),
      this.searchForm.get('etudiantStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('etudiantStatus').value)
        ),
      this.searchForm.get('institution').valueChanges
        .pipe(
          startWith(this.searchForm.get('institution').value)
        ),
      this.searchForm.get('programme').valueChanges
        .pipe(
          startWith(this.searchForm.get('programme').value)
        ),
      this.searchForm.get('anneeAcademique').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademique').value)
        ),
      this.searchForm.get('niveauEtude').valueChanges
        .pipe(
          startWith(this.searchForm.get('niveauEtude').value)
        ),
    ])
      .subscribe((
        [
          eventsearchMatricule,
          eventSearchNationalStudentIdentifier,
          eventSearchName,
          eventFirstname,
          etudiantStatus,
          institution,
          programme,
          anneeAcademique,
          niveauEtude
        ]) => {
        this.searchForm.patchValue({
          matricule: eventsearchMatricule ? eventsearchMatricule['target'].value : null,
          identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
          name: eventSearchName ? eventSearchName['target'].value : null,
          firstName: eventFirstname ? eventFirstname['target'].value : null,
          etudiantStatus,
          institution,
          programme,
          anneeAcademique,
          niveauEtude
        }, { emitEvent: false });
        this.triggerSearch();
      });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      etudiantStatus: null,
      institution: null,
      programme: null,
      identifiantNationalEleve: null,
      name: null,
      firstName: null,
      anneeAcademique: null,
      matricule: null,
      documentId: null,
      genreId: '',
      niveauEtude: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    this.selectedRows = [];
    return this._metfpetService.getDocumentInfoListPage(criteria);
  }

  open(row: MetfpetServiceAgent.DocumentInfo) {
    if (row.impressionDocumentStatus === <any>ImpressionDocumentStatus.APayer) {
      this._dialogService.openDialog(
        ImpressionDocumentPaymentDialog,
        {
          width: '680px',
          data: {
            documentInfo: row,
            documentName: row.impressionDocumentName,
            documentMontant: row.montant,
            impressionDocumentId: row.impressionDocumentId
          }
        }
      ).afterClosed().subscribe(paymentResult => {
        if (paymentResult) {
          this._dialogService.openDialog(
            ImpressionDocumentConfirmationCodeDialog,
            {
              width: '680px',
              data: {
                documentInfo: row,
                documentName: row.impressionDocumentName,
                documentMontant: row.montant,
                paymentId: paymentResult.payment.id
              }
            }
          ).afterClosed().subscribe(() => {
            this._dialogService.openDialog(
              ImpressionDocumentPreviewDialog,
              {
                width: '1300px',
                data: {
                  documentInfo: row,
                  impressionDocumentId: row.impressionDocumentId,
                  diffusion: row.diffusion
                }
              }
            ).afterClosed().subscribe(() => {
              this.triggerSearch();
            });
          });
        }
      });
    } else if (row.impressionDocumentStatus === <any>ImpressionDocumentStatus.Payee || row.impressionDocumentStatus === <any>ImpressionDocumentStatus.Delivree) {
      this._dialogService.openDialog(
        ImpressionDocumentPreviewDialog,
        {
          width: '1300px',
          data: {
            documentInfo: row,
            impressionDocumentId: row.impressionDocumentId,
            diffusion: row.diffusion
          }
        }
      ).afterClosed().subscribe(() => {
        this.triggerSearch();
      });
    }
  }

  printDocument(row: MetfpetServiceAgent.DocumentInfo) {
    if (row.isPdfCardExist) {
      window.open(row.link, '_blank');
    } else {
      this._store.dispatch(showLoading());
      this._metfpetService.generateStudentCardPdf([row]).subscribe({
        next: (fileLinks) => {
          this._store.dispatch(hideLoading());
          this._store.dispatch(showSuccess({ message: "Carte générée avec succès." }));
          const cardPdfLink = fileLinks[0];
          this.triggerSearch()
          window.open(cardPdfLink, '_blank');
        },
        error: (error) => {
          this._store.dispatch(hideLoading());
          console.error("Erreur lors de la génération de la carte:", error);
        }
      });
    }
  }

  removeCardByInsitution() {
    this._dialogService.openDialog(
      AdminImpressionDocumentsDialog,
      {
        width: "600px",
        data: {
          anneeAcademiqueList: this.anneeAcademiqueList,
          institutionList : this.institutionList,
          display: display.removeCard
        }
      }
    ).afterClosed().subscribe(()=>{
      this.triggerSearch()
    })
  }
  
  downloadCardByInstitution(){
    this._dialogService.openDialog(
      AdminImpressionDocumentsDialog,
      {
        width: "600px",
        data: {
          anneeAcademiqueList: this.anneeAcademiqueList,
          institutionList : this.institutionList,
          display: display.download
        }
      }
    )
  }

  public isAdminUser() {
    return this._authService.getUserType() == 'administrateurMinisteriel' || this._authService.getUserType() == 'administrateur'; 
  }

  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { institution: institutionId }
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });
  }

  loadRegistrationPeriod() {
    this._dialogService.openDialog(
      AdminImpressionDocumentsDialog,
      {
        width: "600px",
        data: {
          anneeAcademiqueList: this.anneeAcademiqueList,
          institutionList : this.institutionList,
          display: display.form
        }
      }
    )
  }

  isSelected(row: any): boolean {
    return this.selectedRows.includes(row);
  }

  toggleSelection(row: any): void {
    if (this.isSelected(row)) {
      this.selectedRows = this.selectedRows.filter(selectedRow => selectedRow !== row);
    } else {
      this.selectedRows.push(row);
    }
  }

  getSelectedRow(){
    this.showMultipleSelect = !this.showMultipleSelect;
    if(this.selectedRows.length > 0){
      this._dialogService.openDialog(
        AdminImpressionDocumentsDialog,
        {
          width: "600px",
          data: {
            documentList: this.selectedRows,
            showNumberOfCardLoaded: true,
            display : display.cardLoaded
          }
        }
      ).afterClosed().subscribe(()=>{
        this.selectedRows = [];
      })
    }
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }

  removeCard(row: MetfpetServiceAgent.DocumentInfo) {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention", 
        message: "Voulez-vous définitivement supprimer cette carte ?", 
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if(confirmed){
        this._store.dispatch(showLoading());
        this._metfpetService.removePdfCards([row]).subscribe({
          next: () => {
            this._store.dispatch(hideLoading());
            this._store.dispatch(showSuccess({ message: "Cartes supprimées avec succès." }));
            this.triggerSearch();
          },
          error: () => {
            this._store.dispatch(showError({ message: "Erreur lors de la suppression des cartes." }));
          },
        })
      }
    })
  }
}
