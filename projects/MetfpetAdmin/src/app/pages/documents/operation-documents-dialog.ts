import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  showSuccess,
  showLoading,
  hideLoading,
  showError,
  MetfpetServiceAgent,
} from 'MetfpetLib';

@Component({
  selector: 'app-operation-documents-dialog',
  templateUrl: './operation-documents-dialog.html',
  styleUrls: ['./operation-documents-dialog.css']
})
export class OperationDocumentsDialog {

  form: FormGroup;
  currentAnneeAcademique: MetfpetServiceAgent.AnneeAcademiqueDTO;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<OperationDocumentsDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
  ) { 
    this.currentAnneeAcademique = data.currentAnneeAcademique;
    this.form = this._formBuilder.group({
    });
  }

  onConfirm() {
    this._store.dispatch(showLoading());
    this._metfpetService.getInscriptionProgrammeIdsToGenerateCard(this.currentAnneeAcademique.id)
      .subscribe(
        (data) => {
          this._dialogRef.close({
            inscriptionIds: data,
            anneeAcademiqueId: this.currentAnneeAcademique.id
          });
        },
        error => this._store.dispatch(showError({message: error.response}))
      )
      .add(() => this._store.dispatch(hideLoading()));
  }
  onClose() {
    this._dialogRef.close();
  }

}
