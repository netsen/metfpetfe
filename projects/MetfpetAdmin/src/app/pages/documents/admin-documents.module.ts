import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AdminImpressionDocumentsComponent } from './admin-impression-documents.component';
import { AdminDocumentsComponent } from './admin-documents.component';
import { AdminDocumentComponent } from './admin-document.component';
import { AdminImpressionDocumentsDialog } from './admin-impression-documents.dialog';
import { OperationDocumentsComponent } from './operation-documents.component';
import { OperationDocumentsDialog } from './operation-documents-dialog';

export const routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  },
  { 
    path: 'list', 
    component: AdminDocumentsComponent, 
    pathMatch: 'full'
  },
  { 
    path: 'new', 
    component: AdminDocumentComponent, 
    data: { breadcrumb: 'Nouveau' }  
  },
  { 
    path: 'view/:id', 
    component: AdminDocumentComponent, 
    data: { breadcrumb: 'Consultation' }  
  },
  { 
    path: 'impressionDocuments', 
    component: AdminImpressionDocumentsComponent, 
    data: { breadcrumb: 'Impression Documents' }  
  },
  { 
    path: 'operationDocuments', 
    component: OperationDocumentsComponent, 
    data: { breadcrumb: 'Opération Documents' }  
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    AdminDocumentsComponent,
    AdminImpressionDocumentsComponent,
    AdminDocumentComponent,
    AdminImpressionDocumentsDialog,
    OperationDocumentsComponent,
    OperationDocumentsDialog,
  ],
  entryComponents: [
  ]
})
export class AdminDocumentsModule {

}
