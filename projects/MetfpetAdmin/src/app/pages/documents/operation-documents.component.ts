import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  OnInit, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { 
  AppSettings,
  Settings,
  PerfectScrollService,
  showSuccess,
  showLoading,
  hideLoading,
  DialogService,
  MetfpetServiceAgent,
} from 'MetfpetLib';
import { UserIdleService } from 'angular-user-idle';
import { OperationDocumentsDialog } from './operation-documents-dialog';


@Component({
  selector: 'app-operation-documents',
  templateUrl: './operation-documents.component.html',
  styleUrls: ['./operation-documents.component.css']
})
export class OperationDocumentsComponent implements OnInit {

  settings: Settings;
  currentAnneeAcademique: MetfpetServiceAgent.AnneeAcademiqueDTO;
  processing: boolean;
  processed: number;
  totalToprocess: number;
  showWarningMessage: boolean;
  isOtpEnabled: boolean;
  loadingOtpPermission: boolean = true;
  constructor(
    public appSettings: AppSettings,
    protected _cdRef: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialog: MatDialog,
    private _userIdleService: UserIdleService,
    private _dialogService: DialogService,
    private _metfpetService : MetfpetServiceAgent.HttpService
  ) 
  {
    this.settings = this.appSettings.settings;
    this.processing = false;
  }

  ngOnInit(): void {
    this.getAnneeAcademiqueAdmissionOuverte()
  }

  getAnneeAcademiqueAdmissionOuverte() {
    this._metfpetService.getAnneeAcademiqueAdmissionOuverte().subscribe((data : any) => {
      if (data) {
        this.currentAnneeAcademique = data;
        this._cdRef.markForCheck();
      }
    });
  }
  
  async process(inscriptionIds: Array<string>) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this.showWarningMessage = true;
    this.processed = 0;
    this.totalToprocess = inscriptionIds.length;
    for (let inscriptionId of inscriptionIds) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.generateStudentCard(inscriptionId).toPromise();
      } catch(error) {
      }
    }
    this.processing = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
  }

  generateStudentCards() {
    this._dialog.open(OperationDocumentsDialog, {
      width: '600px',
      data: {
        currentAnneeAcademique: this.currentAnneeAcademique
      }
    }).afterClosed().subscribe(data => {
      if (data) {
        this.process(data.inscriptionIds);
      }
    });
  }

}


