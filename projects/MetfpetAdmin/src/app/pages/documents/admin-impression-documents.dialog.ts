import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Diffusion, ImpressionDocumentStatus, MetfpetServiceAgent, hideLoading, showError, showException, showLoading, showSuccess, validateForm } from 'MetfpetLib';
import jsPDF from 'jspdf';
import * as htmlToImage from 'html-to-image';
import { bufferCount, concatMap, delay, finalize, last, mergeMap, tap } from 'rxjs/operators';
import { from, of } from 'rxjs';

enum display {
  form = 1,
  cardLoaded = 2,
  cardGeneration = 3,
  download = 4,
  removeCard = 5
}

@Component({
  selector: 'app-admin-impression-documents.dialog',
  styleUrls: ['./admin-impression-documents.dialog.css'],
  templateUrl: './admin-impression-documents.dialog.html',
})
export class AdminImpressionDocumentsDialog {
  title: string;
  impressionDocumentId: string;
  documentHtmlContent: string;
  diffusion: number;
  indexDocumentOnPrinting: number;
  impressionDocumentDto: MetfpetServiceAgent.ImpressionDocumentDTO;
  form: FormGroup;
  downloadForm: FormGroup;
  numberOfCard: number;
  showNumberOfCardLoaded: boolean;
  showForm: boolean;
  showCardPrinted: boolean;
  showDownloadForm: boolean;
  generationCardStarted: boolean;
  isRemoveCardProcess: boolean = false;
  removeProcessDone: boolean = false;
  removeCards: boolean;
  isDownloadStarted: boolean = false;
  documentList: Array<MetfpetServiceAgent.DocumentInfo>;
  data: MetfpetServiceAgent.DocumentInfo;
  processedBatchCount: number = 0;
  downloadText : string;
  formTitle: string;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  documentTemplateList: Array<MetfpetServiceAgent.DocumentViewModel>;
  documentTemplateName : string = "Carte d’étudiant biométrique";

  constructor(
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _formBuilder: FormBuilder,
    private _cd: ChangeDetectorRef,
    private _matDialogRef: MatDialogRef<AdminImpressionDocumentsDialog>,
    @Inject(MAT_DIALOG_DATA) public datas: any
  ) {
    this.title = "Générer 1ère demande en lot";
    this.showNumberOfCardLoaded = this.datas.showNumberOfCardLoaded;
    this.showCardPrinted = false;
    this.generationCardStarted = false;
    this.form = this._formBuilder.group({
      numberOfCard: [null, [Validators.required, Validators.max(1000), Validators.min(1)]],
      period: [null, Validators.required],
      institution: [null, Validators.required],
      niveauEtude: [null]
    })
    this.downloadForm = this._formBuilder.group({
      institution: [null, Validators.required],
    })
    if (this.datas.showNumberOfCardLoaded) {
      this.documentList = this.datas.documentList;
      this.numberOfCard = this.documentList.length
    }
    this.changeDiplay(this.datas.display);
  }

  ngOnInit(){
    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    })

    this._metfpetService.getDocumentListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.documentTemplateList = data.results;
      this.documentTemplateList;
    })
  }

  onClose() {
    this._matDialogRef.close();
  }

  onSubmit() {
    validateForm(this.form)
    if (this.form.valid) {
      let template = this.documentTemplateList.find( x => x.name === this.documentTemplateName);
      if(!template) return;
      this._store.dispatch(showLoading());
      this._metfpetService.getDocumentInfoListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: 0, pageSize: this.form.get('numberOfCard').value,
        filters: {
          "impressionDocumentStatus": this.isRemoveCardProcess ? ImpressionDocumentStatus.Delivree : ImpressionDocumentStatus.Payee,
          "anneeAcademique": this.form.get('period').value,
          "institution": this.form.get('institution').value,
          "niveauEtude" : this.form.get('niveauEtude').value,
          "documentId" : template.id
        }
      })).subscribe(data => {
        this.changeDiplay(display.cardLoaded);
        this._cd.markForCheck();
        this.documentList = data.results;
        this.numberOfCard = this.documentList.length;
        this._store.dispatch(hideLoading());
      })
    }
  }

  onGenerateOrRemoveAction(){
    if(this.isRemoveCardProcess){
      this.removeCard()
    }else{
      this.generateCard()
    }
  }

  generateCard() {
    if (this.documentList.length > 0) {
      this.changeDiplay(display.cardGeneration);
      this._cd.markForCheck();
      this._store.dispatch(showLoading());
      from(this.documentList)
        .pipe(
          bufferCount(1),
          concatMap((batch) => this._metfpetService.generateStudentCardPdf(batch).pipe(
            tap(() => {
              this.processedBatchCount = this.processedBatchCount + batch.length
            })
          )),
          finalize(() => {
            this._store.dispatch(hideLoading());
            this._store.dispatch(showSuccess({ message: "Cartes générées avec succès." }));
          })
        )
        .subscribe({
          next: () => {},
          error: () => {
            this._store.dispatch(showError({ message: "Erreur lors de la génération des cartes." }));
          },
        });
    }
  }

  removeCard() {
    if (this.documentList.length > 0) {
      this.changeDiplay(display.cardGeneration);
      this._cd.markForCheck();
      this._store.dispatch(showLoading());
      from(this.documentList)
        .pipe(
          bufferCount(1),
          concatMap((batch) => this._metfpetService.removePdfCards(batch).pipe(
            tap((response) => {
              this.processedBatchCount += response.length
            })
          )),
          finalize(() => {
            this._store.dispatch(hideLoading());
            this._store.dispatch(showSuccess({ message: "Cartes supprimées avec succès." }));
            this.removeProcessDone = true;
          })
        )
        .subscribe({
          next: () => {},
          error: () => {
            this._store.dispatch(showError({ message: "Erreur lors de la suppression des cartes." }));
          },
        });
    }
  }


  onDowload() {
    validateForm(this.downloadForm)
    if (this.downloadForm.valid) {
      this._store.dispatch(showLoading());
      let request = new MetfpetServiceAgent.DownloadCardDTO();
      request.institutionId = this.downloadForm.get("institution").value
      request.downloadAllInstitution = false;
      this._metfpetService.downloadPdfCard(MetfpetServiceAgent.DownloadCardDTO.fromJS(request)).subscribe((res) => {
        this._store.dispatch(hideLoading());
        this.isDownloadStarted = true;
        if(res == null){
          this.downloadText = "Cette institution n'a pas de cartes générées."
          this ._store.dispatch(showError({ message: "Echec de téléchargement." }));
          return;
        }
        const fileName = this.datas.institutionList.find(x => x.id === request.institutionId)?.name
        const blob = new Blob([res.data], { type: 'application/zip' });
        const link = document.createElement('a');
        const url = window.URL.createObjectURL(blob);
        link.setAttribute('href', url);
        link.setAttribute('download', fileName);
        link.click();
        this.downloadText = "Le téléchargement à réussi!"
      });
  }
}

  changeDiplay(value: number) {
    switch (value) {
      case display.form:
        this.showForm = true;
        this.showNumberOfCardLoaded = false;
        this.showCardPrinted = false;
        this.showDownloadForm = false;
        this.removeCards = false;
        this.title = "Générer 1ère demande en lot";
        this.formTitle = "Veillez entrer la période pour laquelle vous souhaitez générer les premières carte des apprenants inscrits ou réinscrits."
        break;
      case display.cardLoaded: 
        this.showForm = false;
        this.showNumberOfCardLoaded = true;
        this.showCardPrinted = false;
        this.showDownloadForm = false;
        this.removeCards = false;
        this.title = "Cartes chargées.";
        break;
      case display.cardGeneration:
        this.showForm = false;
        this.showNumberOfCardLoaded = false;
        this.showCardPrinted = true;
        this.showDownloadForm = false;
        this.removeCards = false;
        this.title = this.isRemoveCardProcess ? "Suppression des cartes" : "Genération des cartes.";
        break;
      case display.download: 
        this.showForm = false;
        this.showNumberOfCardLoaded = false;
        this.showCardPrinted = false;
        this.showDownloadForm = true;
        this.removeCards = false;
        this.title = "Télecargement des cartes par institution.";
       break;
      case display.removeCard: 
        this.showForm = false;
        this.showNumberOfCardLoaded = false;
        this.showCardPrinted = false;
        this.showDownloadForm = false;
        this.removeCards = true;
        this.isRemoveCardProcess = true;
        this.title = "Suppression de cartes.";
        this.formTitle = "Veillez rechercher les cartes à supprimer par institution."
       break;
      default:
        this.showForm = true;
        this.showNumberOfCardLoaded = false;
        this.showCardPrinted = false;
        this.showDownloadForm = false;
        this.removeCards = false;
        this.title = "Générer 1ère demande en lot";
        break;
    }
  }

  public print(): void {
    this._store.dispatch(showLoading());
    from(this.documentList).pipe(
      concatMap((data, index) => {
        if (data.impressionDocumentId) {
          return this._metfpetService.getImpressionDocumentHtmlContent(data.impressionDocumentId).pipe(
            mergeMap(content => {
              this.showCardPrinted = true;
              this.generationCardStarted = true
              this.indexDocumentOnPrinting = index + 1;
              this.data = data;
              this.diffusion = data.diffusion;
              this.impressionDocumentId = data.impressionDocumentId;

              var docContent = document.querySelector('#documentContent');
              while (docContent.hasChildNodes()) {
                docContent.removeChild(docContent.firstChild);
              }

              var divInfomation = document.createElement("div");
              divInfomation.style.textAlign = "center";
              divInfomation.innerText = `Telecharment des cartes...${this.indexDocumentOnPrinting}/${this.numberOfCard}`;
              divInfomation.classList.add("mat-dialog-body");
              docContent.appendChild(divInfomation);
              var divDialogBody = document.createElement("div");
              divDialogBody.innerHTML = content;
              divDialogBody.style.scale = "0";
              divDialogBody.style.position = "fixed";
              divDialogBody.style.top = "-1px";
              divDialogBody.style.left = "-1px";

              docContent.appendChild(divDialogBody);
              this._cd.markForCheck();

              var self = this;
              setTimeout(function () {
                self.printDocument(divInfomation);
              }, 5000)
              return of(true).pipe(delay(10000));
            })
          );
        } else {
          return of(false);
        }
      })
    ).subscribe();
  }

  public printDocument(Information: HTMLDivElement): void {
    const savedfileName = this.data.etudiantName + '_document.pdf';
    const doc = new jsPDF({
      orientation: "landscape",
      unit: "mm",
      format: [85, 54]
    });
    var width = doc.internal.pageSize.getWidth();
    var height = doc.internal.pageSize.getHeight();

    htmlToImage.toJpeg(document.getElementById('card-front'), { backgroundColor: '#ffffff' })
      .then(function (dataUrl) {
        doc.addImage(dataUrl, "jpeg", -3, -3, width + 1, height + 1);
      })
      .then(() => {
        htmlToImage.toJpeg(document.getElementById('card-back'), { backgroundColor: '#ffffff' })
          .then(function (dataUrl) {
            doc.addPage();
            doc.addImage(dataUrl, "jpeg", -3.2, -3.8, width + 3, height + 4);
            doc.save(savedfileName);
          }).then(() => { this.updateDeliveryStatus(true) }).then(() => {
            if (this.indexDocumentOnPrinting === this.numberOfCard) {
              Information.innerText = "Terminé!"
              this._store.dispatch(hideLoading());
            }
          });
      });
  }


  private updateDeliveryStatus(isPrinted: boolean): void {
    var request = new MetfpetServiceAgent.UpdateImpressionDocumentDeliveryStatus();
    request.id = this.impressionDocumentId;
    request.isPrinted = isPrinted;
    request.sentMail = !isPrinted;
    this._metfpetService.updateImpressionDocumentDeliveryStatus(request).subscribe(
      (result) => {
        this.impressionDocumentDto = result;
        this._store.dispatch(showSuccess({}));
      },
      (error) => this._store.dispatch(showException({ error: error }))
    ).add(() => {
    });
  }

  public allowPrint(): boolean {
    return this.diffusion === <any>Diffusion.Imprime || this.diffusion === <any>Diffusion.ImprimeEtMail;
  }

}
