import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { 
  AuthService, 
  validateForm,
  showSuccess,
  MetfpetServiceAgent, 
  showLoading,
  showException,
  hideLoading,
  ReasonStatus,
} from 'MetfpetLib';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './add-reason.dialog.html',
})
export class AddReasonDialog {

  form: FormGroup;
  title: string; 
  error: string;
  loading: boolean;
  id: string;
  isCreation: boolean;
  private _model : MetfpetServiceAgent.ReasonDTO;

  constructor(
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _matDialogRef: MatDialogRef<AddReasonDialog>, 
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.form = this._formBuilder.group({
      'name' : [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50)])],
      'status' : [true],
    })

    if (data.id) {
      this.isCreation = false;
      this.id = this.data.id 
      this._metfpetService.getReason(this.id).subscribe((data)=>{
        this._model = data;
        this.form.patchValue({
          name: this._model.name,
          status: this._model.status === <any>ReasonStatus.Active
        })
      });
    } else {
      this.isCreation = true;
    }
   }

  onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading())
      var saveReason$ : Observable<MetfpetServiceAgent.ReasonDTO>
      if (this.isCreation) {
        saveReason$ = this._metfpetService.createReason(MetfpetServiceAgent.ReasonDTO.fromJS(
          Object.assign( 
            this.form.value,
            {status: this.form.get('status').value ? ReasonStatus.Active : ReasonStatus.Inactive})
        ))
      } else {
        saveReason$ = this._metfpetService.updateReason(MetfpetServiceAgent.ReasonDTO.fromJS(
          Object.assign(
            this._model,
            this.form.value,
            {status: this.form.get('status').value? ReasonStatus.Active : ReasonStatus.Inactive})
        ))
      }

      saveReason$.subscribe(
        () =>  {
          this._store.dispatch(showSuccess({}));
          this._matDialogRef.close(true);
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => {
        this._store.dispatch(hideLoading())
      });
    }
  }

  onClose(){
    this._matDialogRef.close()
  }
}
