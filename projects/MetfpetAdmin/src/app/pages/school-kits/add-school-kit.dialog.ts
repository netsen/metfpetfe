import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Component, 
  ViewEncapsulation,
  Inject, OnInit
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { 
  validateForm,
  showSuccess,
  MetfpetServiceAgent,
  showLoading,
  showException,
  showError,
  hideLoading,
  emailValidator,
  SchoolKitStatus,
  SchoolSupplyStatus,
} from 'MetfpetLib';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './add-school-kit.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddSchoolKitDialog {

  form: FormGroup;
  title: string; 
  error: string;
  loading: boolean;
  isCreation: boolean;
  isCenter: boolean;
  model: MetfpetServiceAgent.SchoolKitDTO;
  schoolSupplyList: Array<MetfpetServiceAgent.SchoolSupplyRowViewModel>;

  constructor(
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _matDialogRef: MatDialogRef<AddSchoolKitDialog>, 
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.form = this._formBuilder.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'status': true,
      'schoolSupplies': this._formBuilder.array([]),
    });

    this._metfpetService.getSchoolSupplyListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1,
      filters: {} 
    })).subscribe(data => {
      this.schoolSupplyList = data.results;
      this._cd.markForCheck();
    });
    
    if (data.id) {
      this.isCreation = false;
      this._metfpetService.getSchoolKit(data.id).subscribe((data) => {
        this.model = data;
        this.form.patchValue({
          name: this.model.name,
          status: this.model.status === <any>SchoolKitStatus.Active
        });

        this.model.schoolSupplies.forEach((item, index) => {
          this.add();
          this.schoolSupplies.controls[index].patchValue({
            id: item.id,
            name: item.name,
            status: item.status === <any>SchoolSupplyStatus.Active
          })
        });
      });
    } else {
      this.isCreation = true;
    }
  }

  get schoolSupplies() {
    return this.form.controls['schoolSupplies'] as FormArray;
  }

  public onSubmit(): void {
    this.error = null;
    validateForm(this.form);
    if (this.form.valid && this.validateSchoolSupplies()) {
      this._store.dispatch(showLoading());

      this.model = Object.assign({},
        this.model,
        this.form.value,
        {status: this.form.get('status').value ? SchoolKitStatus.Active : SchoolKitStatus.Inactive}
      );

      var saveSchoolKit$: Observable<MetfpetServiceAgent.SchoolKitDTO>;
      if (this.isCreation) {
        saveSchoolKit$ = this._metfpetService.createSchoolKit(MetfpetServiceAgent.SchoolKitDTO.fromJS(this.model));
      } else {
        saveSchoolKit$ = this._metfpetService.updateSchoolKit(MetfpetServiceAgent.SchoolKitDTO.fromJS(this.model));
      }

      saveSchoolKit$.subscribe(
        () => {
            this._store.dispatch(showSuccess({}));
            this._matDialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  private validateSchoolSupplies() {
    var choices1 = this.schoolSupplies.value.filter(x => !x.id);
    if (choices1.length > 0) {
      this.error = 'Certains champs obligatoires manquent';
    }

    var choices2 = this.schoolSupplies.value.filter(x => !!x.id).map(x => x.id);
    if (new Set(choices2).size !== choices2.length) {
      this.error = 'Les outils sont dupliqués';
    }

    return !this.error;
  }

  add() {
    this.schoolSupplies.push(this._formBuilder.group({
      'id': [null, Validators.required],
      'name': null,
      'status': true,
    }));
    this._cd.markForCheck();
  }

  delete(i: number) {
    this.schoolSupplies.removeAt(i);
    this._cd.markForCheck();
  }

  onClose() {
    this._matDialogRef.close();
  }
}
