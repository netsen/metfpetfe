import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { startWith, debounceTime, distinctUntilChanged } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent, 
  MetfpetServiceAgent,
  PerfectScrollService,
  AuthService,
  DialogService,
} from 'MetfpetLib';
import { Tabs } from './school-kits-tab.component';
import { AddToolDialog } from './add-tool.dialog';

@Component({
  templateUrl: './admin-tools.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminToolsComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;

  settings: Settings;
  title: string;
  navs = Tabs;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _route: ActivatedRoute, 
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialogService: DialogService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.sort = {prop: 'name', dir: 'desc'};
  }

  ngAfterViewInit() {
    combineLatest([
      fromEvent(
        this.searchName.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('name').value}
            })
          )
    ])
    .subscribe(([
      eventName
    ]) => {
      this.searchForm.patchValue({
        name: eventName ? eventName['target'].value : null
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null,
    });
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getSchoolSupplyListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  open(id) {
    this._dialogService.openDialog(
      AddToolDialog, {
        width: '540px',
        data: {
          id: id
        }
      }
    ).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }
}
