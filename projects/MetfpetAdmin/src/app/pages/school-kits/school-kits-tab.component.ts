import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

export const Tabs = [
  { 
    routerLink: '/' + UiPath.admin.schoolKit.kits, 
    label: 'Gestion des kits' 
  },
  {
    routerLink: '/' + UiPath.admin.schoolKit.tools,
    label: 'Gestion des outils'
  },
  {
    routerLink: '/' + UiPath.admin.schoolKit.reasons, 
    label: 'Raison de diminution quantité' 
  },
  {
    routerLink: '/' + UiPath.admin.schoolKit.import, 
    label: 'Importer des outils et des kits scolaires' 
  },
  
];

@Component({
  templateUrl: './school-kits-tab.component.html',
})
export class AdminSchoolKitsTabComponent {

  navs = Tabs;
  title: string;
  routerEventSubscription: Subscription;
  
  constructor(private router: Router) {
    this.title = 'Gestion des kits';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = selectedTab.label;
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
