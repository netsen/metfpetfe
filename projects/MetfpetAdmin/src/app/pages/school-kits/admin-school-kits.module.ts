import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AdminSchoolKitsTabComponent } from './school-kits-tab.component';
import { AdminKitsComponent } from './admin-kits.component';
import { AddSchoolKitDialog } from './add-school-kit.dialog';
import { AdminToolsComponent } from './admin-tools.component';
import { AddToolDialog } from './add-tool.dialog';
import { AdminReasonsComponent } from './admin-reasons.component';
import { AddReasonDialog } from './add-reason.dialog';
import { ImportSchoolSupplyKitDialog } from './import-school-supply-kit.dialog';
import { ImportSchoolSupplyKitComponent } from './import-school-supply-kit.component';

export const routes = [
  { 
    path: '', 
    component: AdminSchoolKitsTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'kits',
          pathMatch: 'full'
        },
        { 
          path: 'kits', 
          component: AdminKitsComponent, 
          data: { breadcrumb: 'Gestion des kits' }
        },
        { 
          path: 'tools', 
          component: AdminToolsComponent, 
          data: { breadcrumb: 'Gestion des outils' }
        },
        { 
          path: 'reasons', 
          component: AdminReasonsComponent, 
          data: { breadcrumb: 'Raison de diminution quantité' }
        },
        { 
          path: 'import', 
          component: ImportSchoolSupplyKitComponent, 
          data: { breadcrumb: 'Importer des outils et des kits scolaires' }
        }
      ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    AdminSchoolKitsTabComponent,
    AdminKitsComponent,
    AddSchoolKitDialog,
    AdminToolsComponent,
    AddToolDialog,
    AdminReasonsComponent,
    AddReasonDialog,
    ImportSchoolSupplyKitComponent,
    ImportSchoolSupplyKitDialog
  ]
})
export class AdminSchoolKitsModule {}
