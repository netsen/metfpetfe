import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { InscriptionPaymentsComponent } from './inscription-payments.component';
import { PaymentExportDialog } from './payment-export.dialog';
import { AdmissionPaymentsComponent } from './admission-payments.component';
import { PaymentsTabComponent } from './payments-tab.component';
import { DocumentPaymentsComponent } from './document-payments.component';
import { AgrementationPaymentsComponent } from './agrementation-payments.component';
import { AgrementationPaymentDialog } from './agrementation-payment.dialog';

export const routes = [
  {
    path: '',
    component: PaymentsTabComponent,
    children:
      [
        {
          path: '',
          redirectTo: 'admission',
          pathMatch: 'full'
        },
        {
          path: 'inscription',
          component: InscriptionPaymentsComponent,
          data: { breadcrumb: 'Paiements Inscription' }
        },
        {
          path: 'admission',
          component: AdmissionPaymentsComponent,
          data: { breadcrumb: 'Paiements Admission' }
        },
        {
          path: 'document',
          component: DocumentPaymentsComponent,
          data: { breadcrumb: 'Paiements Document' }
        },
        {
          path: "agrement",
          component: AgrementationPaymentsComponent,
          data: { breadcrumb: "Paiments Agrément" }
        }
      ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    PaymentsTabComponent,
    InscriptionPaymentsComponent,
    AdmissionPaymentsComponent,
    DocumentPaymentsComponent,
    PaymentExportDialog,
    AgrementationPaymentsComponent,
    AgrementationPaymentDialog
  ],
  entryComponents: [
    PaymentExportDialog,
  ]
})
export class PaymentsModule { }
