import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { PaymentServiceAgent, PaymentStatus } from 'MetfpetLib';

;
import { hideLoading, showException, showLoading, showSuccess } from 'projects/MetfpetLib/src/lib/store/actions';

@Component({
  templateUrl: './agrementation-payment.dialog.html',
})
export class AgrementationPaymentDialog {
  form: FormGroup;
  payment: PaymentServiceAgent.AgrementationPaymentDTO;
  isEditing: boolean;
  updateTransaction: boolean;
  title: string;
  constructor(
    public dialogRef: MatDialogRef<AgrementationPaymentDialog>,
    private _dialog: MatDialog,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _paymentService: PaymentServiceAgent.HttpService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.payment = data.payment;
    this.isEditing = false;
    this.updateTransaction = false;
    this.form = this._formBuilder.group({
      transactionId: null,
    });
    this.form.patchValue({
      'transactionId': this.payment.transactionId,
    });
    this.title = "Paiement Agrémentation";
  }

  onClose(): void {
    if (this.updateTransaction) {
      this.dialogRef.close("UpdateTransaction");
    }
    else {
      this.dialogRef.close();
    }
  }

  onConfirm(): void {
    this.dialogRef.close('TooglePayment');
  }

  isPaiementEffectue() {
    return this.payment && this.payment.status == <any>PaymentStatus.Success;
  }

  isPaiementEchoue() {
    return this.payment && this.payment.status == <any>PaymentStatus.Failed;
  }

  editTransaction(): void {
    this.isEditing = true;
  }

  public get paymentDescription() {
    if (this.payment) {
      return this.payment.details;
    }
    return null;
  }

  saveTransaction(): void {
    this.payment.transactionId = this.form.get('transactionId').value;
    this._store.dispatch(showLoading());
    var updateTransactionPayment = new PaymentServiceAgent.UpdateTransactionAgrementationPayment();
    updateTransactionPayment.transactionId = this.payment.transactionId;
    updateTransactionPayment.paymentId = this.payment.id;
    this._paymentService.updateAgrementationPaymentTransaction(updateTransactionPayment).subscribe(
      () => {
        this.updateTransaction = true;
        this._store.dispatch(showSuccess({}));
      },
      (error) => this._store.dispatch(showException({ error: error }))
    ).add(() => {
      this._store.dispatch(hideLoading());
    });
    this.isEditing = false;
  }



}
