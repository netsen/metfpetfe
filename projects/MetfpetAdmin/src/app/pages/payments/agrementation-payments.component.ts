import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AgrementTabs, DirectionsNationalesTabs, StudentManagementTabs, Tabs } from './payments-tab.component';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  PerfectScrollService,
  PaymentStatusFilterValues,
  PaymentOperatorsFilterValues,
  ExportService,
  DialogService,
  showSuccess,
  showError,
  MetfpetServiceAgent,
  PaymentServiceAgent,
  showLoading,
  hideLoading,
  showException,
  addPostOptions,
  AuthService,
} from 'MetfpetLib';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, combineLatest, fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators';
import { PaymentExportDialog } from './payment-export.dialog';
import { MatDialog } from '@angular/material/dialog';
import { AgrementationPaymentDialog } from './agrementation-payment.dialog';
@Component({
  selector: 'app-agrementation-payments',
  templateUrl: './agrementation-payments.component.html',
  styleUrls: ['./agrementation-payments.component.css']
})
export class AgrementationPaymentsComponent extends BaseTableComponent {

  settings: Settings;
  title: string;
  navs = Tabs;
  statusList: Array<any>;
  operatorList: Array<any>;
  loadingIndicator: boolean = false;


  @ViewChild('searchTransactionId', { static: true }) searchTransactionId: ElementRef;
  @ViewChild('searchName', { static: true }) searchName: ElementRef;
  @ViewChild('searchFirstname', { static: true }) searchFirstname: ElementRef;
  @ViewChild('searchDemandeNumero', { static: true }) searchDemandeNumero: ElementRef;
  @ViewChild('searchDemandeurIdentifier', { static: true }) searchDemandeurIdentifier: ElementRef;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _fb: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _exportService: ExportService,
    private _paymentService: PaymentServiceAgent.HttpService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialog:MatDialog,
    private _authService: AuthService
  ) {
    super(_cd, _fb, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = "Tableau des paiements";
    this.statusList = PaymentStatusFilterValues;
    this.operatorList = PaymentOperatorsFilterValues;
    this.sort = { prop: 'userName', dir: 'asc' };
    if (this._authService.getUserType() == 'administrateurMinisteriel' || this._authService.getUserType() == 'administrateur') 
    {
      this.navs = Tabs;
    }
    else if (this._authService.getUserType() == 'superviseurMinisteriel'
    || this._authService.getUserType() == 'agentMinisteriel') 
    {
      this.navs = StudentManagementTabs;
    }
    else if (this._authService.getUserType() == 'agrementSupervisorMinisteriel'
    || this._authService.getUserType() == 'agrementAgentMinisteriel') 
    {
      this.navs = AgrementTabs;
    }
    else if (this._authService.getUserType() == 'directionsNationales')
    {
      this.navs = DirectionsNationalesTabs;
    }
  }

  ngOnInit() {
    super.ngOnInit();

    combineLatest([
      fromEvent(this.searchTransactionId.nativeElement, 'keyup')
        .pipe(
          debounceTime(800),
          distinctUntilChanged(),
          startWith({
            target: { value: this.searchForm.get('transactionId').value }
          })
        ),
      fromEvent(this.searchName.nativeElement, 'keyup')
        .pipe(
          debounceTime(800),
          distinctUntilChanged(),
          startWith({
            target: { value: this.searchForm.get('name').value }
          })
        ),
      fromEvent(this.searchFirstname.nativeElement, 'keyup')
        .pipe(
          debounceTime(800),
          distinctUntilChanged(),
          startWith({
            target: { value: this.searchForm.get('firstName').value }
          })
        ),
      fromEvent(this.searchDemandeNumero.nativeElement, 'keyup')
        .pipe(
          debounceTime(800),
          distinctUntilChanged(),
          startWith({
            target: { value: this.searchForm.get('demandeNumero').value }
          })
        ),
      fromEvent(this.searchDemandeurIdentifier.nativeElement, 'keyup')
        .pipe(
          debounceTime(800),
          distinctUntilChanged(),
          startWith({
            target: { value: this.searchForm.get('demandeurIdentifier').value }
          })
        ),
    ])
      .subscribe((
        [
          eventsearchTransactionId,
          eventSearchName,
          eventSearchFirstname,
          eventSearchDemandeNumero,
          eventSearchDemandeurIdentifier,
        ]) => {
        this.searchForm.patchValue({
          transactionId: eventsearchTransactionId ? eventsearchTransactionId['target'].value : null,
          name: eventSearchName ? eventSearchName['target'].value : null,
          firstName: eventSearchFirstname ? eventSearchFirstname['target'].value : null,
          demandeNumero: eventSearchDemandeNumero ? eventSearchDemandeNumero['target'].value : null,
          demandeurIdentifier: eventSearchDemandeurIdentifier ? eventSearchDemandeurIdentifier['target'].value : null,
        }, { emitEvent: false });
        this.triggerSearch();
      });
  }


  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      operateur: null,
      phone: null,
      status: null,
      name: null,
      firstName: null,
      demandeurPhone: null,
      demandeurIdentifier: null,
      demandeNumero: null,
      from: null,
      to: null,
      transactionId: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._paymentService.getAgrementationPaymentListPage(criteria)
  }

  public editPayment(row: PaymentServiceAgent.AgrementationPaymentRowViewModel) {
    this._paymentService.getAgrementationPayment(row.id)
      .subscribe(
        (payment: PaymentServiceAgent.AgrementationPaymentDTO) => {
          this.openAgrementationPaymentDetailDialog({ payment: payment, allowModifyPayment: true })
            .afterClosed().pipe(
              tap((confirmed: any) => {
                if (confirmed === 'TooglePayment') {
                  this._paymentService.toogleAgrementationPayment(payment.id).subscribe(
                    () => {
                      this._store.dispatch(showSuccess({}));
                      this.triggerSearch();
                    },
                    (error) => this._store.dispatch(showException({ error: error }))
                  );
                } else if (confirmed === 'UpdateTransaction') {
                  this.triggerSearch();
                }
              })
            ).subscribe();
        },
        (error) => this._store.dispatch(showException({ error: error }))
      );
  }

  public allowAccessPaymentDetail() {
    return this._authService.getUserType() != 'ministre';
  }

  public exportPayments() {
    this._dialogService.openDialog(PaymentExportDialog,
      {
        width: '650px',
        data: {}
      }
    ).afterClosed().subscribe(fields => {
      if (fields) {
        this._store.dispatch(showLoading());

        this._paymentService.getAgrementationPaymentListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
          pageIndex: -1,
          filters: this.searchForm.value,
        }))
          .subscribe(data =>
            this._exportService.exportPayments(data.results, fields, "Paiements_Inscription_"))
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  public isPaiementEffectue(payment: PaymentServiceAgent.PaymentRowViewModel) {
    return payment && payment.statusName == 'SUCCÈS';
  }

  public isPaiementEchoue(payment: PaymentServiceAgent.PaymentRowViewModel) {
    return payment && payment.statusName == 'ÉCHOUÉ';
  }

  public openAgrementationPaymentDetailDialog(data: any) {
    return this._dialog.open(
      AgrementationPaymentDialog,
      {
        width: '700px',
        data: data
      }
    );
  }

}
