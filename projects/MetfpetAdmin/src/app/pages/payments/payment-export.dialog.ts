import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component ({
  templateUrl: './payment-export.dialog.html',
})
export class PaymentExportDialog {

  form: FormGroup;
  isPaymentsInscription: boolean;

  constructor(
    public dialogRef: MatDialogRef<PaymentExportDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _formBuilder: FormBuilder
  ) {
    this.isPaymentsInscription = data.isPaymentsInscription;
    this.form = this._formBuilder.group({
      exportExcel: true,
      exportEtudiantNom: true, 
      exportINA: true,
      exportOperateur: true,
      exportStatut: true,
      exportDateTransaction: true,
      exportMontant: true, 
      exportNumeroTransaction: true,
      exportConfirmationCode: true,
      exportStatusEtablissement: this.isPaymentsInscription,
      exportInstitution: this.isPaymentsInscription
    });
  }

  onConfirm(): void {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    }
  }

  onClose(): void {
    this.dialogRef.close(null);
  }

}