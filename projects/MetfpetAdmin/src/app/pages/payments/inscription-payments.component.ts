import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators/';
import { Store } from '@ngrx/store';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  PerfectScrollService,
  PaymentStatusFilterValues,
  PaymentOperatorsFilterValues,
  ExportService,
  DialogService,
  showSuccess,
  showError,
  MetfpetServiceAgent,
  PaymentServiceAgent,
  showLoading,
  hideLoading,
  showException,
  addPostOptions,
  AuthService,
} from 'MetfpetLib';
import { PaymentExportDialog } from './payment-export.dialog';
import { AgrementTabs, DirectionsNationalesTabs, StudentManagementTabs, Tabs } from './payments-tab.component';

@Component({
  templateUrl: './inscription-payments.component.html',
  styleUrls: ['./inscription-payments.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InscriptionPaymentsComponent extends BaseTableComponent {

  @ViewChild('searchTransactionId', {static: true}) searchTransactionId: ElementRef;
  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchPVNumber', {static: true}) searchPVNumber: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;

  settings: Settings;
  title: string;
  navs = Tabs;
  optionsList: Array<MetfpetServiceAgent.OptionRowViewModel>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissement>;
  statusList: Array<any>;
  operatorList: Array<any>;
  loadingIndicator: boolean = false;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _fb: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _paymentService: PaymentServiceAgent.HttpService,
    private _authService: AuthService
  ) {
    super(_cd, _fb, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = "Tableau des paiements";
    this.statusList = PaymentStatusFilterValues;
    this.operatorList = PaymentOperatorsFilterValues;
    this.sort = {prop: 'etudiantName', dir: 'asc'};
    if (this._authService.getUserType() == 'administrateurMinisteriel' || this._authService.getUserType() == 'administrateur') 
    {
      this.navs = Tabs;
    }
    else if (this._authService.getUserType() == 'superviseurMinisteriel'
    || this._authService.getUserType() == 'agentMinisteriel') 
    {
      this.navs = StudentManagementTabs;
    }
    else if (this._authService.getUserType() == 'agrementSupervisorMinisteriel'
    || this._authService.getUserType() == 'agrementAgentMinisteriel') 
    {
      this.navs = AgrementTabs;
    }
    else if (this._authService.getUserType() == 'directionsNationales')
    {
      this.navs = DirectionsNationalesTabs;
    }
  }

  public allowAccessPaymentDetail() {
    return this._authService.getUserType() != 'directionsNationales' && this._authService.getUserType() != 'ministre';
  }
  
  ngOnInit() {
    super.ngOnInit();
    this._metfpetService.getStatusEtablissementList().subscribe(data => {
      this.statusEtablissementList = data; 
      this._cd.markForCheck();
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getOptionList().subscribe(data => {
      this.optionsList = addPostOptions(data);
      this._cd.markForCheck();
    });

    this.searchForm.get('institutionId').valueChanges.pipe(
      startWith(this.searchForm.get('institutionId').value),
      tap(institutionId => {
        this.programList = [];
        if (!this.isFirstSearch) {
          this.searchForm.get('programmeId').setValue(null);
        }
        if (!institutionId) {
          return;
        }
        this._loadPrograms(institutionId);
      })
    ).subscribe();

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    combineLatest([
      fromEvent(this.searchTransactionId.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('transactionId').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(this.searchFirstname.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('firstName').value}
          })
        ),
      fromEvent(this.searchPVNumber.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('numeroPV').value}
          })
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      this.searchForm.get('optionId').valueChanges
        .pipe(
          startWith(this.searchForm.get('optionId').value)
        ),
      this.searchForm.get('statusEtablissementId').valueChanges
        .pipe(
          startWith(this.searchForm.get('statusEtablissementId').value)
        )
    ])
    .subscribe((
      [
        eventsearchTransactionId,
        eventSearchName, 
        eventSearchFirstname,
        eventSearchPV,
        eventSearchNationalStudentIdentifier,
        optionId,
        statusEtablissementId,
      ]) => {
      this.searchForm.patchValue({
        transactionId: eventsearchTransactionId ? eventsearchTransactionId['target'].value : null,
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventSearchFirstname ? eventSearchFirstname['target'].value : null, 
        numeroPV: eventSearchPV ? eventSearchPV['target'].value : null,
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        optionId,
        statusEtablissementId,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      operateur: null,
      phone: null,
      status: null,
      name: null,
      firstName: null,
      etudiantPhone: null,
      identifiantNationalEleve: null,
      numeroPV: null,
      optionId: null,
      statusEtablissementId: null,
      from: null,
      to: null,
      anneeAcademiqueId: null,
      institutionId: null,
      programmeId: null,
      matricule: null,
      transactionId: null,
      paymentType: 'Inscription',
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._paymentService.getPaymentListPage(criteria);
  }

  public editPayment(row: PaymentServiceAgent.PaymentRowViewModel) {
    this._paymentService.getPayment(row.id)
      .subscribe(
        (payment: PaymentServiceAgent.PaymentDTO) => {
          this._dialogService.openPaymentDetailDialog({payment: payment, allowModifyPayment: true})
            .afterClosed().pipe(
              tap((confirmed: any) => {
                if (confirmed === 'TooglePayment') {
                  this._paymentService.tooglePayment(payment.id).subscribe(
                    () => {
                      this._store.dispatch(showSuccess({}));
                      this.triggerSearch();
                    },
                    (error) => this._store.dispatch(showException({error: error}))
                  );
                } else if (confirmed === 'UpdateTransaction') {
                  this.triggerSearch();
                }
              })
            ).subscribe();
        }, 
        (error) => this._store.dispatch(showException({error: error}))
      );
  }

  public exportPayments() {
    this._dialogService.openDialog(PaymentExportDialog, 
      {
        width: '650px',
        data: {
          isPaymentsInscription: this.isPaymentsInscription(),
          typePayment: this.searchForm.get('paymentType').value,
        }
      }
    ).afterClosed().subscribe(fields => {
      if (fields) {
        this._store.dispatch(showLoading());
        
        this._paymentService.getPaymentListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
          pageIndex: -1,
          filters: this.searchForm.value,
        }))
          .subscribe(data => 
            this._exportService.exportPayments(data.results, fields, "Paiements_Inscription_"))
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  public isPaiementEffectue(payment: PaymentServiceAgent.PaymentRowViewModel) {
    return payment && payment.statusName == 'SUCCÈS';
  }

  public isPaiementEchoue(payment: PaymentServiceAgent.PaymentRowViewModel) {
    return payment && payment.statusName == 'ÉCHOUÉ';
  }

  loadPaymentsInscription() {
      this.searchForm.patchValue({
        paymentType: 'Inscription',
      }, {emitEvent: false});
      this.triggerSearch();
  }

  isPaymentsInscription() {
    return this.searchForm.get('paymentType').value == 'Inscription';
  }

  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {institution: institutionId}
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });
  }
}