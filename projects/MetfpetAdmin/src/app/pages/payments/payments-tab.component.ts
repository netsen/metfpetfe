import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';
import { AuthService } from 'MetfpetLib';

export const Tabs = [
  {
    routerLink: '/' + UiPath.admin.payments.admission.list,
    label: 'Admission'
  },
  {
    routerLink: '/' + UiPath.admin.payments.inscription.list,
    label: 'Inscription'
  },
  {
    routerLink: '/' + UiPath.admin.payments.document.list,
    label: 'Document'
  },
  {
    routerLink: `/${UiPath.admin.payments.agrement.list}`,
    label: "Agrément"
  }
];

export const AgrementTabs = [
  {
    routerLink: `/${UiPath.admin.payments.agrement.list}`,
    label: "Agrément"
  }
];

export const StudentManagementTabs = [
  {
    routerLink: '/' + UiPath.admin.payments.admission.list,
    label: 'Admission'
  },
  {
    routerLink: '/' + UiPath.admin.payments.inscription.list,
    label: 'Inscription'
  },
  {
    routerLink: '/' + UiPath.admin.payments.document.list,
    label: 'Document'
  }
];

export const DirectionsNationalesTabs = [
  {
    routerLink: '/' + UiPath.admin.payments.admission.list,
    label: 'Admission'
  },
  {
    routerLink: '/' + UiPath.admin.payments.inscription.list,
    label: 'Inscription'
  }
];

@Component({
  templateUrl: './payments-tab.component.html',
})
export class PaymentsTabComponent {

  navs = Tabs;
  title: string;
  routerEventSubscription: Subscription;

  constructor(private router: Router,
    private _authService: AuthService) {
    this.title = 'Tableau de paiements des admissions';
    if (this._authService.getUserType() == 'administrateurMinisteriel' || this._authService.getUserType() == 'administrateur') 
    {
      this.navs = Tabs;
    }
    else if (this._authService.getUserType() == 'superviseurMinisteriel'
    || this._authService.getUserType() == 'agentMinisteriel') 
    {
      this.navs = StudentManagementTabs;
    }
    else if (this._authService.getUserType() == 'agrementSupervisorMinisteriel'
    || this._authService.getUserType() == 'agrementAgentMinisteriel') 
    {
      this.navs = AgrementTabs;
    }
    else if (this._authService.getUserType() == 'directionsNationales')
    {
      this.navs = DirectionsNationalesTabs;
    }
  }

  ngOnInit(): void {
    this.routerEventSubscription = this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
        this.title = 'Tableau de paiements des ' + selectedTab.label.toLowerCase() + 's';
      }
    });
  }

  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
