import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ImportDiplomeDialog } from './import-diplome.dialog';
import { ImportDiplomeTabComponent } from './import-diplome-tab.component';
import { ImportDiplomeComponent } from './import-diplome.component';

export const routes = [
  {
    path: '', 
    component: ImportDiplomeTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'diplome',
          pathMatch: 'full'
        },
        { 
          path: 'diplome', 
          component: ImportDiplomeComponent, 
          data: { breadcrumb: 'Importer les diplôme' }
        },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [
    SharedModule,
    ImportDiplomeComponent,
  ],
  declarations: [
    ImportDiplomeTabComponent,
    ImportDiplomeComponent,
    ImportDiplomeDialog,
  ], 
  entryComponents: [
    ImportDiplomeDialog
  ]
})
export class ImportDiplomeModule {

}
