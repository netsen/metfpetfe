import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Input,
  ViewEncapsulation 
} from '@angular/core';
import { UserIdleService } from 'angular-user-idle'; 
import { 
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  Settings,
  AppSettings,
  ExportService,
} from 'MetfpetLib';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { Router } from '@angular/router';
import { ImportDiplomeDialog } from './import-diplome.dialog';
import { ImportDiplomeTabComponent } from './import-diplome-tab.component';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

export interface ImportLesNotesResult {
  nbImported: number;
  failedAdmissions: Array<any>;
}

@Component({
  templateUrl: './import-diplome.component.html',
  styleUrls: ['./import-diplome.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportDiplomeComponent extends ImportDiplomeTabComponent {
  
  imported: number;
  totalToimport: number;
  importingIndicator: boolean;
  showWarningMessage: boolean;
  public settings: Settings;

  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    public appSettings: AppSettings,
    protected router: Router,
    private _exportService: ExportService
  ) {
    super(router);
    this.settings = this.appSettings.settings;
  }

  onClickImportButton() {
    this._dialogService.openDialog(
      ImportDiplomeDialog,
      {
        width: '600px',
        data: {
          isImport: true,
          title: "Importer les diplôme", 
          message: "Veuillez indiquer l'emplacement du fichier", 
          confirmBtnText: "Importer",
        }
      }
    ).afterClosed().subscribe(result => {
      if (result && result.file) {
        this.processFile(result.file);
      }
    });
  }

  processFile(file: Blob) {
    this.importingIndicator = true;
    this._userIdleService.stopWatching();

    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      var arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, {type:'binary',cellDates:true,dateNF:'dd/mm/yyyy'});
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      var rows = XLSX.utils.sheet_to_json(worksheet, {raw:false,dateNF:'dd/mm/yyyy'});
      
      this._importDiplome(rows);
    }
    fileReader.readAsArrayBuffer(file);
  }

  private _importDiplome(rows: any): void {
      if (rows.length == 0) {
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.INFO, 
            message: 'Aucune donnée importée'
          }
        });
        return;
      }

      this.importDiplome(this._convertToDiplome(rows), true);
  }

  private _convertToDiplome(rows: any): Array<MetfpetServiceAgent.ImportDiplomeViewModel> {
    
    let redoublants =  new Array<MetfpetServiceAgent.ImportDiplomeViewModel>();

    redoublants = rows.map(
      row => {
        return MetfpetServiceAgent.ImportDiplomeViewModel.fromJS({
          'identifiant'                : row['INA'],
          'periode'                    : row['Période']
        });
      }
    );
    return redoublants;
  }
  
  async importDiplome(diplomeToImport: Array<MetfpetServiceAgent.ImportDiplomeViewModel>, allowRetry: boolean) {
    this.showWarningMessage = true;
    this.imported = 0;
    this.totalToimport = diplomeToImport.length;
    let toRetryList = new Array<MetfpetServiceAgent.ImportDiplomeViewModel>();

    for (let diplome of diplomeToImport) {    
      try {
        this.imported++;
        this._cd.markForCheck();
        let importSuccess = await this._metfpetService.importDiplome(diplome).toPromise();
        if (!importSuccess) {
          toRetryList.push(diplome);
        }
      } catch(error) {
        diplome.importErrorMessage = error.isApiException ? (JSON.parse(error.response).Message ? JSON.parse(error.response).Message : error.response) : error;
        toRetryList.push(diplome);
      }
    }
    if (toRetryList.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: (this.imported - toRetryList.length) + " diplome(e)(s) importé(e)(s) avec succès, " 
            + toRetryList.length + " en échec"
        }
      });

    } else {
      this._dialogService.openConfirmDialog({
        data: {
          title: "Attention", 
          message: (this.totalToimport - toRetryList.length) + " diplome(e)s importé(e)s avec succès, " 
            + toRetryList.length + " en échec, reprendre les éléments en échec?"
        }
      })
      .afterClosed().subscribe(confirmed => {
        if (confirmed) {
          this.importDiplome(toRetryList, false);
        } else {
          this.exportFailed(toRetryList);
        }
      });
    }

    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    return {
      nbImported: this.imported,
      failedUsers: toRetryList
    };
  }

  exportFailed(toRetryList: Array<MetfpetServiceAgent.ImportDiplomeViewModel>): void {
    if (toRetryList.length > 0) {
      let json : any[] = new Array();
      for (let failed of toRetryList) {
        json.push({
          'INA'                : failed.identifiant,
          'Période'            : failed.periode,
          'Raison'             : failed.importErrorMessage
        });
      }
      this.exportAsExcelFile(json, 'failed_results');
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'Échec': worksheet }, SheetNames: ['Échec'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_import_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

  downloadTemplate() {
    const headers = [
      { name: "INA", value: "identifiantNationalEleve" },
      { name: "Période", value: "periode" },
    ]
    const templateName = "DiplomeTemplate";
    this._exportService.downloadTemplate(templateName, headers);
  }
}
