import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ImportCandidatsLibresDialog } from './import-candidats-libres.dialog';
import { ImportCandidatsLibresTabComponent } from './import-candidats-libres-tab.component';
import { ImportCandidatsLibresComponent } from './import-candidats-libres.component';

export const routes = [
  {
    path: '', 
    component: ImportCandidatsLibresTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'candidats-libres',
          pathMatch: 'full'
        },
        { 
          path: 'candidats-libres', 
          component: ImportCandidatsLibresComponent, 
          data: { breadcrumb: 'Importer des candidats libres' }
        },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [
    SharedModule,
    ImportCandidatsLibresComponent,
  ],
  declarations: [
    ImportCandidatsLibresTabComponent,
    ImportCandidatsLibresComponent,
    ImportCandidatsLibresDialog,
  ], 
  entryComponents: [
    ImportCandidatsLibresDialog
  ]
})
export class ImportCandidatsLibresModule {

}
