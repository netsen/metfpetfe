import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Input,
  ViewEncapsulation 
} from '@angular/core';
import { UserIdleService } from 'angular-user-idle'; 
import { 
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  Settings,
  AppSettings,
  ExportService,
} from 'MetfpetLib';
import {formatDate} from '@angular/common';
import * as TSXLSX from 'ts-xlsx';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { ImportCandidatsLibresTabComponent } from './import-candidats-libres-tab.component';
import { ImportCandidatsLibresDialog } from './import-candidats-libres.dialog';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

export interface CandidatsLibresImportResult {
  nbImported: number;
  failedAdmissions: Array<any>;
}

@Component({
  templateUrl: './import-candidats-libres.component.html',
  styleUrls: ['./import-candidats-libres.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportCandidatsLibresComponent extends ImportCandidatsLibresTabComponent {

  imported: number;
  totalToimport: number;
  importingIndicator: boolean;
  showWarningMessage: boolean;
  successImportEntries: Array<MetfpetServiceAgent.ImportCandidatLibreViewModel>;
  public settings: Settings;
  
  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    public appSettings: AppSettings,
    protected router: Router,
    private _exportService: ExportService
  ) {
    super(router);
    this.settings = this.appSettings.settings;
  }

  onClickImportButton() {
    this._dialogService.openDialog(
      ImportCandidatsLibresDialog,
      {
        width: '600px',
        data: {
          isImport: true,
          title: "Importer des candidats libres", 
          message: "Veuillez indiquer l'emplacement du fichier", 
          confirmBtnText: "Importer",
        }
      }
    ).afterClosed().subscribe(result => {
      if (result && result.file) {
        this.processFile(result.file);
      }
    });
  }

  processFile(file: Blob) {
    this.importingIndicator = true;
    this._userIdleService.stopWatching();

    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      var arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, {type:'binary',cellDates:true,dateNF:'dd/mm/yyyy'});
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      var rows = XLSX.utils.sheet_to_json(worksheet, {raw:false,dateNF:'dd/mm/yyyy'});
      
      this._importCandidatsLibres(rows);
    }
    fileReader.readAsArrayBuffer(file);
  }

  private _importCandidatsLibres(rows: any): void {
    if (rows.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: 'Aucune donnée importée'
        }
      });
      return;
    }
    this.successImportEntries = new Array<MetfpetServiceAgent.ImportCandidatLibreViewModel>();
    this.importCandidatsLibres(this._convertToCandidatsLibres(rows), true);
  }

  private _convertToCandidatsLibres(rows: any): Array<MetfpetServiceAgent.ImportCandidatLibreViewModel> {
    
    let candidatsLibres =  new Array<MetfpetServiceAgent.ImportCandidatLibreViewModel>();

    candidatsLibres = rows.map(
      row => {  
        return MetfpetServiceAgent.ImportCandidatLibreViewModel.fromJS({
          'identifiantNationalEleve'       : row['INA'], 
          'name'                           : row['Nom'], 
          'firstName'                      : row['Prénom'], 
          'institution'                    : row['Nom de l\'institution'], 
          'programme'                      : row['Programme']
        });
      }
    );
    return candidatsLibres;
  }
  
  async importCandidatsLibres(candidatsLibresToImport: Array<MetfpetServiceAgent.ImportCandidatLibreViewModel>, allowRetry: boolean) {
    this.showWarningMessage = true;
    this.imported = 0;
    this.totalToimport = candidatsLibresToImport.length;
    let toRetryList = new Array<MetfpetServiceAgent.ImportCandidatLibreViewModel>();

    for (let candidatsLibres of candidatsLibresToImport) {    
      try {
        this.imported++;
        this._cd.markForCheck();
        let result = await this._metfpetService.importCandidatLibre(candidatsLibres).toPromise();
        this.successImportEntries.push(result);
      } catch(error) {
        candidatsLibres.importErrorMessage = error.isApiException ? (JSON.parse(error.response).Message ? JSON.parse(error.response).Message : error.response) : error;
        toRetryList.push(candidatsLibres);
      }
    }
    if (toRetryList.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: (this.imported - toRetryList.length) + " candidats libres importé(e)(s) avec succès, " 
            + toRetryList.length + " en échec"
        }
      });
      this.exportResult(this.successImportEntries, false);
    } else {
      if (allowRetry) {
        this._dialogService.openConfirmDialog({
          data: {
            title: "Attention", 
            message: (this.totalToimport - toRetryList.length) + " candidats libres importé(e)s avec succès, " 
              + toRetryList.length + " en échec, reprendre les éléments en échec?"
          }
        })
        .afterClosed().subscribe(confirmed => {
          if (confirmed) {
            this.importCandidatsLibres(toRetryList, false);
          } else {
            this.exportResult(toRetryList, true);
            this.exportResult(this.successImportEntries, false);
          }
        });
      } else {
        this.exportResult(toRetryList, true);
        this.exportResult(this.successImportEntries, false);
      }
    }

    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    return {
      nbImported: this.imported,
      failedStudents: toRetryList
    };
  }

  exportResult(toRetryList: Array<MetfpetServiceAgent.ImportCandidatLibreViewModel>, isFailed: boolean): void {
    if (isFailed) {
      if (toRetryList.length > 0) {
        let json : any[] = new Array();
        for (let failed of toRetryList) {
          json.push({
            INA                          : failed.identifiantNationalEleve,
            Nom                          : failed.name,
            Prénoms                      : failed.firstName,
            Institution                  : failed.institution, 
            Programme                    : failed.programme, 
            Raison                       : failed.importErrorMessage
          });
        }
        this.exportAsExcelFile(json, 'failed_results');
      }
    }
    else 
    {
      if (toRetryList.length > 0) {
        let json : any[] = new Array();
        for (let failed of toRetryList) {
          json.push({
            INA                          : failed.identifiantNationalEleve,
            Nom                          : failed.name,
            Prénoms                      : failed.firstName,
            Institution                  : failed.institution, 
            Programme                    : failed.programme
          });
        }
        this.exportAsExcelFile(json, 'successful_results');
      }
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'Résultat': worksheet }, SheetNames: ['Résultat'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_import_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

  downloadTemplate() {
    const headers = [
      { name: "INA", value: "identifiantNationalEleve" },
      { name: "Nom", value: "nom" },
      { name: "Prénom", value: "prenom" },      
      { name: "Nom de l'institution", value: "institution" },
      { name: "Programme", value: "programme" },
    ]
    const templateName = "CandidatLibreTemplate";
    this._exportService.downloadTemplate(templateName, headers);
  }
}
