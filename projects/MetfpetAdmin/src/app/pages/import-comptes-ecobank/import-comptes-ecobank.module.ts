import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ImportComptesEcobankComponent } from './import-comptes-ecobank.component';
import { ImportComptesEcobankTabComponent } from './import-comptes-ecobank-tab.component';
import { ImportComptesEcobankDialog } from './import-comptes-ecobank.dialog';

export const routes = [
  {
    path: '', 
    component: ImportComptesEcobankTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'comptes-ecobank',
          pathMatch: 'full'
        },
        { 
          path: 'comptes-ecobank', 
          component: ImportComptesEcobankComponent, 
          data: { breadcrumb: 'Importer les comptes Écobank' }
        },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [
    SharedModule,
    ImportComptesEcobankComponent,
  ],
  declarations: [
    ImportComptesEcobankTabComponent,
    ImportComptesEcobankComponent,
    ImportComptesEcobankDialog,
  ], 
  entryComponents: [
    ImportComptesEcobankDialog
  ]
})
export class ImportComptesEcobankModule {

}
