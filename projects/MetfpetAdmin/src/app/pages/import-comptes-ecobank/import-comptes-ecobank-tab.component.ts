import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './import-comptes-ecobank-tab.component.html',
})
export class ImportComptesEcobankTabComponent {

  navs = [
    {
      routerLink: '/' + UiPath.admin.import.comptesEcobank, 
      label: 'Importer les comptes Écobank' 
    },
  ];

  public title: string;
  routerEventSubscription: Subscription;

  constructor(protected router: Router) {
    this.title = 'Import : Importer les comptes Écobank';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = 'Import : ' + selectedTab.label.toLowerCase();
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
