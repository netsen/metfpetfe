import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminCommunicationComponent } from './admin-communication.component';
import { AdminCommunicationParametersComponent } from './admin-communication-parameters.component';
import { SharedModule } from '../../shared/shared.module';
import { NotificationTemplateDialog } from './notification-template.dialog';
import { AdminCommunicationMailDomainsComponent } from './admin-communication-mail-domains.component';
import { MailDomainDialog } from './mail-domain.dialog';

export const routes = [
  { path: '', component: AdminCommunicationComponent, pathMatch: 'full' },
  { path: 'parameters', component: AdminCommunicationParametersComponent, data: { breadcrumb: 'Paramètres' } },
  { path: 'mailDomains', component: AdminCommunicationMailDomainsComponent, data: { breadcrumb: 'Domaine de courriers' } },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    NotificationTemplateDialog,
    AdminCommunicationComponent,
    AdminCommunicationParametersComponent,
    AdminCommunicationMailDomainsComponent,
    MailDomainDialog,
  ],
  entryComponents: [
    NotificationTemplateDialog,
    MailDomainDialog,
  ]
})
export class AdminCommunicationModule {}
