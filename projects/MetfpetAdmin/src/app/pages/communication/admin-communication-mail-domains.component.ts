import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewEncapsulation
} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { Store } from '@ngrx/store';
import {
  AppSettings,
  Settings, 
  BaseTableComponent,
  PerfectScrollService,
  DialogService,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  CommunicationServiceAgent,
  TemplateTypeMap,
  TemplateTypeValues,
} from 'MetfpetLib';
import { NotificationTemplateDialog } from './notification-template.dialog';
import { startWith } from 'rxjs/operators';
import { MailDomainDialog } from './mail-domain.dialog';

@Component({
  templateUrl: './admin-communication-mail-domains.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminCommunicationMailDomainsComponent extends BaseTableComponent {

  settings: Settings; 
  loading: boolean;
  title: string;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _communicationService: CommunicationServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Communication - Domaine de courriers';
    this.sort = {prop: 'name', dir: 'asc'};
  }
  
  ngAfterViewInit() {
    combineLatest([
      this.searchForm.get('name').valueChanges.pipe(startWith(this.searchForm.get('name').value)),
    ])
    .subscribe(([name]) => {
      this.searchForm.patchValue({name}, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null
    });
  }

  protected _search(criteria: any): Observable<any> {
    return this._communicationService.getEmailDomainListPage(criteria);
  }

  newMailDomain() {
    this._dialogService.openDialog(
      MailDomainDialog,
      {
        width: '540px',
        data: {}
      }
    ).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  editMailDomain(row: CommunicationServiceAgent.EmailDomainViewModel) {
    this._dialogService.openDialog(
      MailDomainDialog,
      {
        width: '540px',
        data: {...row}
      }
    ).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  deleteMailDomain(row: CommunicationServiceAgent.EmailDomainViewModel) {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention", 
        message: "Voulez-vous définitivement supprimer ce domaine ?", 
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._communicationService.deleteEmailDomain(row.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({message: 'Le domaine a été supprimée'}));
              this.triggerSearch();
            },
            (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }
}