import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, fromEvent } from 'rxjs';
import { SelectionType } from '@swimlane/ngx-datatable';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { 
  debounceTime, 
  distinctUntilChanged,
  startWith, 
  tap,
} from 'rxjs/operators/';
import { UserIdleService } from 'angular-user-idle'; 
import { Store } from '@ngrx/store';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  CommunicationServiceAgent,
  MetfpetServiceAgent,
  PerfectScrollService,
  StudentStatusValues,
  TemplateType,
  TemplateTypeMap,
  showLoading,
  hideLoading,
  showSuccess,
  showError,
  validateForm,
  TemplateTypeValues,
  AdmissionInstitutionStatusValues,
  InscriptionProgrammeStatusValues,
} from 'MetfpetLib';

@Component({
  templateUrl: './admin-communication.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminCommunicationComponent extends BaseTableComponent {

  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;
  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;

  settings: Settings;
  title: string;
  loading: boolean;
  notificationTemplates: Array<CommunicationServiceAgent.Template>;
  centreExamenList: Array<MetfpetServiceAgent.CentreExamenRowViewModel>;
  sessionsBACList: Array<any>;
  optionsBACList: Array<MetfpetServiceAgent.OptionBACRowViewModel>;
  lyceesList: Array<MetfpetServiceAgent.LyceeRowViewModel>;
  statusList: Array<any>;
  genreList: Array<MetfpetServiceAgent.Genre>;
  communicationModes: Array<any>;
  step: string;
  selectedRows: Array<MetfpetServiceAgent.EtudiantRowViewModel>;
  SelectionType = SelectionType;
  form: FormGroup;
  templateTypeMap = TemplateTypeMap;
  admissionInstitutionStatusList: Array<any>;
  subjectList: Array<any>;
  inscriptionProgrammeStatusList: Array<any>;
  reinscrireApprenantList: Array<any>;
  
  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _store: Store<any>,
    protected _router: Router,
    protected _perfectScrollService: PerfectScrollService,
    private _communicationService: CommunicationServiceAgent.HttpService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _userIdleService: UserIdleService
  ) {
    super(_cd, _formBuilder, _router,_store, _perfectScrollService);
    this.sort = {prop: 'identifiantNationalEleve', dir: 'asc'};
    this.settings = this.appSettings.settings;
    this.title = 'Communication - Envoyer un message';
    this.step = 'stepSearchStudents';
    this.statusList = StudentStatusValues;
    this.admissionInstitutionStatusList = AdmissionInstitutionStatusValues;
    this.inscriptionProgrammeStatusList = InscriptionProgrammeStatusValues;
    this.reinscrireApprenantList = [
      {value : 'Oui', name : 'Oui'},
      {value : 'Non', name : 'Non'},
    ];
    this.communicationModes = TemplateTypeValues;

    this.form = this._formBuilder.group({
      'communicationMode': [null, Validators.required],
      'template': [null, Validators.required],
      'saveTemplate': null,
      'subject': [null, Validators.required],
      'emailMessage': null,
      'smsMessage': null,
    });

    this.selectedRows = [];

    this.form.get('template').valueChanges
      .pipe(
        tap((template: any) => {
          if (template) {
            this.form.get('subject').setValue(template.name);
            let mode = this.form.get('communicationMode').value;
            this.setMessage(mode, template);
          }
        })
      ).subscribe();

    this.form.get('communicationMode').valueChanges
    .pipe(
      tap((communicationMode: any) => {
        let template = this.form.get('template').value;
        if (template) {
          this.setMessage(communicationMode, template);
        }
      })
    ).subscribe();
  }

  private setMessage(mode: any, template: any) {
    if (mode === <any>TemplateType.Email) {
      let notificationTemplate = this.notificationTemplates.find(x => x.subject === template.name && x.type === <any>TemplateType.Email);
      if (notificationTemplate) {
        this.form.get('emailMessage').setValue(notificationTemplate.message);
        this.form.get('smsMessage').setValue(null);
      }
    } else if (mode === <any>TemplateType.Sms) {
      let notificationTemplate = this.notificationTemplates.find(x => x.subject === template.name && x.type === <any>TemplateType.Sms);
      if (notificationTemplate) {
        this.form.get('smsMessage').setValue(notificationTemplate.message);
        this.form.get('emailMessage').setValue(null);
      }
    } else if (mode === <any>TemplateType.All){
      let emailNotificationTemplate = this.notificationTemplates.find(x => x.subject === template.name && x.type === <any>TemplateType.Email);
      if (emailNotificationTemplate) {
        this.form.get('emailMessage').setValue(emailNotificationTemplate.message);
      } else {
        this.form.get('emailMessage').setValue(null);
      }
      let smsNotificationTemplate = this.notificationTemplates.find(x => x.subject === template.name && x.type === <any>TemplateType.Sms);
      if (smsNotificationTemplate) {
        this.form.get('smsMessage').setValue(smsNotificationTemplate.message);
      } else {
        this.form.get('smsMessage').setValue(null);
      }
    }
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getGenreList().subscribe(data => {
      this.genreList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getCentreExamenListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.centreExamenList = data.results;
      this._cd.markForCheck();
    });

    this._communicationService.getTemplateListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.notificationTemplates = data.results;
      let distinctSubjects = [...new Set(this.notificationTemplates.map(item => item.subject))];
      this.subjectList = distinctSubjects.map(data => { return {name: data}});
      this._cd.markForCheck();
    });

    this._metfpetService.getSessionBACList().subscribe(data => {
      this.sessionsBACList = data.map(x => {return {name: x}});
      this._cd.markForCheck();
    });

    this._metfpetService.getOptionBACListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.optionsBACList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getLyceeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.lyceesList = data.results;
      this._cd.markForCheck();
    });

    combineLatest([
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(this.searchFirstname.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('firstName').value}
          })
        ),
      this.searchForm.get('admissionInstitutionStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('admissionInstitutionStatus').value)
        ),
      this.searchForm.get('inscriptionProgrammeStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('inscriptionProgrammeStatus').value)
        ),
      this.searchForm.get('reinscrireApprenant').valueChanges
        .pipe(
          startWith(this.searchForm.get('reinscrireApprenant').value)
        )
    ])
    .subscribe((
      [
        eventSearchNationalStudentIdentifier,
        eventSearchName, 
        eventFirstname,
        admissionInstitutionStatus,
        inscriptionProgrammeStatus,
        reinscrireApprenant
      ]) => {
      this.searchForm.patchValue({
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null, 
        admissionInstitutionStatus,
        inscriptionProgrammeStatus,
        reinscrireApprenant,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      identifiantNationalEleve: null,
      name: null,
      firstName: null,
      status: null,
      numeroPV: null,
      sessionBAC : null,
      optionBACId: null,
      lyceeId: null,
      centreExamenId: null,
      genreId: '',
      admissionInstitutionStatus: null,
      inscriptionProgrammeStatus: null,
      reinscrireApprenant: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getEtudiantListPage(criteria);
  }

  sendNotification() {
    let sendRequest = new CommunicationServiceAgent.SendManyCommunicationsRequestDTO();

    sendRequest.mode = this.form.get('communicationMode').value;
    sendRequest.userIds = this.selectedRows.map(x => x.userId);

    let notificationTemplate = this.notificationTemplates.find(x => x.subject === this.form.get('template').value.name);
    let templateCode = notificationTemplate.code;
    if (this.form.get('saveTemplate').value) {
      sendRequest.saveTemplate = true;
      templateCode = 'new';

      let emailTemplate = new CommunicationServiceAgent.Template();
      emailTemplate.type = CommunicationServiceAgent.TemplateType.Email;
      emailTemplate.subject = this.form.get('subject').value;
      emailTemplate.message = this.form.get('emailMessage').value;
      sendRequest.newEmailTemplate = emailTemplate;
      
      let smsTemplate = new CommunicationServiceAgent.Template();
      smsTemplate.type = CommunicationServiceAgent.TemplateType.SMS;
      smsTemplate.subject = this.form.get('subject').value;
      smsTemplate.message = this.form.get('smsMessage').value;
      sendRequest.newSMSTemplate = smsTemplate;

    } else {
      sendRequest.saveTemplate = false;
    }

    this._store.dispatch(showLoading());
    this._userIdleService.stopWatching();

    this._communicationService.sendManyCommunications(sendRequest, templateCode)
      .subscribe(
        () => {
          this._store.dispatch(showSuccess({message: 'Messages envoyés'}));
          this.returnMainPage();
        },
        (error) => this._store.dispatch(showError({message: error.response}))
      )
      .add(() => {
        this._store.dispatch(hideLoading());
        this._userIdleService.startWatching();
      });
  }

  onSelect({ selected }) {
    this.selectedRows.splice(0, this.selectedRows.length);
    this.selectedRows.push(...selected);
  }

  onNext() {
    switch(this.step) {
      case 'stepSearchStudents': {
        this.step = 'stepSelectStudents';
        break;
      }
      case 'stepSelectStudents': {
        this.step = 'stepEnterMessage';
        break;
      }
      case 'stepEnterMessage': {
        validateForm(this.form);
        if (this.form.valid) {
          this.step = 'stepConfirm';
        }
        
        break;
      }
      case 'stepConfirm': {
        this.sendNotification();
      }
    }
  }

  onCancel() {
    this.returnMainPage();
  }
  
  getOption(row: MetfpetServiceAgent.EtudiantRowViewModel): any {
    if (row.optionBACName) {
      return row.optionBACName;
    }
    if (row.optionBEPCName) {
      return row.optionBEPCName;
    }
    return row.optionTerminaleName;
  }

  getSession(row: MetfpetServiceAgent.EtudiantRowViewModel): any {
    if (row.sessionBAC) {
      return row.sessionBAC;
    }
    if (row.sessionBEPC) {
      return row.sessionBEPC;
    }
    return row.sessionTerminale;
  }

  returnMainPage() {
    this.selectedRows = [];
    this.step = 'stepSearchStudents';
    this.form.reset();
  }

  get stepNumber() {
    switch(this.step) {
      case 'stepSelectStudents': {
        return 1;
      }
      case 'stepEnterMessage': {
        return 2;
      }
      case 'stepConfirm': {
        return 3;
      }
    }
  }
}