import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewEncapsulation
} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { Store } from '@ngrx/store';
import {
  AppSettings,
  Settings, 
  BaseTableComponent,
  PerfectScrollService,
  DialogService,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  CommunicationServiceAgent,
  TemplateTypeMap,
  TemplateTypeValues,
  MetfpetServiceAgent,
} from 'MetfpetLib';
import { NotificationTemplateDialog } from './notification-template.dialog';
import { startWith } from 'rxjs/operators';

@Component({
  templateUrl: './admin-communication-parameters.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminCommunicationParametersComponent extends BaseTableComponent {

  settings: Settings; 
  loading: boolean;
  title: string;
  templateTypeMap = TemplateTypeMap;
  templateTypeValues = TemplateTypeValues;
  smsPermissionsDTO: MetfpetServiceAgent.OtpPermissionsDTO;
  
  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _communicationService: CommunicationServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Communication - Paramètres';
    this.sort = {prop: 'subject', dir: 'asc'};
  }
  
  ngAfterViewInit() {
    this._metfpetService.getSmsPermission().subscribe(data => {
      this.smsPermissionsDTO = data;
      this._cd.markForCheck();
    });
    
    combineLatest([
      this.searchForm.get('subject').valueChanges.pipe(startWith(this.searchForm.get('subject').value)),
      this.searchForm.get('type').valueChanges.pipe(startWith(this.searchForm.get('type').value)),
      this.searchForm.get('message').valueChanges.pipe(startWith(this.searchForm.get('message').value)),
    ])
    .subscribe(([subject, type, message]) => {
      this.searchForm.patchValue({subject, type, message}, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      subject: null,
      type: null,
      message: null
    });
  }

  protected _search(criteria: any): Observable<any> {
    return this._communicationService.getTemplateListPage(criteria);
  }

  newTemplate() {
    this._dialogService.openDialog(
      NotificationTemplateDialog,
      {
        width: '540px',
        data: {}
      }
    ).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  editTemplate(template: CommunicationServiceAgent.Template) {
    this._dialogService.openDialog(
      NotificationTemplateDialog,
      {
        width: '540px',
        data: {...template}
      }
    ).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  deleteTemplate(template: CommunicationServiceAgent.Template) {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention", 
        message: "Voulez-vous définitivement supprimer ce modèle ?", 
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._communicationService.deleteTemplate(template.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({message: 'Le modèle a été supprimée'}));
              this.triggerSearch();
            },
            (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  getSmSPermissionButtonText() {
    if (this.smsPermissionsDTO && this.smsPermissionsDTO.isOtpEnabled)
    {
      return 'SMS sauf OTP bloqués';
    }
    return 'SMS débloqués';
  }

  setSmsPermission() {
    if (this.smsPermissionsDTO)
      {
        let message = 'Cette action va ' + this.getSmSPermissionButtonText() + ', voulez-vous continuer ?';
        this._dialogService.openConfirmDialog({
          data: {
            title: "Attention",
            message: message,
          }
        }).afterClosed().subscribe(confirmed => {
          if (confirmed) {
            this._store.dispatch(showLoading());
            if (this.smsPermissionsDTO.isOtpEnabled)
            {
              this.smsPermissionsDTO.isOtpEnabled = false;
            }
            else
            {
              this.smsPermissionsDTO.isOtpEnabled = true;
            }
            this._metfpetService.updateOtpPermission(this.smsPermissionsDTO)
              .subscribe(
                () => {
                  this._store.dispatch(showSuccess({}));
                },
                (error) => this._store.dispatch(showError({ message: error.response }))
              )
              .add(() => {
                this._store.dispatch(hideLoading());
                this._cd.markForCheck();
              });
          }
        });
      }
  }
}