import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  TemplateTypeValues,
  CommunicationServiceAgent,
  showException,
} from 'MetfpetLib';

@Component({
  templateUrl: './notification-template.dialog.html',
})
export class NotificationTemplateDialog {

  title: string;
  form: FormGroup;
  templateTypeValues = TemplateTypeValues;
  model: CommunicationServiceAgent.Template;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _dialogRef: MatDialogRef<NotificationTemplateDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _communicationService: CommunicationServiceAgent.HttpService
  ) {
    this.form = this._formBuilder.group({
      'type': [null, Validators.required],
      'code': [null, Validators.required],
      'subject': [null, Validators.required],
      'message': [null, Validators.required],
      'status': null,
    });

    this.title = data.id ? 'Modifier un modèle de communication' : 'Nouveau modèle de communication';
    this.model = data;
  }

  ngOnInit() {
    this.form.patchValue({
      'type': this.model.type,
      'code': this.model.code,
      'subject': this.model.subject,
      'message': this.model.message,
      'status': this.model.status
    });

    if (!this.model.id) {
      this.form.get('status').setValue(1);
      this.form.get('code').setValue('new');
    }
  }

  public onClose(): void {
    this._dialogRef.close();
  }

  public onConfirm(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());

      if (!this.model.id) {
        this._communicationService.createTemplate(CommunicationServiceAgent.Template.fromJS(
          Object.assign(this.model, this.form.value)))
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({}));
              this._dialogRef.close(true);
            },
            error => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));

      } else {
        this._communicationService.updateTemplate(this.model.id, CommunicationServiceAgent.Template.fromJS(
          Object.assign(this.model, this.form.value)))
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({}));
              this._dialogRef.close(true);
            },
            error => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    }
  }
}