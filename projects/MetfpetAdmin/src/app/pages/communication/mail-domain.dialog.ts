import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  TemplateTypeValues,
  CommunicationServiceAgent,
  showException,
} from 'MetfpetLib';

@Component({
  templateUrl: './mail-domain.dialog.html',
})
export class MailDomainDialog {

  title: string;
  form: FormGroup;
  templateTypeValues = TemplateTypeValues;
  model: CommunicationServiceAgent.EmailDomainViewModel;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _dialogRef: MatDialogRef<MailDomainDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _communicationService: CommunicationServiceAgent.HttpService
  ) {
    this.form = this._formBuilder.group({
      'name': [null, Validators.required],
    });

    this.title = data.id ? 'Modifier un domaine de courrier' : 'Nouveau domaine de courrier';
    this.model = data;
  }

  ngOnInit() {
    this.form.patchValue({
      'name': this.model.name,
    });
  }

  public onClose(): void {
    this._dialogRef.close();
  }

  public onConfirm(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());

      if (!this.model.id) {
        this._communicationService.createEmailDomain(CommunicationServiceAgent.EmailDomainViewModel.fromJS(
          Object.assign(this.model, this.form.value)))
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({}));
              this._dialogRef.close(true);
            },
            error => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));

      } else {
        this._communicationService.updateEmailDomain(CommunicationServiceAgent.EmailDomainViewModel.fromJS(
          Object.assign(this.model, this.form.value)))
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({}));
              this._dialogRef.close(true);
            },
            error => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    }
  }
}