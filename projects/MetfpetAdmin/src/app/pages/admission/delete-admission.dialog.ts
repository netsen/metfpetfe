import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  ExportService,
  ImportExportType,
  AdmissionInstitutionOption,
} from 'MetfpetLib';
import { UserIdleService } from 'angular-user-idle';

@Component({
  templateUrl: './delete-admission.dialog.html',
})
export class DeleteAdmissionDialog {

  form: FormGroup;
  processing: boolean;
  processed: number;
  totalToprocess: number;
  showWarningMessage: boolean;
  filters: any;
  isArchive: boolean;
  admissionInstitutionIds : Array<string>;
  confirmText: string;
  buttonText: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<DeleteAdmissionDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    protected _cdRef: ChangeDetectorRef,
    private _userIdleService: UserIdleService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.filters = data.filters;
    this.admissionInstitutionIds = data.admissionInstitutionIds;
    this.isArchive = data.isArchive;
    if (this.isArchive)
    {
      this.buttonText = 'Archiver';
    }
    else
    {
      this.buttonText = 'Supprimer';
    }
    if (this.admissionInstitutionIds)
    {
      this.confirmText = this.isArchive ?
        'Veuillez confirmer dans les archives les admissions' :
        'Veuillez confirmer et supprimer les admissions';
    }
    else
    {
      this.confirmText = this.isArchive ?
        'Veuillez confirmer dans les archives toutes les admissions' :
        'Veuillez confirmer et supprimer toutes les admissions';
    }
    this.processing = false;
    this.form = this._formBuilder.group({
    });
  }

  onConfirm() {
    if (this.admissionInstitutionIds)
    {
      this.process(this.admissionInstitutionIds);
    }
    else
    {
      this._store.dispatch(showLoading());
      this._metfpetService.getExportedAdmissionInstitutionIdList(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1,
        filters: this.filters,
      }))
      .subscribe(
        (ids) => {
          this.process(ids);
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
    }
  }

  async process(ids: Array<string>) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this._store.dispatch(showLoading());
    this.showWarningMessage = true;
    this.processed = 0;
    this.totalToprocess = ids.length;
    this._cdRef.markForCheck();

    for (let id of ids) {

      this.processed++;
      this._cdRef.markForCheck(); 

      try {
        if (this.isArchive)
        {
          await this._metfpetService.updateAdmissionInstitutionForAdmin
            (MetfpetServiceAgent.UpdateAdmissionInstitution.fromJS(
              {
                id: id,
                option: <any>AdmissionInstitutionOption.Archive
              }
            )).toPromise();
        }
        else
        {
          await this._metfpetService.deleteAdmissionInstitutionChoice(id).toPromise();
        }
      } catch(error) {
      }
    }

    this.processing = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    this._store.dispatch(hideLoading());
    this._dialogRef.close(true);
  }

  onClose() {
    this._dialogRef.close();
  }
}