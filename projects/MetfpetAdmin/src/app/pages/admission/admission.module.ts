import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { SessionDialog } from './session.dialog';
import { SessionComponent } from './session.component';
import { AdmissionListComponent } from './admission-list.component';
import { AdmissionListExportDialog } from './admission-list-export.dialog';
import { UpdateSessionStatusDialog } from './update-session-status.dialog';
import { DeleteAdmissionListComponent } from './delete-admission-list.component';
import { DeleteAdmissionDialog } from './delete-admission.dialog';

export const routes = [
  { 
    path: 'session', 
    component: SessionComponent, 
    pathMatch: 'full' 
  },
  { 
    path: '', 
    component: AdmissionListComponent, 
    pathMatch: 'full' 
  },
  { 
    path: 'archive', 
    component: DeleteAdmissionListComponent, 
    data: { breadcrumb: 'Archiver ou supprimer des admissions impayées' }  
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    SessionComponent,
    SessionDialog,
    AdmissionListComponent,
    AdmissionListExportDialog,
    UpdateSessionStatusDialog,
    DeleteAdmissionListComponent,
    DeleteAdmissionDialog,
  ],
  entryComponents: [
    SessionDialog,
    AdmissionListExportDialog,
    UpdateSessionStatusDialog,
    DeleteAdmissionDialog,
  ]
})
export class AdmissionModule {}
