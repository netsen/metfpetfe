import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  ExportService,
  ImportExportType,
} from 'MetfpetLib';
import { UserIdleService } from 'angular-user-idle';

@Component ({
  templateUrl: './admission-list-export.dialog.html',
})
export class AdmissionListExportDialog {

  public form: FormGroup;
  processing: boolean;
  processed: number;
  totalToprocess: number;
  showWarningMessage: boolean;
  filters: any;
  getByChoix: boolean;
  fileName: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AdmissionListExportDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    protected _cdRef: ChangeDetectorRef,
    private _userIdleService: UserIdleService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.filters = data.filters;
    this.getByChoix = data.getByChoix;
    this.fileName = data.fileName;

    this.form = this._formBuilder.group({
      exportExcel: true,
      exportPrenom: true,
      exportNom: true,
      exportDateNaissance: true,
      exportSexe: true,
      exportINA: true,
      exportEtablissement: true, 
      exportChoix: true,
      exportAnnee: true,
      exportPv: true,
      exportStatut: true,
      exportDecision: true,
      exportProgramme: false,
      exportFaculte: false, 
      exportInstitution: true,
      exportPvEtudiant: true,
      exportCentreConcours: true,
      exportPrefectureDeResidence: true,
      exportEtudiantLevel: true,
      exportTypeAdmissionName: true,
      exportNiveauAccesName: true,
      exportEmail: true,
      exportPhone: true,
      exportLieuNaissance: true,
      exportNomPere: true,
      exportNomMere: true,
      exportBiometricStatus: true,
    });
  }

  onConfirm(): void {
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.getExportedAdmissionInstitutionIdList(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1,
        filters: this.filters,
      }))
      .subscribe(
        (ids) => {
          this.process(ids);
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
    }
  }

  async process(ids: Array<string>) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this._store.dispatch(showLoading());
    this.showWarningMessage = true;
    this.processed = 0;
    this.totalToprocess = ids.length;
    this._cdRef.markForCheck();

    let exportedAdmissions = new Array<MetfpetServiceAgent.AdmissionInstitutionRowViewModel>();
    for (let id of ids) {

      this.processed++;
      this._cdRef.markForCheck(); 

      try {
        let data = await this._metfpetService.getAdmissionInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
          pageIndex: -1,
          filters: {
            id: id,
            getByChoix: this.getByChoix
          },
        })).toPromise();
        if (data && data.results && data.results.length > 0) {
          for (let item of data.results)
            {
              exportedAdmissions.push(item);
            }
        }
      } catch(error) {
      }
    }

    this._exportService.exportAdmissions(exportedAdmissions, this.form.value, this.fileName);

    this.processing = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    this._store.dispatch(hideLoading());
    this.dialogRef.close();
  }

  onClose(): void {
    this.dialogRef.close(null);
  }
}