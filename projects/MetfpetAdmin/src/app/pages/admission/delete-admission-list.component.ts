import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef,
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators/';
import { Store } from '@ngrx/store';
import { 
  AppSettings,
  Settings,
  AdmissionInstitutionStatusValues,
  AdmissionDecisionValues,
  BaseTableComponent,
  PerfectScrollService,
  DialogService,
  ExportService,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  addPostOptions,
  AdmissionInstitutionStatus,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { Guid } from 'guid-typescript';
import { Location } from '@angular/common';
import { DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';
import { DeleteAdmissionDialog } from './delete-admission.dialog';
import { MatDialog } from '@angular/material/dialog';

@Component({
  templateUrl: './delete-admission-list.component.html',
  styleUrls: ['./delete-admission-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteAdmissionListComponent extends BaseTableComponent {

  @ViewChild('table', { static: true }) table: DatatableComponent;
  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchPVNumber', {static: true}) searchPVNumber: ElementRef;
  @ViewChild('searchPVNumberEtudiant', {static: true}) searchPVNumberEtudiant: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;

  settings: Settings;
  title: string;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  sessionsList: Array<any>;
  optionsList: Array<MetfpetServiceAgent.OptionRowViewModel>;
  statusList: Array<any>;
  decisionList: Array<any>;
  centreExamenList: Array<MetfpetServiceAgent.CentreExamenRowViewModel>;
  lyceesList: Array<MetfpetServiceAgent.LyceeRowViewModel>;
  genreList: Array<MetfpetServiceAgent.Genre>;
  anneeList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  centreConcoursList: Array<MetfpetServiceAgent.CentreConcoursRowViewModel>; 
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissement>;
  hasPaymentValues: Array<any>;
  SelectionType = SelectionType;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialog: MatDialog,
    private _location: Location,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Admission - Archiver ou supprimer des admissions impayées';
    this.statusList = AdmissionInstitutionStatusValues;
    this.decisionList = AdmissionDecisionValues;
    this.hasPaymentValues = [
      {value : 'Oui', name : 'Oui'},
      {value : 'Non', name : 'Non'},
    ];
    this.sort = {prop: 'identifiantNationalEleve', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getGenreList().subscribe(data => {
      this.genreList = data;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getStatusEtablissementList().subscribe(data => {
      this.statusEtablissementList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getCentreConcoursListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.centreConcoursList = data.results;
      var allCentreConcours = new MetfpetServiceAgent.CentreConcoursRowViewModel();
      allCentreConcours.id = Guid.EMPTY;
      allCentreConcours.name = 'Tous les centres de concours';
      this.centreConcoursList.push(allCentreConcours);
      this._cd.markForCheck();
    });

    this._metfpetService.getCentreExamenListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.centreExamenList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getLyceeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.lyceesList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getSessionBACList().subscribe(data => {
      this.sessionsList = data.map(x => {return {name: x}});
      this._cd.markForCheck();
    });

    this._metfpetService.getOptionList().subscribe(data => {
      this.optionsList = addPostOptions(data);
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeList = data.results;
      this._cd.markForCheck();
    });

    this.searchForm.get('institution').valueChanges.pipe(
      startWith(this.searchForm.get('institution').value),
      tap(institution => {
        this.programList = [];
        if (!this.isFirstSearch) {
          this.searchForm.get('programme').setValue(null);
        }
        if (!institution) {
          return;
        }
        this._loadPrograms(institution);
      })
    ).subscribe();
    
    combineLatest([
      fromEvent(this.searchPVNumber.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('numeroPV').value}
          })
        ),
      fromEvent(this.searchPVNumberEtudiant.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('numeroPVEtudiant').value}
          })
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      this.searchForm.get('institution').valueChanges
        .pipe(
          startWith(this.searchForm.get('institution').value)
        ),
      this.searchForm.get('programme').valueChanges
        .pipe(
          startWith(this.searchForm.get('programme').value)
        ),
      this.searchForm.get('status').valueChanges
        .pipe(
          startWith(this.searchForm.get('status').value)
        ),
      this.searchForm.get('decision').valueChanges
        .pipe(
          startWith(this.searchForm.get('decision').value)
        ),
      this.searchForm.get('annee').valueChanges
        .pipe(
          startWith(this.searchForm.get('annee').value)
        ),
      this.searchForm.get('centreConcours').valueChanges
        .pipe(
          startWith(this.searchForm.get('centreConcours').value)
        ),
      this.searchForm.get('hasPayment').valueChanges
        .pipe(
          startWith(this.searchForm.get('hasPayment').value)
        ),
    ])
    .subscribe((
      [
        eventSearchPVNumber,
        eventSearchPVNumberEtudiant,
        eventSearchNationalStudentIdentifier,
        eventSearchName, 
        eventFirstname,
        institution,
        programme,
        status,
        decision,
        annee,
        centreConcours,
        hasPayment
      ]) => {
      this.searchForm.patchValue({
        numeroPV: eventSearchPVNumber ? eventSearchPVNumber['target'].value : null, 
        numeroPVEtudiant: eventSearchPVNumberEtudiant ? eventSearchPVNumberEtudiant['target'].value : null, 
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null, 
        institution,
        programme,
        status,
        decision,
        annee,
        centreConcours,
        hasPayment
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      status: <any> AdmissionInstitutionStatus.APayer,
      institution: null,
      programme: null,
      numeroPV: null,
      numeroPVEtudiant: null,
      identifiantNationalEleve: null,
      name: null,
      firstName: null,
      session: null,
      optionId: null,
      lyceeId: null,
      centreExamenId: null,
      genreId: '',
      decision: null,
      annee: null,
      getByChoix: false,
      centreConcours: null,
      statusEtablissement: null,
      hasPayment: null,
      createFrom: null,
      createTo: null
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getAdmissionInstitutionListPage(criteria);
  }

  open(selectedAdmissionInstitution: MetfpetServiceAgent.AdmissionInstitutionRowViewModel) {
    this._router.navigate([`${UiPath.admin.students.dossier}/${selectedAdmissionInstitution.etudiantId}`], { state: { previousPage: 'Admission' } });
  }

  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {'institution': institutionId}
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });
  }

  goBack() {
    this._location.back()
  }

  deleteAdmission() {
    if (this.table.selected && this.table.selected.length > 0)
    {
      this._dialog.open(DeleteAdmissionDialog, {
        width: '650px',
        data: {
          filters: this.searchForm.value,
          admissionInstitutionIds: this.table.selected.map(x => x.id),
          isArchive: false
        }
      }).afterClosed().subscribe(result => {
        if (result) {
          this.triggerSearch();
        }
      });
    }
  }

  deleteAllAdmission() {
    this._dialog.open(DeleteAdmissionDialog, {
      width: '650px',
      data: {
        filters: this.searchForm.value,
        admissionInstitutionIds: null,
        isArchive: false
      }
    }).afterClosed().subscribe(result => {
      if (result) {
        this.triggerSearch();
      }
    });
  }

  archiveAdmission() {
    if (this.table.selected && this.table.selected.length > 0) {
      this._dialog.open(DeleteAdmissionDialog, {
        width: '650px',
        data: {
          filters: this.searchForm.value,
          admissionInstitutionIds: this.table.selected.map(x => x.id),
          isArchive: true
        }
      }).afterClosed().subscribe(result => {
        if (result) {
          this.triggerSearch();
        }
      });
    }
  }

  archiveAllAdmission() {
    this._dialog.open(DeleteAdmissionDialog, {
      width: '650px',
      data: {
        filters: this.searchForm.value,
        admissionInstitutionIds: null,
        isArchive: true
      }
    }).afterClosed().subscribe(result => {
      if (result) {
        this.triggerSearch();
      }
    });
  }
}
