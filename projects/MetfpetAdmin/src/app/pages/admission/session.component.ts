import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewEncapsulation
} from '@angular/core';
import { Observable } from 'rxjs';
import { Guid } from 'guid-typescript';
import { repeatWhen, filter, take, delay } from 'rxjs/operators';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UserIdleService } from 'angular-user-idle'; 
import { 
  AppSettings,
  Settings,
  DialogService, 
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  SessionAdmissionStatus,
  MetfpetServiceAgent,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { Moment } from 'moment';
import * as moment from 'moment';
import { SessionDialog } from './session.dialog';
import { UiPath } from '../ui-path';
import { UpdateSessionStatusDialog } from './update-session-status.dialog';

@Component({
  templateUrl:'./session.component.html',
  styleUrls: ['./session.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SessionComponent {

  settings: Settings;
  title:string;
  currentSession: MetfpetServiceAgent.SessionAdmissionDTO;
  currentAnneeAcademique: MetfpetServiceAgent.AnneeAcademiqueDTO;
  notificationSentDate: Date;
  anneeAcademiqueName:string;
  dateDebutAdmission: Date;
  dateLimiteAdmission: Date;
  dateLimiteOffreAdmission: Date;
  batchJobExecution: any;
  plateformeInscriptionConfigDTO: MetfpetServiceAgent.PlateformeInscriptionConfigDTO;

  constructor(
    public appSettings:AppSettings, 
    private _cdRef: ChangeDetectorRef, 
    private _router: Router,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _userIdleService: UserIdleService,
    private _dialogService: DialogService
  ) {
    this.settings = this.appSettings.settings;
    this.title = "Admissions - Session en cours";
  }

  ngOnInit() {
    this.getCurrentSession();
    this.getAnneeAcademiqueAdmissionOuverte();
    this._metfpetService.getPlateformeInscriptionConfig('S’inscrire sur la plateforme').subscribe(data => 
      {
        this.plateformeInscriptionConfigDTO = data;
        this._cdRef.markForCheck();
      });
  }

  newSession() {
    if (!this.isSessionExists() && this.isAnneeAcademiqueExists()) {
      this.currentSession = new MetfpetServiceAgent.SessionAdmissionDTO();
      this.currentSession.id = Guid.EMPTY;
      this.currentSession.anneeAcademiqueId = this.currentAnneeAcademique.id;
      this.currentSession.anneeAcademiqueName = this.currentAnneeAcademique.name;
      this.currentSession.status = MetfpetServiceAgent.SessionAdmissionStatus.ADemarrer;
      this._dialogService.openDialog(
        SessionDialog,
        {
          width: '420px',
          data: {
            title: "Nouvelle session admission", 
            message: "Veuillez selectionner l'année puis les dates de début et fin",
            session: this.currentSession
          }
        }
      ).afterClosed().subscribe(result => {
        if (result) {
          this._store.dispatch(showLoading());
          this.currentSession.dateDebutAdmission = result.dateDebutAdmission;
          this.currentSession.dateLimiteAdmission = result.dateLimiteAdmission;
          this.currentSession.dateLimiteOffreAdmission = result.dateLimiteOffreAdmission;
          this._metfpetService.createSessionAdmission(this.currentSession)
          .subscribe(
            (data : any) => {
              if (data && data['message']) {
                this._store.dispatch(showError({message: data['message']}));
              
              } else {
                this._store.dispatch(showSuccess({
                  message: 'Nouvelle session admission rajoutée avec succès'
                }));
                this.getCurrentSession();
              }
            },
            (error) => this._store.dispatch(showError({message: error.response}))
          )
          .add(() => {
            this._store.dispatch(hideLoading());
            this._cdRef.markForCheck();
          });
        }
      });
    }
  }

  modifySessionStatus() {
    this._dialogService.openDialog(
      UpdateSessionStatusDialog,
      {
        width: '500px',
        data: {
          title: "Modification de la session admission", 
          session: this.currentSession
        }
      }
    ).afterClosed().subscribe(result => {
      if (result) {
        this._store.dispatch(showLoading());
        this.currentSession.status = result.status;
        this.currentSession.hideConcoursProgramme = result.hideConcoursProgramme;
        this.currentSession.onlyShowConcoursPrivateProgramme = result.onlyShowConcoursPrivateProgramme;
        this.currentSession.hideConcoursOrientationAccess = result.hideConcoursOrientationAccess;
        this.currentSession.hideDossierOrientationAccess = result.hideDossierOrientationAccess;
        this.currentSession.showSanteConcours = result.showSanteConcours;
        this._metfpetService.updateSessionAdmission(this.currentSession)
        .subscribe(
          (data : any) => {
            if (data && data['message']) {
              this._store.dispatch(showError({message: data['message']}));
            
            } else {
              this._store.dispatch(showSuccess({
                message: 'Modification de la session admission avec succès'
              }));
              this.getCurrentSession();
            }
          },
          (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => {
          this._store.dispatch(hideLoading());
          this._cdRef.markForCheck();
        });
      }
    });
  }

  modifySession() {
    this._dialogService.openDialog(
      SessionDialog,
      {
        width: '420px',
        data: {
          title: "Modification de la session admission", 
          session: this.currentSession
        }
      }
    ).afterClosed().subscribe(result => {
      if (result) {
        this._store.dispatch(showLoading());
        this.currentSession.dateDebutAdmission = result.dateDebutAdmission;
        this.currentSession.dateLimiteAdmission = result.dateLimiteAdmission;
        this.currentSession.dateLimiteOffreAdmission = result.dateLimiteOffreAdmission;
        this._metfpetService.updateSessionAdmission(this.currentSession)
        .subscribe(
          (data : any) => {
            if (data && data['message']) {
              this._store.dispatch(showError({message: data['message']}));
            
            } else {
              this._store.dispatch(showSuccess({
                message: 'Modification de la session admission avec succès'
              }));
              this.getCurrentSession();
            }
          },
          (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => {
          this._store.dispatch(hideLoading());
          this._cdRef.markForCheck();
        });
      }
    });
  }

  startSession() {
    if (this.isEnableDemarrerSession()) {
      this.confirmAndExecuteAction("Cette action va démarrer la session, voulez-vous continuer ?", MetfpetServiceAgent.SessionAdmissionExecution.Demarrer);
    }
  }
  
  private confirmAndExecuteAction(message: string, execution: MetfpetServiceAgent.SessionAdmissionExecution) {
    this._dialogService.openConfirmDialog({
      data: {
        title: "Attention",
        message: message,
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        var updateSessionAdmissionStateRequest = new MetfpetServiceAgent.UpdateSessionAdmissionStateRequest();
        updateSessionAdmissionStateRequest.sessionAdmissionId = this.currentSession.id;
        updateSessionAdmissionStateRequest.execution = execution;
        this._metfpetService.updateSessionAdmissionState(updateSessionAdmissionStateRequest)
          .subscribe(
            (data: any) => {
              if (data) {
                this.setCurrentSession(data);
              }
            },
            (error) => this._store.dispatch(showError({ message: error.response }))
          )
          .add(() => {
            this._store.dispatch(hideLoading());
            this._cdRef.markForCheck();
          });
      }
    });
  }

  holdSession() {
    if (this.isEnableSuspendSession()) {
      this.confirmAndExecuteAction("Cette action va suspendre la session, voulez-vous continuer ?", MetfpetServiceAgent.SessionAdmissionExecution.Suspendre);
    }
  }
  
  stopSession() {
    if (this.isEnableArreterSession()) {
      this.confirmAndExecuteAction("Cette action va arrêter la session en cours ?", MetfpetServiceAgent.SessionAdmissionExecution.Arreter);
    }
  }
  
  runSession() {
    if (this.isSessionAExecuter()) {
      this.confirmAndExecuteAction("Cette action va exécuter la session en cours ?", MetfpetServiceAgent.SessionAdmissionExecution.Executer);
    }
  }

  notifySession() {
    if (this.isSessionANotifier()) {
      this.confirmAndExecuteAction("Cette action va notifier la session en cours ?", MetfpetServiceAgent.SessionAdmissionExecution.Notifier);
    }
  }

  endSession() {
    if (this.isSessionAConfirmer()) {
      this.confirmAndExecuteAction("Cette action va terminer la session en cours ?", MetfpetServiceAgent.SessionAdmissionExecution.Terminer);
    }
  }

  getCurrentSession() {
    this._metfpetService.getCurrentSessionAdmission().subscribe((data : any) => {
      if (data) {
        this.setCurrentSession(data);
      }
    });
  }

  private setCurrentSession(data: any) {
    this.currentSession = data;
    this.anneeAcademiqueName = this.currentSession.anneeAcademiqueName;
    this.dateDebutAdmission = this.currentSession.dateDebutAdmission;
    this.dateLimiteAdmission = this.currentSession.dateLimiteAdmission;
    this.dateLimiteOffreAdmission = this.currentSession.dateLimiteOffreAdmission;
    this._cdRef.markForCheck();
  }

  getAnneeAcademiqueAdmissionOuverte() {
    this._metfpetService.getAnneeAcademiqueAdmissionOuverte().subscribe((data : any) => {
      if (data) {
        this.currentAnneeAcademique = data;
        this._cdRef.markForCheck();
      }
    });
  }

  public isEnableDemarrerSuspendArreter() {
    return this.isSessionExists() && (
      this.currentSession.status === <any>SessionAdmissionStatus.ADemarrer 
      || this.currentSession.status === <any>SessionAdmissionStatus.EnCours
      || this.currentSession.status === <any>SessionAdmissionStatus.Suspendue);
  }

  public isEnableDemarrerSession() {
    return this.isSessionExists() && (
      this.currentSession.status === <any>SessionAdmissionStatus.ADemarrer 
      || this.currentSession.status === <any>SessionAdmissionStatus.Suspendue);
  }

  public isEnableSuspendSession() {
    return this.isSessionExists() && this.currentSession.status === <any>SessionAdmissionStatus.EnCours;
  }

  public isEnableArreterSession() {
    return this.isSessionExists() && (
      this.currentSession.status === <any>SessionAdmissionStatus.EnCours 
      || this.currentSession.status === <any>SessionAdmissionStatus.Suspendue);
  }

  public isSessionExists() {
    return this.currentSession && this.currentSession.id !== Guid.EMPTY && this.currentSession.status !== <any>SessionAdmissionStatus.Terminee;
  }

  public isAnneeAcademiqueExists() {
    return this.currentAnneeAcademique && this.currentAnneeAcademique.id !== Guid.EMPTY;
  }

  public isSessionADemarrer() {
    return this.isSessionExists() && this.currentSession.status === <any>SessionAdmissionStatus.ADemarrer;
  }

  public isSessionEnCours() {
    return this.isSessionExists() && this.currentSession.status === <any>SessionAdmissionStatus.EnCours;
  }

  public isSessionSuspendue() {
    return this.isSessionExists() && this.currentSession.status === <any>SessionAdmissionStatus.Suspendue;
  }

  public isSessionANotifier() {
    return this.isSessionExists() && this.currentSession.status === <any>SessionAdmissionStatus.ANotifier;
  }

  public isSessionAExecuter() {
    return this.isSessionExists() && this.currentSession.status === <any>SessionAdmissionStatus.AExecuter;
  }

  public isSessionAConfirmer() {
    return this.isSessionExists() && this.currentSession.status === <any>SessionAdmissionStatus.AConfirmer;
  }

  public isSessionTerminee() {
    return this.isSessionExists() && this.currentSession.status === <any>SessionAdmissionStatus.Terminee;
  }

  modifyPlateformeInscriptionConfig() {
    if (this.plateformeInscriptionConfigDTO)
    {
      let message = 'Cette action va ' + this.getPlateformeInscriptionConfigButtonText() + ', voulez-vous continuer ?';
      this._dialogService.openConfirmDialog({
        data: {
          title: "Attention",
          message: message,
        }
      }).afterClosed().subscribe(confirmed => {
        if (confirmed) {
          this._store.dispatch(showLoading());
          if (this.plateformeInscriptionConfigDTO.isShown)
          {
            this.plateformeInscriptionConfigDTO.isShown = false;
          }
          else
          {
            this.plateformeInscriptionConfigDTO.isShown = true;
          }
          this._metfpetService.updatePlateformeInscriptionConfig(this.plateformeInscriptionConfigDTO)
            .subscribe(
              () => {
                this._store.dispatch(showSuccess({}));
              },
              (error) => this._store.dispatch(showError({ message: error.response }))
            )
            .add(() => {
              this._store.dispatch(hideLoading());
              this._cdRef.markForCheck();
            });
        }
      });
    }
  }

  getPlateformeInscriptionConfigButtonText() {
    if (this.plateformeInscriptionConfigDTO && this.plateformeInscriptionConfigDTO.isShown)
    {
      return 'Masquer l’inscription à la plateforme des apprenants';
    }
    return 'Afficher l’inscription à la plateforme des apprenants';
  }
}
