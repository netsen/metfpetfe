import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SessionAdmissionStatusValues } from 'MetfpetLib';

@Component({
  templateUrl: './update-session-status.dialog.html',
})
export class UpdateSessionStatusDialog {

  form: FormGroup;
  sessionAdmissionStatusValues: Array<any>;

  constructor(
    public dialogRef: MatDialogRef<UpdateSessionStatusDialog>, 
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _formBuilder: FormBuilder
  ) {
    this.sessionAdmissionStatusValues = SessionAdmissionStatusValues;
    this.form = this._formBuilder.group({
      'anneeAcademiqueName': [data.session.anneeAcademiqueName],
      'status': [data.session.status, Validators.required],
      'hideConcoursProgramme': [data.session.hideConcoursProgramme, Validators.required],
      'onlyShowConcoursPrivateProgramme': [data.session.onlyShowConcoursPrivateProgramme, Validators.required],
      'hideConcoursOrientationAccess': [data.session.hideConcoursOrientationAccess, Validators.required],
      'hideDossierOrientationAccess': [data.session.hideDossierOrientationAccess, Validators.required],
      'showSanteConcours': [data.session.showSanteConcours, Validators.required],
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public onConfirm(): void {
    if (this.form.valid) {
      this.dialogRef.close({
        status: this.form.get('status').value,
        hideConcoursProgramme: this.form.get('hideConcoursProgramme').value,
        onlyShowConcoursPrivateProgramme: this.form.get('onlyShowConcoursPrivateProgramme').value,
        hideConcoursOrientationAccess: this.form.get('hideConcoursOrientationAccess').value,
        hideDossierOrientationAccess: this.form.get('hideDossierOrientationAccess').value,
        showSanteConcours: this.form.get('showSanteConcours').value,
      });
    }
  }
}