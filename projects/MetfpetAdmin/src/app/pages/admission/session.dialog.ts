import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { dateLessThan, dateInFuture } from 'MetfpetLib';

@Component({
  templateUrl: './session.dialog.html',
})
export class SessionDialog {

  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<SessionDialog>, 
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _formBuilder: FormBuilder
  ) {

    let anneeAcademiqueName = data.session ? data.session.anneeAcademiqueName : null;
    let dateDebutAdmission = data.session ? data.session.dateDebutAdmission : null;
    let dateLimiteAdmission = data.session ? data.session.dateLimiteAdmission : null;
    let dateLimiteOffreAdmission = data.session ? data.session.dateLimiteOffreAdmission : null;

    this.form = this._formBuilder.group({
      'anneeAcademiqueName': [anneeAcademiqueName],
      'dateDebutAdmission': [dateDebutAdmission, Validators.required],
      'dateLimiteAdmission': [dateLimiteAdmission, Validators.required],
      'dateLimiteOffreAdmission': [dateLimiteOffreAdmission, Validators.required],
    }, { 
      validator: Validators.compose([
        dateLessThan('dateDebutAdmission', 'dateLimiteAdmission'),
        dateLessThan('dateDebutAdmission', 'dateLimiteOffreAdmission'),
        dateInFuture('dateLimiteAdmission'),
        dateInFuture('dateLimiteOffreAdmission')
      ])
    });
  }

  public onClose(): void {
    this.dialogRef.close();
  }

  public onConfirm(): void {
    if (this.form.valid) {
      this.dialogRef.close({
        anneeAcademiqueName: this.form.get('anneeAcademiqueName').value,
        dateDebutAdmission: this.form.get('dateDebutAdmission').value,
        dateLimiteAdmission: this.form.get('dateLimiteAdmission').value,
        dateLimiteOffreAdmission: this.form.get('dateLimiteOffreAdmission').value,
      });
    }
  }
}