import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import { Guid } from 'guid-typescript';

@Component({
  selector: 'app-agrement-type-authorization.dialog',
  templateUrl: './agrement-type-authorization.dialog.html',
  styleUrls: ['./agrement-type-authorization.dialog.css']
})
export class AgrementTypeAuthorizationDialog {

  title: string;
  form: FormGroup;
  employeId: string;
  model: MetfpetServiceAgent.TypeDemandeAuthorizationListDTO;
  typeDemandeList : Array<MetfpetServiceAgent.TypeDemandeViewModel>;
  error: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<AgrementTypeAuthorizationDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.title = 'Gérer les types d’agréments autorisés';
    this.typeDemandeList = data.typeDemandeList;
    this.employeId = data.employeId;
    this.model = data.typeDemandeAuthorizationList;
    this.form = this._formBuilder.group({
      agrementationId: data.agrementationId,
      typeDemandeAuthorizations: this._formBuilder.array([]),
    });
  }

  ngOnInit() {
    if (this.model.typeDemandeAuthorizationDtos) {
      for (var typeDemandeAuthorization of this.model.typeDemandeAuthorizationDtos) {
        this.typeDemandeAuthorizations.push(this._formBuilder.group({
          id: typeDemandeAuthorization.id,
          typeDemandeId: typeDemandeAuthorization.typeDemandeId,
        }));
      }
    } else {
        this._metfpetService.getTypeDemandeAuthorizationsOfEmployeMinisteriel(this.employeId).subscribe((data)=>{
          this.model = data;
          this._cd.markForCheck();
        });
    }
    this._cd.markForCheck();
  }

  get typeDemandeAuthorizations() {
    return this.form.controls['typeDemandeAuthorizations'] as FormArray;
  }

  add() {
    this.typeDemandeAuthorizations.push(this._formBuilder.group({
      id: Guid.EMPTY,
      typeDemandeId: null
    }));
    this._cd.markForCheck();
  }

  delete(i: number) {
    this.typeDemandeAuthorizations.removeAt(i);
    this._cd.markForCheck();
  }

  onConfirm() {
    this._validateTypeDemandeAuthorizations();
    this.error = null;
    validateForm(this.form);
    if (this.form.valid && this._validateTypeDemandeAuthorizations()) {
      this._store.dispatch(showLoading());
      this.model = Object.assign({}, this.model, this.form.value, {
        employeMinisterielId: this.employeId
      });
      this.model.typeDemandeAuthorizationDtos = this.typeDemandeAuthorizations.value
        .filter(x => !!x.typeDemandeId)
        .map(x => MetfpetServiceAgent.TypeDemandeAuthorizationDTO.fromJS(x));
      
      this._metfpetService.updateTypeDemandeAuthorizationList(MetfpetServiceAgent.TypeDemandeAuthorizationListDTO.fromJS(this.model))
        .subscribe(
              () => {
                this._store.dispatch(showSuccess({}));
                this._dialogRef.close(true);
              },
              error => this._store.dispatch(showException({error: error}))
            )
            .add(() => this._store.dispatch(hideLoading()));
    }
  }

  private _validateTypeDemandeAuthorizations() {
    var choices1 = this.typeDemandeAuthorizations.value.filter(x => !x.typeDemandeId);
    if (choices1.length > 0) {
      this.error = 'Certains champs obligatoires manquent';
    }
    var choices2 = this.typeDemandeAuthorizations.value.filter(x => !!x.typeDemandeId).map(x => x.typeDemandeId);
    if (new Set(choices2).size !== choices2.length) {
      this.error = 'Type de demande sont dupliqués';
    }
    return !this.error;
  }
  
  onClose() {
    this._dialogRef.close();
  }

}
