import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { UsersTabComponent } from './users-tab.component';
import { MinistryUsersComponent } from './ministry-users.component';
import { MinistryUserComponent } from './ministry-user.component';
import { InstitutionUsersComponent } from './institution-users.component';
import { InstitutionUserComponent } from './institution-user.component';
import { RolesComponent } from './roles/roles.component';
import { RolesDialogComponent } from './roles/roles-dialog.component';
import { AgrementTypeAuthorizationDialog } from './agrement-type-authorization.dialog';
import { AgrementationAuthorizationDialog } from './agrementation-authorization.dialog';

export const routes = [
  { 
    path: '', 
    component: UsersTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'ministry',
          pathMatch: 'full'
        },
        { 
          path: 'ministry', 
          component: MinistryUsersComponent, 
          data: { breadcrumb: 'Personnel du ministère' }  
        },
        { 
          path: 'ministry/view/:id', 
          component: MinistryUserComponent, 
          data: { breadcrumb: 'Consultation' }  
        },
        { 
          path: 'ministry/new', 
          component: MinistryUserComponent, 
          data: { breadcrumb: 'Nouveau' }  
        },
        { 
          path: 'institution', 
          component: InstitutionUsersComponent, 
          data: { breadcrumb: 'Personnel des institutions' }  
        },
        { 
          path: 'institution/view/:id', 
          component: InstitutionUserComponent, 
          data: { breadcrumb: 'Consultation' }  
        },
        { 
          path: 'institution/new', 
          component: InstitutionUserComponent, 
          data: { breadcrumb: 'Nouveau' }  
        },
        {
          path: 'role',
          component: RolesComponent,
          data: { breadcrum: 'Role' }
        },
      ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    UsersTabComponent,
    MinistryUsersComponent,
    MinistryUserComponent,
    InstitutionUsersComponent,
    InstitutionUserComponent,
    RolesComponent,
    RolesDialogComponent,
    AgrementTypeAuthorizationDialog,
    AgrementationAuthorizationDialog,
  ]
})
export class AdminUsersModule {}
