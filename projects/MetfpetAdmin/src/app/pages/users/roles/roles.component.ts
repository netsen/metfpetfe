import {
  ChangeDetectorRef,
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  MinistryEmployeeAccessMap,
  MetfpetServiceAgent,
  PerfectScrollService,
  MinistryEmployeeAccessValues,
  BiometricStatusValues,
  AuthService,
} from 'MetfpetLib';
import { Tabs } from '../users-tab.component';
import { RolesDialogComponent } from './roles-dialog.component';

@Component({
  templateUrl: './roles.component.html',
})
export class RolesComponent extends BaseTableComponent{

  settings: Settings;
  title: string;
  navs = Tabs;
  loading: boolean;
  error: string;
  dialogRef: any;
  form: FormGroup;
  private _model: MetfpetServiceAgent.EmployeRoleDTO
  accessMap = MinistryEmployeeAccessMap;
  accessList = MinistryEmployeeAccessValues;
  biometricList = BiometricStatusValues;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _route: ActivatedRoute,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _matDialog: MatDialog,
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Roles';
    this.sort = { prop: 'name', dir: 'desc' };
  }

  ngOnInit() {
    this.triggerSearch();
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null,
      isActive: true,
    });
  }

  protected _search(criteria: any): Observable<any> {
    criteria.filters = {}
    return this._metfpetService.getEmployeRoleListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  openDialogAdd() {
    var dialogRef = this._matDialog.open(RolesDialogComponent,
      {
        data: {}
      })
      .afterClosed().subscribe((saveSuccess) => {
        if (saveSuccess) {
          this.triggerSearch()
        }
      })
  }

  open(id: string) {
    var dialogref = this._matDialog.open(RolesDialogComponent,
      {
        data: { id: id }
      })
      .afterClosed().subscribe((saveSuccess) => {
        if (saveSuccess) {
          this.triggerSearch()
        }
      })
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }
}
