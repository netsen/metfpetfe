import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { 
  AuthService, 
  validateForm,
  showSuccess,
  MetfpetServiceAgent, 
  showLoading,
  showException,
  hideLoading,
} from 'MetfpetLib';
import { EmployeRoleStatus } from 'projects/MetfpetLib/src/public-api';
import { Observable } from 'rxjs';
@Component({
  templateUrl: './roles-dialog.component.html',
})
export class RolesDialogComponent implements OnInit {

  form: FormGroup;
  title: string; 
  error: string;
  loading: boolean;
  id: string;
  isCreation: boolean;
  private _model : MetfpetServiceAgent.EmployeRoleDTO 

  constructor(
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _matDialogRef: MatDialogRef<RolesDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.form = this._formBuilder.group({
      'name' : [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50)])],
      'status' : [null],
    })

    if (data.id) {
      this.isCreation = false;
      this.title = 'Modifier un role';
      this.id = this.data.id 
      this._metfpetService.getEmployRole(this.id).subscribe((data)=>{
        this._model = data;
        this.form.patchValue({
          name: this._model.name,
          status: this._model.status === <any>EmployeRoleStatus.Active
        })
      })
    }else{
      this.isCreation = true;
      this.title = 'Créer un rôle';
    }
   }

  ngOnInit(): void {
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading())
      var saveRole$ : Observable<MetfpetServiceAgent.EmployeRoleDTO>
      if(this.isCreation){
        saveRole$ = this._metfpetService.createEmployeRole(MetfpetServiceAgent.EmployeRoleDTO.fromJS(
          Object.assign( 
            this.form.value,
            {status: this.form.get('status').value ? EmployeRoleStatus.Active : EmployeRoleStatus.Inactive})
        ))} else{
          saveRole$ = this._metfpetService.updateEmployeRole(MetfpetServiceAgent.EmployeRoleDTO.fromJS(
            Object.assign(
              this._model,
              this.form.value,
              {status: this.form.get('status').value? EmployeRoleStatus.Active : EmployeRoleStatus.Inactive})
          ))}

        saveRole$.subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._matDialogRef.close(true)
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => {
          this._store.dispatch(hideLoading())
        });
    }
  }

  onClose(){
    this._matDialogRef.close()
  }

}
