import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

export const Tabs = [
  { 
    routerLink: '/' + UiPath.admin.users.ministry.list, 
    label: 'Personnel du ministère' 
  },
  {
    routerLink: '/' + UiPath.admin.users.role.list,
    label: 'Roles'
  },
  {
    routerLink: '/' + UiPath.admin.users.institution.list, 
    label: 'Personnel des institutions' 
  },
  
];

@Component({
  templateUrl: './users-tab.component.html',
})
export class UsersTabComponent {

  navs = Tabs;
  title: string;
  routerEventSubscription: Subscription;
  
  constructor(private router: Router) {
    this.title = 'Personnel du ministère';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = selectedTab.label;
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
