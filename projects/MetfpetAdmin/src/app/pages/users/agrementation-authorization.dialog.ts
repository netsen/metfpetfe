import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import { Guid } from 'guid-typescript';

@Component({
  selector: 'app-agrementation-authorization.dialog',
  templateUrl: './agrementation-authorization.dialog.html',
  styleUrls: ['./agrementation-authorization.dialog.css']
})
export class AgrementationAuthorizationDialog {
  title: string;
  form: FormGroup;
  typeDemandeAuthorizationId: string;
  typeDemandeId: string;
  model: MetfpetServiceAgent.AgrementationAuthorizationListDTO;
  agrementationList : Array<MetfpetServiceAgent.AgrementationViewModel>;
  typeDemandeName: string;
  error: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<AgrementationAuthorizationDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.title = 'Agréments autorisés';
    this.typeDemandeId = data.typeDemandeId;
    this.typeDemandeAuthorizationId = data.typeDemandeAuthorizationId;
    this.form = this._formBuilder.group({
      typeDemandeAuthorizationId: data.typeDemandeAuthorizationId,
      allowAll: null,
      agrementationAuthorizations: this._formBuilder.array([]),
    });
  }

  ngOnInit() {
    if (this.typeDemandeAuthorizationId) {
      this._metfpetService.getAgrementationListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {
          'typeDemandeId': this.typeDemandeId
        }
      })).subscribe((data)=>{
        this.agrementationList = data.results;
        this._cd.markForCheck();
      });
      this._metfpetService.getAgrementationsOfTypeDemandeAuthorization(this.typeDemandeAuthorizationId).subscribe((data)=>{
        this.model = data;
        for (var agrementationAuthorization of data.agrementationAuthorizations) {
          this.agrementationAuthorizations.push(this._formBuilder.group({
            id: agrementationAuthorization.id,
            agrementationId: agrementationAuthorization.agrementationId
          }));
        }
        this.form.patchValue({allowAll: data.allowAll});
        this.typeDemandeName = data.typeDemandeName;
        this._cd.markForCheck();
      });
    }
  }

  get agrementationAuthorizations() {
    return this.form.controls['agrementationAuthorizations'] as FormArray;
  }

  add() {
    this.agrementationAuthorizations.push(this._formBuilder.group({
      id: Guid.EMPTY,
      agrementationId: null
    }));
    this._cd.markForCheck();
  }

  delete(i: number) {
    this.agrementationAuthorizations.removeAt(i);
    this._cd.markForCheck();
  }

  onConfirm() {
    this._validateAgrementationAuthorization();
    this.error = null;
    validateForm(this.form);
    if (this.form.valid && this._validateAgrementationAuthorization()) {
      this._store.dispatch(showLoading());
      this.model = Object.assign({}, this.model, this.form.value);
      this.model.agrementationAuthorizations = this.agrementationAuthorizations.value
        .filter(x => !!x.agrementationId)
        .map(x => MetfpetServiceAgent.AgrementationAuthorizationDTO.fromJS(x));
      
      this._metfpetService.updateAgrementationAuthorizationList(MetfpetServiceAgent.AgrementationAuthorizationListDTO.fromJS(this.model))
        .subscribe(
              () => {
                this._store.dispatch(showSuccess({}));
                this._dialogRef.close(true);
              },
              error => this._store.dispatch(showException({error: error}))
            )
            .add(() => this._store.dispatch(hideLoading()));
    }
  }

  private _validateAgrementationAuthorization() {
    var choices1 = this.agrementationAuthorizations.value.filter(x => !x.agrementationId);
    if (choices1.length > 0) {
      this.error = 'Certains champs obligatoires manquent';
    }
    var choices2 = this.agrementationAuthorizations.value.filter(x => !!x.agrementationId).map(x => x.agrementationId);
    if (new Set(choices2).size !== choices2.length) {
      this.error = 'Les agréments sont dupliqués';
    }
    return !this.error;
  }
  
  onClose() {
    this._dialogRef.close();
  }
}
