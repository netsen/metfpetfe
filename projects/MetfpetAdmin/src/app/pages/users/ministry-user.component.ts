import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray} from '@angular/forms';
import { Observable } from 'rxjs';
import { 
  AppSettings,
  Settings,
  RestrictedMinistryEmployeeAccessValues,
  emailValidator,
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  DialogService,
  AuthService,
  MinistryEmployeeAccessValues,
  EmployeeAccess,
  selectLoggedInPerson,
  AuthorityEnum,
  MinistryEmployeeAccessValuesWithoutSuperAdmin,
} from 'MetfpetLib';
import { Store, select } from '@ngrx/store';
import { UiPath } from '../ui-path';
import { MatDialog } from '@angular/material/dialog';
import { AgrementTypeAuthorizationDialog } from './agrement-type-authorization.dialog';
import { AgrementationAuthorizationDialog } from './agrementation-authorization.dialog';
import { selectCurrentDemandeur } from 'projects/MetfpetDemandeur/src/app/shared/store/selectors';

@Component({
  templateUrl: './ministry-user.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MinistryUserComponent implements OnInit {

  form: FormGroup;
  settings: Settings;
  user: any;
  autorityEnum: any;
  title: string;
  isCreation: boolean = true;
  accessList = RestrictedMinistryEmployeeAccessValues;
  documentList: Array<MetfpetServiceAgent.DocumentViewModel>;
  employeRoles:Array<MetfpetServiceAgent.EmployeRoleDTO>;
  allowInserjeuneAccount: boolean;
  public _model: MetfpetServiceAgent.EmployeMinisterielProfileViewModel;
  typeDemandes : Array<MetfpetServiceAgent.TypeDemandeViewModel>;
  typeDemandeAuthorizationList: MetfpetServiceAgent.TypeDemandeAuthorizationListDTO;

  constructor(
    public appSettings: AppSettings,
    private _cd: ChangeDetectorRef,
    private _fb: FormBuilder,
    private _location: Location,
    private _router: Router,
    private _route: ActivatedRoute, 
    private _store: Store<any>,
    private _dialog: MatDialog,
    private _dialogService: DialogService,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings;
    this.autorityEnum = AuthorityEnum;
    if (this._authService.getUserType() == 'administrateurMinisteriel') 
    {
      this.accessList = MinistryEmployeeAccessValues;
    }
    else if (this._authService.getUserType() == 'administrateur')
    {
      this.accessList = MinistryEmployeeAccessValuesWithoutSuperAdmin;
    }
    this.form = this._fb.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'firstName': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'email': [null, Validators.compose([Validators.required, emailValidator])],
      'phone': [null, Validators.compose([Validators.pattern("^[0-9]{9}$")])],
      'identifiant': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'matricule': [null, Validators.compose([Validators.minLength(3)])],
      'categorie': [null, Validators.compose([Validators.minLength(3)])],
      'service': [null, Validators.compose([Validators.minLength(3)])],
      'fonction': [null, Validators.compose([Validators.minLength(3)])],
      'hierarchie': [null, Validators.compose([Validators.minLength(3)])],
	    'typePersonnel': [null, Validators.compose([Validators.minLength(3)])],
      'acces': [null, Validators.compose([Validators.required])],
      'isActive': [null],
      'documentIds' : [null],
      'employeRoleId' : [null],
      'hasInserjeuneAccount' : [false]
    });
  }

  ngOnInit() {
    this._store.pipe(select(selectLoggedInPerson)).subscribe((data) => {
      this.user = data;
    })

    this._metfpetService.getTypeDemandeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe((data)=>{
      this.typeDemandes = data.results;
    });

    this._route.params.subscribe(params => {
      let id = params['id'];
      this.isCreation = !id;
      
      if(!this.isCreation) {
        this._metfpetService.getEmployeMinisterielProfile(id).subscribe(data => {
          this._model = data;
          this.title = this._model.firstName + ' ' + this._model.name;
          this.form.controls['name'].setValue(this._model.name);
          this.form.controls['firstName'].setValue(this._model.firstName);
          this.form.controls['email'].setValue(this._model.email);
          this.form.controls['phone'].setValue(this._model.phone);
          this.form.controls['identifiant'].setValue(this._model.identifiant);
          this.form.controls['matricule'].setValue(this._model.matricule);
          this.form.controls['categorie'].setValue(this._model.categorie);
          this.form.controls['service'].setValue(this._model.service);
          this.form.controls['fonction'].setValue(this._model.fonction);
          this.form.controls['hierarchie'].setValue(this._model.hierarchie);
          this.form.controls['typePersonnel'].setValue(this._model.typePersonnel);
          this.form.controls['acces'].setValue(this._model.acces);
          this.form.controls['isActive'].setValue(this._model.isActive);
          this.form.controls['documentIds'].setValue(this._model.documents.map(x => x.id)); 
          this.form.controls['employeRoleId'].setValue(this._model.employeRoleId);
          this.form.controls['hasInserjeuneAccount'].setValue(this._model.hasInserjeuneAccount);
          if(!data.photoPath){
            this._model.photoPath = "/assets/img/users/default-user.jpg";
          }
          this.allowInserjeuneAccount = this._model.acces === <any>EmployeeAccess.Administrator ||
                                        this._model.acces === <any>EmployeeAccess.Supervisor;
          this.loadTypeDemandeAuthorizationList(this._model.id);
          this._cd.markForCheck();
        });
      }
    });

    
  }

  ngAfterViewInit() {
    this._metfpetService.getDocumentListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {'isActive':'1'}
    })).subscribe(data => {
      this.documentList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getEmployeRoleListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {'isActive':'1'}
   })).subscribe(data => {
      this.employeRoles = data.results;
      this._cd.markForCheck();
   })
  }

  public resetPassword(): void {
    if(!this.isCreation && this._model) {
      this._store.dispatch(showLoading());
      this._metfpetService.resetEmployeMinisterielPassword(this._model.id)
      .subscribe(
        success => {
          if (success) {
            this._store.dispatch(showSuccess({message: 'Le mot de passe a été réinitialisé'}));
          } else {
            this._store.dispatch(showError({message: 'Échec de la réinitialisation du mot de passe'}));
          }
        },
        (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveUser$: Observable<any>;

      if (this.isCreation) {
        saveUser$= this._metfpetService.registerEmployeMinisteriel(MetfpetServiceAgent.RegisterEmployeMinisterielViewModel.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value,
            { documents: this.form.controls['documentIds'].value.map(x => {return {id: x}})})
        ));
      } else {
        saveUser$ = this._metfpetService.updateEmployeMinisterielProfile(MetfpetServiceAgent.EmployeMinisterielProfileViewModel.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value,
            { documents: this.form.controls['documentIds'].value.map(x => {return {id: x}})})
        ));
      }

      saveUser$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([UiPath.admin.users.ministry.list]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public onFileUpload(event) {
    if (event.target.files && event.target.files.length > 0) {
      this._store.dispatch(showLoading());

      let file : Blob = event.target.files[0];
      var fileReader = new FileReader();
      fileReader.onloadend = function () {
        
      }
      fileReader.readAsBinaryString(file);
      
      let fileParameter = 
      {
        data : file,
        fileName : event.target.fileName
      };
      
      this._metfpetService.uploadEmployeMinisterielPhoto(fileParameter, this._model.id)
        .subscribe(
          (result) => {
            this._model.photoPath = result;
            this._cd.markForCheck();
            this._store.dispatch(showSuccess({}));
          },
          (error) => this._store.dispatch(showError({message: error.message}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onAccesChange() {
    this.allowInserjeuneAccount = this.form.get('acces').value === <any>EmployeeAccess.Administrator ||
                                  this.form.get('acces').value === <any>EmployeeAccess.Supervisor;
    this._cd.markForCheck();
  }

  public openFile(){
    document.getElementById('photoSelector').click();
  }

  goBack() {
    this._location.back();
  }

  loadTypeDemandeAuthorizationList(employeId: string) {
    this._metfpetService.getTypeDemandeAuthorizationsOfEmployeMinisteriel(employeId).subscribe((data)=>{
      this.typeDemandeAuthorizationList = data;
      this._cd.markForCheck();
    });
  }

  addAgrementationAuthorization(typeDemandeAuthorizationId: string, typeDemandeId: string) {
    this._dialog.open(AgrementationAuthorizationDialog, {
      width: '700px',
      data: {
        typeDemandeAuthorizationId: typeDemandeAuthorizationId,
        typeDemandeId: typeDemandeId
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.loadTypeDemandeAuthorizationList(this._model.id);
      }
    });
  }

  deleteAgrementationAuthorization(id: string) {
    this._dialogService.openConfirmDialog({
      width: '430px',
      data: {
        title: "Attention",
        message: "Voulez-vous définitivement supprimer ce type d’agrément ?",
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteTypeDemandeAuthorization(id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({ message: 'Le demande a été supprimée' }));
              this.loadTypeDemandeAuthorizationList(this._model.id);
            },
            (error) => this._store.dispatch(showError({ message: error.response }))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }
  
  addAgrementType() {
    this._dialog.open(AgrementTypeAuthorizationDialog, {
      width: '700px',
      data: {
        employeId: this._model.id,
        typeDemandeList: this.typeDemandes,
        typeDemandeAuthorizationList: this.typeDemandeAuthorizationList
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.loadTypeDemandeAuthorizationList(this._model.id);
      }
    });
  }

}