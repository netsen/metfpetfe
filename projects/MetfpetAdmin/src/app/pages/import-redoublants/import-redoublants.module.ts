import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ImportRedoublantsDialog } from './import-redoublants.dialog';
import { ImportRedoublantsTabComponent } from './import-redoublants-tab.component';
import { ImportRedoublantsComponent } from './import-redoublants.component';

export const routes = [
  {
    path: '', 
    component: ImportRedoublantsTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'redoublants',
          pathMatch: 'full'
        },
        { 
          path: 'redoublants', 
          component: ImportRedoublantsComponent, 
          data: { breadcrumb: 'Importer les redoublants ou admis apprenants' }
        },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [
    SharedModule,
    ImportRedoublantsComponent,
  ],
  declarations: [
    ImportRedoublantsTabComponent,
    ImportRedoublantsComponent,
    ImportRedoublantsDialog,
  ], 
  entryComponents: [
    ImportRedoublantsDialog
  ]
})
export class ImportRedoublantsModule {

}
