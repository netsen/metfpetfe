import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Input,
  ViewEncapsulation 
} from '@angular/core';
import {formatDate} from '@angular/common';
import { UserIdleService } from 'angular-user-idle'; 
import { 
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  EmployeeAccess,
  ExportService
} from 'MetfpetLib';
import * as TSXLSX from 'ts-xlsx';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { ImportInstitutionUserDialog } from './import-institution-user.dialog';
import { Guid } from 'guid-typescript';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

export interface InstitutionUserImportResult {
  nbImported: number;
  failedUsers: Array<any>;
}

@Component({
  selector: 'app-institution-user-import',
  templateUrl: './import-institution-user.component.html',
  styleUrls: ['./import-institution-user.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportInstitutionUserComponent {

  imported: number;
  totalToimport: number;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  importingIndicator: boolean;
  showWarningMessage: boolean;

  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _exportService: ExportService
  ) {
    
  }

  ngAfterViewInit() {
    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      var all = new MetfpetServiceAgent.InstitutionRowViewModel();
      all.id = Guid.EMPTY;
      all.name = 'Toutes les institutions';
      this.institutionList.push(all);
      this._cd.markForCheck();
    });
  }

  onClickImportButton() {
    this._dialogService.openDialog(
      ImportInstitutionUserDialog,
      {
        width: '600px',
        data: {
          title: "Importation du personnel du institutions", 
          message: "Veuillez sélectionner l'institution puis indiquer l'emplacement du fichier à importer", 
          institutionList: this.institutionList
        }
      }
    ).afterClosed().subscribe(result => {
      if (result && result.file) {
        this.processFile(result.institutionId, result.file);
      }
    });
  }

  processFile(institutionId: string, file: Blob) {
    this.importingIndicator = true;
    this._userIdleService.stopWatching();

    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      var arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, {type:'binary',cellDates:true,dateNF:'dd/mm/yyyy'});
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      var rows = XLSX.utils.sheet_to_json(worksheet, {raw:false,dateNF:'dd/mm/yyyy'});
      
      this._importEmployeInstitution(rows, institutionId);
    }
    fileReader.readAsArrayBuffer(file);
  }

  private _importEmployeInstitution(rows: any, institutionId: string): void {
	  if (rows.length == 0) {
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.INFO, 
            message: 'Aucune donnée importée'
          }
        });
        return;
      }

      this.importEmployees(this._convertToEmployees(rows, institutionId), true);
  }
  
  private _getInstitution(row: any, institutionId: string) {
    if (institutionId === Guid.EMPTY) {
      let institution = this.institutionList.find(x => x.name === row['Nom de l\'institution']);
      if (institution) {
        return institution.id;
      }
    }
    return institutionId;
  }

  private _convertToEmployees(rows: any, institutionId: string): Array<MetfpetServiceAgent.ImportEmployeUniversitaireViewModel> {
    
    let employees =  new Array<MetfpetServiceAgent.ImportEmployeUniversitaireViewModel>();

    employees = rows.map(
      row => {
        return MetfpetServiceAgent.ImportEmployeUniversitaireViewModel.fromJS({
          'accessName'                : row['Accès'], 
          'name'                      : row['Nom'],
          'firstName'                 : row['Prénoms'],
          'email'                     : row['Email'], 
          'password'                  : row['Password'], 
          'phone'                     : row['Téléphone'],
          'matricule'                 : row['Matricule'], 
          'categorie'                 : row['Catégorie'],
          'service'                   : row['Service'], 
          'fonction'                  : row['Fonction'],
          'hierarchie'                : row['Hiérarchie'],
          'diplomePlusEleve'          : row['Diplôme le plus élevé'], 
          'specialite'                : row['Spécialité'], 
          'typeEcole'                 : row['Type d\'école'], 
          'prefectureName'            : row['Préfecture'],
          'institutionId'             : this._getInstitution(row, institutionId)
        });
      }
    );
    return employees;
  }
  
  async importEmployees(employeesToImport: Array<MetfpetServiceAgent.ImportEmployeUniversitaireViewModel>, allowRetry: boolean) {
    this.showWarningMessage = true;
    this.imported = 0;
    this.totalToimport = employeesToImport.length;
    let toRetryList = new Array<MetfpetServiceAgent.ImportEmployeUniversitaireViewModel>();

    for (let employee of employeesToImport) {  
      try {
        this.imported++;
        this._cd.markForCheck();
        await this._metfpetService.importEmployeUniversitaire(employee).toPromise();
      } catch(error) {
        employee.importErrorMessage = error.isApiException ? (JSON.parse(error.response).Message ? JSON.parse(error.response).Message : error.response) : error;
        toRetryList.push(employee);
      }
    }
    if (toRetryList.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: (this.imported - toRetryList.length) + " employé(e)(s) importé(e)(s) avec succès, " 
            + toRetryList.length + " en échec"
        }
      });

    } else {
      this._dialogService.openConfirmDialog({
        data: {
          title: "Attention", 
          message: (this.totalToimport - toRetryList.length) + " employé(e)s importé(e)s avec succès, " 
            + toRetryList.length + " en échec, reprendre les éléments en échec?"
        }
      })
      .afterClosed().subscribe(confirmed => {
        if (confirmed) {
          this.importEmployees(toRetryList, false);
        } else {
          this.exportFailed(toRetryList);
        }
      });
    }

    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    return {
      nbImported: this.imported,
      failedUsers: toRetryList
    };
  }

  exportFailed(toRetryList: Array<MetfpetServiceAgent.ImportEmployeUniversitaireViewModel>): void {
    if (toRetryList.length > 0) {
      let json : any[] = new Array();
      for (let failed of toRetryList) {
        json.push({
          Nom                : failed.name,
          Prénoms            : failed.firstName,
          Email              : failed.email,
          Téléphone          : failed.phone,
          Raison             : failed.importErrorMessage
        });
      }
      this.exportAsExcelFile(json, 'failed_results');
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'Échec': worksheet }, SheetNames: ['Échec'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_import_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

  downloadTemplate() {
    const headers = [
      { name: "Accès", value: "accessName" },
      { name: "Nom", value: "nom" },
      { name: "Prénoms", value: "prenom" },      
      { name: "Email", value: "email" },
      { name: "Password", value: "password" },
      { name: "Téléphone", value: "telephone" },
      { name: "Matricule", value: "matricule" },
      { name: "Catégorie", value: "categorie" },      
      { name: "Service", value: "service" },
      { name: "Fonction", value: "fonction" },
      { name: "Hiérarchie", value: "hierarchie" },
      { name: "Diplôme le plus élevé", value: "diplomePlusEleve" },
      { name: "Spécialité", value: "specialite" },
      { name: "Type d'école", value: "typeEcole" },
      { name: "Préfecture", value: "prefectureName" },
      { name: "Nom de l'institution", value: "institutionName" }
    ]
    const templateName = "InstitutionalStaffTemplate";
    this._exportService.downloadTemplate(templateName, headers);
  }
}