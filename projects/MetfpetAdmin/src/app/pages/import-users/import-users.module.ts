import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImportUsersTabComponent } from './import-users-tab.component';
import { ImportMinistryUserComponent } from './import-ministry-user.component';
import { ImportMinistryUserDialog } from './import-ministry-user.dialog';
import { ImportMinistryUsersComponent } from './import-ministry-users.component';
import { ImportInstitutionUserComponent } from './import-institution-user.component';
import { ImportInstitutionUserDialog } from './import-institution-user.dialog';
import { ImportInstitutionUsersComponent } from './import-institution-users.component';
import { SharedModule } from '../../shared/shared.module';

export const routes = [
  {
    path: '', 
    component: ImportUsersTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'ministry-users',
          pathMatch: 'full'
        },
        { 
          path: 'ministry-users', 
          component: ImportMinistryUsersComponent, 
          data: { breadcrumb: 'Importer personnels Ministère' }
        },
        { 
          path: 'institution-users', 
          component: ImportInstitutionUsersComponent, 
          data: { breadcrumb: 'Importer personnels Institution' }
        },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [
    SharedModule,
    ImportMinistryUserComponent,
    ImportInstitutionUserComponent,
  ],
  declarations: [
    ImportUsersTabComponent,
    ImportMinistryUserComponent,
    ImportMinistryUserDialog,
    ImportMinistryUsersComponent,
    ImportInstitutionUserComponent,
    ImportInstitutionUserDialog,
    ImportInstitutionUsersComponent,
  ], 
  entryComponents: [
    ImportMinistryUserDialog,
    ImportInstitutionUserDialog
  ]
})
export class ImportUsersModule {

}
