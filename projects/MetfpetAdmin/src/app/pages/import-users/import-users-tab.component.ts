import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './import-users-tab.component.html',
})
export class ImportUsersTabComponent {

  navs = [
    {
      routerLink: '/' + UiPath.admin.import.ministryUsers, 
      label: 'Importer personnels Ministère' 
    },
    {
      routerLink: '/' + UiPath.admin.import.institutionUsers, 
      label: 'Importer personnels Institution' 
    },
  ];

  public title: string;
  routerEventSubscription: Subscription;

  constructor(protected router: Router) {
    this.title = 'Import : importer personnels ministère';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = 'Import : ' + selectedTab.label.toLowerCase();
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
