import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppSettings, Settings } from 'MetfpetLib';
import { ImportUsersTabComponent } from './import-users-tab.component';

@Component({
  templateUrl: './import-ministry-users.component.html',
})
export class ImportMinistryUsersComponent extends ImportUsersTabComponent {

  public settings: Settings;

  constructor(
    public appSettings: AppSettings,
    protected router: Router
   ) {
   	super(router);
    this.settings = this.appSettings.settings;
    this.title = 'Importation du personnel du ministère';
  }

}
