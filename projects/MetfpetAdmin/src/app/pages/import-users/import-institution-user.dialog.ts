import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './import-institution-user.dialog.html',
})
export class ImportInstitutionUserDialog {

  form: FormGroup;
  fileMissing: boolean;

  constructor(
    public dialogRef: MatDialogRef<ImportInstitutionUserDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public fb: FormBuilder, 
  ) {
    this.form = this.fb.group({
      'institutionId': [null, Validators.required]
    });
  }

  onFileChange(event) {
    this.data.file = event.target.files[0];
    this.fileMissing = false;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onConfirm(): void {
    if (!this.data.file) {
      this.fileMissing = true;
      return;
    }
    if (this.form.valid) {
      this.data.institutionId = this.form.controls['institutionId'].value;
      this.dialogRef.close(this.data);
    }
  }
}