import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImportResultsTabComponent } from './import-results-tab.component';
import { SharedModule } from '../../shared/shared.module';
import { ImportAdmissionComponent } from './import-admission.component';
import { ImportExportAdmissionDialog } from './import-export-admission.dialog';
import { ImportApprenantAdmissionComponent } from './import-apprenant-admission.component';
import { ImportPasserellesComponent } from './import-passerelles.component';

export const routes = [
  {
    path: '', 
    component: ImportResultsTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'results-admission',
          pathMatch: 'full'
        },
        { 
          path: 'results-admission', 
          component: ImportAdmissionComponent, 
          data: { breadcrumb: 'Importer résultats Admission' }
        },
        { 
          path: 'apprenant-admission', 
          component: ImportApprenantAdmissionComponent, 
          data: { breadcrumb: 'Importer des apprenants et admissions' }
        },
        { 
          path: 'passerelles', 
          component: ImportPasserellesComponent, 
          data: { breadcrumb: 'Importer des passerelles' }
        },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [
    SharedModule,
    ImportAdmissionComponent,
  ],
  declarations: [
    ImportResultsTabComponent,
    ImportAdmissionComponent,
    ImportExportAdmissionDialog,
    ImportApprenantAdmissionComponent,
    ImportPasserellesComponent,
  ], 
  entryComponents: [
    ImportExportAdmissionDialog
  ]
})
export class ImportResultsModule {

}
