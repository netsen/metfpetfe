import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './import-results-tab.component.html',
})
export class ImportResultsTabComponent {

  navs = [
    {
      routerLink: '/' + UiPath.admin.import.importAdmission, 
      label: 'Importer résultats Admission' 
    },
    {
      routerLink: '/' + UiPath.admin.import.importApprenantAdmission, 
      label: 'Importer des apprenants et admissions' 
    },
    {
      routerLink: '/' + UiPath.admin.import.importPasserelles, 
      label: 'Importer des passerelles' 
    },
  ];

  public title: string;
  routerEventSubscription: Subscription;

  constructor(protected router: Router) {
    this.title = 'Import : importer résultats admission';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = 'Import : ' + selectedTab.label.toLowerCase();
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
