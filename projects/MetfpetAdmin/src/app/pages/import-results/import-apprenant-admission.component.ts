import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Input,
  ViewEncapsulation 
} from '@angular/core';
import { UserIdleService } from 'angular-user-idle'; 
import { 
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  Settings,
  AppSettings,
  ExportService,
} from 'MetfpetLib';
import {formatDate} from '@angular/common';
import * as TSXLSX from 'ts-xlsx';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { ImportResultsTabComponent } from './import-results-tab.component';
import { Router } from '@angular/router';
import { ImportExistingStudentDialog } from '../import-students/import-existing-student.dialog';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

export interface AdmissionImportResult {
  nbImported: number;
  failedAdmissions: Array<any>;
}

@Component({
  templateUrl: './import-apprenant-admission.component.html',
  styleUrls: ['./import-apprenant-admission.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportApprenantAdmissionComponent extends ImportResultsTabComponent {

  imported: number;
  totalToimport: number;
  importingIndicator: boolean;
  showWarningMessage: boolean;
  public settings: Settings;
  
  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    public appSettings: AppSettings,
    protected router: Router,
    private _exportService: ExportService
  ) {
    super(router);
    this.settings = this.appSettings.settings;
  }

  ngAfterViewInit() {
  }

  onClickImportButton() {
    this._dialogService.openDialog(
      ImportExistingStudentDialog,
      {
        width: '600px',
        data: {
          isImport: true,
          title: "Importation des apprenants et admissions", 
          message: "Veuillez indiquer l'emplacement du fichier", 
          confirmBtnText: "Importer"
        }
      }
    ).afterClosed().subscribe(result => {
      if (result && result.file) {
        this.processFile(result.file);
      }
    });
  }

  processFile(file: Blob) {
    this.importingIndicator = true;
    this._userIdleService.stopWatching();

    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      var arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, {type:'binary',cellDates:true,dateNF:'dd/mm/yyyy'});
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      var rows = XLSX.utils.sheet_to_json(worksheet, {raw:false,dateNF:'dd/mm/yyyy'});
      
      this._importResultsAdmission(rows);
    }
    fileReader.readAsArrayBuffer(file);
  }

  private _importResultsAdmission(rows: any): void {
    if (rows.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: 'Aucune donnée importée'
        }
      });
      return;
    }

    this.importEtudiantAndAdmissions(this._convertToEtudiantAdmissions(rows));
  }

  private _mapGenre(sexe: string): string {
    if (sexe === 'M') {
      return 'Homme';
    } else if (sexe === 'F') {
      return 'Femme';
    } else {
      return sexe;
    }
  }

  private formatDateNaissance(dateNaissance: any): string {
    if (dateNaissance instanceof Date) {
      return formatDate(dateNaissance, 'yyyy-MM-dd', 'en-US');
		}
    const [day, month, year] = dateNaissance.split('/');
    return [year, month, day].join('-');
  }

  private _convertToEtudiantAdmissions(rows: any): Array<MetfpetServiceAgent.ImportEtudiantAndAdmissionViewModel> {
    
    let admissions =  new Array<MetfpetServiceAgent.ImportEtudiantAndAdmissionViewModel>();

    admissions = rows.map(
      row => {  
        return MetfpetServiceAgent.ImportEtudiantAndAdmissionViewModel.fromJS({
          'institution'                    : row['Nom de l\'institution'], 
          'firstName'                      : row['Prénom'], 
          'name'                           : row['Nom'], 
          'genre'                          : this._mapGenre(row['Sexe']), 
          'dateNaissance'                  : this.formatDateNaissance(row['Date de naissance']), 
          'lieuNaissance'                  : row['Lieu de naissance'], 
          'nomPere'                        : row['Nom du père'], 
          'nomMere'                        : row['Nom de la mère'], 
          'email'                          : row['Email'],
          'phone'                          : row['Numéro de téléphone'], 
          'session'                        : row['Session'],
          'programme'                      : row['Programme'], 
        });
      }
    );
    return admissions;
  }
  
  async importEtudiantAndAdmissions(admissionsToImport: Array<MetfpetServiceAgent.ImportEtudiantAndAdmissionViewModel>) {
    this.showWarningMessage = true;
    this.imported = 0;
    this.totalToimport = admissionsToImport.length;
    let exportList = new Array<MetfpetServiceAgent.ImportEtudiantAndAdmissionViewModel>();

    for (let admission of admissionsToImport) {    
      try {
        this.imported++;
        this._cd.markForCheck();
        let result = await this._metfpetService.importEtudiantAndAdmission(admission).toPromise();
        exportList.push(result);
      } catch(error) {
        admission.importErrorMessage = error.isApiException ? (JSON.parse(error.response).Message ? JSON.parse(error.response).Message : error.response) : error;
        exportList.push(admission);
      }
    }
    this._dialogService.openInfoDialog({
      data: {
        severity: SeverityEnum.INFO, 
        message: (this.imported - exportList.length) + " apprenant(e)(s) importé(e)(s) avec succès, " 
          + exportList.length + " en échec"
      }
    }).afterClosed().subscribe(() => {
      this.exportItems(exportList);
    });
    
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    return {
      nbImported: this.imported,
      failedStudents: exportList
    };
  }

  exportItems(toRetryList: Array<MetfpetServiceAgent.ImportEtudiantAndAdmissionViewModel>): void {
    if (toRetryList.length > 0) {
      let json : any[] = new Array();
      for (let failed of toRetryList) {
        json.push({
          'INA'                        : failed.identifiantNationalEtudiant,
          'Nom de l\'institution'      : failed.institution,
          'Nom'                        : failed.name,
          'Prénom'                     : failed.firstName,
          'Sexe'                       : failed.genre, 
          'Date de naissance'          : failed.dateNaissance,
          'Lieu de naissance'          : failed.lieuNaissance,
          'Nom du père'                : failed.nomPere, 
          'Nom de la mère'             : failed.nomMere, 
          'Email'                      : failed.email,
          'Numéro de téléphone'        : failed.phone,
          'Session'                    : failed.session,
          'Programme'                  : failed.programme,
          'Raison'                     : failed.importErrorMessage
        });
      }
      this.exportAsExcelFile(json, 'export_results');
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'Result': worksheet }, SheetNames: ['Result'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_import_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

  downloadTemplate() {
    const headers = [
      { name: "Nom de l'institution", value: "nomInstitution" },
      { name: "Nom", value: "nom" },
      { name: "Prénoms", value: "prenom" },      
      { name: "Sexe", value: "genre" },
      { name: "Date Naissance", value: "dateNaissance" },
      { name: "Lieu de naissance", value: "lieuNaissance" },
      { name: "Nom du père", value: "nomPere" },
      { name: "Nom de la mère", value: "nomMere" },
      { name: "Email", value: "email" },
      { name: "Numéro de téléphone", value: "telephone" },
      { name: "Session", value: "session" },
      { name: "Programme", value: "programme" },
    ]
    const templateName = "ApprenantEtAdmissionTemplate";
    this._exportService.downloadTemplate(templateName, headers);
  }
}
