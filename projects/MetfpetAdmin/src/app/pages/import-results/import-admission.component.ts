import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Input,
  ViewEncapsulation 
} from '@angular/core';
import { UserIdleService } from 'angular-user-idle'; 
import { 
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  Settings,
  AppSettings,
  ExportService,
} from 'MetfpetLib';
import {formatDate} from '@angular/common';
import * as TSXLSX from 'ts-xlsx';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { ImportResultsTabComponent } from './import-results-tab.component';
import { ImportExportAdmissionDialog } from './import-export-admission.dialog';
import { Router } from '@angular/router';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

export interface AdmissionImportResult {
  nbImported: number;
  failedAdmissions: Array<any>;
}

@Component({
  templateUrl: './import-admission.component.html',
  styleUrls: ['./import-admission.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportAdmissionComponent extends ImportResultsTabComponent {

  sessionAdmissionList: Array<MetfpetServiceAgent.SessionAdmissionDTO>;
  imported: number;
  totalToimport: number;
  importingIndicator: boolean;
  showWarningMessage: boolean;
  public settings: Settings;
  
  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    public appSettings: AppSettings,
    protected router: Router,
    private _exportService: ExportService
  ) {
    super(router);
    this.settings = this.appSettings.settings;
  }

  ngAfterViewInit() {
    this._metfpetService.getAllSessionAdmissions().subscribe(data => {
      this.sessionAdmissionList = data;
      this._cd.markForCheck();
    });
  }

  onClickImportButton() {
    this._dialogService.openDialog(
      ImportExportAdmissionDialog,
      {
        width: '600px',
        data: {
          isImport: true,
          title: "Importation des résultats d’admission", 
          message: "Veuillez sélectionner la session puis indiquer l'emplacement du fichier", 
          confirmBtnText: "Importer",
          sessionAdmissionList: this.sessionAdmissionList
        }
      }
    ).afterClosed().subscribe(result => {
      if (result && result.file) {
        this.processFile(result.sessionId, result.file);
      }
    });
  }

  processFile(sessionId: string, file: Blob) {
    this.importingIndicator = true;
    this._userIdleService.stopWatching();

    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      var arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, {type:'binary',cellDates:true,dateNF:'dd/mm/yyyy'});
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      var rows = XLSX.utils.sheet_to_json(worksheet, {raw:false,dateNF:'dd/mm/yyyy'});
      
      this._importResultsAdmission(rows, sessionId);
    }
    fileReader.readAsArrayBuffer(file);
  }

  private _importResultsAdmission(rows: any, sessionId: string): void {
    if (rows.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: 'Aucune donnée importée'
        }
      });
      return;
    }

    this.importAdmissions(this._convertToAdmissions(rows, sessionId), true);
  }

  private _mapGenre(sexe: string): string {
    if (sexe === 'M') {
      return 'Homme';
    } else if (sexe === 'F') {
      return 'Femme';
    } else {
      return sexe;
    }
  }

  private formatDateNaissance(dateNaissance: any): string {
    if (dateNaissance instanceof Date) {
      return formatDate(dateNaissance, 'yyyy-MM-dd', 'en-US');
		}
    const [day, month, year] = dateNaissance.split('/');
    return [year, month, day].join('-');
  }

  private _convertToAdmissions(rows: any, sessionId: string): Array<MetfpetServiceAgent.ImportAdmissionViewModel> {
    
    let admissions =  new Array<MetfpetServiceAgent.ImportAdmissionViewModel>();

    admissions = rows.map(
      row => {  
        return MetfpetServiceAgent.ImportAdmissionViewModel.fromJS({
          'sessionId'                      : sessionId,
          'annee'                          : row['Année'], 
          'pv'                             : row['Numéro PV'], 
          'identifiantNationalEtudiant'    : row['Identifiant National Apprenant'], 
          'firstName'                      : row['Prénoms'], 
          'name'                           : row['Nom'], 
          'dateNaissance'                  : this.formatDateNaissance(row['Date Naissance']), 
          'genre'                          : this._mapGenre(row['Sexe']), 
          'centreExamen'                   : row['Centre Concours'],
          'institution'                    : row['Institution'], 
          'programme'                      : row['Programme'], 
          'niveau'                         : row['Niveau'], 
          'decision'                       : row['Décision'],  
        });
      }
    );
    return admissions;
  }
  
  async importAdmissions(admissionsToImport: Array<MetfpetServiceAgent.ImportAdmissionViewModel>, allowRetry: boolean) {
    this.showWarningMessage = true;
    this.imported = 0;
    this.totalToimport = admissionsToImport.length;
    let toRetryList = new Array<MetfpetServiceAgent.ImportAdmissionViewModel>();

    for (let admission of admissionsToImport) {    
      try {
        this.imported++;
        this._cd.markForCheck();
        let importSuccess = await this._metfpetService.importAdmission(admission).toPromise();
        if (!importSuccess) {
          toRetryList.push(admission);
        }
      } catch(error) {
        admission.importErrorMessage = error.isApiException ? (JSON.parse(error.response).Message ? JSON.parse(error.response).Message : error.response) : error;
        toRetryList.push(admission);
      }
    }
    if (toRetryList.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: (this.imported - toRetryList.length) + " admission(e)(s) importé(e)(s) avec succès, " 
            + toRetryList.length + " en échec"
        }
      });

    } else {
      if (allowRetry) {
        this._dialogService.openConfirmDialog({
          data: {
            title: "Attention", 
            message: (this.totalToimport - toRetryList.length) + " admission(e)s importé(e)s avec succès, " 
              + toRetryList.length + " en échec, reprendre les éléments en échec?"
          }
        })
        .afterClosed().subscribe(confirmed => {
          if (confirmed) {
            this.importAdmissions(toRetryList, false);
          } else {
            this.exportFailed(toRetryList);
          }
        });
      } else {
        this.exportFailed(toRetryList);
      }
    }

    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    return {
      nbImported: this.imported,
      failedStudents: toRetryList
    };
  }

  exportFailed(toRetryList: Array<MetfpetServiceAgent.ImportAdmissionViewModel>): void {
    if (toRetryList.length > 0) {
      let json : any[] = new Array();
      for (let failed of toRetryList) {
        json.push({
          Année                        : failed.annee, 
          PV                           : failed.pv,
          IdentifiantNationalApprenant : failed.identifiantNationalEtudiant,
          Prénoms                      : failed.firstName,
          Nom                          : failed.name,
          Institution                  : failed.institution,
          Programme                    : failed.programme, 
          Niveau                       : failed.niveau, 
          Decision                     : failed.decision,
          Raison                       : failed.importErrorMessage
        });
      }
      this.exportAsExcelFile(json, 'failed_results');
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'Échec': worksheet }, SheetNames: ['Échec'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_import_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

  downloadTemplate() {
    const headers = [
      { name: "Numéro PV", value: "numeroPV" },
      { name: "Identifiant National Apprenant", value: "ina" },
      { name: "Nom", value: "nom" },
      { name: "Prénoms", value: "prenom" },      
      { name: "Date Naissance", value: "dateNaissance" },
      { name: "Sexe", value: "genre" },
      { name: "Centre Concours", value: "centreConcours" },
      { name: "Institution", value: "institution" },
      { name: "Programme", value: "programme" },
      { name: "Niveau", value: "niveau" },
      { name: "Décision", value: "decision" },
    ]
    const templateName = "ResultatAdmissionTemplate";
    this._exportService.downloadTemplate(templateName, headers);
  }
}
