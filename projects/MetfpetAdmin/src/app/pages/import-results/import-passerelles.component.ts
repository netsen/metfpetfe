import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Input,
  ViewEncapsulation 
} from '@angular/core';
import { UserIdleService } from 'angular-user-idle'; 
import { 
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  Settings,
  AppSettings,
  ExportService
} from 'MetfpetLib';
import {formatDate} from '@angular/common';
import * as TSXLSX from 'ts-xlsx';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { ImportResultsTabComponent } from './import-results-tab.component';
import { ImportExportAdmissionDialog } from './import-export-admission.dialog';
import { Router } from '@angular/router';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

export interface AdmissionImportResult {
  nbImported: number;
  failedAdmissions: Array<any>;
}

@Component({
  templateUrl: './import-passerelles.component.html',
  styleUrls: ['./import-passerelles.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportPasserellesComponent extends ImportResultsTabComponent {

  sessionAdmissionList: Array<MetfpetServiceAgent.SessionAdmissionDTO>;
  imported: number;
  totalToimport: number;
  importingIndicator: boolean;
  showWarningMessage: boolean;
  public settings: Settings;
  
  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    public appSettings: AppSettings,
    protected router: Router,
    private _exportService: ExportService
  ) {
    super(router);
    this.settings = this.appSettings.settings;
  }

  ngAfterViewInit() {
    this._metfpetService.getAllSessionAdmissions().subscribe(data => {
      this.sessionAdmissionList = data;
      this._cd.markForCheck();
    });
  }

  onClickImportButton() {
    this._dialogService.openDialog(
      ImportExportAdmissionDialog,
      {
        width: '600px',
        data: {
          isImport: true,
          title: "Importation des passerelles", 
          message: "Veuillez sélectionner la session puis indiquer l'emplacement du fichier", 
          confirmBtnText: "Importer",
          sessionAdmissionList: this.sessionAdmissionList
        }
      }
    ).afterClosed().subscribe(result => {
      if (result && result.file) {
        this.processFile(result.sessionId, result.file);
      }
    });
  }

  processFile(sessionId: string, file: Blob) {
    this.importingIndicator = true;
    this._userIdleService.stopWatching();

    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      var arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, {type:'binary',cellDates:true,dateNF:'dd/mm/yyyy'});
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      var rows = XLSX.utils.sheet_to_json(worksheet, {raw:false,dateNF:'dd/mm/yyyy'});
      
      this._importPasserelles(rows, sessionId);
    }
    fileReader.readAsArrayBuffer(file);
  }

  private _importPasserelles(rows: any, sessionId: string): void {
    if (rows.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: 'Aucune donnée importée'
        }
      });
      return;
    }

    this.importPasserelles(this._convertToPasserelles(rows, sessionId), true);
  }

  private _convertToPasserelles(rows: any, sessionId: string): Array<MetfpetServiceAgent.ImportPasserelleViewModel> {
    
    let admissions =  new Array<MetfpetServiceAgent.ImportPasserelleViewModel>();

    admissions = rows.map(
      row => {  
        return MetfpetServiceAgent.ImportPasserelleViewModel.fromJS({
          'session'                        : sessionId, 
          'identifiantNationalEleve'       : row['Identifiant National Apprenant'], 
          'institution'                    : row['Institution'], 
          'programme'                      : row['Programme'], 
          'niveauEtude'                    : row['Niveau'], 
        });
      }
    );
    return admissions;
  }
  
  async importPasserelles(passerellesToImport: Array<MetfpetServiceAgent.ImportPasserelleViewModel>, allowRetry: boolean) {
    this.showWarningMessage = true;
    this.imported = 0;
    this.totalToimport = passerellesToImport.length;
    let toRetryList = new Array<MetfpetServiceAgent.ImportPasserelleViewModel>();

    for (let passerelle of passerellesToImport) {    
      try {
        this.imported++;
        this._cd.markForCheck();
        let importSuccess = await this._metfpetService.importPasserelle(passerelle).toPromise();
        if (!importSuccess) {
          toRetryList.push(passerelle);
        }
      } catch(error) {
        passerelle.importErrorMessage = error.isApiException ? (JSON.parse(error.response).Message ? JSON.parse(error.response).Message : error.response) : error;
        toRetryList.push(passerelle);
      }
    }
    if (toRetryList.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: (this.imported - toRetryList.length) + " passerelle(e)(s) importé(e)(s) avec succès, " 
            + toRetryList.length + " en échec"
        }
      });

    } else {
      if (allowRetry) {
        this._dialogService.openConfirmDialog({
          data: {
            title: "Attention", 
            message: (this.totalToimport - toRetryList.length) + " passerelle(e)s importé(e)s avec succès, " 
              + toRetryList.length + " en échec, reprendre les éléments en échec?"
          }
        })
        .afterClosed().subscribe(confirmed => {
          if (confirmed) {
            this.importPasserelles(toRetryList, false);
          } else {
            this.exportFailed(toRetryList);
          }
        });
      } else {
        this.exportFailed(toRetryList);
      }
    }

    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    return {
      nbImported: this.imported,
      failedStudents: toRetryList
    };
  }

  exportFailed(toRetryList: Array<MetfpetServiceAgent.ImportPasserelleViewModel>): void {
    if (toRetryList.length > 0) {
      let json : any[] = new Array();
      for (let failed of toRetryList) {
        json.push({
          'Identifiant National Apprenant' : failed.identifiantNationalEleve,
          'Institution'                    : failed.institution,
          'Programme'                      : failed.programme, 
          'Niveau'                         : failed.niveauEtude, 
          'Raison'                         : failed.importErrorMessage
        });
      }
      this.exportAsExcelFile(json, 'failed_results');
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'Échec': worksheet }, SheetNames: ['Échec'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_import_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

  downloadTemplate() {
    const headers = [      
      { name: "Identifiant National Apprenant", value: "identifiantNationalEleve" },
      { name: "Institution", value: "institution" },
      { name: "Programme", value: "programme" },      
      { name: "Niveau", value: "niveau" },      
    ]
    const templateName = "PasserelleTemplate";
    this._exportService.downloadTemplate(templateName, headers);
  }
}
