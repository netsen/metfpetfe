import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './import-export-admission.dialog.html',
})
export class ImportExportAdmissionDialog {

  form: FormGroup;
  fileMissing: boolean;

  constructor(
    public dialogRef: MatDialogRef<ImportExportAdmissionDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public fb: FormBuilder, 
  ) {
    this.form = this.fb.group({
      'sessionId': [null, Validators.required],
    });
  }

  onFileChange(event) {
    this.data.file = event.target.files[0];
    this.fileMissing = false;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onConfirm(): void {
    if (this.data.isImport && !this.data.file) {
      this.fileMissing = true;
      return;
    }
    if (this.form.valid) {
      this.data.sessionId = this.form.controls['sessionId'].value;
      this.dialogRef.close(this.data);
    }
  }
}