import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { 
  AppSettings,
  Settings,
  BaseTableComponent, 
  MetfpetServiceAgent,
  PerfectScrollService,
  AuthService,
} from 'MetfpetLib';
import { FraisInscriptionDialog } from './frais-inscription.dialog';


@Component({
  templateUrl: './frais-inscription.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FraisInscriptionComponent extends BaseTableComponent {

  settings: Settings;
  title: string;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialog: MatDialog,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Frais d’institutions';
    this.sort = {prop: 'name', dir: 'desc'};
  }

  ngAfterViewInit() {
    this.triggerSearch();
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getTarificationListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  open(row: MetfpetServiceAgent.TarificationRowViewModel) {
    this._dialog.open(FraisInscriptionDialog, {
      width: '540px',
      data: {
        tarificationId: row.id
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }
}