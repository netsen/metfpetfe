import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  OnInit, 
  ViewEncapsulation 
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import { UiPath } from '../ui-path';
import {
  AppSettings,
  Settings,
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  InstitutionStatus,
  showException,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  templateUrl: './institution.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstitutionComponent implements OnInit {

  settings: Settings;
  title: string;
  form: FormGroup;
  isCreation: boolean;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissement>;
  typeInstitutionList: Array<MetfpetServiceAgent.TypeInstitution>;

  public _model: MetfpetServiceAgent.InstitutionDTO;
  public hasFinalExamen: boolean;

  constructor(
    public appSettings: AppSettings, 
    private _activatedRoute: ActivatedRoute, 
    private _cd: ChangeDetectorRef,
    private _fb: FormBuilder, 
    private _location: Location,
    private _router:Router,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings;
    this.hasFinalExamen = false;
    this.form = this._fb.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'prefectureId': null,
      'description': null,
      'status': null,
      'typeInstitutionId': [null, Validators.required],
      'statusEtablissementId': [null, Validators.required],
      'inscriptionMontant': [null, Validators.pattern('^[0-9]*$')],
      'reinscriptionMontant': [null, Validators.pattern('^[0-9]*$')],
      'hasFinalExamen': false
    });
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      let id = params['id'];
      this.isCreation = !id;
      
      if (!this.isCreation) {
        this._metfpetService.getInstitution(id).subscribe((institution : MetfpetServiceAgent.InstitutionDTO) => {
          this._model = institution;
          this.title = this._model.name;
          this.hasFinalExamen = this._model.hasFinalExamen;

          this.form.patchValue({
            'name': this._model.name,
            'prefectureId': this._model.prefectureId,
            'description': this._model.description,
            'status': this._model.status === <any>InstitutionStatus.Active,
            'typeInstitutionId': this._model.typeInstitutionId,
            'statusEtablissementId': this._model.statusEtablissementId,
            'inscriptionMontant': this._model.inscriptionMontant,
            'reinscriptionMontant': this._model.reinscriptionMontant,
            'hasFinalExamen': this._model.hasFinalExamen
          });
          if(!this._model.logoPath){
            this._model.logoPath = "/assets/img/users/default-institution-logo.jpg";
          }
          this._cd.markForCheck();
        });
      }
      else{
        this._cd.markForCheck();
      }
    });
  }

  ngAfterViewInit() {
    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cd.markForCheck(); 
    });
    
    this._metfpetService.getStatusEtablissementList().subscribe(data => {
      this.statusEtablissementList = data; 
      this._cd.markForCheck();
    });
    
    this._metfpetService.getTypeInstitutionList().subscribe(data => {
      this.typeInstitutionList = data;
      this._cd.markForCheck();
    });
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveInstitution$: Observable<MetfpetServiceAgent.InstitutionDTO>;

      if (this.isCreation) {
        var newInstitution = MetfpetServiceAgent.InstitutionDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: InstitutionStatus.Active },
          )
        );
        saveInstitution$= this._metfpetService.createInstitution(newInstitution);
      } else {
        var existingInstitution = MetfpetServiceAgent.InstitutionDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: this.form.get('status').value ? InstitutionStatus.Active : InstitutionStatus.Inactive },
          )
        );
        saveInstitution$ = this._metfpetService.updateInstitution(existingInstitution);
      }

      saveInstitution$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([`${UiPath.admin.institutions.list}`]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public onFileUpload(event) {
    if (event.target.files && event.target.files.length > 0) {
      this._store.dispatch(showLoading());

      let file : Blob = event.target.files[0];
      var fileReader = new FileReader();
      fileReader.onloadend = function () {
        
      }
      fileReader.readAsBinaryString(file);
      
      let fileParameter = 
      {
        data : file,
        fileName : event.target.fileName
      };
      
      this._metfpetService.uploadInstitutionLogo(fileParameter, this._model.id)
        .subscribe(
          (result) => {
            this._model.logoPath = result;
            this._cd.markForCheck();
            this._store.dispatch(showSuccess({}));
          },
          (error) => this._store.dispatch(showError({message: error.message}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public uploadLogo(){
    document.getElementById('photoSelector').click();
  }

  public removeLogo() {
    this._store.dispatch(showLoading());      
    this._metfpetService.removeInstitutionLogo(this._model.id)
      .subscribe(
        () => {
          this._model.logoPath = "/assets/img/users/default-institution-logo.jpg";
          this._cd.markForCheck();
          this._store.dispatch(showSuccess({}));
        },
        (error) => this._store.dispatch(showError({message: error.message}))
      )
      .add(() => this._store.dispatch(hideLoading()));
  }

  goBack() {
    this._location.back();
  }

  get institutionId() {
    return this._model ? this._model.id : null;
  }

  onHasFinalExamenToggle(event: MatSlideToggleChange) {
    this.hasFinalExamen = event.checked;
    this._cd.markForCheck();
  }
}