import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component ({
  templateUrl: './competition-centers-export.dialog.html',
})
export class CompetitionCentersExportDialog {

  public form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<CompetitionCentersExportDialog>,
    private _formBuilder: FormBuilder
  ) {
    this.form = this._formBuilder.group({
      exportExcel: true,
      exportNom: true,
      exportNiveauAcces: true,
      exportRegion: true,
      exportPrefecture: true,
      exportStatut: true,
      exportProgramme: true,
    });
  }

  onConfirm(): void {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    }
  }

  onClose(): void {
    this.dialogRef.close(null);
  }
}