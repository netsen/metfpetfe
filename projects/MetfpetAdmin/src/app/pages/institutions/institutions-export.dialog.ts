import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component ({
  templateUrl: './institutions-export.dialog.html',
})
export class InstitutionsExportDialog {

  public form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<InstitutionsExportDialog>,
    private _formBuilder: FormBuilder
  ) {
    this.form = this._formBuilder.group({
      exportExcel: true,
      exportNom: true,
      exportDescription: true,
      exportPrefecture: true,
      exportTypeInstitution: true,
      exportStatusEtablissement: true,
      exportStatut: true,
    });
  }

  onConfirm(): void {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    }
  }

  onClose(): void {
    this.dialogRef.close(null);
  }
}