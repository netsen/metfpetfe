import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';

@Component({
  templateUrl: './frais-inscription.dialog.html',
})
export class FraisInscriptionDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.TarificationDTO;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<FraisInscriptionDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.form = this._formBuilder.group({
      montant: [null, Validators.required],
    });

    this.title = data.tarificationId ? 'Modifier un frais d’inscription' : 'Nouveau frais d’inscription';

    this._metfpetService.getTarification(data.tarificationId).subscribe(
      (tarification) => {
        this.model = tarification;
        this.form.patchValue({
          montant: this.model.montant,
        });
      }
    );
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.updateTarification(MetfpetServiceAgent.TarificationDTO.fromJS(
        Object.assign(this.model, this.form.value)))
        .subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}