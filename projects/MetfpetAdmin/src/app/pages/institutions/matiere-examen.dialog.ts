import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Guid } from 'guid-typescript';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  TypeExamenValues,
  TypeExamen,
  TypeNoteValues,
  TypeNote,
} from 'MetfpetLib';

@Component({
  templateUrl: './matiere-examen.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MatiereExamenDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.FinalExamenMatiereListDTO;
  typeExamenList: Array<any>;
  typeNoteList: Array<any>;
  error: string;
  isAPCType: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<MatiereExamenDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.typeExamenList = TypeExamenValues;
    this.typeNoteList = TypeNoteValues;
    this.isAPCType = true;
    this.form = this._formBuilder.group({
      typeExamen: [null, Validators.required],
      typeNote: [null],
      apcMoyenneTotalMin: [null],
      examenMatieres: this._formBuilder.array([]),
    });

    this.title = 'Matières de l’examen de sortie'; 

    this._metfpetService.getFinalExamenMatieres(data.programmeId).subscribe(
      (data) => {
        this.model = data;
        this.form.patchValue({
          typeExamen: this.model.typeExamen,
          typeNote: this.model.typeNote,
          apcMoyenneTotalMin: this.model.apcMoyenneTotalMin,
        });
        if (this.model.typeExamen === <any>TypeExamen.APC) {
          this.isAPCType = true;
          if (!this.model.typeNote)
          {
            this.form.controls['typeNote'].setValue(<any>TypeNote.Sur100);
          }
        }
        else 
        {
          this.isAPCType = false;
          if (!this.model.typeNote)
          {
            this.form.controls['typeNote'].setValue(<any>TypeNote.Sur20);
          }
        }
        if (this.model.finalExamenMatieres) 
        {
          for (let finalExamenMatiere of this.model.finalExamenMatieres) {
            this.examenMatieres.push(this._formBuilder.group({
              id: finalExamenMatiere.id,
              name: finalExamenMatiere.name,
              coefficient: finalExamenMatiere.coefficient
            }));
          }
        }
        this._cd.markForCheck();
      }
    );
  }

  get examenMatieres() {
    return this.form.controls['examenMatieres'] as FormArray;
  }

  addExamenMatiere() {
    this.examenMatieres.push(this._formBuilder.group({
      id: Guid.EMPTY,
      name: null,
      coefficient: null
    }));
    this._cd.markForCheck();
  }

  delete(i: number) {
    this.examenMatieres.removeAt(i);
    this._cd.markForCheck();
  }

  onConfirm() {
    this.error = null;
    validateForm(this.form);
    if (this.form.valid && this._validateExamenMatieres()) {

      this._store.dispatch(showLoading());
      
      this.model.typeExamen = this.form.get('typeExamen').value;
      this.model.typeNote = this.form.get('typeNote').value;
      this.model.apcMoyenneTotalMin = this.form.get('apcMoyenneTotalMin').value;
      this.model.finalExamenMatieres = this.examenMatieres.value
        .filter(x => !!x.name && !!x.coefficient)
        .map(x => MetfpetServiceAgent.FinalExamenMatiereDTO.fromJS(x));

      this._metfpetService.updateFinalExamenMatieres(MetfpetServiceAgent.FinalExamenMatiereListDTO.fromJS(this.model))
        .subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  private _validateExamenMatieres() {
    var choices1 = this.examenMatieres.value.filter(x => !x.name || !x.coefficient);
    if (choices1.length > 0) {
      this.error = 'Certains champs obligatoires manquent';
    }

    var choices2 = this.examenMatieres.value.filter(x => !!x.name).map(x => x.name);
    if (new Set(choices2).size !== choices2.length) {
      this.error = 'Les matières sont dupliquées';
    }

    return !this.error;
  }

  onClose() {
    this._dialogRef.close();
  }

  onTypeExamenChange() {
    if (this.form.get('typeExamen').value === <any>TypeExamen.APC) {
      this.isAPCType = true;
      this.form.controls['typeNote'].setValue(<any>TypeNote.Sur100);
    } 
    else 
    {
      this.isAPCType = false;
      this.form.controls['typeNote'].setValue(<any>TypeNote.Sur20);
      this.form.controls['apcMoyenneTotalMin'].setValue(null);
    }
    this._cd.markForCheck();
  }
}