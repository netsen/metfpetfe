import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { 
  AppSettings,
  Settings,
  DialogService,
  showLoading,
  BaseTableComponent,
  MetfpetServiceAgent,
  PerfectScrollService,
  AuthService,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { InstitutionsTab, Tabs } from './institutions-tab.component';

@Component({
  templateUrl: './academic-years.component.html',
  styleUrls: ['./academic-years.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AcademicYearsComponent extends BaseTableComponent {
  
  settings: Settings;
  title: string;
  navs = Tabs;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _authService: AuthService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Année d\'étude';
    this.sort = {prop: 'name', dir: 'desc'};
    if (this._authService.getUserType() == 'directionsNationales')
    {
      this.navs = InstitutionsTab;
    }
    else 
    {
      this.navs = Tabs;
    }
  }

  ngAfterViewInit() {
    this.triggerSearch();
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }
  
  open(row) {
    this._router.navigate([`${UiPath.admin.institutions.academicYears.view}/${row.id}`]);
  }

  create() {
    this._router.navigate([UiPath.admin.institutions.academicYears.add]);
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }
}
