import { 
  ChangeDetectionStrategy,
  Component, 
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import {
  AppSettings,
  Settings,
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './academic-year.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AcademicYearComponent implements OnInit {

  public form: FormGroup;
  public settings: Settings;
  public title: string;
  public isCreation: boolean;
  private _model: MetfpetServiceAgent.AnneeAcademiqueDTO;

  constructor(
    public appSettings: AppSettings, 
    public fb: FormBuilder, 
    private _location: Location,
    private _router: Router,
    private _route: ActivatedRoute, 
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings; 
    this.form = this.fb.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'dateDebut': [null, Validators.required],
      'dateFin': [null, Validators.required],
      'dateDebutInscription': [null, Validators.required],
      'dateLimiteInscription': [null, Validators.required]
    });
  }

  ngOnInit() {
    this._route.params.subscribe(params => {
      let id = params['id'];
      this.isCreation = !id;

      if(!this.isCreation) {
        this._metfpetService.getAnneeAcademique(id)
          .subscribe(data => {
            this._model = data;
            this.title = this._model.name;
            this.form.controls['name'].setValue(this._model.name);
            this.form.controls['dateDebut'].setValue(this._model.dateDebut);
            this.form.controls['dateFin'].setValue(this._model.dateFin);
            this.form.controls['dateDebutInscription'].setValue(this._model.dateDebutInscription);
            this.form.controls['dateLimiteInscription'].setValue(this._model.dateLimiteInscription);
          });
      }
    });
  }

  public onSubmit():void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveAnneeAcademique$: Observable<MetfpetServiceAgent.AnneeAcademiqueDTO>;

      if (this.isCreation) {
        saveAnneeAcademique$= this._metfpetService.createAnneeAcademique(MetfpetServiceAgent.AnneeAcademiqueDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
          )
        ));
      } else {
        saveAnneeAcademique$ = this._metfpetService.updateAnneeAcademique(MetfpetServiceAgent.AnneeAcademiqueDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
          )
        ));
      }

      saveAnneeAcademique$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([UiPath.admin.institutions.academicYears.list]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  goBack() {
    this._location.back();
  }

}