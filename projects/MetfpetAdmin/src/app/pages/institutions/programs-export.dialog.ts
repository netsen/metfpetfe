import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component ({
  templateUrl: './programs-export.dialog.html',
})
export class ProgramsExportDialog {

  public form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<ProgramsExportDialog>,
    private _formBuilder: FormBuilder
  ) {
    this.form = this._formBuilder.group({
      exportExcel: true,
      exportNom: true,
      exportDescription: true,
      exportDuree: true,
      exportInstitution: true,
      exportFaculte: true,
      exportTypeProgramme: true,
      exportTypeAdmission: true,
      exportTypeDiplome: true,
      exportNiveauAcces: true,
      exportStatut: true,
      exportMatieresExamen: true,
      exportTypeExamen: true,
    });
  }

  onConfirm(): void {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    }
  }

  onClose(): void {
    this.dialogRef.close(null);
  }
}