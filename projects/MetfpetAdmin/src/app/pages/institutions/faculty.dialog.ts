import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  Inject,
  ViewEncapsulation
} from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { 
  FacultyStatus,
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';

@Component({
  templateUrl: './faculty.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FacultyDialog {

  title: string;
  form: FormGroup;
  isCreation: boolean;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  private _model: MetfpetServiceAgent.FaculteDTO;
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _cd: ChangeDetectorRef, 
    private _dialogRef: MatDialogRef<FacultyDialog>,
    private _fb: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.form = this._fb.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'description': [null],
      'institutionId': [null, Validators.compose([Validators.required])],
      'status': [null],
    });

    if (data.institutionId) {
      this.form.get('institutionId').setValue(data.institutionId);

    } else {
      this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
         pageIndex: -1, filters: {}
      })).subscribe(data => {
        this.institutionList = data.results;
        this._cd.markForCheck();
      });
    }

    if (data.facultyId) {
      this.isCreation = false;
      this._metfpetService.getFaculte(data.facultyId).subscribe(faculty => {
        this._model = faculty;
        this.title = this._model.name;
        this.form.controls['name'].setValue(this._model.name);
        this.form.controls['description'].setValue(this._model.description);
        this.form.controls['institutionId'].setValue(this._model.institutionId);
        this.form.controls['status'].setValue(this._model.status === <any>FacultyStatus.Active);
        this._cd.markForCheck();
      });
    } else {
      this.isCreation = true;
      this.title = 'Nouvelle département';
    }
  }

  onClose(): void {
    this._dialogRef.close();
  }

  onConfirm(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveFaculty$: Observable<MetfpetServiceAgent.FaculteDTO>;

      if (this.isCreation) {
        saveFaculty$= this._metfpetService.createFaculte(MetfpetServiceAgent.FaculteDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: FacultyStatus.Active },
          )
        ));
      } else {
        saveFaculty$ = this._metfpetService.updateFaculte(MetfpetServiceAgent.FaculteDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: this.form.get('status').value ? FacultyStatus.Active : FacultyStatus.Inactive },
          )
        ));
      }

      saveFaculty$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

}