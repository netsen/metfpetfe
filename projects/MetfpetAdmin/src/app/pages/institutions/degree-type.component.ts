import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component, 
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import { 
  AppSettings,
  Settings,
  CompetitionCenterStatus,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  showException,
  TypeDiplomeStatus,
  validateForm,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './degree-type.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DegreeTypeComponent implements OnInit {

  form: FormGroup;
  settings: Settings;
  title: string;
  isCreation: boolean;
  private _model: MetfpetServiceAgent.TypeDiplomeDTO;

  constructor(
    public appSettings: AppSettings, 
    private _cdRef: ChangeDetectorRef,
    private _fb: FormBuilder,
    private _location: Location, 
    private _router: Router,
    private _route: ActivatedRoute, 
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings; 
    this.form = this._fb.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'code': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'status': [null]
    });
  }

  ngOnInit() {
    this._route.params.subscribe(params => {
      let id = params['id'];
      this.isCreation = !id;

      if (!this.isCreation) {
        this._metfpetService.getTypeDiplome(id)
          .subscribe((data : any) => {
            this._model = data;
            this.title = this._model.name;
            this.form.controls['name'].setValue(this._model.name);
            this.form.controls['code'].setValue(this._model.code);
            this.form.controls['status'].setValue(this._model.status === <any>CompetitionCenterStatus.Active);
            this._cdRef.markForCheck();
          });
      }
    });
  }

  public onSubmit():void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveTypeDiplome$: Observable<MetfpetServiceAgent.TypeDiplomeDTO>;

      if (this.isCreation) {
        saveTypeDiplome$= this._metfpetService.createTypeDiplome(MetfpetServiceAgent.TypeDiplomeDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: CompetitionCenterStatus.Active }
          )
        ));
      } else {
        saveTypeDiplome$ = this._metfpetService.updateTypeDiplome(MetfpetServiceAgent.TypeDiplomeDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
          )
        ));
      }

      saveTypeDiplome$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([UiPath.admin.institutions.degreeTypes.list]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  goBack() {
    this._location.back();
  }
}