import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';
import {
  AuthService,
} from 'MetfpetLib';

export const Tabs = [
  { 
    routerLink: '/' + UiPath.admin.institutions.list, 
    label: 'Institutions' 
  },
  {
    routerLink: '/' + UiPath.admin.institutions.competitionCenters.list, 
    label: 'Centres de concours' 
  },
  {
    routerLink: '/' + UiPath.admin.institutions.levels.list, 
    label: 'Niveaux' 
  },
  {
    routerLink: '/' + UiPath.admin.institutions.academicYears.list, 
    label: 'Années scolaires' 
  },
  {
    routerLink: '/' + UiPath.admin.institutions.degreeTypes.list, 
    label: 'Type de diplôme' 
  },
  {
    routerLink: '/' + UiPath.admin.institutions.activityAreas.list, 
    label: 'Secteur d’activité' 
  },
];

export const InstitutionsTab = [
  { 
    routerLink: '/' + UiPath.admin.institutions.list, 
    label: 'Institutions' 
  }
];

@Component({
  templateUrl: './institutions-tab.component.html',
})
export class InstitutionsTabComponent {

  navs = Tabs;
  title: string;
  routerEventSubscription: Subscription;

  constructor(private router: Router,
    private _authService: AuthService) {
    this.title = 'Institutions - Paramètres : insitutions';
    if (this._authService.getUserType() == 'directionsNationales')
    {
      this.navs = InstitutionsTab;
    }
    else 
    {
      this.navs = Tabs;
    }
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = 'Institutions - Paramètres : ' + selectedTab.label.toLowerCase();
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
