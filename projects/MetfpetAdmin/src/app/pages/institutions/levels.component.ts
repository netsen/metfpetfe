import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent,
  MetfpetServiceAgent,
  PerfectScrollService,
  AuthService,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { InstitutionsTab, Tabs } from './institutions-tab.component';

@Component({
  templateUrl: './levels.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LevelsComponent extends BaseTableComponent {
  
  settings: Settings;
  title: string;
  navs = Tabs;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _authService: AuthService,
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Niveaux d\'étude';
    this.sort = {prop: 'name', dir: 'desc'};
    if (this._authService.getUserType() == 'directionsNationales')
    {
      this.navs = InstitutionsTab;
    }
    else 
    {
      this.navs = Tabs;
    }
  }

  ngAfterViewInit() {
    this.searchForm.get('name').valueChanges.pipe(startWith(this.searchForm.get('name').value))
      .subscribe((name) => {
        this.searchForm.patchValue({name}, {emitEvent: false});
        this.triggerSearch();
      });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null,
    });
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }
  
  open(selectedRow) {
    this._router.navigate([`${UiPath.admin.institutions.levels.view}/${selectedRow.id}`]);
  }

  newLevel() {
    this._router.navigate([UiPath.admin.institutions.levels.add]);
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }
}
