import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder} from '@angular/forms';
import { InstitutionsTab, Tabs } from './institutions-tab.component';
import {
  AppSettings,
  Settings,
  MetfpetServiceAgent,
  PerfectScrollService,
  AuthService,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './activity-areas.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActivityAreasComponent implements OnInit {

  activityAreaList: Array<MetfpetServiceAgent.ActivityAreaViewModel>;
  navs = Tabs;

  constructor(
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    private _route: ActivatedRoute,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _authService: AuthService,
  ) {
    if (this._authService.getUserType() == 'directionsNationales')
    {
      this.navs = InstitutionsTab;
    }
    else 
    {
      this.navs = Tabs;
    }
  }

  ngOnInit() {
    this._metfpetService.getActivityAreaListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.activityAreaList = data.results;
      this._cd.markForCheck();
    });
  }

  newActivityArea() {
    this._router.navigate([UiPath.admin.institutions.activityAreas.add]);
  }

  open(row) {
    this._router.navigate([`${UiPath.admin.institutions.activityAreas.view}/${row.id}`]);
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }
}