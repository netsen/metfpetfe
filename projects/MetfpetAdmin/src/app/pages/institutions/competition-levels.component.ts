import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef,
  Component, 
  Input, 
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { 
  DialogService,
  MetfpetServiceAgent,
  PerfectScrollService,
  hideLoading,
  showException,
  showLoading,
  showSuccess,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';

@Component({
  selector: 'app-competition-levels',
  templateUrl: './competition-levels.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompetitionLevelsComponent {

  form: FormGroup;
  model: MetfpetServiceAgent.InstitutionDTO;
  concoursList: Array<MetfpetServiceAgent.ConcoursDTO>;

  @Input('institutionId')
  get institutionId(): String {
    return this.form.get('id').value;
  }

  set institutionId(value) {
    this.form.get('id').setValue(value);
    this.loadInstitution();
  }

  constructor(
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.form = this._formBuilder.group({
      'id': null,
      'concoursInstitutions': this._formBuilder.array([]),
    });
  }

  ngOnInit() {
    this._metfpetService.getAllConcours(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.concoursList = data.results;
      this._cd.markForCheck();
    });
  }

  get concoursInstitutions() {
    return this.form.controls['concoursInstitutions'] as FormArray;
  }

  getNiveauAccesName(form: FormGroup) {
    return form.value?.niveauAccesName;
  }

  getTypeProgrammeName(form: FormGroup) {
    return form.value?.typeProgrammeName;
  }

  loadInstitution() {
    if (this.institutionId) {
      this._metfpetService.getInstitution(this.institutionId.toString()).subscribe((data) => {
        this.model = data;
        if (this.model.concoursInstitutions) {
          for (var item of this.model.concoursInstitutions) {
            this.concoursInstitutions.push(this._formBuilder.group({
              concoursId: item.concoursId,
              niveauAccesId: item.niveauAccesId,
              niveauAccesName: item.niveauAccesName,
              typeProgrammeId: item.typeProgrammeId,
              typeProgrammeName: item.typeProgrammeName
            }));
          }
        }
        this._cd.markForCheck();
      });
    }
  }

  onConfirm() {
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this.model = Object.assign({}, this.model, this.form.value);

      this.model.concoursInstitutions = this.concoursInstitutions.value
        .map(x => MetfpetServiceAgent.ConcoursInstitutionDTO.fromJS(x));

      this._metfpetService.updateInstitution(MetfpetServiceAgent.InstitutionDTO.fromJS(this.model))
        .subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([UiPath.admin.institutions.list]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }
}
