import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component, 
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray} from '@angular/forms';
import { Observable } from 'rxjs';
import { 
  AppSettings,
  Settings,
  CompetitionCenterStatus,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { UiPath } from '../ui-path';
import { Guid } from 'guid-typescript';

@Component({
  templateUrl: './competition-center.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompetitionCenterComponent implements OnInit {

  form: FormGroup;
  settings: Settings;
  isBrevetSelected: boolean;
  isBacSelected: boolean;
  title: string;
  isCreation: boolean;
  niveauAccesList: Array<MetfpetServiceAgent.NiveauAcces>;
  regionList: Array<MetfpetServiceAgent.Region>;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  institutionList: Array<MetfpetServiceAgent.InstitutionDescription>;
  private _model: MetfpetServiceAgent.CentreConcoursDTO;

  constructor(
    public appSettings: AppSettings, 
    private _cdRef: ChangeDetectorRef,
    private _fb: FormBuilder,
    private _location: Location, 
    private _router: Router,
    private _route: ActivatedRoute, 
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings; 
    this.isBrevetSelected = false;
    this.isBacSelected = false;
    this.form = this._fb.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'status': [null],
      'hasBac': [false],
      'hasTerminal': [false],
      'hasBepc': [false],
      'hasPostSecondaire': [false],
      'hasPostPrimaire': [false],
      'hasAutre': [false],
      'regions': this._fb.array([]),
      'prefectures': this._fb.array([]),
      'programmes': this._fb.array([]),
    });
  }
  
  ngOnInit() {
    this._metfpetService.getNiveauAccesList().subscribe(data => this.niveauAccesList = data);
    this._metfpetService.getRegionList().subscribe(data => this.regionList = data);
    this._metfpetService.getPrefectureList().subscribe(data => this.prefectureList = data);

    this._metfpetService.getInstitutionList()
      .subscribe((data) => {
        this.institutionList = data;
        this._cdRef.markForCheck();
      });

    this._route.params.subscribe(params => {
      let id = params['id'];
      this.isCreation = !id;

      if (!this.isCreation) {
        this._metfpetService.getCentreConcours(id)
          .subscribe((data : any) => {
            this._model = data;
            this.title = this._model.name;
            this.form.controls['name'].setValue(this._model.name);
            this.form.controls['status'].setValue(this._model.status === <any>CompetitionCenterStatus.Active);
            this.form.controls['hasBac'].setValue(this._model.hasBac);
            this.form.controls['hasTerminal'].setValue(this._model.hasTerminal);
            this.form.controls['hasBepc'].setValue(this._model.hasBepc);
            this.form.controls['hasPostSecondaire'].setValue(this._model.hasPostSecondaire);
            this.form.controls['hasPostPrimaire'].setValue(this._model.hasPostPrimaire);
            this.form.controls['hasAutre'].setValue(this._model.hasAutre);
            
            if (this._model.regionIds)
            {
              for (var regionId of this._model.regionIds) {
                this.regions.push(this._fb.group({
                  id: regionId,
                }));
              }
            }

            if (this._model.prefectureIds)
              {
                for (var prefectureId of this._model.prefectureIds) {
                  this.prefectures.push(this._fb.group({
                    id: prefectureId,
                  }));
                }
              }
            
            if (this._model.programmes)
            {
              for (var programme of this._model.programmes) {
                let programmeId = programme.programmeId ? programme.programmeId : Guid.EMPTY;
                this.programmes.push(this._fb.group({
                  institutionId: programme.institutionId,
                  programmeId: programmeId
                }));
              }
            }

            this._cdRef.markForCheck();
          });
      }
    });
  }

  public onSubmit():void {
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveCentreConcours$: Observable<MetfpetServiceAgent.CentreConcoursDTO>;

      if (this.isCreation) {
        saveCentreConcours$ = this._metfpetService.createCentreConcours(MetfpetServiceAgent.CentreConcoursDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { 
              status: CompetitionCenterStatus.Active,
              regionIds: this.getRegionIds(),
              prefectureIds: this.getPrefectureIds(),
              programmes: this.getUpdatedProgrammes()
            }
          )
        ));
      } else {
        saveCentreConcours$ = this._metfpetService.updateCentreConcours(MetfpetServiceAgent.CentreConcoursDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { 
              status: this.form.get('status').value ? CompetitionCenterStatus.Active : CompetitionCenterStatus.Inactive,
              regionIds: this.getRegionIds(),
              prefectureIds: this.getPrefectureIds(),
              programmes: this.getUpdatedProgrammes()
            }
          )
        ));
      }

      saveCentreConcours$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([UiPath.admin.institutions.competitionCenters.list]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  goBack() {
    this._location.back();
  }

  getRegionIds() {
    return this.regions.value.filter(x => !!x.id).map(x => x.id);
  }

  getPrefectureIds() {
    return this.prefectures.value.filter(x => !!x.id).map(x => x.id);
  }

  getUpdatedProgrammes() {
    return this.programmes.value.filter(x => !!x.institutionId && !!x.programmeId).map(x => 
      MetfpetServiceAgent.CentreConcoursProgrammeDTO.fromJS({
        institutionId: x.institutionId,
        programmeId: x.programmeId === Guid.EMPTY ? null : x.programmeId
      }));
  }

  get regions() {
    return this.form.controls['regions'] as FormArray;
  }

  get prefectures() {
    return this.form.controls['prefectures'] as FormArray;
  }

  get programmes() {
    return this.form.controls['programmes'] as FormArray;
  }

  addRegion() {
    this.regions.push(this._fb.group({
      id: null, 
    }));
    this._cdRef.markForCheck();
  }

  deleteRegion(i: number) {
    this.regions.removeAt(i);
    this._cdRef.markForCheck();
  }

  addPrefecture() {
    this.prefectures.push(this._fb.group({
      id: null, 
    }));
    this._cdRef.markForCheck();
  }

  deletePrefecture(i: number) {
    this.prefectures.removeAt(i);
    this._cdRef.markForCheck();
  }

  addProgramme() {
    this.programmes.push(this._fb.group({
      institutionId: null,
      programmeId: null
    }));
    this._cdRef.markForCheck();
  }

  deleteProgramme(i: number) {
    this.programmes.removeAt(i);
    this._cdRef.markForCheck();
  }

  getProgrammes(id: any) {
    if (id && this.institutionList)
    {
      let institution = this.institutionList.find(x => x.id == id);
      if (institution) 
      {
        return institution.programmes;
      }
    }
    return [];
  }

  getPrefectures() {
    let regionIds = this.getRegionIds();
    if (regionIds && regionIds.length > 0 && this.prefectureList)
    {
      return this.prefectureList.filter(x => regionIds.includes(x.regionId));
    }
    return 
  }
}