import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { InstitutionsTabComponent } from './institutions-tab.component';
import { InstitutionsComponent } from './institutions.component';
import { InstitutionComponent } from './institution.component';
import { CompetitionCentersComponent } from './competition-centers.component';
import { CompetitionCenterComponent } from './competition-center.component';
import { FacultiesComponent } from './faculties.component';
import { FacultyDialog } from './faculty.dialog';
import { ProgramsComponent } from './programs.component';
import { ProgramDialog } from './program.dialog';
import { LevelsComponent } from './levels.component';
import { LevelComponent } from './level.component';
import { AcademicYearsComponent } from './academic-years.component';
import { AcademicYearComponent } from './academic-year.component';
import { FraisInscriptionComponent } from './frais-inscription.component';
import { FraisInscriptionDialog } from './frais-inscription.dialog'; 
import { InstitutionsExportDialog } from './institutions-export.dialog';
import { CompetitionCentersExportDialog } from './competition-centers-export.dialog';
import { ConfigurationsAttenteComponent } from './configurations-attente.component';
import { MatiereExamenDialog } from './matiere-examen.dialog';
import { DegreeTypesComponent } from './degree-types.component';
import { DegreeTypeComponent } from './degree-type.component';
import { CompetitionLevelsComponent } from './competition-levels.component';
import { ActivityAreasComponent } from './activity-areas.component';
import { ActivityAreaComponent } from './activity-area.component';
import { ProgramsExportDialog } from './programs-export.dialog';

export const routes = [
  { 
    path: '', 
    component: InstitutionsTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'list',
          pathMatch: 'full'
        },
        { 
          path: 'list', 
          component: InstitutionsComponent, 
          data: { breadcrumb: 'Liste' }
        },
        { 
          path: 'new', 
          component: InstitutionComponent, 
          data: { breadcrumb: 'Nouvelle  Institution' }
        },
        { 
          path: 'view/:id', 
          component: InstitutionComponent, 
          data: { breadcrumb: 'Consultation  Institution' }
        },
        { 
          path: 'inscription-levels', 
          component: LevelsComponent, 
          data: { breadcrumb: 'Niveaux' },
          pathMatch: 'full' 
        },
        { 
          path: 'inscription-levels/new', 
          component: LevelComponent, 
          data: { breadcrumb: 'Nouveau niveau' }  
        },
        { 
          path: 'inscription-levels/view/:id', 
          component: LevelComponent, 
          data: { breadcrumb: 'Consultation niveau' }  
        },
        { 
          path: 'academic-years', 
          component: AcademicYearsComponent, 
          data: { breadcrumb: 'Années scolaires' },
          pathMatch: 'full' 
        },
        { 
          path: 'academic-years/new', 
          component: AcademicYearComponent, 
          data: { breadcrumb: 'Nouvelle années scolaire' }  
        },
        { 
          path: 'academic-years/view/:id', 
          component: AcademicYearComponent, 
          data: { breadcrumb: 'Consultation années scolaire' }  
        },
        { 
          path: 'competition-centers', 
          component: CompetitionCentersComponent, 
          data: { breadcrumb: 'Centres de concours' }  
        },
        { 
          path: 'competition-centers/view/:id', 
          component: CompetitionCenterComponent, 
          data: { breadcrumb: 'Consultation' }  
        },
        { 
          path: 'competition-centers/new', 
          component: CompetitionCenterComponent, 
          data: { breadcrumb: 'Nouveau' }  
        },
        { 
          path: 'degree-types', 
          component: DegreeTypesComponent, 
          data: { breadcrumb: 'Type de diplôme' }  
        },
        { 
          path: 'degree-types/new', 
          component: DegreeTypeComponent, 
          data: { breadcrumb: 'Nouveau type de diplôme' }  
        },
        { 
          path: 'degree-types/view/:id', 
          component: DegreeTypeComponent, 
          data: { breadcrumb: 'Consultation' }  
        },
        { 
          path: 'activity-areas', 
          component: ActivityAreasComponent, 
          data: { breadcrumb: 'Secteur d’activité' }  
        },
        { 
          path: 'activity-areas/new', 
          component: ActivityAreaComponent, 
          data: { breadcrumb: 'Nouveau secteur d’activité' }  
        },
        { 
          path: 'activity-areas/view/:id', 
          component: ActivityAreaComponent, 
          data: { breadcrumb: 'Consultation' }  
        },
      ]
  },
  { 
    path: 'frais-institutions', 
    component: FraisInscriptionComponent, 
    data: { breadcrumb: 'Frais d’institutions' },
    pathMatch: 'full' 
  },
  { 
    path: 'configurations-attente', 
    component: ConfigurationsAttenteComponent, 
    data: { breadcrumb: 'Configurations en attente' },
    pathMatch: 'full' 
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    InstitutionsTabComponent,
    InstitutionsComponent,
    InstitutionComponent,
    CompetitionCentersComponent,
    CompetitionCenterComponent,
    FacultiesComponent,
    FacultyDialog,
    ProgramsComponent,
    ProgramDialog,
    LevelsComponent,
    LevelComponent,
    AcademicYearsComponent,
    AcademicYearComponent,
    FraisInscriptionComponent,
    ConfigurationsAttenteComponent,
    DegreeTypesComponent,
    DegreeTypeComponent,
    FraisInscriptionDialog,
    InstitutionsExportDialog,
    CompetitionCentersExportDialog,
    MatiereExamenDialog,
    CompetitionLevelsComponent,
    ActivityAreasComponent,
    ActivityAreaComponent,
    ProgramsExportDialog,
  ]
})
export class InstitutionsModule {}
