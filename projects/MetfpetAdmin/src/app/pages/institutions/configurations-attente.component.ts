import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ElementRef, 
  ViewChild, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { startWith, debounceTime, distinctUntilChanged } from 'rxjs/operators/';
import { MatDialog } from '@angular/material/dialog';
import { 
  AppSettings,
  Settings,
  BaseTableComponent, 
  MetfpetServiceAgent,
  PerfectScrollService,
  DemandeStatus,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';


@Component({
  templateUrl: './configurations-attente.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfigurationsAttenteComponent extends BaseTableComponent {

  @ViewChild('searchNumero', {static: true}) searchNumero: ElementRef;
  @ViewChild('searchInstitutionName', {static: true}) searchInstitutionName: ElementRef;

  settings: Settings;
  title: string;
  selected = [];
  typeDemandeList : Array<MetfpetServiceAgent.TypeDemandeViewModel>;
  agrementationList : Array<MetfpetServiceAgent.AgrementationViewModel>;
  typeInstitutionList: Array<MetfpetServiceAgent.TypeInstitutionViewModel>;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialog: MatDialog,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Configurations en attente';
    this.sort = {prop: 'name', dir: 'desc'};
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getTypeDemandeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe((data)=>{
      this.typeDemandeList = data.results;
    });

    this._metfpetService.getAgrementationListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe((data)=>{
      this.agrementationList = data.results;
    });

    this._metfpetService.getTypeInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.typeInstitutionList = data.results;
    });

    combineLatest([
      fromEvent(this.searchNumero.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('numero').value}
          })
        ),
      fromEvent(
        this.searchInstitutionName.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('institutionName').value}
            })
          ),
      this.searchForm.get('typeDemandeId').valueChanges
        .pipe(
          startWith(this.searchForm.get('typeDemandeId').value)
        ),
      this.searchForm.get('agrementationId').valueChanges
        .pipe(
          startWith(this.searchForm.get('agrementationId').value)
        ),
      this.searchForm.get('typeInstitutionId').valueChanges
        .pipe(
          startWith(this.searchForm.get('typeInstitutionId').value)
        ),
    ])
    .subscribe((
      [
        eventSearchNumero, 
        eventInstitutionName,
        typeDemandeId,
        agrementationId,
        typeInstitutionId
      ]) => {
      this.searchForm.patchValue({
        numero: eventSearchNumero ? eventSearchNumero['target'].value : null, 
        institutionName: eventInstitutionName ? eventInstitutionName['target'].value : null, 
        typeDemandeId,
        agrementationId,
        typeInstitutionId
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  ngAfterViewInit() {
    this.triggerSearch();
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      typeDemandeId: null,
      agrementationId: null,
      numero: null,
      institutionName: null,
      typeInstitutionId: null,
      deliveryDateFrom: null,
      deliveryDateTo: null
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    criteria.filters.status = DemandeStatus.Delivre;
    criteria.filters.impactConfigurationInstitution = true;
    criteria.filters.institutionId = null;
    return this._metfpetService.getDemandeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  getId(row) {
    return row.id;
  }

  applySelectedDemande() {
    if (this.selected.length > 0) {
      var demandeIds = this.selected.map(x => x.id);
      this._metfpetService.generateInstitutionDataForSelectedDemandes(demandeIds).subscribe((data)=>{
        if (data) {
          this._router.navigate([UiPath.admin.institutions.list]);
        }
      });
    }
  }

  applyFilteredDemande() {
    let criteria = Object.assign({}, {
      pageSize: 0,
      pageIndex: 0,
      filters: this.searchForm.value,
      sort: null
    });
    this._metfpetService.generateInstitutionDataForFilteredDemandes(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria)).subscribe(data => {
      if (data) {
        this._router.navigate([UiPath.admin.institutions.list]);
      }
    });
  }
}
