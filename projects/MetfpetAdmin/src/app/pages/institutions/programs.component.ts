import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef,
  Component, 
  Input, 
  ViewEncapsulation,
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, of as observableOf } from 'rxjs';
import { 
  BaseTableComponent,
  DialogService,
  MetfpetServiceAgent,
  PerfectScrollService,
} from 'MetfpetLib';
import { ProgramDialog } from './program.dialog';
import { MatiereExamenDialog } from './matiere-examen.dialog';

@Component({
  selector: 'app-programs',
  templateUrl: './programs.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProgramsComponent extends BaseTableComponent {

  @Input('institutionId')
  get institutionId(): String {
    return this.searchForm.get('institution').value;
  }

  set institutionId(value) {
    this.searchForm.get('institution').setValue(value);
    this.triggerSearch();
  }

  @Input('hasFinalExamen')
  get hasFinalExamen(): boolean {
    return this.searchForm.get('hasFinalExamen').value;
  }

  set hasFinalExamen(value) {
    this.searchForm.get('hasFinalExamen').setValue(value);
    this.triggerSearch();
  }

  constructor(
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.sort = {prop: 'name', dir: 'desc'};
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      institution: null,
      hasFinalExamen: false
    });
  }

  protected _search(criteria: any): Observable<any> {
    // to convert True/false value to number 1/0
    criteria.filters.isActive = +criteria.filters.isActive;

    if (criteria.filters.institution) {
      return this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
    } else {
      return observableOf([]);
    }
  }

  newProgram() {
    this._dialogService.openDialog(
      ProgramDialog,
      {
        width: '800px',
        data: {
          institutionId: this.institutionId
        }
      }
    ).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  open(selectedRow) {
    this._dialogService.openDialog(
      ProgramDialog,
      {
        width: '800px',
        data: {
          programId: selectedRow.id,
          institutionId: this.institutionId
        }
      }
    ).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  openExamen(selectedRow) {
    this._dialogService.openDialog(
      MatiereExamenDialog,
      {
        width: '800px',
        data: {
          programmeId: selectedRow.id
        }
      }
    ).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }
}
