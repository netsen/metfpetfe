import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewChild,
  ViewEncapsulation,
  ElementRef,
  OnInit,
  AfterViewInit
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { InstitutionsTab, Tabs } from './institutions-tab.component';
import {
  AppSettings,
  Settings,
  MetfpetServiceAgent,
  PerfectScrollService,
  AuthService,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';


@Component({
  templateUrl: './degree-types.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DegreeTypesComponent implements OnInit{
  typeDiplomeList: Array<MetfpetServiceAgent.TypeDiplomeViewModel>;
  navs = Tabs;

  constructor(
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    private _route: ActivatedRoute,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _authService: AuthService,
  ) {
    if (this._authService.getUserType() == 'directionsNationales')
    {
      this.navs = InstitutionsTab;
    }
    else 
    {
      this.navs = Tabs;
    }
  }

  ngOnInit() {
    this._metfpetService.getTypeDiplomeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.typeDiplomeList = data.results;
      this._cd.markForCheck();
    });
  }

  newDegreeType() {
    this._router.navigate([UiPath.admin.institutions.degreeTypes.add]);
  }

  open(row) {
    this._router.navigate([`${UiPath.admin.institutions.degreeTypes.view}/${row.id}`]);
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }
}