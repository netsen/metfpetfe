import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component, 
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import { 
  AppSettings,
  Settings,
  ActivityAreaStatus,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  DialogService, 
  MetfpetServiceAgent,
  showException,
  validateForm,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './activity-area.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActivityAreaComponent implements OnInit {

  form: FormGroup;
  settings: Settings;
  title: string;
  isCreation: boolean;
  private _model: MetfpetServiceAgent.ActivityAreaDTO;

  constructor(
    public appSettings: AppSettings, 
    private _cdRef: ChangeDetectorRef,
    private _fb: FormBuilder,
    private _location: Location, 
    private _router: Router,
    private _route: ActivatedRoute, 
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings; 
    this.form = this._fb.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'status': [null]
    });
  }

  ngOnInit() {
    this._route.params.subscribe(params => {
      let id = params['id'];
      this.isCreation = !id;

      if (!this.isCreation) {
        this._metfpetService.getActivityArea(id)
          .subscribe((data : any) => {
            this._model = data;
            this.title = this._model.name;
            this.form.controls['name'].setValue(this._model.name);
            this.form.controls['status'].setValue(this._model.status === <any>ActivityAreaStatus.Active);
            this._cdRef.markForCheck();
          });
      }
    });
  }

  public onSubmit():void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveActivityArea$: Observable<MetfpetServiceAgent.ActivityAreaDTO>;

      if (this.isCreation) {
        saveActivityArea$= this._metfpetService.createActivityArea(MetfpetServiceAgent.ActivityAreaDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: ActivityAreaStatus.Active }
          )
        ));
      } else {
        saveActivityArea$ = this._metfpetService.updateActivityArea(MetfpetServiceAgent.ActivityAreaDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
          )
        ));
      }

      saveActivityArea$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([UiPath.admin.institutions.activityAreas.list]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  goBack() {
    this._location.back();
  }
}