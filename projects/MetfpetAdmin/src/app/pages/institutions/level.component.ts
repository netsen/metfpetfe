import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component, 
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import { 
  AppSettings,
  Settings,
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './level.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LevelComponent implements OnInit {

  form: FormGroup;
  settings: Settings;
  title: string;
  isCreation: boolean;
  typeProgrammeList: Array<any>;
  private _model: MetfpetServiceAgent.NiveauEtudeDTO;
  
  constructor(
    public appSettings: AppSettings,
    private _cd: ChangeDetectorRef, 
    private _fb: FormBuilder, 
    private _location: Location,
    private _router: Router,
    private _route: ActivatedRoute,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings; 
    this.form = this._fb.group({
      'name': [null, Validators.required],
      'typeProgrammeId': [null, Validators.required],
    });
  }

  ngOnInit() {
    this._route.params.subscribe(params => {
      let id = params['id'];
      this.isCreation = !id;

      if (!this.isCreation) {
        this._metfpetService.getNiveauEtude(id).subscribe(data => {
          this._model = data;
          this.title = this._model.name;
          this.form.controls['name'].setValue(this._model.name);
          this.form.controls['typeProgrammeId'].setValue(this._model.typeProgrammeId);
        });
      }
    });

    this._metfpetService.getTypeProgrammeList().subscribe(data => {
      this.typeProgrammeList = data;
      this._cd.markForCheck();
    });
  }

  public onSubmit():void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveNiveau$: Observable<MetfpetServiceAgent.NiveauEtudeDTO>;

      if (this.isCreation) {
        saveNiveau$= this._metfpetService.createNiveauEtude(MetfpetServiceAgent.NiveauEtudeDTO.fromJS(
          Object.assign({}, this._model, this.form.value)
        ));
      } else {
        saveNiveau$ = this._metfpetService.updateNiveauEtude(MetfpetServiceAgent.NiveauEtudeDTO.fromJS(
          Object.assign({}, this._model, this.form.value)
        ));
      }

      saveNiveau$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([UiPath.admin.institutions.levels.list]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  goBack() {
    this._location.back();
  }
}