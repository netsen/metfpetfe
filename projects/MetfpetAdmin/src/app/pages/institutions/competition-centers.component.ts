import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { startWith } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent, 
  MetfpetServiceAgent,
  PerfectScrollService,
  DialogService,
  ExportService,
  showLoading,
  hideLoading,
  AuthService,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { InstitutionsTab, Tabs } from './institutions-tab.component';
import { CompetitionCentersExportDialog } from './competition-centers-export.dialog';

@Component({
  templateUrl: './competition-centers.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompetitionCentersComponent extends BaseTableComponent {
  
  settings: Settings;
  title: string;
  navs = Tabs;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _authService: AuthService,
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Centres de concours';
    this.sort = {prop: 'name', dir: 'desc'};
    if (this._authService.getUserType() == 'directionsNationales')
    {
      this.navs = InstitutionsTab;
    }
    else 
    {
      this.navs = Tabs;
    }
  }

  ngAfterViewInit() {
    combineLatest([
      this.searchForm.get('name').valueChanges.pipe(startWith(this.searchForm.get('name').value)),
      this.searchForm.get('isActive').valueChanges.pipe(startWith(this.searchForm.get('isActive').value)),
    ])
    .subscribe(([name, isActive]) => {
      this.searchForm.patchValue({name, isActive}, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null,
      isActive: true,
    });
  }

  protected _search(criteria: any): Observable<any> {
    // to convert True/false value to number 1/0
    criteria.filters.isActive = +criteria.filters.isActive;
    return this._metfpetService.getCentreConcoursListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  open(selectedRow) {
    this._router.navigate([`${UiPath.admin.institutions.competitionCenters.view}/${selectedRow.id}`]);
  }

  newCompetitionCenter() {
    this._router.navigate([UiPath.admin.institutions.competitionCenters.add]);
  }

  public exportCentreConcours() {
    this._dialogService.openDialog(CompetitionCentersExportDialog, 
      {
        width: '700px'
      }
    ).afterClosed().subscribe(fields => {
      if (fields) {
        this._store.dispatch(showLoading());
        
        this._metfpetService.getCentreConcoursListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
          pageIndex: -1,
          filters: this.searchForm.value,
        }))
          .subscribe(data => 
            this._exportService.exportCentreConcours(data.results, fields))
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }
}
