import {   
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  Input,
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, of as observableOf } from 'rxjs';
import { 
  BaseTableComponent,
  DialogService,
  MetfpetServiceAgent,
  PerfectScrollService,
} from 'MetfpetLib';
import { FacultyDialog } from './faculty.dialog';

@Component({
  selector: 'app-faculties',
  templateUrl: './faculties.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FacultiesComponent extends BaseTableComponent {

  @Input('institutionId')
  get institutionId(): String {
    return this.searchForm.get('institution').value;
  }

  set institutionId(value) {
    this.searchForm.get('institution').setValue(value);
    this.triggerSearch();
  }

  constructor(
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.sort = {prop: 'name', dir: 'desc'};
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      institution: null,
    });
  }

  protected _search(criteria: any): Observable<any> {
    // to convert True/false value to number 1/0
    criteria.filters.isActive = +criteria.filters.isActive;

    if (criteria.filters.institution) {
      return this._metfpetService.getFaculteListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
    } else {
      return observableOf([]);
    }
  }

  newFaculty() {
    this._dialogService.openDialog(
      FacultyDialog,
      {
        width: '600px',
        data: {
          institutionId: this.institutionId
        }
      }
    ).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  open(selectedRow: MetfpetServiceAgent.FaculteRowViewModel) {
    this._dialogService.openDialog(
      FacultyDialog,
      {
        width: '600px',
        data: {
          facultyId: selectedRow.id,
          institutionId: this.institutionId
        }
      }
    ).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }
}
