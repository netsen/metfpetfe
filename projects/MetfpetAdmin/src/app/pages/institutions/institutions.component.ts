import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { startWith } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent,
  MetfpetServiceAgent,
  PerfectScrollService,
  DialogService,
  showLoading,
  hideLoading,
  ExportService,
  AuthService,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { InstitutionsTab, Tabs } from './institutions-tab.component';
import { Guid } from 'guid-typescript';
import { InstitutionsExportDialog } from './institutions-export.dialog';
import { ProgramsExportDialog } from './programs-export.dialog';

@Component({
  templateUrl: './institutions.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstitutionsComponent extends BaseTableComponent {

  settings: Settings;
  title: string;
  navs = Tabs;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  typeInstitutionList: Array<MetfpetServiceAgent.TypeInstitution>;
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissement>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _authService: AuthService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Institutions';
    this.sort = {prop: 'name', dir: 'desc'};
    if (this._authService.getUserType() == 'directionsNationales')
    {
      this.navs = InstitutionsTab;
    }
    else 
    {
      this.navs = Tabs;
    }
  }

  public allowAccessInstitutionDetail() {
    return this._authService.getUserType() != 'directionsNationales' && this._authService.getUserType() != 'ministre';
  }

  ngAfterViewInit() {
    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cd.markForCheck(); 
    });

    this._metfpetService.getStatusEtablissementList().subscribe(data => {
      this.statusEtablissementList = data;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getTypeInstitutionList().subscribe(data => {
      this.typeInstitutionList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { }
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });

    combineLatest([
      this.searchForm.get('name').valueChanges.pipe(startWith(this.searchForm.get('name').value)),
      this.searchForm.get('prefecture').valueChanges.pipe(startWith(this.searchForm.get('prefecture').value)),
      this.searchForm.get('typeInstitution').valueChanges.pipe(startWith(this.searchForm.get('typeInstitution').value)),
      this.searchForm.get('statusEtablissement').valueChanges.pipe(startWith(this.searchForm.get('statusEtablissement').value)),
      this.searchForm.get('programme').valueChanges.pipe(startWith(this.searchForm.get('programme').value)),
      this.searchForm.get('isActive').valueChanges.pipe(startWith(this.searchForm.get('isActive').value)),
    ])
    .subscribe(([name, prefecture, typeInstitution, statusEtablissement, programme, isActive]) => {
      this.searchForm.patchValue({name, prefecture, typeInstitution, statusEtablissement, programme, isActive}, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null,
      prefecture: null,
      typeInstitution: null,
      statusEtablissement: null,      
      isActive: true,
      programme: null,
    });
  }

  protected _search(criteria: any): Observable<any> {
    // to convert True/false value to number 1/0
    criteria.filters.isActive = +criteria.filters.isActive;
    return this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  open(selectedRow) {
    this._router.navigate([`${UiPath.admin.institutions.view}/${selectedRow.id}`]);
  }

  newInstitution() {
    this._router.navigate([`${UiPath.admin.institutions.add}`]);
  }

  public exportInstitutions() {
    this._dialogService.openDialog(InstitutionsExportDialog, 
      {
        width: '600px'
      }
    ).afterClosed().subscribe(fields => {
      if (fields) {
        this._store.dispatch(showLoading());
        
        this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
          pageIndex: -1,
          filters: this.searchForm.value,
        }))
          .subscribe(data => 
            this._exportService.exportInstitutions(data.results, fields))
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  public exportProgrammes() {
    this._dialogService.openDialog(ProgramsExportDialog, 
      {
        width: '800px'
      }
    ).afterClosed().subscribe(fields => {
      if (fields) {
        this._store.dispatch(showLoading());
        
        this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
          pageIndex: -1,
          filters: {},
        }))
          .subscribe(data => 
            this._exportService.exportProgrammes(data.results, fields))
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  public exportFinalExamenMatieres() {
    this._store.dispatch(showLoading());     
    this._metfpetService.getAllProgrammeFinalExamen()
      .subscribe(data => 
        this._exportService.exportFinalExamenMatieres(data))
      .add(() => this._store.dispatch(hideLoading()));
  }
}
