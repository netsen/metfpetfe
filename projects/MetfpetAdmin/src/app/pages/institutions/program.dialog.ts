import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef,
  Component, 
  Inject,
  OnInit, 
  ViewEncapsulation 
} from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators/';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  ProgramStatus,
  isNormalFloating,
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  DialogService, 
  SeverityEnum,
  showException,
} from 'MetfpetLib';

@Component({
  templateUrl: './program.dialog.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProgramDialog {

  title: string;
  form: FormGroup;
  isCreation: boolean;
  institutionList : Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  faculteList: Array<MetfpetServiceAgent.FaculteRowViewModel>;
  typeProgrammeList: Array<MetfpetServiceAgent.TypeProgramme>;
  typeAdmissionList: Array<MetfpetServiceAgent.TypeAdmission>;
  niveauAccesList: Array<MetfpetServiceAgent.NiveauAcces>;
  typeDiplomeList: Array<MetfpetServiceAgent.TypeDiplomeViewModel>;
  activityAreaList: Array<MetfpetServiceAgent.ActivityAreaViewModel>;
  private _model: MetfpetServiceAgent.ProgrammeDTO;
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _cd: ChangeDetectorRef,
    private _dialogRef: MatDialogRef<ProgramDialog>,
    private _fb: FormBuilder, 
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _store: Store<any>
  ) {
    this.form = this._fb.group({
      'status': [null],
      'name': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'description': [null],
      'duree': [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])],
      'limitePlace': [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])],
      'institutionId': [null, Validators.compose([Validators.required])],
      'faculteId': [null, Validators.compose([Validators.required])],
      'typeProgrammeId': [null, Validators.required],
      'typeAdmissionId': [null, Validators.required],
      'niveauAccesId': [null, Validators.required],
      'moyenneSeuilMin' : [null],
      'moyenneSeuilMax' : [null],
      'inscriptionMontant': [null, Validators.pattern('^[0-9]*$')],
      'reinscriptionMontant': [null, Validators.pattern('^[0-9]*$')],
      'typeDiplomeId': [null],
      'activityAreaId': [null, Validators.required]
    });
    
    this._metfpetService.getTypeProgrammeList().subscribe(data => {
      this.typeProgrammeList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getNiveauAccesList().subscribe(data => {
      this.niveauAccesList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getTypeAdmissionList().subscribe(data => {
      this.typeAdmissionList = data;
      this._cd.markForCheck();
    });
    
    this.form.get('institutionId').valueChanges.pipe(
      tap(institutionId => {
        this.faculteList = [];
        
        if (institutionId) {
          this._metfpetService.getFaculteListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
             pageIndex: -1, filters: {'institution': institutionId}
          })).subscribe(data => {
            this.faculteList = data.results;
            this._cd.markForCheck();
          });
        }
      })
    ).subscribe();

    if (data.institutionId) {
      this.form.get('institutionId').setValue(data.institutionId);

    } else {
      this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
         pageIndex: -1, filters: {}
      })).subscribe(data => {
        this.institutionList = data.results;
        this._cd.markForCheck();
      });
    }

    this._metfpetService.getTypeDiplomeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.typeDiplomeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getActivityAreaListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.activityAreaList = data.results;
      this._cd.markForCheck();
    });
  }

  ngAfterViewInit() {
    if (this.data.programId) {
      this.isCreation = false;
      this._metfpetService.getProgramme(this.data.programId).subscribe(program => {
        this._model = program;
        this.title = this._model.name;
        this.form.patchValue({
          'name': this._model.name,
          'description': this._model.description,
          'duree': this._model.duree,
          'limitePlace': this._model.limitePlace,
          'institutionId': this._model.institutionId,
          'faculteId': this._model.faculteId,
          'status': this._model.status === <any>ProgramStatus.Active,
          'typeProgrammeId': this._model.typeProgrammeId,
          'typeAdmissionId': this._model.typeAdmissionId,
          'niveauAccesId': this._model.niveauAccesId,
          'moyenneSeuilMin': this._model.moyenneSeuilMin,
          'moyenneSeuilMax': this._model.moyenneSeuilMax,
          'inscriptionMontant': this._model.inscriptionMontant,
          'reinscriptionMontant': this._model.reinscriptionMontant,
          'typeDiplomeId': this._model.typeDiplomeId,
          'activityAreaId': this._model.activityAreaId
        }, {emitEvent: false});
        this._cd.detectChanges();
      });
    } else {
      this.isCreation = true;
      this.title = 'Nouveau programme';
    }
  }

  onClose(): void {
    this._dialogRef.close();
  }

  onConfirm():void {
    validateForm(this.form);
    if (this.form.valid && this._validateMoyennes()) {
      this._store.dispatch(showLoading());
      var saveProgram$: Observable<MetfpetServiceAgent.ProgrammeDTO>;

      if (this.isCreation) {
        saveProgram$= this._metfpetService.createProgramme(MetfpetServiceAgent.ProgrammeDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: ProgramStatus.Active }
          )
        ));
      } else {
        saveProgram$ = this._metfpetService.updateProgramme(MetfpetServiceAgent.ProgrammeDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: this.form.get('status').value ? ProgramStatus.Active : ProgramStatus.Inactive }
          )
        ));
      }

      saveProgram$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  private _validateMoyennes(): boolean {
    if (this.form.get('moyenneSeuilMin').value && !isNormalFloating(this.form.get('moyenneSeuilMin').value)) 
    {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.ERROR, 
          message: 'La moyenne minimale doit être un nombre' 
        }
      });
      return false;
    }
    if (this.form.get('moyenneSeuilMax').value && !isNormalFloating(this.form.get('moyenneSeuilMax').value)) 
    {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.ERROR, 
          message: 'La moyenne maximale doit être un nombre' 
        }
      });
      return false;
    }
    if (this.form.get('moyenneSeuilMin').value && this.form.get('moyenneSeuilMax').value && this.form.get('moyenneSeuilMin').value > this.form.get('moyenneSeuilMax').value) 
    {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.ERROR, 
          message: 'La moyenne maximale doit être plus grande que la moyenne minimale' 
        }
      });
      return false;
    }
    return true;
  }
}