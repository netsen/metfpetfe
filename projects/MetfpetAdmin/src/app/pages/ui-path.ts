export const UiPath = {
  error: 'error',
  login: 'login',
  admin: {
    login: 'login',
    retrievePassword: 'retrieve-password',
    register: 'register',
    activities: {
      list: 'activities'
    },
    cursus: {
      inscriptions : 'cursus/inscriptions',
      examens      : {
         currentSession : 'cursus/examens/currentSession',
         pastSession  : 'cursus/examens/pastSession',
         publierLesNotes : 'cursus/examens/publierLesNotes',
         report  : 'cursus/examens/report',
      },
      gestion      : 'cursus/gestions',
      configurations: {
        mention       : 'cursus/configurations/mention',
        exporter      : 'cursus/configurations/exporter',
        importerNote  : 'cursus/configurations/importerNote',
        importerNoteDetails  : 'cursus/configurations/importerNoteDetails',
        importerRedoublant  : 'cursus/configurations/importerRedoublant',
        centre  : 'cursus/configurations/centre',
        matieresDesExamens :  'cursus/configurations/matieresDesExamens',
      }
    },
    concours: {
      currentSession : 'concours/currentSession',
      pastSession  : 'concours/pastSession',
      publierLesResultats : 'concours/publierLesResultats',
      actualiserOffres : 'concours/actualiserOffres',
      report  : 'concours/report',
      configurations: {
        mention       : 'concours/configurations/mention',
        concoursAcces : 'concours/configurations/concoursAcces',
        exporter      : 'concours/configurations/exporter',
        importerNote  : 'concours/configurations/importerNote',
      }
    },
    configuration: {
      agrements: {
        list: 'agrementation-configuration/agrements'
      }
    },
    document: {
      list: 'document/list',
      add: 'document/new',
      view: 'document/view',
      impressionDocuments: 'document/impressionDocuments'
    },
    students: {
      list: 'students',
      dossier: 'students/dossier',
      add: 'students/add',
    },
    bourses: {
      list         : 'bourses/list',
      motifs       : 'bourses/motifs',
      trimestres   : 'bourses/trimestres',
    },
    institutions: {
      list: 'institutions/list',
      add: 'institutions/new',
      view: 'institutions/view',

      competitionCenters: {
        list: 'institutions/competition-centers',
        add: 'institutions/competition-centers/new',
        view: 'institutions/competition-centers/view',
      },
      faculties: {
        list: 'institutions/faculties',
        add: 'institutions/faculties/new',
        view: 'institutions/faculties/view',
      },
      programs: {
        list: 'institutions/programs',
        add: 'institutions/programs/new',
        view: 'institutions/programs/view',
      },
      levels: {
        list: 'institutions/inscription-levels',
        add: 'institutions/inscription-levels/new',
        view: 'institutions/inscription-levels/view',
      },
      academicYears: {
        list: 'institutions/academic-years',
        add: 'institutions/academic-years/new',
        view: 'institutions/academic-years/view',
      },
      degreeTypes: {
        list       : 'institutions/degree-types',
        add        : 'institutions/degree-types/new',
        view       : 'institutions/degree-types/view',
      },
      activityAreas: {
        list       : 'institutions/activity-areas',
        add        : 'institutions/activity-areas/new',
        view       : 'institutions/activity-areas/view',
      }
    },
    admission: {
      control: 'admission/session',
      list: 'admission',
      archive: 'admission/archive',
    },
    lycees: {
      list: 'lycees/list',
      add: 'lycees/new',
      view: 'lycees/view',
      colleges: {
        list: 'lycees/colleges',
        add: 'lycees/colleges/new',
        view: 'lycees/colleges/view',
      },
      examCenters: {
        list: 'lycees/exam-centers',
        add: 'lycees/exam-centers/new',
        view: 'lycees/exam-centers/view',
      },
      matters: {
        list: 'lycees/matters',
        add: 'lycees/matters/new',
        view: 'lycees/matters/view',
      },
      optionsBAC: {
        list: 'lycees/optionsBAC',
        add: 'lycees/optionsBAC/new',
        view: 'lycees/optionsBAC/view',
      },
      optionsBEPC: {
        list: 'lycees/optionsBEPC',
        add: 'lycees/optionsBEPC/new',
        view: 'lycees/optionsBEPC/view',
      },
    },
    users: {
      ministry: {
        list: 'users/ministry',
      },
      institution: {
        list: 'users/institution',
      },
      role: {
        list: 'users/role',
      },
    },
    payments: {
      admission: {
        list: 'payments/admission',
      },
      inscription: {
        list: 'payments/inscription',
      },
      document: {
        list: 'payments/document',
      },
      agrement: {
        list: "payments/agrement"
      },
    },
    diplomes:{
      gerenate:{
        list       :'diplomes/generate-diplome'
      },
      admettre:{
        list: 'diplomes/admettre-diplome'
      },
      diplomer:{
        list:'diplomes/diplomer-etudiants'
      }
    },
    import : {
      existing   : 'import-students/existing',
      resultsBAC   : 'import-students/results-bac',
      resultsBEPC   : 'import-students/results-bepc',
      resultsTerminale   : 'import-students/results-terminale',
      importAdmission   : 'import-results/results-admission',
      importApprenantAdmission   : 'import-results/apprenant-admission',
      importPasserelles   : 'import-results/passerelles',
      importInscription : 'import-inscriptions/inscriptions',
      importLastYearInscription : 'import-inscriptions/lastYearInscriptions',
      actualiserInscription : 'actualiser-inscriptions/inscriptions',
      importRedoublant : 'import-redoublants/redoublants',
      importDiplome : 'import-diplome/diplome',
      syncInserjeune : 'sync-inserjeune/sync',
      ministryUsers   : 'import-users/ministry-users',
      institutionUsers   : 'import-users/institution-users',
      importCandidatsLibres : 'import-candidats-libres/candidats-libres',
      studentProfile   : 'import-student-profile/profile',
      importBourse : 'import-bourses/bourses',
      deleteBourse : 'import-bourses/delete-bourses',
      comptesEcobank : 'import-comptes-ecobank/comptes-ecobank',
    },
    export: {
      exportAdmission: 'export/export-admission',
      exportInscription: 'export/export-inscription',
    },
    agrementation: {
      configuration: {
        agrements: {
          list: 'agrementation-configuration/agrements'
        },
        documents: {
          list: 'agrementation-configuration/documents'
        },
        champs: {
          list: 'agrementation-configuration/champs'
        },
        valeurs: {
          list: 'agrementation-configuration/valeurs'
        },
        typeDemandes: {
          list: 'agrementation-configuration/type-demandes'
        },
        demandeurDocuments: {
          list: 'agrementation-configuration/demandeur-documents'
        },
        donneesFormation: {
          list: 'agrementation-configuration/donnees-formation'
        },
        import:{
          importAgrementations: 'agrementation-configuration/import/agrementations',
        }
      },
      dossiers: {
        list: 'agrementation-dossiers',
        dossier: 'agrementation-dossiers/dossier',
        add: 'agrementation-dossiers/add',
      },
      agrements: {
        list: 'agrements/list',
        demandeurs: 'agrements/demandeurs',
        demande: 'agrements/nouvelle-demande',
      }
    },
    statistiques: {
      admissions: 'statistiques/admissions',
      inscriptions: 'statistiques/inscriptions'
    },
    schoolKit: {
      kits: 'school-kits/kits',
      tools: 'school-kits/tools',
      reasons: 'school-kits/reasons',
      import: 'school-kits/import'
    }
  },
};