import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AdminInscriptionsComponent } from './admin-inscriptions.component';
import { AdminExamensComponent } from './admin-examens.component';
import { AdminGestionComponent } from './admin-gestions.component';
import { GenererCandidatsDialog } from './generer-candidats.dialog';
import { GenererRelevesDeNotesDialog } from './generer-releves-de-notes.dialog';
import { AdminExamensTabComponent } from './admin-examens-tab.component';
import { AdminPastExamensComponent } from './admin-past-examens.component';
import { ConfigExamensTabComponent } from './config-examens-tab.component';
import { ConfigExamensMentionsComponent } from './config-examens-mentions.component';
import { ConfigExamensCentresComponent } from './config-examens-centres.component';
import { MentionDetailDialog } from './mention-detail.dialog';
import { ExporteLesTemplatesDeNotesComponent } from './exporte-les-templates-de-notes.component';
import { AdminPublierLesNotesComponent } from './admin-publier-les-notes.component';
import { AdminPublierLesNotesTabComponent } from './admin-publier-les-notes-tab.component';
import { ExamenCentreDetailDialog } from './examen-centre-detail.dialog';
import { ImportLesNotesDialog } from './import-les-notes.dialog';
import { ImportLesNotesComponent } from './import-les-notes.component';
import { AdminRapportsComponent } from './admin-rapports.component';
import { GenerationDeRapportDialog } from './generation-de-rapport.dialog';
import { TelechargerRapportDialog } from './telecharger-rapport.dialog';
import { TelechargerNationalProgrammeRapportDialog } from './telecharger-national-programme-rapport.dialog';
import { ImportLesRedoublantsComponent } from './import-les-redoublants.component';
import { ImportLesRedoublantsDialog } from './import-les-redoublants.dialog';
import { ExportInscriptionsDialog } from './export-inscriptions.dialog';
import { MatieresDesExamensComponent } from './matieres-des-examens.component';
import { MatiereDesExamenDialog } from './matiere-des-examen.dialog';
import { ExportExamensDialog } from './export-examens.dialog';
import { ImportLesNotesDetailComponent } from './import-les-notes-detail.component';

export const routes = [
  {
    path: "",
    redirectTo: "inscriptions",
    pathMatch: "full",
  },
  {
    path: "inscriptions",
    component: AdminInscriptionsComponent,
    data: { breadcrumb: "Inscriptions" },
    pathMatch: "full",
  },
  {
    path: "examens",
    redirectTo: "examens/currentSession",
    pathMatch: "full",
  },
  {
    path: "examens/currentSession",
    component: AdminExamensComponent,
    data: { breadcrumb: "Session en cours" },
    pathMatch: "full",
  },
  {
    path: "examens/pastSession",
    component: AdminPastExamensComponent,
    data: { breadcrumb: "Session passées" },
    pathMatch: "full",
  },
  {
    path: "examens/report",
    component: AdminRapportsComponent,
    data: { breadcrumb: "Rapports" },
    pathMatch: "full",
  },
  {
    path: "examens/publierLesNotes",
    component: AdminPublierLesNotesComponent,
    data: { breadcrumb: "Rapports" },
    pathMatch: "full",
  },
  {
    path: "configurations",
    redirectTo: "configurations/mention",
    pathMatch: "full",
  },
  {
    path: "configurations/mention",
    component: ConfigExamensMentionsComponent,
    data: { breadcrumb: "Seuil des mentions et de passage" },
    pathMatch: "full",
  },
  {
    path: "configurations/exporter",
    component: ExporteLesTemplatesDeNotesComponent,
    data: { breadcrumb: "Exporter les templates de notes" },
    pathMatch: "full",
  },
  {
    path: "configurations/importerNote",
    component: ImportLesNotesComponent,
    data: { breadcrumb: "Configuration - Examens de sortie" },
    pathMatch: "full",
  },
  {
    path: "configurations/importerNoteDetails",
    component: ImportLesNotesDetailComponent,
    data: { breadcrumb: "Configuration - Examens de sortie" },
    pathMatch: "full",
  },
  {
    path: "configurations/centre",
    component: ConfigExamensCentresComponent,
    data: { breadcrumb: "Configuration - Examens de sortie" },
    pathMatch: "full",
  },
  {
    path: "gestions",
    component: AdminGestionComponent,
    data: { breadcrumb: "Gestion des examens" },
    pathMatch: "full",
  },
  {
    path: "configurations/importerRedoublant",
    component: ImportLesRedoublantsComponent,
    data: { breadcrumb: "Configuration - Examens de sortie" },
    pathMatch: "full",
  },
  {
    path: "configurations/matieresDesExamens",
    component: MatieresDesExamensComponent,
    data: { breadcrumb: "Configuration - Matières des examens" },
    pathMatch: "full",
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    AdminInscriptionsComponent,
    AdminExamensComponent,
    AdminGestionComponent,
    GenererCandidatsDialog,
    GenererRelevesDeNotesDialog,
    AdminExamensTabComponent,
    AdminPastExamensComponent,
    AdminPublierLesNotesTabComponent,
    AdminPublierLesNotesComponent,
    ConfigExamensTabComponent,
    ConfigExamensMentionsComponent,
    ConfigExamensCentresComponent,
    ExporteLesTemplatesDeNotesComponent,
    ImportLesNotesDialog,
    ImportLesNotesComponent,
    ImportLesNotesDetailComponent,
    ConfigExamensCentresComponent,
    MentionDetailDialog,
    ExamenCentreDetailDialog,
    AdminRapportsComponent,
    GenerationDeRapportDialog,
    TelechargerRapportDialog,
    TelechargerNationalProgrammeRapportDialog,
    ImportLesRedoublantsComponent,
    ImportLesRedoublantsDialog,
    ExportInscriptionsDialog,
    MatieresDesExamensComponent,
    MatiereDesExamenDialog,
    ExportExamensDialog,
  ]
})
export class AdminCursusModule {}
