import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  FinalExamenRapportType,
} from 'MetfpetLib';
import * as FileSaver from 'file-saver';
import { Guid } from 'guid-typescript';
import { tap } from 'rxjs/operators';

@Component({
  templateUrl: './telecharger-national-programme-rapport.dialog.html',
})
export class TelechargerNationalProgrammeRapportDialog {

  form: FormGroup;
  programmeList: Array<any>;
  rapport: MetfpetServiceAgent.FinalExamenRapportViewModel;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<TelechargerNationalProgrammeRapportDialog>,
    protected _cdRef: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.rapport = data.rapport;
    this.form = this._formBuilder.group({
      finalExamenRapportId: this.rapport.id,
      anneeAcademiqueId: this.rapport.anneeAcademiqueId,
      rapportType: this.rapport.type,
      institutionId: Guid.EMPTY,
      programmeId:  Guid.EMPTY,
      programmeName:  [null, Validators.required],
    });
  }

  ngOnInit() {
    if (this.rapport.type === <any> FinalExamenRapportType.NationalProgramme)
    {
      this._metfpetService.getNationalProgrammesForReport(this.rapport.anneeAcademiqueId).subscribe(programmes => {
        this.programmeList = [];
        this.programmeList.push({value: 'Tous', name: 'Tous'});
        for (let programme of programmes) {
          this.programmeList.push({value: programme, name: programme});
        }
        this._cdRef.markForCheck();
      });
    }
    else if (this.rapport.type === <any> FinalExamenRapportType.AdmisParProgramme)
    {
      this._metfpetService.getProgrammesForAdmisParProgrammeRapport(this.rapport.anneeAcademiqueId).subscribe(programmes => {
        this.programmeList = [];
        this.programmeList.push({value: 'Tous', name: 'Tous'});
        for (let programme of programmes) {
          this.programmeList.push({value: programme, name: programme});
        }
        this._cdRef.markForCheck();
      });
    }
    else 
    {
      this._metfpetService.getProgrammesForFinalExamenCandidateReport(this.rapport.anneeAcademiqueId).subscribe(programmes => {
        this.programmeList = [];
        this.programmeList.push({value: 'Tous', name: 'Tous'});
        for (let programme of programmes) {
          this.programmeList.push({value: programme, name: programme});
        }
        this._cdRef.markForCheck();
      });
    }
  }
  
  getZipFileName() {
    var fileName = this.rapport.typeName + '_' + this.rapport.anneeAcademiqueName + '_' + this.form.get('programmeName').value + '_' + new  Date().getTime() + '.zip';
    return fileName;
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.downloadRapport(MetfpetServiceAgent.DownloadRapportRequest.fromJS(Object.assign(this.form.value)))
        .subscribe(
          (file) => {
            const blob = new Blob([file.data], {
              type: 'application/zip'
            });
            FileSaver.saveAs(blob, this.getZipFileName());
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}