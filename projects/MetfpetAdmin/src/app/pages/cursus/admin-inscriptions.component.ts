import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent } from 'rxjs';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators/';
import {
  AppSettings,
  Settings,
  InscriptionProgrammeStatusValues, 
  BaseTableComponent,
  PerfectScrollService,
  MetfpetServiceAgent,
  BiometricStatusValues,
  RedoublementDialog,
  showLoading,
  hideLoading,
  AuthService,
  StudentYearValues,
  RedoublementStatusValues,
  RegimeValues,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { MatDialog } from '@angular/material/dialog';
import { ExportInscriptionsDialog } from './export-inscriptions.dialog';

@Component({
  templateUrl: './admin-inscriptions.component.html',
  styleUrls: ['./admin-inscriptions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminInscriptionsComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchEmail', {static: true}) searchEmail: ElementRef;
  @ViewChild('searchPhone', {static: true}) searchPhone: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;
  @ViewChild('searchMatricule', {static: true}) searchMatricule: ElementRef;

  settings: Settings;
  title: string;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  typeDiplomeList: Array<MetfpetServiceAgent.TypeDiplomeViewModel>;
  allProgramList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  statusList: Array<any>;
  biometricList: Array<any>;
  nsgBankingStatus: Array<any>;
  studentYearList: Array<any>;
  ecobankStatus: Array<any>;
  telephoneAuthentifieStatus: Array<any>;
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissement>;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  redoublementStatusList: Array<any>;
  typeProgrammeList: Array<MetfpetServiceAgent.TypeProgramme>;
  regimeList = RegimeValues;
  
  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    private _dialog: MatDialog,
    protected _perfectScrollService: PerfectScrollService,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Cursus - Gestion des inscriptions et réinscriptions';
    this.statusList = InscriptionProgrammeStatusValues;
    this.biometricList = BiometricStatusValues;
    this.studentYearList = StudentYearValues;
    this.redoublementStatusList = RedoublementStatusValues;
    this.nsgBankingStatus = [
      {value : 'Oui', name : 'Oui'},
      {value : 'Non', name : 'Non'},
    ];
    this.ecobankStatus = [
      {value : 'Oui', name : 'Oui'},
      {value : 'Non', name : 'Non'},
    ];
    this.telephoneAuthentifieStatus = [
      {value : 'Oui', name : 'Oui'},
      {value : 'Non', name : 'Non'},
    ];
    this.sort = {prop: 'identifiantNationalEleve', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cd.markForCheck(); 
    });

    this._metfpetService.getTypeProgrammeList().subscribe(data => {
      this.typeProgrammeList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getStatusEtablissementList().subscribe(data => {
      this.statusEtablissementList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.allProgramList = data.results;
      this.programList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getTypeDiplomeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
   })).subscribe(data => {
     this.typeDiplomeList = data.results;
     this._cd.markForCheck();
   });

    this.searchForm.get('institution').valueChanges.pipe(
      startWith(this.searchForm.get('institution').value),
      tap(institutionId => {
        this.programList = this.allProgramList;
        if (!this.isFirstSearch) {
          this.searchForm.get('programme').setValue(null);
        }
        if (!institutionId) {
          return;
        }
        this._loadPrograms(institutionId);
      })
    ).subscribe();

    combineLatest([
      this.searchForm.get('status').valueChanges
        .pipe(
          startWith(this.searchForm.get('status').value)
        ),
      this.searchForm.get('programme').valueChanges
        .pipe(
          startWith(this.searchForm.get('programme').value)
        ),
      this.searchForm.get('anneeAcademique').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademique').value)
        ),
      this.searchForm.get('niveauEtude').valueChanges
        .pipe(
          startWith(this.searchForm.get('niveauEtude').value)
        ),
        this.searchForm.get('typeDiplome').valueChanges
        .pipe(
          startWith(this.searchForm.get('typeDiplome').value)
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchMatricule.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('matricule').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      fromEvent(
        this.searchEmail.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('email').value}
            })
          ),
      fromEvent(
        this.searchPhone.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('phone').value}
            })
          ),
      this.searchForm.get('statusEtablissement').valueChanges
        .pipe(
          startWith(this.searchForm.get('statusEtablissement').value)
        ),
      this.searchForm.get('biometricStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('biometricStatus').value)
        ),
      this.searchForm.get('nsgBanking').valueChanges
        .pipe(
          startWith(this.searchForm.get('nsgBanking').value)
        ),
      this.searchForm.get('studentYear').valueChanges
        .pipe(
          startWith(this.searchForm.get('studentYear').value)
        ),
      this.searchForm.get('residencePrefecture').valueChanges
        .pipe(
          startWith(this.searchForm.get('residencePrefecture').value)
        ),
      this.searchForm.get('ecobank').valueChanges
        .pipe(
          startWith(this.searchForm.get('ecobank').value)
        ),
      this.searchForm.get('telephoneAuthentifie').valueChanges
        .pipe(
          startWith(this.searchForm.get('telephoneAuthentifie').value)
        ),
      this.searchForm.get('redoublementStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('redoublementStatus').value)
        ),
      this.searchForm.get('typeProgramme').valueChanges
        .pipe(
          startWith(this.searchForm.get('typeProgramme').value)
        ),
      this.searchForm.get('regime').valueChanges
        .pipe(
          startWith(this.searchForm.get('regime').value)
        )
    ])
    .subscribe((
      [
        status,
        programme,
        anneeAcademique,
        niveauEtude,
        typeDiplome,
        eventSearchNationalStudentIdentifier,
        eventsearchMatricule, 
        eventSearchName, 
        eventFirstname,
        eventSearchEmail,
        eventSearchPhone,
        statusEtablissement,
        biometricStatus,
        nsgBanking,
        studentYear,
        residencePrefecture,
        ecobank,
        telephoneAuthentifie,
        redoublementStatus,
        typeProgramme,
        regime,
      ]) => {
      this.searchForm.patchValue({
        status,
        programme,
        anneeAcademique,
        niveauEtude,
        typeDiplome,
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        matricule: eventsearchMatricule ? eventsearchMatricule['target'].value : null, 
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null, 
        email: eventSearchEmail ? eventSearchEmail['target'].value : null, 
        phone: eventSearchPhone ? eventSearchPhone['target'].value : null, 
        statusEtablissement,
        biometricStatus,
        nsgBanking,
        studentYear,
        residencePrefecture,
        ecobank,
        telephoneAuthentifie,
        redoublementStatus,
        typeProgramme,
        regime,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      status: null,
      identifiantNationalEleve: null,
      matricule: null,
      institution: null,
      programme: null,
      anneeAcademique: null,
      niveauEtude: null,
      name: null,
      firstName: null,
      statusEtablissement: null,
      biometricStatus: null,
      typeDiplome: null,
      email: null,
      phone: null,
      nsgBanking: null,
      studentYear: null,
      residencePrefecture: null,
      telephoneAuthentifie: null,
      ecobank: null,
      redoublementStatus: null,
      typeProgramme: null,
      regime: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getInscriptionProgrammeListPage(criteria);
  }
  
  open(row: MetfpetServiceAgent.InscriptionProgrammeRowViewModel) {
    this._router.navigate([`${UiPath.admin.students.dossier}/${row.etudiantId}`], { state: { previousPage: 'Cursus' } });
  }

  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {institution: institutionId}
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });
  }

  redoublement() {
    this._dialog.open(RedoublementDialog , {
      width: '800px',
      data: {
        anneeAcademiqueList: this.anneeAcademiqueList
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  exportInscriptions() {
    this._dialog.open(ExportInscriptionsDialog, {
      width: '650px',
      data: {
        filters: this.searchForm.value
      }
    });
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }

  public isDirectionsNationales() {
    return this._authService.getUserType() == 'directionsNationales';
  }
}