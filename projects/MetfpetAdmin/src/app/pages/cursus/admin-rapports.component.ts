import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent } from 'rxjs';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators/';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  PerfectScrollService,
  MetfpetServiceAgent,
  FinalExamenRapportTypeValues,
  FinalExamenRapportType,
  AuthService,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { Tabs } from './admin-examens-tab.component';
import { MatDialog } from '@angular/material/dialog';
import { GenerationDeRapportDialog } from './generation-de-rapport.dialog';
import { TelechargerRapportDialog } from './telecharger-rapport.dialog';
import { TelechargerNationalProgrammeRapportDialog } from './telecharger-national-programme-rapport.dialog';

@Component({
  templateUrl: './admin-rapports.component.html',
  styleUrls: ['./admin-rapports.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminRapportsComponent extends BaseTableComponent {

  settings: Settings;
  title: string;
  navs = Tabs;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  reportTypeList: Array<any>;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    private _dialog: MatDialog,
    protected _perfectScrollService: PerfectScrollService,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Examens de sortie - Rapports';
    this.reportTypeList = FinalExamenRapportTypeValues;
    this.sort = {prop: 'typeName', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    combineLatest([
      this.searchForm.get('anneeAcademique').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademique').value)
        ),
      this.searchForm.get('type').valueChanges
        .pipe(
          startWith(this.searchForm.get('type').value)
        ),
    ])
    .subscribe((
      [
        anneeAcademique,
        type,
      ]) => {
      this.searchForm.patchValue({
        anneeAcademique,
        type,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      anneeAcademique: null,
      type: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getFinalExamenRapportListPage(criteria);
  }
  
  open(row: MetfpetServiceAgent.FinalExamenRapportViewModel) {
    if (row.type == <any>FinalExamenRapportType.NationalProgramme ||
        row.type == <any>FinalExamenRapportType.ListeDesCandidatsPdf ||
        row.type == <any>FinalExamenRapportType.AdmisParProgramme) 
    {
      this._dialog.open(TelechargerNationalProgrammeRapportDialog, {
        width: '650px',
        data: {
          rapport: row
        }
      });
    }
    else 
    {
      this._dialog.open(TelechargerRapportDialog, {
        width: '650px',
        data: {
          rapport: row
        }
      });
    }
  }

  generateReport() {
    this._dialog.open(GenerationDeRapportDialog, {
      width: '650px',
      data: {
      }
    }).afterClosed().subscribe(data => {
      this.triggerSearch();
    });
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }

  public isAdministrateur() {
    return this._authService.getUserType() === 'administrateur';
  }
}