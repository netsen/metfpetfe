import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { id } from '@swimlane/ngx-datatable';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  FinalExamenRapportTypeValues,
  FinalExamenRapportType,
  ExportService,
} from 'MetfpetLib';
import { UserIdleService } from 'angular-user-idle';

@Component({
  templateUrl: './generation-de-rapport.dialog.html',
})
export class GenerationDeRapportDialog {

  form: FormGroup;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  reportTypeList: Array<any>;
  processing: boolean;
  processed: number;
  totalToprocess: number;
  showWarningMessage: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<GenerationDeRapportDialog>,
    protected _cdRef: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _userIdleService: UserIdleService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.processing = false;
    this.reportTypeList = FinalExamenRapportTypeValues;
    this.form = this._formBuilder.group({
      anneeAcademiqueId: [null, Validators.required],
      reportType:  [null, Validators.required],
    });
  }

  ngOnInit() {
    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cdRef.markForCheck();
    });
  }

  async generateRelevesDeNotes(ids: Array<string>) {
    for (let id of ids) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.generateTranscript(MetfpetServiceAgent.GenerateTranscriptRequest.fromJS({
          finalExamenEtudiantId: id,
        })).toPromise();
      } catch(error) {
      }
    }
    this.postProcessing();
  }

  async generateListeDesCandidats(ids: Array<string>) {
    let exportedList = new Array<MetfpetServiceAgent.ExportFinalExamenEtudiantViewModel>();
    for (let id of ids) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        let vm = await this._metfpetService.getFinalExamenEtudiant(id).toPromise();
        exportedList.push(vm);
      } catch(error) {
      }
    }
    this._exportService.exportListeDesFinalExamenCandidats(exportedList);
    this.postProcessing();
  }

  private postProcessing() {
    this.processing = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    this._store.dispatch(hideLoading());
  }

  async generateListeDesRedoublants(ids: Array<string>) {
    for (let id of ids) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.generateFinalExamenRapport(MetfpetServiceAgent.GenerateFinalExamenRapportRequest.fromJS({
          programmeId: id,
          anneeAcademiqueId: this.form.get('anneeAcademiqueId').value,
          reportType: this.form.get('reportType').value,
        })).toPromise();
      } catch(error) {
      }
    }
    this.postProcessing();
  }

  async generateListeDesAdmis(ids: Array<string>) {
    for (let id of ids) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.generateFinalExamenRapport(MetfpetServiceAgent.GenerateFinalExamenRapportRequest.fromJS({
          programmeId: id,
          anneeAcademiqueId: this.form.get('anneeAcademiqueId').value,
          reportType: this.form.get('reportType').value,
        })).toPromise();
      } catch(error) {
      }
    }
    this.postProcessing();
  }

  async generateSurLesResultats(ids: Array<string>) {
    let exportedList = new Array<MetfpetServiceAgent.ExportFinalExamenEtudiantViewModel>();
    for (let id of ids) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        let vm = await this._metfpetService.getFinalExamenEtudiant(id).toPromise();
        exportedList.push(vm);
      } catch(error) {
      }
    }
    this._exportService.exportSurLesResultats(exportedList);
    this.postProcessing();
  }

  async generateNationalProgrammes(programmes: Array<string>) {
    for (let programme of programmes) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.generateNationalProgrammeRapport(MetfpetServiceAgent.GenerateNationalProgrammeRapportRequest.fromJS({
          anneeAcademiqueId: this.form.get('anneeAcademiqueId').value,
          programmeName: programme,
        })).toPromise();
      } catch(error) {
      }
    }
    this.postProcessing();
  }

  async generateListeDesCandidatsPdf(items: Array<MetfpetServiceAgent.FinalExamenCandidateReportInfo>) {
    for (let item of items) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.generateFinalExamenCandidateReport(MetfpetServiceAgent.FinalExamenCandidateReportRequest.fromJS({
          anneeAcademiqueId: this.form.get('anneeAcademiqueId').value,
          programmeName: item.programmeName,
          finalExamenCenterId: item.finalExamenCenterId,
        })).toPromise();
      } catch(error) {
      }
    }
    this.postProcessing();
  }

  async generateListeDesCandidatsPerInstitutionProgrammePdf(items: Array<MetfpetServiceAgent.FinalExamenCandidatePerInstitutionProgrammeReportInfo>) {
    for (let item of items) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.generateFinalExamenCandidatePerInstitutionProgrammeReport(MetfpetServiceAgent.FinalExamenCandidatePerInstitutionProgrammeReportRequest.fromJS({
          anneeAcademiqueId: this.form.get('anneeAcademiqueId').value,
          programmeId: item.programmeId,
          institutionId: item.institutionId,
          finalExamenCenterId: item.finalExamenCenterId,
        })).toPromise();
      } catch(error) {
      }
    }
    this.postProcessing();
  }

  async generateAdmisParProgramme(programmes: Array<string>) {
    for (let programme of programmes) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.generateAdmisParProgrammeRapport(MetfpetServiceAgent.GenerateNationalProgrammeRapportRequest.fromJS({
          anneeAcademiqueId: this.form.get('anneeAcademiqueId').value,
          programmeName: programme,
        })).toPromise();
      } catch(error) {
      }
    }
    this.postProcessing();
  }

  async generateStatistiques(rows: Array<any>, isBeforePublish: boolean) {
    let exportedList = new Array<MetfpetServiceAgent.FinalExamenStatistiquesInfo>();
    try {
      let allProgrammes = await this._metfpetService.getProgrammesForFinalExamenCandidateReport(this.form.get('anneeAcademiqueId').value).toPromise();
      this.totalToprocess = allProgrammes.length;
      this._cdRef.markForCheck();
      for (let row of rows) {    
        try {
          let programmes = await this._metfpetService.getProgrammesForStatistiquesReport(MetfpetServiceAgent.FinalExamenStatistiquesRequest.fromJS({
            anneeAcademiqueId: this.form.get('anneeAcademiqueId').value, 
            typeDiplomeId: row.id,
            isBeforePublish: isBeforePublish
          })).toPromise();
          let order: number = 0;
          for (let programme of programmes)
          {
            try {
              this.processed++;
              this._cdRef.markForCheck();
              let vm = await this._metfpetService.getFinalExamenStatistiquesForProgram(MetfpetServiceAgent.FinalExamenStatistiquesRequest.fromJS({
                anneeAcademiqueId: this.form.get('anneeAcademiqueId').value, 
                typeDiplomeId: row.id,
                programName: programme,
                isBeforePublish: isBeforePublish
              })).toPromise();
              order++;
              vm.orderNumber = order.toString();
              exportedList.push(vm);
            } catch(error) {
            }
          }
          if (order > 0)
          {
            let subTotal = await this._metfpetService.getFinalExamenStatistiquesForTypeDiplome(MetfpetServiceAgent.FinalExamenStatistiquesRequest.fromJS({
              anneeAcademiqueId: this.form.get('anneeAcademiqueId').value, 
              typeDiplomeId: row.id,
              isBeforePublish: isBeforePublish
            })).toPromise();
            exportedList.push(subTotal);
          }
        } catch(error) {
        }
      }

      let total = await this._metfpetService.getFinalExamenStatistiquesTotal(MetfpetServiceAgent.FinalExamenStatistiquesRequest.fromJS({
        anneeAcademiqueId: this.form.get('anneeAcademiqueId').value,
        isBeforePublish: isBeforePublish
      })).toPromise();
      exportedList.push(total);
    } catch(error) {
    }
    this._exportService.exportFinalExamenStatistiques(exportedList, isBeforePublish);
    this.postProcessing();
  }

  generate(ids: Array<any>) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this._store.dispatch(showLoading());
    this.showWarningMessage = true;
    this.processed = 0;
    if (this.form.get('reportType').value !== <any>FinalExamenRapportType.Statistiques)
    {
      this.totalToprocess = ids.length;
      this._cdRef.markForCheck();
    }

    switch(this.form.get('reportType').value) {
      case <any>FinalExamenRapportType.RelevesDeNotes:
        this.generateRelevesDeNotes(ids);
        break;
      case <any>FinalExamenRapportType.ListeDesCandidats:
        this.generateListeDesCandidats(ids);
        break;
      case <any>FinalExamenRapportType.ListeDesRedoublants:
        this.generateListeDesRedoublants(ids);
        break;
      case <any>FinalExamenRapportType.ListeDesAdmis:
        this.generateListeDesAdmis(ids);
        break;
      case <any>FinalExamenRapportType.SurLesResultats:
        this.generateSurLesResultats(ids);
        break;
      case <any>FinalExamenRapportType.NationalProgramme:
        this.generateNationalProgrammes(ids);
        break;
      case <any>FinalExamenRapportType.ListeDesCandidatsPdf:
        this.generateListeDesCandidatsPdf(ids);
        break;
      case <any>FinalExamenRapportType.AdmisParProgramme:
        this.generateAdmisParProgramme(ids);
        break;
      case <any>FinalExamenRapportType.Statistiques:
        this.generateStatistiques(ids, false);
        break;
      case <any>FinalExamenRapportType.StatistiquesBeforePublish:
        this.generateStatistiques(ids, true);
        break;
      case <any>FinalExamenRapportType.ListeDesCandidatsByProgrammeAndInstitutionPdf:
        this.generateListeDesCandidatsPerInstitutionProgrammePdf(ids);
        break;
    }
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      if (this.form.get('reportType').value == <any>FinalExamenRapportType.NationalProgramme) {
        this._metfpetService.getNationalProgrammesForReport(this.form.get('anneeAcademiqueId').value)
        .subscribe(
          (programmes) => {
            this.generate(programmes);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
      else if (this.form.get('reportType').value == <any>FinalExamenRapportType.AdmisParProgramme) {
        this._metfpetService.getProgrammesForAdmisParProgrammeRapport(this.form.get('anneeAcademiqueId').value)
        .subscribe(
          (programmes) => {
            this.generate(programmes);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
      else if (this.form.get('reportType').value == <any>FinalExamenRapportType.ListeDesCandidatsPdf) {
        this._metfpetService.getAllFinalExamenCandidateReportInfos(this.form.get('anneeAcademiqueId').value)
        .subscribe(
          (programmes) => {
            this.generate(programmes);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
      else if (this.form.get('reportType').value == <any>FinalExamenRapportType.ListeDesCandidatsByProgrammeAndInstitutionPdf) {
        this._metfpetService.getAllFinalExamenCandidatePerInstitutionProgrammeReportInfos(this.form.get('anneeAcademiqueId').value)
        .subscribe(
          (programmes) => {
            this.generate(programmes);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
      else if (this.form.get('reportType').value == <any>FinalExamenRapportType.Statistiques ||
               this.form.get('reportType').value == <any>FinalExamenRapportType.StatistiquesBeforePublish) {
        this._metfpetService.getTypeDiplomeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
          pageIndex: -1, filters: {}
        }))
        .subscribe(
          (data) => {
            this.generate(data.results);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
      else
      {
        this._metfpetService.loadDataForReportGeneration(MetfpetServiceAgent.ReportGenerationRequest.fromJS(Object.assign(this.form.value)))
        .subscribe(
          (ids) => {
            this.generate(ids);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}