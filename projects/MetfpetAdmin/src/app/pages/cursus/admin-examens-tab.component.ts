import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

export const Tabs = [
  { 
    routerLink: '/' + UiPath.admin.cursus.examens.currentSession, 
    label: 'Session en cours' 
  },
  {
    routerLink: '/' + UiPath.admin.cursus.examens.pastSession, 
    label: 'Session passées' 
  },
  {
    routerLink: '/' + UiPath.admin.cursus.examens.report, 
    label: 'Rapports' 
  }
];

@Component({
  templateUrl: './admin-examens-tab.component.html',
})
export class AdminExamensTabComponent {

  navs = Tabs;
  title: string;
  routerEventSubscription: Subscription;
  
  constructor(private router: Router) {
    this.title = 'Examens de sortie';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = selectedTab.label;
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
