import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import * as FileSaver from 'file-saver';
import { tap } from 'rxjs/operators';

@Component({
  templateUrl: './telecharger-rapport.dialog.html',
})
export class TelechargerRapportDialog {

  form: FormGroup;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programmeList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  rapport: MetfpetServiceAgent.FinalExamenRapportViewModel;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<TelechargerRapportDialog>,
    protected _cdRef: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.rapport = data.rapport;
    this.form = this._formBuilder.group({
      finalExamenRapportId: this.rapport.id,
      anneeAcademiqueId: this.rapport.anneeAcademiqueId,
      rapportType: this.rapport.type,
      institutionId: [null, Validators.required],
      programmeId:  [null, Validators.required],
    });

    this.form.get('institutionId').valueChanges.pipe(
      tap(institutionId => {
        this.programmeList = [];
        this.form.get('programmeId').setValue(null);
        if (!institutionId) {
          return;
        }
        this._loadPrograms(institutionId);
      })
    ).subscribe();
  }

  ngOnInit() {
    this._metfpetService.getInstitutionsForReportDownload(this.rapport).subscribe(data => {
      this.institutionList = data;
      this._cdRef.markForCheck();
    });
  }

  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammesForReportDownload(MetfpetServiceAgent.GetProgrammeForReportDownloadRequest.fromJS({
      finalExamenRapportId: this.rapport.id,
      anneeAcademiqueId: this.rapport.anneeAcademiqueId,
      rapportType: this.rapport.type,
      institutionId: institutionId
      })).subscribe(data => {
        this.programmeList = data;
        this._cdRef.markForCheck();
      });
  }
  
  getZipFileName() {
    var fileName = this.rapport.typeName + '_' + this.rapport.anneeAcademiqueName;
    if (this.rapport.typeName == 'Liste d’affichage des candidats aux examens par programme et institution (PDF)')
    {
      fileName = "Candidats_Examens_" + this.rapport.anneeAcademiqueName;
    }
    var institution = this.institutionList.find(x => x.id == this.form.get('institutionId').value);
    if (institution) {
      fileName = fileName + '_' + institution.name;
    }
    var programme = this.programmeList.find(x => x.id == this.form.get('programmeId').value);
    if (programme) {
      fileName = fileName + '_' + programme.name;
    }
    fileName = fileName + '_' + new  Date().getTime() + '.zip';
    return fileName.replace(' ', '_').replace('’', '');
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.downloadRapport(MetfpetServiceAgent.DownloadRapportRequest.fromJS(Object.assign(this.form.value)))
        .subscribe(
          (file) => {
            const blob = new Blob([file.data], {
              type: 'application/zip'
            });
            FileSaver.saveAs(blob, this.getZipFileName());
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}