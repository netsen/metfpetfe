import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { startWith } from 'rxjs/operators/';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  PerfectScrollService,
  MetfpetServiceAgent,
} from 'MetfpetLib';
import { Tabs } from './config-examens-tab.component';
import { MatDialog } from '@angular/material/dialog';
import { ExamenCentreDetailDialog } from './examen-centre-detail.dialog';

@Component({
  templateUrl: './config-examens-centres.component.html',
  styleUrls: ['./config-examens-centres.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfigExamensCentresComponent extends BaseTableComponent {

  @ViewChild('searchCenterName', {static: true}) searchCenterName: ElementRef;

  settings: Settings;
  title: string;
  navs = Tabs;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  institutionDescriptions: Array<MetfpetServiceAgent.InstitutionDescription>;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialog: MatDialog,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Configuration - Examens de sortie - Configurer les centres d’examen';
    this.sort = {prop: 'name', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();
    this._metfpetService.getInstitutionListPage(
        MetfpetServiceAgent.PagingSearchDTO.fromJS({
          pageIndex: -1,
          filters: {},
        })
      )
      .subscribe((data) => {
        this.institutionList = data.results;
        this._cd.markForCheck();
      });
    this._metfpetService.getInstitutionList()
      .subscribe((data) => {
        this.institutionDescriptions = data;
        this._cd.markForCheck();
      });
  }

  ngAfterViewInit() {
    combineLatest([
      this.searchForm.get('searchName').valueChanges.pipe(startWith(this.searchForm.get('searchName').value)),
      this.searchForm.get('institution').valueChanges.pipe(startWith(this.searchForm.get('institution').value)),
      this.searchForm.get('isActive').valueChanges.pipe(startWith(this.searchForm.get('isActive').value)),
    ])
    .subscribe(([searchName, institution, isActive]) => {
      this.searchForm.patchValue({searchName, institution, isActive}, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      searchName: null,
      institution: null,
      isActive: true,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    criteria.filters.isActive = +criteria.filters.isActive;
    MetfpetServiceAgent.FinalExamenCentreStatus.Active;
    return this._metfpetService.getFinalExamenCentreListPaged(criteria);
  }
  
  createExamenCentre(){
    this._dialog.open(ExamenCentreDetailDialog , {
      width: '800px',
      data: {
        institutionList: this.institutionDescriptions
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  open(row: MetfpetServiceAgent.FinalExamenCenterRowViewModel) {
    this._dialog.open(ExamenCentreDetailDialog , {
      width: '800px',
      data: {
        id: row.id,
        institutionList: this.institutionDescriptions
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

}