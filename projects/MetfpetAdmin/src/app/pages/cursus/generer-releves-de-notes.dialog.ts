import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  FinalExamenRapportType,
} from 'MetfpetLib';

@Component({
  templateUrl: './generer-releves-de-notes.dialog.html',
})
export class GenererRelevesDeNotesDialog {

  form: FormGroup;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<GenererRelevesDeNotesDialog>,
    protected _cdRef: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.form = this._formBuilder.group({
      anneeAcademiqueId: [null, Validators.required],
      reportType: <any>FinalExamenRapportType.RelevesDeNotes
    });
  }

  ngOnInit() {
    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cdRef.markForCheck();
    });
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.loadDataForReportGeneration(MetfpetServiceAgent.ReportGenerationRequest.fromJS(Object.assign(this.form.value)))
        .subscribe(
          (data) => {
            this._dialogRef.close({
              finalExamenEtudiantIds: data,
              anneeAcademiqueId: this.form.get('anneeAcademiqueId').value
            });
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}