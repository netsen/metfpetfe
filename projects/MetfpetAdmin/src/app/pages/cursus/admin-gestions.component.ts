import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent } from 'rxjs';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators/';
import {
  AppSettings,
  Settings,
  PerfectScrollService,
  MetfpetServiceAgent,
  DialogService,
  showLoading,
  showSuccess,
  showException,
  hideLoading,
  MessageType,
} from 'MetfpetLib';
import { MatDialog } from '@angular/material/dialog';
import { UserIdleService } from 'angular-user-idle';
import { GenererCandidatsDialog } from './generer-candidats.dialog';
import { GenererRelevesDeNotesDialog } from './generer-releves-de-notes.dialog';

@Component({
  templateUrl: './admin-gestions.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminGestionComponent {

  settings: Settings;
  currentAnneeAcademique: MetfpetServiceAgent.AnneeAcademiqueDTO;
  finalExamenMessageStatus: MetfpetServiceAgent.FinalExamenMessageStatus;
  processing: boolean;
  processed: number;
  totalToprocess: number;
  showWarningMessage: boolean;
  title: string;

  constructor(
    public appSettings: AppSettings,
    protected _cdRef: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialog: MatDialog,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService) 
  {
    this.settings = this.appSettings.settings;
    this.processing = false;
    this.title = 'Gestion des examens';
  }

  ngOnInit() {
    this.refreshAnneAcademique();
  }
  
  refreshAnneAcademique() {
    this._metfpetService.getAnneeAcademiqueAdmissionOuverte().subscribe((data : any) => {
      if (data) {
        this.currentAnneeAcademique = data;
        this.getFinalExamenMessageStatus();
        this._cdRef.markForCheck();
      }
    });
  }

  async process(inscriptionIds: Array<string>, anneeAcademiqueId: any) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this.showWarningMessage = true;
    this.processed = 0;
    this.totalToprocess = inscriptionIds.length;

    for (let inscriptionId of inscriptionIds) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.genererCandidat(inscriptionId).toPromise();
      } catch(error) {
      }
    }

    this._metfpetService.updateDateDerniereGenererCandidats(anneeAcademiqueId).subscribe((data : any) => {
      if (data) {
        this.currentAnneeAcademique = data;
        this._cdRef.markForCheck();
      }
    });

    this.processing = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
  }

  generateNewCandidates() {
    this._dialog.open(GenererCandidatsDialog, {
      width: '600px',
      data: {
        currentAnneeAcademique: this.currentAnneeAcademique
      }
    }).afterClosed().subscribe(data => {
      if (data) {
        this.process(data.inscriptionIds, data.anneeAcademiqueId);
      }
    });
  }

  async processGenerateTranscripts(finalExamenEtudiantIds: Array<string>) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this.showWarningMessage = true;
    this.processed = 0;
    this.totalToprocess = finalExamenEtudiantIds.length;

    for (let finalExamenEtudiantId of finalExamenEtudiantIds) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.generateTranscript(MetfpetServiceAgent.GenerateTranscriptRequest.fromJS({
          finalExamenEtudiantId: finalExamenEtudiantId,
        })).toPromise();
      } catch(error) {
      }
    }

    this.processing = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
  }

  generateTranscripts() {
    this._dialog.open(GenererRelevesDeNotesDialog, {
      width: '650px',
      data: {
      }
    }).afterClosed().subscribe(data => {
      if (data) {
        this.processGenerateTranscripts(data.finalExamenEtudiantIds);
      }
    });
  }

  archiveFailedStudents() {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Confirmation de publication", 
        message: "Voulez vous vraiment archiver le statut des apprenants n’ayant pas terminés le processus d’examen de sortie ? Attention cette action est irréversible.", 
        confirmBtnText: 'Archiver'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.archiveNotCompletedStudents(this.currentAnneeAcademique.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({}));
            },
            error => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  getFinalExamenMessageStatus() {
    this._metfpetService.getFinalExamenMessageStatus(this.currentAnneeAcademique.id).subscribe((data : any) => {
      if (data) {
        this.finalExamenMessageStatus = data;
        this._cdRef.markForCheck();
      }
    });
  }

  sendExamenCandidatureMessage() {
    this.sendExamenMessage(MessageType.ExamenCandidature);
  }

  sendExamenResultatsMessage() {
    this.sendExamenMessage(MessageType.ExamenResultats);
  }

  sendExamenMessage(messageType: MessageType) {
    this._metfpetService.getFinalExamenEtudiantIdsToSentMessage(MetfpetServiceAgent.FinalExamenMessageRequest.fromJS({
      messageType: messageType,
      anneeAcademiqueId: this.currentAnneeAcademique.id
    })).subscribe(finalExamenEtudiantIds => {
      if (finalExamenEtudiantIds) {
        this.processSendExamenMessage(finalExamenEtudiantIds, messageType);
      }
    });
  }

  async processSendExamenMessage(finalExamenEtudiantIds: Array<string>, messageType: MessageType) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this.showWarningMessage = true;
    this.processed = 0;
    this.totalToprocess = finalExamenEtudiantIds.length;

    for (let finalExamenEtudiantId of finalExamenEtudiantIds) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.sendFinalExamenMessage(MetfpetServiceAgent.FinalExamenMessageRequest.fromJS({
          finalExamenEtudiantId: finalExamenEtudiantId,
          messageType: messageType,
          anneeAcademiqueId: this.currentAnneeAcademique.id
        })).toPromise();
      } catch(error) {
      }
    }

    this._metfpetService.updateDateSentFinalExamenMessage(MetfpetServiceAgent.FinalExamenMessageRequest.fromJS({
      messageType: messageType,
      anneeAcademiqueId: this.currentAnneeAcademique.id
    })).subscribe((data : any) => {
      if (data) {
        this.currentAnneeAcademique = data;
        this._cdRef.markForCheck();
      }
    });

    this.getFinalExamenMessageStatus();

    this.processing = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
  }
}