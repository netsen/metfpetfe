import { Component, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Store } from "@ngrx/store";
import { validateForm, MetfpetServiceAgent, showLoading } from "MetfpetLib";


@Component({
  templateUrl: "./mention-detail.dialog.html",
  styleUrls: ["./mention-detail.dialog.scss"],
})
export class MentionDetailDialog {
  mentionId: string;
  isEditMode: boolean = false;
  title: string;
  form: FormGroup;
  hasChanges: boolean = false;
  model: MetfpetServiceAgent.MentionDTO;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<MentionDetailDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.title = "Ajouter une mention";
    this.form = this._formBuilder.group({
      name: ["", Validators.required],
      moyenneTotalMin: ["", Validators.required],
    });

    this.parseInputData(data);
  }

  private parseInputData(data: any) {
    if (data.id) {
      this.isEditMode = true;
      this.mentionId = data.id;

      this._metfpetService.getMention(this.mentionId)
        .subscribe(res => {
          this.model = res;
          this.form.patchValue({
            name: this.model.name,
            moyenneTotalMin: this.model.moyenneTotalMin,
          });
        });
    }
  }

  onConfirm() {
    validateForm(this.form);
    if (!this.form.valid) return;

    this.model = Object.assign({}, this.model, this.form.value);

    if (this.isEditMode) {
      this._metfpetService.updateMention(this.model)
      .subscribe(res => {
        this.hasChanges = true;
        this._dialogRef.close(this.hasChanges);
      });
    }
    else {
      this._metfpetService.createMention(this.model)
        .subscribe(res => {
          this.hasChanges = true;
          this._dialogRef.close(this.hasChanges);
        });
    }
  }

  onClose() {
    this._dialogRef.close(this.hasChanges);
  }
}
