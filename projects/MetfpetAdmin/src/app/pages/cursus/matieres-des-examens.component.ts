import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { startWith } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent,
  MetfpetServiceAgent,
  PerfectScrollService,
  DialogService,
  showLoading,
  hideLoading,
  ExportService,
  TypeExamenValues,
} from 'MetfpetLib';
import { Tabs } from './config-examens-tab.component';
import { MatiereDesExamenDialog } from './matiere-des-examen.dialog';

@Component({
  templateUrl: './matieres-des-examens.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MatieresDesExamensComponent extends BaseTableComponent {

  settings: Settings;
  title: string;
  navs = Tabs;
  typeExamesList: Array<any>;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Configuration - Examens de sortie - Matières des examens';
    this.sort = {prop: 'name', dir: 'desc'};
    this.typeExamesList = TypeExamenValues;
  }

  ngAfterViewInit() {
    combineLatest([
      this.searchForm.get('name').valueChanges.pipe(startWith(this.searchForm.get('name').value)),
      this.searchForm.get('typeExamen').valueChanges.pipe(startWith(this.searchForm.get('typeExamen').value)),
    ])
    .subscribe(([name, typeExamen]) => {
      this.searchForm.patchValue({name, typeExamen}, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null,
      typeExamen: null,   
      isActive: true,
      hasFinalExamen: true
    });
  }

  protected _search(criteria: any): Observable<any> {
    // to convert True/false value to number 1/0
    criteria.filters.isActive = +criteria.filters.isActive;
    return this._metfpetService.getProgrammeListForFinalExamen(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  public configMatiereDesExamen(row: MetfpetServiceAgent.ProgrammeRowViewModel) {
    this._dialogService.openDialog(
      MatiereDesExamenDialog,
      {
        width: '800px',
        data: {
          programmeId: row.id
        }
      }
    ).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }
}