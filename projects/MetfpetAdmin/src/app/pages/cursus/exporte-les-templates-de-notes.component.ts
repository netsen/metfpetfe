import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { startWith } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent,
  MetfpetServiceAgent,
  PerfectScrollService,
  DialogService,
  showLoading,
  hideLoading,
  ExportService,
  TypeExamenValues,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { Tabs } from './config-examens-tab.component';

@Component({
  templateUrl: './exporte-les-templates-de-notes.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExporteLesTemplatesDeNotesComponent extends BaseTableComponent {

  settings: Settings;
  title: string;
  navs = Tabs;
  typeExamesList: Array<any>;
  onlyProgramWithSubjectOrStudentList: Array<any>;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Configuration - Examens de sortie - Exporter les templates de notes';
    this.sort = {prop: 'name', dir: 'desc'};
    this.typeExamesList = TypeExamenValues;
    this.onlyProgramWithSubjectOrStudentList = [
      {value : 'Oui', name : 'Oui'},
      {value : 'Non', name : 'Non'},
      {value : 'Tous', name : 'Tous'},
    ];
  }

  ngAfterViewInit() {
    combineLatest([
      this.searchForm.get('name').valueChanges.pipe(startWith(this.searchForm.get('name').value)),
      this.searchForm.get('typeExamen').valueChanges.pipe(startWith(this.searchForm.get('typeExamen').value)),
      this.searchForm.get('onlyProgramWithSubject').valueChanges.pipe(startWith(this.searchForm.get('onlyProgramWithSubject').value)),
      this.searchForm.get('onlyProgramWithStudent').valueChanges.pipe(startWith(this.searchForm.get('onlyProgramWithStudent').value)),
    ])
    .subscribe(([name, typeExamen]) => {
      this.searchForm.patchValue({name, typeExamen}, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null,
      typeExamen: null,   
      isActive: true,
      hasFinalExamen: true,
      onlyProgramWithSubject: null,
      onlyProgramWithStudent: null,
    });
  }

  protected _search(criteria: any): Observable<any> {
    // to convert True/false value to number 1/0
    criteria.filters.isActive = +criteria.filters.isActive;
    return this._metfpetService.getProgrammeListForFinalExamen(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  public exportTemplate(row: MetfpetServiceAgent.ProgrammeRowViewModel) {
    this._store.dispatch(showLoading());
        
    this._metfpetService.getFinalExamenMatieresForProgram(MetfpetServiceAgent.GetFinalExamenMatiereRequest.fromJS({
      programName: row.name,
      typeExamen: row.typeExamen
    }))
      .subscribe(data => this._exportService.exportLesTemplatesDeNotes(data))
      .add(() => this._store.dispatch(hideLoading()));
  }

  public async exportAllTemplates() {
    this._store.dispatch(showLoading());
    let data = await this._metfpetService.getAllExportFinalExamenMatieres(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1,
      filters: this.searchForm.value,
    })).toPromise();
    await this._exportService.exportAllLesTemplatesDeNotes(data);
    this._store.dispatch(hideLoading());
  }
}