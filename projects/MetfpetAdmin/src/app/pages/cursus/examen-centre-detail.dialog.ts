import { ChangeDetectorRef, Component, Inject, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Store } from "@ngrx/store";
import { validateForm, MetfpetServiceAgent, FinalExamenCentreStatus } from "MetfpetLib";


@Component({
  templateUrl: "./examen-centre-detail.dialog.html",
  styleUrls: ["./examen-centre-detail.dialog.scss"],
})
export class ExamenCentreDetailDialog {
  examenCentreId: string;
  isEditMode: boolean = false;
  title: string;
  form: FormGroup;
  hasChanges: boolean = false;  
  institutionList: Array<MetfpetServiceAgent.InstitutionDescription>;
  model: MetfpetServiceAgent.FinalExamenCenterViewModel;
  institutionListControl: FormArray;
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<ExamenCentreDetailDialog>,
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.title = "Gestion des centres d’examens de sortie ";
    this.institutionList = data.institutionList;
    this.form = this._formBuilder.group({
      name: ["", Validators.required],
      isActive: true,
      institutions: this._formBuilder.array([]),
    });
    this.institutionListControl = <FormArray>this.form.get("institutions");
    this.parseInputData(data);
  }

  private parseInputData(data: any) {
    if (data.id) {
      this.isEditMode = true;
      this.examenCentreId = data.id;

      this._metfpetService
        .getExamenCenterDetails(this.examenCentreId)
        .subscribe((res) => {
          this.model = res;
          this.form.patchValue({
            name: this.model.name,
            status: this.model.status,
            isActive: this.model.status === <any> FinalExamenCentreStatus.Active
          });
          this.model.institutions.forEach((x) => {
            this.institutionListControl.push(this.patchInstitutionValue(x));
          });
        });
    }
    else {
      //init new empty institution row
      this.addInstitution();
    }
  }

  patchInstitutionValue(
    x: MetfpetServiceAgent.FinalExamenInstitutionDTO
  ): FormGroup {
    var subForm = this._formBuilder.group({
      id: [x.id, Validators.required],
      name: null,
      programmeId: x.programmeId
    });
    return subForm;
  }

  addInstitution() {
    this.institutionListControl.push(
      this.patchInstitutionValue(
        MetfpetServiceAgent.FinalExamenInstitutionDTO.fromJS({
          id: null,
          name: null,
          programmeId: null
        })
      )
    );
  }

  remove(rowIdx: number){
    this.institutionListControl.removeAt(rowIdx);

  }

  onConfirm() {
    validateForm(this.form);
    if (!this.form.valid) return;

    if (this.isEditMode) {
      this._metfpetService
        .saveFinalExamenCentreInfo(
          Object.assign({}, this.model, this.form.value, {
            status: this.form.get("isActive").value
              ? FinalExamenCentreStatus.Active
              : FinalExamenCentreStatus.Inactive,
          })
        )
        .subscribe((res) => {
          this.hasChanges = true;
          this._dialogRef.close(this.hasChanges);
        });
    } else {
      this._metfpetService
        .saveFinalExamenCentreInfo(
          Object.assign({}, this.model, this.form.value, {
            status: FinalExamenCentreStatus.Active,
          })
        )
        .subscribe((res) => {
          this.hasChanges = true;
          this._dialogRef.close(this.hasChanges);
        });
    }
  }

  onClose() {
    this._dialogRef.close(this.hasChanges);
  }

  getProgrammes(id: any) {
    if (id)
    {
      let institution = this.institutionList.find(x => x.id == id);
      if (institution) 
      {
        return institution.programmes;
      }
    }
    return [];
  }
}
