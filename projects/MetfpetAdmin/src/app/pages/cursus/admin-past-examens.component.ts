import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent } from 'rxjs';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators/';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  PerfectScrollService,
  MetfpetServiceAgent,
  BiometricStatusValues,
  FinalExamenStatusResultValues,
  FinalExamenStatusGlobalValues,
  FinalExamenStatusGlobal,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { Tabs } from './admin-examens-tab.component';
import { ExportExamensDialog } from './export-examens.dialog';
import { MatDialog } from '@angular/material/dialog';

@Component({
  templateUrl: './admin-past-examens.component.html',
  styleUrls: ['./admin-past-examens.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminPastExamensComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;
  @ViewChild('searchPV', {static: true}) searchPV: ElementRef;
  @ViewChild('searchMoyenneTotalMin', {static: true}) searchMoyenneTotalMin: ElementRef;
  @ViewChild('searchMoyenneTotalMax', {static: true}) searchMoyenneTotalMax: ElementRef;

  settings: Settings;
  title: string;
  navs = Tabs;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  allProgramList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  centreList: Array<MetfpetServiceAgent.FinalExamenCenterRowViewModel>;
  statusResultList: Array<any>;
  statusGlobalList: Array<any>;
  biometricList: Array<any>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissement>;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    private _dialog: MatDialog,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Examens de sortie - Session passées';
    this.statusResultList = FinalExamenStatusResultValues;
    this.statusGlobalList = FinalExamenStatusGlobalValues;
    this.biometricList = BiometricStatusValues;
    this.sort = {prop: 'identifiantNationalEleve', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getStatusEtablissementList().subscribe(data => {
      this.statusEtablissementList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.allProgramList = data.results;
      this.programList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getFinalExamenCentreListPaged(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { 'isActive': '1' }
    })).subscribe(data => {
      this.centreList = data.results;
      this._cd.markForCheck();
    });
    
    this.searchForm.get('institution').valueChanges.pipe(
      startWith(this.searchForm.get('institution').value),
      tap(institutionId => {
        this.programList = this.allProgramList;
        if (!this.isFirstSearch) {
          this.searchForm.get('programme').setValue(null);
        }
        if (!institutionId) {
          return;
        }
        this._loadPrograms(institutionId);
      })
    ).subscribe();

    combineLatest([
      this.searchForm.get('statusResult').valueChanges
        .pipe(
          startWith(this.searchForm.get('statusResult').value)
        ),
      this.searchForm.get('statusGlobal').valueChanges
        .pipe(
          startWith(this.searchForm.get('statusGlobal').value)
        ),
      this.searchForm.get('programme').valueChanges
        .pipe(
          startWith(this.searchForm.get('programme').value)
        ),
      this.searchForm.get('anneeAcademique').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademique').value)
        ),
      this.searchForm.get('niveauEtude').valueChanges
        .pipe(
          startWith(this.searchForm.get('niveauEtude').value)
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      fromEvent(
        this.searchPV.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('numeroPV').value}
            })
          ),
      fromEvent(
        this.searchMoyenneTotalMin.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('moyenneTotalMin').value}
            })
          ),
      fromEvent(
        this.searchMoyenneTotalMax.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('moyenneTotalMax').value}
            })
          ),
      this.searchForm.get('finalExamenCentre').valueChanges
        .pipe(
          startWith(this.searchForm.get('finalExamenCentre').value)
        ),
      this.searchForm.get('biometricStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('biometricStatus').value)
        ),
      this.searchForm.get('statusEtablissement').valueChanges
        .pipe(
          startWith(this.searchForm.get('statusEtablissement').value)
        ),
    ])
    .subscribe((
      [
        statusResult,
        statusGlobal,
        programme,
        anneeAcademique,
        niveauEtude,
        eventSearchNationalStudentIdentifier,
        eventSearchName, 
        eventFirstname,
        eventSearchPV,
        eventSearchMoyenneTotalMin,
        eventSearchMoyenneTotalMax,
        finalExamenCentre,
        biometricStatus,
        statusEtablissement,
      ]) => {
      this.searchForm.patchValue({
        statusResult,
        statusGlobal,
        programme,
        anneeAcademique,
        niveauEtude,
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null,
        numeroPV: eventSearchPV ? eventSearchPV['target'].value : null, 
        moyenneTotalMin: eventSearchMoyenneTotalMin ? eventSearchMoyenneTotalMin['target'].value : null, 
        moyenneTotalMax: eventSearchMoyenneTotalMax ? eventSearchMoyenneTotalMax['target'].value : null, 
        finalExamenCentre,
        biometricStatus,
        statusEtablissement,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      identifiantNationalEleve: null,
      institution: null,
      programme: null,
      anneeAcademique: null,
      niveauEtude: null,
      name: null,
      firstName: null,
      numeroPV: null,
      biometricStatus: null,
      finalExamenCentre: null,
      moyenneTotalMin: null,
      moyenneTotalMax: null,
      statusResult: null,
      statusGlobal: null,
      isCurrentSession: false,
      statusEtablissement: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getFinalExamenEtudiantListPage(criteria);
  }
  
  open(row: MetfpetServiceAgent.FinalExamenEtudiantViewModel) {
    this._router.navigate([`${UiPath.admin.students.dossier}/${row.etudiantId}`], { state: { previousPage: 'Cursus' } });
  }

  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {institution: institutionId}
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });
  }

  exportFinalExamenEtudiants() {
    this._dialog.open(ExportExamensDialog, {
      width: '650px',
      data: {
        filters: this.searchForm.value
      }
    });
  }
}