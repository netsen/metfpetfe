import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

export const Tabs = [
  { 
    routerLink: '/' + UiPath.admin.cursus.configurations.mention, 
    label: 'Seuil des mentions et de passage' 
  },
  {
    routerLink: '/' + UiPath.admin.cursus.configurations.exporter, 
    label: 'Exporter les templates de notes' 
  },
  {
    routerLink: '/' + UiPath.admin.cursus.configurations.importerNote, 
    label: 'Importer les notes' 
  },
  {
    routerLink: '/' + UiPath.admin.cursus.configurations.importerNoteDetails, 
    label: 'Importer les détails de la note manquante' 
  },
  {
    routerLink: '/' + UiPath.admin.cursus.configurations.importerRedoublant, 
    label: 'Importer les redoublants' 
  },
  {
    routerLink: '/' + UiPath.admin.cursus.configurations.centre, 
    label: 'Configurer les centres d’examen' 
  },
  {
    routerLink: '/' + UiPath.admin.cursus.configurations.matieresDesExamens, 
    label: 'Matières des examens' 
  }
];

@Component({
  templateUrl: './config-examens-tab.component.html',
})
export class ConfigExamensTabComponent {

  navs = Tabs;
  title: string;
  routerEventSubscription: Subscription;
  
  constructor(private router: Router) {
    this.title = 'Configuration - Examens de sortie';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = selectedTab.label;
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
