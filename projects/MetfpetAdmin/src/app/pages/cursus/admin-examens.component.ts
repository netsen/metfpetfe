import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent } from 'rxjs';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators/';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  PerfectScrollService,
  MetfpetServiceAgent,
  BiometricStatusValues,
  FinalExamenStatusResultValues,
  FinalExamenStatusGlobalValues,
  FinalExamenStatusGlobal,
  EntrerLesNotesDialog,
  RedoublementDialog,
  AuthService,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { Tabs } from './admin-examens-tab.component';
import { MatDialog } from '@angular/material/dialog';
import { ExportExamensDialog } from './export-examens.dialog';

@Component({
  templateUrl: './admin-examens.component.html',
  styleUrls: ['./admin-examens.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminExamensComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;
  @ViewChild('searchPV', {static: true}) searchPV: ElementRef;
  @ViewChild('searchMoyenneTotalMin', {static: true}) searchMoyenneTotalMin: ElementRef;
  @ViewChild('searchMoyenneTotalMax', {static: true}) searchMoyenneTotalMax: ElementRef;

  settings: Settings;
  title: string;
  navs = Tabs;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  allProgramList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  centreList: Array<MetfpetServiceAgent.FinalExamenCenterRowViewModel>;
  statusResultList: Array<any>;
  statusGlobalList: Array<any>;
  biometricList: Array<any>;
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissement>;
  genreList: Array<MetfpetServiceAgent.Genre>;
  missingCenterOrPvValues: Array<any>;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    private _dialog: MatDialog,
    protected _perfectScrollService: PerfectScrollService,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Examens de sortie - Session en cours';
    this.statusResultList = FinalExamenStatusResultValues;
    this.statusGlobalList = FinalExamenStatusGlobalValues;
    this.biometricList = BiometricStatusValues;
    this.missingCenterOrPvValues = [
      {value : 'Oui', name : 'Oui'},
      {value : 'Non', name : 'Non'},
    ];
    this.sort = {prop: 'identifiantNationalEleve', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cd.markForCheck(); 
    });

    this._metfpetService.getGenreList().subscribe(data => {
      this.genreList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getStatusEtablissementList().subscribe(data => {
      this.statusEtablissementList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.allProgramList = data.results;
      this.programList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getFinalExamenCentreListPaged(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { 'isActive': '1' }
    })).subscribe(data => {
      this.centreList = data.results;
      this._cd.markForCheck();
    });

    this.searchForm.get('institution').valueChanges.pipe(
      startWith(this.searchForm.get('institution').value),
      tap(institutionId => {
        this.programList = this.allProgramList;
        if (!this.isFirstSearch) {
          this.searchForm.get('programme').setValue(null);
        }
        if (!institutionId) {
          return;
        }
        this._loadPrograms(institutionId);
      })
    ).subscribe();

    combineLatest([
      this.searchForm.get('statusResult').valueChanges
        .pipe(
          startWith(this.searchForm.get('statusResult').value)
        ),
      this.searchForm.get('statusGlobal').valueChanges
        .pipe(
          startWith(this.searchForm.get('statusGlobal').value)
        ),
      this.searchForm.get('programme').valueChanges
        .pipe(
          startWith(this.searchForm.get('programme').value)
        ),
      this.searchForm.get('anneeAcademique').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademique').value)
        ),
      this.searchForm.get('niveauEtude').valueChanges
        .pipe(
          startWith(this.searchForm.get('niveauEtude').value)
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      fromEvent(
        this.searchPV.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('numeroPV').value}
            })
          ),
      fromEvent(
        this.searchMoyenneTotalMin.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('moyenneTotalMin').value}
            })
          ),
      fromEvent(
        this.searchMoyenneTotalMax.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('moyenneTotalMax').value}
            })
          ),
      this.searchForm.get('finalExamenCentre').valueChanges
        .pipe(
          startWith(this.searchForm.get('finalExamenCentre').value)
        ),
      this.searchForm.get('biometricStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('biometricStatus').value)
        ),
      this.searchForm.get('statusEtablissement').valueChanges
        .pipe(
          startWith(this.searchForm.get('statusEtablissement').value)
        ),
      this.searchForm.get('missingCenterOrPv').valueChanges
        .pipe(
          startWith(this.searchForm.get('missingCenterOrPv').value)
        ),
      this.searchForm.get('genre').valueChanges
        .pipe(
          startWith(this.searchForm.get('genre').value)
        ),
      this.searchForm.get('prefecture').valueChanges
        .pipe(
          startWith(this.searchForm.get('prefecture').value)
        ),
    ])
    .subscribe((
      [
        statusResult,
        statusGlobal,
        programme,
        anneeAcademique,
        niveauEtude,
        eventSearchNationalStudentIdentifier,
        eventSearchName, 
        eventFirstname,
        eventSearchPV,
        eventSearchMoyenneTotalMin,
        eventSearchMoyenneTotalMax,
        finalExamenCentre,
        biometricStatus,
        statusEtablissement,
        missingCenterOrPv,
        genre,
        prefecture,
      ]) => {
      this.searchForm.patchValue({
        statusResult,
        statusGlobal,
        programme,
        anneeAcademique,
        niveauEtude,
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null,
        numeroPV: eventSearchPV ? eventSearchPV['target'].value : null, 
        moyenneTotalMin: eventSearchMoyenneTotalMin ? eventSearchMoyenneTotalMin['target'].value : null, 
        moyenneTotalMax: eventSearchMoyenneTotalMax ? eventSearchMoyenneTotalMax['target'].value : null, 
        finalExamenCentre,
        biometricStatus,
        statusEtablissement,
        missingCenterOrPv,
        genre,
        prefecture,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      identifiantNationalEleve: null,
      institution: null,
      programme: null,
      anneeAcademique: null,
      niveauEtude: null,
      name: null,
      firstName: null,
      numeroPV: null,
      biometricStatus: null,
      finalExamenCentre: null,
      moyenneTotalMin: null,
      moyenneTotalMax: null,
      statusResult: null,
      statusGlobal: null,
      isCurrentSession: true,
      statusEtablissement: null,
      missingCenterOrPv: null,
      genre: null,
      prefecture: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getFinalExamenEtudiantListPage(criteria);
  }
  
  open(row: MetfpetServiceAgent.FinalExamenEtudiantViewModel) {
    this._router.navigate([`${UiPath.admin.students.dossier}/${row.etudiantId}`], { state: { previousPage: 'Cursus' } });
  }

  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {institution: institutionId}
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });
  }

  addResult(row: MetfpetServiceAgent.FinalExamenEtudiantViewModel) {
    this._dialog.open(EntrerLesNotesDialog , {
      width: '800px',
      data: {
        numeroPV: row.numeroPV
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  allowAddResult(row: MetfpetServiceAgent.FinalExamenEtudiantViewModel) {
    return !this.isMinistre() && !this.isAdministrateur() && !row.isPublished && (row.statusGlobal === <any> FinalExamenStatusGlobal.ResultatAEntrer || 
                                row.statusGlobal === <any> FinalExamenStatusGlobal.APublier || 
                                row.statusGlobal === <any> FinalExamenStatusGlobal.Admis ||
                                row.statusGlobal === <any> FinalExamenStatusGlobal.NonAdmis);
  }

  allowToSeeResult(row: MetfpetServiceAgent.FinalExamenEtudiantViewModel) {
    return !this.isAdministrateur() || row.isPublished;
  }
  
  getDisplayedMoyenneTotal(row: MetfpetServiceAgent.FinalExamenEtudiantViewModel) {
    if (this.allowToSeeResult(row)) {
      return row.displayedMoyenneTotal;
    }
    return 'A compléter';
  }
  
  exportFinalExamenEtudiants() {
    this._dialog.open(ExportExamensDialog, {
      width: '650px',
      data: {
        filters: this.searchForm.value
      }
    });
  }

  redoublement() {
    this._dialog.open(RedoublementDialog , {
      width: '800px',
      data: {
        anneeAcademiqueList: this.anneeAcademiqueList
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  enterGrades() {
    this._dialog.open(EntrerLesNotesDialog , {
      width: '800px',
      data: {
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  publishResults() {
    this._router.navigate([`${UiPath.admin.cursus.examens.publierLesNotes}`]);
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }

  public isAdministrateur() {
    return this._authService.getUserType() === 'administrateur';
  }
}