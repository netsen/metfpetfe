import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

export const Tabs = [
  { 
    routerLink: '/' + UiPath.admin.cursus.examens.publierLesNotes, 
    label: 'Publier les notes' 
  }
];

@Component({
  templateUrl: './admin-publier-les-notes-tab.component.html',
})
export class AdminPublierLesNotesTabComponent {

  navs = Tabs;
  title: string;
  routerEventSubscription: Subscription;
  
  constructor(private router: Router) {
    this.title = 'Examens de sortie';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = selectedTab.label;
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
