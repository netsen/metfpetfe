import {
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';

@Component({
  templateUrl: './admin-retrieve-password.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminRetrievePasswordComponent {

  userType: string;

  constructor(
    private _cd: ChangeDetectorRef,
    private _fb: FormBuilder,
    private _store: Store<any>,
  ) {
    this.userType = "employeMinisteriel";
  }
}