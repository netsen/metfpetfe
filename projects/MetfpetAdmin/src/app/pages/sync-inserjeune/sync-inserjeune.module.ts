import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { SyncInserjeuneTabComponent } from './sync-inserjeune-tab.component';
import { SyncInserjeuneComponent } from './sync-inserjeune.component';

export const routes = [
  {
    path: '', 
    component: SyncInserjeuneTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'sync',
          pathMatch: 'full'
        },
        { 
          path: 'sync', 
          component: SyncInserjeuneComponent, 
          data: { breadcrumb: 'Synchroniser avec Inserjeune' }
        },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [
    SharedModule,
    SyncInserjeuneComponent,
  ],
  declarations: [
    SyncInserjeuneTabComponent,
    SyncInserjeuneComponent,
  ], 
  entryComponents: [
  ]
})
export class SyncInserjeuneModule {

}
