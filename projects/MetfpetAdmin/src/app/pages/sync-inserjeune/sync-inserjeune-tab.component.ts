import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './sync-inserjeune-tab.component.html',
})
export class SyncInserjeuneTabComponent {

  navs = [
    {
      routerLink: '/' + UiPath.admin.import.syncInserjeune, 
      label: 'Synchroniser avec Inserjeune' 
    },
  ];

  public title: string;
  routerEventSubscription: Subscription;

  constructor(protected router: Router) {
    this.title = 'Synchroniser avec Inserjeune';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = selectedTab.label;
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
