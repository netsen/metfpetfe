import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Input,
  ViewEncapsulation 
} from '@angular/core';
import { UserIdleService } from 'angular-user-idle'; 
import { 
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  Settings,
  AppSettings,
} from 'MetfpetLib';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { Router } from '@angular/router';
import { SyncInserjeuneTabComponent } from './sync-inserjeune-tab.component';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

export interface ImportLesNotesResult {
  nbImported: number;
  failedAdmissions: Array<any>;
}

@Component({
  templateUrl: './sync-inserjeune.component.html',
  styleUrls: ['./sync-inserjeune.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SyncInserjeuneComponent extends SyncInserjeuneTabComponent {
  
  imported: number;
  totalToimport: number;
  importingIndicator: boolean;
  showWarningMessage: boolean;
  public settings: Settings;

  constructor(
    private _cd: ChangeDetectorRef,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    public appSettings: AppSettings,
    protected router: Router
  ) {
    super(router);
    this.settings = this.appSettings.settings;
  }

  onClickImportButton() {
    this._metfpetService.getEtudiantNotSyncToInserjeune().subscribe(etudiantIds => {
      if (etudiantIds) {
        this.sync(etudiantIds);
      }
    });
  }

  async sync(etudiantIds: Array<string>) {
    this.importingIndicator = true;
    this._userIdleService.stopWatching();
    this.showWarningMessage = true;
    this.imported = 0;
    this.totalToimport = etudiantIds.length;

    for (let etudiantId of etudiantIds) {    
      try {
        this.imported++;
        this._cd.markForCheck();
        await this._metfpetService.synchronizeEtudiantWithInserjeune(etudiantId).toPromise();
      } catch(error) {
      }
    }

    this.importingIndicator = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
  }
}
