import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef,
  Component,
  ViewEncapsulation
} from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { 
  AuthService, 
  emailValidator,
  validateForm,
  showSuccess,
  MetfpetServiceAgent, 
} from 'MetfpetLib';

@Component({
  templateUrl: './admin-profile.dialog.html', 
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminProfileDialog {

  form: FormGroup;
  title: string;
  error: string;
  loading: boolean;
  private _model: MetfpetServiceAgent.EmployeMinisterielProfileViewModel;

  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogRef: MatDialogRef<AdminProfileDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.form = this._formBuilder.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'firstName': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'email': [null, Validators.compose([Validators.required, emailValidator])],
      'identifiant': [null, Validators.compose([Validators.required, Validators.minLength(3)])]
    });
  }

  ngAfterViewInit() {
    this._metfpetService.getEmployeMinisterielProfile(this._authService.getUserId()).subscribe((data : any) => {
      this._model = data;
      this.title = this._model.firstName + ' ' + this._model.name;
      this.form.controls['name'].setValue(this._model.name);
      this.form.controls['firstName'].setValue(this._model.firstName);
      this.form.controls['email'].setValue(this._model.email);
      this.form.controls['identifiant'].setValue(this._model.identifiant);
      this._cd.markForCheck();
    });
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this.loading = true;
      this._cd.markForCheck();

      this._metfpetService.updateEmployeMinisterielProfile(MetfpetServiceAgent.EmployeMinisterielProfileViewModel.fromJS(
        Object.assign({}, this._model, this.form.value)
      )).subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this.error = error
        )
        .add(() => {
          this.loading = false;
          this._cd.markForCheck();
        });
    }
  }

  public onClose(): void {
    this._dialogRef.close();
  }
}