import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppSettings, AuthService, Settings } from 'MetfpetLib';
import { UiPath } from '../ui-path';

@Component({
  templateUrl:'./admin-home.component.html',
})
export class AdminHomeComponent {

  public settings: Settings;
  public title: string;

  constructor(
    public appSettings: AppSettings, 
    private _authService: AuthService,
    private _router: Router
  ) {
    this.settings = this.appSettings.settings;
    this.title = 'Accueil';
  }

  ngAfterViewInit(){
    setTimeout(() => this.settings.loadingSpinner = false);
  }

  goToStudentDossiers() {
    this._router.navigate([UiPath.admin.students.list]);
  }

  goToAdmission() {
    this._router.navigate([UiPath.admin.admission.list]);
  }

  goToInscriptions() {
    if (this.isExamRole()) 
    {
      this._router.navigate([UiPath.admin.cursus.examens.currentSession]);
    } 
    else 
    {
      this._router.navigate([UiPath.admin.cursus.inscriptions]);
    }
  }

  goToPaiements() {
    this._router.navigate([UiPath.admin.payments.inscription.list]);
  }

  goToDemandeurDossiers() {
    this._router.navigate([UiPath.admin.agrementation.dossiers.list]);
  }

  goToAgrement() {
    this._router.navigate([UiPath.admin.agrementation.agrements.list]);
  }
  
  isAdministrateur() {
    return this._authService.getUserType() == 'administrateurMinisteriel' || 
           this._authService.getUserType() == 'administrateur' ||
           this._authService.getUserType() == 'ministre';
  }

  isStudentManagementRole() {
    return this._authService.getUserType() == 'superviseurMinisteriel'
        || this._authService.getUserType() == 'agentMinisteriel';
  }

  isExamRole() {
    return this._authService.getUserType() == 'examSupervisorMinisteriel'
        || this._authService.getUserType() == 'examAgentMinisteriel';
  }

  isAgrementRole() {
    return this._authService.getUserType() == 'agrementSupervisorMinisteriel'
        || this._authService.getUserType() == 'agrementAgentMinisteriel';
  }

  isDirectionsNationalesRole() {
    return this._authService.getUserType() == 'directionsNationales';
  }
}
