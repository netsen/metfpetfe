import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ImportAgrementationsComponent } from './import-agrementations.component';
import { ImportAgrementationsTabComponent } from './import-agrementations-tab';
import { ImportAgrementationsDialog } from './import-agrementations.dialog';


export const routes = [
  {
    path: '',
    component: ImportAgrementationsTabComponent,
    children:
      [
        {
          path: '',
          redirectTo: 'agrementations',
          pathMatch: 'full'
        },
        {
          path: 'agrementations',
          component: ImportAgrementationsComponent,
          data: { breadcrumb: 'Importer les établissements déjà agrémentées' }
        },
      ]
  }
];

@NgModule({
  declarations: [
    ImportAgrementationsComponent,
    ImportAgrementationsTabComponent,
    ImportAgrementationsDialog],
  exports: [
    SharedModule,
    ImportAgrementationsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  entryComponents: [
    ImportAgrementationsDialog
  ]
})
export class ImportAgrementationsModule { }
