import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';


@Component({
  templateUrl: './import-agrementations-tab.html',
})
export class ImportAgrementationsTabComponent {
  navs = [
    {
      routerLink: '/' + UiPath.admin.agrementation.configuration.import.importAgrementations,
      label: 'Importer les établissements déjà agrémentées '
    },
  ];

  public title: string;
  routerEventSubscription: Subscription;

  constructor(protected router: Router) {
    this.title = 'Import : importer les établissements déjà agrémentées ';
  }

  ngOnInit(): void {
    this.routerEventSubscription = this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
        this.title = 'Import : ' + selectedTab.label.toLowerCase();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }

}
