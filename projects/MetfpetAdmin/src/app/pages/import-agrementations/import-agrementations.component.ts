import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewEncapsulation
} from '@angular/core';
import { UserIdleService } from 'angular-user-idle';
import {
  DialogService,
  SeverityEnum,
  MetfpetServiceAgent,
  Settings,
  AppSettings,
} from 'MetfpetLib';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { Router } from '@angular/router';
import { ImportAgrementationsTabComponent } from './import-agrementations-tab';
import { ImportAgrementationsDialog } from './import-agrementations.dialog';
import { formatDate } from '@angular/common';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';


@Component({
  templateUrl: './import-agrementations.component.html',
  styleUrls: ['./import-agrementations.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportAgrementationsComponent  extends ImportAgrementationsTabComponent {

  imported: number;
  totalToimport: number;
  importingIndicator: boolean;
  showWarningMessage: boolean;
  public settings: Settings;

  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    public appSettings: AppSettings,
    protected router: Router
  ) {
    super(router);
    this.settings = this.appSettings.settings;
  }

  ngOnInit(): void {
  }

  onClickImportButton() {
    this._dialogService.openDialog(
      ImportAgrementationsDialog,
      {
        width: '600px',
        data: {
          isImport: true,
          title: "Importer les établissements déjà agrémentées", 
          message: "Veuillez indiquer l'emplacement du fichier", 
          confirmBtnText: "Importer",
        }
      }
    ).afterClosed().subscribe(result => {
      if (result && result.file) {
        this.processFile(result.file);
      }
    });
  }

  processFile(file: Blob) {
    this.importingIndicator = true;
    this._userIdleService.stopWatching();

    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      var arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, {type:'binary',cellDates:true,dateNF:'dd/mm/yyyy'});
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      var rows = XLSX.utils.sheet_to_json(worksheet, {raw:false,dateNF:'dd/mm/yyyy'});
      
      this._importInscriptions(rows);
    }
    fileReader.readAsArrayBuffer(file);
  }

  private formatDateForImport(date: any): string {
    if (date instanceof Date) {
      return formatDate(date, 'yyyy-MM-dd', 'en-US');
		}
    const [day, month, year] = date.split('/');
    return [year, month, day].join('-');
  }

  private _importInscriptions(rows: any): void {
    if (rows.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: 'Aucune donnée importée'
        }
      });
      return;
    }
    this.importAgrement(this._convertToAgrement(rows), true);
  }

  private _convertToAgrement(rows: any): Array<MetfpetServiceAgent.ImportAgrementExistingViewModel> {
    
    let agrement =  new Array<MetfpetServiceAgent.ImportAgrementExistingViewModel>();

    agrement = rows.map(
      row => {
        return MetfpetServiceAgent.ImportAgrementExistingViewModel.fromJS({
          'agrementName'                : row['Agrement'], 
          'numeroDemande'               : row['Numero'],
          'typeDemandeName'             : row['Type'],
          'identifiantDemandeur'        : row['Identifiant'], 
          'expirationDate'              : this.formatDateForImport(row['DateExpiration']),
          'deliveryDate'                : this.formatDateForImport(row['DateDebut']), 
          'etablissementName'           : row['Nom de l’établissement'],
          'localisation'                : row['Localisation'],
          'statusEtablissement'         : row['Statut de l’établissement'],
          'typeInstitution'             : row['Type d’institution'],
        });
      }
    );
    return agrement;
  }

  async importAgrement(agrementToImport: Array<MetfpetServiceAgent.ImportAgrementExistingViewModel>, allowRetry: boolean) {
    this.showWarningMessage = true;
    this.imported = 0;
    this.totalToimport = agrementToImport.length;
    let toRetryList = new Array<MetfpetServiceAgent.ImportAgrementExistingViewModel>();

    for (let agrement of agrementToImport) {    
      try {
        this.imported++;
        this._cd.markForCheck();
        await this._metfpetService.importAgrement(agrement).toPromise();
      } catch(error) {
        agrement.importErrorMessage = error.isApiException ? (JSON.parse(error.response).Message ? JSON.parse(error.response).Message : error.response) : error;
        toRetryList.push(agrement);
      }
    }
    if (toRetryList.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: (this.imported - toRetryList.length) + " agrement(e)(s) importé(e)(s) avec succès, " 
            + toRetryList.length + " en échec"
        }
      });

    } else {
      this._dialogService.openConfirmDialog({
        data: {
          title: "Attention", 
          message: (this.totalToimport - toRetryList.length) + " agrement(e)s importé(e)s avec succès, " 
            + toRetryList.length + " en échec, reprendre les éléments en échec?"
        }
      })
      .afterClosed().subscribe(confirmed => {
        if (confirmed) {
          this.importAgrement(toRetryList, false);
        } else {
          this.exportFailed(toRetryList);
        }
      });
    }

    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    return {
      nbImported: this.imported,
      failedUsers: toRetryList
    };
  }

  exportFailed(toRetryList: Array<MetfpetServiceAgent.ImportAgrementExistingViewModel>): void {
    if (toRetryList.length > 0) {
      let json : any[] = new Array();
      for (let failed of toRetryList) {
        json.push({
          Agrement          : failed.agrementName,
          Numero            : failed.numeroDemande,
          Type              : failed.typeDemandeName,
          Identifiant       : failed.identifiantDemandeur,
          Raison            : failed.importErrorMessage
        });
      }
      this.exportAsExcelFile(json, 'failed_results');
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'Échec': worksheet }, SheetNames: ['Échec'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_import_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

}
