import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPermissionsGuard } from 'ngx-permissions';
import {
  AuthorityEnum,
  PagesComponent,
  RoutesByPermissionsConfiguration,
  AuthRoleGuardService,
  PublicLayoutComponent,
} from 'MetfpetLib';
import { SharedModule } from '../shared/shared.module';
import { AdminHomeComponent } from './home/admin-home.component';
import { AdminProfileDialog } from './profile';

export const routes = [
  {
    path: 'login',
    component: PublicLayoutComponent,
    loadChildren: () => import('./login/admin-login.module').then(m => m.AdminLoginModule),
  },
  {
    path: 'retrieve-password',
    component: PublicLayoutComponent,
    loadChildren: () => import('./retrieve-password/admin-retrieve-password.module').then(m => m.AdminRetrievePasswordModule),
  },
  {
    path: '',
    component: PagesComponent,
    canActivate: [AuthRoleGuardService],
    children: [
      {
        path: '',
        component: AdminHomeComponent,
        data: { breadcrumb: 'Accueil' },
      },
      {
        path: 'cursus',
        loadChildren: () => import('./cursus/admin-cursus.module').then(m => m.AdminCursusModule),
        data: {
          breadcrumb: 'Cursus',
          permissions: {
            only: RoutesByPermissionsConfiguration['cursus'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'document',
        loadChildren: () => import('./documents/admin-documents.module').then(m => m.AdminDocumentsModule),
        data: {
          breadcrumb: 'Documents',
          permissions: {
            only: RoutesByPermissionsConfiguration['document'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'bourses',
        loadChildren: () => import('./bourses/admin-bourses.module').then(m => m.AdminBoursesModule),
        data: {
          breadcrumb: 'Bourses',
          permissions: {
            only: RoutesByPermissionsConfiguration['bourses'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'students',
        loadChildren: () => import('./students/admin-students.module').then(m => m.AdminStudentsModule),
        data: {
          breadcrumb: 'Apprenants',
          permissions: {
            only: RoutesByPermissionsConfiguration['students'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'institutions',
        loadChildren: () => import('./institutions/institutions.module').then(m => m.InstitutionsModule),
        data: {
          breadcrumb: 'Institutions',
          permissions: {
            only: RoutesByPermissionsConfiguration['institutions'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'lycees',
        loadChildren: () => import('./lycees/admin-lycees.module').then(m => m.AdminLyceesModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['lycees'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'users',
        loadChildren: () => import('./users/admin-users.module').then(m => m.AdminUsersModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['users'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'payments',
        loadChildren: () => import('./payments/payments.module').then(m => m.PaymentsModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['payments'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'activities',
        loadChildren: () => import('./activities/activities.module').then(m => m.ActivitiesModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['activities'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'statistiques',
        loadChildren: () => import('./statistiques/admin-statistiques.module').then(m => m.AdminStatistiquesModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['statistiques'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'communication',
        loadChildren: () => import('./communication/admin-communication.module').then(m => m.AdminCommunicationModule),
        data: {
          breadcrumb: 'Communication',
          permissions: {
            only: RoutesByPermissionsConfiguration['communication'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'import-students',
        loadChildren: () => import('./import-students/import-students.module').then(m => m.ImportStudentsModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['import'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'import-results',
        loadChildren: () => import('./import-results/import-results.module').then(m => m.ImportResultsModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['import'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'import-inscriptions',
        loadChildren: () => import('./import-inscriptions/import-inscriptions.module').then(m => m.ImportInscriptionsModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['import'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'actualiser-inscriptions',
        loadChildren: () => import('./actualiser-inscriptions/actualiser-inscriptions.module').then(m => m.ActualiserInscriptionsModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['import'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'import-redoublants',
        loadChildren: () => import('./import-redoublants/import-redoublants.module').then(m => m.ImportRedoublantsModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['import'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'import-diplome',
        loadChildren: () => import('./import-diplome/import-diplome.module').then(m => m.ImportDiplomeModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['import'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'sync-inserjeune',
        loadChildren: () => import('./sync-inserjeune/sync-inserjeune.module').then(m => m.SyncInserjeuneModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['import'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'import-email',
        loadChildren: () => import('./import-email/import-email.module').then(m => m.ImportEmailModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['import'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'import-student-profile',
        loadChildren: () => import('./import-student-profile/import-student-profile.module').then(m => m.ImportStudenProfileModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['import'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'import-candidats-libres',
        loadChildren: () => import('./import-candidats-libres/import-candidats-libres.module').then(m => m.ImportCandidatsLibresModule),
        data: { 
          permissions: {
            only: RoutesByPermissionsConfiguration['import'],
            redirectTo: 'login'
          }
        },
        canActivate: [ NgxPermissionsGuard ]
      },
      {
        path: 'import-users',
        loadChildren: () => import('./import-users/import-users.module').then(m => m.ImportUsersModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['import'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'import-bourses',
        loadChildren: () => import('./import-bourses/import-bourses.module').then(m => m.ImportBoursesModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['import'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'import-comptes-ecobank',
        loadChildren: () => import('./import-comptes-ecobank/import-comptes-ecobank.module').then(m => m.ImportComptesEcobankModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['import'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'export',
        loadChildren: () => import('./export/export.module').then(m => m.ExportModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['export'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'admission',
        loadChildren: () => import('./admission/admission.module').then(m => m.AdmissionModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['admission'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'agrementation-configuration',
        loadChildren: () => import('./agrementation/configuration/configuration.module').then(m => m.AgrementationConfigurationModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['agrementation'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'frais',
        loadChildren: () => import('./agrementation/configuration/frais/frais.module').then(m => m.FraisModule),
        data: {
          breadcrumb: 'Frais',
          permissions: {
            only: RoutesByPermissionsConfiguration['agrementation'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      { 
        path: 'agrementation-dossiers', 
        loadChildren: () => import('./agrementation/dossiers/admin-dossiers.module').then(m => m.AdminDossiersModule),
        data: { 
          permissions: {
            only: RoutesByPermissionsConfiguration['agrementation'],
            redirectTo: 'login'
          }  
        },
        canActivate: [ NgxPermissionsGuard ]
      },
      { 
        path: 'agrements', 
        loadChildren: () => import('./agrementation/agrements/admin-agrementation.module').then(m => m.AdminAgrementationModule),
        data: { 
          permissions: {
            only: RoutesByPermissionsConfiguration['agrementation'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      {
        path: 'diplomes',
        loadChildren: () => import('./diplomes/diplomes.module').then(m => m.DiplomesModule),
        data: {
          permissions: {
            only: RoutesByPermissionsConfiguration['diplomes'],
            redirectTo: 'login'
          }
        },
        canActivate: [NgxPermissionsGuard]
      },
      { 
        path: 'concours', 
        loadChildren: () => import('./concours/concours.module').then(m => m.ConcoursModule),
        data: { 
          permissions: {
            only: RoutesByPermissionsConfiguration['examen'],
            redirectTo: 'login'
          }  
        },
        canActivate: [ NgxPermissionsGuard ]
      },
      { 
        path: 'school-kits', 
        loadChildren: () => import('./school-kits/admin-school-kits.module').then(m => m.AdminSchoolKitsModule),
        data: { 
          permissions: {
            only: RoutesByPermissionsConfiguration['examen'],
            redirectTo: 'login'
          }  
        },
        canActivate: [ NgxPermissionsGuard ]
      },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    AdminProfileDialog,
    AdminHomeComponent,
  ],
  entryComponents: [
    AdminProfileDialog
  ]
})
export class AdminModule { }