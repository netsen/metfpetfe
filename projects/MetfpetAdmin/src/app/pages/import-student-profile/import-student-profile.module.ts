import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ImportStudentProfileDialog } from './import-student-profile.dialog';
import { ImportStudentProfileTabComponent } from './import-student-profile-tab.component';
import { ImportStudentProfileComponent } from './import-student-profile.component';

export const routes = [
  {
    path: '', 
    component: ImportStudentProfileTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'profile',
          pathMatch: 'full'
        },
        { 
          path: 'profile', 
          component: ImportStudentProfileComponent, 
          data: { breadcrumb: 'Import données de profil' }
        },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [
    SharedModule,
    ImportStudentProfileComponent,
  ],
  declarations: [
    ImportStudentProfileTabComponent,
    ImportStudentProfileComponent,
    ImportStudentProfileDialog,
  ], 
  entryComponents: [
    ImportStudentProfileDialog
  ]
})
export class ImportStudenProfileModule {

}
