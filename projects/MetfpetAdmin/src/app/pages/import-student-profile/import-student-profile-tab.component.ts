import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './import-student-profile-tab.component.html',
})
export class ImportStudentProfileTabComponent {

  navs = [
    {
      routerLink: '/' + UiPath.admin.import.studentProfile, 
      label: 'Import de données de profil apprenant' 
    },
  ];

  public title: string;
  routerEventSubscription: Subscription;

  constructor(protected router: Router) {
    this.title = 'Import : Données de profil d\'apprenant existants';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = 'Import : Données de profil d\'apprenant existants';
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
