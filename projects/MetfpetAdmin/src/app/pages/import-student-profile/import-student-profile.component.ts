import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Input,
  ViewEncapsulation 
} from '@angular/core';
import { UserIdleService } from 'angular-user-idle'; 
import { 
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  Settings,
  AppSettings,
  ExportService,
} from 'MetfpetLib';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { Router } from '@angular/router';
import { ImportStudentProfileDialog } from './import-student-profile.dialog';
import { ImportStudentProfileTabComponent } from './import-student-profile-tab.component';
import { formatDate } from '@angular/common';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Component({
  templateUrl: './import-student-profile.component.html',
  styleUrls: ['./import-student-profile.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportStudentProfileComponent extends ImportStudentProfileTabComponent {
  
  imported: number;
  totalToimport: number;
  importingIndicator: boolean;
  showWarningMessage: boolean;
  public settings: Settings;

  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    public appSettings: AppSettings,
    protected router: Router,
    private _exportService: ExportService
  ) {
    super(router);
    this.settings = this.appSettings.settings;
  }

  onClickImportButton() {
    this._dialogService.openDialog(
      ImportStudentProfileDialog,
      {
        width: '600px',
        data: {
          isImport: true,
          title: "Import de données de profil apprenant", 
          message: "Veuillez indiquer l'emplacement du fichier", 
          confirmBtnText: "Importer",
        }
      }
    ).afterClosed().subscribe(result => {
      if (result && result.file) {
        this.processFile(result.file);
      }
    });
  }

  processFile(file: Blob) {
    this.importingIndicator = true;
    this._userIdleService.stopWatching();

    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      var arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, {type:'binary',cellDates:true,dateNF:'dd/mm/yyyy'});
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      var rows = XLSX.utils.sheet_to_json(worksheet, {raw:false,dateNF:'dd/mm/yyyy'});

      this._importStudentProfile(rows);
    }
    fileReader.readAsArrayBuffer(file);
  }

  private _importStudentProfile(rows: any): void {
      if (rows.length == 0) {
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.INFO, 
            message: 'Aucune donnée importée'
          }
        });
        return;
      }

      this.importStudentProfile(this._convertToStudentProfile(rows), true);
  }

  private formatDateNaissance(dateNaissance: any): string {
    if (!dateNaissance)
    {
      return '';
    }
    if (dateNaissance instanceof Date) {
      return formatDate(dateNaissance, 'yyyy-MM-dd', 'en-US');
		}
    const [day, month, year] = dateNaissance.split('/');
    return [year, month, day].join('-');
  }
  
  private _convertToStudentProfile(rows: any): Array<MetfpetServiceAgent.UploadEtudiantProfileViewModel> {
    
    let redoublants =  new Array<MetfpetServiceAgent.UploadEtudiantProfileViewModel>();

    redoublants = rows.map(
      row => {
        return MetfpetServiceAgent.UploadEtudiantProfileViewModel.fromJS({
          'identifiantNationalEtudiant'  : row['INA'],
          'sexe'                         : row['Sexe'],
          'dateNaissance'                : this.formatDateNaissance(row['Date de naissance']),
          'lieuNaissance'                : row['Lieu de naissance'],
          'nationalite'                  : row['Nationalité'],
          'nomPere'                      : row['Nom du père'],
          'nomMere'                      : row['Nom de la mère'],
        });
      }
    );
    return redoublants;
  }
  
  async importStudentProfile(studentToImport: Array<MetfpetServiceAgent.UploadEtudiantProfileViewModel>, allowRetry: boolean) {
    this.showWarningMessage = true;
    this.imported = 0;
    this.totalToimport = studentToImport.length;
    let toRetryList = new Array<MetfpetServiceAgent.UploadEtudiantProfileViewModel>();

    for (let student of studentToImport) {    
      try {
        this.imported++;
        this._cd.markForCheck();
        let importSuccess = await this._metfpetService.uploadEtudiantProfile(student).toPromise();
        if (!importSuccess) {
          toRetryList.push(student);
        }
      } catch(error) {
        student.importErrorMessage = error.isApiException ? (JSON.parse(error.response).Message ? JSON.parse(error.response).Message : error.response) : error;
        toRetryList.push(student);
      }
    }
    if (toRetryList.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: (this.imported - toRetryList.length) + " apprenant(e)(s) importé(e)(s) avec succès, " 
            + toRetryList.length + " en échec"
        }
      });

    } else {
      this._dialogService.openConfirmDialog({
        data: {
          title: "Attention", 
          message: (this.totalToimport - toRetryList.length) + " apprenant(e)(s) importé(e)s avec succès, " 
            + toRetryList.length + " en échec, reprendre les éléments en échec?"
        }
      })
      .afterClosed().subscribe(confirmed => {
        if (confirmed) {
          this.importStudentProfile(toRetryList, false);
        } else {
          this.exportFailed(toRetryList);
        }
      });
    }

    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    return {
      nbImported: this.imported,
      failedUsers: toRetryList
    };
  }

  exportFailed(toRetryList: Array<MetfpetServiceAgent.UploadEtudiantProfileViewModel>): void {
    if (toRetryList.length > 0) {
      let json : any[] = new Array();
      for (let failed of toRetryList) {
        json.push({
          'INA'                : failed.identifiantNationalEtudiant,
          'Raison'             : failed.importErrorMessage
        });
      }
      this.exportAsExcelFile(json, 'failed_results');
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'Échec': worksheet }, SheetNames: ['Échec'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_import_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

  downloadTemplate() {
    const headers = [
      { name: "INA", value: "identifiantNationalEleve" },
      { name: "Sexe", value: "sexe" },
      { name: "Date de naissance", value: "dateNaissance" },
      { name: "Lieu de naissance", value: "lieuNaissance" },
      { name: "Nationalité", value: "nationalite" },
      { name: "Nom du père", value: "nomPere" },
      { name: "Nom de la mère", value: "nomMere" },
    ]
    const templateName = "StudentProfileTemplate";
    this._exportService.downloadTemplate(templateName, headers);
  }
}
