import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './import-student-bac.dialog.html',
})
export class ImportStudentBACDialog {

  form: FormGroup;
  fileMissing: boolean;

  constructor(
    public dialogRef: MatDialogRef<ImportStudentBACDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public fb: FormBuilder, 
  ) {
    if (this.isImportResultsBAC() && !this.data.isDownloadTemplate) {
      this.form = this.fb.group({
        'idOptionBAC': [null, Validators.required],
        'sessionBAC': [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]*$")])],
      });
    } else {
      this.form = this.fb.group({
        'idOptionBAC': [null, Validators.required],
      });
    }
  }

  onFileChange(event) {
    this.data.file = event.target.files[0];
    this.fileMissing = false;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onConfirm(): void {
    if(!this.data.isDownloadTemplate){
      if (!this.data.file) {      
        this.fileMissing = true;
        return;     
      }
    }
    if (this.form.valid) {
      if (this.data.type == 'ResultsBAC' && !this.data.isDownloadTemplate) {
        this.data.idOptionBAC = this.form.controls['idOptionBAC'].value;
        this.data.sessionBAC = this.form.controls['sessionBAC'].value;
      } else {
        this.data.idOptionBAC = this.form.controls['idOptionBAC'].value;
      }
      this.dialogRef.close(this.data);
    }
  }

  public isImportResultsBAC(): boolean {
    return 'ResultsBAC' == this.data.type;
  }

}