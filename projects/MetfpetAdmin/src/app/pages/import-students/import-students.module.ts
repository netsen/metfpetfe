import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImportStudentsTabComponent } from './import-students-tab.component';
import { ImportExistingStudentComponent } from './import-existing-student.component';
import { ImportExistingStudentDialog } from './import-existing-student.dialog';
import { ImportExistingComponent } from './import-existing.component';
import { ImportStudentBACComponent } from './import-student-bac.component';
import { ImportStudentBACDialog } from './import-student-bac.dialog';
import { ImportResultsBACComponent } from './import-results-bac.component';
import { ImportStudentBEPCComponent } from './import-student-bepc.component';
import { ImportStudentBEPCDialog } from './import-student-bepc.dialog';
import { ImportResultsBEPCComponent } from './import-results-bepc.component';
import { SharedModule } from '../../shared/shared.module';
import { ImportStudentTerminaleComponent } from './import-student-terminale.component';
import { ImportStudentTerminaleDialog } from './import-student-terminale.dialog';
import { ImportResultsTerminaleComponent } from './import-results-terminale.component';

export const routes = [
  {
    path: '', 
    component: ImportStudentsTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'existing',
          pathMatch: 'full'
        },
        { 
          path: 'existing', 
          component: ImportExistingComponent, 
          data: { breadcrumb: 'Importer existants' }
        },
        { 
          path: 'results-bac', 
          component: ImportResultsBACComponent, 
          data: { breadcrumb: 'Importer résultats BAC' }
        },
        { 
          path: 'results-bepc', 
          component: ImportResultsBEPCComponent, 
          data: { breadcrumb: 'Importer résultats BEPC' }
        },
        { 
          path: 'results-terminale', 
          component: ImportResultsTerminaleComponent, 
          data: { breadcrumb: 'Importer résultats Terminale' }
        },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [
    SharedModule,
	  ImportExistingStudentComponent,
    ImportStudentBACComponent,
    ImportStudentBEPCComponent,
    ImportStudentTerminaleComponent,
  ],
  declarations: [
    ImportStudentsTabComponent,
	  ImportExistingStudentComponent,
	  ImportExistingStudentDialog,
	  ImportExistingComponent,
    ImportStudentBACComponent,
    ImportStudentBACDialog,
    ImportResultsBACComponent,
    ImportStudentBEPCComponent,
    ImportStudentBEPCDialog,
    ImportResultsBEPCComponent,
    ImportStudentTerminaleComponent,
    ImportStudentTerminaleDialog,
    ImportResultsTerminaleComponent,
  ], 
  entryComponents: [
    ImportExistingStudentDialog,
    ImportStudentBACDialog,
    ImportStudentBEPCDialog,
    ImportStudentTerminaleDialog
  ]
})
export class ImportStudentsModule {

}
