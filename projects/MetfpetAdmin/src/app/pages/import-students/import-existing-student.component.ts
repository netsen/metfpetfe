import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Input,
  ViewEncapsulation 
} from '@angular/core';
import {formatDate} from '@angular/common';
import { UserIdleService } from 'angular-user-idle'; 
import { 
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  ExportService,
} from 'MetfpetLib';
import * as TSXLSX from 'ts-xlsx';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { ImportExistingStudentDialog } from './import-existing-student.dialog';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

export enum ImportType {
  Existing = 'Existing'
}

export interface StudentImportResult {
  nbImported: number;
  failedStudents: Array<any>;
}

@Component({
  selector: 'app-existing-student-import',
  templateUrl: './import-existing-student.component.html',
  styleUrls: ['./import-existing-student.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportExistingStudentComponent {

  @Input() importType: ImportType = ImportType.Existing;
  imported: number;
  totalToimport: number;
  genreMap: Map<string, MetfpetServiceAgent.Genre>
  importingIndicator: boolean;
  showWarningMessage: boolean;

  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _exportService: ExportService
  ) {
    this.genreMap = new Map();
  }

  ngAfterViewInit() {
    this._metfpetService.getGenreList().subscribe(data => {
      data.forEach(genre => {
        if (genre.code === 'homme') {
          this.genreMap.set('M', genre);
        } else if (genre.code === 'femme') {
          this.genreMap.set('F', genre);
        }
      })
      this._cd.markForCheck();
    });
  }

  onClickImportButton() {
    this._dialogService.openDialog(
      ImportExistingStudentDialog,
      {
        width: '600px',
        data: {
          title: "Importation des apprenants existants", 
          message: "Veuillez indiquer l'emplacement du fichier", 
          type: this.importType
        }
      }
    ).afterClosed().subscribe(result => {
      if (result && result.file) {
        this.processFile(result.file);
      }
    });
  }

  processFile(file: Blob) {
    this.importingIndicator = true;
    this._userIdleService.stopWatching();

    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      var arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, {type:'binary',cellDates:true,dateNF:'dd/mm/yyyy'});
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      var rows = XLSX.utils.sheet_to_json(worksheet, {raw:false,dateNF:'dd/mm/yyyy'});
      
      this._importExisting(rows);
    }
    fileReader.readAsArrayBuffer(file);
  }

  private _importExisting(rows: any): void {
    let columnNames: Map<string, string> = new Map();
      
    if (rows.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: 'Aucune donnée importée'
        }
      });
      return;
    }

    this.importStudents(this._convertToStudents(rows, columnNames), true);
  }

  private formatDateNaissance(dateNaissance: any): string {
    if (dateNaissance instanceof Date) {
      return formatDate(dateNaissance, 'yyyy-MM-dd', 'en-US');
		}
    const [day, month, year] = dateNaissance.split('/');
    return [year, month, day].join('-');
  }

  private _convertToStudents(rows: any, columnNames: Map<string, string>): Array<MetfpetServiceAgent.ImportEtudiantViewModel> {
    
    let students =  new Array<MetfpetServiceAgent.ImportEtudiantViewModel>();

    students = rows.map(
      row => {
        var fullName = row['Nom Complet'].trim();
        var firstName = fullName.split(' ').slice(0, -1).join(' ');
        var name = fullName.split(' ').slice(-1).join(' ');

        return MetfpetServiceAgent.ImportEtudiantViewModel.fromJS({
          'numeroPV'                  : row['Numéro PV'], 
          'name'                      : name,
          'firstName'                 : firstName,
          'genreId'                   : this.genreMap.get(row['Sexe']).id, 
          'dateNaissance'             : this.formatDateNaissance(row['Date Naissance']),
          'operation'                 : row['Opération']
        });
      }
    );
    return students;
  }
  
  async importStudents(studentsToImport: Array<MetfpetServiceAgent.ImportEtudiantViewModel>, allowRetry: boolean) {
    this.showWarningMessage = true;
    this.imported = 0;
    this.totalToimport = studentsToImport.length;
    let toRetryList = new Array<MetfpetServiceAgent.ImportEtudiantViewModel>();

    for (let student of studentsToImport) {  
      try {
        this.imported++;
        this._cd.markForCheck();
        await this._metfpetService.importEtudiant(student).toPromise();
      } catch(error) {
        student.importErrorMessage = error.isApiException ? (JSON.parse(error.response).Message ? JSON.parse(error.response).Message : error.response) : error;
        toRetryList.push(student);
      }
    }
    if (toRetryList.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: (this.imported - toRetryList.length) + " apprenant(e)(s) importé(e)(s) avec succès, " 
            + toRetryList.length + " en échec"
        }
      });

    } else {
      this._dialogService.openConfirmDialog({
        data: {
          title: "Attention", 
          message: (this.totalToimport - toRetryList.length) + " apprenant(e)s importé(e)s avec succès, " 
            + toRetryList.length + " en échec, reprendre les éléments en échec?"
        }
      })
      .afterClosed().subscribe(confirmed => {
        if (confirmed) {
          this.importStudents(toRetryList, false);
        } else {
          this.exportFailed(toRetryList);
        }
      });
    }

    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    return {
      nbImported: this.imported,
      failedStudents: toRetryList
    };
  }

  exportFailed(toRetryList: Array<MetfpetServiceAgent.ImportEtudiantViewModel>): void {
    if (toRetryList.length > 0) {
      let json : any[] = new Array();
      for (let failed of toRetryList) {
        json.push({
          PV                 : failed.numeroPV, 
          Nom                : failed.name,
          Prénom             : failed.firstName,
          Raison             : failed.importErrorMessage
        });
      }
      this.exportAsExcelFile(json, 'failed_results');
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'Échec': worksheet }, SheetNames: ['Échec'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_import_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

  downloadTemplate() {
    const headers = [
      { name: "Numéro PV", value: "numeroPV" },
      { name: "Nom Complet", value: "nomComplet" },
      { name: "Sexe", value: "sexe" },
      { name: "Date Naissance", value: "dateNaissance" },
      { name: "Opération", value: "operation" },
    ]
    const templateName = "ExistingStudentTemplate";
    this._exportService.downloadTemplate(templateName, headers);
  }
}
