import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './import-student-bepc.dialog.html',
})
export class ImportStudentBEPCDialog {

  form: FormGroup;
  fileMissing: boolean;

  constructor(
    public dialogRef: MatDialogRef<ImportStudentBEPCDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public fb: FormBuilder, 
  ) {
    if (this.isImportResultsBEPC()) {
      this.form = this.fb.group({
        'idOptionBEPC': [null, Validators.required],
        'sessionBEPC': [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]*$")])],
      });
    } else {
      this.form = this.fb.group({});
    }
  }

  onFileChange(event) {
    this.data.file = event.target.files[0];
    this.fileMissing = false;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onConfirm(): void {
    if (!this.data.file) {
      this.fileMissing = true;
      return;
    }
    if (this.form.valid) {
      if (this.data.type == 'ResultsBEPC') {
        this.data.idOptionBEPC = this.form.controls['idOptionBEPC'].value;
        this.data.sessionBEPC = this.form.controls['sessionBEPC'].value;
      }
      this.dialogRef.close(this.data);
    }
  }

  public isImportResultsBEPC(): boolean {
    return 'ResultsBEPC' == this.data.type;
  }

}