import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './import-students-tab.component.html',
})
export class ImportStudentsTabComponent {

  navs = [
    {
      routerLink: '/' + UiPath.admin.import.existing, 
      label: 'Importer existants' 
    },
    {
      routerLink: '/' + UiPath.admin.import.resultsBAC, 
      label: 'Importer résultats BAC' 
    },
    {
      routerLink: '/' + UiPath.admin.import.resultsBEPC, 
      label: 'Importer résultats BEPC' 
    },
    {
      routerLink: '/' + UiPath.admin.import.resultsTerminale, 
      label: 'Importer résultats Terminale' 
    },
  ];

  public title: string;
  routerEventSubscription: Subscription;

  constructor(protected router: Router) {
    this.title = 'Import : importer des apprenants existants';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = 'Import : ' + selectedTab.label.toLowerCase();
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
