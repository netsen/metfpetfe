import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppSettings, Settings } from 'MetfpetLib';
import { ImportStudentsTabComponent } from './import-students-tab.component';

@Component({
  templateUrl: './import-existing.component.html',
})
export class ImportExistingComponent extends ImportStudentsTabComponent {

  public settings: Settings;

  constructor(
    public appSettings: AppSettings,
    protected router: Router
   ) {
   	super(router);
    this.settings = this.appSettings.settings;
    this.title = 'Importation des apprenants existants';
  }

}
