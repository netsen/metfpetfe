import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Input,
  TemplateRef,
  ViewEncapsulation 
} from '@angular/core';
import {formatDate} from '@angular/common';
import { UserIdleService } from 'angular-user-idle'; 
import { 
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  ExportService,
} from 'MetfpetLib';
import * as TSXLSX from 'ts-xlsx';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { ImportStudentBACDialog } from './import-student-bac.dialog';
import { FormBuilder} from '@angular/forms';
import { error } from 'console';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

export enum ImportType {
  ResultsBAC = 'ResultsBAC'
}

export interface StudentImportResult {
  nbImported: number;
  failedStudents: Array<any>;
}

@Component({
  selector: 'app-student-bac-import',
  templateUrl: './import-student-bac.component.html',
  styleUrls: ['./import-student-bac.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportStudentBACComponent {

  @Input() importType: ImportType = ImportType.ResultsBAC;
  imported: number;
  totalToimport: number;
  matieresBACList: Array<MetfpetServiceAgent.MatiereBAC>;
  optionsBACList: Array<MetfpetServiceAgent.OptionBACRowViewModel>;
  genreMap: Map<string, MetfpetServiceAgent.Genre>
  importingIndicator: boolean;
  showWarningMessage: boolean;
  public matieres: MetfpetServiceAgent.MatiereBACRowViewModel[];

  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _exportService: ExportService,
  ) {
    this.genreMap = new Map();
  }

  ngAfterViewInit() {
    this._metfpetService.getGenreList().subscribe(data => {
      data.forEach(genre => {
        if (genre.code === 'homme') {
          this.genreMap.set('M', genre);
        } else if (genre.code === 'femme') {
          this.genreMap.set('F', genre);
        }
      })
      this._cd.markForCheck();
    });

    this._metfpetService.getOptionBACListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.optionsBACList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getMatiereBACList().subscribe(data => {
      this.matieresBACList = data;
      this._cd.markForCheck();
    });
  }

  onClickImportButton() {
    this._dialogService.openDialog(
      ImportStudentBACDialog,
      {
        width: '600px',
        data: {
          title: "Importation des résultats du BAC", 
          message: "Veuillez sélectionner l'option à importer, préciser la session puis indiquer l'emplacement du fichier", 
          type: this.importType,
          optionsBACList: this.optionsBACList
        }
      }
    ).afterClosed().subscribe(result => {
      if (result && result.file) {
        this.processFile(result.idOptionBAC, result.sessionBAC, result.file);
      }
    });
  }

  processFile(idOptionBAC: string, sessionBAC: number, file: Blob) {
    this.importingIndicator = true;
    this._userIdleService.stopWatching();

    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      var arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, {type:'binary',cellDates:true,dateNF:'dd/mm/yyyy'});
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      var rows = XLSX.utils.sheet_to_json(worksheet, {raw:false,dateNF:'dd/mm/yyyy'});
      
      this._importResultsBAC(rows, idOptionBAC, sessionBAC);
    }
    fileReader.readAsArrayBuffer(file);
  }

  private _importResultsBAC(rows: any, idOptionBAC: string, sessionBAC: number): void {
    this._metfpetService.getOptionBAC(idOptionBAC).subscribe((optionBAC : MetfpetServiceAgent.OptionBACDTO) => {
      let columnNames: Map<string, string> = new Map();
      
      for (let relation of optionBAC.matieres) {
        let foundMatiereBAC: MetfpetServiceAgent.MatiereBACDTO;
        for (let matiereBAC of this.matieresBACList) {
          if (relation.id == matiereBAC.id) {
            foundMatiereBAC = matiereBAC;
            break;
          }
        }
        columnNames.set(foundMatiereBAC.id, foundMatiereBAC.name + " (" + relation.coefficient + ")");
      }

      if (rows.length == 0) {
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.INFO, 
            message: 'Aucune donnée importée'
          }
        });
        return;
      }

      this.importStudents(this._convertToStudents(rows, columnNames, optionBAC, sessionBAC), true);
    });
  }

  private formatDateNaissance(dateNaissance: any): string {
    if (dateNaissance instanceof Date) {
      return formatDate(dateNaissance, 'yyyy-MM-dd', 'en-US');
		}
    const [day, month, year] = dateNaissance.split('/');
    return [year, month, day].join('-');
  }

  private _convertToStudents(rows: any, columnNames: Map<string, string>, 
    optionBAC: MetfpetServiceAgent.OptionBACDTO, sessionBAC: number): Array<MetfpetServiceAgent.ImportEtudiantViewModel> {
    
    let students =  new Array<MetfpetServiceAgent.ImportEtudiantViewModel>();

    students = rows.map(
      row => {
        let matterResultList: string = '';
        for (let matterId of Array.from(columnNames.keys())) {
          if (row[columnNames.get(matterId)]) {
            matterResultList = matterResultList.concat(matterId + ":" + row[columnNames.get(matterId)] + ";");
          }
        }

        var fullName = row['Nom Complet'].trim();
        var firstName = fullName.split(' ').slice(0, -1).join(' ');
        var name = fullName.split(' ').slice(-1).join(' ');
  
        return MetfpetServiceAgent.ImportEtudiantViewModel.fromJS({
          'numeroPV'                  : row['Numéro PV'], 
          'name'                      : name,
          'firstName'                 : firstName,
          'genreId'                   : this.genreMap.get(row['Sexe']).id, 
          'dateNaissance'             : this.formatDateNaissance(row['Date Naissance']), 
          'sessionBAC'                : sessionBAC,
          'optionBACId'               : optionBAC.id, 
          'codeOptionBAC'             : optionBAC.code,
          'lieuNaissance'             : row['Lieu Naissance'], 
          'nomPere'                   : row['Père'], 
          'nomMere'                   : row['Mère'], 
          'nationalite'               : row['Nationalité'], 
          'numeroPhoto'               : row['Numéro Photo'], 
          'moyenneBAC'                : row['Moyenne Examen'], 
          'nomLycee'                  : row['Origine'], 
          'nomCentreExamen'           : row['Centre'], 
          'prefectureLycee'           : row['Prefecture'], 
          'sousPrefectureLycee'       : row['Sous Prefecture'], 
          'regionLycee'               : row['Region'], 
          'districtLycee'             : row['District'], 
          'zoneLycee'                 : row['Zone'], 
          'statutOrganisationnelLycee': row['Statut'], 
          'moyenneGenerale'           : row['Moyenne Générale'], 
          'moyenneExamen'             : row['Moyenne Examen'], 
          'rang'                      : row['Rang'],
          'operation'                 : row['Opération'],
          'listResultatsMatieres'     : matterResultList
        });
      }
    );
    return students;
  }
  
  async importStudents(studentsToImport: Array<MetfpetServiceAgent.ImportEtudiantViewModel>, allowRetry: boolean) {
    this.showWarningMessage = true;
    this.imported = 0;
    this.totalToimport = studentsToImport.length;
    let toRetryList = new Array<MetfpetServiceAgent.ImportEtudiantViewModel>();

    for (let student of studentsToImport) {    
      try {
        this.imported++;
        this._cd.markForCheck();
        await this._metfpetService.importEtudiant(student).toPromise();
      } catch(error) {
        student.importErrorMessage = error.isApiException ? (JSON.parse(error.response).Message ? JSON.parse(error.response).Message : error.response) : error;
        toRetryList.push(student);
      }
    }
    if (toRetryList.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: (this.imported - toRetryList.length) + " apprenant(e)(s) importé(e)(s) avec succès, " 
            + toRetryList.length + " en échec"
        }
      });

    } else {
      this._dialogService.openConfirmDialog({
        data: {
          title: "Attention", 
          message: (this.totalToimport - toRetryList.length) + " apprenant(e)s importé(e)s avec succès, " 
            + toRetryList.length + " en échec, reprendre les éléments en échec?"
        }
      })
      .afterClosed().subscribe(confirmed => {
        if (confirmed) {
          this.importStudents(toRetryList, false);
        } else {
          this.exportFailed(toRetryList);
        }
      });
    }

    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    return {
      nbImported: this.imported,
      failedStudents: toRetryList
    };
  }

  exportFailed(toRetryList: Array<MetfpetServiceAgent.ImportEtudiantViewModel>): void {
    if (toRetryList.length > 0) {
      let json : any[] = new Array();
      for (let failed of toRetryList) {
        json.push({
          PV                 : failed.numeroPV, 
          Nom                : failed.name,
          Prénom             : failed.firstName,
          Option             : failed.codeOptionBAC,
          SessionBAC         : failed.sessionBAC, 
          Moyennee           : failed.moyenneGenerale,
          Raison             : failed.importErrorMessage
        });
      }
      this.exportAsExcelFile(json, 'failed_results');
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'Échec': worksheet }, SheetNames: ['Échec'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_import_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

  OpenBacTemplateDialog(){
    this._dialogService.openDialog(
      ImportStudentBACDialog,
      {
        width: '600px',
        data: {
          title: "Télécharger le modèle", 
          message: "Veuillez sélectionner l'option à importer", 
          type: this.importType,
          optionsBACList: this.optionsBACList,
          isDownloadTemplate: true,
        }
      }
    ).afterClosed().subscribe(result =>{
      if(result && result.idOptionBAC){               
        this.downloadTemplate(result.idOptionBAC);
      }
    })
  }

  downloadTemplate(optionId: string) {
    let headers = [
      { name: "Numéro PV", value: "numeroPV" },
      { name: "Nom Complet", value: "nomComplet" },
      { name: "Sexe", value: "sexe" },
      { name: "Date Naissance", value: "dateNaissance" },
      { name: "Lieu Naissance", value: "lieuNaissance" },
      { name: "Père", value: "nomPere" },
      { name: "Mère", value: "nomMere" },
      { name: "Nationalité", value: "nationalite" },
      { name: "Numéro Photo", value: "numeroPhoto" },
      { name: "Origine", value: "nomLycee" },
      { name: "Centre", value: "nomCentreExamen" },
      { name: "Moyenne Générale", value: "moyenneGenerale" },
      { name: "Moyenne Examen", value: "moyenneBAC" },
      { name: "Rang", value: "rang" }, 
      { name: "Region", value: "regionLycee" },
      { name: "Prefecture", value: "prefectureLycee" },
      { name: "Sous Prefecture", value: "sousPrefectureLycee" },      
      { name: "District", value: "districtLycee" },
      { name: "Zone", value: "zoneLycee" },
      { name: "Statut", value: "statutOrganisationnelLycee" },
      { name: "Opération", value: "operation" },
    ]    

    this._metfpetService.getOptionBAC(optionId).subscribe((optionBAC : MetfpetServiceAgent.OptionBACDTO) => {
      let columnNames: Map<string, string> = new Map();
      for (let relation of optionBAC.matieres) {
        let foundMatiereBAC: MetfpetServiceAgent.MatiereBACDTO;
        for (let matiereBAC of this.matieresBACList) {         
          if (relation.id == matiereBAC.id) {
            foundMatiereBAC = matiereBAC;
            if (foundMatiereBAC) {
              columnNames.set(foundMatiereBAC.id, foundMatiereBAC.name + " (" + relation.coefficient + ")");
            }
            break;
          }
        }
      }
      
      const sortedColumnNames = Array.from(columnNames).sort((a, b) => a[1].localeCompare(b[1]));
      for (let [id, name] of sortedColumnNames) {
        headers.push({ name: name, value: id });          
      }
      
      const templateName = "BACTemplate";
      this._exportService.downloadTemplate(templateName, headers);
    });  
  }
}