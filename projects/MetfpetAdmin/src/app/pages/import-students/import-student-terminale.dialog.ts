import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './import-student-terminale.dialog.html',
})
export class ImportStudentTerminaleDialog {

  form: FormGroup;
  fileMissing: boolean;

  constructor(
    public dialogRef: MatDialogRef<ImportStudentTerminaleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public fb: FormBuilder, 
  ) {
    if (this.isImportResultsTerminale() && !this.data.isDownloadTemplate) {
      this.form = this.fb.group({
        'idOptionTerminale': [null, Validators.required],
        'sessionTerminale': [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]*$")])],
      });
    } else {
      this.form = this.fb.group({
        'idOptionTerminale': [null, Validators.required],
      });
    }
  }

  onFileChange(event) {
    this.data.file = event.target.files[0];
    this.fileMissing = false;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onConfirm(): void {
    if(!this.data.isDownloadTemplate) {
      if (!this.data.file) {
        this.fileMissing = true;
        return;
      }
    }
    if (this.form.valid) {
      if (this.data.type == 'ResultsTerminale' && !this.data.isDownloadTemplate) {
        this.data.idOptionTerminale = this.form.controls['idOptionTerminale'].value;
        this.data.sessionTerminale = this.form.controls['sessionTerminale'].value;
      }else {
        this.data.idOptionTerminale = this.form.controls['idOptionTerminale'].value;
      }
      this.dialogRef.close(this.data);
    }
  }

  public isImportResultsTerminale(): boolean {
    return 'ResultsTerminale' == this.data.type;
  }

}