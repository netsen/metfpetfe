import { Location } from '@angular/common';
import { ChangeDetectorRef, Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { 
  AppSettings,
  MetfpetServiceAgent,
  PerfectScrollService,
  BaseTableComponent,
  Settings,
  DiplomeStatus,
  ConfirmDialog,
  InfoDialog,
  showLoading,
  showException,
  hideLoading,
  showSuccess,
} from 'MetfpetLib';
import { Observable, combineLatest, fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators';
import { UiPath } from '../ui-path';
import { CreateInserjeuneAccountDialog } from './create-inserjeune-account.dialog';
@Component({
  selector: 'app-diplomer-etudiant',
  templateUrl: './diplomer-etudiant.component.html',
  styleUrls: ['./diplomer-etudiant.component.css']
})
export class DiplomerEtudiantComponent extends BaseTableComponent {
  @ViewChild('searchIne', {static: true}) searchIne: ElementRef;
  @ViewChild('searchMatricule', {static: true}) searchMatricule: ElementRef;

  settings: Settings;
  title: string;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  programmeList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  nav = { 
    routerLink: '/' + UiPath.admin.diplomes.diplomer.list, 
    label: 'Diplômer les étudiants' 
  }
  constructor(
    protected _router: Router,
    protected _fb: FormBuilder,
    private _dialog: MatDialog,
    protected _store: Store<any>,
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _location: Location,
  ) { 
    super(_cd, _fb, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.sort = {prop: 'INA', dir: 'asc'};
    this.title = "Diplômes";
  }

  ngOnInit() {
    super.ngOnInit();
    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.programmeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    this.searchForm.get('institutionId').valueChanges.pipe(
      startWith(this.searchForm.get('institutionId').value),
      tap(institutionId => {
        this.programmeList=[];
        if (!this.isFirstSearch) {
          this.searchForm.get('programmeId').setValue(null);
        }
        if (!institutionId) {
          return;
        }
        this._loadPrograms(institutionId);
      })
    ).subscribe();
    combineLatest([
      fromEvent(
        this.searchIne.nativeElement, 'keyup')
        .pipe(
          debounceTime(800),
          distinctUntilChanged(),
          startWith({
            target: { value: this.searchForm.get('identifiantNational').value }
          })
        ),
      fromEvent(
        this.searchMatricule.nativeElement, 'keyup')
        .pipe(
          debounceTime(800),
          distinctUntilChanged(),
          startWith({
            target: { value: this.searchForm.get('matricule').value }
          })
        ),
      this.searchForm.get('niveauEtudeId').valueChanges.pipe(startWith(this.searchForm.get('niveauEtudeId').value)),
      this.searchForm.get('anneeAcademiqueId').valueChanges.pipe(startWith(this.searchForm.get('anneeAcademiqueId').value)),
      this.searchForm.get('programmeId').valueChanges.pipe(startWith(this.searchForm.get('programmeId').value)),
      this.searchForm.get('institutionId').valueChanges.pipe(startWith(this.searchForm.get('institutionId').value)),
    ]).subscribe(([
      eventIne,
      eventMatricule,
      eventNiveau,
      eventPeriode,
      eventProgramme,
      eventInstitution,
    ]) => {
      this.searchForm.patchValue({
        identifiantNational: eventIne ? eventIne['target'].value : null,
        matricule: eventMatricule ? eventMatricule['target'].value : null,
        eventProgramme,
        eventPeriode,
        eventNiveau,
        eventInstitution,
      }, { emitEvent: false });
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      identifiantNational: null,
      matricule: null,
      programmeId: null,
      anneeAcademiqueId: null,
      niveauEtudeId: null,
      institutionId: null,
      status:DiplomeStatus.AdmissibleDiplomation
    });
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getDiplomeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {institution: institutionId}
    })).subscribe(data => {
      this.programmeList= data.results;
      this._cd.markForCheck();
    });
  }

  goBack() {
    this._location.back()
  }
  confirmeDiplomation() {
    this._dialog.open(ConfirmDialog, {
      data: {
        message: "Veuillez confirmer l'action: Diplômer les Apprenant",
        title: "Confirmation d'action"
      }
    }).afterClosed()
      .subscribe((result) => {
        if(result){
          this._store.dispatch(showLoading());
          let filters = filterNullUndefined(this.searchForm.value); 
          this._metfpetService.changeStatusDiplome(MetfpetServiceAgent.PagingSearchDTO.fromJS({
            pageIndex: -1, filters
          })).subscribe({
            next:(res)=> {
              this._store.dispatch(showSuccess({message:"Les Apprenants ont été diplômés avec succès"}));
              this.triggerSearch();
              if (res && res.length > 0) {
                this._dialog.open(CreateInserjeuneAccountDialog, {
                  width: '550px',
                  data: {
                    etudiantIds: res
                  }
                }).afterClosed().subscribe(() => {
                });
              }
          },
          error:(error) =>{
            this._store.dispatch(showException({error: error}))
          }
        }).add(() => this._store.dispatch(hideLoading()));
        }
      })
  }
  open(row:MetfpetServiceAgent.DiplomeViewModel){
    this._router.navigate([`${UiPath.admin.students.dossier}/${row.etudiantId}`], { state: { previousPage: 'diplomer-etudiants' } });    
  }

}
/**
 * Teste toutes les valeurs d'un objet et renvoie ceux qui ne sont pas null
 * @param obj l'objet a verifier
 * @returns Le nouvel objet contenant les propriétés non nuls
 */
function filterNullUndefined(obj: { [key: string]: any }): { [key: string]: any } {
  const filteredObj: { [key: string]: any } = {};

  for (const [key, value] of Object.entries(obj)) {
    if (value !== null && value !== undefined) {
      filteredObj[key] = value;
    }
  }

  return filteredObj;
}
