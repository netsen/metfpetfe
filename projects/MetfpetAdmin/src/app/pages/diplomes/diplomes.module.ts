import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DiplomesComponent } from './diplomes.component';
import { RouterModule, Routes } from '@angular/router';
import { GenerateDiplomeComponent } from './generate-diplome.component';
import { AdminDiplomesTabComponent } from './admin-diplomes-tab.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdmettreDiplomeComponent } from './admettre-diplome.component';
import { DiplomerEtudiantComponent } from './diplomer-etudiant.component';
import { GenerationListDiplomeComponent } from './generation-list-diplome.component';
import { GenerationNouveauEtudiantsDialog } from './generation-nouveau-etudiants.dialog';
import { TelechargerDiplomesDialog } from './telecharger-diplomes.dialog';
import { CreateInserjeuneAccountDialog } from './create-inserjeune-account.dialog';

export const routes:Routes = [
  {
    path: "",
    redirectTo: "diplomes",
    pathMatch: "full",
  },
  {
    path: "diplomes",
    component: DiplomesComponent,
    data: { breadcrumb: "Diplomes" },
    pathMatch: "full",
  },
  {
    path: "admettre-diplome",
    component: AdmettreDiplomeComponent,
    data: { breadcrumb: "admettre-diplome" },
    pathMatch: "full",
  },
  {
    path:'generate-diplome',
    component:GenerateDiplomeComponent,
    data:{breadcrumb:'Générer les diplômes'.toLocaleUpperCase()}
  },
  {
    path:'generation-list-diplome',
    component:GenerationListDiplomeComponent,
    data:{breadcrumb:'Génération liste diplômés'.toLocaleUpperCase()}
  },
  {
    path:'diplomer-etudiants',
    component:DiplomerEtudiantComponent,
    data:{breadcrumb:'Diplômés les etudiants'.toLocaleUpperCase()}
  },

]

@NgModule({
  declarations: [
    DiplomesComponent,
    GenerateDiplomeComponent,
    AdminDiplomesTabComponent,
    AdmettreDiplomeComponent,
    DiplomerEtudiantComponent,
    GenerationListDiplomeComponent,
    GenerationNouveauEtudiantsDialog,
    TelechargerDiplomesDialog,
    CreateInserjeuneAccountDialog
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class DiplomesModule { }
