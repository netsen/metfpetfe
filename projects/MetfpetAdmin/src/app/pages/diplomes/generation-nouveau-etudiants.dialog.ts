import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';

@Component({
  templateUrl: './generation-nouveau-etudiants.dialog.html',
})
export class GenerationNouveauEtudiantsDialog {

  form: FormGroup;
  currentAnneeAcademique: MetfpetServiceAgent.AnneeAcademiqueDTO;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<GenerationNouveauEtudiantsDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.currentAnneeAcademique = data.currentAnneeAcademique;
    this.form = this._formBuilder.group({
    });
  }

  onConfirm() {
    this._store.dispatch(showLoading());
    this._metfpetService.loadInscriptionIdsForNouvellesBourses(this.currentAnneeAcademique.id)
      .subscribe(
        (data) => {
          this._dialogRef.close({
            inscriptionIds: data,
            anneeAcademiqueId: this.currentAnneeAcademique.id
          });
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
  }

  onClose() {
    this._dialogRef.close();
  }
}