import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateDiplomeComponent } from './generate-diplome.component';

describe('GenerateDiplomeComponent', () => {
  let component: GenerateDiplomeComponent;
  let fixture: ComponentFixture<GenerateDiplomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenerateDiplomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateDiplomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
