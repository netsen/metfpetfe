import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import * as FileSaver from 'file-saver';
import { tap } from 'rxjs/operators';

@Component({
  templateUrl: './telecharger-diplomes.dialog.html',
})
export class TelechargerDiplomesDialog {

  form: FormGroup;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programmeList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<TelechargerDiplomesDialog>,
    protected _cdRef: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.form = this._formBuilder.group({
      anneeAcademiqueId: [null, Validators.required],
      institutionId: [null, Validators.required],
      programmeId:  [null, Validators.required],
    });

    this.form.get('anneeAcademiqueId').valueChanges.pipe(
      tap(anneeAcademiqueId => {
        this.institutionList = [];
        this.form.get('institutionId').setValue(null);
        this.programmeList = [];
        this.form.get('programmeId').setValue(null);
        if (!anneeAcademiqueId) {
          return;
        }
        this._loadInstitutions(anneeAcademiqueId);
      })
    ).subscribe();

    this.form.get('institutionId').valueChanges.pipe(
      tap(institutionId => {
        this.programmeList = [];
        this.form.get('programmeId').setValue(null);
        if (!institutionId) {
          return;
        }
        this._loadPrograms(institutionId);
      })
    ).subscribe();
  }

  ngOnInit() {
    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cdRef.markForCheck();
    });
  }

  private _loadInstitutions(anneeAcademiqueId: string) {
    this._metfpetService.getInstitutionsForDiplomeDownload(anneeAcademiqueId).subscribe(data => {
        this.institutionList = data;
        this._cdRef.markForCheck();
      });
  }

  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammesForDiplomeDownload(MetfpetServiceAgent.GetProgrammesForDiplomeDownloadRequest.fromJS({
        anneeAcademiqueId: this.form.get('anneeAcademiqueId').value,
        institutionId: institutionId
      })).subscribe(data => {
        this.programmeList = data;
        this._cdRef.markForCheck();
      });
  }
  
  getZipFileName() {
    var fileName = 'Diplomes';
    var annee = this.anneeAcademiqueList.find(x => x.id == this.form.get('anneeAcademiqueId').value);
    if (annee) {
      fileName = fileName + '_' + annee.name;
    }

    var institution = this.institutionList.find(x => x.id == this.form.get('institutionId').value);
    if (institution) {
      fileName = fileName + '_' + institution.name;
    }

    var programme = this.programmeList.find(x => x.id == this.form.get('programmeId').value);
    if (programme) {
      fileName = fileName + '_' + programme.name;
    }

    fileName = fileName + '_' + new  Date().getTime() + '.zip';
    return fileName;
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.downloadDiplomes(MetfpetServiceAgent.DownloadDiplomeRequest.fromJS(Object.assign(this.form.value)))
        .subscribe(
          (file) => {
            const blob = new Blob([file.data], {
              type: 'application/zip'
            });
            FileSaver.saveAs(blob, this.getZipFileName());
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}