import { ChangeDetectorRef, Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppSettings, BaseTableComponent, ConfirmDialog, DialogService, DiplomeStatus, MetfpetServiceAgent, PerfectScrollService, Settings, SeverityEnum, showSuccess } from 'MetfpetLib';
import { Observable, combineLatest, from, fromEvent } from 'rxjs';
import { Tabs } from './admin-diplomes-tab.component';
import { Location } from '@angular/common';
import { bufferCount, concatMap, debounceTime, distinctUntilChanged, startWith, tap, toArray } from 'rxjs/operators';


@Component({
  selector: 'app-generate-diplome',
  templateUrl: './generate-diplome.component.html',
  styleUrls: ['./generate-diplome.component.css']
})
export class GenerateDiplomeComponent extends BaseTableComponent {
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  programmeList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>

  @ViewChild('searchIne', { static: true }) searchIne: ElementRef;
  @ViewChild('searchMatricule', { static: true }) searchMatricule: ElementRef;

  settings: Settings;
  title: string;
  navs = Tabs;
  loadingPercent: number;
  displayProgressBar: boolean;
  disabledGenerateButton: boolean;
  generationBatchSize: number;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    private _dialog: MatDialog,
    private _dialogService: DialogService,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _location: Location
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Diplômes';
    this.loadingPercent = 0;
    this.displayProgressBar = false;
    this.disabledGenerateButton = false;
    this.generationBatchSize = 1;

    this.sort = { prop: 'INA', dir: 'asc' };
  }


  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      identifiantNational: null,
      matricule: null,
      programmeId: null,
      anneeAcademiqueId: null,
      niveauEtudeId: null,
      institutionId: null,
      status: DiplomeStatus.Diplome
    });
  }
  ngOnInit() {
    super.ngOnInit();
    this._metfpetService.getGenerationBatchSize().subscribe(size => {
      this.generationBatchSize = size;
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.programmeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    this.searchForm.get('institutionId').valueChanges.pipe(
      startWith(this.searchForm.get('institutionId').value),
      tap(institutionId => {
        this.programmeList = [];
        if (!this.isFirstSearch) {
          this.searchForm.get('programmeId').setValue(null);
        }
        if (!institutionId) {
          return;
        }
        this._loadPrograms(institutionId);
      })
    ).subscribe();
    combineLatest([
      fromEvent(
        this.searchIne.nativeElement, 'keyup')
        .pipe(
          debounceTime(800),
          distinctUntilChanged(),
          startWith({
            target: { value: this.searchForm.get('identifiantNational').value }
          })
        ),
      fromEvent(
        this.searchMatricule.nativeElement, 'keyup')
        .pipe(
          debounceTime(800),
          distinctUntilChanged(),
          startWith({
            target: { value: this.searchForm.get('matricule').value }
          })
        ),
      this.searchForm.get('niveauEtudeId').valueChanges.pipe(startWith(this.searchForm.get('niveauEtudeId').value)),
      this.searchForm.get('anneeAcademiqueId').valueChanges.pipe(startWith(this.searchForm.get('anneeAcademiqueId').value)),
      this.searchForm.get('programmeId').valueChanges.pipe(startWith(this.searchForm.get('programmeId').value)),
      this.searchForm.get('institutionId').valueChanges.pipe(startWith(this.searchForm.get('institutionId').value)),
    ]).subscribe(([
      eventIne,
      eventMatricule,
      eventNiveau,
      eventPeriode,
      eventProgramme,
      eventIes,
    ]) => {
      this.searchForm.patchValue({
        identifiantNational: eventIne ? eventIne['target'].value : null,
        matricule: eventMatricule ? eventMatricule['target'].value : null,
        eventProgramme,
        eventPeriode,
        eventNiveau,
        eventIes,
      }, { emitEvent: false });
      this.triggerSearch();
    });
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getDiplomeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { institution: institutionId }
    })).subscribe(data => {
      this.programmeList = data.results;
      this._cd.markForCheck();
    });
  }

  goBack() {
    this._location.back()
  }

  confirmeGeneration() {
    this._dialog.open(ConfirmDialog, {
      data: {
        message: "Veuillez confirmer l'action: Générer les diplômes",
        title: "Confirmation d'action"
      }
    }).afterClosed()
      .subscribe((result) => {
        if (result) {
          this.loadingPercent = 0;
        this._metfpetService.getNoGeneratedDiplomesId(MetfpetServiceAgent.PagingSearchDTO.fromJS({
          pageIndex: -1,
          filters: this.searchForm.value
        })).pipe(
          concatMap(async (diplomeIds) => {
            let generateCompleted = 0;

            const diplomeIdsBatches = from(diplomeIds).pipe(
              bufferCount(this.generationBatchSize),
              toArray()
            );

            for (const diplomeIdsBatch of await diplomeIdsBatches.toPromise()) {
              this.displayProgressBar = true;
              this.disabledGenerateButton = true;

              var result = await this._metfpetService.generateDiplomas(diplomeIdsBatch).toPromise();

              if (result === false){
                this.disabledGenerateButton = false;
                this._dialogService.openInfoDialog({
                  data: {
                    message: `${generateCompleted} diplomes sont générés sur ${diplomeIds.length}`,
                    severity: SeverityEnum.WARN
                  }
                });
                return
              }

              generateCompleted += diplomeIdsBatch.length;
              this.loadingPercent = Math.ceil((generateCompleted / diplomeIds.length) * 100);

              if (generateCompleted === diplomeIds.length) {
                this._store.dispatch(showSuccess({ message: "Les diplomes ont été généré avec succèes" }));
                this.disabledGenerateButton = false;
                this.triggerSearch();
              }
            }
          })
        ).subscribe();
        }
      })
  }
}
