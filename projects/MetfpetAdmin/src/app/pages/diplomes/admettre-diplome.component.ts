import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import {
  AppSettings,
  MetfpetServiceAgent,
  PerfectScrollService,
  BaseTableComponent,
  DialogService,
  Settings,
  ConfirmDialog,
  showSuccess,
} from 'MetfpetLib';
import { DiplomeStatus, DiplomeStatusValues } from 'projects/MetfpetLib/src/public-api';
import { Observable, combineLatest, fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators';
import { DatatableComponent, SelectionType } from '@swimlane/ngx-datatable';


@Component({
  selector: 'app-admettre-diplome',
  templateUrl: './admettre-diplome.component.html',
})
export class AdmettreDiplomeComponent extends BaseTableComponent {

  @ViewChild('table', { static: true }) table: DatatableComponent;
  @ViewChild('searchIne', { static: true }) searchIne: ElementRef;
  @ViewChild('searchMatricule', { static: true }) searchMatricule: ElementRef;

  settings: Settings;
  title: string;
  SelectionType = SelectionType;
  statusList: Array<{ value: DiplomeStatus, name: string }>;
  diplomes: Array<MetfpetServiceAgent.DiplomeViewModel> = [];
  selectedRows: Array<MetfpetServiceAgent.DiplomeViewModel>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  programmeList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  availableAverageValues =[{name:"Ont une Moyenne",value:true},{name:"N'ont pas de moyenne",value:false}];

  constructor(
    private _location: Location,
    protected _router: Router,
    protected _fb: FormBuilder,
    private _dialog: MatDialog,
    protected _store: Store<any>,
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
  ) {
    super(_cd, _fb, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.sort = { prop: 'INA', dir: 'asc' };
    this.title = "Admettre-diplômes";
    this.statusList = DiplomeStatusValues;
  }

  ngOnInit(): void {
    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this.searchProgramme(null)

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    combineLatest([
      fromEvent(
        this.searchIne.nativeElement, 'keyup')
        .pipe(
          debounceTime(800),
          distinctUntilChanged(),
          startWith({
            target: { value: this.searchForm.get('identifiantNational').value }
          })
        ),
      fromEvent(
        this.searchMatricule.nativeElement, 'keyup')
        .pipe(
          debounceTime(800),
          distinctUntilChanged(),
          startWith({
            target: { value: this.searchForm.get('matricule').value }
          })
        ),
      this.searchForm.get('niveauEtudeId').valueChanges.pipe(startWith(this.searchForm.get('niveauEtudeId').value)),
      this.searchForm.get('anneeAcademiqueId').valueChanges.pipe(startWith(this.searchForm.get('anneeAcademiqueId').value)),
      this.searchForm.get('programmeId').valueChanges.pipe(startWith(this.searchForm.get('programmeId').value)),
      this.searchForm.get('institutionId').valueChanges.pipe(startWith(this.searchForm.get('institutionId').value)),
      this.searchForm.get('status').valueChanges.pipe(startWith(this.searchForm.get('status').value)),
      this.searchForm.get('availableAverage').valueChanges.pipe(startWith(this.searchForm.get('availableAverage').value)),
    ]).subscribe(([
      eventIne,
      eventMatricule,
      eventNiveau,
      eventPeriode,
      eventProgramme,
      eventIes,
      eventStatus,
      eventAvailableAverage
    ]) => {
      this.searchForm.patchValue({
        identifiantNational: eventIne ? eventIne['target'].value : null,
        eventStatus,
        matricule: eventMatricule ? eventMatricule['target'].value : null,
        eventProgramme,
        eventPeriode,
        eventNiveau,
        eventIes,
        eventAvailableAverage
      }, { emitEvent: false });
      this.triggerSearch();
    });

    this.searchForm.get('institutionId').valueChanges.pipe(
      startWith(this.searchForm.get('institutionId').value),
      tap(instutionId => {
        this.programmeList = []
        if (!this.isFirstSearch) {
          this.searchForm.get('programmeId').setValue(null)
        }
        if (!instutionId) {
          return
        }
        this.searchProgramme(instutionId)
      })
    ).subscribe()
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      identifiantNational: null,
      status: DiplomeStatus.Inscrit,
      matricule: null,
      programmeId: null,
      anneeAcademiqueId: null,
      niveauEtudeId: null,
      institutionId: null,
      availableAverage: null
    });
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getDiplomeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  searchProgramme(id: any) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { institution: id }
    })).subscribe(data => {

      this.programmeList = data.results;
      this._cd.markForCheck();
    });
  }

  goBack() {
    this._location.back()
  }

  confirmeAdmission() {
    this._dialog.open(ConfirmDialog, {
      data: {
        message: "Veuillez confirmer l'action l'action d'admettre les apprenants à la diplomation",
        title: "Confirmation d'action"
      }
    }).afterClosed()
      .subscribe((result) => {
        this.diplomes = this.table.selected
        this._metfpetService.updateDiplomeList(this.diplomes).subscribe(data =>{
          this.triggerSearch();
        })
      })
  }

  admitAllStudents(){
    this._dialog.open(ConfirmDialog, {
      data: {
        message: "Veuillez confirmer l'action: Admettre tous les apprenants à la diplomation",
        title: "Confirmation d'action"
      }
    }).afterClosed()
      .subscribe((result) => {
        if(result){
          this._metfpetService.admitAllStudents(MetfpetServiceAgent.PagingSearchDTO.fromJS({
            pageIndex: -1, filters: this.searchForm.value
          })).subscribe(data =>{
            this._store.dispatch(showSuccess({}));
            this.triggerSearch();
          })
        }
      })
  }
}
