import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { 
  AppSettings,
  MetfpetServiceAgent,
  PerfectScrollService,
  BaseTableComponent,
  DialogService,
  ExportService,
  AuthService,
  Settings,
} from 'MetfpetLib';
import { DiplomeStatus, DiplomeStatusValues } from 'projects/MetfpetLib/src/public-api';
import { Observable, combineLatest, fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators';
import { UiPath } from '../ui-path';
import { TelechargerDiplomesDialog } from './telecharger-diplomes.dialog';

@Component({
  templateUrl: './diplomes.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DiplomesComponent extends BaseTableComponent {
  
  @ViewChild('searchIne', {static: true}) searchIne: ElementRef;
  @ViewChild('searchMatricule', {static: true}) searchMatricule: ElementRef;

  settings: Settings;
  title: string;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  programmeList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>
  statusList: Array<{value: DiplomeStatus, name: string}>
  constructor(
    protected _router: Router,
    protected _fb: FormBuilder,
    private _dialog: MatDialog,
    protected _store: Store<any>,
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
    private _authService: AuthService,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
  ) 
  {
    super(_cd, _fb, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.sort = {prop: 'INA', dir: 'asc'};
    this.title = "Diplômes"
    this.statusList = DiplomeStatusValues;
   }

  ngOnInit(): void {

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this.searchProgramme(null)

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    combineLatest([
      fromEvent(
        this.searchIne.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('identifiantNational').value}
            })
          ),
      fromEvent(
        this.searchMatricule.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('matricule').value}
            })
          ),
          this.searchForm.get('niveauEtudeId').valueChanges.pipe(startWith(this.searchForm.get('niveauEtudeId').value)),
          this.searchForm.get('anneeAcademiqueId').valueChanges.pipe(startWith(this.searchForm.get('anneeAcademiqueId').value)),
          this.searchForm.get('programmeId').valueChanges.pipe(startWith(this.searchForm.get('programmeId').value)),
          this.searchForm.get('institutionId').valueChanges.pipe(startWith(this.searchForm.get('institutionId').value)),
          this.searchForm.get('status').valueChanges.pipe(startWith(this.searchForm.get('status').value)),
    ]).subscribe(([
      eventIne, 
      eventMatricule,
      eventNiveau,
      eventPeriode,
      eventProgramme,
      eventIes,
      eventStatut,
    ]) => {
      this.searchForm.patchValue({
        identifiantNational: eventIne ? eventIne['target'].value : null, 
        eventStatut,
        matricule: eventMatricule ? eventMatricule['target'].value : null, 
        eventProgramme,
        eventPeriode,
        eventNiveau,
        eventIes,
      }, {emitEvent: false});
      this.triggerSearch();
    });

    this.searchForm.get('institutionId').valueChanges.pipe(
      startWith(this.searchForm.get('institutionId').value),
      tap(instutionId => {
        this.programmeList=[]
        if(!this.isFirstSearch)
        {
          this.searchForm.get('programmeId').setValue(null)
        }
        if(!instutionId){
          return
        }
        this.searchProgramme(instutionId)
      })
      ).subscribe()
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      identifiantNational: null,
      status: null,
      matricule: null,
      programmeId: null,
      anneeAcademiqueId: null,
      niveauEtudeId: null,
      institutionId: null,
    });
  }

  protected _search(criteria: any): Observable<any> {
      return this._metfpetService.getDiplomeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  searchProgramme(id:any){
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {institution : id}
    })).subscribe(data => {

      this.programmeList = data.results;
      this._cd.markForCheck();
    });
  }

  open(row:MetfpetServiceAgent.DiplomeViewModel){
    this._router.navigate([`${UiPath.admin.students.dossier}/${row.etudiantId}`], { state: { previousPage: 'Diplome' } });
  }

  redirectToGenerateDiplome(){
    this._router.navigateByUrl(`${UiPath.admin.diplomes.gerenate.list}`)
  }

  goToAdmettreDiplome(){
    this._router.navigateByUrl(`${UiPath.admin.diplomes.admettre.list}`)
  }
  
  alreadyGeneratedDiplome(row:MetfpetServiceAgent.DiplomeViewModel) {
    return row.status === <any> DiplomeStatus.DiplomeGenere && row.diplomaUrl;
  }

  openDiplome(row:MetfpetServiceAgent.DiplomeViewModel) {
    if (row.diplomaUrl) {
      window.open(row.diplomaUrl, "_blank");
    }
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }

  public allowToDiplomer() {
    return this._authService.getUserType() == 'administrateurMinisteriel' &&
           (this._authService.getIdentifiant() == 'FRCA0001' ||
            this._authService.getIdentifiant() == 'VITR0001' ||
            this._authService.getIdentifiant() == 'THMODIABY' ||
            this._authService.getIdentifiant() == 'MOSACAMARA' ||
            this._authService.getIdentifiant() == 'aminatakaba' ||
            this._authService.getIdentifiant() == 'metfpet' ||
            this._authService.getIdentifiant() == 'JUBOSG');
  }

  downloadDiplome() {
    this._dialog.open(TelechargerDiplomesDialog , {
      width: '800px',
      data: {
      }
    });
  }
}
