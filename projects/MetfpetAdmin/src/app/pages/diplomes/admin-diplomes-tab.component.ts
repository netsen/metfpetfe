import { Component, OnInit } from '@angular/core';
import { UiPath } from '../ui-path';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

export const Tabs = [
  { 
    routerLink: '/' + UiPath.admin.diplomes.gerenate.list, 
    label: 'Générer les diplômes' 
  },
];
@Component({
  selector: 'app-admin-diplomes-tab',
  templateUrl: './admin-diplomes-tab.component.html',
})
export class AdminDiplomesTabComponent implements OnInit {

  navs = Tabs;
  title: string;
  routerEventSubscription: Subscription;
  
  constructor(private router: Router) {
    this.title = 'Diplômes';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = selectedTab.label;
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }

}
