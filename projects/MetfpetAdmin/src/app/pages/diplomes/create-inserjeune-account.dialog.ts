import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException
} from 'MetfpetLib';
import { UserIdleService } from 'angular-user-idle';

@Component({
  templateUrl: './create-inserjeune-account.dialog.html',
})
export class CreateInserjeuneAccountDialog {

  form: FormGroup;
  processing: boolean;
  processed: number;
  total: number;
  showWarningMessage: boolean;
  etudiantIds : Array<string>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<CreateInserjeuneAccountDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    protected _cdRef: ChangeDetectorRef,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.etudiantIds = data.etudiantIds;
    this.processing = false;
    this.form = this._formBuilder.group({
    });
  }

  ngOnInit() {
    if (this.etudiantIds) {
      this.process(this.etudiantIds);
    }
  }

  async process(ids: Array<string>) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this._store.dispatch(showLoading());
    this.showWarningMessage = true;
    this.processed = 0;
    this.total = ids.length;
    this._cdRef.markForCheck();

    for (let id of ids) {
      this.processed++;
      this._cdRef.markForCheck(); 
      try {
        await this._metfpetService.synchronizeEtudiantWithInserjeune(id).toPromise();
      } catch(error) {
      }
    }

    this.processing = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    this._store.dispatch(hideLoading());
    this._dialogRef.close(true);
  }

  onClose() {
    this._dialogRef.close();
  }
}