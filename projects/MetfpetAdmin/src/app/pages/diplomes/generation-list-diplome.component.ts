import {
  ChangeDetectorRef,
  Component,
  OnInit
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import {
  AppSettings,
  Settings,
  MetfpetServiceAgent,
  PerfectScrollService,
  showLoading,
  showSuccess,
  showException,
  hideLoading,
  DialogService,
  SeverityEnum,
  ConfirmDialog,
} from 'MetfpetLib';
import { UserIdleService } from 'angular-user-idle';
@Component({
  selector: 'app-generation-list-diplome',
  templateUrl: './generation-list-diplome.component.html',
  styleUrls: ['./generation-list-diplome.component.css']
})
export class GenerationListDiplomeComponent implements OnInit {
  settings: Settings;
  currentAnneeAcademique: MetfpetServiceAgent.AnneeAcademiqueDTO;
  processing: boolean;
  processed: number;
  totalToprocess: number;
  showWarningMessage: boolean;

  constructor(
    public appSettings: AppSettings,
    protected _cdRef: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialog: MatDialog,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) { 
    this.settings = this.appSettings.settings;
    this.processing = false;
  }

  ngOnInit() {
    this.refreshAnneAcademique();
  }
  
  refreshAnneAcademique() {
    this._metfpetService.getAnneeAcademiqueAdmissionOuverte().subscribe((data : any) => {
      if (data) {
        this.currentAnneeAcademique = data;
        this._cdRef.markForCheck();
      }
    });
  }

  generateNewDiplome() {
    this._dialog.open(ConfirmDialog, {
      data: {
        message: "Veuillez confirmer l'action: Générer la liste des apprenants à diplômer",
        title: "Confirmation d'action"
      }
    }).afterClosed()
      .subscribe((result) => {
        if (result) {
          this._metfpetService.getEtudiantListToGenerate(this.currentAnneeAcademique.id).subscribe(etudiantIds => {
            if (etudiantIds) {
              this.process(etudiantIds);
            }
          });
        }
      })
  }

  async process(etudiantIds: Array<string>) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this.showWarningMessage = true;
    this.processed = 0;
    this.totalToprocess = etudiantIds.length;

    for (let etudiantId of etudiantIds) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.generateEtudiantDiplomeItem(MetfpetServiceAgent.GenerateDiplomeRequest.fromJS({
          etudiantId: etudiantId,
          anneeAcademiqueId: this.currentAnneeAcademique.id
        })).toPromise();
      } catch(error) {
      }
    }

    this._metfpetService.updateDateDiplomeGeneration(this.currentAnneeAcademique.id).subscribe((data : any) => {
      if (data) {
        this.currentAnneeAcademique = data;
        this._cdRef.markForCheck();
      }
    });

    this.processing = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
  }

  archiveDiplome() { 
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Confirmation de publication", 
        message: "Voulez-vous vraiment archiver le statut des apprenants qui n'ont pas terminé la génération de diplôme ? Veuillez noter que cette action est irréversible.", 
        confirmBtnText: 'Archiver'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.archiveNotCompletedDiplomeItems(this.currentAnneeAcademique.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({}));
            },
            error => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }
}
