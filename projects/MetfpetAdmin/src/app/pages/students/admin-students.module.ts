import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AdminStudentsComponent } from './admin-students.component';
import { AdminManageStudentComponent } from './admin-manage-student.component';
import { AverageDialog } from './average.dialog';
import { ExportStudentDialog } from './export-student.dialog';

export const routes = [
  { 
    path: '', 
    component: AdminStudentsComponent, 
    pathMatch: 'full'
  },
  { 
    path: 'dossier/:id', 
    component: AdminManageStudentComponent, 
    data: { breadcrumb: 'Dossier' }
  },
  { 
    path: 'add', 
    component: AdminManageStudentComponent, 
    data: { breadcrumb: 'Ajouter un apprenant manuellement' } 
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    AdminStudentsComponent,
    AdminManageStudentComponent,
    AverageDialog,
    ExportStudentDialog,
  ],
  entryComponents: [
    AverageDialog,
    ExportStudentDialog,
  ]
})
export class AdminStudentsModule {

}
