import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef,
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators/';
import { Store } from '@ngrx/store';
import { 
  AppSettings,
  Settings,
  AuthorityEnum,
  StudentStatusValues,
  BaseTableComponent,
  PerfectScrollService,
  AuthService,
  DialogService,
  ExportService,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  BiometricStatusValues,
  addPostOptions,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { ExportStudentDialog } from './export-student.dialog';
import { ThrowStmt } from '@angular/compiler';

@Component({
  templateUrl: './admin-students.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminStudentsComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchEmail', {static: true}) searchEmail: ElementRef;
  @ViewChild('searchPhone', {static: true}) searchPhone: ElementRef;
  @ViewChild('searchPVNumber', {static: true}) searchPVNumber: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;

  settings: Settings;
  title: string;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  allProgramList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  centreExamenList: Array<MetfpetServiceAgent.CentreExamenRowViewModel>;
  sessionsList: Array<any>;
  optionsList: Array<MetfpetServiceAgent.OptionRowViewModel>;
  lyceesList: Array<MetfpetServiceAgent.LyceeRowViewModel>;
  statusList: Array<any>;
  genreList: Array<MetfpetServiceAgent.Genre>;
  biometricList: Array<any>;
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissement>;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _authService: AuthService,
    private _dialogService: DialogService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Dossier Apprenant - Liste des apprenants';
    this.statusList = StudentStatusValues;
    this.biometricList = BiometricStatusValues;
    this.sort = {prop: 'identifiantNationalEleve', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getGenreList().subscribe(data => {
      this.genreList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getStatusEtablissementList().subscribe(data => {
      this.statusEtablissementList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getCentreExamenListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.centreExamenList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
   })).subscribe(data => {
      this.allProgramList = data.results;
      this.programList = data.results;
      this._cd.markForCheck();
   });

    this._metfpetService.getSessionBACList().subscribe(data => {
      this.sessionsList = data.map(x => {return {name: x}});
      this._cd.markForCheck();
    });

    this._metfpetService.getOptionList().subscribe(data => {
      this.optionsList = addPostOptions(data);
      this._cd.markForCheck();
    });

    this._metfpetService.getLyceeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.lyceesList = data.results;
      this._cd.markForCheck();
    });

    this.searchForm.get('institutionId').valueChanges.pipe(
      startWith(this.searchForm.get('institutionId').value),
      tap(institutionId => {
        this.programList = this.allProgramList;
        if (!this.isFirstSearch) {
          this.searchForm.get('programmeId').setValue(null);
        }
        if (!institutionId) {
          return;
        }
        this._loadPrograms(institutionId);
      })
    ).subscribe();

    combineLatest([
      fromEvent(this.searchPVNumber.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('numeroPV').value}
          })
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      fromEvent(
        this.searchEmail.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('email').value}
            })
          ),
      fromEvent(
        this.searchPhone.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('phone').value}
            })
          ),
      this.searchForm.get('optionId').valueChanges
        .pipe(
          startWith(this.searchForm.get('optionId').value)
        ),
      this.searchForm.get('session').valueChanges
        .pipe(
          startWith(this.searchForm.get('session').value)
        ),
      this.searchForm.get('etudiantStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('etudiantStatus').value)
        ),
      this.searchForm.get('programmeId').valueChanges
        .pipe(
          startWith(this.searchForm.get('programmeId').value)
        ),
      this.searchForm.get('biometricStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('biometricStatus').value)
        ),
      this.searchForm.get('otpPhoneNumber').valueChanges
        .pipe(
          startWith(this.searchForm.get('otpPhoneNumber').value)
        ),
      this.searchForm.get('cashoutPhoneNumber').valueChanges
        .pipe(
          startWith(this.searchForm.get('cashoutPhoneNumber').value)
        ),
    ])
    .subscribe((
      [
        eventSearchPVNumber, 
        eventSearchNationalStudentIdentifier,
        eventSearchName, 
        eventFirstname,
        eventSearchEmail, 
        eventSearchPhone, 
        optionId,
        session,
        etudiantStatus,
        programmeId,
        biometricStatus,
        otpPhoneNumber,
        cashoutPhoneNumber,
      ]) => {
      this.searchForm.patchValue({
        numeroPV: eventSearchPVNumber ? eventSearchPVNumber['target'].value : null, 
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null, 
        email: eventSearchEmail ? eventSearchEmail['target'].value : null, 
        phone: eventSearchPhone ? eventSearchPhone['target'].value : null, 
        optionId,
        session,
        etudiantStatus,
        programmeId,
        biometricStatus,
        otpPhoneNumber,
        cashoutPhoneNumber,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      etudiantStatus: null,
      institutionId: null,
      programmeId: null,
      numeroPV: null,
      identifiantNationalEleve: null,
      name: null,
      firstName: null,
      session : null,
      optionId: null,
      lyceeId: null,
      centreExamenId: null,
      genreId: '',
      biometricStatus: null,
      statusEtablissement: null,
      email: null,
      phone: null,
      anneeAcademique: null,
      niveauEtude: null,
      otpPhoneNumber: null,
      cashoutPhoneNumber: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getEtudiantListPage(criteria);
  }

  add() {
    this._router.navigate([UiPath.admin.students.add], { state: { previousPage: 'Dossier apprenant' } });
  }

  open(selectedStudent: MetfpetServiceAgent.EtudiantRowViewModel) {
    this._router.navigate([`${UiPath.admin.students.dossier}/${selectedStudent.id}`], { state: { previousPage: 'Dossier apprenant' } });
  }

  getOption(row: MetfpetServiceAgent.EtudiantRowViewModel): any {
    if (row.optionBACName) {
      return row.optionBACName;
    }
    if (row.optionBEPCName) {
      return row.optionBEPCName;
    }
    if (row.optionTerminaleName) {
      return row.optionTerminaleName;
    }
    if (row.optionPostPrimaireName) {
      return row.optionPostPrimaireName;
    }
    if (row.optionPostSecondaireName) {
      return row.optionPostSecondaireName;
    }
    return row.optionAutreName;
  }

  getSession(row: MetfpetServiceAgent.EtudiantRowViewModel): any {
    if (row.sessionBAC) {
      return row.sessionBAC;
    }
    if (row.sessionBEPC) {
      return row.sessionBEPC;
    }
    return row.sessionTerminale;
  }

  exportStudents() {
    this._dialogService.openDialog(ExportStudentDialog, {
      width: '600px',
    }).afterClosed().subscribe(fields => {
      if (fields) {
        this._store.dispatch(showLoading());
        
        this._metfpetService.getEtudiantListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
          pageIndex: -1,
          filters: this.searchForm.value,
        }))
          .subscribe(data => 
            this._exportService.exportStudentsOfCurrentInstitutionToExcel(data.results, fields))
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }
  
  public isAllowed() {
    return this._authService.getUserType() == 'administrateurMinisteriel'
    || this._authService.getUserType() == 'administrateur'
    || this._authService.getUserType() == 'superviseurMinisteriel'
  }

  public isAdministrateurMinisteriel() {
    return this._authService.getUserType() == 'administrateurMinisteriel' || this._authService.getUserType() == 'administrateur';
  }

  public allowAccessStudentFolder() {
    return this._authService.getUserType() != 'directionsNationales';
  }

  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {institution: institutionId}
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });
  }
}
