import { 
  ChangeDetectionStrategy, 
  Component, 
  OnInit, 
  ViewEncapsulation 
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AppSettings,
  Settings,
  MetfpetServiceAgent,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './admin-manage-student.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminManageStudentComponent implements OnInit {

  settings: Settings;
  title: string;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  studentId: string;
  loadingIndicator: boolean;
  previousPage: string;
  iconName = 'menu-clipboard';

  constructor(
    public appSettings: AppSettings, 
    private _router: Router,
    private _route: ActivatedRoute
  ) {
    this.settings = this.appSettings.settings;
    this.previousPage = this._router.getCurrentNavigation().extras.state.previousPage;
    this.title = this.previousPage;
    if (this.previousPage === 'Admission') {
      this.iconName = 'menu-admission';
    } else if (this.previousPage == 'Cursus') {
      this.iconName = 'menu-management';
    } else {
      this.iconName = 'menu-dossier-students';
    }
  }

  ngOnInit() {
    this._route.params.subscribe(params => this.studentId = params['id']);
  }

  onLoadedStudent(student: MetfpetServiceAgent.EtudiantProfileViewModel) {
    this.student = student;
    this.title = this.previousPage + " - Dossier de " + this.student.firstName + ' ' + this.student.name;
  }

  onSavedStudent(student: MetfpetServiceAgent.EtudiantProfileViewModel) {
    this.student = student;
    this._router.navigate([UiPath.admin.students.list]);
  }
}