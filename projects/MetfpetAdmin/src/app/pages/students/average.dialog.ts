import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  templateUrl: './average.dialog.html'
})
export class AverageDialog {

  public form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AverageDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    private _formBuilder: FormBuilder) {
    this.form = this._formBuilder.group({
      'generalAverage': [null, Validators.compose([Validators.pattern("^[0-9]+(.[0-9]*)?$")])],
      'ranc': [null, Validators.compose([Validators.pattern("^[0-9]*$")])]
    });
  }

  onClose(): void {
    this.dialogRef.close();
  }

  public onConfirm(): void {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    }
  }
}