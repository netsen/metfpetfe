import { Component, Inject, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AutoCompleteComponent } from '../../components/autocomplete';

@Component ({
  templateUrl: './export-student.dialog.html',
})
export class ExportStudentDialog {

  form: FormGroup;
  @ViewChild('institutionComponent') institutionComponent: AutoCompleteComponent;

  constructor(
    public dialogRef: MatDialogRef<ExportStudentDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _formBuilder: FormBuilder
  ) {
    this.form = this._formBuilder.group({
      exportExcel: true,
      exportNom: true,
      exportPrenom: true,
      exportEmail: true, 
      exportNumeroTelephone: true,
      exportDateNaissance: true,
      exportSexe: true,
      exportPv: true,
      exportSession: true, 
      exportOption: true, 
      exportCentreExamen: true,
      exportLycee: true,
      exportMoyenne: true,
      exportLieuNaissance: true,
      exportNomPere: true, 
      exportNomMere: true,
      exportBiometricStatus: true
    });
  }

  onConfirm(): void {
    if (this.institutionComponent) {
      this.institutionComponent.validate(null);
    }
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    }
  }

  onClose(): void {
    this.dialogRef.close(null);
  }

}