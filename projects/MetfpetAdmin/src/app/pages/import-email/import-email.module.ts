import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ImportEmailDialog } from './import-email.dialog';
import { ImportEmailTabComponent } from './import-email-tab.component';
import { ImportEmailComponent } from './import-email.component';

export const routes = [
  {
    path: '', 
    component: ImportEmailTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'email',
          pathMatch: 'full'
        },
        { 
          path: 'email', 
          component: ImportEmailComponent, 
          data: { breadcrumb: 'Importer l\'e-mail de l\'apprenants' }
        },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [
    SharedModule,
    ImportEmailComponent,
  ],
  declarations: [
    ImportEmailTabComponent,
    ImportEmailComponent,
    ImportEmailDialog,
  ], 
  entryComponents: [
    ImportEmailDialog
  ]
})
export class ImportEmailModule {

}
