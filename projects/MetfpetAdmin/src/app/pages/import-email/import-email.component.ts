import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Input,
  ViewEncapsulation 
} from '@angular/core';
import { UserIdleService } from 'angular-user-idle'; 
import { 
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  Settings,
  AppSettings,
  ExportService
} from 'MetfpetLib';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { Router } from '@angular/router';
import { ImportEmailDialog } from './import-email.dialog';
import { ImportEmailTabComponent } from './import-email-tab.component';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

export interface ImportLesNotesResult {
  nbImported: number;
  failedAdmissions: Array<any>;
}

@Component({
  templateUrl: './import-email.component.html',
  styleUrls: ['./import-email.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportEmailComponent extends ImportEmailTabComponent {
  
  imported: number;
  totalToimport: number;
  importingIndicator: boolean;
  showWarningMessage: boolean;
  public settings: Settings;

  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    public appSettings: AppSettings,
    protected router: Router,
    private _exportService: ExportService
  ) {
    super(router);
    this.settings = this.appSettings.settings;
  }

  onClickImportButton() {
    this._dialogService.openDialog(
      ImportEmailDialog,
      {
        width: '600px',
        data: {
          isImport: true,
          title: "Importer l'e-mail de l'apprenants", 
          message: "Veuillez indiquer l'emplacement du fichier", 
          confirmBtnText: "Importer",
        }
      }
    ).afterClosed().subscribe(result => {
      if (result && result.file) {
        this.processFile(result.file);
      }
    });
  }

  processFile(file: Blob) {
    this.importingIndicator = true;
    this._userIdleService.stopWatching();

    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      var arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, {type:'binary',cellDates:true,dateNF:'dd/mm/yyyy'});
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      var rows = XLSX.utils.sheet_to_json(worksheet, {raw:false,dateNF:'dd/mm/yyyy'});
      
      this._importEmail(rows);
    }
    fileReader.readAsArrayBuffer(file);
  }

  private _importEmail(rows: any): void {
      if (rows.length == 0) {
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.INFO, 
            message: 'Aucune donnée importée'
          }
        });
        return;
      }

      this.importEmail(this._convertToEmail(rows), true);
  }

  private _convertToEmail(rows: any): Array<MetfpetServiceAgent.UpdateEtudiantEmailViewModel> {
    
    let apprenants =  new Array<MetfpetServiceAgent.UpdateEtudiantEmailViewModel>();

    apprenants = rows.map(
      row => {
        return MetfpetServiceAgent.UpdateEtudiantEmailViewModel.fromJS({
          'identifiantNationalEleve' : row['INA'],
          'email'                    : row['Email']
        });
      }
    );
    return apprenants;
  }
  
  async importEmail(apprenantToImport: Array<MetfpetServiceAgent.UpdateEtudiantEmailViewModel>, allowRetry: boolean) {
    this.showWarningMessage = true;
    this.imported = 0;
    this.totalToimport = apprenantToImport.length;
    let toRetryList = new Array<MetfpetServiceAgent.UpdateEtudiantEmailViewModel>();

    for (let apprenant of apprenantToImport) {    
      try {
        this.imported++;
        this._cd.markForCheck();
        let importSuccess = await this._metfpetService.updateEtudiantEmail(apprenant).toPromise();
        if (!importSuccess) {
          toRetryList.push(apprenant);
        }
      } catch(error) {
        apprenant.errorMessage = error.isApiException ? (JSON.parse(error.response).Message ? JSON.parse(error.response).Message : error.response) : error;
        toRetryList.push(apprenant);
      }
    }
    if (toRetryList.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: (this.imported - toRetryList.length) + " apprenant(e)(s) importé(e)(s) avec succès, " 
            + toRetryList.length + " en échec"
        }
      });

    } else {
      this._dialogService.openConfirmDialog({
        data: {
          title: "Attention", 
          message: (this.totalToimport - toRetryList.length) + " diplome(e)s importé(e)s avec succès, " 
            + toRetryList.length + " en échec, reprendre les éléments en échec?"
        }
      })
      .afterClosed().subscribe(confirmed => {
        if (confirmed) {
          this.importEmail(toRetryList, false);
        } else {
          this.exportFailed(toRetryList);
        }
      });
    }

    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    return {
      nbImported: this.imported,
      failedUsers: toRetryList
    };
  }

  exportFailed(toRetryList: Array<MetfpetServiceAgent.UpdateEtudiantEmailViewModel>): void {
    if (toRetryList.length > 0) {
      let json : any[] = new Array();
      for (let failed of toRetryList) {
        json.push({
          'INA'                : failed.identifiantNationalEleve,
          'Email'              : failed.email,
          'Raison'             : failed.errorMessage
        });
      }
      this.exportAsExcelFile(json, 'failed_results');
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'Échec': worksheet }, SheetNames: ['Échec'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_import_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

  downloadTemplate() {
    const headers = [
      { name: "INA", value: "identifiantNationalEleve" },
      { name: "Email", value: "email" },
    ]
    const templateName = "ApprenantEmailTemplate";
    this._exportService.downloadTemplate(templateName, headers);
  }
}
