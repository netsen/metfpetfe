import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { startWith } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent, 
  MetfpetServiceAgent,
  PerfectScrollService,
  DialogService,
  ExportService,
  showLoading,
  hideLoading,
  RaisonStatutBourseTypeValues,
  RaisonStatutBourseType,
  AuthService,
} from 'MetfpetLib';
import { Tabs } from './admin-bourses-tab.component';
import { MatDialog } from '@angular/material/dialog';
import { AdminMotifDialog } from './admin-motif.dialog';

@Component({
  templateUrl: './admin-motifs.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminMotifsComponent extends BaseTableComponent {
  
  settings: Settings;
  title: string;
  navs = Tabs;
  raisonStatutBourseTypeList = RaisonStatutBourseTypeValues;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _dialog: MatDialog,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Configuration des motifs de perte';
    this.sort = {prop: 'name', dir: 'desc'};
  }

  ngAfterViewInit() {
    combineLatest([
      this.searchForm.get('name').valueChanges.pipe(startWith(this.searchForm.get('name').value)),
      this.searchForm.get('type').valueChanges.pipe(startWith(this.searchForm.get('type').value)),
      this.searchForm.get('isActive').valueChanges.pipe(startWith(this.searchForm.get('isActive').value)),
    ])
    .subscribe(([name, type, isActive]) => {
      this.searchForm.patchValue({name, type, isActive}, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null,
      type: null,
      isActive: true,
    });
  }

  protected _search(criteria: any): Observable<any> {
    // to convert True/false value to number 1/0
    criteria.filters.isActive = +criteria.filters.isActive;
    return this._metfpetService.getRaisonStatutBourseListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  isManuel(selectedRow: MetfpetServiceAgent.RaisonStatutBourseDTO): boolean {
    return selectedRow.type === <any> RaisonStatutBourseType.Manuel;
  }

  editRaisonStatutBourse(selectedRow: MetfpetServiceAgent.RaisonStatutBourseDTO) {
    this._dialog.open(AdminMotifDialog, {
      width: '800px',
      data: {
        model: selectedRow
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  createRaisonStatutBourse() {
    this._dialog.open(AdminMotifDialog, {
      width: '800px',
      data: {
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }
}