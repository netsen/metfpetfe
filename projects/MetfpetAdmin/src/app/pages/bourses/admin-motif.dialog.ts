import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  RaisonStatutBourseStatus,
} from 'MetfpetLib';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './admin-motif.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminMotifDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.RaisonStatutBourseDTO;
  isCreation: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<AdminMotifDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.model = data.model;
    this.form = this._formBuilder.group({
      name: [null, Validators.required],
      isActive: true,
      description: null
    });

    if (this.model) {
      this.form.patchValue({
        name: this.model.name,
        description: this.model.description,
        isActive: this.model.status === <any> RaisonStatutBourseStatus.Active
      });
      this.title = 'Modifier une notif de perte d’une bourse';
      this.isCreation = false;
    } 
    else
    {
      this.title = 'Ajouter une motif de perte d’une bourse'; 
      this.isCreation = true;
    }
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveRaisonStatutBourse$: Observable<MetfpetServiceAgent.RaisonStatutBourseDTO>;

      if (this.isCreation) {
        var newRaisonStatutBourse = MetfpetServiceAgent.RaisonStatutBourseDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value, 
            { status: RaisonStatutBourseStatus.Active },
          )
        );
        saveRaisonStatutBourse$= this._metfpetService.createRaisonStatutBourse(newRaisonStatutBourse);
      } else {
        var existingRaisonStatutBourse = MetfpetServiceAgent.RaisonStatutBourseDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value, 
            { status: this.form.get('isActive').value ? RaisonStatutBourseStatus.Active : RaisonStatutBourseStatus.Inactive },
          )
        );
        saveRaisonStatutBourse$ = this._metfpetService.updateRaisonStatutBourse(existingRaisonStatutBourse);
      }

      saveRaisonStatutBourse$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}