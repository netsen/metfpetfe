import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AdminBoursesTabComponent } from './admin-bourses-tab.component';
import { AdminBoursesComponent } from './admin-bourses.component';
import { CreateBourseDialog } from './create-bourse.dialog';
import { AdminBourseComponent } from './admin-bourse.component';
import { CreateInstitutionBourseDialog } from './create-institution-bourse.dialog';
import { EditInstitutionBourseDialog } from './edit-institution-bourse.dialog';
import { CreateBoursesPaymentManuallyDialog } from './create-bourses-payment-manually.dialog';
import { AdminMotifsComponent } from './admin-motifs.component';
import { AdminMotifDialog } from './admin-motif.dialog';
import { DeterminerBourseComponent } from './determiner-bourse.component';
import { NouvellesBoursesDialog } from './nouvelles-bourses.dialog';
import { ActualisationDeterminationDialog } from './actualisation-determination.dialog';
import { BoursePaymentsComponent } from './bourse-payments.component';
import { BourseRapportsComponent } from './bourse-rapports.component';
import { ArchiveBoursesDialog } from './archive-bourses.dialog';
import { AdminTrimestresComponent } from './admin-trimestres.component';
import { AdminTrimestreDialog } from './admin-trimestre.dialog';
import { ChangeBoursesStatusDialog } from './change-bourses-status.dialog';
import { SelectionSessionEtTrimestreDialog } from './selection-session-et-trimestre.dialog';
import { AutomatiqueBoursePaymentsComponent } from './automatique-bourse-payments.component';
import { EnvoieTermineDialog } from './envoie-termine.dialog';
import { ExportBoursePaymentsDialog } from './export-bourse-payments.dialog';
import { LibererLesBoursesDialog } from './liberer-les-bourses.dialog';

export const routes = [
  { 
    path: '', 
    component: AdminBoursesTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'list',
          pathMatch: 'full'
        },
        { 
          path: 'list',
          component: AdminBoursesComponent, 
          data: { breadcrumb: 'Configuration des bourses' }  
        },
        { 
          path: 'list/view/:id', 
          component: AdminBourseComponent, 
          data: { breadcrumb: 'Configuration des bourses' }  
        },
        { 
          path: 'motifs', 
          component: AdminMotifsComponent, 
          data: { breadcrumb: 'Configuration des motifs de perte' }  
        },
        { 
          path: 'trimestres', 
          component: AdminTrimestresComponent, 
          data: { breadcrumb: 'Configuration des trimestres' }  
        },
      ]
  },
  {
    path: 'determiner', 
    component: DeterminerBourseComponent, 
    pathMatch: 'full'
  },
  {
    path: 'payments', 
    component: BoursePaymentsComponent, 
    pathMatch: 'full'
  },
  {
    path: 'payments/:session/:trimestre', 
    component: AutomatiqueBoursePaymentsComponent, 
    pathMatch: 'full'
  },
  {
    path: 'rapports', 
    component: BourseRapportsComponent, 
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    AdminBoursesTabComponent,
    AdminBoursesComponent,
    CreateBourseDialog,
    AdminBourseComponent,
    AdminMotifsComponent,
    CreateInstitutionBourseDialog,
    EditInstitutionBourseDialog,
    CreateBoursesPaymentManuallyDialog,
    AdminMotifDialog,
    DeterminerBourseComponent,
    ActualisationDeterminationDialog,
    NouvellesBoursesDialog,
    BoursePaymentsComponent,
    BourseRapportsComponent,
    ArchiveBoursesDialog,
    AdminTrimestresComponent,
    AdminTrimestreDialog,
    ChangeBoursesStatusDialog,
    SelectionSessionEtTrimestreDialog,
    AutomatiqueBoursePaymentsComponent,
    EnvoieTermineDialog,
    ExportBoursePaymentsDialog,
    LibererLesBoursesDialog
  ]
})
export class AdminBoursesModule {}
