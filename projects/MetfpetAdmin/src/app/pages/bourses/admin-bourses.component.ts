import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { startWith, debounceTime, distinctUntilChanged } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent, 
  MetfpetServiceAgent,
  PerfectScrollService,
  addPostOptions,
  TypeApprenantValues,
  TypeBourseValues,
  AuthService,
} from 'MetfpetLib';
import { Tabs } from './admin-bourses-tab.component';
import { CreateBourseDialog } from './create-bourse.dialog';
import { MatDialog } from '@angular/material/dialog';

@Component({
  templateUrl: './admin-bourses.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminBoursesComponent extends BaseTableComponent {
  
  @ViewChild('searchName', {static: true}) searchName: ElementRef;

  settings: Settings;
  title: string;
  navs = Tabs;
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissement>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  niveauAccesList: Array<MetfpetServiceAgent.NiveauAcces>;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  typeApprenantList = TypeApprenantValues;
  typeBourseList = TypeBourseValues;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _route: ActivatedRoute, 
    private _dialog: MatDialog,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Personnels';
    this.sort = {prop: 'name', dir: 'desc'};
  }

  ngAfterViewInit() {
    this._metfpetService.getStatusEtablissementList().subscribe(data => {
      this.statusEtablissementList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getNiveauAccesList().subscribe(data => {
      this.niveauAccesList = data;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    combineLatest([
      fromEvent(
        this.searchName.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('name').value}
            })
          ),
        this.searchForm.get('statusEtablissementId').valueChanges.pipe(startWith(this.searchForm.get('statusEtablissementId').value)),
        this.searchForm.get('institutionId').valueChanges.pipe(startWith(this.searchForm.get('institutionId').value)),
        this.searchForm.get('niveauAccesId').valueChanges.pipe(startWith(this.searchForm.get('niveauAccesId').value)),
        this.searchForm.get('typeApprenant').valueChanges.pipe(startWith(this.searchForm.get('typeApprenant').value)),
        this.searchForm.get('niveauEtudeId').valueChanges.pipe(startWith(this.searchForm.get('niveauEtudeId').value)),
        this.searchForm.get('montant').valueChanges.pipe(startWith(this.searchForm.get('montant').value)),
        this.searchForm.get('typeBourse').valueChanges.pipe(startWith(this.searchForm.get('typeBourse').value)),
    ])
    .subscribe(([
      eventName, 
      statusEtablissementId,
      institutionId,
      niveauAccesId,
      typeApprenant,
      niveauEtudeId,
      montant,
      typeBourse
    ]) => {
      this.searchForm.patchValue({
        name: eventName ? eventName['target'].value : null, 
        statusEtablissementId,
        institutionId,
        niveauAccesId,
        typeApprenant,
        niveauEtudeId,
        montant,
        typeBourse
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null,
      institutionId: null,
      niveauAccesId: null,
      typeApprenant: null,
      niveauEtudeId: null,
      statusEtablissementId: null,
      montant: null,
      typeBourse: null
    });
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getBourseListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  create() {
    this._dialog.open(CreateBourseDialog, {
      width: '800px',
      data: {
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  open(selectedRow) {
    this._router.navigate([`view/${selectedRow.id}`], {relativeTo: this._route});
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }
}
