import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ElementRef, 
  ViewChild, 
  ViewEncapsulation 
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { combineLatest, fromEvent, Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { 
  AppSettings,
  Settings,
  MetfpetServiceAgent,
  PerfectScrollService,
  BaseTableComponent,
  DialogService,
  PaymentServiceAgent,
  showLoading,
  hideLoading,
  CreateBoursesPaymentDialog,
  CreateBoursesTrimestrePaymentDialog,
  AuthService,
  PaiementBourseStatus,
  showException,
} from 'MetfpetLib';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators';
import { UiPath } from '../ui-path';
import { SelectionType } from '@swimlane/ngx-datatable';
import { EnvoieTermineDialog } from './envoie-termine.dialog';

@Component({
  templateUrl: './automatique-bourse-payments.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutomatiqueBoursePaymentsComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;
  @ViewChild('searchEmail', {static: true}) searchEmail: ElementRef;
  @ViewChild('searchPhone', {static: true}) searchPhone: ElementRef;

  settings: Settings;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  nsgBankingStatus: Array<any>;
  shareMailOrPhoneStatus: Array<any>;
  loadingIndicator: boolean = false;
  selectedRows: Array<MetfpetServiceAgent.AutomatiquePaiementBourseRowViewModel>;
  SelectionType = SelectionType;
  anneeAcademique: MetfpetServiceAgent.AnneeAcademiqueDTO;
  bourseTrimestre: MetfpetServiceAgent.BourseTrimestreDTO;
  nsgBankAmount: number;
  boursePaymentSummaryInfo: MetfpetServiceAgent.AutomatiquePaiementBourseSummaryInfo;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _fb: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _dialog: MatDialog,
    private _route: ActivatedRoute, 
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _location: Location,
  ) {
    super(_cd, _fb, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.selectedRows = [];
    this.nsgBankingStatus = [
      {value : 'Oui', name : 'Oui'},
      {value : 'Non', name : 'Non'},
    ];
    this.shareMailOrPhoneStatus = [
      {value : 'Oui', name : 'Oui'},
      {value : 'Non', name : 'Non'},
    ];
    this.sort = {prop: 'etudiantINA', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();

    this.getNsgBankAmount(false);

    this._route.params.subscribe(params => {
      let session = params['session'];
      let trimestre = params['trimestre'];
      this._metfpetService.getAnneeAcademique(session).subscribe(data => {
        this.anneeAcademique = data;
        this._cd.markForCheck();
      });
      this._metfpetService.getBourseTrimestre(trimestre).subscribe(data => {
        this.bourseTrimestre = data;
        this._cd.markForCheck();
      });
      this.searchForm.patchValue({
        anneeAcademiqueId: session,
        bourseTrimestreId: trimestre
      });
      this._cd.markForCheck();
    });

    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });

    combineLatest([
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(this.searchFirstname.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('firstName').value}
          })
        ),
      fromEvent(this.searchEmail.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('email').value}
          })
        ),
      fromEvent(this.searchPhone.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('phone').value}
          })
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      this.searchForm.get('status').valueChanges
        .pipe(
          startWith(this.searchForm.get('status').value)
        ),
      this.searchForm.get('anneeAcademiqueId').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademiqueId').value)
        ),
      this.searchForm.get('prefectureId').valueChanges
        .pipe(
          startWith(this.searchForm.get('prefectureId').value)
        ),
      this.searchForm.get('institutionId').valueChanges
        .pipe(
          startWith(this.searchForm.get('institutionId').value)
        ),
      this.searchForm.get('nsgBanking').valueChanges
        .pipe(
          startWith(this.searchForm.get('nsgBanking').value)
        ),
      this.searchForm.get('shareMailOrPhone').valueChanges
        .pipe(
          startWith(this.searchForm.get('shareMailOrPhone').value)
        )
    ])
    .subscribe((
      [
        eventSearchName, 
        eventSearchFirstname,
        eventSearchEmail,
        eventSearchPhone,
        eventSearchNationalStudentIdentifier,
        status,
        anneeAcademiqueId,
        prefectureId,
        institutionId,
        nsgBanking,
        shareMailOrPhone,
      ]) => {
      this.searchForm.patchValue({
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventSearchFirstname ? eventSearchFirstname['target'].value : null, 
        email: eventSearchEmail ? eventSearchEmail['target'].value : null, 
        phone: eventSearchPhone ? eventSearchPhone['target'].value : null, 
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        status,
        anneeAcademiqueId,
        prefectureId,
        institutionId,
        nsgBanking,
        shareMailOrPhone,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  getBoursePaymentSummaryInfo() {
    this._metfpetService.getAutomatiquePaiementBourseSummaryInfo(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1,
      filters: this.searchForm.value,
    })).subscribe(data => {
      this.boursePaymentSummaryInfo = data;
      this._cd.markForCheck();
    });
  }

  getNsgBankAmount(needToRelogin: boolean) {
    this._store.dispatch(showLoading());
    this._metfpetService.getNsgBankAmount()
      .subscribe(
        (amount) => {
          this.nsgBankAmount = amount;
          this._cd.markForCheck();
        },
        error =>  {
          if (needToRelogin) {
            this._store.dispatch(showException({error: error}));
            this.reloginNsgBank();
          }
        })
        .add(() => this._store.dispatch(hideLoading()));
  }

  reloginNsgBank() {
    this._metfpetService.getCyclosAuthorizationRequest().subscribe(url => {
      if (url)
      {
        window.open(url, "_blank");
      }
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      status: <any>PaiementBourseStatus.APayer,
      name: null,
      firstName: null,
      identifiantNationalEleve: null,
      anneeAcademiqueId: null,
      institutionId: null,
      programmeId: null,
      prefectureId: null,
      bourseTrimestreId: null,
      nsgBanking: null,
      email: null,
      phone: null,
      shareMailOrPhone: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  onSelect({ selected }) {
    this.selectedRows.splice(0, this.selectedRows.length);
    this.selectedRows.push(...selected);
  }

  protected _search(criteria: any): Observable<any> {
    this.getBoursePaymentSummaryInfo();
    return this._metfpetService.getAutomatiquePaiementBourseListPage(criteria);
  }

  public payBourseAutomatique() {
    this.payBourse(false);
  }

  public payAllBourses() {
    this.payBourse(true);
  }

  payBourse(isPayAll: boolean) {
    if (!isPayAll && this.selectedRows.length == 0) {
      return;
    }
    var bourseAmount = 0;
    if (isPayAll)
    {
      bourseAmount = this.boursePaymentSummaryInfo.totalBoursesWithNsgBank;
    }
    else
    {
      for (var item of this.selectedRows) {
        if (item.nsgBanking == 'Oui') {
          bourseAmount += item.montant;
        }
      }
    }
    if (this.nsgBankAmount > bourseAmount) 
      {
        this._dialogService.openConfirmDialog({
          width: '600px',
          data: {
            title: 'Vérification avant envoie',
            confirmBtnText: 'Lancer l’envoie',
            message: `
              <p>En cliquant sur terminer, vous allez créditer les comptes des apprenants.</p> 
    
              <p>Les apprenants n’ayant pas créés ou renseignés leurs comptes NSG Banking recevront un message leur demandant de se créer un compte NSG Banking et/ou de le renseigner sur la plateforme.</p>
            `
          }
        })
        .afterClosed().subscribe(confirmed => {
          if (confirmed) {
            if (isPayAll)
            {
              this._metfpetService.getEtudiantIdsForAutomatiquePaiementBourse(MetfpetServiceAgent.PagingSearchDTO.fromJS({
                pageIndex: -1,
                filters: this.searchForm.value,
              })).subscribe(etudiantIds => {
                this.processPayment(etudiantIds);
              });
            }
            else
            {
              this.processPayment(this.selectedRows.map(x => x.etudiantId));
            }
          }
        });
      }
      else 
      {
        this._dialogService.openConfirmDialog({
          width: '600px',
          data: {
            title: 'Attention',
            confirmBtnText: 'Fermer',
            message: `
              <p>Le montant disponible dans le compte du METFP n’est pas suffisant pour procéder à l’envoie.</p> 
    
              <p>Veuillez approvisionner suffisamment le compte pour pouvoir poursuivre.</p>
            `
          }
        });
      }
  }

  processPayment(etudiantIds: Array<string>) {
    this._dialog.open(EnvoieTermineDialog, {
      width: '850px',
      data: {
        etudiantIds: etudiantIds,
        anneeAcademiqueId: this.anneeAcademique.id,
        bourseTrimestreId: this.bourseTrimestre.id
      }
    }).afterClosed().subscribe(() => {
      this.triggerSearch();
    });
  }

  goBack() {
    this._location.back();
  }
}