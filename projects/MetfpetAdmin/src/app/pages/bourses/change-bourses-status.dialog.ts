import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  PaiementBourseStatus,
} from 'MetfpetLib';

@Component({
  templateUrl: './change-bourses-status.dialog.html',
})
export class ChangeBoursesStatusDialog {

  form: FormGroup;
  currentAnneeAcademique: MetfpetServiceAgent.AnneeAcademiqueDTO;
  trimestreList: Array<MetfpetServiceAgent.BourseTrimestreDTO>;
  statusList = [
    {value : PaiementBourseStatus.AVenir, name : 'A venir'},
    {value : PaiementBourseStatus.Archive, name : 'Archivé'},
  ];
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<ChangeBoursesStatusDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.currentAnneeAcademique = data.currentAnneeAcademique;
    this.trimestreList = data.trimestreList;
    this.form = this._formBuilder.group({
      anneeAcademiqueId: this.currentAnneeAcademique.id,
      bourseTrimestreId: [null, Validators.required],
      status: [null, Validators.required],
    });
  }

  onConfirm() {
    this._store.dispatch(showLoading());
    this._metfpetService.updatePaiementBourseStatut(MetfpetServiceAgent.UpdatePaiementBourseStatutInfo.fromJS(
      Object.assign(this.form.value)))
      .subscribe(
        (data) => {
          this._dialogRef.close();
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
  }

  onClose() {
    this._dialogRef.close();
  }
}