import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  RaisonStatutBourseStatus,
} from 'MetfpetLib';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './admin-trimestre.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminTrimestreDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.BourseTrimestreDTO;
  isCreation: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<AdminTrimestreDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.model = data.model;
    this.form = this._formBuilder.group({
      name: [null, Validators.required],
      isJanvier: false,
      isFevrier: false,
      isMars: false,
      isAvril: false,
      isMai: false,
      isJuin: false,
      isJuillet: false,
      isAout: false,
      isSeptembre: false,
      isOctobre: false,
      isNovembre: false,
      isDecembre: false
    });

    this.title = 'Configuration des trimestres';
    if (this.model) {
      this.form.patchValue({
        name: this.model.name,
        isJanvier: this.model.mois && this.model.mois.includes('Janvier'),
        isFevrier: this.model.mois && this.model.mois.includes('Février'),
        isMars: this.model.mois && this.model.mois.includes('Mars'),
        isAvril: this.model.mois && this.model.mois.includes('Avril'),
        isMai: this.model.mois && this.model.mois.includes('Mai'),
        isJuin: this.model.mois && this.model.mois.includes('Juin'),
        isJuillet: this.model.mois && this.model.mois.includes('Juillet'),
        isAout: this.model.mois && this.model.mois.includes('Août'),
        isSeptembre: this.model.mois && this.model.mois.includes('Septembre'),
        isOctobre: this.model.mois && this.model.mois.includes('Octobre'),
        isNovembre: this.model.mois && this.model.mois.includes('Novembre'),
        isDecembre: this.model.mois && this.model.mois.includes('Décembre'),
      });
      this.isCreation = false;
    } 
    else
    {
      this.isCreation = true;
    }
  }

  getMois() {
    let mois = '';
    if (this.form.get('isJanvier').value) {
      mois = mois + 'Janvier ; ';
    }
    if (this.form.get('isFevrier').value) {
      mois = mois + 'Février ; ';
    }
    if (this.form.get('isMars').value) {
      mois = mois + 'Mars ; ';
    }
    if (this.form.get('isAvril').value) {
      mois = mois + 'Avril ; ';
    }
    if (this.form.get('isMai').value) {
      mois = mois + 'Mai ; ';
    }
    if (this.form.get('isJuin').value) {
      mois = mois + 'Juin ; ';
    }
    if (this.form.get('isJuillet').value) {
      mois = mois + 'Juillet ; ';
    }
    if (this.form.get('isAout').value) {
      mois = mois + 'Août ; ';
    }
    if (this.form.get('isSeptembre').value) {
      mois = mois + 'Septembre ; ';
    }
    if (this.form.get('isOctobre').value) {
      mois = mois + 'Octobre ; ';
    }
    if (this.form.get('isNovembre').value) {
      mois = mois + 'Novembre ; ';
    }
    if (this.form.get('isDecembre').value) {
      mois = mois + 'Décembre ; ';
    }
    if (mois.endsWith(' ; ')) {
      mois = mois.slice(0, mois.length - 3);
    }
    return mois;
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveBourseTrimestre$: Observable<MetfpetServiceAgent.BourseTrimestreDTO>;

      if (this.isCreation) {
        var newBourseTrimestre = MetfpetServiceAgent.BourseTrimestreDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value, 
            { status: RaisonStatutBourseStatus.Active },
            { mois: this.getMois() }
          )
        );
        saveBourseTrimestre$= this._metfpetService.createBourseTrimestre(newBourseTrimestre);
      } else {
        var existingBourseTrimestre = MetfpetServiceAgent.BourseTrimestreDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value,
            { mois: this.getMois() }
          )
        );
        saveBourseTrimestre$ = this._metfpetService.updateBourseTrimestre(existingBourseTrimestre);
      }

      saveBourseTrimestre$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}