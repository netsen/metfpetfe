import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
} from 'MetfpetLib';
import { UserIdleService } from 'angular-user-idle';

@Component({
  templateUrl: './envoie-termine.dialog.html',
  styleUrls: ['./envoie-termine.component.scss']
})
export class EnvoieTermineDialog {

  form: FormGroup;
  etudiantIds: Array<string>;
  anneeAcademiqueId: string;
  bourseTrimestreId: string;
  totalCases: number;
  totalSentReminder: number;
  totalPayBourse: number;
  totalMontant: number;
  isFinished: boolean;
  title: string;
  percentage: number;
  processed: number;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<EnvoieTermineDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
    private _userIdleService: UserIdleService,
  ) {
    this.etudiantIds = data.etudiantIds;
    this.anneeAcademiqueId = data.anneeAcademiqueId;
    this.bourseTrimestreId = data.bourseTrimestreId;
    this.title = 'Envoie en cours';
    this.totalCases = this.etudiantIds.length;
    this.totalSentReminder = 0;
    this.totalPayBourse = 0;
    this.totalMontant = 0;
    this.isFinished = false;
    this.percentage = 0;
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.process();
    });
  }

  async process() {
    this._userIdleService.startWatching();
    this.processed = 0;

    for (let etudiantId of this.etudiantIds) {    
      try {
        this.processed++;
        var result = await this._metfpetService.executeAutomatiquePaiementBourse(MetfpetServiceAgent.AutomatiquePaiementBourseRequest.fromJS({
          etudiantId: etudiantId,
          bourseTrimestreId: this.bourseTrimestreId,
          anneeAcademiqueId: this.anneeAcademiqueId
        })).toPromise();
        this.percentage = Math.floor(this.processed * 100 / this.totalCases);
        this._cd.markForCheck();
        if (result.isSendReminder)
        {
          this.totalSentReminder++;
        }
        else
        {
          this.totalPayBourse++;
          this.totalMontant = this.totalMontant + result.amount;
        }
      } catch(error) {
      }
    }

    this.title = 'Envoie terminé';
    this.percentage = 100;
    this.isFinished = true;
    this._cd.markForCheck();
    this._userIdleService.stopWatching();
  }

  onConfirm() {
    this._dialogRef.close();
  }

  onClose() {
    this._dialogRef.close();
  }
}