import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  validateForm,
  MetfpetServiceAgent,
} from 'MetfpetLib';

@Component({
  templateUrl: './liberer-les-bourses.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LibererLesBoursesDialog {

  form: FormGroup;
  trimestreList: Array<MetfpetServiceAgent.BourseTrimestreDTO>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<LibererLesBoursesDialog>,
    private _formBuilder: FormBuilder,
  ) {
    this.trimestreList = data.trimestreList;
    this.anneeAcademiqueList = data.anneeAcademiqueList;
    this.form = this._formBuilder.group({
      session: [null, Validators.required],
      trimestre: [null, Validators.required],
    });
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._dialogRef.close({
        session: this.form.get('session').value,
        trimestre: this.form.get('trimestre').value
      });
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}