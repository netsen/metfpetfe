import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ElementRef, 
  ViewChild, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { combineLatest, fromEvent, Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { 
  AppSettings,
  Settings,
  MetfpetServiceAgent,
  PerfectScrollService,
  BaseTableComponent,
  DialogService,
  ExportService,
  PaymentServiceAgent,
  PaiementBourseStatusValues,
  MoisAnneeValues,
  showLoading,
  hideLoading,
  AuthService,
} from 'MetfpetLib';

@Component({
  templateUrl: './bourse-rapports.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BourseRapportsComponent extends BaseTableComponent {

  settings: Settings;
  currentAnneeAcademique: MetfpetServiceAgent.AnneeAcademiqueDTO;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  loadingIndicator: boolean = false;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _fb: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _exportService: ExportService,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
  ) {
    super(_cd, _fb, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.sort = {prop: 'etudiantINA', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();
    
    this._metfpetService.getAnneeAcademiqueAdmissionOuverte().subscribe((data : any) => {
      if (data) {
        this.currentAnneeAcademique = data;
        this._cd.markForCheck();
      }
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      status: null,
      name: null,
      firstName: null,
      identifiantNationalEleve: null,
      anneeAcademiqueId: null,
      institutionId: null,
      programmeId: null,
      moisAnnee: null,
      prefectureId: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getPaiementBourseListPage(criteria);
  }

  public payBourseManual() {

  }

  public generateRapport() {

  }

  public editBoursePayment(row){

  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }
}