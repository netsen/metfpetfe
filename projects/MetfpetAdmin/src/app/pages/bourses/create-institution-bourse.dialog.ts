import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  TypeBourse,
  BourseStatus,
  ProgrammesTypeValues,
} from 'MetfpetLib';

@Component({
  templateUrl: './create-institution-bourse.dialog.html',
  styleUrls: ['./create-institution-bourse.dialog.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateInstitutionBourseDialog {

  title: string;
  form: FormGroup;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programmesTypes = ProgrammesTypeValues;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<CreateInstitutionBourseDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.form = this._formBuilder.group({
      bourseId: data.bourseId,
      institutionId: [null, Validators.required],
      programmesType: [null, Validators.required],
    });

    this.title = 'Ajouter une institution'; 

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
    });
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.createBourseInstitution(MetfpetServiceAgent.BourseInstitutionDTO.fromJS(
        Object.assign(this.form.value)))
        .subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}