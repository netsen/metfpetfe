import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { 
  AppSettings,
  Settings,
  MetfpetServiceAgent,
  PerfectScrollService,
  showSuccess,
  showLoading,
  hideLoading,
  DialogService,
} from 'MetfpetLib';
import { NouvellesBoursesDialog } from './nouvelles-bourses.dialog';
import { ActualisationDeterminationDialog } from './actualisation-determination.dialog';
import { UserIdleService } from 'angular-user-idle';
import { ArchiveBoursesDialog } from './archive-bourses.dialog';
import { ChangeBoursesStatusDialog } from './change-bourses-status.dialog';

@Component({
  templateUrl: './determiner-bourse.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeterminerBourseComponent {

  settings: Settings;
  currentAnneeAcademique: MetfpetServiceAgent.AnneeAcademiqueDTO;
  trimestreList: Array<MetfpetServiceAgent.BourseTrimestreDTO>;
  bourseInfoList: MetfpetServiceAgent.BourseInfoList;
  processing: boolean;
  processed: number;
  totalToprocess: number;
  showWarningMessage: boolean;
  isOtpEnabled: boolean;
  otpPermission: MetfpetServiceAgent.OtpPermissionsDTO;
  loadingOtpPermission: boolean = true;

  constructor(
    public appSettings: AppSettings,
    protected _cdRef: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialog: MatDialog,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialogService: DialogService,
  ) 
  {
    this.settings = this.appSettings.settings;
    this.processing = false;
  }

  ngOnInit() {
    this.refreshAnneAcademique();
    this._metfpetService.getBourseInfoList().subscribe(data => {
      this.bourseInfoList = data;
    });
    this._metfpetService.getBourseTrimestreList().subscribe(data => {
      this.trimestreList = data.results;
    });
  }
  
  refreshAnneAcademique() {
    this._metfpetService.getAnneeAcademiqueAdmissionOuverte().subscribe((data : any) => {
      if (data) {
        this.currentAnneeAcademique = data;
        this._cdRef.markForCheck();
      }
    });

    this.getOtpPermission();
  }

  async process(inscriptionIds: Array<string>, anneeAcademiqueId: any, isNouvellesBourses: boolean) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this.showWarningMessage = true;
    this.processed = 0;
    this.totalToprocess = inscriptionIds.length;

    for (let inscriptionId of inscriptionIds) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.determinateBourse(MetfpetServiceAgent.DeterminationBourseInfo.fromJS(Object.assign( 
          {
            bourseInfos: this.bourseInfoList,
            anneeAcademiqueId: anneeAcademiqueId,
            inscriptionId: inscriptionId
          }))).toPromise();
      } catch(error) {
      }
    }

    if (isNouvellesBourses) {
      this._metfpetService.updateDateDerniereDetermination(anneeAcademiqueId).subscribe((data : any) => {
        if (data) {
          this.currentAnneeAcademique = data;
          this._cdRef.markForCheck();
        }
      });
    } else {
      this._metfpetService.updateDateDerniereActualisation(anneeAcademiqueId).subscribe((data : any) => {
        if (data) {
          this.currentAnneeAcademique = data;
          this._cdRef.markForCheck();
        }
      });
    }

    this.processing = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
  }

  createNewBourse() {
    this._dialog.open(NouvellesBoursesDialog, {
      width: '600px',
      data: {
        currentAnneeAcademique: this.currentAnneeAcademique
      }
    }).afterClosed().subscribe(data => {
      if (data) {
        this.process(data.inscriptionIds, data.anneeAcademiqueId, true);
      }
    });
  }

  updateBourse() {
    this._dialog.open(ActualisationDeterminationDialog, {
      width: '650px',
      data: {
      }
    }).afterClosed().subscribe(data => {
      if (data) {
        this.process(data.inscriptionIds, data.anneeAcademiqueId, false);
      }
    });
  }

  async archive(paiementBourseIds: Array<string>) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this.showWarningMessage = true;
    this.processed = 0;
    this.totalToprocess = paiementBourseIds.length;

    for (let paiementBourseId of paiementBourseIds) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.archivePaiementBourse(paiementBourseId).toPromise();
      } catch(error) {
      }
    }

    this.processing = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
  }

  archiveBourse() {
    this._dialog.open(ArchiveBoursesDialog, {
      width: '650px',
      data: {
      }
    }).afterClosed().subscribe(data => {
      if (data) {
        this.archive(data.paiementBourseIds);
      }
    });
  }

  changeStatus() {
    this._dialog.open(ChangeBoursesStatusDialog, {
      width: '650px',
      data: {
        trimestreList: this.trimestreList,
        currentAnneeAcademique: this.currentAnneeAcademique
      }
    });
  }

  getOtpPermission(){
    this._metfpetService.getOtpPermission().subscribe((data)=>{
      this.otpPermission = data;
      this.isOtpEnabled = this.otpPermission.isOtpEnabled;
      this.loadingOtpPermission = false;
      this._cdRef.markForCheck();
    })
  }

  changeOtpPermissionStatus(isOtpEnabled: boolean){
    this._dialogService.openConfirmDialog({
      data: {
        title: "Attention", 
        message: "Voulez-vous changer le statut DE la fonctionnalIté OTP ?"
      }
    })
    .afterClosed().subscribe(confirmed => {
      if (confirmed) {
        if(this.otpPermission){
          this._store.dispatch(showLoading());
          this.otpPermission.isOtpEnabled = isOtpEnabled;
          this._metfpetService.updateOtpPermission(this.otpPermission).subscribe((res)=>{
            this.otpPermission = res;
            this.isOtpEnabled = this.otpPermission.isOtpEnabled;
            this._store.dispatch(hideLoading());
            this._store.dispatch(showSuccess({}));
            this._cdRef.markForCheck();
          })
        }
      }
    });
  }
}