import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ElementRef, 
  ViewChild, 
  ViewEncapsulation 
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { combineLatest, fromEvent, Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { 
  AppSettings,
  Settings,
  MetfpetServiceAgent,
  PerfectScrollService,
  BaseTableComponent,
  DialogService,
  ExportService,
  PaymentServiceAgent,
  PaiementBourseStatusValues,
  MoisAnneeValues,
  showLoading,
  hideLoading,
  CreateBoursesPaymentDialog,
  CreateBoursesTrimestrePaymentDialog,
  BiometricStatusValues,
  AuthService,
  SeverityEnum,
} from 'MetfpetLib';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators';
import { CreateBoursesPaymentManuallyDialog } from './create-bourses-payment-manually.dialog';
import { UiPath } from '../ui-path';
import { SelectionSessionEtTrimestreDialog } from './selection-session-et-trimestre.dialog';
import { ExportBoursePaymentsDialog } from './export-bourse-payments.dialog';
import { LibererLesBoursesDialog } from './liberer-les-bourses.dialog';

@Component({
  templateUrl: './bourse-payments.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BoursePaymentsComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;
  @ViewChild('searchEmail', {static: true}) searchEmail: ElementRef;
  @ViewChild('searchPhone', {static: true}) searchPhone: ElementRef;

  settings: Settings;
  currentAnneeAcademique: MetfpetServiceAgent.AnneeAcademiqueDTO;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  trimestreList: Array<MetfpetServiceAgent.BourseTrimestreDTO>;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  statusList: Array<any>;
  biometricList: Array<any>;
  moisAnneeList: Array<any>;
  nsgBankingStatus: Array<any>;
  ecobankStatus: Array<any>;
  telephoneAuthentifieStatus: Array<any>;
  shareMailOrPhoneStatus: Array<any>;
  hasMoreThanOneStatus: Array<any>;
  redoublantStatus: Array<any>;
  loadingIndicator: boolean = false;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _fb: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _dialog: MatDialog,
    private _exportService: ExportService,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _route: ActivatedRoute, 
  ) {
    super(_cd, _fb, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.statusList = PaiementBourseStatusValues;
    this.moisAnneeList = MoisAnneeValues;
    this.biometricList = BiometricStatusValues;
    this.nsgBankingStatus = [
      {value : 'Oui', name : 'Oui'},
      {value : 'Non', name : 'Non'},
    ];
    this.shareMailOrPhoneStatus = [
      {value : 'Oui', name : 'Oui'},
      {value : 'Non', name : 'Non'},
    ];
    this.hasMoreThanOneStatus = [
      {value : 'Oui', name : 'Oui'},
      {value : 'Non', name : 'Non'},
    ];
    this.ecobankStatus = [
      {value : 'Oui', name : 'Oui'},
      {value : 'Non', name : 'Non'},
    ];
    this.telephoneAuthentifieStatus = [
      {value : 'Oui', name : 'Oui'},
      {value : 'Non', name : 'Non'},
    ];
    this.redoublantStatus = [
      {value : 'Oui', name : 'Oui'},
      {value : 'Non', name : 'Non'},
    ];
    this.sort = {prop: 'etudiantINA', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();
    
    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getBourseTrimestreList().subscribe(data => {
      this.trimestreList = data.results;
    });

    this._metfpetService.getAnneeAcademiqueAdmissionOuverte().subscribe((data : any) => {
      if (data) {
        this.currentAnneeAcademique = data;
        this._cd.markForCheck();
      }
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results.filter(x => x.statusEtablissementName === 'Publique');
      this._cd.markForCheck();
    });

    this.searchForm.get('institutionId').valueChanges.pipe(
      startWith(this.searchForm.get('institutionId').value),
      tap(institutionId => {
        this.programList = [];
        if (!this.isFirstSearch) {
          this.searchForm.get('programmeId').setValue(null);
        }
        if (!institutionId) {
          return;
        }
        this._loadPrograms(institutionId);
      })
    ).subscribe();

    combineLatest([
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(this.searchFirstname.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('firstName').value}
          })
        ),
      fromEvent(this.searchEmail.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('email').value}
          })
        ),
      fromEvent(this.searchPhone.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('phone').value}
          })
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      this.searchForm.get('status').valueChanges
        .pipe(
          startWith(this.searchForm.get('status').value)
        ),
      this.searchForm.get('anneeAcademiqueId').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademiqueId').value)
        ),
      this.searchForm.get('moisAnnee').valueChanges
        .pipe(
          startWith(this.searchForm.get('moisAnnee').value)
        ),
      this.searchForm.get('prefectureId').valueChanges
        .pipe(
          startWith(this.searchForm.get('prefectureId').value)
        ),
      this.searchForm.get('institutionId').valueChanges
        .pipe(
          startWith(this.searchForm.get('institutionId').value)
        ),
      this.searchForm.get('programmeId').valueChanges
        .pipe(
          startWith(this.searchForm.get('programmeId').value)
        ),
      this.searchForm.get('biometricStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('biometricStatus').value)
        ),
      this.searchForm.get('nsgBanking').valueChanges
        .pipe(
          startWith(this.searchForm.get('nsgBanking').value)
        ),
      this.searchForm.get('shareMailOrPhone').valueChanges
        .pipe(
          startWith(this.searchForm.get('shareMailOrPhone').value)
        ),
      this.searchForm.get('niveauEtudeId').valueChanges
        .pipe(
          startWith(this.searchForm.get('niveauEtudeId').value)
        ),
      this.searchForm.get('hasMoreThanOne').valueChanges
        .pipe(
          startWith(this.searchForm.get('hasMoreThanOne').value)
        ),
      this.searchForm.get('ecobank').valueChanges
        .pipe(
          startWith(this.searchForm.get('ecobank').value)
        ),
      this.searchForm.get('telephoneAuthentifie').valueChanges
        .pipe(
          startWith(this.searchForm.get('telephoneAuthentifie').value)
        ),
      this.searchForm.get('redoublant').valueChanges
        .pipe(
          startWith(this.searchForm.get('redoublant').value)
        )
    ])
    .subscribe((
      [
        eventSearchName, 
        eventSearchFirstname,
        eventSearchEmail,
        eventSearchPhone,
        eventSearchNationalStudentIdentifier,
        status,
        anneeAcademiqueId,
        moisAnnee,
        prefectureId,
        institutionId,
        programmeId,
        biometricStatus,
        nsgBanking,
        shareMailOrPhone,
        niveauEtudeId,
        hasMoreThanOne,
        ecobank,
        telephoneAuthentifie,
        redoublant,
      ]) => {
      this.searchForm.patchValue({
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventSearchFirstname ? eventSearchFirstname['target'].value : null, 
        email: eventSearchEmail ? eventSearchEmail['target'].value : null, 
        phone: eventSearchPhone ? eventSearchPhone['target'].value : null, 
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        status,
        anneeAcademiqueId,
        moisAnnee,
        prefectureId,
        institutionId,
        programmeId,
        biometricStatus,
        nsgBanking,
        shareMailOrPhone,
        niveauEtudeId,
        hasMoreThanOne,
        ecobank,
        telephoneAuthentifie,
        redoublant,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      status: null,
      name: null,
      firstName: null,
      identifiantNationalEleve: null,
      anneeAcademiqueId: null,
      institutionId: null,
      programmeId: null,
      moisAnnee: null,
      prefectureId: null,
      biometricStatus: null,
      from: null,
      to: null,
      email: null,
      phone: null,
      nsgBanking: null,
      shareMailOrPhone: null,
      niveauEtudeId: null,
      hasMoreThanOne: null,
      ecobank: null,
      telephoneAuthentifie: null,
      redoublant: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getPaiementBourseListPage(criteria);
  }

  public payBourseManual() {
    this._dialog.open(CreateBoursesPaymentManuallyDialog , {
      width: '800px',
      data: {
        anneeAcademiqueId : this.currentAnneeAcademique.id
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  public payBourse(row: MetfpetServiceAgent.PaiementBourseRowViewModel) {
    this._dialog.open(CreateBoursesPaymentDialog , {
      width: '800px',
      data: {
        paymentBoursesData: row,
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }
  
  public payBourseTrimestre() {
    this._dialog.open(CreateBoursesTrimestrePaymentDialog , {
      width: '800px',
      data: {
        anneeAcademiqueId : this.currentAnneeAcademique.id
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  public editBoursePayment(row: MetfpetServiceAgent.PaiementBourseRowViewModel) {
    this._router.navigate([`${UiPath.admin.students.dossier}/${row.etudiantId}`], { state: { previousPage: 'Paiement des bourses' } });
  }

  public exportPayments() {
    this._dialog.open(ExportBoursePaymentsDialog , {
      width: '600px',
      data: {
        filters: this.searchForm.value
      }
    });
  }
  
  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {institution: institutionId}
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });
  }

  public isAllowed() {
    return this._authService.getUserType() == 'administrateurMinisteriel'
    || this._authService.getUserType() == 'administrateur'
    || this._authService.getUserType() == 'superviseurMinisteriel'
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }
  
  public isDirectionsNationalesRole() {
    return this._authService.getUserType() == 'directionsNationales';
  }

  public payBourseAutomatique() {
    this._metfpetService.getCyclosAuthorizationRequest().subscribe(url => {
      if (url)
      {
        window.open(url, "_blank");
        this._dialog.open(SelectionSessionEtTrimestreDialog , {
          width: '800px',
          data: {
            anneeAcademiqueList: this.anneeAcademiqueList,
            trimestreList: this.trimestreList
          }
        }).afterClosed().subscribe(data => {
          if (data) {
            this._router.navigate([`${data.session}/${data.trimestre}`], {relativeTo: this._route});
          }
        });
      }
    });
  }

  public libererLesBourses() {
    this._dialog.open(LibererLesBoursesDialog , {
      width: '800px',
      data: {
        anneeAcademiqueList: this.anneeAcademiqueList,
        trimestreList: this.trimestreList
      }
    }).afterClosed().subscribe(data => {
      if (data) {
        this._metfpetService.libererLesBourses(MetfpetServiceAgent.LibererLesBoursesRequest.fromJS({
          trimestreId: data.trimestre, 
          anneeAcademiqueId: data.session
        })).subscribe(result => {
          if (result)
          {
            this._dialogService.openInfoDialog({
              width: '600px',
              data: {
                title: 'Confirmation de libération des bourses',
                severity: SeverityEnum.INFO, 
                closeBtnText: 'Terminer',
                message: `
                  Les bourses ont bien été libérées et les apprenants peuvent maintenant procéder à leur retrait.
                `
              }
            }).afterClosed().subscribe(() => {this.triggerSearch();});
          }
        });
      }
    });
  }
}