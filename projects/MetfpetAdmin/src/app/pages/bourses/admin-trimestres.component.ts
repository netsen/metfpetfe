import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { startWith } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent, 
  MetfpetServiceAgent,
  PerfectScrollService,
  DialogService,
  ExportService,
  showLoading,
  hideLoading,
  RaisonStatutBourseTypeValues,
  RaisonStatutBourseType,
  AuthService,
} from 'MetfpetLib';
import { Tabs } from './admin-bourses-tab.component';
import { MatDialog } from '@angular/material/dialog';
import { AdminTrimestreDialog } from './admin-trimestre.dialog';

@Component({
  templateUrl: './admin-trimestres.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminTrimestresComponent extends BaseTableComponent {
  
  settings: Settings;
  title: string;
  navs = Tabs;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _dialog: MatDialog,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Configuration des trimestres';
    this.sort = {prop: 'name', dir: 'desc'};
  }

  ngAfterViewInit() {
    this.triggerSearch();
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
    });
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getBourseTrimestreList();
  }

  editTrimestre(selectedRow: MetfpetServiceAgent.BourseTrimestreDTO) {
    this._dialog.open(AdminTrimestreDialog, {
      width: '800px',
      data: {
        model: selectedRow
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  createTrimestre() {
    this._dialog.open(AdminTrimestreDialog, {
      width: '800px',
      data: {
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }
}