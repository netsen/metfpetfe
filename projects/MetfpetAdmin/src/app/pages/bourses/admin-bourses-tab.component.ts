import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

export const Tabs = [
  { 
    routerLink: '/' + UiPath.admin.bourses.list, 
    label: 'Configuration des bourses' 
  },
  {
    routerLink: '/' + UiPath.admin.bourses.motifs, 
    label: 'Configuration des motifs de perte' 
  },
  {
    routerLink: '/' + UiPath.admin.bourses.trimestres, 
    label: 'Configuration des trimestres' 
  }
];

@Component({
  templateUrl: './admin-bourses-tab.component.html',
})
export class AdminBoursesTabComponent {

  navs = Tabs;
  title: string;
  routerEventSubscription: Subscription;
  
  constructor(private router: Router) {
    this.title = 'Configuration des bourses';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = selectedTab.label;
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
