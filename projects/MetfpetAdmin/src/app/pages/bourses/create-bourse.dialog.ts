import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  TypeBourse,
  BourseStatus,
} from 'MetfpetLib';

@Component({
  templateUrl: './create-bourse.dialog.html',
})
export class CreateBourseDialog {

  title: string;
  form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<CreateBourseDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.form = this._formBuilder.group({
      name: [null, Validators.required],
      montant: [null, Validators.required],
      isCummulative: true,
      isActive: true,
      hasInstitutionSpecifique: true,
      hasNiveauAcces: true,
      hasNiveauEtude: true,
      hasTypeApprenant: true,
      hasStatusEtablissement: true
    });

    this.title = 'Création d’une bourse'; 
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.createBourse(MetfpetServiceAgent.CreateBourse.fromJS(
        Object.assign(this.form.value,
          { status: this.form.get('isActive').value ? BourseStatus.Active : BourseStatus.Inactive },
          { typeBourse: this.form.get('isCummulative').value ? TypeBourse.Cummulative : TypeBourse.NonCummulative })))
        .subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}