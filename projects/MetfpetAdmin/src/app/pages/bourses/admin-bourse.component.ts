import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component,
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray} from '@angular/forms';
import { Observable } from 'rxjs';
import { 
  AppSettings,
  Settings,
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  TypeApprenantValues,
  TypeBourseValues,
  BourseStatus,
  TypeBourse,
  DialogService,
  TypeApprenant,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { UiPath } from '../ui-path';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { CreateInstitutionBourseDialog } from './create-institution-bourse.dialog';
import { MatDialog } from '@angular/material/dialog';
import { EditInstitutionBourseDialog } from './edit-institution-bourse.dialog';

@Component({
  templateUrl: './admin-bourse.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminBourseComponent {

  form: FormGroup;
  settings: Settings;
  title: string;
  public _model: MetfpetServiceAgent.BourseDTO;
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissement>;
  niveauAccesList: Array<MetfpetServiceAgent.NiveauAcces>;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  bourseNiveauAcces: Array<MetfpetServiceAgent.BourseNiveauAccesDTO>;
  bourseNiveauEtude: Array<MetfpetServiceAgent.BourseNiveauEtudeDTO>;
  typeApprenantList = TypeApprenantValues;
  typeBourseList = TypeBourseValues;
  hasInstitutionSpecifique: boolean;
  hasNiveauAcces: boolean;
  hasNiveauEtude: boolean;
  hasTypeApprenant: boolean;
  hasStatusEtablissement: boolean;
  bourseInstitution: Array<MetfpetServiceAgent.BourseInstitutionDTO>;

  constructor(
    public appSettings: AppSettings,
    private _cd: ChangeDetectorRef,
    private _fb: FormBuilder,
    private _location: Location,
    private _router: Router,
    private _route: ActivatedRoute, 
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _dialog: MatDialog,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings;
    this.title = 'Configuration des bourses';
    this.bourseNiveauAcces = [];
    this.bourseNiveauEtude = [];
    this.form = this._fb.group({
      name: [null, Validators.required],
      montant: [null, Validators.required],
      isCummulative: true,
      isActive: true,
      statusEtablissementId: null,
      typeApprenant: null,
      hasInstitutionSpecifique: true,
      hasNiveauAcces: true,
      hasNiveauEtude: true,
      hasTypeApprenant: true,
      hasStatusEtablissement: true
    });
  }

  ngOnInit() {
    this._metfpetService.getStatusEtablissementList().subscribe(data => {
      this.statusEtablissementList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getNiveauAccesList().subscribe(data => {
      this.niveauAccesList = data;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this._route.params.subscribe(params => {
      let id = params['id'];
      this.getBourseInstitutions(id);
      this._metfpetService.getBourse(id).subscribe(data => {
        this._model = data;
        this.bourseNiveauAcces = this._model.bourseNiveauAcces;
        this.bourseNiveauEtude = this._model.bourseNiveauEtude;
        this.hasInstitutionSpecifique = this._model.hasInstitutionSpecifique;
        this.hasNiveauAcces = this._model.hasNiveauAcces;
        this.hasNiveauEtude = this._model.hasNiveauEtude;
        this.hasTypeApprenant = this._model.hasTypeApprenant;
        this.hasStatusEtablissement = this._model.hasStatusEtablissement;
        this.form.patchValue({
          name                     : this._model.name,
          montant                  : this._model.montant,
          isActive                 : this._model.status === <any>BourseStatus.Active,
          isCummulative            : this._model.typeBourse === <any>TypeBourse.Cummulative,
          statusEtablissementId    : this._model.statusEtablissementId,
          typeApprenant            : this._model.typeApprenant,
          hasInstitutionSpecifique : this._model.hasInstitutionSpecifique,
          hasNiveauAcces           : this._model.hasNiveauAcces,
          hasNiveauEtude           : this._model.hasNiveauEtude,
          hasTypeApprenant         : this._model.hasTypeApprenant,
          hasStatusEtablissement   : this._model.hasStatusEtablissement
        });
        this._cd.markForCheck();
      });
    });
  }

  private getBourseInstitutions(id: any) {
    this._metfpetService.getBourseInstitutions(id).subscribe(data => {
      this.bourseInstitution = data;
      this._cd.markForCheck();
    });
  }

  onHasStatusEtablissementToggle(event: MatSlideToggleChange) {
    this.hasStatusEtablissement = event.checked;
    if (this.hasInstitutionSpecifique) {
      if (this.statusEtablissementList && this.statusEtablissementList.length > 0) {
        this.form.controls['statusEtablissementId'].setValue(this.statusEtablissementList[0].id);
      }
    } else {
      this.form.controls['statusEtablissementId'].setValue(null);
    }
    this._cd.markForCheck();
  }

  onHasInstitutionSpecifiqueToggle(event: MatSlideToggleChange) {
    this.hasInstitutionSpecifique = event.checked;
    this._cd.markForCheck();
  }
  
  onHasNiveauAccesToggle(event: MatSlideToggleChange) {
    this.hasNiveauAcces = event.checked;
    this._cd.markForCheck();
  }

  onHasTypeApprenantToggle(event: MatSlideToggleChange) {
    this.hasTypeApprenant = event.checked;
    if (this.hasTypeApprenant) {
      this.form.controls['typeApprenant'].setValue(<any> TypeApprenant.Guinneen);
    } else {
      this.form.controls['typeApprenant'].setValue(null);
    }
    this._cd.markForCheck();
  }

  onHasNiveauEtudeToggle(event: MatSlideToggleChange) {
    this.hasNiveauEtude = event.checked;
    this._cd.markForCheck();
  }

  public isSelectedNiveauAcces(niveauAcces: MetfpetServiceAgent.NiveauAcces): boolean {
    if (this.bourseNiveauAcces) {
      var index = this.bourseNiveauAcces.findIndex(x => x.niveauAccesId === niveauAcces.id);
      return index > -1;
    }
    return false;
  }

  public checkedNiveauAccesChange(checked: boolean, niveauAcces: MetfpetServiceAgent.NiveauAcces) {
    if (checked) {
      this.bourseNiveauAcces = [...this.bourseNiveauAcces, 
        MetfpetServiceAgent.BourseNiveauAccesDTO.fromJS(Object.assign( 
          {
            bourseId: this._model.id,
            niveauAccesId: niveauAcces.id
          }))];
    } else {
      var index =  this.bourseNiveauAcces.findIndex(x => x.niveauAccesId === niveauAcces.id);
      if (index > -1) {
        this.bourseNiveauAcces.splice(index, 1);
      }
    }
  }

  public isSelectedNiveauEtude(niveauEtude: MetfpetServiceAgent.NiveauEtudeRowViewModel): boolean {
    if (this.bourseNiveauEtude) {
      var index = this.bourseNiveauEtude.findIndex(x => x.niveauEtudeId === niveauEtude.id);
      return index > -1;
    }
    return false;
  }

  public checkedNiveauEtudeChange(checked: boolean, niveauEtude: MetfpetServiceAgent.NiveauEtudeRowViewModel) {
    if (checked) {
      this.bourseNiveauEtude = [...this.bourseNiveauEtude, 
        MetfpetServiceAgent.BourseNiveauEtudeDTO.fromJS(Object.assign( 
          {
            bourseId: this._model.id,
            niveauEtudeId: niveauEtude.id
          }))];
    } else {
      var index =  this.bourseNiveauEtude.findIndex(x => x.niveauEtudeId === niveauEtude.id);
      if (index > -1) {
        this.bourseNiveauEtude.splice(index, 1);
      }
    }
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.updateBourse(MetfpetServiceAgent.BourseDTO.fromJS(
        Object.assign({}, 
          this._model, 
          this.form.value,
          { status: this.form.get('isActive').value ? BourseStatus.Active : BourseStatus.Inactive },
          { typeBourse: this.form.get('isCummulative').value ? TypeBourse.Cummulative : TypeBourse.NonCummulative },
          { bourseNiveauAcces: this.bourseNiveauAcces },
          { bourseNiveauEtude: this.bourseNiveauEtude })
      ))
      .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([UiPath.admin.bourses.list]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public onDelete(): void {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention", 
        message: "Voulez-vous définitivement supprimer ce bourse ?", 
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteBourse(this._model.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({message: 'Le bourse a été supprimée'}));
              this._router.navigate([UiPath.admin.bourses.list]);
            },
            (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }
  
  goBack() {
    this._location.back();
  }

  addBourseInstitution() {
    this._dialog.open(CreateInstitutionBourseDialog, {
      width: '800px',
      data: {
        bourseId: this._model.id
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.getBourseInstitutions(this._model.id);
      }
    });
  }

  editBourseInstitution(row: MetfpetServiceAgent.BourseInstitutionDTO) {
    this._dialog.open(EditInstitutionBourseDialog, {
      width: '900px',
      data: {
        bourseInstitution: row
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.getBourseInstitutions(this._model.id);
      }
    });
  }

  deleteBourseInstitution(row: MetfpetServiceAgent.BourseInstitutionDTO) {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention", 
        message: "Voulez-vous définitivement supprimer ce bourse institution ?", 
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteBourseInstitution(row)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({message: 'Le bourse institution a été supprimée'}));
              this.getBourseInstitutions(this._model.id);
            },
            (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }
}