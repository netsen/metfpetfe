import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  TypeBourse,
  BourseStatus,
  ProgrammesTypeValues,
  ProgrammesType,
} from 'MetfpetLib';

@Component({
  templateUrl: './edit-institution-bourse.dialog.html',
  styleUrls: ['./edit-institution-bourse.dialog.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditInstitutionBourseDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.BourseInstitutionDTO;
  programmesTypes = ProgrammesTypeValues;
  programmeList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<EditInstitutionBourseDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.model = data.bourseInstitution;
    this.form = this._formBuilder.group({
      programmesType: [null, Validators.required],
      programmes: this._formBuilder.array([]),
    });

    this.title = 'Gestion institution et programmes'; 

    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {'institution': this.model.institutionId}
    })).subscribe(data => {
      this.programmeList = data.results;
      this._cd.markForCheck();
    });

    this.form.controls['programmesType'].setValue(this.model.programmesType);
    if (this.model.programmes) {
      for (let programmeId of this.model.programmes) {
        this.programmes.push(this._formBuilder.group({
          programmeId: programmeId, 
        }));
      }
      this._cd.markForCheck();
    }
  }

  isSpecifique(): boolean {
    return this.form.controls['programmesType'].value === <any> ProgrammesType.Specifique;
  }

  get programmes() {
    return this.form.controls['programmes'] as FormArray;
  }

  addProgramme() {
    this.programmes.push(this._formBuilder.group({
      programmeId: null, 
    }));
    this._cd.markForCheck();
  }

  delete(i: number) {
    this.programmes.removeAt(i);
    this._cd.markForCheck();
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this.model.programmesType = this.form.controls['programmesType'].value;
      this.model.programmes = this.programmes.value.filter(x => !!x.programmeId).map(x => x.programmeId);
      this._metfpetService.updateBourseInstitution(MetfpetServiceAgent.BourseInstitutionDTO.fromJS(this.model))
        .subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}