import { 
  ChangeDetectionStrategy,
  Component, 
  ViewChild, 
  OnInit, 
  ViewEncapsulation 
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { 
  AppSettings,
  Settings,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  validateForm,
  showException,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { UiPath } from '../ui-path';

@Component({
  selector: 'app-optionBAC',
  templateUrl: './optionBAC.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptionBACComponent implements OnInit {

  @ViewChild(DatatableComponent) table: DatatableComponent;
  form: FormGroup;
  settings: Settings;
  title: string;
  isCreation: boolean;
  matters : Array<MetfpetServiceAgent.MatiereBACRowViewModel>;
  coefficients = Array(21).fill(1).map((x, i) => i);
  private _model: MetfpetServiceAgent.OptionBACDTO;
  rows = [];
  temp = [];
  selected = [];

  constructor(
    public appSettings: AppSettings, 
    private _activatedRoute: ActivatedRoute, 
    private _fb:FormBuilder,
    private _location: Location, 
    private _router: Router,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings; 
    this.form = this._fb.group({
      'code': [null, Validators.compose([Validators.required])],
      'name': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'categorie': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'description': [null],
    });
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      let optionBACId = params['id'];
      this.isCreation = !optionBACId;

      this._metfpetService.getMatiereBACList().subscribe((data: any) => {
        this.matters = data;
        this.temp = [...this.matters];
        this.rows = this.matters;

        if (!this.isCreation) {
          this._metfpetService.getOptionBAC(optionBACId).subscribe((data : any) => {
            this._model = data;
            this.title = this._model.name;
            this.form.controls['code'].setValue(this._model.code);
            this.form.controls['name'].setValue(this._model.name);
            this.form.controls['categorie'].setValue(this._model.categorie);
            this.form.controls['description'].setValue(this._model.description);
            this.updateMatieresTable();
          });
        }
      });
    });
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid && this._validateMatterCoefficient()) {
      this._store.dispatch(showLoading());
      var saveOptionBAC$: Observable<MetfpetServiceAgent.OptionBACDTO>;

      if (this.isCreation) {
        saveOptionBAC$= this._metfpetService.createOptionBAC(MetfpetServiceAgent.OptionBACDTO.fromJS(
          Object.assign({}, this._model, this.form.value, {'matieres': this.selected})
        ));
      } else {
        saveOptionBAC$ = this._metfpetService.updateOptionBAC(MetfpetServiceAgent.OptionBACDTO.fromJS(
          Object.assign({}, this._model, this.form.value, {'matieres': this.selected})
        ));
      }

      saveOptionBAC$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([UiPath.admin.lycees.optionsBAC.list]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  updateMatieresTable() {
    for (let selectedMatter of this._model.matieres) {
      for (let matter of this.matters) {
        if (selectedMatter.id == matter.id) {
          matter.coefficient = selectedMatter.coefficient;
          this.selected.push(matter);
        }
      }
    }
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    const temp = this.temp.filter(function(d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows = temp;
    this.table.offset = 0;
  }

  private _validateMatterCoefficient(): boolean {
    let isValid = true;
    this.selected.forEach(matter => {
      if (matter.coefficient == undefined || matter.coefficient == 0) {
        isValid = false;
        return;
      }
    });
    if (!isValid) {
      this._store.dispatch(showError({
        message: 'Veuillez sélectionner un coéfficient pour chaque matière sélectionnée' 
      }));
    }
    return isValid;
  }

  goBack() {
    this._location.back();
  }

}