import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import {
  AppSettings,
  Settings, 
  CollegeStatus,
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './college.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CollegeComponent implements OnInit {

  form: FormGroup;
  settings: Settings;
  title: string;
  isCreation: boolean = true;
  optionsBEPCList : Array<MetfpetServiceAgent.OptionBEPCRowViewModel>;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  zoneList: Array<MetfpetServiceAgent.Zone>;
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissement>;
  private _model: MetfpetServiceAgent.CollegeDTO;
  
  constructor(
    public appSettings: AppSettings,
    private _activatedRoute: ActivatedRoute, 
    private _cdRef: ChangeDetectorRef,
    private _fb: FormBuilder, 
    private _location: Location,
    private _router: Router,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {

    this.settings = this.appSettings.settings; 
    this.form = this._fb.group({
      'status': [null],
      'name': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'prefectureId': [null, Validators.required],
      'zoneId': [null, Validators.required],
      'statusEtablissementId': [null, Validators.required],
      'optionBEPCIds' : [null]
    });
  }

  ngOnInit() {
    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cdRef.markForCheck();
    });
    
    this._metfpetService.getZoneList().subscribe(data => {
      this.zoneList = data;
      this._cdRef.markForCheck();
    });
    
    this._metfpetService.getStatusEtablissementList().subscribe(data => {
      this.statusEtablissementList = data;
      this._cdRef.markForCheck();
    });
    
    this._metfpetService.getOptionBEPCListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.optionsBEPCList = data.results;
      this._cdRef.markForCheck();
    });

    this._activatedRoute.params.subscribe(params => {
      let id = params['id'];
      this.isCreation = !id;

      if (!this.isCreation) {
        this._metfpetService.getCollege(id).subscribe(data => {
          this._model = data;
          this.title = this._model.name;
          this.form.patchValue({
            'name': this._model.name,
            'prefectureId': this._model.prefectureId,
            'zoneId': this._model.zoneId,
            'statusEtablissementId': this._model.statusEtablissementId,
            'optionBEPCIds': data.options.map(x => x.id),
            'status': this._model.status === <any>CollegeStatus.Active,
          });
          this._cdRef.markForCheck();
        });
      }
    });
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveCollege$: Observable<MetfpetServiceAgent.CollegeDTO>;

      if (this.isCreation) {
        saveCollege$= this._metfpetService.createCollege(MetfpetServiceAgent.CollegeDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: CollegeStatus.Active },
            { options: this.form.controls['optionBEPCIds'].value.map(x => {return {id: x}})}
          )
        ));
      } else {
        saveCollege$ = this._metfpetService.updateCollege(MetfpetServiceAgent.CollegeDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: this.form.get('status').value ? CollegeStatus.Active : CollegeStatus.Inactive },
            { options: this.form.controls['optionBEPCIds'].value.map(x => {return {id: x}})}
          )
        ));
      }

      saveCollege$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([`${UiPath.admin.lycees.colleges.list}`]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  goBack() {
    this._location.back();
  }

}