import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import {
  AppSettings,
  Settings, 
  LyceeStatus,
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  ReferenceDataService,
  showException,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './lycee.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LyceeComponent implements OnInit {

  form: FormGroup;
  settings: Settings;
  title: string;
  isCreation: boolean = true;
  optionsBACList : Array<MetfpetServiceAgent.OptionBACRowViewModel>;
  optionsTerminaleList : Array<MetfpetServiceAgent.OptionBACRowViewModel>;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  zoneList: Array<MetfpetServiceAgent.Zone>;
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissement>;
  districtList: Array<string>;
  private _model: MetfpetServiceAgent.LyceeDTO;
  
  constructor(
    public appSettings: AppSettings,
    private _activatedRoute: ActivatedRoute, 
    private _cdRef: ChangeDetectorRef,
    private _fb: FormBuilder, 
    private _location: Location,
    private _router: Router,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _referenceDataService: ReferenceDataService
  ) {

    this.settings = this.appSettings.settings; 
    this.form = this._fb.group({
      'status': [null],
      'name': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'prefectureId': [null, Validators.required],
      'district': [null, Validators.required],
      'zoneId': [null, Validators.required],
      'statusEtablissementId': [null, Validators.required],
      'optionBACIds' : [null],
      'optionTerminaleIds' : [null]
    });
  }

  ngOnInit() {
    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cdRef.markForCheck();
    });
    
    this._metfpetService.getZoneList().subscribe(data => {
      this.zoneList = data;
      this._cdRef.markForCheck();
    });
    
    this._metfpetService.getStatusEtablissementList().subscribe(data => {
      this.statusEtablissementList = data;
      this._cdRef.markForCheck();
    });
    
    this._referenceDataService.fetchListDistricts(data => {
      this.districtList = data;
      this._cdRef.markForCheck();
    });
    
    this._metfpetService.getOptionBACListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.optionsBACList = data.results;
      this._cdRef.markForCheck();
    });

    this._metfpetService.getOptionTerminaleListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.optionsTerminaleList = data.results;
      this._cdRef.markForCheck();
    });

    this._activatedRoute.params.subscribe(params => {
      let lyceeId = params['id'];
      this.isCreation = !lyceeId;

      if (!this.isCreation) {
        this._metfpetService.getLycee(lyceeId).subscribe(data => {
          this._model = data;
          this.title = this._model.name;
          this.form.controls['name'].setValue(this._model.name);
          this.form.controls['prefectureId'].setValue(this._model.prefectureId);
          this.form.controls['district'].setValue(this._model.district);
          this.form.controls['zoneId'].setValue(this._model.zoneId);
          this.form.controls['statusEtablissementId'].setValue(this._model.statusEtablissementId);
          this.form.controls['optionBACIds'].setValue(data.options.map(x => x.id)); 
          this.form.controls['optionTerminaleIds'].setValue(data.optionsTerminale.map(x => x.id)); 
          this.form.controls['status'].setValue(this._model.status === <any>LyceeStatus.Active);
          this._cdRef.markForCheck();
        });
      }
    });
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveLycee$: Observable<MetfpetServiceAgent.LyceeDTO>;

      if (this.isCreation) {
        saveLycee$= this._metfpetService.createLycee(MetfpetServiceAgent.LyceeDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: LyceeStatus.Active },
            { options: this.form.controls['optionBACIds'].value.map(x => {return {id: x}})},
            { optionsTerminale: this.form.controls['optionTerminaleIds'].value.map(x => {return {id: x}})},
          )
        ));
      } else {
        saveLycee$ = this._metfpetService.updateLycee(MetfpetServiceAgent.LyceeDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: this.form.get('status').value ? LyceeStatus.Active : LyceeStatus.Inactive },
            { options: this.form.controls['optionBACIds'].value.map(x => {return {id: x}})},
            { optionsTerminale: this.form.controls['optionTerminaleIds'].value.map(x => {return {id: x}})}
          )
        ));
      }

      saveLycee$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([`${UiPath.admin.lycees.list}`]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  goBack() {
    this._location.back();
  }

}