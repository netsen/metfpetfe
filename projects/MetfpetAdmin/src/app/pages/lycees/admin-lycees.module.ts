import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { LyceesTabComponent } from './lycees-tab.component';
import { LyceesComponent } from './lycees.component';
import { LyceeComponent } from './lycee.component';
import { MattersComponent } from './matters.component';
import { MatterComponent } from './matter.component';
import { OptionsBACComponent } from './optionsBAC.component';
import { OptionBACComponent } from './optionBAC.component';
import { ExamCentersComponent } from './exam-centers.component';
import { ExamCenterComponent } from './exam-center.component';
import { CollegesComponent } from './colleges.component';
import { CollegeComponent } from './college.component';
import { OptionsBEPCComponent } from './optionsBEPC.component';
import { OptionBEPCComponent } from './optionBEPC.component';

export const routes = [
  { 
    path: '', 
    component: LyceesTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'list',
          pathMatch: 'full'
        },
        { 
          path: 'list', 
          component: LyceesComponent, 
          data: { breadcrumb: 'Lycées' }  
        },
        { 
          path: 'view/:id', 
          component: LyceeComponent, 
          data: { breadcrumb: 'Consultation' }  
        },
        { 
          path: 'new', 
          component: LyceeComponent, 
          data: { breadcrumb: 'Nouveau' }  
        },
        { 
          path: 'colleges', 
          component: CollegesComponent, 
          data: { breadcrumb: 'Collèges' }  
        },
        { 
          path: 'colleges/view/:id', 
          component: CollegeComponent, 
          data: { breadcrumb: 'Consultation' }  
        },
        { 
          path: 'colleges/new', 
          component: CollegeComponent, 
          data: { breadcrumb: 'Nouveau' }  
        },
        { 
          path: 'exam-centers', 
          component: ExamCentersComponent, 
          data: { breadcrumb: 'Centres d\'examen' }  
        },
        { 
          path: 'exam-centers/view/:id', 
          component: ExamCenterComponent, 
          data: { breadcrumb: 'Consultation' }  
        },
        { 
          path: 'exam-centers/new', 
          component: ExamCenterComponent, 
          data: { breadcrumb: 'Nouveau' }  
        },
        { 
          path: 'matters', 
          component: MattersComponent, 
          data: { breadcrumb: 'Matières' }  
        },
        { 
          path: 'matters/view/:id', 
          component: MatterComponent, 
          data: { breadcrumb: 'Consultation' }  
        },
        { 
          path: 'matters/new', 
          component: MatterComponent, 
          data: { breadcrumb: 'Nouveau' }  
        },
        { 
          path: 'optionsBAC', 
          component: OptionsBACComponent, 
          data: { breadcrumb: 'Options BAC' }  
        },
        { 
          path: 'optionsBAC/view/:id', 
          component: OptionBACComponent, 
          data: { breadcrumb: 'Consultation' }  
        },
        { 
          path: 'optionsBAC/new', 
          component: OptionBACComponent, 
          data: { breadcrumb: 'Nouveau' }  
        },
        { 
          path: 'optionsBEPC', 
          component: OptionsBEPCComponent, 
          data: { breadcrumb: 'Options BEPC' }  
        },
        { 
          path: 'optionsBEPC/view/:id', 
          component: OptionBEPCComponent, 
          data: { breadcrumb: 'Consultation' }  
        },
        { 
          path: 'optionsBEPC/new', 
          component: OptionBEPCComponent, 
          data: { breadcrumb: 'Nouveau' }  
        },
      ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    LyceesTabComponent,
    CollegesComponent,
    CollegeComponent,
    ExamCentersComponent,
    ExamCenterComponent,
    MattersComponent,
    MatterComponent,
    LyceesComponent,
    LyceeComponent,
    OptionsBACComponent,
    OptionBACComponent,
    OptionsBEPCComponent,
    OptionBEPCComponent,
  ]
})
export class AdminLyceesModule {}
