import { 
  ChangeDetectionStrategy,
  Component, 
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import { 
  AppSettings,
  Settings,
  MatiereBACStatus,
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './matter.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MatterComponent implements OnInit {

  form: FormGroup;
  settings: Settings;
  title: string;
  isCreation: boolean;
  private _model: MetfpetServiceAgent.MatiereBACDTO;
  
  constructor(public appSettings: AppSettings, 
    private _fb: FormBuilder, 
    private _location: Location,
    private _router: Router,
    private _route: ActivatedRoute, 
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings; 
    this.form = this._fb.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'description': null,
      'status': null,
    });
  }

  ngOnInit() {
    this._route.params.subscribe(params => {
      let id = params['id'];
      this.isCreation = !id;

      if(!this.isCreation) {
        this._metfpetService.getMatiereBAC(id).subscribe((data : any) => {
          this._model = data;
          this.title = this._model.name;
          this.form.controls['name'].setValue(this._model.name);
          this.form.controls['description'].setValue(this._model.description);
          this.form.controls['status'].setValue(this._model.status === <any>MatiereBACStatus.Active);
        });
      }
    });
  }

  public onSubmit():void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveMatiere$: Observable<MetfpetServiceAgent.MatiereBACDTO>;

      if (this.isCreation) {
        saveMatiere$= this._metfpetService.createMatiereBAC(MetfpetServiceAgent.MatiereBACDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: MatiereBACStatus.Active },
          )
        ));
      } else {
        saveMatiere$ = this._metfpetService.updateMatiereBAC(MetfpetServiceAgent.MatiereBACDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: this.form.get('status').value ? MatiereBACStatus.Active : MatiereBACStatus.Inactive },
          )
        ));
      }

      saveMatiere$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([UiPath.admin.lycees.matters.list]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  goBack() {
    this._location.back();
  }
}