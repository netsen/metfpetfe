import { 
  ChangeDetectionStrategy,
  Component, 
  ViewChild, 
  OnInit, 
  ViewEncapsulation 
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { 
  AppSettings,
  Settings,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  validateForm,
  showException,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './optionBEPC.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptionBEPCComponent implements OnInit {

  @ViewChild(DatatableComponent) table: DatatableComponent;
  form: FormGroup;
  settings: Settings;
  title: string;
  isCreation: boolean;
  private _model: MetfpetServiceAgent.OptionBEPCDTO;

  constructor(
    public appSettings: AppSettings, 
    private _activatedRoute: ActivatedRoute, 
    private _fb:FormBuilder,
    private _location: Location, 
    private _router: Router,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings; 
    this.form = this._fb.group({
      'code': [null, Validators.compose([Validators.required])],
      'name': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'description': [null],
    });
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      let id = params['id'];
      this.isCreation = !id;

      if (!this.isCreation) {
        this._metfpetService.getOptionBEPC(id).subscribe((data : any) => {
          this._model = data;
          this.title = this._model.name;
          this.form.patchValue({
            'code': this._model.code,
            'name': this._model.name,
            'description': this._model.description,
          });
        });
      }
    });
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveOptionBEPC$: Observable<MetfpetServiceAgent.OptionBEPCDTO>;

      if (this.isCreation) {
        saveOptionBEPC$= this._metfpetService.createOptionBEPC(MetfpetServiceAgent.OptionBEPCDTO.fromJS(
          Object.assign({}, this._model, this.form.value)
        ));
      } else {
        saveOptionBEPC$ = this._metfpetService.updateOptionBEPC(MetfpetServiceAgent.OptionBEPCDTO.fromJS(
          Object.assign({}, this._model, this.form.value)
        ));
      }

      saveOptionBEPC$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([UiPath.admin.lycees.optionsBEPC.list]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  goBack() {
    this._location.back();
  }

}