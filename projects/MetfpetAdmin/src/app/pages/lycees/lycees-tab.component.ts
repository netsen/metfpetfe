import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

export const Tabs = [
  { 
    routerLink: '/' + UiPath.admin.lycees.list, 
    label: 'Lycées' 
  },
  { 
    routerLink: '/' + UiPath.admin.lycees.colleges.list, 
    label: 'Collèges' 
  },
  {
    routerLink: '/' + UiPath.admin.lycees.examCenters.list, 
    label: 'Centre d\'examen' 
  },
  {
    routerLink: '/' + UiPath.admin.lycees.optionsBAC.list, 
    label: 'Options du BAC' 
  },
  {
    routerLink: '/' + UiPath.admin.lycees.matters.list, 
    label: 'Matières du BAC' 
  },
  {
    routerLink: '/' + UiPath.admin.lycees.optionsBEPC.list, 
    label: 'Options du BEPC' 
  },
];

@Component({
  templateUrl: './lycees-tab.component.html',
})
export class LyceesTabComponent {

  navs = Tabs;
  title: string;
  routerEventSubscription: Subscription;
  
  constructor(private router: Router) {
    this.title = 'Admissions - Lycées et écoles : lycées';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = 'Admissions - Lycées et écoles : ' + selectedTab.label.toLowerCase();
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
