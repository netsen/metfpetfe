import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  Component, 
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import { 
  AppSettings,
  Settings,
  ExamCenterStatus,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './exam-center.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExamCenterComponent implements OnInit {

  form: FormGroup;
  settings: Settings;
  title: string;
  isCreation: boolean;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  private _model: MetfpetServiceAgent.CentreExamenDTO;

  constructor(
    public appSettings: AppSettings, 
    private _cdRef: ChangeDetectorRef,
    private _fb: FormBuilder,
    private _location: Location, 
    private _router: Router,
    private _route: ActivatedRoute, 
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings; 
    this.form = this._fb.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'prefectureId': [null, Validators.required],
      'status': [null]
    });
  }

  ngOnInit() {
    this._metfpetService.getPrefectureList().subscribe(data => this.prefectureList = data);

    this._route.params.subscribe(params => {
      let id = params['id'];
      this.isCreation = !id;

      if (!this.isCreation) {
        this._metfpetService.getCentreExamen(id)
          .subscribe((data : any) => {
            this._model = data;
            this.title = this._model.name;
            this.form.controls['name'].setValue(this._model.name);
            this.form.controls['prefectureId'].setValue(this._model.prefectureId);
            this.form.controls['status'].setValue(this._model.status === <any>ExamCenterStatus.Active);
            this._cdRef.markForCheck();
          });
      }
    });
  }

  public onSubmit():void {
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveCentreExamen$: Observable<MetfpetServiceAgent.CentreExamenDTO>;

      if (this.isCreation) {
        saveCentreExamen$ = this._metfpetService.createCentreExamen(MetfpetServiceAgent.CentreExamenDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: ExamCenterStatus.Active }
          )
        ));
      } else {
        saveCentreExamen$ = this._metfpetService.updateCentreExamen(MetfpetServiceAgent.CentreExamenDTO.fromJS(
          Object.assign({}, 
            this._model, 
            this.form.value, 
            { status: this.form.get('status').value ? ExamCenterStatus.Active : ExamCenterStatus.Inactive }
          )
        ));
      }

      saveCentreExamen$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([UiPath.admin.lycees.examCenters.list]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  goBack() {
    this._location.back();
  }
}