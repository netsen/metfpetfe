import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { startWith } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent,
  MetfpetServiceAgent,
  PerfectScrollService,
  AuthService,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { Tabs } from './lycees-tab.component';

@Component({
  templateUrl: './matters.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MattersComponent extends BaseTableComponent {
  
  settings: Settings;
  title: string;
  navs = Tabs;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Matières du BAC';
    this.sort = {prop: 'name', dir: 'desc'};
  }

  ngAfterViewInit() {
    combineLatest([
      this.searchForm.get('name').valueChanges.pipe(startWith(this.searchForm.get('name').value)),
      this.searchForm.get('isActive').valueChanges.pipe(startWith(this.searchForm.get('isActive').value)),
    ])
    .subscribe(([name, isActive]) => {
      this.searchForm.patchValue({name, isActive}, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null,
      isActive: true,
    });
  }

  protected _search(criteria: any): Observable<any> {
    // to convert True/false value to number 1/0
    criteria.filters.isActive = +criteria.filters.isActive;
    return this._metfpetService.getMatiereBACListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }
  
  open(selectedRow) {
    this._router.navigate([`${UiPath.admin.lycees.matters.view}/${selectedRow.id}`]);
  }

  newMatter() {
    this._router.navigate([UiPath.admin.lycees.matters.add]);
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }
}
