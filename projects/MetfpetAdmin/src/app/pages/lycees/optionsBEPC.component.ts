import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ViewEncapsulation
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent,
  MetfpetServiceAgent,
  PerfectScrollService,
  AuthService,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { Tabs } from './lycees-tab.component';

@Component({
  templateUrl: './optionsBEPC.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptionsBEPCComponent extends BaseTableComponent {

  settings: Settings;
  title: string;
  navs = Tabs;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Options du PEBC';
    this.sort = {prop: 'name', dir: 'desc'};
  }

  ngAfterViewInit(){
    this.searchForm.get('name').valueChanges.pipe(startWith(this.searchForm.get('name').value))
      .subscribe((name) => {
        this.searchForm.patchValue({name}, {emitEvent: false});
        this.triggerSearch();
      });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null,
    });
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getOptionBEPCListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  open(selectedRow) {
    this._router.navigate([`${UiPath.admin.lycees.optionsBEPC.view}/${selectedRow.id}`]);
  }

  newOptionBEPC() {
    this._router.navigate([UiPath.admin.lycees.optionsBEPC.add]);
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }
}
