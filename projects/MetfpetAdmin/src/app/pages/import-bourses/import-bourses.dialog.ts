import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './import-bourses.dialog.html',
})
export class ImportBoursesDialog {

  form: FormGroup;
  fileMissing: boolean;

  constructor(
    public dialogRef: MatDialogRef<ImportBoursesDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public fb: FormBuilder, 
  ) {
    this.form = this.fb.group({
      'anneeAcademiqueId': [null, Validators.required],
      'trimestreId': [null, Validators.required],
    });
  }

  onFileChange(event) {
    this.data.file = event.target.files[0];
    this.fileMissing = false;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onConfirm(): void {
    if (this.data.isImport && !this.data.file) {
      this.fileMissing = true;
      return;
    }
    if (this.form.valid) {
      this.data.trimestreId = this.form.controls['trimestreId'].value;
      this.data.anneeAcademiqueId = this.form.controls['anneeAcademiqueId'].value;
      this.dialogRef.close(this.data);
    }
  }
}