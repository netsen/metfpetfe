import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Input,
  ViewEncapsulation 
} from '@angular/core';
import { UserIdleService } from 'angular-user-idle'; 
import { 
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  Settings,
  AppSettings,
  ExportService
} from 'MetfpetLib';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { Router } from '@angular/router';
import { ImportBoursesDialog } from './import-bourses.dialog';
import { ImportBoursesTabComponent } from './import-bourses-tab.component';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Component({
  templateUrl: './import-bourses.component.html',
  styleUrls: ['./import-bourses.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportBoursesComponent extends ImportBoursesTabComponent {
  
  imported: number;
  totalToimport: number;
  importingIndicator: boolean;
  showWarningMessage: boolean;
  public settings: Settings;
  trimestreList: Array<MetfpetServiceAgent.BourseTrimestreDTO>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;

  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    public appSettings: AppSettings,
    protected router: Router,
    private _exportService: ExportService
  ) {
    super(router);
    this.settings = this.appSettings.settings;
  }
  
  ngAfterViewInit() {   

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
    });

    this._metfpetService.getBourseTrimestreList().subscribe(data => {
      this.trimestreList = data.results;
    });
  }

  onClickImportButton() {
    this._dialogService.openDialog(
      ImportBoursesDialog,
      {
        width: '600px',
        data: {
          isImport: true,
          title: "Validation de bourses par import", 
          message: "Veuillez sélectionner la session et le trimestre puis indiquer l'emplacement du fichier", 
          confirmBtnText: "Valider",
          trimestreList: this.trimestreList,
          anneeAcademiqueList: this.anneeAcademiqueList
        }
      }
    ).afterClosed().subscribe(result => {
      if (result && result.file) {
        this.processFile(result.anneeAcademiqueId, result.trimestreId, result.file);
      }
    });
  }

  processFile(anneeAcademiqueId: string, trimestreId: string, file: Blob) {
    this.importingIndicator = true;
    this._userIdleService.stopWatching();

    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      var arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, {type:'binary',cellDates:true,dateNF:'dd/mm/yyyy'});
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      var rows = XLSX.utils.sheet_to_json(worksheet, {raw:false,dateNF:'dd/mm/yyyy'});
      
      this._validateBourses(anneeAcademiqueId, trimestreId, rows);
    }
    fileReader.readAsArrayBuffer(file);
  }

  private _validateBourses(anneeAcademiqueId: string, trimestreId: string, rows: any): void {
      if (rows.length == 0) {
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.INFO, 
            message: 'Aucune donnée importée'
          }
        });
        return;
      }

      this.validateBourses(this._convertToValidateTrimestrePaiementBourseRequests(anneeAcademiqueId, trimestreId, rows), true);
  }

  private _convertToValidateTrimestrePaiementBourseRequests(anneeAcademiqueId: string, trimestreId: string, rows: any): Array<MetfpetServiceAgent.ValidateTrimestrePaiementBourseRequest> {
    
    let redoublants =  new Array<MetfpetServiceAgent.ValidateTrimestrePaiementBourseRequest>();

    redoublants = rows.map(
      row => {
        return MetfpetServiceAgent.ValidateTrimestrePaiementBourseRequest.fromJS({
          'identifiantNationalEleve'   : row['INA'],
          'trimestreId'                : trimestreId,
          'anneeAcademiqueId'          : anneeAcademiqueId
        });
      }
    );
    return redoublants;
  }
  
  async validateBourses(rowsToImport: Array<MetfpetServiceAgent.ValidateTrimestrePaiementBourseRequest>, allowRetry: boolean) {
    this.showWarningMessage = true;
    this.imported = 0;
    this.totalToimport = rowsToImport.length;
    let toRetryList = new Array<MetfpetServiceAgent.ValidateTrimestrePaiementBourseRequest>();

    for (let row of rowsToImport) {    
      try {
        this.imported++;
        this._cd.markForCheck();
        let importSuccess = await this._metfpetService.validateTrimestrePaiementBourse(row).toPromise();
        if (!importSuccess) {
          toRetryList.push(row);
        }
      } catch(error) {
        row.errorMessage = error.isApiException ? (JSON.parse(error.response).Message ? JSON.parse(error.response).Message : error.response) : error;
        toRetryList.push(row);
      }
    }
    if (toRetryList.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: (this.imported - toRetryList.length) + " avec succès, " 
            + toRetryList.length + " en échec"
        }
      });

    } else {
      this._dialogService.openConfirmDialog({
        data: {
          title: "Attention", 
          message: (this.totalToimport - toRetryList.length) + " avec succès, " 
            + toRetryList.length + " en échec, reprendre les éléments en échec?"
        }
      })
      .afterClosed().subscribe(confirmed => {
        if (confirmed) {
          this.validateBourses(toRetryList, false);
        } else {
          this.exportFailed(toRetryList);
        }
      });
    }

    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    return {
      nbImported: this.imported,
      failedUsers: toRetryList
    };
  }

  exportFailed(toRetryList: Array<MetfpetServiceAgent.ValidateTrimestrePaiementBourseRequest>): void {
    if (toRetryList.length > 0) {
      let json : any[] = new Array();
      for (let failed of toRetryList) {
        json.push({
          'INA'                : failed.identifiantNationalEleve,
          'Raison'             : failed.errorMessage
        });
      }
      this.exportAsExcelFile(json, 'failed_results');
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'Échec': worksheet }, SheetNames: ['Échec'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_import_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

  downloadTemplate() {
    const headers = [
      { name: "INA", value: "identifiantNationalEleve" },        
    ]
    const templateName = "ImportBourseTemplate";
    this._exportService.downloadTemplate(templateName, headers);
  }
}
