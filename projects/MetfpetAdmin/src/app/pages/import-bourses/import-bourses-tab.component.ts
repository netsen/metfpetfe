import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './import-bourses-tab.component.html',
})
export class ImportBoursesTabComponent {

  navs = [
    {
      routerLink: '/' + UiPath.admin.import.importBourse, 
      label: 'Validation de bourses' 
    },
    {
      routerLink: '/' + UiPath.admin.import.deleteBourse, 
      label: 'Supprimer les bourses' 
    },
  ];

  public title: string;
  routerEventSubscription: Subscription;

  constructor(protected router: Router) {
    this.title = 'Import : Validation de bourses';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = 'Import : ' + selectedTab.label.toLowerCase();
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
