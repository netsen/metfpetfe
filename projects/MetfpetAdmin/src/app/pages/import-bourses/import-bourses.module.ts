import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ImportBoursesComponent } from './import-bourses.component';
import { ImportBoursesTabComponent } from './import-bourses-tab.component';
import { ImportBoursesDialog } from './import-bourses.dialog';
import { DeleteBoursesComponent } from './delete-bourses.component';
import { DeleteBoursesDialog } from './delete-bourses.dialog';

export const routes = [
  {
    path: '', 
    component: ImportBoursesTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'bourses',
          pathMatch: 'full'
        },
        { 
          path: 'bourses', 
          component: ImportBoursesComponent, 
          data: { breadcrumb: 'Validation de bourses' }
        },
        { 
          path: 'delete-bourses', 
          component: DeleteBoursesComponent, 
          data: { breadcrumb: 'Supprimer les bourses' }
        },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [
    SharedModule,
    ImportBoursesComponent,
  ],
  declarations: [
    ImportBoursesTabComponent,
    ImportBoursesComponent,
    ImportBoursesDialog,
    DeleteBoursesComponent,
    DeleteBoursesDialog
  ], 
  entryComponents: [
    ImportBoursesDialog,
    DeleteBoursesDialog
  ]
})
export class ImportBoursesModule {

}
