import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ImportInscriptionsDialog } from './import-inscriptions.dialog';
import { ImportInscriptionsTabComponent } from './import-inscriptions-tab.component';
import { ImportInscriptionsComponent } from './import-inscriptions.component';
import { ImportLastYearInscriptionsComponent } from './import-last-year-inscriptions.component';

export const routes = [
  {
    path: '', 
    component: ImportInscriptionsTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'inscriptions',
          pathMatch: 'full'
        },
        { 
          path: 'inscriptions', 
          component: ImportInscriptionsComponent, 
          data: { breadcrumb: 'Importer des inscriptions et réinscriptions' }
        },
        { 
          path: 'lastYearInscriptions', 
          component: ImportLastYearInscriptionsComponent, 
          data: { breadcrumb: 'Importer les inscriptions de l\'année dernière' }
        },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [
    SharedModule,
    ImportInscriptionsComponent,
  ],
  declarations: [
    ImportInscriptionsTabComponent,
    ImportInscriptionsComponent,
    ImportInscriptionsDialog,
    ImportLastYearInscriptionsComponent,
  ], 
  entryComponents: [
    ImportInscriptionsDialog
  ]
})
export class ImportInscriptionsModule {

}
