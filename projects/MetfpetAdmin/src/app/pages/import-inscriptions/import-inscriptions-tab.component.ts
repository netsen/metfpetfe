import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './import-inscriptions-tab.component.html',
})
export class ImportInscriptionsTabComponent {

  navs = [
    {
      routerLink: '/' + UiPath.admin.import.importInscription, 
      label: 'Importer des inscriptions et réinscriptions' 
    },
    {
      routerLink: '/' + UiPath.admin.import.importLastYearInscription, 
      label: 'Importer les inscriptions de l\'année dernière' 
    },
  ];

  public title: string;
  routerEventSubscription: Subscription;

  constructor(protected router: Router) {
    this.title = 'Import : importer des inscriptions et réinscriptions';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = 'Import : ' + selectedTab.label.toLowerCase();
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
