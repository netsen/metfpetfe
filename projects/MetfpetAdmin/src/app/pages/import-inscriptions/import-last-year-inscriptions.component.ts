import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Input,
  ViewEncapsulation 
} from '@angular/core';
import { UserIdleService } from 'angular-user-idle'; 
import { 
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  Settings,
  AppSettings,
  ExportService,
} from 'MetfpetLib';
import {formatDate} from '@angular/common';
import * as TSXLSX from 'ts-xlsx';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { ImportInscriptionsTabComponent } from './import-inscriptions-tab.component';
import { ImportInscriptionsDialog } from './import-inscriptions.dialog';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

export interface AdmissionImportResult {
  nbImported: number;
  failedAdmissions: Array<any>;
}

@Component({
  templateUrl: './import-last-year-inscriptions.component.html',
  styleUrls: ['./import-last-year-inscriptions.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportLastYearInscriptionsComponent extends ImportInscriptionsTabComponent {

  imported: number;
  totalToimport: number;
  importingIndicator: boolean;
  showWarningMessage: boolean;
  public settings: Settings;
  
  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    public appSettings: AppSettings,
    protected router: Router,
    private _exportService: ExportService
  ) {
    super(router);
    this.settings = this.appSettings.settings;
  }

  onClickImportButton() {
    this._dialogService.openDialog(
      ImportInscriptionsDialog,
      {
        width: '600px',
        data: {
          isImport: true,
          title: "Importer les inscriptions de l'année dernière", 
          message: "Veuillez indiquer l'emplacement du fichier", 
          confirmBtnText: "Importer",
        }
      }
    ).afterClosed().subscribe(result => {
      if (result && result.file) {
        this.processFile(result.file);
      }
    });
  }

  processFile(file: Blob) {
    this.importingIndicator = true;
    this._userIdleService.stopWatching();

    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      var arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, {type:'binary',cellDates:true,dateNF:'dd/mm/yyyy'});
      var first_sheet_name = workbook.SheetNames[0];
      var worksheet = workbook.Sheets[first_sheet_name];
      var rows = XLSX.utils.sheet_to_json(worksheet, {raw:false,dateNF:'dd/mm/yyyy'});
      
      this._importInscriptions(rows);
    }
    fileReader.readAsArrayBuffer(file);
  }

  private _importInscriptions(rows: any): void {
    if (rows.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: 'Aucune donnée importée'
        }
      });
      return;
    }

    this.importInscriptions(this._convertToInscriptions(rows), true);
  }

  private _convertToInscriptions(rows: any): Array<MetfpetServiceAgent.ImportLastYearInscriptionViewModel> {
    
    let inscriptions =  new Array<MetfpetServiceAgent.ImportLastYearInscriptionViewModel>();

    inscriptions = rows.map(
      row => {  
        return MetfpetServiceAgent.ImportLastYearInscriptionViewModel.fromJS({
          'identifiantNationalEtudiant'    : row['INA'],
          'institution'                    : row['Institution'], 
          'programme'                      : row['Programme'],  
          'niveauEtudeName'                : row['Niveau de l\'année précédente'],
        });
      }
    );
    return inscriptions;
  }
  
  async importInscriptions(inscriptionsToImport: Array<MetfpetServiceAgent.ImportLastYearInscriptionViewModel>, allowRetry: boolean) {
    this.showWarningMessage = true;
    this.imported = 0;
    this.totalToimport = inscriptionsToImport.length;
    let toRetryList = new Array<MetfpetServiceAgent.ImportLastYearInscriptionViewModel>();

    for (let inscription of inscriptionsToImport) {    
      try {
        this.imported++;
        this._cd.markForCheck();
        let importSuccess = await this._metfpetService.importLastYearInscription(inscription).toPromise();
        if (!importSuccess) {
          toRetryList.push(inscription);
        }
      } catch(error) {
        inscription.importErrorMessage = error.isApiException ? (JSON.parse(error.response).Message ? JSON.parse(error.response).Message : error.response) : error;
        toRetryList.push(inscription);
      }
    }
    if (toRetryList.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: (this.imported - toRetryList.length) + " inscription(e)(s) importé(e)(s) avec succès, " 
            + toRetryList.length + " en échec"
        }
      });

    } else {
      if (allowRetry) {
        this._dialogService.openConfirmDialog({
          data: {
            title: "Attention", 
            message: (this.totalToimport - toRetryList.length) + " inscription(e)s importé(e)s avec succès, " 
              + toRetryList.length + " en échec, reprendre les éléments en échec?"
          }
        })
        .afterClosed().subscribe(confirmed => {
          if (confirmed) {
            this.importInscriptions(toRetryList, false);
          } else {
            this.exportFailed(toRetryList);
          }
        });
      } else {
        this.exportFailed(toRetryList);
      }
    }

    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    return {
      nbImported: this.imported,
      failedStudents: toRetryList
    };
  }

  exportFailed(toRetryList: Array<MetfpetServiceAgent.ImportLastYearInscriptionViewModel>): void {
    if (toRetryList.length > 0) {
      let json : any[] = new Array();
      for (let failed of toRetryList) {
        json.push({
          INA                          : failed.identifiantNationalEtudiant,
          Institution                  : failed.institution, 
          Programme                    : failed.programme, 
          Niveau                       : failed.niveauEtudeName,
          Raison                       : failed.importErrorMessage
        });
      }
      this.exportAsExcelFile(json, 'failed_results');
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'Échec': worksheet }, SheetNames: ['Échec'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_import_' + new  Date().getTime() + EXCEL_EXTENSION);
  }

  downloadTemplate() {
    const headers = [
      { name: "INA", value: "ina" }, 
      { name: "Institution", value: "institution" },
      { name: "Programme", value: "programme" },
      { name: "Niveau de l'année précédente", value: "niveauEtudeName" },      
    ]
    const templateName = "LastYearInscriptionTemplate";
    this._exportService.downloadTemplate(templateName, headers);
  }
}
