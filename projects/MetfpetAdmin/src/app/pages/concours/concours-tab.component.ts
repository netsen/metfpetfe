import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

export const Tabs = [
  { 
    routerLink: '/' + UiPath.admin.concours.configurations.mention, 
    label: 'Seuil des mentions et de passage' 
  },
  {
    routerLink: '/' + UiPath.admin.concours.configurations.concoursAcces, 
    label: 'Configurer les concours d’accès' 
  },
  {
    routerLink: '/' + UiPath.admin.concours.configurations.exporter, 
    label: 'Exporter les templates de notes' 
  },
  {
    routerLink: '/' + UiPath.admin.concours.configurations.importerNote, 
    label: 'Importer les notes' 
  },
];

@Component({
  templateUrl: './concours-tab.component.html',
})
export class ConcoursTabComponent {

  navs = Tabs;
  title: string;
  routerEventSubscription: Subscription;
  
  constructor(private router: Router) {
    this.title = 'Configuration - Concours d’entrée';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = selectedTab.label;
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
