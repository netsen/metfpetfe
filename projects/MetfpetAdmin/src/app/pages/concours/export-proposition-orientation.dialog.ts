import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  ExportService,
} from 'MetfpetLib';
import { UserIdleService } from 'angular-user-idle';

@Component({
  templateUrl: './export-proposition-orientation.dialog.html',
})
export class ExportPropositionOrientationDialog {

  form: FormGroup;
  processing: boolean;
  processed: number;
  totalToprocess: number;
  showWarningMessage: boolean;
  proprammeList: Array<MetfpetServiceAgent.ProposerOffreProgramme>;
  message: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<ExportPropositionOrientationDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    protected _cdRef: ChangeDetectorRef,
    private _userIdleService: UserIdleService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.proprammeList = data.proprammeList;
    this.processing = false;
    this.form = this._formBuilder.group({
    });
  }

  onConfirm() {
    this._store.dispatch(showLoading());
    this._metfpetService.getConcoursEtudiantIdsForActualiser(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1,
      filters: {
        isCurrentSession: true,
        isOrienterResult: false,
        isActualiserOffres: true,
        exportProposition: true
      },
    }))
    .subscribe(
      (data) => {
        this.process(data);
      },
      error => this._store.dispatch(showException({error: error}))
    )
    .add(() => this._store.dispatch(hideLoading()));
  }

  async process(info: MetfpetServiceAgent.PublishConcoursResultInfo) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this._store.dispatch(showLoading());
    this.showWarningMessage = true;
    this.processed = 0;
    this.totalToprocess = info.concoursEtudiantIds.length;
    this.message = 'Export proposition d\'orientation';
    this._cdRef.markForCheck();

    let programmes = this.proprammeList;
    let exportedProposerOffreViewModel = new Array<MetfpetServiceAgent.ProposerOffreViewModel>();
    for (let item of info.concoursEtudiantIds) {

      this.processed++;
      this._cdRef.markForCheck(); 

      for (let id of item)
      {
        try {
          let result = await this._metfpetService.proposerOffre(MetfpetServiceAgent.ProposerOffreRequest.fromJS({
            concoursEtudiantId: id,
            programmes: programmes
          })).toPromise();
          if (result) {
            programmes = result.programmes;
            if (result.proposerOffres && result.proposerOffres.length > 0)
            {
              for (let proposerOffre of result.proposerOffres)
              {
                exportedProposerOffreViewModel.push(proposerOffre);
              }
            }
          }
        } catch(error) {
        }
      }
    }
    this._exportService.exportListeDesProposerOffres(exportedProposerOffreViewModel);

    this.processed = 0;
    this.totalToprocess = programmes.length;
    this.message = 'Export liste des institutions et programmes avec statut de quotas';
    this._cdRef.markForCheck();

    let exportedProgrammes = new Array<MetfpetServiceAgent.ProgrammeExportViewModel>();
    for (let programme of programmes)
    {
      try {
        this.processed++;
        this._cdRef.markForCheck();
        let vm = await this._metfpetService.getListProgrammeReport(programme.id).toPromise();
        let exportedVm = Object.assign({}, 
          vm, 
          { 
            limitePlace: programme.limitePlace,
            nombrePlacesOccupees: programme.nombrePlacesOccupees
          }
        );
        exportedProgrammes.push(exportedVm);
      } catch (error) { }
    }
    this._exportService.exportListeDesInstitutions(exportedProgrammes);

    this.processing = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    this._store.dispatch(hideLoading());
    this._dialogRef.close();
  }

  onClose() {
    this._dialogRef.close();
  }
}