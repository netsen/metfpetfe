import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent } from 'rxjs';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators/';
import {
  AppSettings,
  Settings,
  PerfectScrollService,
  MetfpetServiceAgent,
  DialogService,
  showLoading,
  showSuccess,
  showException,
  hideLoading,
  MessageType,
  ConcoursMessageType,
} from 'MetfpetLib';
import { MatDialog } from '@angular/material/dialog';
import { UserIdleService } from 'angular-user-idle';
import { GenererCandidatsDialog } from './generer-candidats.dialog';
import { ResetTermineEtudiantsDialog } from './reset-termine-etudiants.dialog';

@Component({
  templateUrl: './concours-operations.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConcoursOperationsComponent {

  settings: Settings;
  currentAnneeAcademique: MetfpetServiceAgent.AnneeAcademiqueDTO;
  processing: boolean;
  processed: number;
  totalToprocess: number;
  showWarningMessage: boolean;
  title: string;

  constructor(
    public appSettings: AppSettings,
    protected _cdRef: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialog: MatDialog,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService) 
  {
    this.settings = this.appSettings.settings;
    this.processing = false;
    this.title = 'Opération - Concours';
  }

  ngOnInit() {
    this.refreshAnneAcademique();
  }
  
  refreshAnneAcademique() {
    this._metfpetService.getAnneeAcademiqueAdmissionOuverte().subscribe((data : any) => {
      if (data) {
        this.currentAnneeAcademique = data;
        this._cdRef.markForCheck();
      }
    });
  }

  async process(admissionIds: Array<string>, anneeAcademiqueId: any) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this.showWarningMessage = true;
    this.processed = 0;
    this.totalToprocess = admissionIds.length;

    for (let admissionId of admissionIds) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.genererConcoursCandidat(admissionId).toPromise();
      } catch(error) {
      }
    }

    this._metfpetService.updateDateGenererConcoursCandidats(anneeAcademiqueId).subscribe((data : any) => {
      if (data) {
        this.currentAnneeAcademique = data;
        this._cdRef.markForCheck();
      }
    });

    this.processing = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
  }

  generateNewCandidates() {
    this._dialog.open(GenererCandidatsDialog, {
      width: '600px',
      data: {
        currentAnneeAcademique: this.currentAnneeAcademique
      }
    }).afterClosed().subscribe(data => {
      if (data) {
        this.process(data.admissionIds, data.anneeAcademiqueId);
      }
    });
  }

  archiveFailedStudents() {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Confirmation de archiv", 
        message: "Voulez-vous vraiment archiver le statut des apprenants qui n'ont pas terminé le concours ? Veuillez noter que cette action est irréversible.", 
        confirmBtnText: 'Archiver'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.archiveNotOffreAccepteeStudents(this.currentAnneeAcademique.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({}));
            },
            error => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  resetQuotas() {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Confirmation de remettre", 
        message: "Voulez-vous vraiment remettre les quotas à 0 ? Veuillez noter que cette action est irréversible.", 
        confirmBtnText: 'Remettre'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.resetZeroQuotaForAllPrograms()
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({}));
            },
            error => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  allowToPayAdmission() {
    this._store.dispatch(showLoading());
    this._metfpetService.allowToPayAdmission(this.currentAnneeAcademique.id)
      .subscribe(
        (data) => {
          if (data)
          {
            this.currentAnneeAcademique = data;
            this._cdRef.markForCheck();
            this._store.dispatch(showSuccess({}));
          } 
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
}

  blockToPayAdmission() {
    this._store.dispatch(showLoading());
    this._metfpetService.blockToPayAdmission(this.currentAnneeAcademique.id)
      .subscribe(
        (data) => {
          if (data)
          {
            this.currentAnneeAcademique = data;
            this._cdRef.markForCheck();
            this._store.dispatch(showSuccess({}));
          } 
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
  }

  allowToCreateInscriptionPublic() {
    this._store.dispatch(showLoading());
    this._metfpetService.allowToCreateInscriptionPublic(this.currentAnneeAcademique.id)
      .subscribe(
        (data) => {
          if (data)
          {
            this.currentAnneeAcademique = data;
            this._cdRef.markForCheck();
            this._store.dispatch(showSuccess({}));
          } 
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
  }

  blockToCreateInscriptionPublic() {
    this._store.dispatch(showLoading());
    this._metfpetService.blockToCreateInscriptionPublic(this.currentAnneeAcademique.id)
      .subscribe(
        (data) => {
          if (data)
          {
            this.currentAnneeAcademique = data;
            this._cdRef.markForCheck();
            this._store.dispatch(showSuccess({}));
          } 
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
  }

  allowToCreateInscriptionPrivate() {
    this._store.dispatch(showLoading());
    this._metfpetService.allowToCreateInscriptionPrivate(this.currentAnneeAcademique.id)
      .subscribe(
        (data) => {
          if (data)
          {
            this.currentAnneeAcademique = data;
            this._cdRef.markForCheck();
            this._store.dispatch(showSuccess({}));
          } 
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
  }

  blockToCreateInscriptionPrivate() {
    this._store.dispatch(showLoading());
    this._metfpetService.blockToCreateInscriptionPrivate(this.currentAnneeAcademique.id)
      .subscribe(
        (data) => {
          if (data)
          {
            this.currentAnneeAcademique = data;
            this._cdRef.markForCheck();
            this._store.dispatch(showSuccess({}));
          } 
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
  }

  generateInscriptions() {
    this._store.dispatch(showLoading());
    this._metfpetService.getListOfEtudiantIdsToGenerateInscription()
      .subscribe(
        (etudiantIds) => {
          if (etudiantIds)
          {
            this.autoGenerateInscriptions(etudiantIds);
          } 
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
  }

  async autoGenerateInscriptions(etudiantIds: Array<string>) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this.showWarningMessage = true;
    this.processed = 0;
    this.totalToprocess = etudiantIds.length;

    for (let etudiantId of etudiantIds) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.autoGenerateInscription(etudiantId).toPromise();
      } catch(error) {
      }
    }
    
    this.processing = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
  }

  resetTermineEtudiants() {
    this._dialog.open(ResetTermineEtudiantsDialog, {
      width: '600px',
      data: {
      }
    });
  }

  sendExamenCandidatureMessage() {
    this.sendConcoursMessage(ConcoursMessageType.ConcoursCandidature);
  }

  sendConcoursMessage(messageType: ConcoursMessageType) {
    this._metfpetService.getConcoursEtudiantIdsToSentMessage(MetfpetServiceAgent.ConcoursMessageRequest.fromJS({
      messageType: messageType,
      anneeAcademiqueId: this.currentAnneeAcademique.id
    })).subscribe(etudiantIds => {
      if (etudiantIds) {
        this.processSendExamenMessage(etudiantIds, messageType);
      }
    });
  }

  async processSendExamenMessage(etudiantIds: Array<string>, messageType: ConcoursMessageType) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this.showWarningMessage = true;
    this.processed = 0;
    this.totalToprocess = etudiantIds.length;

    for (let etudiantId of etudiantIds) {    
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.sendConcoursMessage(MetfpetServiceAgent.ConcoursMessageRequest.fromJS({
          etudiantId: etudiantId,
          messageType: messageType,
          anneeAcademiqueId: this.currentAnneeAcademique.id
        })).toPromise();
      } catch(error) {
      }
    }
  }
}