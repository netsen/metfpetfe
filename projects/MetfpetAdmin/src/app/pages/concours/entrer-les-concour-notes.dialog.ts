import { ChangeDetectorRef, Component, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Store } from "@ngrx/store";
import {
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';

const EMPHASIS_CURRENT_STEP_CSS = "text-bold";

const CREATE_BOURSES_PAYMENT_STEP_1 = "Identification";
const CREATE_BOURSES_PAYMENT_STEP_2 = "Entrée des notes";

@Component({
  templateUrl: "./entrer-les-concour-notes.dialog.html",
  styleUrls: ["./entrer-les-concour-notes.dialog.scss"],
})
export class EntrerLesConcourNotesDialog {
  currentStepName: string;
  curPVNumber: number;
  curIdentifiantNationalEleve: string;
  concoursEtudiants: Array<MetfpetServiceAgent.ConcoursEtudiantDTO>;
  title: string;
  form: FormGroup;
  showSaveAndEnterNewCaseBtn: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<EntrerLesConcourNotesDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _cd: ChangeDetectorRef,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.title = "Entrer les notes";
    this.initFirstStep();
    this.showSaveAndEnterNewCaseBtn = true;
    if (data.concoursEtudiantId) {
      this.showSaveAndEnterNewCaseBtn = false;
      this.form.patchValue({
        concoursEtudiantId: data.concoursEtudiantId
      });
      this.goToNextStep();
    }
  }

  initFirstStep() {
    this.currentStepName = CREATE_BOURSES_PAYMENT_STEP_1;
    this.curPVNumber = null;
    this. curIdentifiantNationalEleve = null;
    this.concoursEtudiants = [];
    this.form = this._formBuilder.group({
      numeroPV: null,
      identifiantNationalEleve: null,
      concoursEtudiantId: null,
      notes: this._formBuilder.array([]),
    });
  }

  get notes() {
    return this.form.controls['notes'] as FormArray;
  }

  buildNoteForm() {
    if (this.concoursEtudiants) {
      for (let concoursEtudiant of this.concoursEtudiants) {
        let childForm = this._formBuilder.group({
          id: concoursEtudiant.id,
          concoursName: concoursEtudiant.concoursName,
          concoursEtudiantNotes: this._formBuilder.array([]),
        });
        let concoursEtudiantNotes = childForm.controls['concoursEtudiantNotes'] as FormArray;
        for (let item of concoursEtudiant.concoursEtudiantNotes)
        {
          concoursEtudiantNotes.push(this._formBuilder.group({
            id: item.id,
            concoursEtudiantId: item.concoursEtudiantId,
            matiereName: item.matiereName,
            moyenne: item.moyenne
          }));
        }
        this.notes.push(childForm);
      }
    }
  }

  getCurrentStepCss(step: string) {
    if (this.currentStepName === step)
    {
      return EMPHASIS_CURRENT_STEP_CSS;
    }
    return "";
  }

  goToNextStep() {
    this._store.dispatch(showLoading());    
    this._metfpetService.identifyConcoursEtudiant(MetfpetServiceAgent.EnterNoteRequest.fromJS(this.form.getRawValue()))
      .subscribe(
        (data : MetfpetServiceAgent.ConcoursEtudiantResult) => {
          this.curPVNumber = data.numeroPV;
          this.curIdentifiantNationalEleve = data.identifiantNationalEleve;
          this.concoursEtudiants = data.concoursEtudiants;
          this.buildNoteForm();
          this.currentStepName = CREATE_BOURSES_PAYMENT_STEP_2;
          this._cd.markForCheck();
        },
        (error) => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
  }

  onSave(needToCloseDialog: boolean) {
    this._store.dispatch(showLoading());
    this.concoursEtudiants = this.notes.value.map(x => MetfpetServiceAgent.ConcoursEtudiantDTO.fromJS(x));
    let savedNotes: Array<MetfpetServiceAgent.ConcoursEtudiantNoteDTO> = [];
    for (let item of this.concoursEtudiants)
    {
      for (let note of item.concoursEtudiantNotes)
      {
        savedNotes.push(note);
      }
    }
    this._metfpetService.enterConcoursGrades(savedNotes)
    .subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            if (needToCloseDialog) {
              this._dialogRef.close(true);
            } else {
              this.initFirstStep();
            }
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
  }

  save() {
    this.onSave(true);
  }

  saveAndEnterNewCase() {
    this.onSave(false);
  }

  onClose() {
    this._dialogRef.close();
  }
}
