import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { Guid } from 'guid-typescript';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  TypeExamen,
} from 'MetfpetLib';

@Component({
  templateUrl: './concours.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConcoursDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.ConcoursDTO;
  niveauAccesList: Array<MetfpetServiceAgent.NiveauAcces>;
  typeProgrammeList: Array<MetfpetServiceAgent.TypeProgramme>;
  error: string;
  isCreation: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<ConcoursDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.form = this._formBuilder.group({
      name: [null, Validators.required],
      successMoyenneMin: [null, Validators.required],
      typeProgrammeId: [null, Validators.required],
      niveauAccesIds: null,
      concoursMatieres: this._formBuilder.array([]),
    });

    this.title = 'Configuration concours accès';
    this.niveauAccesList = data.niveauAccesList;
    this.typeProgrammeList = data.typeProgrammeList;
    if (data.concours)
    {
      this.model = data.concours;
      this.isCreation = false;
      this.form.patchValue({
        name: this.model.name,
        successMoyenneMin: this.model.successMoyenneMin,
        typeProgrammeId: this.model.typeProgrammeId,
        niveauAccesIds: this.model.concoursNiveauAcces.map(x => x.niveauAccesId)
      });
      if (this.model.concoursMatieres)
      {
        for (let concoursMatiere of this.model.concoursMatieres) {
          this.concoursMatieres.push(this._formBuilder.group({
            id: concoursMatiere.id,
            concoursId: concoursMatiere.concoursId,
            name: concoursMatiere.name,
            coefficient: concoursMatiere.coefficient
          }));
        }
      }
      this._cd.markForCheck();
    }
    else 
    {
      this.model = new MetfpetServiceAgent.ConcoursDTO();
      this.model.id = Guid.EMPTY;
      this.isCreation = true;
    }
  }

  get concoursMatieres() {
    return this.form.controls['concoursMatieres'] as FormArray;
  }

  addConcoursMatiere() {
    this.concoursMatieres.push(this._formBuilder.group({
      id: Guid.EMPTY,
      concoursId: this.model.id,
      name: null,
      coefficient: null
    }));
    this._cd.markForCheck();
  }

  delete(i: number) {
    this.concoursMatieres.removeAt(i);
    this._cd.markForCheck();
  }

  onConfirm() {
    this.error = null;
    validateForm(this.form);
    if (this.form.valid && this._validateConcoursMatieres()) {

      this._store.dispatch(showLoading());
      
      this.model.name = this.form.get('name').value;
      this.model.successMoyenneMin = this.form.get('successMoyenneMin').value;
      this.model.typeProgrammeId = this.form.get('typeProgrammeId').value;
      this.model.concoursNiveauAcces = this.form.controls['niveauAccesIds'].value.map(x => {return {niveauAccesId: x}});
      this.model.concoursMatieres = this.concoursMatieres.value
        .filter(x => !!x.name && !!x.coefficient)
        .map(x => MetfpetServiceAgent.ConcoursMatiereDTO.fromJS(x));

        if (this.isCreation)
        {
          this._metfpetService.createConcours(MetfpetServiceAgent.ConcoursDTO.fromJS(this.model))
            .subscribe(
              () => {
                this._store.dispatch(showSuccess({}));
                this._dialogRef.close(true);
              },
              error => this._store.dispatch(showException({error: error}))
            )
            .add(() => this._store.dispatch(hideLoading()));
        }
        else 
        {
          this._metfpetService.updateConcours(MetfpetServiceAgent.ConcoursDTO.fromJS(this.model))
            .subscribe(
              () => {
                this._store.dispatch(showSuccess({}));
                this._dialogRef.close(true);
              },
              error => this._store.dispatch(showException({error: error}))
            )
            .add(() => this._store.dispatch(hideLoading()));
        }
    }
  }

  private _validateConcoursMatieres() {
    var choices1 = this.concoursMatieres.value.filter(x => !x.name || !x.coefficient);
    if (choices1.length > 0) {
      this.error = 'Certains champs obligatoires manquent';
    }

    var choices2 = this.concoursMatieres.value.filter(x => !!x.name).map(x => x.name);
    if (new Set(choices2).size !== choices2.length) {
      this.error = 'Les matières sont dupliquées';
    }

    return !this.error;
  }

  onClose() {
    this._dialogRef.close();
  }
}