import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';
import { startWith } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent,
  MetfpetServiceAgent,
  PerfectScrollService,
  DialogService,
  showLoading,
  hideLoading,
  ExportService,
  ConcoursStatus
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { Tabs } from './concours-tab.component';

@Component({
  templateUrl: './concours-exporte-les-templates-de-notes.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConcoursExporteLesTemplatesDeNotesComponent extends BaseTableComponent {

  settings: Settings;
  title: string;
  navs = Tabs;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Configuration - Exporter les templates de notes';
    this.sort = {prop: 'name', dir: 'desc'};
  }

  ngAfterViewInit() {
    combineLatest([
      this.searchForm.get('name').valueChanges.pipe(startWith(this.searchForm.get('name').value)),
    ])
    .subscribe(([name]) => {
      this.searchForm.patchValue({name}, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null,
      status: ConcoursStatus.Active,
    });
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getConcoursListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  public exportTemplate(row: MetfpetServiceAgent.ConcoursDTO) {
    this._store.dispatch(showLoading());
    this._metfpetService.getExportConcoursMatieres(row.id)
      .subscribe(data => this._exportService.exportConcoursLesTemplatesDeNotes(data))
      .add(() => this._store.dispatch(hideLoading()));
  }

  public async exportAllTemplates() {
    this._store.dispatch(showLoading());
    let data = await this._metfpetService.getAllExportConcoursMatieres(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1,
        filters: this.searchForm.value,
      })).toPromise();
    await this._exportService.exportAllConcoursLesTemplatesDeNotes(data);
    this._store.dispatch(hideLoading());
  }
}