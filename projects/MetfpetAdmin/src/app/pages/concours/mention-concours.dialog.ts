import { Component, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Store } from "@ngrx/store";
import { validateForm, MetfpetServiceAgent, showLoading } from "MetfpetLib";


@Component({
  templateUrl: "./mention-concours.dialog.html",
})
export class MentionConcoursDialog {

  mentionConcoursId: string;
  isEditMode: boolean = false;
  title: string;
  form: FormGroup;
  hasChanges: boolean = false;
  model: MetfpetServiceAgent.MentionConcoursDTO;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<MentionConcoursDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.title = data.id ? "Modifier une mention" : "Ajouter une mention";
    this.form = this._formBuilder.group({
      name: ["", Validators.required],
      moyenneMin: ["", Validators.required],
    });

    this.parseInputData(data);
  }

  private parseInputData(data: any) {
    if (data.id) {
      this.isEditMode = true;
      this.mentionConcoursId = data.id;

      this._metfpetService.getMentionConcour(this.mentionConcoursId)
        .subscribe(res => {
          this.model = res;
          this.form.patchValue({
            name: this.model.name,
            moyenneMin: this.model.moyenneMin,
          });
        });
    }
  }

  onConfirm() {
    validateForm(this.form);
    if (!this.form.valid) return;

    this.model = Object.assign({}, this.model, this.form.value);

    if (this.isEditMode) {
      this._metfpetService.updateMentionConcours(this.model)
      .subscribe(res => {
        this.hasChanges = true;
        this._dialogRef.close(this.hasChanges);
      });
    }
    else {
      this._metfpetService.createMentionConcours(this.model)
        .subscribe(res => {
          this.hasChanges = true;
          this._dialogRef.close(this.hasChanges);
        });
    }
  }

  onClose() {
    this._dialogRef.close(this.hasChanges);
  }
}
