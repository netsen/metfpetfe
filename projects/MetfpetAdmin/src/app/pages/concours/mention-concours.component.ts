import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  PerfectScrollService,
  MetfpetServiceAgent,
} from 'MetfpetLib';
import { MatDialog } from '@angular/material/dialog';
import { MentionConcoursDialog } from './mention-concours.dialog';
import { Tabs } from './concours-tab.component';

@Component({
  templateUrl: './mention-concours.component.html',
  styleUrls: ['./mention-concours.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MentionConcoursComponent extends BaseTableComponent {

  settings: Settings;
  title: string;
  navs = Tabs;
 
  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialog: MatDialog,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Configuration - Seuil des mentions et de passage';
    this.sort = {prop: 'name', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();
    this.triggerSearch();
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      status: null,
      name: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  createMention(){
    this._dialog.open(MentionConcoursDialog , {
      width: '800px',
      data: {
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getMentionConcours();
  }
  
  open(row: MetfpetServiceAgent.MentionConcoursRowModel) {
    this._dialog.open(MentionConcoursDialog , {
      width: '800px',
      data: {
        id: row.id
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }
}