import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent } from 'rxjs';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators/';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  PerfectScrollService,
  MetfpetServiceAgent,
  FinalExamenRapportType,
  ConcoursRapportTypeValues,
  AuthService,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { MatDialog } from '@angular/material/dialog';
import { Tabs } from './concours-etudiant-tab.component';
import { GenerationDeConcoursRapportDialog } from './generation-de-concours-rapport.dialog';
import { TelechargerConcoursRapportDialog } from './telecharger-concours-rapport.dialog';


@Component({
  templateUrl: './concours-rapports.component.html',
  styleUrls: ['./concours-rapports.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConcoursRapportsComponent extends BaseTableComponent {

  settings: Settings;
  title: string;
  navs = Tabs;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  reportTypeList: Array<any>;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    private _dialog: MatDialog,
    private _authService: AuthService,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Gestion des concours - Rapports';
    this.reportTypeList = ConcoursRapportTypeValues;
    this.sort = {prop: 'typeName', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    combineLatest([
      this.searchForm.get('anneeAcademique').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademique').value)
        ),
      this.searchForm.get('type').valueChanges
        .pipe(
          startWith(this.searchForm.get('type').value)
        ),
    ])
    .subscribe((
      [
        anneeAcademique,
        type,
      ]) => {
      this.searchForm.patchValue({
        anneeAcademique,
        type,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      anneeAcademique: null,
      type: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getConcoursRapportListPage(criteria);
  }
  
  open(row: MetfpetServiceAgent.ConcoursRapportViewModel) {
    this._dialog.open(TelechargerConcoursRapportDialog, {
      width: '650px',
      data: {
        rapport: row
      }
    });
  }

  generateReport() {
    this._dialog.open(GenerationDeConcoursRapportDialog, {
      width: '650px',
      data: {
      }
    }).afterClosed().subscribe(data => {
      this.triggerSearch();
    });
  }

  public isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }
}