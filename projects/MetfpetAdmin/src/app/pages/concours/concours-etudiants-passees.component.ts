import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent } from 'rxjs';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators/';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  PerfectScrollService,
  MetfpetServiceAgent,
  BiometricStatusValues,
  ConcoursStatusResultValues,
  ConcoursStatusGlobalValues,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { Tabs } from './concours-etudiant-tab.component';
import { MatDialog } from '@angular/material/dialog';
import { EntrerLesConcourNotesDialog } from './entrer-les-concour-notes.dialog';

@Component({
  templateUrl: './concours-etudiants-passees.component.html',
  styleUrls: ['./concours-etudiants-passees.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConcoursEtudiantsPasseesComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;
  @ViewChild('searchPV', {static: true}) searchPV: ElementRef;
  @ViewChild('searchMoyenneTotalMin', {static: true}) searchMoyenneTotalMin: ElementRef;
  @ViewChild('searchMoyenneTotalMax', {static: true}) searchMoyenneTotalMax: ElementRef;

  settings: Settings;
  title: string;
  navs = Tabs;
  niveauAccesList: Array<MetfpetServiceAgent.NiveauAccesViewModel>;
  concoursList: Array<MetfpetServiceAgent.ConcoursDTO>;
  centreList: Array<MetfpetServiceAgent.CentreConcoursRowViewModel>;
  statusResultList: Array<any>;
  statusGlobalList: Array<any>;
  biometricList: Array<any>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    private _dialog: MatDialog,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Gestion des concours - Session passées';
    this.statusResultList = ConcoursStatusResultValues;
    this.statusGlobalList = ConcoursStatusGlobalValues;
    this.biometricList = BiometricStatusValues;
    this.sort = {prop: 'identifiantNationalEleve', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getNiveauAccesListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauAccesList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getCentreConcoursListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.centreList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAllConcours(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.concoursList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    combineLatest([
      this.searchForm.get('statusResult').valueChanges
        .pipe(
          startWith(this.searchForm.get('statusResult').value)
        ),
      this.searchForm.get('statusGlobal').valueChanges
        .pipe(
          startWith(this.searchForm.get('statusGlobal').value)
        ),
      this.searchForm.get('concours').valueChanges
        .pipe(
          startWith(this.searchForm.get('concours').value)
        ),
      this.searchForm.get('anneeAcademique').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademique').value)
        ),
      this.searchForm.get('niveauAcces').valueChanges
        .pipe(
          startWith(this.searchForm.get('niveauAcces').value)
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      fromEvent(
        this.searchPV.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('numeroPV').value}
            })
          ),
      fromEvent(
        this.searchMoyenneTotalMin.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('moyenneTotalMin').value}
            })
          ),
      fromEvent(
        this.searchMoyenneTotalMax.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('moyenneTotalMax').value}
            })
          ),
      this.searchForm.get('centreConcours').valueChanges
        .pipe(
          startWith(this.searchForm.get('centreConcours').value)
        ),
      this.searchForm.get('biometricStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('biometricStatus').value)
        ),
    ])
    .subscribe((
      [
        statusResult,
        statusGlobal,
        concours,
        anneeAcademique,
        niveauAcces,
        eventSearchNationalStudentIdentifier,
        eventSearchName, 
        eventFirstname,
        eventSearchPV,
        eventSearchMoyenneTotalMin,
        eventSearchMoyenneTotalMax,
        centreConcours,
        biometricStatus,
      ]) => {
      this.searchForm.patchValue({
        statusResult,
        statusGlobal,
        concours,
        anneeAcademique,
        niveauAcces,
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null,
        numeroPV: eventSearchPV ? eventSearchPV['target'].value : null, 
        moyenneTotalMin: eventSearchMoyenneTotalMin ? eventSearchMoyenneTotalMin['target'].value : null, 
        moyenneTotalMax: eventSearchMoyenneTotalMax ? eventSearchMoyenneTotalMax['target'].value : null, 
        centreConcours,
        biometricStatus,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      identifiantNationalEleve: null,
      concours: null,
      anneeAcademique: null,
      niveauAcces: null,
      name: null,
      firstName: null,
      numeroPV: null,
      biometricStatus: null,
      centreConcours: null,
      moyenneTotalMin: null,
      moyenneTotalMax: null,
      statusResult: null,
      statusGlobal: null,
      isCurrentSession: false
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getConcoursEtudiantListPage(criteria);
  }
  
  open(row: MetfpetServiceAgent.ConcoursEtudiantViewModel) {
    this._router.navigate([`${UiPath.admin.students.dossier}/${row.etudiantId}`], { state: { previousPage: 'Cursus' } });
  }
}