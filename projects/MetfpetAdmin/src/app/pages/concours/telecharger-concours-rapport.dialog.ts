import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import * as FileSaver from 'file-saver';
import { Guid } from 'guid-typescript';

@Component({
  templateUrl: './telecharger-concours-rapport.dialog.html',
})
export class TelechargerConcoursRapportDialog {

  form: FormGroup;
  concoursList: Array<any>;
  rapport: MetfpetServiceAgent.ConcoursRapportViewModel;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<TelechargerConcoursRapportDialog>,
    protected _cdRef: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.rapport = data.rapport;
    this.form = this._formBuilder.group({
      concoursRapportId: this.rapport.id,
      anneeAcademiqueId: this.rapport.anneeAcademiqueId,
      rapportType: this.rapport.type,
      concoursId:  [null, Validators.required],
      concoursName:  [null],
    });
  }

  ngOnInit() {
    this._metfpetService.getConcoursForReport(this.rapport.anneeAcademiqueId).subscribe(data => {
      this.concoursList = data;
      this.concoursList.push(MetfpetServiceAgent.ConcoursDTO.fromJS({
        id: Guid.EMPTY,
        name: 'Tous'
      }));
      this._cdRef.markForCheck();
    });
  }
  
  getZipFileName() {
    var fileName = this.rapport.typeName + '_' + this.rapport.anneeAcademiqueName + '_' + this.form.get('concoursName').value + '_' + new  Date().getTime() + '.zip';
    return fileName;
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      let selectedConcoursId = this.form.get('concoursId').value;
      let concoursName = this.concoursList.find(x => x.id == selectedConcoursId).name;
      this.form.controls['concoursName'].setValue(concoursName);
      this._metfpetService.downloadConcoursRapport(MetfpetServiceAgent.DownloadConcoursRapportRequest.fromJS(Object.assign(this.form.value)))
        .subscribe(
          (file) => {
            const blob = new Blob([file.data], {
              type: 'application/zip'
            });
            FileSaver.saveAs(blob, this.getZipFileName());
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}