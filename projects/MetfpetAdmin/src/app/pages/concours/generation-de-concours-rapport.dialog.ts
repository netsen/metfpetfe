import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { id } from '@swimlane/ngx-datatable';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  ExportService,
  ConcoursRapportTypeValues,
  ConcoursRapportType,
} from 'MetfpetLib';
import { UserIdleService } from 'angular-user-idle';

@Component({
  templateUrl: './generation-de-concours-rapport.dialog.html',
})
export class GenerationDeConcoursRapportDialog {

  form: FormGroup;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  reportTypeList: Array<any>;
  processing: boolean;
  processed: number;
  totalToprocess: number;
  showWarningMessage: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<GenerationDeConcoursRapportDialog>,
    protected _cdRef: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _userIdleService: UserIdleService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.processing = false;
    this.reportTypeList = ConcoursRapportTypeValues;
    this.form = this._formBuilder.group({
      anneeAcademiqueId: [null, Validators.required],
      reportType:  [null, Validators.required],
    });
  }

  ngOnInit() {
    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cdRef.markForCheck();
    });
  }

  async generateListeDesCandidats(ids: Array<string>) {
    let exportedList = await this.loadConcoursEtudiants(ids);
    this._exportService.exportListeDesConcoursCandidats(exportedList);
    this.postProcessing();
  }

  private async loadConcoursEtudiants(ids: string[]) {
    let exportedList = new Array<MetfpetServiceAgent.ConcoursEtudiantViewModel>();
    for (let id of ids) {
      try {
        this.processed++;
        this._cdRef.markForCheck();
        let vm = await this._metfpetService.getConcoursEtudiant(id).toPromise();
        exportedList.push(vm);
      } catch (error) {
      }
    }
    return exportedList;
  }

  async generateListeDesEnFilAttente(ids: Array<string>) {
    let exportedList = await this.loadConcoursEtudiants(ids);
    this._exportService.exportListeDesEnFilAttente(exportedList);
    this.postProcessing();
  }

  private postProcessing() {
    this.processing = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    this._store.dispatch(hideLoading());
  }

  async generateOffreAccepteeParConcours(ids: Array<string>) {
    for (let id of ids) {
      try {
        this.processed++;
        this._cdRef.markForCheck();
        await this._metfpetService.generateOffreAccepteeParConcoursRapport(MetfpetServiceAgent.GenerateOffreAccepteeParConcoursRapportRequest.fromJS({
          concoursId: id,
          anneeAcademiqueId: this.form.get('anneeAcademiqueId').value,
        })).toPromise();
      } catch(error) {
      }
    }
    this.postProcessing();
  }

  async generateNotesMoyenne(ids: Array<string>) {
    let exportedList = await this.loadConcoursEtudiants(ids);
    this._exportService.exportListeNotesMoyenne(exportedList);
    this.postProcessing();
  }

  private async loadProgramme(ids: string[]) {
    let exportedList = new Array<MetfpetServiceAgent.ProgrammeExportViewModel>();
    for (let id of ids) {
      try {
        this.processed++;
        this._cdRef.markForCheck();
        let vm = await this._metfpetService.getListProgrammeReport(id).toPromise();
        exportedList.push(vm);
      } catch (error) { }
    }
    return exportedList;
  }

  async generateListeDesInstitutions(ids: Array<string>) {
    let exportedList = await this.loadProgramme(ids);
    this._exportService.exportListeDesInstitutions(exportedList);
    this.postProcessing();
  }

  async generateListeDesAdmissions(ids: Array<string>) {
    let exportedAdmissionList = new Array<MetfpetServiceAgent.AdmissionInstitutionRowViewModel>();
    for (let id of ids) {
      try {
        this.processed++;
        this._cdRef.markForCheck();
        let admissions = await this._metfpetService.loadAdmissionInstitutionsForEtudiant(MetfpetServiceAgent.ExportAdmissionInstitutionRequest.fromJS({
          etudiantId: id,
          anneeAcademiqueId: this.form.get('anneeAcademiqueId').value,
          byChoix: false
        })).toPromise();
        if (admissions)
        {
          for (let admission of admissions) 
          {
            exportedAdmissionList.push(admission);
          }
        }
      } catch (error) { }
    }
    this._exportService.exportAdmissions(exportedAdmissionList, {
        exportExcel: true,
        exportPrenom: true,
        exportNom: true,
        exportDateNaissance: true,
        exportLieuNaissance: true,
        exportSexe: true,
        exportINA: true,
        exportEtablissement: true, 
        exportChoix: true,
        exportAnnee: true,
        exportPv: true,
        exportStatut: true,
        exportDecision: true,
        exportProgramme: false,
        exportFaculte: false, 
        exportInstitution: true,
        exportPvEtudiant: true,
        exportCentreConcours: true,
        exportPrefectureDeResidence: true,
        exportNomPere: true,
        exportNomMere: true,
        exportEtudiantLevel: true,
        exportTypeAdmissionName: true,
        exportNiveauAccesName: true
      }, "ListeDesAdmissions");
    this.postProcessing();
  }

  async generateRapportCompletSurLesConcours(ids: Array<string>) {
    let rapportCompletSurLesConcoursViewModelList = new Array<MetfpetServiceAgent.RapportCompletSurLesConcoursViewModel>();
    for (let id of ids) {
      try {
        this.processed++;
        this._cdRef.markForCheck();
        let items = await this._metfpetService.getRapportCompletSurLesConcours(MetfpetServiceAgent.RapportCompletSurLesConcoursRequest.fromJS({
          etudiantId: id,
          anneeAcademiqueId: this.form.get('anneeAcademiqueId').value
        })).toPromise();
        if (items)
        {
          for (let item of items) 
          {
            rapportCompletSurLesConcoursViewModelList.push(item);
          }
        }
      } catch (error) { }
    }
    this._exportService.exportRapportCompletSurLesConcours(rapportCompletSurLesConcoursViewModelList);
    this.postProcessing();
  }

  generate(ids: Array<string>) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this._store.dispatch(showLoading());
    this.showWarningMessage = true;
    this.processed = 0;
    this.totalToprocess = ids.length;
    this._cdRef.markForCheck();

    switch(this.form.get('reportType').value) {
      case <any>ConcoursRapportType.ListeDesCandidats:
        this.generateListeDesCandidats(ids);
        break;
      case <any>ConcoursRapportType.ListeDesEnFilAttente:
        this.generateListeDesEnFilAttente(ids);
        break;
      case <any>ConcoursRapportType.OffreAccepteeParConcours:
        this.generateOffreAccepteeParConcours(ids);
        break;
      case <any>ConcoursRapportType.NotesMoyenne:
        this.generateNotesMoyenne(ids);
        break;
      case <any>ConcoursRapportType.ListeDesInstitutions:
        this.generateListeDesInstitutions(ids);
        break;
      case <any>ConcoursRapportType.ListeDesAdmissions:
        this.generateListeDesAdmissions(ids);
        break;
      case <any>ConcoursRapportType.RapportCompletSurLesConcours:
        this.generateRapportCompletSurLesConcours(ids);
        break;
    }
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.loadDataForConcoursReportGeneration(MetfpetServiceAgent.ConcoursReportGenerationRequest.fromJS(Object.assign(this.form.value)))
        .subscribe(
          (ids) => {
            this.generate(ids);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}