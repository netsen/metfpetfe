import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  Input,
  ViewEncapsulation 
} from '@angular/core';
import { UserIdleService } from 'angular-user-idle'; 
import { 
  DialogService, 
  SeverityEnum,
  MetfpetServiceAgent,
  Settings,
  AppSettings,
} from 'MetfpetLib';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { Router } from '@angular/router';
import { ConcoursImportLesNotesDialog } from './concours-import-les-notes.dialog';
import { Tabs } from './concours-tab.component';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

export interface ImportLesNotesResult {
  nbImported: number;
  failedAdmissions: Array<any>;
}

@Component({
  templateUrl: './concours-import-les-notes.component.html',
  styleUrls: ['./concours-import-les-notes.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConcoursImportLesNotesComponent {

  title: string;
  navs = Tabs;
  imported: number;
  totalToimport: number;
  importingIndicator: boolean;
  showWarningMessage: boolean;
  public settings: Settings;
  importedCases: Array<MetfpetServiceAgent.ImportConcoursEtudiantNoteViewModel>;
  
  constructor(
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
    private _userIdleService: UserIdleService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    public appSettings: AppSettings,
    protected router: Router
  ) {
    this.settings = this.appSettings.settings;
    this.title = 'Configuration - Importer les notes';
  }

  onClickImportButton() {
    this._dialogService.openDialog(
      ConcoursImportLesNotesDialog,
      {
        width: '600px',
        data: {
          isImport: true,
          title: "Importer les notes", 
          message: "Veuillez indiquer l'emplacement du fichier", 
          confirmBtnText: "Importer",
        }
      }
    ).afterClosed().subscribe(result => {
      if (result && result.file) {
        this.processFile(result.file);
      }
    });
  }

  processFile(file: Blob) {
    this.importingIndicator = true;
    this._userIdleService.stopWatching();

    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      var arrayBuffer: any = fileReader.result;
      var data = new Uint8Array(arrayBuffer);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");
      var workbook = XLSX.read(bstr, {type:'binary',cellDates:true,dateNF:'dd/mm/yyyy'});
      this.importedCases = new Array<MetfpetServiceAgent.ImportConcoursEtudiantNoteViewModel>();
      for (let first_sheet_name of workbook.SheetNames) {
        var worksheet = workbook.Sheets[first_sheet_name];
        var rows = XLSX.utils.sheet_to_json(worksheet, {raw:false,dateNF:'dd/mm/yyyy'});
        if (rows.length > 0 && rows[0]) {
          this._convertToNotes(rows);
        }
      }
      this._importNotes();
    }
    fileReader.readAsArrayBuffer(file);
  }

  private _importNotes(): void {
    if (this.importedCases.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: 'Aucune donnée importée'
        }
      });
      return;
    }

    this.importNotes();
  }

  private _convertToNotes(rows: any) {
    var keys = Object.keys(rows[0]);
    for (var row of rows) {
      var note = new MetfpetServiceAgent.ImportConcoursEtudiantNoteViewModel();
      note.numeroPV = row['PV'];
      note.identifiantNationalEleve = row['INA'];
      note.concoursName = row['Nom du concours'];
      var noteInfos = new Array<MetfpetServiceAgent.ConcoursEtudiantNoteInfo>();
      for (var key of keys) {
        if (key !== 'PV' && key !== 'INA' && key !== 'Nom du concours') {
          noteInfos.push(MetfpetServiceAgent.ConcoursEtudiantNoteInfo.fromJS({
            matiereName: key,
            moyenne: row[key]
          })); 
        }
      }
      note.notes = noteInfos;
      this.importedCases.push(note);
    }
  }

  async importNotes() {
    this.showWarningMessage = true;
    this.imported = 0;
    this.totalToimport = this.importedCases.length;
    let toRetryList = new Array<MetfpetServiceAgent.ImportConcoursEtudiantNoteViewModel>();

    for (let note of this.importedCases) {
      try {
        this.imported++;
        this._cd.markForCheck();
        let importSuccess = await this._metfpetService.importConcoursEtudiantNote(note).toPromise();
        if (!importSuccess) {
          toRetryList.push(note);
        }
      } catch(error) {
        note.importErrorMessage = error.isApiException ? (JSON.parse(error.response).Message ? JSON.parse(error.response).Message : error.response) : error;
        toRetryList.push(note);
      }
    }

    if (toRetryList.length == 0) {
      this._dialogService.openInfoDialog({
        data: {
          severity: SeverityEnum.INFO, 
          message: (this.imported - toRetryList.length) + " note(s) importé(e)(s) avec succès, " 
            + toRetryList.length + " en échec"
        }
      });

    } else {
      this.exportFailed(toRetryList);
    }

    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    return {
      nbImported: this.imported,
      failedNotes: toRetryList
    };
  }

  exportFailed(toRetryList: Array<MetfpetServiceAgent.ImportConcoursEtudiantNoteViewModel>): void {
    if (toRetryList.length > 0) {
      let json : any[] = new Array();
      for (let failed of toRetryList) {
        json.push({
          PV                  : failed.numeroPV, 
          Raison              : failed.importErrorMessage
        });
      }
      this.exportAsExcelFile(json, 'failed_results');
    }
  }

  exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'Échec': worksheet }, SheetNames: ['Échec'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + '_import_' + new  Date().getTime() + EXCEL_EXTENSION);
  }
}
