import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent } from 'rxjs';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators/';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  PerfectScrollService,
  MetfpetServiceAgent,
  BiometricStatusValues,
  DialogService,
  showLoading,
  showException,
  hideLoading,
} from 'MetfpetLib';
import { MatDialog } from '@angular/material/dialog';
import { Tabs } from './publier-les-resultats-tab.component';
import { UserIdleService } from 'angular-user-idle';

@Component({
  templateUrl: './publier-les-resultats.component.html',
  styleUrls: ['./publier-les-resultats.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PublierLesResultatsComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;
  @ViewChild('searchPV', {static: true}) searchPV: ElementRef;
  @ViewChild('searchMoyenneTotalMin', {static: true}) searchMoyenneTotalMin: ElementRef;
  @ViewChild('searchMoyenneTotalMax', {static: true}) searchMoyenneTotalMax: ElementRef;

  settings: Settings;
  title: string;
  navs = Tabs;
  niveauAccesList: Array<MetfpetServiceAgent.NiveauAccesViewModel>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  concoursList: Array<MetfpetServiceAgent.ConcoursDTO>;
  centreList: Array<MetfpetServiceAgent.CentreConcoursRowViewModel>;
  biometricList: Array<any>;
  processing: boolean;
  processed: number;
  totalToprocess: number;
  showWarningMessage: boolean;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    private _dialog: MatDialog,
    private _location: Location,
    private _dialogService: DialogService,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _userIdleService: UserIdleService,
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Gestion des concours - Générer/actualiser les statuts des résultats';
    this.biometricList = BiometricStatusValues;
    this.sort = {prop: 'identifiantNationalEleve', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getNiveauAccesListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauAccesList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getCentreConcoursListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.centreList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAllConcours(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.concoursList = data.results;
      this._cd.markForCheck();
    });

    combineLatest([
      this.searchForm.get('institution').valueChanges
        .pipe(
          startWith(this.searchForm.get('institution').value)
        ),
      this.searchForm.get('concours').valueChanges
        .pipe(
          startWith(this.searchForm.get('concours').value)
        ),
      this.searchForm.get('anneeAcademique').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademique').value)
        ),
      this.searchForm.get('niveauAcces').valueChanges
        .pipe(
          startWith(this.searchForm.get('niveauAcces').value)
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      fromEvent(
        this.searchPV.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('numeroPV').value}
            })
          ),
      fromEvent(
        this.searchMoyenneTotalMin.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('moyenneTotalMin').value}
            })
          ),
      fromEvent(
        this.searchMoyenneTotalMax.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('moyenneTotalMax').value}
            })
          ),
      this.searchForm.get('centreConcours').valueChanges
        .pipe(
          startWith(this.searchForm.get('centreConcours').value)
        ),
      this.searchForm.get('biometricStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('biometricStatus').value)
        ),
    ])
    .subscribe((
      [
        institution,
        concours,
        anneeAcademique,
        niveauAcces,
        eventSearchNationalStudentIdentifier,
        eventSearchName, 
        eventFirstname,
        eventSearchPV,
        eventSearchMoyenneTotalMin,
        eventSearchMoyenneTotalMax,
        centreConcours,
        biometricStatus,
      ]) => {
      this.searchForm.patchValue({
        institution,
        concours,
        anneeAcademique,
        niveauAcces,
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null,
        numeroPV: eventSearchPV ? eventSearchPV['target'].value : null, 
        moyenneTotalMin: eventSearchMoyenneTotalMin ? eventSearchMoyenneTotalMin['target'].value : null, 
        moyenneTotalMax: eventSearchMoyenneTotalMax ? eventSearchMoyenneTotalMax['target'].value : null, 
        centreConcours,
        biometricStatus,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      identifiantNationalEleve: null,
      institution: null,
      concours: null,
      anneeAcademique: null,
      niveauAcces: null,
      name: null,
      firstName: null,
      numeroPV: null,
      biometricStatus: null,
      centreConcours: null,
      moyenneTotalMin: null,
      moyenneTotalMax: null,
      statusResult: null,
      statusGlobal: null,
      isCurrentSession: true,
      isOrienterResult: true
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getConcoursEtudiantListPage(criteria);
  }

  publishResults() {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Confirmation de générer/actualiser les statuts des résultats", 
        message: "Voulez vous vraiment générer/actualiser les statuts des résultats ? Attention cette action est irréversible.", 
        confirmBtnText: 'Actualiser'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.getConcoursEtudiantIdsForPublish(MetfpetServiceAgent.PagingSearchDTO.fromJS({
          pageIndex: -1,
          filters: this.searchForm.value,
        }))
          .subscribe(
            (data) => {
              this.process(data);
            },
            error => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  async process(info: MetfpetServiceAgent.PublishConcoursResultInfo) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this.showWarningMessage = true;
    this.processed = 0;
    this.totalToprocess = info.numberOfItems;

    for (let item of info.concoursEtudiantIds) {    
      try {
        this.processed = this.processed + item.length;
        this._cd.markForCheck();
        await this._metfpetService.publishConcoursResults(item).toPromise();
      } catch(error) {
      }
    }

    this.processing = false;
    this.triggerSearch();
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
  }

  goBack() {
    this._location.back();
  }
}