import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { combineLatest, Observable } from 'rxjs';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  PerfectScrollService,
  MetfpetServiceAgent,
} from 'MetfpetLib';
import { Tabs } from './concours-tab.component';
import { MatDialog } from '@angular/material/dialog';
import { ConcoursDialog } from './concours.dialog';
import { startWith } from 'rxjs/operators';

@Component({
  templateUrl: './concours.component.html',
  styleUrls: ['./concours.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConcoursComponent extends BaseTableComponent {

  settings: Settings;
  title: string;
  navs = Tabs;
  niveauAccesList: Array<MetfpetServiceAgent.NiveauAcces>;
  typeProgrammeList: Array<MetfpetServiceAgent.TypeProgramme>;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialog: MatDialog,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Configuration - Configurer les concours d’accès';
    this.sort = {prop: 'name', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();
    this._metfpetService.getNiveauAccesList().subscribe(data => {
      this.niveauAccesList = data;
      this._cd.markForCheck();
    });
    this._metfpetService.getTypeProgrammeList().subscribe(data => {
      this.typeProgrammeList = data;
      this._cd.markForCheck();
    });
    this.triggerSearch();
  }

  ngAfterViewInit() {
    combineLatest([
      this.searchForm.get('name').valueChanges.pipe(startWith(this.searchForm.get('name').value)),
    ])
    .subscribe(([name]) => {
      this.searchForm.patchValue({name}, {emitEvent: false});
      this.triggerSearch();
    });
  }
  
  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      status: null,
      name: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  createConcours(){
    this._dialog.open(ConcoursDialog , {
      width: '800px',
      data: {
        niveauAccesList: this.niveauAccesList,
        typeProgrammeList: this.typeProgrammeList
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getConcoursListPage(criteria);
  }
  
  open(row: MetfpetServiceAgent.ConcoursDTO) {
    this._dialog.open(ConcoursDialog , {
      width: '800px',
      data: {
        niveauAccesList: this.niveauAccesList,
        typeProgrammeList: this.typeProgrammeList,
        concours: row
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }
}