import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ConcoursTabComponent } from './concours-tab.component';
import { ConcoursComponent } from './concours.component';
import { ConcoursDialog } from './concours.dialog';
import { MentionConcoursComponent } from './mention-concours.component';
import { MentionConcoursDialog } from './mention-concours.dialog';
import { ConcoursEtudiantTabComponent } from './concours-etudiant-tab.component';
import { ConcoursEtudiantsComponent } from './concours-etudiants.component';
import { ConcoursOperationsComponent } from './concours-operations.component';
import { GenererCandidatsDialog } from './generer-candidats.dialog';
import { EntrerLesConcourNotesDialog } from './entrer-les-concour-notes.dialog';
import { PublierLesResultatsComponent } from './publier-les-resultats.component';
import { PublierLesResultatsTabComponent } from './publier-les-resultats-tab.component';
import { ActualiserOffresComponent } from './actualiser-offres.component';
import { ActualiserOffresTabComponent } from './actualiser-offres-tab.component';
import { ConcoursEtudiantsPasseesComponent } from './concours-etudiants-passees.component';
import { ConcoursRapportsComponent } from './concours-rapports.component';
import { GenerationDeConcoursRapportDialog } from './generation-de-concours-rapport.dialog';
import { ConcoursImportLesNotesDialog } from './concours-import-les-notes.dialog';
import { ConcoursImportLesNotesComponent } from './concours-import-les-notes.component';
import { ConcoursExporteLesTemplatesDeNotesComponent } from './concours-exporte-les-templates-de-notes.component';
import { TelechargerConcoursRapportDialog } from './telecharger-concours-rapport.dialog';
import { ExportPropositionOrientationDialog } from './export-proposition-orientation.dialog';
import { ResetTermineEtudiantsDialog } from './reset-termine-etudiants.dialog';

export const routes = [
  {
    path: "",
    redirectTo: "currentSession",
    pathMatch: "full",
  },
  {
    path: "currentSession",
    component: ConcoursEtudiantsComponent,
    data: { breadcrumb: "Session en cours" },
    pathMatch: "full",
  },
  {
    path: "pastSession",
    component: ConcoursEtudiantsPasseesComponent,
    data: { breadcrumb: "Session passées" },
    pathMatch: "full",
  },
  {
    path: "report",
    component: ConcoursRapportsComponent,
    data: { breadcrumb: "Rapports" },
    pathMatch: "full",
  },
  {
    path: "publierLesResultats",
    component: PublierLesResultatsComponent,
    data: { breadcrumb: "Publier tous les résultats" },
    pathMatch: "full",
  },
  {
    path: "actualiserOffres",
    component: ActualiserOffresComponent,
    data: { breadcrumb: "Actualiser classement et offres" },
    pathMatch: "full",
  },
  {
    path: "configurations",
    redirectTo: "configurations/mention",
    pathMatch: "full",
  },
  {
    path: "configurations/mention",
    component: MentionConcoursComponent,
    data: { breadcrumb: "Seuil des mentions et de passage" },
    pathMatch: "full",
  },
  {
    path: "configurations/exporter",
    component: ConcoursExporteLesTemplatesDeNotesComponent,
    data: { breadcrumb: "Exporter les templates de notes" },
    pathMatch: "full",
  },
  {
    path: "configurations/importerNote",
    component: ConcoursImportLesNotesComponent,
    data: { breadcrumb: "Importer les notes" },
    pathMatch: "full",
  },
  {
    path: "configurations/concoursAcces",
    component: ConcoursComponent,
    data: { breadcrumb: "Configurer les concours d’accès" },
    pathMatch: "full",
  },
  {
    path: "operation",
    component: ConcoursOperationsComponent,
    data: { breadcrumb: "Opération - Concours" },
    pathMatch: "full",
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    ConcoursTabComponent,
    ConcoursComponent,
    ConcoursDialog,
    MentionConcoursComponent,
    MentionConcoursDialog,
    ConcoursEtudiantTabComponent,
    ConcoursEtudiantsComponent,
    GenererCandidatsDialog,
    ConcoursOperationsComponent,
    EntrerLesConcourNotesDialog,
    PublierLesResultatsTabComponent,
    PublierLesResultatsComponent,
    ActualiserOffresComponent,
    ActualiserOffresTabComponent,
    ConcoursEtudiantsPasseesComponent,
    ConcoursRapportsComponent,
    GenerationDeConcoursRapportDialog,
    ConcoursImportLesNotesDialog,
    ConcoursImportLesNotesComponent,
    ConcoursExporteLesTemplatesDeNotesComponent,
    TelechargerConcoursRapportDialog,
    ExportPropositionOrientationDialog,
    ResetTermineEtudiantsDialog
  ]
})
export class ConcoursModule {}
