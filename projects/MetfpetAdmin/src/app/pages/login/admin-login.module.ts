import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLoginComponent } from './admin-login.component';
import { SharedModule } from '../../shared/shared.module';

export const routes = [
  { path: '', component: AdminLoginComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule, 
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    AdminLoginComponent
  ]

})
export class AdminLoginModule { }