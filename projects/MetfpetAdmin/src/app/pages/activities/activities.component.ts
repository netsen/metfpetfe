import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ElementRef, 
  ViewChild, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, of as observableOf } from 'rxjs';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { startWith } from 'rxjs/operators/startWith';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  PerfectScrollService,
  MetfpetServiceAgent,
} from 'MetfpetLib';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { ExportActivitiesDialog } from './export-activities.dialog';

@Component({
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActivitiesComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;

  settings: Settings;
  actionTypeList: Array<any>;
  title: string;
  
  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialog: MatDialog,
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Journaux d’évènements';
    this.actionTypeList = [
      {value : 'Valider le paiement de l\'admission', name : 'Valider le paiement de l\'admission'},
      {value : 'Annuler le paiement de l\'admission', name : 'Annuler le paiement de l\'admission'},
      {value : 'Valider le paiement de l\'inscription', name : 'Valider le paiement de l\'inscription'},
      {value : 'Annuler le paiement de l\'inscription', name : 'Annuler le paiement de l\'inscription'},
      {value : 'Valider le paiement du document', name : 'Valider le paiement du document'},
      {value : 'Annuler le paiement du document', name : 'Annuler le paiement du document'},
      {value : 'Ajouter une admission manuellement', name : 'Ajouter une admission manuellement'},
      {value : 'Ajouter une inscription manuellement', name : 'Ajouter une inscription manuellement'},
      {value : 'Modifier une inscription existante', name : 'Modifier une inscription existante'},
      {value : 'Modifier une admission existante', name : 'Modifier une admission existante'},
      {value : 'Générer une bourse manuellement', name : 'Générer une bourse manuellement'},
      {value : 'Payer les bourses manuellement', name : 'Payer les bourses manuellement'},
      {value : 'Payer les bourses du trimestre', name : 'Payer les bourses du trimestre'},
      {value : 'Opération - Bourses', name : 'Opération - Bourses'},
      {value : 'Opération - Examens', name : 'Opération - Examens'},
      {value : 'Opération - Concours', name : 'Opération - Concours'},
      {value : 'Opération - Diplômes', name : 'Opération - Diplômes'},
      {value : 'Agréments', name : 'Agréments'},
      {value : 'Modification du profil', name : 'Modification du profil'},
      {value : 'Entrée les concour notes', name : 'Entrée les concour notes'},
      {value : 'Importer les concour notes', name : 'Importer les concour notes'},
      {value : 'Modifier l\'accès du personnel du ministère', name : 'Modifier l\'accès du personnel du ministère'},
      {value : 'Accepter l\'offre', name : 'Accepter l\'offre'},
      {value : 'Refuser l\'offre', name : 'Refuser l\'offre'},
      {value : 'Générer/actualiser l\'offre', name : 'Générer/actualiser l\'offre'},
      {value : 'Ajouter un kit étudiant', name : 'Ajouter un kit étudiant'},
      {value : 'Générer fiche de réception', name : 'Générer fiche de réception'},
      {value : 'Import d\'une fiche de réception', name : 'Import d\'une fiche de réception'},
      {value : 'Modifier l\'outil', name : 'Modifier l\'outil'},
      {value : 'Diminution de quantité', name : 'Diminution de quantité'},
      {value : 'Modifier une programme', name : 'Modifier une programme'},
      {value : 'Modifier une institution', name : 'Modifier une institution'},
    ];
    this.sort = {prop: 'identifiantNationalEleve', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();

    combineLatest(
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      this.searchForm.get('actionType').valueChanges
        .pipe(
          startWith(this.searchForm.get('actionType').value)
        ),
    )
    .subscribe(([
      eventSearchNationalStudentIdentifier,
      eventSearchName, 
      eventFirstname,
      actionType]) => {
      this.searchForm.patchValue({
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null, 
        actionType,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      identifiantNationalEleve: null,
      name: null,
      firstName: null,
      actionType: null,
      from: null,
      to: null
    };
    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getUserActivityListPage(criteria);
  }

  exportLogs() {
    this._dialog.open(ExportActivitiesDialog, {
      width: '650px',
      data: {
        filters: this.searchForm.value
      }
    });
  }
}
