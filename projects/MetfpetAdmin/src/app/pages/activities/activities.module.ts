import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ActivitiesComponent } from './activities.component';
import { ExportActivitiesDialog } from './export-activities.dialog';

export const routes = [
  { path: '', component: ActivitiesComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    ActivitiesComponent,
    ExportActivitiesDialog
  ]
})
export class ActivitiesModule {}
