import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef,
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators/';
import { Store } from '@ngrx/store';
import { 
  AppSettings,
  Settings,
  AuthorityEnum,
  BaseTableComponent,
  PerfectScrollService,
  AuthService,
  DialogService,
  ExportService,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  DemandeurStatusValues,
  BiometricStatusValues,
} from 'MetfpetLib';
import { UiPath } from '../../ui-path';

@Component({
  templateUrl: './admin-dossiers.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminDossiersComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;

  settings: Settings;
  title: string;
  sessionsList: Array<any>;
  statusList: Array<any>;
  biometricList = BiometricStatusValues;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Dossier des demandeurs';
    this.statusList = DemandeurStatusValues;
    this.sort = {prop: 'identifiant', dir: 'asc'};
  }

  isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }
  
  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getSessionBACList().subscribe(data => {
      this.sessionsList = data.map(x => {return {name: x}});
      this._cd.markForCheck();
    });

    combineLatest([
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      this.searchForm.get('typeDemandeur').valueChanges
        .pipe(
          startWith(this.searchForm.get('typeDemandeur').value)
        ),
      this.searchForm.get('email').valueChanges
        .pipe(
          startWith(this.searchForm.get('email').value)
        ),
      this.searchForm.get('periode').valueChanges
        .pipe(
          startWith(this.searchForm.get('periode').value)
        ),
      this.searchForm.get('demandeurStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('demandeurStatus').value)
        ),
      this.searchForm.get('biometricStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('biometricStatus').value)
        ),
    ])
    .subscribe((
      [
        eventSearchName, 
        eventFirstname,
        typeDemandeur,
        email,
        periode,
        demandeurStatus,
        biometricStatus
      ]) => {
      this.searchForm.patchValue({
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null, 
        typeDemandeur,
        email,
        periode,
        demandeurStatus,
        biometricStatus
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      name: null,
      firstName: null,   
      typeDemandeur : null,
      email: null,
      periode: null,
      demandeurStatus: null,
      biometricStatus: null
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getDemandeurListPage(criteria);
  }

  open(selectedDossier: MetfpetServiceAgent.DemandeurViewModel) {
    this._router.navigate([`${UiPath.admin.agrementation.dossiers.dossier}/${selectedDossier.id}`], { state: { previousPage: 'Dossier des demandeurs' } });
  }
}
