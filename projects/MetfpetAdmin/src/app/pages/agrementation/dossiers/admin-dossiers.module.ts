import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { AdminDossiersComponent } from './admin-dossiers.component';
import { AdminManageDemandeurComponent } from './admin-manage-demandeur.component';

export const routes = [
  { 
    path: '', 
    component: AdminDossiersComponent, 
    pathMatch: 'full'
  },
  { 
    path: 'dossier/:id', 
    component: AdminManageDemandeurComponent, 
    data: { breadcrumb: 'Dossier' }
  },
  { 
    path: 'add', 
    component: AdminManageDemandeurComponent, 
    data: { breadcrumb: 'Ajouter un demandeur manuellement' } 
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    AdminDossiersComponent,
    AdminManageDemandeurComponent
  ],
  entryComponents: [
  ]
})
export class AdminDossiersModule {

}
