import { 
  ChangeDetectionStrategy, 
  Component, 
  OnInit, 
  ViewEncapsulation 
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AppSettings,
  Settings,
  MetfpetServiceAgent,
} from 'MetfpetLib';
import { UiPath } from '../../ui-path';
import { Location } from '@angular/common';

@Component({
  templateUrl: './admin-manage-demandeur.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminManageDemandeurComponent implements OnInit {

  settings: Settings;
  title: string;
  demandeur: MetfpetServiceAgent.DemandeurProfileViewModel;
  demandeurId: string;
  loadingIndicator: boolean;
  previousPage: string;
  fromComptesDemandeurs: boolean;
  iconName = 'menu-clipboard';

  constructor(
    public appSettings: AppSettings, 
    private _router: Router,
    private _route: ActivatedRoute,
    private _location: Location,
  ) {
    this.settings = this.appSettings.settings;
    this.previousPage = this._router.getCurrentNavigation().extras.state.previousPage;
    this.title = this.previousPage;
    if (this.previousPage == 'Gestion des comptes demandeurs') {
      this.iconName = 'menu-management';
      this.fromComptesDemandeurs = true;
    } else {
      this.iconName = 'menu-dossier-students';
      this.fromComptesDemandeurs = false;
    }
  }

  ngOnInit() {
    this._route.params.subscribe(params => this.demandeurId = params['id']);
  }

  onLoadedDemandeur(demandeur: MetfpetServiceAgent.DemandeurProfileViewModel) {
    this.demandeur = demandeur;
    this.title = this.previousPage + " - Dossier de " + this.demandeur.firstName + ' ' + this.demandeur.name;
  }

  onSavedDemandeur(demandeur: MetfpetServiceAgent.DemandeurProfileViewModel) {
    this.demandeur = demandeur;
    this._location.back();
  }
}