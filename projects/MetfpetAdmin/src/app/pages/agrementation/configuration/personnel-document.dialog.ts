import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  TypeDemandeStatus,
  ChampStatus,
  ChampTypeValues,
  PersonnelDocumentStatus,
} from 'MetfpetLib';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './personnel-document.dialog.html',
})
export class PersonnelDocumentDialog {

  title: string;
  form: FormGroup;
  id: string;
  model: MetfpetServiceAgent.PersonnelDocumentDTO;
  documentList: Array<MetfpetServiceAgent.AgrementationDocumentViewModel>;
  isCreation: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<PersonnelDocumentDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.documentList = data.documentList;
    this.id = data.id;

    this.form = this._formBuilder.group({
      agrementationId: data.agrementationId,
      status: true,
      agrementationDocumentId: [null, Validators.required],
    });
    
    if (data.id) {
      this.title = 'Modifier un document';
      this.isCreation = false;
    } else {
      this.title = 'Ajouter un document';
      this.isCreation = true;
    }
  }

  ngOnInit() {
    if (!this.isCreation && this.id) {
      this._metfpetService.getPersonnelDocument(this.id).subscribe(
        (data) => {
          this.model = data;
          this.form.patchValue({
            agrementationId: this.model.agrementationId,
            agrementationDocumentId: this.model.agrementationDocumentId,
            status: this.model.status === <any>PersonnelDocumentStatus.Active,
          });
        }
      );
    }
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var savePersonnelDocument$: Observable<MetfpetServiceAgent.PersonnelDocumentDTO>;

      if (this.isCreation) {
        savePersonnelDocument$ = this._metfpetService.createPersonnelDocument(MetfpetServiceAgent.PersonnelDocumentDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value, 
            { status: this.form.get('status').value ? PersonnelDocumentStatus.Active : PersonnelDocumentStatus.Inactive }
          )));
      } else {
        savePersonnelDocument$ = this._metfpetService.updatePersonnelDocument(MetfpetServiceAgent.PersonnelDocumentDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value, 
            { status: this.form.get('status').value ? PersonnelDocumentStatus.Active : PersonnelDocumentStatus.Inactive }
          )));
      }
      
      savePersonnelDocument$.subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}