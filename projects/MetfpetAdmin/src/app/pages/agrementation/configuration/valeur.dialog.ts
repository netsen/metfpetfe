import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  ValeurStatus,
} from 'MetfpetLib';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './valeur.dialog.html',
})
export class ValeurDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.ValeurDTO;
  isCreation: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<ValeurDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.form = this._formBuilder.group({
      name: [null, Validators.required],
      description: [null, Validators.required],
      status: true,
    });

    if (data.id) {
      this.title = 'Modifier un valeur';
      this.isCreation = false;
      this._metfpetService.getValeur(data.id).subscribe(
        (champ) => {
          this.model = champ;
          this.form.patchValue({
            name: this.model.name,
            description: this.model.description,
            status: this.model.status === <any>ValeurStatus.Active,
          });
        }
      );
    } else {
      this.title = 'Ajouter un valeur';
      this.isCreation = true;
    }
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveValeur$: Observable<MetfpetServiceAgent.ValeurDTO>;

      if (this.isCreation) {
        saveValeur$ = this._metfpetService.createValeur(MetfpetServiceAgent.ValeurDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value, 
            { status: this.form.get('status').value ? ValeurStatus.Active : ValeurStatus.Inactive }
          )));
      } else {
        saveValeur$ = this._metfpetService.updateValeur(MetfpetServiceAgent.ValeurDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value, 
            { status: this.form.get('status').value ? ValeurStatus.Active : ValeurStatus.Inactive }
          )));
      }
      
      saveValeur$.subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}