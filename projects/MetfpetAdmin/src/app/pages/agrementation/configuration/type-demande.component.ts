import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import {
  AppSettings,
  BaseTableComponent,
  DialogService,
  MetfpetServiceAgent,
  PerfectScrollService,
  Settings,
}
  from 'MetfpetLib';
import { combineLatest, fromEvent, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith } from 'rxjs/operators';
import { Tabs } from './configuration-tab.component';
import { TypeDemandeDialog } from './type-demande.dialog';

@Component({
  selector: 'app-type-demande',
  templateUrl: './type-demande.component.html',
})
export class TypeDemandeComponent extends BaseTableComponent implements OnInit {

  @ViewChild('searchName', { static: true }) searchName: ElementRef;

  settings: Settings;
  title: string;
  navs = Tabs;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialog: MatDialog,
    private _dialogService: DialogService,
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Configuration - Types de demandes';
    this.sort = { prop: 'name', dir: 'desc' };
  }

  ngAfterViewInit() {
    combineLatest([
      fromEvent(
        this.searchName.nativeElement, 'keyup')
        .pipe(
          debounceTime(800),
          distinctUntilChanged(),
          startWith({
            target: { value: this.searchForm.get('name').value }
          })
        ),
      this.searchForm.get('isActive').valueChanges.pipe(startWith(this.searchForm.get('isActive').value)),
    ])
      .subscribe(([
        eventName,
        isActive
      ]) => {
        this.searchForm.patchValue({
          name: eventName ? eventName['target'].value : null,
          isActive
        }, { emitEvent: false });
        this.triggerSearch();
      });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null,
      isActive: true,
    });
  }

  protected _search(criteria: any): Observable<any> {
    criteria.filters.isActive = +criteria.filters.isActive;
    return this._metfpetService.getTypeDemandeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  open(row: MetfpetServiceAgent.TypeDemandeViewModel) {
    this._dialog.open(TypeDemandeDialog, {
      width: '540px',
      data: {
        id: row.id,
        name: row.name,
        status: row.status,
        logoUrl: row.logoUrl,
        typeDemande: row
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  addAgrement() {
    this._dialog.open(TypeDemandeDialog, {
      width: '540px',
      data: {
      }

    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

}
