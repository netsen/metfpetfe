import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  DocumentSectionStatus,
} from 'MetfpetLib';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './document-section.dialog.html',
})
export class DocumentSectionDialog {

  title: string;
  form: FormGroup;
  id: string;
  model: MetfpetServiceAgent.DocumentSectionDTO;
  documentList: Array<MetfpetServiceAgent.AgrementationDocumentViewModel>;
  sections: Array<MetfpetServiceAgent.SectionDTO>;
  isCreation: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<DocumentSectionDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.documentList = data.documentList;
    this.sections = data.sections;

    this.form = this._formBuilder.group({
      status: true,
      sectionId: [null, Validators.required],
      agrementationDocumentId: [null, Validators.required],
    });
    
    if (data.model) {
      this.model = data.model;
      this.title = 'Modifier un document';
      this.isCreation = false;
    } else {
      this.title = 'Ajouter un document';
      this.isCreation = true;
    }
  }

  ngOnInit() {
    if (!this.isCreation && this.model) {
      this.form.patchValue({
        status: this.model.status === <any>DocumentSectionStatus.Active,
        sectionId: this.model.sectionId,
        agrementationDocumentId: this.model.agrementationDocumentId,
      });
    }
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveDocumentSection$: Observable<MetfpetServiceAgent.DocumentSectionDTO>;

      if (this.isCreation) {
        saveDocumentSection$ = this._metfpetService.createDocumentSection(MetfpetServiceAgent.DocumentSectionDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value, 
            { status: this.form.get('status').value ? DocumentSectionStatus.Active : DocumentSectionStatus.Inactive }
          )));
      } else {
        saveDocumentSection$ = this._metfpetService.updateDocumentSection(MetfpetServiceAgent.DocumentSectionDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value, 
            { status: this.form.get('status').value ? DocumentSectionStatus.Active : DocumentSectionStatus.Inactive }
          )));
      }
      
      saveDocumentSection$.subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}