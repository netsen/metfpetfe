import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';

@Component({
  templateUrl: './demandeur-document.dialog.html',
})
export class DemandeurDocumentDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.DemandeurDocumentListDTO;
  agrementationDocumentList: Array<MetfpetServiceAgent.AgrementationDocumentViewModel>;
  error: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<DemandeurDocumentDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.agrementationDocumentList = data.agrementationDocumentList;

    this.form = this._formBuilder.group({
      agrementationDocuments: this._formBuilder.array([]),
    });

    this.title = 'Gérer les documents de création de compte';

    if (data.model) {
      this.model = data.model;
      if (this.model.demandeurDocuments) {
        for (var dto of this.model.demandeurDocuments) {
          this.agrementationDocuments.push(this._formBuilder.group({
            agrementationDocumentId: dto.agrementationDocumentId,
            isMandatory: dto.isMandatory
          }));
        }
      }
      this._cd.markForCheck();
    }
  }

  get agrementationDocuments() {
    return this.form.controls['agrementationDocuments'] as FormArray;
  }

  addAgrementationDocument() {
    this.agrementationDocuments.push(this._formBuilder.group({
      agrementationDocumentId: null,
      isMandatory: true
    }));
    this._cd.markForCheck();
  }

  delete(i: number) {
    this.agrementationDocuments.removeAt(i);
    this._cd.markForCheck();
  }

  onConfirm() {
    this.error = null;
    validateForm(this.form);
    if (this.form.valid && this._validateDocuments()) {

      this._store.dispatch(showLoading());
      this.model = Object.assign({}, this.model, this.form.value);
      this.model.demandeurDocuments = this.agrementationDocuments.value.filter(x => !!x.agrementationDocumentId).map(x => MetfpetServiceAgent.DemandeurDocumentDTO.fromJS(x));

      this._metfpetService.updateDemandeurDocuments(MetfpetServiceAgent.DemandeurDocumentListDTO.fromJS(this.model)).subscribe(
        () => {
          this._store.dispatch(showSuccess({}));
          this._dialogRef.close(true);
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
    }
  }

  private _validateDocuments() {
    var choices = this.agrementationDocuments.value.filter(x => !!x.agrementationDocumentId).map(x => x.agrementationDocumentId);

    if (new Set(choices).size !== choices.length) {
      this.error = 'Les document de création de compte sont dupliquées';
    }

    return !this.error;
  }

  onClose() {
    this._dialogRef.close();
  }
}