import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  PersonnelMemberStatus,
  PersonnelType,
} from 'MetfpetLib';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './personnel-member.dialog.html',
})
export class PersonnelMemberDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.PersonnelMemberDTO;
  personnelDocumentList: Array<MetfpetServiceAgent.AgrementationDocumentViewModel>;
  isCreation: boolean;
  id: string;
  error: string;
  personnelType: MetfpetServiceAgent.PersonnelType;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<PersonnelMemberDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.personnelDocumentList = data.personnelDocumentList;
    this.id = data.id;
    this.personnelType = data.isAdministratif ? MetfpetServiceAgent.PersonnelType.Administratif : MetfpetServiceAgent.PersonnelType.Formateur;

    this.form = this._formBuilder.group({
      agrementationId: data.agrementationId,
      poste: [null, Validators.required],
      status: true,
      personnelDocuments: this._formBuilder.array([]),
    });

    if (data.id) {
      this.title = 'Modifier un membre du personnel';
      this.isCreation = false;
    } else {
      this.title = 'Ajouter un membre du personnel';
      this.isCreation = true;
    }
  }

  ngOnInit() {
    if (!this.isCreation && this.id) {
      this._metfpetService.getPersonnelMember(this.id).subscribe(
        (data) => {
          this.model = data;
          this.form.patchValue({
            agrementationId: this.model.agrementationId,
            poste: this.model.poste,
            status: this.model.status === <any>PersonnelMemberStatus.Active,
          });
          if (this.model.documents) {
            for (var id of this.model.documents) {
              this.personnelDocuments.push(this._formBuilder.group({
                personnelDocumentId: id, 
              }));
            }
          }
          this._cd.markForCheck();
        }
      );
    }
  }

  get personnelDocuments() {
    return this.form.controls['personnelDocuments'] as FormArray;
  }

  addPersonnelDocument() {
    this.personnelDocuments.push(this._formBuilder.group({
      personnelDocumentId: null, 
    }));
    this._cd.markForCheck();
  }

  delete(i: number) {
    this.personnelDocuments.removeAt(i);
    this._cd.markForCheck();
  }

  onConfirm() {
    this.error = null;
    validateForm(this.form);
    if (this.form.valid && this._validatePersonnelDocuments()) {

      this._store.dispatch(showLoading());
      this.model = Object.assign({}, this.model, this.form.value, 
          { status: this.form.get('status').value ? PersonnelMemberStatus.Active : PersonnelMemberStatus.Inactive }
        );

      this.model.documents = this.personnelDocuments.value.filter(x => !!x.personnelDocumentId).map(x => x.personnelDocumentId);
      
      var savePersonnelMember$: Observable<MetfpetServiceAgent.PersonnelMemberDTO>;

      if (this.isCreation) {
        this.model.personnelType = this.personnelType;
        savePersonnelMember$ = this._metfpetService.createPersonnelMember(MetfpetServiceAgent.PersonnelMemberDTO.fromJS(this.model));
      } else {
        savePersonnelMember$ = this._metfpetService.updatePersonnelMember(MetfpetServiceAgent.PersonnelMemberDTO.fromJS(this.model));
      }

      savePersonnelMember$.subscribe(
            () => {
              this._store.dispatch(showSuccess({}));
              this._dialogRef.close(true);
            },
            error => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
    }
  }

  private _validatePersonnelDocuments() {
    var choices = this.personnelDocuments.value.filter(x => !!x.documentId).map(x => x.documentId);

    if (new Set(choices).size !== choices.length) {
      this.error = 'Les membre du personnel sont dupliquées';
    }

    return !this.error;
  }

  onClose() {
    this._dialogRef.close();
  }
}