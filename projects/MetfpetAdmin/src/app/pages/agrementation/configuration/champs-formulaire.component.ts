import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { startWith, debounceTime, distinctUntilChanged } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent, 
  MetfpetServiceAgent,
  PerfectScrollService,
  ChampTypeValues,
  DialogService,
  showLoading,
  showSuccess,
  showError,
  hideLoading,
  ChampType,
} from 'MetfpetLib';
import { Tabs } from './configuration-tab.component';
import { MatDialog } from '@angular/material/dialog';
import { ChampDialog } from './champ.dialog';
import { ChampValeurDialog } from './champ-valeur.dialog';

@Component({
  templateUrl: './champs-formulaire.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChampsFormulaireComponent extends BaseTableComponent {
  
  @ViewChild('searchName', {static: true}) searchName: ElementRef;

  settings: Settings;
  title: string;
  navs = Tabs;
  champTypeList: Array<any>;
  valeurList: Array<MetfpetServiceAgent.ValeurViewModel>;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialog: MatDialog,
    private _dialogService: DialogService,
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.champTypeList = ChampTypeValues;
    this.title = 'Configuration - Champs de formulaire';
    this.sort = {prop: 'name', dir: 'desc'};

    this._metfpetService.getValeurListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.valeurList = data.results;
    });
  }

  ngAfterViewInit() {
    combineLatest([
      fromEvent(
        this.searchName.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('name').value}
            })
          ),
        this.searchForm.get('type').valueChanges.pipe(startWith(this.searchForm.get('type').value)),
        this.searchForm.get('isActive').valueChanges.pipe(startWith(this.searchForm.get('isActive').value)),
    ])
    .subscribe(([
      eventName, 
      type,
      isActive
    ]) => {
      this.searchForm.patchValue({
        name: eventName ? eventName['target'].value : null, 
        type,
        isActive
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null,
      isActive: true,
      type: null
    });
  }

  protected _search(criteria: any): Observable<any> {
    // to convert True/false value to number 1/0
    criteria.filters.isActive = +criteria.filters.isActive;
    return this._metfpetService.getChampListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  allowLinkValeurs(row: MetfpetServiceAgent.ChampViewModel) {
    return row.type === <any> ChampType.CheckBox || row.type === <any> ChampType.RadioButton || row.type === <any> ChampType.DropdownList;
  }

  linkValeurs(row: MetfpetServiceAgent.ChampViewModel) {
    this._dialog.open(ChampValeurDialog, {
      width: '600px',
      data: {
        champ: row,
        valeurList: this.valeurList
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  open(row: MetfpetServiceAgent.ChampViewModel) {
    this._dialog.open(ChampDialog, {
      width: '600px',
      data: {
        id: row.id
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  addChamp() {
    this._dialog.open(ChampDialog, {
      width: '600px',
      data: {
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }
  
  delete(row: MetfpetServiceAgent.ChampViewModel) {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention", 
        message: "Voulez-vous définitivement supprimer ce champ ?", 
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteChamp(row.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({message: 'Le champ a été supprimée'}));
              this.triggerSearch();
            },
            (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }
}
