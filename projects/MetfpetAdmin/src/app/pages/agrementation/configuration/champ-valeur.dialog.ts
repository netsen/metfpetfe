import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';

@Component({
  templateUrl: './champ-valeur.dialog.html',
})
export class ChampValeurDialog {

  title: string;
  form: FormGroup;
  champ: MetfpetServiceAgent.ChampViewModel;
  valeurList: Array<MetfpetServiceAgent.ValeurViewModel>;
  champValeur: MetfpetServiceAgent.ChampValeurViewModel;
  error: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<ChampValeurDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.title = 'Lier des valeurs à un champ';
    this.champ = data.champ;
    this.valeurList = data.valeurList;

    this.form = this._formBuilder.group({
      valeurs: this._formBuilder.array([]),
    });
  }

  ngOnInit() {
    this._metfpetService.getChampValeur(this.champ.id).subscribe(
      (champValeur) => {
        this.champValeur = champValeur;
        if (this. champValeur.valeurIds) {
          for (var id of this.champValeur.valeurIds) {
            this.valeurs.push(this._formBuilder.group({
              valeurId: id, 
            }));
          }
          this._cd.markForCheck();
        }
      }
    );
  }

  get valeurs() {
    return this.form.controls['valeurs'] as FormArray;
  }

  addValeur() {
    this.valeurs.push(this._formBuilder.group({
      valeurId: null, 
    }));
    this._cd.markForCheck();
  }

  delete(i: number) {
    this.valeurs.removeAt(i);
    this._cd.markForCheck();
  }

  onConfirm() {
    this.error = null;

    if (this._validateValeurs()) {

      this._store.dispatch(showLoading());

      this.champValeur.valeurIds = this.valeurs.value.filter(x => !!x.valeurId).map(x => x.valeurId);
      this._metfpetService.updateChampValeur(this.champValeur) 
        .subscribe(
            () => {
              this._store.dispatch(showSuccess({}));
              this._dialogRef.close(true);
            },
            error => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
    }
  }

  private _validateValeurs() {
    var choices = this.valeurs.value.filter(x => !!x.valeurId).map(x => x.valeurId);

    if (new Set(choices).size !== choices.length) {
      this.error = 'Les valeurs sont dupliquées';
    }

    return !this.error;
  }

  onClose() {
    this._dialogRef.close();
  }
}