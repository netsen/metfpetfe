import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  DialogService,
} from 'MetfpetLib';

@Component({
  selector: 'app-order-formulaire-section-champdialog',
  templateUrl: './order-formulaire-section-champ.dialog.html',
  styleUrls: ['./order-formulaire-section-champ.dialog.css']
})
export class OrderFormulaireSectionChampDialog implements OnInit {
  title: string;
  form: FormGroup;
  formulaireSections: Array<MetfpetServiceAgent.FormulaireSectionDTO>;
  formulaireSectionChamps: MetfpetServiceAgent.FormulaireSectionChampDTO[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<OrderFormulaireSectionChampDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
  ) {
    this.title = 'Gérer l’ordre des champs';
    this.formulaireSections = data.formulaireSections;
    this.form = this._formBuilder.group({
      formulaireSectionId: [null, Validators.required],
    });
  }

  ngOnInit() {
    if (this.formulaireSections && this.formulaireSections.length > 0) {
      this.form.patchValue({
        formulaireSectionId: this.formulaireSections[0].id
      });
    }
    this.loadFormulaireSectionChamps();
  }

  drop(event: CdkDragDrop<MetfpetServiceAgent.FormulaireSectionChampDTO[]>) {
    moveItemInArray(this.formulaireSectionChamps, event.previousIndex, event.currentIndex);
  }

  moveDown(i: number) {
    moveItemInArray(this.formulaireSectionChamps, i, i+1);
  }

  moveUp(i: number) {
    moveItemInArray(this.formulaireSectionChamps, i, i-1);
  }

  allowMoveDown(i: number): boolean {
    return i < this.formulaireSectionChamps.length - 1;
  }

  allowMoveUp(i: number): boolean {
    return i >= 1;
  }

  onFormulaireSectionChange() {
    if (this.formulaireSectionChamps && this.formulaireSectionChamps.length > 1) {
      this._dialogService.openConfirmDialog({
        width: '420px',
        data: {
          title: "Attention", 
          message: "Voulez-vous enregistrer avant de modifier la section de formulaire?", 
          confirmBtnText: 'Oui',
          cancelBtnText: 'Non'
        }
      }).afterClosed().subscribe(confirmed => {
        if (confirmed) {
          this.onConfirm();
        } else {
          this.loadFormulaireSectionChamps();
        }
      });
    } else {
      this.loadFormulaireSectionChamps();
    }
  }

  loadFormulaireSectionChamps() {
    this._metfpetService.getFormulaireSectionChampsOfSection(this.form.get("formulaireSectionId").value).subscribe(data => {
      this.formulaireSectionChamps = data;
      this._cd.markForCheck();
    });
  }


  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.orderFormulaireSectionChamps(this.formulaireSectionChamps).subscribe(
        () => {
          this._store.dispatch(showSuccess({}));
          this.loadFormulaireSectionChamps();
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}
