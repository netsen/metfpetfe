import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewEncapsulation
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  MetfpetServiceAgent,
  PerfectScrollService,
} from 'MetfpetLib';
import { DocumentTemplateDialog } from './agrementation-document-templates.dialog';
//import { DocumentTemplateDialog } from './document-template.dialog';


@Component({
  templateUrl: './agrementation-document-templates.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DocumentTemplatesComponent extends BaseTableComponent {

  settings: Settings;
  title: string;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialog: MatDialog,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Gestion des modèles de documents';
    this.sort = { prop: 'name', dir: 'desc' };
  }

  ngAfterViewInit() {
    this.triggerSearch();
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getAgrementationDocumentTemplateListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  open(row: MetfpetServiceAgent.AgrementationDocumentTemplateViewModel) {
    this._dialog.open(DocumentTemplateDialog, {
      width: '850px',
      height: '635px',
      data: {
        id: row.id,
        HtmlTemplate: row.htmlTemplate
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }
}