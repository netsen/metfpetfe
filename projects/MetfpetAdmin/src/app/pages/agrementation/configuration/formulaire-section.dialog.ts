import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import { Guid } from 'guid-typescript';
@Component({
  selector: 'app-formulaire-sectiondialog',
  templateUrl: './formulaire-section.dialog.html',
})
export class FormulaireSectionDialog implements OnInit {
  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.FormulaireSectionListDTO;
  employeRoleList: Array<MetfpetServiceAgent.EmployeRoleDTO>;
  error: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<FormulaireSectionDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.title = 'Gérer les sections de formulaire';
    this.employeRoleList = data.employeRoleList;
    this.model = data.formulaireSectionList;

    this.form = this._formBuilder.group({
      agrementationId: data.agrementationId,
      formulaireSections: this._formBuilder.array([]),
    });
  }

  ngOnInit() {
    if (this.model.formulaireSections) {
      for (var formulaireSection of this.model.formulaireSections) {
        this.formulaireSections.push(this._formBuilder.group({
          id: formulaireSection.id,
          name: formulaireSection.name,
          employeRoleId: formulaireSection.employeRoleId
        }));
      }
    }
    this._cd.markForCheck();
  }

  get formulaireSections() {
    return this.form.controls['formulaireSections'] as FormArray;
  }

  addFormulaireSection() {
    this.formulaireSections.push(this._formBuilder.group({
      id: Guid.EMPTY,
      name: null,
      employeRoleId: null
    }));
    this._cd.markForCheck();
  }

  allowAddFormulaireSection(): boolean {
    return this.formulaireSections.value.length < 5;
  }

  delete(i: number) {
    this.formulaireSections.removeAt(i);
    this._cd.markForCheck();
  }

  onConfirm() {
    this.error = null;
    validateForm(this.form);
    if (this.form.valid && this._validateFormulaireSections()) {

      this._store.dispatch(showLoading());
      this.model = Object.assign({}, this.model, this.form.value);

      this.model.formulaireSections = this.formulaireSections.value
        .filter(x => !!x.name && !!x.employeRoleId)
        .map(x => MetfpetServiceAgent.FormulaireSectionDTO.fromJS(x));
      
      this._metfpetService.updateFormulaireSectionList(MetfpetServiceAgent.FormulaireSectionListDTO.fromJS(this.model))
        .subscribe(
              () => {
                this._store.dispatch(showSuccess({}));
                this._dialogRef.close(true);
              },
              error => this._store.dispatch(showException({error: error}))
            )
            .add(() => this._store.dispatch(hideLoading()));
    }
  }

  private _validateFormulaireSections() {
    var choices1 = this.formulaireSections.value.filter(x => !x.name || !x.employeRoleId);
    if (choices1.length > 0) {
      this.error = 'Certains champs obligatoires manquent';
    }

    var choices2 = this.formulaireSections.value.filter(x => !!x.name).map(x => x.name);
    if (new Set(choices2).size !== choices2.length) {
      this.error = 'Les sections sont dupliquées';
    }

    return !this.error;
  }

  onClose() {
    this._dialogRef.close();
  }
}
