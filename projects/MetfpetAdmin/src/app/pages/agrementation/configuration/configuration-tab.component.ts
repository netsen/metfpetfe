import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../../ui-path';

export const Tabs = [
  { 
    routerLink: '/' + UiPath.admin.agrementation.configuration.agrements.list, 
    label: 'Agréments' 
  },
  { 
    routerLink: '/' + UiPath.admin.agrementation.configuration.documents.list, 
    label: 'Documents' 
  },
  { 
    routerLink: '/' + UiPath.admin.agrementation.configuration.champs.list, 
    label: 'Champs de formulaire' 
  },
  { 
    routerLink: '/' + UiPath.admin.agrementation.configuration.valeurs.list, 
    label: 'Valeurs champs de formulaires' 
  },
  { 
    routerLink: '/' + UiPath.admin.agrementation.configuration.typeDemandes.list, 
    label: 'Type de demandes' 
  },
  { 
    routerLink: '/' + UiPath.admin.agrementation.configuration.demandeurDocuments.list, 
    label: 'Document de création de compte' 
  },
  { 
    routerLink: '/' + UiPath.admin.agrementation.configuration.donneesFormation.list, 
    label: 'Données de formation' 
  }
];

@Component({
  templateUrl: './configuration-tab.component.html',
})
export class ConfigurationTabComponent {

  navs = Tabs;
  title: string;
  routerEventSubscription: Subscription;
  
  constructor(private router: Router) {
    this.title = 'Configuration - Agrémentation';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = 'Configuration - ' + selectedTab.label;
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
