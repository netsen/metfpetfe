import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import { Guid } from 'guid-typescript';

@Component({
  templateUrl: './process-step.dialog.html',
})
export class ProcessStepDialog {

  title: string;
  form: FormGroup;
  processStepList: Array<MetfpetServiceAgent.ProcessStepDTO>;
  agrementationId: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<ProcessStepDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.title = 'Etapes du processus';
    this.processStepList = data.processStepList;
    this.agrementationId = data.agrementationId;

    this.form = this._formBuilder.group({
      processSteps: this._formBuilder.array([]),
    });
  }

  ngOnInit() {
    if (this.processStepList) {
      for (var processStep of this.processStepList) {
        this.processSteps.push(this._formBuilder.group({
          processStepId: processStep.id, 
        }));
      }
      this._cd.markForCheck();
    }
  }

  get processSteps() {
    return this.form.controls['processSteps'] as FormArray;
  }

  allowAddProcessStep(): boolean {
    return this.processSteps.value.length < 4;
  }

  addProcessStep() {
    this.processSteps.push(this._formBuilder.group({
      processStepId: Guid.EMPTY, 
    }));
    this._cd.markForCheck();
  }

  delete(i: number) {
    this.processSteps.removeAt(i);
    this._cd.markForCheck();
  }

  onConfirm() {
    this._store.dispatch(showLoading());

    let numberOfProcessSteps = this.processSteps.value.length;
    let request = new MetfpetServiceAgent.UpdateNumberOfProcessStepsRequest();
    request.numberOfProcessSteps = numberOfProcessSteps;
    request.agrementationId = this.agrementationId;

    this._metfpetService.updateNumberOfProcessSteps(request) 
      .subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
  }

  onClose() {
    this._dialogRef.close();
  }
}