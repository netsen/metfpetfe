import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';

@Component({
  templateUrl: './process-step-validator.dialog.html',
})
export class ProcessStepValidatorDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.ProcessStepDTO;
  employeRoleList: Array<MetfpetServiceAgent.EmployeRoleDTO>;
  error: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<ProcessStepValidatorDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.model = data.model;
    this.employeRoleList = data.employeRoleList;
    this.title = 'Validateur - Étape ' + this.model.position;

    this.form = this._formBuilder.group({
      processStepValidators: this._formBuilder.array([]),
    });
  }

  ngOnInit() {
    if (this.model.processStepValidators) {
      for (var processStepValidator of this.model.processStepValidators) {
        this.processStepValidators.push(this._formBuilder.group({
          action: processStepValidator.action,
          employeRoleId: processStepValidator.employeRoleId, 
        }));
      }
      this._cd.markForCheck();
    }
  }

  get processStepValidators() {
    return this.form.controls['processStepValidators'] as FormArray;
  }

  allowAddProcessStepValidator(): boolean {
    return this.processStepValidators.value.length < 3;
  }

  addProcessStepValidator() {
    this.processStepValidators.push(this._formBuilder.group({
      action: null, 
      employeRoleId: null
    }));
    this._cd.markForCheck();
  }

  delete(i: number) {
    this.processStepValidators.removeAt(i);
    this._cd.markForCheck();
  }

  onConfirm() {
    this.error = null;
    if (this._validateProcessStepValidators()) {

      this._store.dispatch(showLoading());

      this.model.processStepValidators = this.processStepValidators.value
        .filter(x => !!x.action && !!x.employeRoleId)
        .map(x => MetfpetServiceAgent.ProcessStepValidatorDTO.fromJS(x));
      
      this._metfpetService.updateProcessStep(MetfpetServiceAgent.ProcessStepDTO.fromJS(this.model))
        .subscribe(
              () => {
                this._store.dispatch(showSuccess({}));
                this._dialogRef.close(true);
              },
              error => this._store.dispatch(showException({error: error}))
            )
            .add(() => this._store.dispatch(hideLoading()));
    }
  }

  private _validateProcessStepValidators() {
    var choices1 = this.processStepValidators.value.filter(x => !x.action || !x.employeRoleId);
    if (choices1.length > 0) {
      this.error = 'Certains champs obligatoires manquent';
    }

    var choices2 = this.processStepValidators.value.filter(x => !!x.action).map(x => x.action);
    if (new Set(choices2).size !== choices2.length) {
      this.error = 'Les validateur sont dupliquées';
    }

    return !this.error;
  }

  onClose() {
    this._dialogRef.close();
  }
}