import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';
import { TypeDemandeStatus } from 'projects/MetfpetLib/src/public-api';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-type-demande.dialog',
  templateUrl: './type-demande.dialog.html',
})
export class TypeDemandeDialog implements OnInit {
  title: string;
  isCreation: boolean;
  form: FormGroup;
  model: MetfpetServiceAgent.TypeDemandeDTO;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<TypeDemandeDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.form = this._formBuilder.group({
      name: [null, Validators.required],
      status: [null],
      logoUrl: [null],
    });

    if (data.id) {
      this.title = 'Modification un type de demande';
      this.isCreation = false;
      this.model = data.typeDemande
      this.form.patchValue({
        name: this.data.name,
        status : this.data.status === <any>TypeDemandeStatus.Active
      });
    } else {
      this.title = 'Ajouter un type de demande';
      this.isCreation = true;
    } 
  }

  ngOnInit(): void {
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveTypeDemande$: Observable<MetfpetServiceAgent.TypeDemandeDTO>;
      if(this.isCreation){
        saveTypeDemande$ = this._metfpetService.createTypeDemande(MetfpetServiceAgent.TypeDemandeDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value, 
            { status: this.form.get('status').value ? TypeDemandeStatus.Active : TypeDemandeStatus.Inactive }
          )))
        }else{
          saveTypeDemande$ = this._metfpetService.updateTypeDemande(MetfpetServiceAgent.TypeDemandeDTO.fromJS(
            Object.assign({}, 
              this.model, 
              this.form.value, 
              { status: this.form.get('status').value ? TypeDemandeStatus.Active : TypeDemandeStatus.Inactive }
            )))
        }
        saveTypeDemande$.subscribe(() => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
         )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }

   uploadLogo() {
     document.getElementById('logoSelector').click();
  }

  onFileUpload(event) {
    if (event.target.files && event.target.files.length > 0) {
      this._store.dispatch(showLoading());

      let file : Blob = event.target.files[0];
      let uploadedFileName : string = event.target.files[0].name;
      var fileReader = new FileReader();
      fileReader.onloadend = function () {
        
      }
      fileReader.readAsBinaryString(file);
      
      let fileParameter = 
      {
        data : file,
        fileName : uploadedFileName
      };

      this._metfpetService.uploadTypeDemandeLogo(fileParameter, this.data.id)
        .subscribe(
          (logoUrl) => {
            this.form.patchValue({
              logoUrl: logoUrl
            });
            this._cd.markForCheck();
            this._store.dispatch(showSuccess({}));
          },
          (error) => this._store.dispatch(showError({message: error.message}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

}
