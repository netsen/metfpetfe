import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  PersonnelMemberStatus,
} from 'MetfpetLib';
import { Guid } from 'guid-typescript';
import { Observable } from 'rxjs';
import * as internal from 'stream';

@Component({
  templateUrl: './section.dialog.html',
})
export class SectionDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.SectionListDTO;
  employeRoleList: Array<MetfpetServiceAgent.EmployeRoleDTO>;
  error: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<SectionDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.title = 'Gérer les sections de document';
    this.employeRoleList = data.employeRoleList;
    this.model = data.sectionList;

    this.form = this._formBuilder.group({
      agrementationId: data.agrementationId,
      sections: this._formBuilder.array([]),
    });
  }

  ngOnInit() {
    if (this.model.sections) {
      for (var section of this.model.sections) {
        this.sections.push(this._formBuilder.group({
          id: section.id,
          name: section.name,
          employeRoleId: section.employeRoleId
        }));
      }
    }
    this._cd.markForCheck();
  }

  get sections() {
    return this.form.controls['sections'] as FormArray;
  }

  addSection() {
    this.sections.push(this._formBuilder.group({
      id: Guid.EMPTY,
      name: null,
      employeRoleId: null
    }));
    this._cd.markForCheck();
  }

  allowAddSection(): boolean {
    return this.sections.value.length < 5;
  }

  delete(i: number) {
    this.sections.removeAt(i);
    this._cd.markForCheck();
  }

  onConfirm() {
    this.error = null;
    validateForm(this.form);
    if (this.form.valid && this._validateSections()) {

      this._store.dispatch(showLoading());
      this.model = Object.assign({}, this.model, this.form.value);

      this.model.sections = this.sections.value
        .filter(x => !!x.name && !!x.employeRoleId)
        .map(x => MetfpetServiceAgent.SectionDTO.fromJS(x));
      
      this._metfpetService.updateSectionList(MetfpetServiceAgent.SectionListDTO.fromJS(this.model))
        .subscribe(
              () => {
                this._store.dispatch(showSuccess({}));
                this._dialogRef.close(true);
              },
              error => this._store.dispatch(showException({error: error}))
            )
            .add(() => this._store.dispatch(hideLoading()));
    }
  }

  private _validateSections() {
    var choices1 = this.sections.value.filter(x => !x.name || !x.employeRoleId);
    if (choices1.length > 0) {
      this.error = 'Certains champs obligatoires manquent';
    }

    var choices2 = this.sections.value.filter(x => !!x.name).map(x => x.name);
    if (new Set(choices2).size !== choices2.length) {
      this.error = 'Les sections sont dupliquées';
    }

    return !this.error;
  }

  onClose() {
    this._dialogRef.close();
  }
}