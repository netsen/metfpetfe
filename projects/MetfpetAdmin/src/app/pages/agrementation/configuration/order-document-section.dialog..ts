import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  DialogService,
} from 'MetfpetLib';

@Component({
  templateUrl: './order-document-section.dialog.html',
  styleUrls: ['./order-document-section.dialog.css']
})
export class OrderDocumentSectionDialog {

  title: string;
  form: FormGroup;
  sections: Array<MetfpetServiceAgent.SectionDTO>;
  documentSections: MetfpetServiceAgent.DocumentSectionDTO[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<OrderDocumentSectionDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
    private _dialogService: DialogService,
  ) {
    this.title = 'Gérer l’ordre des documents';
    this.sections = data.sections;
    this.form = this._formBuilder.group({
      sectionId: [null, Validators.required],
    });
  }

  ngOnInit() {
    if (this.sections && this.sections.length > 0) {
      this.form.patchValue({
        sectionId: this.sections[0].id
      });
    }
    this.loadDocumentSections();
  }

  drop(event: CdkDragDrop<MetfpetServiceAgent.DocumentSectionDTO[]>) {
    moveItemInArray(this.documentSections, event.previousIndex, event.currentIndex);
  }

  moveDown(i: number) {
    moveItemInArray(this.documentSections, i, i + 1);
  }

  moveUp(i: number) {
    moveItemInArray(this.documentSections, i, i - 1);
  }

  allowMoveDown(i: number): boolean {
    return i < this.documentSections.length - 1;
  }

  allowMoveUp(i: number): boolean {
    return i >= 1;
  }

  onSectionChange() {
    if (this.documentSections && this.documentSections.length > 1) {
      this._dialogService.openConfirmDialog({
        width: '420px',
        data: {
          title: "Attention",
          message: "Voulez-vous enregistrer avant de modifier la section du document?",
          confirmBtnText: 'Oui',
          cancelBtnText: 'Non'
        }
      }).afterClosed().subscribe(confirmed => {
        if (confirmed) {
          this.onConfirm();
        } else {
          this.loadDocumentSections();
        }
      });
    } else {
      this.loadDocumentSections();
    }
  }

  loadDocumentSections() {
    this._metfpetService.getDocumentSectionsOfSection(this.form.get("sectionId").value).subscribe(data => {
      this.documentSections = data;
      this._cd.markForCheck();
    });
  }


  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.orderDocumentSections(this.documentSections).subscribe(
        () => {
          this._store.dispatch(showSuccess({}));
          this._dialogRef.close(true);
          this.loadDocumentSections();
        },
        error => this._store.dispatch(showException({ error: error }))
      )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}