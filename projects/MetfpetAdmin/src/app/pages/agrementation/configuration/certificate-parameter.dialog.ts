import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';

@Component({
  templateUrl: './certificate-parameter.dialog.html',
})
export class CertificateParameterDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.AgrementationDTO;
  typeEcoleList: Array<any>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<CertificateParameterDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.model = data.model;
    this.title = 'Paramètres du certificat d’agrément';
    this.typeEcoleList = [
      {value : 'Nom de l\'institution', name : 'Nom de l\'institution'},
      {value : 'Nom de l\'institut', name : 'Nom de l\'institut'},
      {value : 'Nom du centre', name : 'Nom du centre'},
    ];

    this.form = this._formBuilder.group({
      certificateTitleColor: null,
      certificateTitle: null,
      hasCertificateSubTitle: true,
      certificateSubTitle: null,
      hasCertificateMedicalSpecialty: true,
      certificateMedicalSpecialty: null,
      hasCertificateService: true,
      certificateService: null,
      hasCertificateEncadre: true,
      certificateEncadre: null,
      hasCertificateTexteDeLoi: true,
      certificateTexteDeLoi: null,
      hasCertificateNoteTexteDeLoi: true,
      certificateNoteTexteDeLoi: null,
      typeEcole: null,
      hasCertificateLieuImplantation: true,
      hasCertificateNiveauRecrutement: true,
      hasCertificateDiplomeAttendu: true,
      hasCertificateFiliereFormation: true,
      hasCertificateTypeAgrementVerso: true,
      typeAgrementVerso: null,
    });

    this.form.patchValue({
      certificateTitleColor: this.model.certificateTitleColor,
      certificateTitle: this.model.certificateTitle,
      hasCertificateSubTitle: this.model.hasCertificateSubTitle,
      certificateSubTitle: this.model.certificateSubTitle,
      hasCertificateMedicalSpecialty: this.model.hasCertificateMedicalSpecialty,
      certificateMedicalSpecialty: this.model.certificateMedicalSpecialty,
      hasCertificateService: this.model.hasCertificateService,
      certificateService: this.model.certificateService,
      hasCertificateEncadre: this.model.hasCertificateEncadre,
      certificateEncadre: this.model.certificateEncadre,
      hasCertificateTexteDeLoi: this.model.hasCertificateTexteDeLoi,
      certificateTexteDeLoi: this.model.certificateTexteDeLoi,
      hasCertificateNoteTexteDeLoi: this.model.hasCertificateNoteTexteDeLoi,
      certificateNoteTexteDeLoi: this.model.certificateNoteTexteDeLoi,
      typeEcole: this.model.typeEcole,
      hasCertificateLieuImplantation: this.model.hasCertificateLieuImplantation,
      hasCertificateNiveauRecrutement: this.model.hasCertificateNiveauRecrutement,
      hasCertificateDiplomeAttendu: this.model.hasCertificateDiplomeAttendu,
      hasCertificateFiliereFormation: this.model.hasCertificateFiliereFormation,
      hasCertificateTypeAgrementVerso: this.model.hasCertificateTypeAgrementVerso,
      typeAgrementVerso: this.model.typeAgrementVerso,
    });
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this.model.certificateTitleColor = this.form.get('certificateTitleColor').value;
      this.model.certificateTitle = this.form.get('certificateTitle').value;
      this.model.hasCertificateSubTitle = this.form.get('hasCertificateSubTitle').value;
      this.model.certificateSubTitle = this.form.get('certificateSubTitle').value;
      this.model.hasCertificateMedicalSpecialty = this.form.get('hasCertificateMedicalSpecialty').value;
      this.model.certificateMedicalSpecialty = this.form.get('certificateMedicalSpecialty').value;
      this.model.hasCertificateService = this.form.get('hasCertificateService').value;
      this.model.certificateService = this.form.get('certificateService').value;
      this.model.hasCertificateEncadre = this.form.get('hasCertificateEncadre').value;
      this.model.certificateEncadre = this.form.get('certificateEncadre').value;
      this.model.hasCertificateTexteDeLoi = this.form.get('hasCertificateTexteDeLoi').value;
      this.model.certificateTexteDeLoi = this.form.get('certificateTexteDeLoi').value;
      this.model.hasCertificateNoteTexteDeLoi = this.form.get('hasCertificateNoteTexteDeLoi').value;
      this.model.certificateNoteTexteDeLoi = this.form.get('certificateNoteTexteDeLoi').value;
      this.model.typeEcole = this.form.get('typeEcole').value;
      this.model.hasCertificateLieuImplantation = this.form.get('hasCertificateLieuImplantation').value;
      this.model.hasCertificateNiveauRecrutement = this.form.get('hasCertificateNiveauRecrutement').value;
      this.model.hasCertificateDiplomeAttendu = this.form.get('hasCertificateDiplomeAttendu').value;
      this.model.hasCertificateFiliereFormation = this.form.get('hasCertificateFiliereFormation').value;
      this.model.hasCertificateTypeAgrementVerso = this.form.get('hasCertificateTypeAgrementVerso').value;
      this.model.typeAgrementVerso = this.form.get('typeAgrementVerso').value;
      this._metfpetService.updateAgrementation(MetfpetServiceAgent.AgrementationDTO.fromJS(this.model)).subscribe(
        () => {
          this._store.dispatch(showSuccess({}));
          this._dialogRef.close(this.model);
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}