import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { 
  AppSettings,
  Settings,
  MetfpetServiceAgent,
  PerfectScrollService,
  DialogService,
  showLoading,
  showSuccess,
  hideLoading,
  showError,
} from 'MetfpetLib';
import { Tabs } from './configuration-tab.component';
import { DemandeurDocumentDialog } from './demandeur-document.dialog';

@Component({
  templateUrl: './demandeur-document.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DemandeurDocumentComponent {

  settings: Settings;
  title: string;
  navs = Tabs;
  agrementationDocumentList: Array<MetfpetServiceAgent.AgrementationDocumentViewModel>;
  model: MetfpetServiceAgent.DemandeurDocumentListDTO;
  demandeurDocuments: Array<MetfpetServiceAgent.DemandeurDocumentDTO>;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialog: MatDialog,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
  ) {
    this.settings = this.appSettings.settings;
    this.title = 'Document de création de compte';
  }

  ngOnInit() {
    this._metfpetService.getAgrementationDocumentListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.agrementationDocumentList = data.results;
      this._cd.markForCheck();
    });
    this.getDemandeurDocuments();
  }

  getDemandeurDocuments() {
    this._metfpetService.getAllDemandeurDocuments().subscribe(data => {
      this.model = data;
      this.demandeurDocuments = this.model.demandeurDocuments;
      this._cd.markForCheck();
    });
  }

  editDemandeurDocuments() {
    this._dialog.open(DemandeurDocumentDialog, {
      width: '700px',
      data: {
        agrementationDocumentList: this.agrementationDocumentList,
        model: this.model
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.getDemandeurDocuments();
      }
    });
  }

  deleteDemandeurDocument(id: string) {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention", 
        message: "Voulez-vous définitivement supprimer ce document de création de compte ?", 
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteDemandeurDocument(id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({message: 'Le document de création de compte a été supprimée'}));
              this.getDemandeurDocuments();
            },
            (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }
}