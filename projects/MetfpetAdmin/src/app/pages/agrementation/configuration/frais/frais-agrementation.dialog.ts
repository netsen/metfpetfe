import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  TarificationStatus,
} from 'MetfpetLib';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './frais-agrementation.dialog.html',
})
export class FraisAgrementationDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.AgrementationTarificationDTO;
  agrementationList: Array<MetfpetServiceAgent.AgrementationViewModel>;
  isCreation: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<FraisAgrementationDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.agrementationList = data.agrementationList;
    this.form = this._formBuilder.group({
      agrementationId: [null, Validators.required],
      montant: [null, Validators.required],
      status: true,
    });

    if (data.agrementationTarificationId) {
      this.title = 'Modifier un frais d’agrémentation';
      this._metfpetService.getAgrementationTarification(data.agrementationTarificationId).subscribe(
        (agrementationTarification) => {
          this.model = agrementationTarification;
          this.form.patchValue({
            agrementationId: this.model.agrementationId,
            montant: this.model.montant,
            status: this.model.status === <any>TarificationStatus.Active,
          });
        }
      );
      this.isCreation = false;
    } 
    else 
    {
      this.title = 'Nouveau frais d’agrémentation';
      this.isCreation = true;
    }    
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      
      this.model = Object.assign({}, this.model, this.form.value, 
        { status: this.form.get('status').value ? TarificationStatus.Active : TarificationStatus.Inactive }
      );

      var saveAgrementationTarification$: Observable<MetfpetServiceAgent.AgrementationTarificationDTO>;

      if (this.isCreation) {
        saveAgrementationTarification$ = this._metfpetService.createAgrementationTarification(MetfpetServiceAgent.AgrementationTarificationDTO.fromJS(this.model));
      } else {
        saveAgrementationTarification$ = this._metfpetService.updateAgrementationTarification(MetfpetServiceAgent.AgrementationTarificationDTO.fromJS(this.model));
      }

      saveAgrementationTarification$.subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}