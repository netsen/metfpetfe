import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { 
  AppSettings,
  Settings,
  BaseTableComponent, 
  MetfpetServiceAgent,
  PerfectScrollService,
  showLoading,
  showSuccess,
  showError,
  hideLoading,
  DialogService,
} from 'MetfpetLib';
import { FraisAgrementationDialog } from './frais-agrementation.dialog';


@Component({
  templateUrl: './frais-agrementation.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FraisAgrementationComponent extends BaseTableComponent {

  settings: Settings;
  title: string;
  agrementationList: Array<MetfpetServiceAgent.AgrementationViewModel>;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialog: MatDialog,
    private _dialogService: DialogService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Frais d’agrémentations';
    this.sort = {prop: 'name', dir: 'desc'};
  }

  ngOnInit() {
    this._metfpetService.getAgrementationListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.agrementationList = data.results;
      this._cd.markForCheck();
    });
  }

  ngAfterViewInit() {
    this.triggerSearch();
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getAgrementationTarificationListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  edit(row: MetfpetServiceAgent.AgrementationTarificationRowViewModel) {
    this._dialog.open(FraisAgrementationDialog, {
      width: '600px',
      data: {
        agrementationList: this.agrementationList,
        agrementationTarificationId: row.id
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  add() {
    this._dialog.open(FraisAgrementationDialog, {
      width: '600px',
      data: {
        agrementationList: this.agrementationList,
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  delete(row: MetfpetServiceAgent.AgrementationTarificationRowViewModel) {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention", 
        message: "Voulez-vous définitivement supprimer ce frais d’agrémentation ?", 
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteAgrementationTarification(row.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({message: 'Le frais d’agrémentation a été supprimée'}));
              this.triggerSearch();
            },
            (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }
}