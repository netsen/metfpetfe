import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../../shared/shared.module';
import { FraisAgrementationComponent } from './frais-agrementation.component';
import { FraisAgrementationDialog } from './frais-agrementation.dialog';

export const routes = [
  { 
    path: 'frais-agrementations', 
    component: FraisAgrementationComponent, 
    data: { breadcrumb: 'Frais d’agrémentations' },
    pathMatch: 'full' 
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [
    FraisAgrementationComponent,
    FraisAgrementationDialog,
  ]
})
export class FraisModule {}
