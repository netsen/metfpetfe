import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { startWith, debounceTime, distinctUntilChanged } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent, 
  MetfpetServiceAgent,
  PerfectScrollService,
  ChampTypeValues,
  DialogService,
  showLoading,
  showSuccess,
  showError,
  hideLoading,
} from 'MetfpetLib';
import { Tabs } from './configuration-tab.component';
import { MatDialog } from '@angular/material/dialog';
import { ValeurDialog } from './valeur.dialog';

@Component({
  templateUrl: './valeurs-formulaire.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ValeursFormulaireComponent extends BaseTableComponent {
  
  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchDescription', {static: true}) searchDescription: ElementRef;

  settings: Settings;
  title: string;
  navs = Tabs;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialog: MatDialog,
    private _dialogService: DialogService,
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Configuration - Valeurs champs de formulaires';
    this.sort = {prop: 'name', dir: 'desc'};
  }

  ngAfterViewInit() {
    combineLatest([
      fromEvent(
        this.searchName.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('name').value}
            })
          ),
      fromEvent(
        this.searchDescription.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('description').value}
            })
          ),
         this.searchForm.get('isActive').valueChanges.pipe(startWith(this.searchForm.get('isActive').value)),
    ])
    .subscribe(([
      eventName, 
      eventDescription,
      isActive
    ]) => {
      this.searchForm.patchValue({
        name: eventName ? eventName['target'].value : null, 
        description: eventDescription ? eventDescription['target'].value : null,
        isActive
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null,
      description: null,
      isActive: true
    });
  }

  protected _search(criteria: any): Observable<any> {
    // to convert True/false value to number 1/0
    criteria.filters.isActive = +criteria.filters.isActive;
    return this._metfpetService.getValeurListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  open(row: MetfpetServiceAgent.ValeurViewModel) {
    this._dialog.open(ValeurDialog, {
      width: '600px',
      data: {
        id: row.id
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  addValeur() {
    this._dialog.open(ValeurDialog, {
      width: '600px',
      data: {
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  delete(row: MetfpetServiceAgent.ValeurViewModel) {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention", 
        message: "Voulez-vous définitivement supprimer ce valeur ?", 
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteValeur(row.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({message: 'Le valeur a été supprimée'}));
              this.triggerSearch();
            },
            (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }
}
