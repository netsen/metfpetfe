import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  ChampStatus,
  ChampTypeValues,
} from 'MetfpetLib';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './champ.dialog.html',
})
export class ChampDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.ChampDTO;
  typeList: Array<any>;
  isCreation: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<ChampDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.typeList = ChampTypeValues;

    this.form = this._formBuilder.group({
      name: [null, Validators.required],
      status: true,
      type: [null, Validators.required],
    });

    if (data.id) {
      this.title = 'Modifier un champ';
      this.isCreation = false;
      this._metfpetService.getChamp(data.id).subscribe(
        (champ) => {
          this.model = champ;
          this.form.patchValue({
            name: this.model.name,
            type: this.model.type,
            status: this.model.status === <any>ChampStatus.Active,
          });
        }
      );
    } else {
      this.title = 'Ajouter un champ';
      this.isCreation = true;
    }
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveChamp$: Observable<MetfpetServiceAgent.ChampDTO>;

      if (this.isCreation) {
        saveChamp$ = this._metfpetService.createChamp(MetfpetServiceAgent.ChampDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value, 
            { status: this.form.get('status').value ? ChampStatus.Active : ChampStatus.Inactive }
          )));
      } else {
        saveChamp$ = this._metfpetService.updateChamp(MetfpetServiceAgent.ChampDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value, 
            { status: this.form.get('status').value ? ChampStatus.Active : ChampStatus.Inactive }
          )));
      }
      
      saveChamp$.subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}