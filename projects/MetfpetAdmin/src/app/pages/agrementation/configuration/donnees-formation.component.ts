import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
  ViewEncapsulation,
  ElementRef
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest, of } from 'rxjs';
import { startWith, debounceTime, distinctUntilChanged } from 'rxjs/operators/';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  MetfpetServiceAgent,
  PerfectScrollService,
  DialogService,
  showLoading,
  showSuccess,
  showError,
  hideLoading,
  TypeDataFormation
} from 'MetfpetLib';
import { Tabs } from './configuration-tab.component';
import { MatDialog } from '@angular/material/dialog';

interface DataFormationViewModel {
  typeName: string,
  values: string,
}

@Component({
  templateUrl: './donnees-formation.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class DonneesFormationComponent implements OnInit {

  settings: Settings;
  title: string;
  navs = Tabs;
  loadingIndicator: boolean;
  langues: MetfpetServiceAgent.LangueListDTO;
  niveauEtudes: MetfpetServiceAgent.NiveauEtudeListDTO;
  niveauAcces: MetfpetServiceAgent.NiveauAccesListDTO;
  typeAdmissions: MetfpetServiceAgent.TypeAdmissionListDTO;
  typeDisciplines: MetfpetServiceAgent.TypeDisciplineListDTO;
  typeProgrammes: MetfpetServiceAgent.TypeProgrammeListDTO;
  listOfDataFormation: Array<DataFormationViewModel>;
  size: number;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialog: MatDialog,
    private _dialogService: DialogService,
  ) {
    this.settings = this.appSettings.settings;
    this.title = 'Données de formation';
    this.loadingIndicator = true;

  }

  ngOnInit(): void {
    this.listOfDataFormation = [];
    this.loadNiveauAccess();
    this.loadTypeAdmissions();
    this.loadTypeProgrammes();
    this.loadTypeDisciplines();
    this.loadNiveauEtudes();
    this.loadTypeDiplomes();
    this.loadLangues();
  }

  loadNiveauAccess() {
    this._metfpetService.getNiveauAccesListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.listOfDataFormation.push({
        typeName: TypeDataFormation.NiveauAcces,
        values: data.results.filter(x => x.name != 'Autre').map(x => x.name).join('; ')
      });
      this.listOfDataFormation = [...this.listOfDataFormation];
      this._cd.markForCheck();
    });
  }

  loadTypeAdmissions() {
    this._metfpetService.getTypeAdmissionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.listOfDataFormation.push({
        typeName: TypeDataFormation.TypeAdmission,
        values: data.results.map(x => x.name).join('; ')
      });
      this.listOfDataFormation = [...this.listOfDataFormation];
      this._cd.markForCheck();
    });
  }

  loadTypeProgrammes() {
    this._metfpetService.getTypeProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.listOfDataFormation.push({
        typeName: TypeDataFormation.TypeProgramme,
        values: data.results.map(x => x.name).join('; ')
      });
      this.listOfDataFormation = [...this.listOfDataFormation];
      this._cd.markForCheck();
    });
  }

  loadTypeDisciplines() {
    this._metfpetService.getTypeDisciplineListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.listOfDataFormation.push({
        typeName: TypeDataFormation.TypeDiscipline,
        values: data.results.map(x => x.name).join('; ')
      });
      this.listOfDataFormation = [...this.listOfDataFormation];
      this._cd.markForCheck();
    });
  }

  loadNiveauEtudes() {
    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.listOfDataFormation.push({
        typeName: TypeDataFormation.NiveauEtude,
        values: data.results.map(x => x.name).join('; ')
      });
      this.listOfDataFormation = [...this.listOfDataFormation];
      this._cd.markForCheck();
    });
  }

  loadTypeDiplomes() {
    this._metfpetService.getTypeDiplomeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.listOfDataFormation.push({
        typeName: TypeDataFormation.TypeDiplome,
        values: data.results.map(x => x.name).join('; ')
      });
      this.listOfDataFormation = [...this.listOfDataFormation];
      this._cd.markForCheck();
    });
  }

  loadLangues() {
    this._metfpetService.getLangueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.listOfDataFormation.push({
        typeName: TypeDataFormation.Langues,
        values: data.results.map(x => x.name).join('; ')
      });
      this.listOfDataFormation = [...this.listOfDataFormation];
      this._cd.markForCheck();
    });
  }
}
