import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularCounterModule } from 'angular-input-counter';
import { ConfigurationTabComponent } from './configuration-tab.component';
import { ChampsFormulaireComponent } from './champs-formulaire.component';
import { ValeursFormulaireComponent } from './valeurs-formulaire.component';
import { ChampDialog } from './champ.dialog';
import { ValeurDialog } from './valeur.dialog';
import { ChampValeurDialog } from './champ-valeur.dialog';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { SharedModule } from '../../../shared/shared.module';
import { AgrementationDocumentsComponent } from './agrementation-document.component';
import { AgrementationDocumentDialog } from './agrementation-document.dialog';
import { TypeDemandeComponent } from './type-demande.component';
import { TypeDemandeDialog } from './type-demande.dialog';
import { AgrementsComponent } from './agrements.component';
import { DocumentTemplatesComponent } from './agrementation-document-templates.component';
import { DocumentTemplateDialog } from './agrementation-document-templates.dialog';
import { CreateAgrementDialog } from './create-agrement.dialog';
import { AgrementDetailComponent } from './agrement-detail.component';
import { FormulaireSectionDialog } from './formulaire-section.dialog';
import { OrderFormulaireSectionChampDialog } from './order-formulaire-section-champ.dialog';
import { FormulaireSectionChampDialog } from './formulaire-section-champ.dialog';
import { SelectTemplateDialog } from './select-template.dialog';
import { CertificateParameterDialog } from './certificate-parameter.dialog';
import { ValidityPeriodDialog } from './validity-period.dialog';
import { PersonnelDocumentDialog } from './personnel-document.dialog';
import { PersonnelMemberDialog } from './personnel-member.dialog';
import { AgrementPrealableDialog } from './agrement-prealable.dialog';
import { ProcessStepValidatorDialog } from './process-step-validator.dialog';
import { ProcessStepDialog } from './process-step.dialog';
import { DemandeurDocumentComponent } from './demandeur-document.component';
import { DemandeurDocumentDialog } from './demandeur-document.dialog';
import { DocumentSectionDialog } from './document-section.dialog';
import { OrderDocumentSectionDialog } from './order-document-section.dialog.';
import { SectionDialog } from './section.dialog.';
import { DonneesFormationComponent } from './donnees-formation.component';
import { RoutesByPermissionsConfiguration } from 'MetfpetLib';
import { NgxPermissionsGuard } from 'ngx-permissions';

export const routes = [
  {
    path: '',
    component: ConfigurationTabComponent,
    children:
      [
        {
          path: '',
          redirectTo: 'agrements',
          pathMatch: 'full'
        },
        {
          path: 'agrements',
          component: AgrementsComponent,
          data: { breadcrumb: 'Agréments' }
        },
        {
          path: 'agrements/view/:id',
          component: AgrementDetailComponent,
          data: { breadcrumb: 'Agréments' }
        },
        {
          path: 'documents',
          component: AgrementationDocumentsComponent,
          data: { breadcrumb: 'Documents' }
        },
        {
          path: 'champs',
          component: ChampsFormulaireComponent,
          data: { breadcrumb: 'Champs de formulaire' }
        },
        {
          path: 'valeurs',
          component: ValeursFormulaireComponent,
          data: { breadcrumb: 'Valeurs champs de formulaires' }
        },
        {
          path: 'type-demandes',
          component: TypeDemandeComponent,
          data: { breadcrumb: 'Types de demande' }
        },
        {
          path: 'document-template',
          component: DocumentTemplatesComponent,
          data: { breadcrumb: 'Document template' }
        },
        {
          path: 'demandeur-documents',
          component: DemandeurDocumentComponent,
          data: { breadcrumb: 'Document de création de compte' }
        },
        {
          path: 'donnees-formation',
          component: DonneesFormationComponent,
          data: { breadcrumb: 'Données de formation' }
        },
        {
          path: 'import',
          loadChildren: () => import('../../import-agrementations/import-agrementations.module').then(m => m.ImportAgrementationsModule),
          data: {
            permissions: {
              only: RoutesByPermissionsConfiguration['admin.agrementation'],
              redirectTo: 'login'
            }
          },
          canActivate: [NgxPermissionsGuard]
        },
      ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AngularCounterModule,
    DragDropModule
  ],
  declarations: [
    ConfigurationTabComponent,
    ChampsFormulaireComponent,
    ValeursFormulaireComponent,
    AgrementationDocumentsComponent,
    AgrementsComponent,
    AgrementDetailComponent,
    ChampDialog,
    ValeurDialog,
    ChampValeurDialog,
    AgrementationDocumentDialog,
    TypeDemandeComponent,
    TypeDemandeDialog,
    DocumentTemplatesComponent,
    DocumentTemplateDialog,
    CreateAgrementDialog,
    FormulaireSectionDialog,
    OrderFormulaireSectionChampDialog,
    FormulaireSectionChampDialog,
    SelectTemplateDialog,
    CertificateParameterDialog,
    ValidityPeriodDialog,
    PersonnelDocumentDialog,
    PersonnelMemberDialog,
    AgrementPrealableDialog,
    ProcessStepValidatorDialog,
    ProcessStepDialog,
    DemandeurDocumentComponent,
    DemandeurDocumentDialog,
    DocumentSectionDialog,
    OrderDocumentSectionDialog,
    SectionDialog,
    DonneesFormationComponent,
  ]
})
export class AgrementationConfigurationModule { }
