import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  AppSettings,
  Settings,
  MetfpetServiceAgent,
  PerfectScrollService,
  AgrementationStatus,
  PersonnelType,
  DialogService,
  showLoading,
  showSuccess,
  showError,
  hideLoading,
  validateForm,
  showException,
} from 'MetfpetLib';
import { MatDialog } from '@angular/material/dialog';
import { Location } from '@angular/common';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { FormulaireSectionDialog } from './formulaire-section.dialog';
import { OrderFormulaireSectionChampDialog } from './order-formulaire-section-champ.dialog';
import { FormulaireSectionChampDialog } from './formulaire-section-champ.dialog';
import { SelectTemplateDialog } from './select-template.dialog';
import { CertificateParameterDialog } from './certificate-parameter.dialog';
import { ValidityPeriodDialog } from './validity-period.dialog';
import { PersonnelDocumentDialog } from './personnel-document.dialog';
import { PersonnelMemberDialog } from './personnel-member.dialog';
import { AgrementPrealableDialog } from './agrement-prealable.dialog';
import { UiPath } from '../../ui-path';
import { ProcessStepValidatorDialog } from './process-step-validator.dialog';
import { ProcessStepDialog } from './process-step.dialog';
import { DocumentSectionDialog } from './document-section.dialog';
import { OrderDocumentSectionDialog } from './order-document-section.dialog.';
import { SectionDialog } from './section.dialog.';


@Component({
  templateUrl: './agrement-detail.component.html',
  styleUrls: ['./agrement-detail.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AgrementDetailComponent implements OnInit {
  documentTemplate: any;
  form: FormGroup;
  settings: Settings;
  title: string;
  agrementId: string;
  displayAfterInputDisabled: boolean;
  disableInputIfModification: boolean;
  typeDemandeList: Array<MetfpetServiceAgent.TypeDemandeViewModel>;
  documentList: Array<MetfpetServiceAgent.AgrementationDocumentViewModel>;
  agrementationDocumentTemplateList: Array<MetfpetServiceAgent.AgrementationDocumentTemplateViewModel>;
  personnelDocumentList: Array<MetfpetServiceAgent.PersonnelDocumentDTO>;
  administratifPersonnelMemberList: Array<MetfpetServiceAgent.PersonnelMemberDTO>;
  formateurPersonnelMemberList: Array<MetfpetServiceAgent.PersonnelMemberDTO>;
  processStepList: Array<MetfpetServiceAgent.ProcessStepDTO>;
  sectionWidth: number = 25;
  formulaireSectionWidth: number = 25;
  public _model: MetfpetServiceAgent.AgrementationDTO;
  formulaireSectionChampList: Array<MetfpetServiceAgent.FormulaireSectionChampDTO>;
  formulaireSections: Array<MetfpetServiceAgent.FormulaireSectionDTO>;
  formulaireSectionList: MetfpetServiceAgent.FormulaireSectionListDTO;
  employeRoleList: Array<MetfpetServiceAgent.EmployeRoleDTO>;
  champList: Array<MetfpetServiceAgent.ChampViewModel>;
  targetAgrementations: Array<MetfpetServiceAgent.AgrementationInfo>;
  agrementPrealableViewModel: MetfpetServiceAgent.AgrementPrealableViewModel;
  agrementationInfos: Array<MetfpetServiceAgent.AgrementationInfo>;
  sections: Array<MetfpetServiceAgent.SectionDTO>;
  documentSectionList: Array<MetfpetServiceAgent.DocumentSectionDTO>;
  sectionList: MetfpetServiceAgent.SectionListDTO;
  hasDocuments: boolean;
  hasPersonnel: boolean;
  hasAgrementPrealable: boolean;
  hasFormulaire: boolean;
  hasProgramme: boolean;
  hasPaymentReceipt: boolean;
  impactConfigurationInstitution: boolean;
  validator5: string = "Refusée";
  demandeur: string = "En attente de soumission";
  lastIndex: any;
  isModification: boolean;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    private _location: Location,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    private _route: ActivatedRoute,
    protected _store: Store<any>,
    private _dialog: MatDialog,
    private _dialogService: DialogService,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings;
    this.title = 'Configuration - Agrémentation';
    this.form = this._formBuilder.group({
      name: [null, Validators.required],
      typeDemandeId: [null, Validators.required],
      status: true,
      hasDocuments: true,
      hasPersonnel: true,
      hasAgrementPrealable: true,
      requirePayment: true,
      hasFormulaire: true,
      hasProgrammesFormation: true,
      requireApprovalPersonnel: null,
      requireApprovalSociete: null,
      impactConfigurationInstitution: null,
      hasPaymentReceipt: false,
      isCenter: null,
    });
  }

  ngOnInit() {
    this._metfpetService.getTypeDemandeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.typeDemandeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getEmployeRoleListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { 'withDemandeurRole': true }
    })).subscribe(data => {
      this.employeRoleList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getChampListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.champList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAgrementationDocumentListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.documentList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAgrementationDocumentTemplateListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.agrementationDocumentTemplateList = data.results;
      this._cd.markForCheck();
    });

    this.getAgrementation()
  }

  changeForModification(name: string) {
    this.isModification = name === 'Modification à un dossier';
    if (this.isModification) {
      this.hasDocuments = false;
      this.hasPersonnel = false;
      this.hasFormulaire = false;
      this.form.patchValue({
        hasDocuments: this.hasDocuments,
        hasPersonnel: this.hasPersonnel,
        hasFormulaire: this.hasFormulaire,
      });
      this._cd.markForCheck();
    }
  }

  getAgrementation() {
    this._route.params.subscribe(params => {
      this.agrementId = params['id'];
      if (this.agrementId) {
        this._metfpetService.getAgrementation(this.agrementId).subscribe(data => {
          this._model = data;
          if (this._model.agrementationDocumentTemplateId) {
            this.insertDocumentTemplateData();
          }
          this.hasDocuments = this._model.hasDocuments;
          this.hasPersonnel = this._model.hasPersonnel;
          this.hasAgrementPrealable = this._model.hasAgrementPrealable;
          this.hasFormulaire = this._model.hasFormulaire;
          this.isModification = this._model.isModification;
          this.hasProgramme = this._model.hasProgrammesFormation;
          this.hasPaymentReceipt = this._model.hasPaymentReceipt;
          this.impactConfigurationInstitution = this._model.impactConfigurationInstitution;
          this.form.patchValue({
            name: this._model.name,
            typeDemandeId: this._model.typeDemandeId,
            status: this._model.status === <any>AgrementationStatus.Active,
            hasDocuments: this._model.hasDocuments,
            hasPersonnel: this._model.hasPersonnel,
            hasAgrementPrealable: this._model.hasAgrementPrealable,
            hasProgrammesFormation: this._model.hasProgrammesFormation,
            requirePayment: this._model.requirePayment,
            hasFormulaire: this._model.hasFormulaire,
            hasPaymentReceipt: this._model.hasPaymentReceipt,
            requireApprovalPersonnel: this._model.requireApprovalPersonnel,
            requireApprovalSociete: this._model.requireApprovalSociete,
            impactConfigurationInstitution: this._model.impactConfigurationInstitution,
            isCenter: this._model.isCenter,
          });
          this._cd.markForCheck();
        });
        this.loadFormulaireSectionChamps();
        this.loadFormulaireSections();
        this.loadPersonnelDocuments();
        this.loadAdministratifPersonnelMembers();
        this.loadFormateurPersonnelMembers();
        this.loadAgrementationInfos();
        this.loadAgrementPrealable();
        this.loadProcessSteps();
        this.loadSections();
        this.loadDocumentSections();
      }
    });
  }

  editDocumentSection(row: MetfpetServiceAgent.DocumentSectionDTO) {
    this._dialog.open(DocumentSectionDialog, {
      width: '600px',
      data: {
        sections: this.sections,
        documentList: this.documentList,
        model: row
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.loadDocumentSections();
      }
    });
  }

  deleteDocumentSection(row: MetfpetServiceAgent.DocumentSectionDTO) {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention",
        message: "Voulez-vous définitivement supprimer ce document ?",
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteDocumentSection(row.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({ message: 'Le document a été supprimée' }));
              this.loadDocumentSections();
            },
            (error) => this._store.dispatch(showError({ message: error.response }))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  addDocumentSection() {
    this._dialog.open(DocumentSectionDialog, {
      width: '600px',
      data: {
        sections: this.sections,
        documentList: this.documentList
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.loadDocumentSections();
      }
    });
  }

  orderDocumentSection() {
    this._dialog.open(OrderDocumentSectionDialog, {
      width: '800px',
      data: {
        sections: this.sections,
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.loadDocumentSections();
      }
    });
  }

  editSections() {
    this._dialog.open(SectionDialog, {
      width: '700px',
      data: {
        agrementationId: this._model.id,
        employeRoleList: this.employeRoleList,
        sectionList: this.sectionList
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.loadSections();
        this.loadDocumentSections();
      }
    });
  }


  editProcessus() {
    this._dialog.open(ProcessStepDialog, {
      width: '510px',
      data: {
        agrementationId: this._model.id,
        processStepList: this.processStepList
      }

    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {

        this.loadProcessSteps();
      }
    });
  }

  deleteProcessStep(processStep: MetfpetServiceAgent.ProcessStepDTO) {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention",
        message: "Voulez-vous définitivement supprimer ce étape ?",
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteProcessStep(processStep.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({ message: 'Le étape a été supprimée' }));
              this.loadProcessSteps();
            },
            (error) => this._store.dispatch(showError({ message: error.response }))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  editProcessStepValidator(processStep: MetfpetServiceAgent.ProcessStepDTO) {
    this._dialog.open(ProcessStepValidatorDialog, {
      width: '1050px',
      data: {
        model: processStep,
        employeRoleList: this.employeRoleList
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.loadProcessSteps();
      }
    });
  }

  dateFormat(date: Date) {
    return date.toLocaleString('fr-FR', { day: '2-digit', month: '2-digit', year: 'numeric' });
  }

  expiracy(validityPeriod: number, timeUnit: number) {
    var today = new Date();
    switch (timeUnit) {
      case 1:
        today.setSeconds(today.getSeconds() + validityPeriod).toLocaleString()
        return this.dateFormat(today);
      case 2:
        today.setMinutes(today.getMinutes() + validityPeriod).toLocaleString()
        return this.dateFormat(today);
      case 3:
        today.setHours(today.getHours() + validityPeriod).toLocaleString()
        return this.dateFormat(today);
      case 4:
        today.setDate(today.getDate() + validityPeriod).toLocaleString()
        return this.dateFormat(today);
      case 5:
        today.setTime(today.getTime() + (validityPeriod * 7 * 24 * 60 * 60 * 1000)).toLocaleString()
        return this.dateFormat(today);
      case 6:
        today.setMonth(today.getMonth() + validityPeriod).toLocaleString()
        return this.dateFormat(today);
      case 7:
        today.setFullYear(today.getFullYear() + validityPeriod).toLocaleString()
        return this.dateFormat(today);
      default:
        break;
    }
  }

  getValidity(validityPeriod: number, timeUnit: number) {
    switch (timeUnit) {
      case 1:
        if (validityPeriod > 1) {
          return validityPeriod + ' secondes';
        }
        return validityPeriod + ' seconde';
      case 2:
        if (validityPeriod > 1) {
          return validityPeriod + ' minutes';
        }
        return validityPeriod + ' minute';
      case 3:
        if (validityPeriod > 1) {
          return validityPeriod + ' heures';
        }
        return validityPeriod + ' heure';
      case 4:
        if (validityPeriod > 1) {
          return validityPeriod + ' jours';
        }
        return validityPeriod + ' jour';
      case 5:
        if (validityPeriod > 1) {
          return validityPeriod + ' semaines';
        }
        return validityPeriod + ' semaine';
      case 6:
        return validityPeriod + ' mois';
      case 7:
        if (validityPeriod > 1) {
          return validityPeriod + ' années';
        }
        return validityPeriod + ' année';
      default:
        break;
    }
  }

  insertDocumentTemplateData() {
    if (this._model.documentTemplateHtml) {
      this.documentTemplate = document.getElementById("HtmlTemplate");
      this.documentTemplate.innerHTML = this._model.documentTemplateHtml;
      let certificateTitle_recto = document.getElementById("certificateTitle_recto");
      let certificateSubTitle_recto = document.getElementById("certificateSubTitle_recto");
      let certificateTitle_verso = document.getElementById("certificateTitle_verso");
      let certificateSubTitle_verso = document.getElementById("certificateSubTitle_verso");
      let certificateTexteDeLoi = document.getElementById("certificateTexteDeLoi");
      let certificateNoteTexteDeLoi = document.getElementById("certificateNoteTexteDeLoi");
      let expiracyDate = document.getElementById("expiracy");
      let deliveryDate = document.getElementById("delivery");
      certificateTitle_recto.innerText = this._model.certificateTitle;
      certificateSubTitle_recto.innerText = this._model.certificateSubTitle;
      certificateTitle_verso.innerText = this._model.certificateTitle;
      certificateSubTitle_verso.innerText = this._model.certificateSubTitle;
      certificateTexteDeLoi.innerText = this._model.certificateTexteDeLoi;
      certificateNoteTexteDeLoi.innerText = this._model.certificateNoteTexteDeLoi;

      certificateTitle_recto.style.color = `#${this._model.certificateTitleColor}`;
      certificateTitle_verso.style.color = `#${this._model.certificateTitleColor}`;

      var today = new Date();
      deliveryDate.innerText = today.toLocaleString('fr-FR', { day: '2-digit', month: '2-digit', year: 'numeric' });
      expiracyDate.innerText = this.expiracy(this._model.certificateValidityPeriod, this._model.timeUnit);

      let typeAgrement = document.getElementById("type-agrement");
      if (typeAgrement) {
        if (this._model.hasCertificateTypeAgrementVerso) {
          typeAgrement.hidden = false;
          typeAgrement.innerText = 'Type : ' + this._model.typeAgrementVerso;
        }
        else {
          typeAgrement.hidden = true;
        }
      }
      let labelInstitution = document.getElementById("label-institution");
      if (labelInstitution && this._model.typeEcole) {
        labelInstitution.innerText = this._model.typeEcole + ' : ';
      }
      let physicalLocation = document.getElementById("physical-location");
      if (physicalLocation) {
        physicalLocation.hidden = !this._model.hasCertificateLieuImplantation;
      }
      let recruitmentLevel = document.getElementById("recruitment-level");
      if (recruitmentLevel) {
        recruitmentLevel.hidden = !this._model.hasCertificateNiveauRecrutement;
      }
      let diploma = document.getElementById("diploma");
      if (diploma) {
        diploma.hidden = !this._model.hasCertificateDiplomeAttendu;
      }
      let trainingCourse = document.getElementById("training-course");
      if (trainingCourse) {
        trainingCourse.hidden = !this._model.hasCertificateFiliereFormation;
      }
      let validity = document.getElementById("validity");
      if (validity) {
        if (this._model.certificateValidityPeriod && this._model.timeUnit) {
          validity.innerText = 'Validité du document : ' + this.getValidity(this._model.certificateValidityPeriod, this._model.timeUnit) + ' à compter de la date d’émission';
        }
        else {
          validity.innerText = 'Validité du document : 1 année(s) à compter de la date d’émission';
        }
      }
    }
  }

  loadDocumentSections() {
    this._metfpetService.getDocumentSectionsOfAgrementation(this.agrementId).subscribe(data => {
      this.documentSectionList = data;
      this._cd.markForCheck();
    });
  }

  loadSections() {
    this._metfpetService.getSectionsOfAgrementation(this.agrementId).subscribe(data => {
      this.sectionList = data;
      this.sections = this.sectionList.sections;
      if (this.sections && this.sections.length > 0) {
        if (this.sections.length >= 4) {
          this.sectionWidth = 25;
        } else {
          this.sectionWidth = 100 / this.sections.length;
        }
      }
      this._cd.markForCheck();
    });
  }

  loadPersonnelDocuments() {
    this._metfpetService.getPersonnelDocumentsOfAgrementation(this.agrementId).subscribe(data => {
      this.personnelDocumentList = data;
      this._cd.markForCheck();
    });
  }

  loadAdministratifPersonnelMembers() {
    this._metfpetService.getPersonnelMemberListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {
        'agrementationId': this.agrementId,
        'personnelType': PersonnelType.Administratif
      }
    })).subscribe(data => {
      this.administratifPersonnelMemberList = data.results;
      this._cd.markForCheck();
    });
  }

  loadFormateurPersonnelMembers() {
    this._metfpetService.getPersonnelMemberListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {
        'agrementationId': this.agrementId,
        'personnelType': PersonnelType.Formateur
      }
    })).subscribe(data => {
      this.formateurPersonnelMemberList = data.results;
      this._cd.markForCheck();
    });
  }

  goBack() {
    this._location.back();
  }

  onHasDocumentsToggle(event: MatSlideToggleChange) {
    this.hasDocuments = event.checked;
  }

  onHasPersonnelToggle(event: MatSlideToggleChange) {
    this.hasPersonnel = event.checked;
  }

  onHasAgrementPrealableToggle(event: MatSlideToggleChange) {
    this.hasAgrementPrealable = event.checked;
  }

  onHasFormulaireToggle(event: MatSlideToggleChange) {
    this.hasFormulaire = event.checked;
  }

  onHasProgrammesFormationToggle(event: MatSlideToggleChange) {
    this.hasProgramme = event.checked;
  }

  onHasPaymentReceiptToggle(event: MatSlideToggleChange) {
    this.hasPaymentReceipt = event.checked;
  }

  onImpactConfigurationInstitutionToggle(event: MatSlideToggleChange)
  {
    this.impactConfigurationInstitution = event.checked;
    if (this.impactConfigurationInstitution)
    {
      this.form.patchValue({
        isCenter: false,
      });
    }
    this._cd.markForCheck();
  }

  uploadLogo() {
    document.getElementById('logoSelector').click();
  }

  disabledForModification() {
    this.disableInputIfModification = this._model.typeDemandeName === 'Modification à un dossier';
    this.displayAfterInputDisabled = true;
    this._cd.markForCheck();
  }

  loadProcessSteps() {
    this._metfpetService.getProcessStepsOfAgrementation(this.agrementId).subscribe(data => {
      this.processStepList = data;
      this._cd.markForCheck();
    });
  }

  loadFormulaireSections() {
    this._metfpetService.getFormulaireSectionsOfAgrementation(this.agrementId).subscribe(data => {
      this.formulaireSectionList = data;
      this.formulaireSections = this.formulaireSectionList.formulaireSections;
      if (this.formulaireSections && this.formulaireSections.length > 0) {
        if (this.formulaireSections.length >= 4) {
          this.formulaireSectionWidth = 25;
        } else {
          this.formulaireSectionWidth = 100 / this.formulaireSections.length;
        }
      }
      this._cd.markForCheck();
    });
  }

  loadFormulaireSectionChamps() {
    this._metfpetService.getFormulaireSectionChampsOfAgrementation(this.agrementId).subscribe(data => {
      this.formulaireSectionChampList = data;
      this._cd.markForCheck();
    });
  }

  loadAgrementPrealable() {
    this._metfpetService.getAgrementPrealable(this.agrementId).subscribe(data => {
      this.agrementPrealableViewModel = data;
      this.targetAgrementations = this.agrementPrealableViewModel.targetAgrementations;
      this._cd.markForCheck();
    });
  }

  loadAgrementationInfos() {
    this._metfpetService.getAgrementationInfos(this.agrementId).subscribe(data => {
      this.agrementationInfos = data;
      this._cd.markForCheck();
    });
  }

  onSubmit() {
    validateForm(this.form);
    if (this.form.valid) {
      this._model = Object.assign({}, this._model, this.form.value,
        { status: this.form.get('status').value ? AgrementationStatus.Active : AgrementationStatus.Inactive });
      this._store.dispatch(showLoading());
      this._metfpetService.updateAgrementation(MetfpetServiceAgent.AgrementationDTO.fromJS(this._model)).subscribe(
        () => {
          this._store.dispatch(showSuccess({}));
          this._router.navigate([UiPath.admin.configuration.agrements.list]);
        },
        error => this._store.dispatch(showException({ error: error }))
      )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onDelete() {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention",
        message: "Voulez-vous définitivement supprimer ce agrément ?",
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteAgrementation(this._model.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({ message: 'Le agrément a été supprimée' }));
              this._router.navigate([UiPath.admin.configuration.agrements.list]);
            },
            (error) => this._store.dispatch(showError({ message: error.response }))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  deleteAgrementPrealable(targetAgrementation: MetfpetServiceAgent.AgrementationInfo) {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention",
        message: "Voulez-vous définitivement supprimer ce agrément préalable ?",
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        let dto = new MetfpetServiceAgent.AgrementPrealableDTO();
        dto.sourceAgrementationId = this.agrementId;
        dto.targetAgrementationId = targetAgrementation.id;
        this._metfpetService.deleteAgrementPrealable(dto)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({ message: 'Le agrément préalable a été supprimée' }));
              this.loadAgrementPrealable();
            },
            (error) => this._store.dispatch(showError({ message: error.response }))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  editAgrementPrealable() {
    this._dialog.open(AgrementPrealableDialog, {
      width: '600px',
      data: {
        agrementationInfos: this.agrementationInfos,
        model: this.agrementPrealableViewModel
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.loadAgrementPrealable();
      }
    });
  }

  editFormulaireSections() {
    this._dialog.open(FormulaireSectionDialog, {
      width: '700px',
      data: {
        agrementationId: this._model.id,
        employeRoleList: this.employeRoleList,
        formulaireSectionList: this.formulaireSectionList
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.loadFormulaireSections();
        this.loadFormulaireSectionChamps();
      }
    });
  }

  orderFormulaireSectionChamp() {
    this._dialog.open(OrderFormulaireSectionChampDialog, {
      width: '800px',
      data: {
        formulaireSections: this.formulaireSections,
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.loadFormulaireSectionChamps();
      }
    });
  }

  addFormulaireSectionChamp() {
    this._dialog.open(FormulaireSectionChampDialog, {
      width: '600px',
      data: {
        champList: this.champList,
        formulaireSections: this.formulaireSections
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.loadFormulaireSectionChamps();
      }
    });
  }

  editFormulaireSectionChamp(row: MetfpetServiceAgent.FormulaireSectionChampDTO) {
    this._dialog.open(FormulaireSectionChampDialog, {
      width: '600px',
      data: {
        model: row,
        champList: this.champList,
        formulaireSections: this.formulaireSections
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.loadFormulaireSectionChamps();
      }
    });
  }

  deleteFormulaireSectionChamp(row: MetfpetServiceAgent.FormulaireSectionChampDTO) {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention",
        message: "Voulez-vous définitivement supprimer ce champ de formulaire ?",
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteFormulaireSectionChamp(row.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({ message: 'Le champ de formulaire a été supprimée' }));
              this.loadFormulaireSectionChamps();
            },
            (error) => this._store.dispatch(showError({ message: error.response }))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  public onFileUpload(event) {
    if (event.target.files && event.target.files.length > 0) {
      this._store.dispatch(showLoading());

      let file: Blob = event.target.files[0];
      let uploadedFileName: string = event.target.files[0].name;
      var fileReader = new FileReader();
      fileReader.onloadend = function () {

      }
      fileReader.readAsBinaryString(file);

      let fileParameter =
      {
        data: file,
        fileName: uploadedFileName
      };

      this._metfpetService.uploadAgrementationLogo(fileParameter, this._model.id)
        .subscribe(
          (logoUrl) => {

            this._model.logoUrl = logoUrl;

            this._cd.markForCheck();
            this._store.dispatch(showSuccess({}));
          },
          (error) => this._store.dispatch(showError({ message: error.message }))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  selectTemplate() {
    this._dialog.open(SelectTemplateDialog, {
      width: '600px',
      data: {
        model: this._model,
        agrementationDocumentTemplateList: this.agrementationDocumentTemplateList
      }
    }).afterClosed().subscribe(model => {
      if (model) {
        this._model = model;
        this.getAgrementation();
      }
    });
  }

  editParameter() {
    this._dialog.open(CertificateParameterDialog, {
      width: '800px',
      data: {
        model: this._model
      }
    }).afterClosed().subscribe(model => {
      if (model) {
        this._model = model;
        this.getAgrementation();
      }
    });
  }

  addValidityPeriod() {
    this._dialog.open(ValidityPeriodDialog, {
      data: {
        model: this._model
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.getAgrementation();
      }
    });
  }

  addPersonnelDocument() {
    this._dialog.open(PersonnelDocumentDialog, {
      width: '600px',
      data: {
        agrementationId: this._model.id,
        documentList: this.documentList
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.loadPersonnelDocuments();
      }
    });
  }

  editPersonnelDocument(row: MetfpetServiceAgent.PersonnelDocumentDTO) {
    this._dialog.open(PersonnelDocumentDialog, {
      width: '600px',
      data: {
        id: row.id,
        agrementationId: this._model.id,
        documentList: this.documentList
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.loadPersonnelDocuments();
      }
    });
  }

  deletePersonnelDocument(row: MetfpetServiceAgent.PersonnelDocumentDTO) {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention",
        message: "Voulez-vous définitivement supprimer ce document ?",
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deletePersonnelDocument(row.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({ message: 'Le document a été supprimée' }));
              this.loadPersonnelDocuments();
            },
            (error) => this._store.dispatch(showError({ message: error.response }))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  addPersonnelMember(isAdministratif: boolean) {
    this._dialog.open(PersonnelMemberDialog, {
      width: '600px',
      data: {
        agrementationId: this._model.id,
        personnelDocumentList: this.documentList,
        isAdministratif: isAdministratif
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.reloadPersonnelMembers(isAdministratif);
      }
    });
  }

  editPersonnelMember(row: MetfpetServiceAgent.PersonnelMemberDTO) {
    let isAdministratif = row.personnelType == <any>PersonnelType.Administratif;
    this._dialog.open(PersonnelMemberDialog, {
      width: '600px',
      data: {
        id: row.id,
        agrementationId: this._model.id,
        personnelDocumentList: this.documentList,
        isAdministratif: isAdministratif
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.reloadPersonnelMembers(isAdministratif);
      }
    });
  }

  deletePersonnelMember(row: MetfpetServiceAgent.PersonnelMemberDTO) {
    let isAdministratif = row.personnelType == <any>PersonnelType.Administratif;
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention",
        message: "Voulez-vous définitivement supprimer ce membre du personnel ?",
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deletePersonnelMember(row.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({ message: 'Le membre du personnel a été supprimée' }));
              this.reloadPersonnelMembers(isAdministratif);
            },
            (error) => this._store.dispatch(showError({ message: error.response }))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  reloadPersonnelMembers(isAdministratif: boolean) {
    if (isAdministratif) {
      this.loadAdministratifPersonnelMembers();
    }
    else {
      this.loadFormateurPersonnelMembers();
    }
  }
}
