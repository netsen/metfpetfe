import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  TimeUnitsValues,
} from 'MetfpetLib';

@Component({
  templateUrl: './validity-period.dialog.html',
})
export class ValidityPeriodDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.AgrementationDTO;
  timeUnitsList = TimeUnitsValues;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<ValidityPeriodDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.model = data.model;
    this.title = 'Durée de validité de l’agrément';

    this.form = this._formBuilder.group({
      certificateValidityPeriod: [null, Validators.compose([Validators.required, Validators.min(1)])],
      timeUnit: [null, Validators.required]
    });

    this.form.patchValue({
      certificateValidityPeriod: this.model.certificateValidityPeriod,
      timeUnit: this.model.timeUnit,
    });
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this.model = Object.assign({}, this.model, this.form.value);
      this._metfpetService.updateAgrementation(MetfpetServiceAgent.AgrementationDTO.fromJS(this.model)).subscribe(
        () => {
          this._store.dispatch(showSuccess({}));
          this._dialogRef.close(this.model);
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}
