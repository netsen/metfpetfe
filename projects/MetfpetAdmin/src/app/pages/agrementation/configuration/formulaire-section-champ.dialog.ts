import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  FormulaireSectionChampStatus,
} from 'MetfpetLib';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-formulaire-section-champdialog',
  templateUrl: './formulaire-section-champ.dialog.html',

})
export class FormulaireSectionChampDialog implements OnInit {

  title: string;
  form: FormGroup;
  id: string;
  model: MetfpetServiceAgent.FormulaireSectionChampDTO;
  champList: Array<MetfpetServiceAgent.ChampViewModel>;
  formulaireSections: Array<MetfpetServiceAgent.FormulaireSectionDTO>;
  isCreation: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<FormulaireSectionChampDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.champList = data.champList;
    this.formulaireSections = data.formulaireSections;

    this.form = this._formBuilder.group({
      status: true,
      formulaireSectionId: [null, Validators.required],
      champId: [null, Validators.required],
    });
    
    if (data.model) {
      this.model = data.model;
      this.title = 'Modifier un champ de formulaire';
      this.isCreation = false;
    } else {
      this.title = 'Ajouter un champ de formulaire';
      this.isCreation = true;
    }
  }

  ngOnInit() {
    if (!this.isCreation && this.model) {
      this.form.patchValue({
        status: this.model.status === <any>FormulaireSectionChampStatus.Active,
        formulaireSectionId: this.model.formulaireSectionId,
        champId: this.model.champId,
      });
    }
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveFormulaireSectionChamp$: Observable<MetfpetServiceAgent.FormulaireSectionChampDTO>;

      if (this.isCreation) {
        saveFormulaireSectionChamp$ = this._metfpetService.createFormulaireSectionChamp(MetfpetServiceAgent.FormulaireSectionChampDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value, 
            { status: this.form.get('status').value ? FormulaireSectionChampStatus.Active : FormulaireSectionChampStatus.Inactive }
          )));
      } else {
        saveFormulaireSectionChamp$ = this._metfpetService.updateFormulaireSectionChamp(MetfpetServiceAgent.FormulaireSectionChampDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value, 
            { status: this.form.get('status').value ? FormulaireSectionChampStatus.Active : FormulaireSectionChampStatus.Inactive }
          )));
      }
      
      saveFormulaireSectionChamp$.subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}
