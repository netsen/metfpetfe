import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  AgrementationStatus,
} from 'MetfpetLib';
@Component({
  templateUrl: './create-agrement.dialog.html',
})
export class CreateAgrementDialog {

  title: string;
  isModification: boolean;
  form: FormGroup;
  model: MetfpetServiceAgent.AgrementationDTO;
  typeDemandeList: Array<MetfpetServiceAgent.TypeDemandeViewModel>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<CreateAgrementDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.title = 'Ajouter un agrément';
    this.typeDemandeList = data.typeDemandeList;

    this.form = this._formBuilder.group({
      name: [null, Validators.required],
      typeDemandeId: [null, Validators.required],
      hasDocuments: true,
      hasPersonnel: true,
      hasAgrementPrealable: true,
      hasProgrammesFormation: true,
      requirePayment: true,
      hasFormulaire: true,
      impactConfigurationInstitution:true,
    });
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this.model = MetfpetServiceAgent.AgrementationDTO.fromJS(
        Object.assign({}, 
          this.model, 
          this.form.value, 
          { status: AgrementationStatus.Inactive}
        ));
      if (this.isModification) {
        this.model.isModification = true;
        this.model.hasDocuments = false;
        this.model.hasPersonnel = false;
        this.model.hasFormulaire = false;
      } else {
        this.model.isModification = false;
      }
      this._metfpetService.createAgrementation(this.model).subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }

  changeForModification(name: string){
    this.isModification = name === 'Modification à un dossier' || name === 'Modifications à un dossier';
  }
}