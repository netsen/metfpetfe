import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { startWith, debounceTime, distinctUntilChanged } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent, 
  MetfpetServiceAgent,
  PerfectScrollService,
} from 'MetfpetLib';
import { Tabs } from './configuration-tab.component';
import { MatDialog } from '@angular/material/dialog';
import { CreateAgrementDialog } from './create-agrement.dialog';

@Component({
  templateUrl: './agrements.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AgrementsComponent extends BaseTableComponent {
  
  @ViewChild('searchName', {static: true}) searchName: ElementRef;

  settings: Settings;
  title: string;
  typeDemandeList: Array<MetfpetServiceAgent.TypeDemandeViewModel>;
  navs = Tabs;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    private _route: ActivatedRoute,
    protected _store: Store<any>,
    private _dialog: MatDialog,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Configuration - Agrémentation';
    this.sort = {prop: 'name', dir: 'desc'};

    this._metfpetService.getTypeDemandeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {
        isActive: "1"
      }
    })).subscribe(data => {
      this.typeDemandeList = data.results;
      this._cd.markForCheck();
    });
  }

  ngAfterViewInit() {
    combineLatest([
      fromEvent(
        this.searchName.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('name').value}
            })
          ),
      this.searchForm.get('typeDemande').valueChanges.pipe(startWith(this.searchForm.get('typeDemande').value)),
      this.searchForm.get('isActive').valueChanges.pipe(startWith(this.searchForm.get('isActive').value)),
    ])
    .subscribe(([
      eventName,
      typeDemande,
      isActive
    ]) => {
      this.searchForm.patchValue({
        name: eventName ? eventName['target'].value : null, 
        typeDemande,
        isActive
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      name: null,
      typeDemande: null,
      isActive: true,
    });
  }

  protected _search(criteria: any): Observable<any> {
    // to convert True/false value to number 1/0
    criteria.filters.isActive = +criteria.filters.isActive;
    return this._metfpetService.getAgrementationListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  open(row: MetfpetServiceAgent.AgrementationViewModel) {
    this._router.navigate([`view/${row.id}`], { relativeTo: this._route });
  }

  addAgrement() {
    this._dialog.open(CreateAgrementDialog, {
      width: '1000px',
      data: {
        typeDemandeList: this.typeDemandeList
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }
}
