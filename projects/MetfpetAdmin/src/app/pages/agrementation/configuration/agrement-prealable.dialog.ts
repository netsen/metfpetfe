import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';

@Component({
  templateUrl: './agrement-prealable.dialog.html',
})
export class AgrementPrealableDialog {

  form: FormGroup;
  model: MetfpetServiceAgent.AgrementPrealableViewModel;
  agrementationInfos: Array<MetfpetServiceAgent.AgrementationInfo>;
  id: string;
  error: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<AgrementPrealableDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.agrementationInfos = data.agrementationInfos;
    this.model = data.model;

    this.form = this._formBuilder.group({
      agrementPrealables: this._formBuilder.array([]),
    });

    if (this.model && this.model.targetAgrementations) {
      for (var item of this.model.targetAgrementations) {
        this.agrementPrealables.push(this._formBuilder.group({
          id: item.id, 
        }));
      }
      this._cd.markForCheck();
    }
  }

  get agrementPrealables() {
    return this.form.controls['agrementPrealables'] as FormArray;
  }

  addAgrementPrealable() {
    this.agrementPrealables.push(this._formBuilder.group({
      id: null, 
    }));
    this._cd.markForCheck();
  }

  delete(i: number) {
    this.agrementPrealables.removeAt(i);
    this._cd.markForCheck();
  }

  onConfirm() {
    this.error = null;
    if (this._validateAgrementPrealables()) {

      this._store.dispatch(showLoading());
      
      this.model.targetAgrementations = this.agrementPrealables.value.filter(x => !!x.id).map(x => MetfpetServiceAgent.AgrementationInfo.fromJS(x));
    
      this._metfpetService.updateAgrementPrealable(MetfpetServiceAgent.AgrementPrealableViewModel.fromJS(this.model)).subscribe(
            () => {
              this._store.dispatch(showSuccess({}));
              this._dialogRef.close(true);
            },
            error => this._store.dispatch(showException({error: error}))
          )
          .add(() => this._store.dispatch(hideLoading()));
    }
  }

  private _validateAgrementPrealables() {
    var choices = this.agrementPrealables.value.filter(x => !!x.id).map(x => x.id);

    if (new Set(choices).size !== choices.length) {
      this.error = 'Les agrément préalable sont dupliquées';
    }

    return !this.error;
  }

  onClose() {
    this._dialogRef.close();
  }
}
