import { ThrowStmt } from '@angular/compiler';
import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  TypeDemandeStatus,
  ReferenceTemplateStatus,
} from 'MetfpetLib';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './agrementation-document.dialog.html',
})
export class AgrementationDocumentDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.AgrementationDocumentDTO;
  fileName: string = '';
  hasFile: boolean = false;
  isCreation: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<AgrementationDocumentDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.form = this._formBuilder.group({
      name: [null, Validators.required],
      maxSize: [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])],
      maxNumberOfFiles: [1, Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])],
      status: true,
      referenceTemplateStatus: true,
      isPdf: false,
      isJpg: false,
      isJpeg: false,
      isPng: false
    });

    if (data.id) {
      this.isCreation = false;
      this.title = 'Modifier un document';
      this._metfpetService.getAgrementationDocument(data.id).subscribe(
        (documentTemplate) => {
          this.model = documentTemplate;
          this.form.patchValue({
            name: this.model.name,
            maxSize: this.model.maxSize,
            maxNumberOfFiles: this.model.maxNumberOfFiles,
            status: this.model.status === <any>TypeDemandeStatus.Active,
            referenceTemplateStatus: this.model.referenceTemplateStatus === <any>ReferenceTemplateStatus.Oui,
            isPdf: this.model.acceptedFormat && this.model.acceptedFormat.includes('pdf'),
            isJpg: this.model.acceptedFormat && this.model.acceptedFormat.includes('jpg'),
            isJpeg: this.model.acceptedFormat && this.model.acceptedFormat.includes('jpeg'),
            isPng: this.model.acceptedFormat && this.model.acceptedFormat.includes('png'),
          });
          if (this.model.referenceTemplateFileName) 
          {
            this.fileName = this.model.referenceTemplateFileName;
            this.hasFile = true;
            this._cd.detectChanges();
          }
        }
      );
    } else {
      this.isCreation = true;
      this.title = 'Ajouter un document';  
    }
  }

  getAcceptedFormat() {
    let acceptedFormat = '';
    if (this.form.get('isPdf').value) {
      acceptedFormat = acceptedFormat + '.pdf,';
    }
    if (this.form.get('isJpg').value) {
      acceptedFormat = acceptedFormat + '.jpg,';
    }
    if (this.form.get('isJpeg').value) {
      acceptedFormat = acceptedFormat + '.jpeg,';
    }
    if (this.form.get('isPng').value) {
      acceptedFormat = acceptedFormat + '.png,';
    }
    if (acceptedFormat.endsWith(',')) {
      acceptedFormat = acceptedFormat.slice(0, acceptedFormat.length - 1);
    }
    return acceptedFormat;
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveDocument$: Observable<MetfpetServiceAgent.AgrementationDocumentDTO>;
      
      if (this.isCreation) {
        saveDocument$ = this._metfpetService.createAgrementationDocument(MetfpetServiceAgent.AgrementationDocumentDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value, 
            { status: this.form.get('status').value ? TypeDemandeStatus.Active : TypeDemandeStatus.Inactive },
            { referenceTemplateStatus: this.form.get('referenceTemplateStatus').value ? ReferenceTemplateStatus.Oui : ReferenceTemplateStatus.Non },
            { acceptedFormat: this.getAcceptedFormat() }
          )));
      } else {
        saveDocument$ = this._metfpetService.updateAgrementationDocument(MetfpetServiceAgent.AgrementationDocumentDTO.fromJS(
          Object.assign({}, 
            this.model, 
            this.form.value, 
            { status: this.form.get('status').value ? TypeDemandeStatus.Active : TypeDemandeStatus.Inactive },
            { referenceTemplateStatus: this.form.get('referenceTemplateStatus').value ? ReferenceTemplateStatus.Oui : ReferenceTemplateStatus.Non },
            { acceptedFormat: this.getAcceptedFormat() }
          )));
      }
      
      saveDocument$.subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }

  uploadFile() {
    document.getElementById('fileSelector').click();
  }

  deleteFile() {
    this._store.dispatch(showLoading());
    this._metfpetService.deleteAgrementationDocumentReferenceTemplate(this.model.id)
        .subscribe(
          () => {
            this.fileName = '';
            this.hasFile = false;
            this._cd.markForCheck();
            this._store.dispatch(showSuccess({}));
          },
          (error) => this._store.dispatch(showError({message: error.message}))
        )
        .add(() => this._store.dispatch(hideLoading()));
  }

  public onFileUpload(event) {
    if (event.target.files && event.target.files.length > 0) {
      this._store.dispatch(showLoading());

      let file : Blob = event.target.files[0];
      let uploadedFileName : string = event.target.files[0].name;
      var fileReader = new FileReader();
      fileReader.onloadend = function () {
        
      }
      fileReader.readAsBinaryString(file);
      
      let fileParameter = 
      {
        data : file,
        fileName : uploadedFileName
      };

      this._metfpetService.uploadAgrementationDocumentReferenceTemplate(fileParameter, this.model.id)
        .subscribe(
          (fileName) => {
            this.fileName = fileName;
            this.hasFile = true;
            this._cd.markForCheck();
            this._store.dispatch(showSuccess({}));
          },
          (error) => this._store.dispatch(showError({message: error.message}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }
}