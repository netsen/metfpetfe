import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  DocumentTemplateStatus,
} from 'MetfpetLib';
import { DTemplateStatus } from 'projects/MetfpetLib/src/lib/interfaces/metfpet.model';

@Component({
  templateUrl: './agrementation-document-templates.dialog.html',
})
export class DocumentTemplateDialog implements OnInit {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.AgrementationDocumentTemplateDTO;
  documentContent: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<DocumentTemplateDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.form = this._formBuilder.group({
      name: [null, Validators.required],
      status: [null],
    });

    this.title = 'Modèle de document';

    this._metfpetService.getAgrementationDocumentTemplate(data.id).subscribe(
      (documentTemplate) => {
        this.model = documentTemplate;
        this.form.patchValue({
          name: this.model.name,
          status: this.model.status === <any>DTemplateStatus.Active,
        });
      }
    );

  }

  ngOnInit(): void {
    this.documentContent = document.getElementById('documentContent')
    this.documentContent.innerHTML = this.data.HtmlTemplate
  }  

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this._metfpetService.updateAgrementationDocumentTemplate(MetfpetServiceAgent.AgrementationDocumentTemplateDTO.fromJS(
        Object.assign({}, 
          this.model, 
          this.form.value, 
          { status: this.form.get('status').value ? DTemplateStatus.Active : DTemplateStatus.Inactive }
        )))
        .subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._dialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }

  
}