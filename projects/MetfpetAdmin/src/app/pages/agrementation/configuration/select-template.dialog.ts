import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  validateForm,
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
} from 'MetfpetLib';

@Component({
  templateUrl: './select-template.dialog.html',
})
export class SelectTemplateDialog {

  title: string;
  form: FormGroup;
  model: MetfpetServiceAgent.AgrementationDTO;
  agrementationDocumentTemplateList: Array<MetfpetServiceAgent.AgrementationDocumentTemplateViewModel>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<SelectTemplateDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _cd: ChangeDetectorRef,
  ) {
    this.agrementationDocumentTemplateList = data.agrementationDocumentTemplateList;
    this.model = data.model;
    this.title = 'Sélection du template';

    this.form = this._formBuilder.group({
      templateId: [null, Validators.required]
    });

    this.form.patchValue({
      templateId: this.model.agrementationDocumentTemplateId,
    });
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this.model.agrementationDocumentTemplateId = this.form.get('templateId').value;
      this._metfpetService.updateAgrementation(MetfpetServiceAgent.AgrementationDTO.fromJS(this.model)).subscribe(
        () => {
          this._store.dispatch(showSuccess({}));
          this._dialogRef.close(this.model);
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}