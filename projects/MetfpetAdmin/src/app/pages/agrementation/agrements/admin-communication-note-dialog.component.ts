import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Component, 
  ViewEncapsulation,
	Inject, OnInit
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { 
  validateForm,
  showSuccess,
  MetfpetServiceAgent,
  showLoading,
  showException,
  showError,
  hideLoading,
} from 'MetfpetLib';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './admin-communication-note-dialog.component.html',
	encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminCommunicationNoteDialogComponent {

  form: FormGroup;
  title: string; 
  loading: boolean;
  fileName: string = 'Aucun fichier';
  hasFile: boolean = false;
  isCreation: boolean;
  model: MetfpetServiceAgent.CommunicationNoteDTO;
  employeRoles: Array<MetfpetServiceAgent.EmployeRoleDTO>;
  fileLink: string;
  

  constructor(
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _matDialogRef: MatDialogRef<AdminCommunicationNoteDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.form = this._formBuilder.group({
			object: [null, [Validators.required, Validators.maxLength(255)]],
      message: [null, [Validators.required, Validators.maxLength(2500)]],
      employeRoleIds : [null, Validators.required]
    });
    
    if (data.id) {
      this.isCreation = false;
      this.title = 'Modifier communication';
      this._metfpetService.getCommunicationNote(data.id).subscribe((data)=>{
        this.model = data;
        this.form.patchValue({
          object: this.model.object,
          message: this.model.message,
          employeRoleIds : this.model?.employeRoleIds
        })
        if (this.model.attachedFileName) {
            this.fileName = this.model.attachedFileName;
            this.fileLink = this.model.attachedFileLink;
            this.hasFile = true;  
            this._cd.detectChanges();
        }
      })
    } else {
      this.isCreation = true;
      this.title = 'Nouvelle communication';
      this.model = data;
    }
	}

  ngOnInit(){
    this.loadEmploRoles()
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      this.model = Object.assign({},
        this.model,
        this.form.value, 
      );   

      var saveCommunicationNote$: Observable<MetfpetServiceAgent.CommunicationNoteDTO>;

      if (this.isCreation) {
        saveCommunicationNote$ = this._metfpetService.createCommunicationNote(MetfpetServiceAgent.CommunicationNoteDTO.fromJS(this.model));
      } else {
        saveCommunicationNote$ = this._metfpetService.updateCommunicationNote(MetfpetServiceAgent.CommunicationNoteDTO.fromJS(this.model));
      }

      saveCommunicationNote$.subscribe(
        () => {
            this._store.dispatch(showSuccess({}));
            this._matDialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  loadEmploRoles(){
    this._metfpetService.getEmployeRoleWithDemandeurListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { status : 1 }
    })).subscribe((res)=>{
      this.employeRoles = res.results
    })
  }
  
  uploadFile() {
    document.getElementById('attachedFileSelector').click();
  }

  deleteFile() {
    this._store.dispatch(showLoading());
    this._metfpetService.deleteAttachedFile(this.model.id)
      .subscribe(
        () => {
          this.fileName = 'Aucun fichier';
          this.hasFile = false;
          this._cd.markForCheck();
          this._store.dispatch(showSuccess({}));
        },
        (error) => this._store.dispatch(showError({message: error.message}))
      )
      .add(() => this._store.dispatch(hideLoading()));
  }

	public onFileUpload(event) {
    if (event.target.files && event.target.files.length > 0) {
      this._store.dispatch(showLoading());

      let file : Blob = event.target.files[0];
      let uploadedFileName : string = event.target.files[0].name;
      var fileReader = new FileReader();
      fileReader.onloadend = function () {
        
      }
      fileReader.readAsBinaryString(file);
      
      let fileParameter = 
      {
        data : file,
        fileName : uploadedFileName
      };

      this._metfpetService.uploadAttachedFile(fileParameter, this.model.id)
        .subscribe(
          (attachedFileName) => {
            this.fileName = attachedFileName;
            this.hasFile = true;
            this._cd.markForCheck();
            this._store.dispatch(showSuccess({}));
          },
          (error) => this._store.dispatch(showError({message: error.message}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._matDialogRef.close()
  }
}
