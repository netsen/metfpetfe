import { 
  Component, 
	Inject, OnInit
} from '@angular/core';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-admin-communication-readonly.dialog',
  templateUrl: './admin-communication-readonly.dialog.component.html',
  styleUrls: ['./admin-communication-readonly.dialog.component.css']
})
export class AdminCommunicationReadonlyDialog implements OnInit {
  messageContent: string;
  messageSubject: string;
  attachedFileLink: string;
  attachedFileName: string;
  title: string;

  constructor(
    private _matDialogRef: MatDialogRef<AdminCommunicationReadonlyDialog>, 
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 
    this.title = "Message"
  }

  ngOnInit(): void {
    this.messageContent = this.data.messageContent;
    this.messageSubject = this.data.messageSubject;
    this.attachedFileLink = this.data.attachedFileLink;
    this.attachedFileName = this.data.attachedFileName;
  }

  onClose() {
    this._matDialogRef.close()
  }

}
