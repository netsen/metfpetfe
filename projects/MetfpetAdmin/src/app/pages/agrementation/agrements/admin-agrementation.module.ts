import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { AdminAgrementationTabComponent } from './admin-agrementation-tab.component';
import { AdminComptesDemandeursComponent } from './admin-comptes-demandeurs.component';
import { AdminDemandesAgrementsComponent } from './admin-demandes-agrements.component';
import { AdminNouvelleDemandeComponent } from './admin-nouvelle-demande.component';
import { AdminDemandeDialog } from './admin-demande.dialog.component';
import { AdminDemandeDetailComponent} from './admin-demande-detail.component';
import { AdminCommunicationNoteDialogComponent } from './admin-communication-note-dialog.component';
import { AdminDemandePersonnelMemberDialogComponent } from './admin-demande-personnel-member-dialog.component';
import { AdminDemandeModificationDialog } from './admin-demande-modification-dialog.component';
import { AdminDemandeProgrammeDialogComponent } from './admin-demande-programme-dialog.component';
import { AdminDemandeManagementDialog } from './admin-demande-management.dialog.component';
import { AdminGestionAttributionDialog } from './admin-gestion-attribution.dialog.component';
import { AdminDemandePaymentReceiptComponent } from './admin-demande-payment-receipt.dialog';
import { AdminCommunicationNoteRefusDialog } from './admin-communication-note-refus.dialog.component';
import { AdminCommunicationReadonlyDialog } from './admin-communication-readonly.dialog.component';

export const routes = [
  { 
    path: '', 
    component: AdminAgrementationTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'list',
          pathMatch: 'full'
        },
        { 
          path: 'list', 
          component: AdminDemandesAgrementsComponent, 
          data: { breadcrumb: 'Demandes d’agréments' }  
        },
        { 
          path: 'demandeurs', 
          component: AdminComptesDemandeursComponent, 
          data: { breadcrumb: 'Gestion des comptes demandeurs' }  
        },
        { 
          path: 'nouvelle-demande', 
          component: AdminNouvelleDemandeComponent, 
          data: { breadcrumb: 'Créer une nouvelle demande' }  
        },
      ]
  },
  { 
    path: 'agrements/view/:id', 
    component: AdminDemandeDetailComponent, 
    data: { breadcrumb: 'Demandes' }  
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    AdminAgrementationTabComponent,
    AdminComptesDemandeursComponent,
    AdminDemandesAgrementsComponent,
    AdminNouvelleDemandeComponent,
    AdminDemandeDetailComponent,
    AdminDemandeDialog,
    AdminCommunicationNoteDialogComponent,
    AdminDemandePersonnelMemberDialogComponent,
    AdminDemandeModificationDialog,
    AdminDemandeProgrammeDialogComponent,
    AdminDemandeManagementDialog,
    AdminGestionAttributionDialog,
    AdminDemandePaymentReceiptComponent,
    AdminCommunicationNoteRefusDialog,
    AdminCommunicationReadonlyDialog,
  ]
})
export class AdminAgrementationModule {}
