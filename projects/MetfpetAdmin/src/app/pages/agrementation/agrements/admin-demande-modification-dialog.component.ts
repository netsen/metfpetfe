import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  DemandeStatus,
  MetfpetServiceAgent,
  hideLoading,
  IAppState,
  showException,
  showLoading,
  showSuccess,
  validateForm,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  templateUrl: './admin-demande-modification-dialog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminDemandeModificationDialog {
  form: FormGroup;
  agrementPrealable: string;

  constructor(
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _router: Router,
    private _formBuilder: FormBuilder,
    private _store: Store<IAppState>,
    private _dialogRef: MatDialogRef<AdminDemandeModificationDialog>,
    protected _cd: ChangeDetectorRef,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 
    this.form = this._formBuilder.group({
      demande: [null, Validators.required]
    })
    this._cd.markForCheck()
  }
  
  public onSubmit(): void {
    validateForm(this.form)
    if(this.form.valid){
      this.agrementPrealable = this.form.get('demande').value.join(",");
      this._dialogRef.close(this.agrementPrealable);
    }
  }

  onClose() {
    this._dialogRef.close();
  }
}
