import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  DemandeStatus,
  MetfpetServiceAgent,
  hideLoading,
  IAppState,
  showException,
  showLoading,
  showSuccess,
  validateForm,
  PerfectScrollService,
  BaseTableComponent,
  AppSettings,
  Settings,
  AuthService,
} from 'MetfpetLib';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { selectCurrentDemandeur } from 'projects/MetfpetDemandeur/src/app/shared/store/selectors';

@Component({
  selector: 'app-admin-demande-management.dialog',
  templateUrl: './admin-demande-management.dialog.component.html',
  styleUrls: ['./admin-demande-management.dialog.component.css']
})
export class AdminDemandeManagementDialog extends BaseTableComponent implements OnInit {
  
  form: FormGroup;
  settings: Settings;
  displayItem: boolean;
  displayHistoricTab: boolean;
  status : number;
  objetTitle: string;
  demandeur: MetfpetServiceAgent.DemandeurProfileViewModel;
  changeDemandeStatus: MetfpetServiceAgent.ChandeDemandeStatusForSuspensionDTO;
  demandeStatus : any;

  constructor(
    public appSettings: AppSettings, 
    protected _metfpetService: MetfpetServiceAgent.HttpService,
    protected _router: Router,
    protected _formBuilder: FormBuilder,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    protected _dialogRef: MatDialogRef<AdminDemandeManagementDialog>,
    protected _cd: ChangeDetectorRef,
    private _authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.form = this._formBuilder.group({
      raison: [null, Validators.required],
      dateDebut: [null],
      dateFin:[null]
    })
    this.displayItem = true;
    this.demandeStatus = DemandeStatus;
    this.displayHistoricTab = false;
    this.settings = this.appSettings.settings;
    this.sort = {prop: 'userName', dir: 'asc'};
  }
  ngOnInit(): void {
    this.triggerSearch();
  }

  protected _search(criteria: any): Observable<any> {
    criteria.filters = {
      demandeId: this.data.demandeId
    }
    return this._metfpetService.getHistoriqueDemandeListPage(criteria);
  }

  displayHistoric(){
    this.displayHistoricTab = true;
  }

  onButtonClick(status: number){
    this.status = status;
    switch (this.status) {
      case this.demandeStatus.Annule:
        this.displayItem = false;
        this.objetTitle = "Annulation"
        break;
      case this.demandeStatus.Suspendu:
        this.displayItem = false;
        this.objetTitle = "Suspension"
        break;
      case this.demandeStatus.Reactive:
        this.status = status;
          this._store.dispatch(showLoading());
           this.changeDemandeStatus = MetfpetServiceAgent.ChandeDemandeStatusForSuspensionDTO.fromJS(
            Object.assign(
              {
                raison: "-",
                status: DemandeStatus.Reactive,
                demandeId: this.data.demandeId,
              }))
          this._metfpetService.reactivateDemande(this.changeDemandeStatus).subscribe(
            () => {
              this._store.dispatch(showSuccess({}));
              this._dialogRef.close(true);
              this.triggerSearch();
            },
            error => this._store.dispatch(showException({ error: error }))
          ).add(() => {
            this._store.dispatch(hideLoading());
          });
        break;
      default:
        break;
    }
  }

  onSubmit(){
      switch (this.status) {
      case DemandeStatus.Annule:
        if(this.form.valid){
          this._store.dispatch(showLoading());
           this.changeDemandeStatus = MetfpetServiceAgent.ChandeDemandeStatusForSuspensionDTO.fromJS(
            Object.assign(
              {
                raison: this.form.get('raison').value,
                status: DemandeStatus.Annule,
                demandeId: this.data.demandeId,
              }))
          this._metfpetService.invalidateDemande(this.changeDemandeStatus).subscribe(
            () => {
              this._store.dispatch(showSuccess({}));
              this._dialogRef.close(true);
              this.triggerSearch();
            },
            error => this._store.dispatch(showException({ error: error }))
          ).add(() => {
            this._store.dispatch(hideLoading());
          });
        }
        break;
      case DemandeStatus.Suspendu:
        if(this.form.valid){
          this._store.dispatch(showLoading());
           this.changeDemandeStatus = MetfpetServiceAgent.ChandeDemandeStatusForSuspensionDTO.fromJS(
            Object.assign(
              {
                raison: this.form.get('raison').value,
                dateDebut : this.form.get('dateDebut').value,
                dateFin : this.form.get('dateFin').value,
                status: DemandeStatus.Suspendu,
                demandeId: this.data.demandeId,
              }))
          this._metfpetService.suspendDemande(this.changeDemandeStatus).subscribe(
            () => {
              this._store.dispatch(showSuccess({}));
              this._dialogRef.close(true);
              this.triggerSearch();
            },
            error => this._store.dispatch(showException({ error: error }))
          ).add(() => {
            this._store.dispatch(hideLoading());
          });
        }
        break;
    
      default:
        break;
    }
  }

  close(){
    this._dialogRef.close();
  }

  generateCertificate() {
    this._store.dispatch(showLoading());
    this._metfpetService.generateAgrementCertificate(this.data.demandeId).subscribe(
      () => {
        this._store.dispatch(showSuccess({}));
        this._dialogRef.close(true);
      },
      error => this._store.dispatch(showException({ error: error }))
      ).add(() => {
        this._store.dispatch(hideLoading());
      });
  }

  isAdministrateur() {
    return this._authService.getUserType() == 'administrateurMinisteriel' || this._authService.getUserType() == 'administrateur';
  }
}
