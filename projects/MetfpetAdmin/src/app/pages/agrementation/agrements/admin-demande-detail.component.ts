import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ViewChild,
  ViewEncapsulation,
  ElementRef,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { startWith, debounceTime, distinctUntilChanged } from 'rxjs/operators/';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  MetfpetServiceAgent,
  PerfectScrollService,
  AgrementationStatus,
  DialogService,
  showLoading,
  showSuccess,
  showError,
  hideLoading,
  validateForm,
  showException,
  DemandeStatus,
  SeverityEnum,
  ChampType,
  AuthService,
  DemandeProcessStepStatus,
  PersonnelType,
  PaymentReceiptStatus,
} from 'MetfpetLib';
import { MatDialog } from '@angular/material/dialog';
import { Location } from '@angular/common';
import { UiPath } from '../../ui-path';
import { AdminCommunicationNoteDialogComponent } from './admin-communication-note-dialog.component';
import { AdminDemandePersonnelMemberDialogComponent } from './admin-demande-personnel-member-dialog.component';
import { AdminDemandeProgrammeDialogComponent } from './admin-demande-programme-dialog.component';
import { AdminDemandeManagementDialog } from './admin-demande-management.dialog.component';
import { AdminDemandePaymentReceiptComponent } from './admin-demande-payment-receipt.dialog';
import { AdminCommunicationNoteRefusDialog } from './admin-communication-note-refus.dialog.component';
import { AdminCommunicationReadonlyDialog } from './admin-communication-readonly.dialog.component';

export const NORMAL_CSS = 'row mb-2';
export const ERROR_CSS = 'row mb-2 warn-color';

@Component({
  templateUrl: './admin-demande-detail.component.html',
  styleUrls: ['./admin-demande-detail.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminDemandeDetailComponent implements OnInit{
  form: FormGroup;
  demandeInstitutionForm: FormGroup;
  settings: Settings;
  firstStepTitle: string;
  enAttente : boolean; 
  title: string;
  demandeProcessStepData: any[]
  allStep: number[];
  repeatedStep: number[];
  noRepeatedStep: number[];
  lastStep: any;
  agrementPrealableNumbers: string [];
  agrementPrealableTitle: string;
  communicationNoteList: Array<MetfpetServiceAgent.CommunicationNoteViewModel>;
  demandeId: string;
  employeMinisterielId: string;
  employeRoleId: string;
  public _model: MetfpetServiceAgent.DemandeDTO;
  demandeInstitutionModel: MetfpetServiceAgent.DemandeInstitutionDTO;
  demande:Array<MetfpetServiceAgent.DemandeViewModel>;
  agrementation: MetfpetServiceAgent.AgrementationDTO;
  tarification: MetfpetServiceAgent.AgrementationTarificationDTO;
  currentProcessStep: MetfpetServiceAgent.DemandeProcessStepDTO;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  attachedDocumentSections: Array<MetfpetServiceAgent.AttachedDocumentSectionListDTO>;
  formulaireSectionChampValeurs: Array<MetfpetServiceAgent.FormulaireSectionChampValeurListDTO>;
  saveFormulaireSectionChampValeurs: Array<MetfpetServiceAgent.FormulaireSectionChampValeurDTO>;
  personnelMembersOfAgrementation: Array<MetfpetServiceAgent.PersonnelMemberDTO>;
  demandePersonnelList: Array<MetfpetServiceAgent.DemandePersonnelMemberDTO>;
  administratifDemandePersonnelMemberList: Array<MetfpetServiceAgent.DemandePersonnelMemberDTO>;
  administratifPersonnelMemberList: Array<MetfpetServiceAgent.PersonnelMemberDTO>;
  formateurDemandePersonnelMemberList: Array<MetfpetServiceAgent.DemandePersonnelMemberDTO>;
  formateurPersonnelMemberList: Array<MetfpetServiceAgent.PersonnelMemberDTO>;
  demandeProgrammeList: Array<MetfpetServiceAgent.DemandeProgrammeDTO>;
  demandeProgrammeListView: Array<MetfpetServiceAgent.DemandeProgrammeViewModel>;
  demandeDisciplineList: Array<MetfpetServiceAgent.DemandeDisciplineDTO>;
  niveauAccesList: Array<MetfpetServiceAgent.NiveauAccesViewModel>;
  typeAdmissionList: Array<MetfpetServiceAgent.TypeAdmissionViewModel>;
  typeProgrammeList: Array<MetfpetServiceAgent.TypeProgrammeViewModel>;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  typeDisciplineList: Array<MetfpetServiceAgent.TypeDisciplineViewModel>;
  typeDiplomeList: Array<MetfpetServiceAgent.TypeDiplomeViewModel>;
  langueList: Array<MetfpetServiceAgent.LangueViewModel>;
  demandeFaculteList: Array<MetfpetServiceAgent.DemandeFaculteViewModel>;
  typeInstitutionList: Array<MetfpetServiceAgent.TypeInstitutionViewModel>;
  prefecturesList: Array<MetfpetServiceAgent.PrefectureViewModel>;
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissementViewModel>;
  requiredAdministratifPosteListString: string;
  requiredPosteListString: string;
  isValidated: boolean;
  isAdministrateur: boolean;
  isAgrementSupervisor: boolean;
  isCorrectionsNeeded: boolean;
  showDeleteButton: boolean;
  showSaveButton: boolean;
  alreadySubmitted: boolean;
  isSelectedAdministratifPersonnelTab = true;
  demandeStatus: any;
  hasPaymentReceipet: boolean;
  paymentReceiptStatusName : string;
  paymentReceiptStatus : number;
  activeButtonReceiptPayment: boolean = false;
  demandeurRoleId: string;
  loadingIndicator: boolean;


  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    private _location: Location,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    private _authService: AuthService,
    private _route: ActivatedRoute,
    protected _store: Store<any>,
    private _dialog: MatDialog,
    private _dialogService: DialogService,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,

  ) {
    this.settings = this.appSettings.settings;
    this.saveFormulaireSectionChampValeurs = [];
    this.employeRoleId = _authService.getEmployeRoleId();
    this.isAdministrateur = _authService.getEmployeRole() === 'Administrateur';
    this.isAgrementSupervisor = _authService.getUserType() == 'agrementSupervisorMinisteriel';
    this.title = 'Demandes';
    this.demandeStatus = DemandeStatus;
  }

  ngOnInit() {
    this._metfpetService.getPrefectureList().subscribe((data) => {
      this.prefectureList = data;
      this._cd.markForCheck();
    });

    this.demandeInstitutionForm = this._formBuilder.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'description': [null],
      'typeInstitutionId': [null, Validators.required],
      'prefectureId': [null, Validators.required],
      'statusEtablissementId': [null, Validators.required],
    });

    this._route.params.subscribe(params => {
      this.demandeId = params['id'];
      if (this.demandeId) {
        this._metfpetService.getDemande(this.demandeId).subscribe(data => {
          this._model = data;
          this.alreadySubmitted = this._model.status !== <any>DemandeStatus.APayer;
          this.isCorrectionsNeeded = this._model.status === <any>DemandeStatus.CorrectionsNecessaire;
          this.showDeleteButton = this.isAdministrateur && !this.alreadySubmitted;
          this.hasPaymentReceipet = this._model.hasPaymentReceipt;
          this.paymentReceiptStatusName = this._model.paymentReceiptStatusName;
          this.activeButtonReceiptPayment = true;
          this.paymentReceiptStatus = this._model.paymentReceiptStatus;
          this.loadAgrementation();
          this.loadAgrementPrealableNumber();
          this.loadEmployeMinisteriel();
          this.loadTarification();
          this.loadAttachedDocumentSections();
          this.loadFormulaireSectionChampValeurs();
          this.loadPersonnelMembersOfAgrementation(this._model.agrementationId);
          this.loadDemandeProcessSteps();
          this.loadAdministratifPersonnelMembers(this._model.agrementationId);
          this.loadFormateurPersonnelMembers(this._model.agrementationId);
          this.loadDemandeFacultes();
          this.loadInstitutionOfDemande();
          this.loadDemandeProgrammeListView();
          this._cd.markForCheck();
        });
        //this.loadDemandePersonnelList();
        this.loadAdministratifDemandePersonnelMembers();
        this.loadFormateurDemandePersonnelMembers();
        this.getDemandeurRoleId();
        this.loadProgrammesOfDemande();
        this.loadDisciplinesOfDemande();
        this.loadNiveauAccess();
        this.loadTypeAdmissions();
        this.loadTypeProgrammes();
        this.loadTypeDisciplines();
        this.loadNiveauEtudes();
        this.loadTypeDiplomes();
        this.loadLangues();
        this.loadTypeInstitutions();
        this.loadPrefectures();
        this.loadStatusEtablissements()
      }
    });
  }

  loadAgrementation() {
    this._metfpetService.getAgrementation(this._model.agrementationId).subscribe((data) => {
      this.agrementation = data;
    });
  }

  loadEmployeMinisteriel() {
    this._metfpetService.getEmployeMinisterielProfile(this._authService.getUserId()).subscribe((data : any) => {
      this.employeMinisterielId = data.id;
      this.employeRoleId = data.employeRoleId;
      this.loadCurrentProcessStep();
    });
  }


  filtrerElementsRepetes(element, index, liste) {
    return index !== liste.indexOf(element) && index === liste.lastIndexOf(element);
  }

  circleByStep(step: number){
    return this.demandeProcessStepData.filter(x => x.step === step).reverse();
  }

  circleWidth(step: number){
    return this.repeatedStep.includes(step) ? '35px' : '40px';
  }

  circleHeight(step: number){
    return this.repeatedStep.includes(step) ? '35px' : '40px';
  }

  circleBgColor(process : any){
    if(this._model.status == <any>DemandeStatus.Delivre || this._model.status == <any>DemandeStatus.Reactive){
      return 'valide';
    }

    if(process.step === this._model.currentProcessStep && process.status === <any>DemandeProcessStepStatus.AnalyseEnCours){
      return 'enCours';
    }

    if (process.step <= this._model.currentProcessStep && !this.enAttente) {
      switch (process.status) {
        case 1:
          return 'analyseEnCours';
        case 2:
          return 'correctionsNecessaire';
        case 3:
          return 'valide';
        case 4:
          return 'refuse';
        default:
          return 'analyseEnCours';
      }
    }else{
      return 'analyseEnCours';
    }
  }

  stepIcon(process : any){
    if(this._model.status === <any>DemandeStatus.Delivre){
      return 'valide';
    }

    if(process.step === this._model.currentProcessStep && process.status === <any>DemandeProcessStepStatus.AnalyseEnCours){
      return 'enAttente';
    }

    if  (process.step <=  this._model.currentProcessStep && !this.enAttente) {
      switch (process.status) {
        case 1:
          return '';
        case 2:
          return 'correctionsNecessaire';
        case 3:
          return 'valide';
        case 4:
          return 'refuse';
        default:
          return '';
      }
    }else{
      return '';
    }
  }

  isVerticalCircle(step){
   return this.repeatedStep.includes(step); 
  }


  loadDemandeProcessSteps() {
    this._metfpetService.getDemandeProcessSteps(
      MetfpetServiceAgent.DemandeDetailRequest.fromJS(
        Object.assign(
          {
            demandeId: this._model.id,
            agrementationId: this._model.agrementationId
          })
      )
    ).subscribe((data) => {
      this.demandeProcessStepData = data.sort((a, b) => (a.step > b.step) ? 1 : -1);
      this.lastStep = this.demandeProcessStepData.find(x => x.step === Math.max(...this.demandeProcessStepData.map(x => x.step)));
      this.demandeProcessStepData.pop();
      this.allStep = this.demandeProcessStepData.map(x => x.step);
      this.repeatedStep = this.allStep.filter(this.filtrerElementsRepetes);
        this.noRepeatedStep = this.allStep.reduce((acc, step) => {
          if (!acc.includes(step)) {
            acc.push(step);
          }
          return acc.sort();
        }, []);
      })
      if(this._model.status == 1){
        this.enAttente = true;
        this.firstStepTitle = 'En attente';
      }else{
        this.firstStepTitle = 'Dossier soumis';
        this.enAttente = false;
      }
    }

  navigateToAgrementPrealable(number:string){
    this._metfpetService.getDemandeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, 
      filters: {
        numero: number
      }
    })).subscribe((data)=>{
      this.demande = data.results;
      if (this.demande.length > 0) {
        const url = this._router.serializeUrl(this._router.createUrlTree(['agrements/agrements/view', this.demande[0].id]));
        window.open(url, '_blank');
      }
    })
  }

  loadAgrementPrealableNumber(){
    if(this._model.agrementPrealable){
      this.agrementPrealableNumbers = this._model.agrementPrealable.split(",");
      if (this._model.isModification) {
        this.agrementPrealableTitle = "Agrément à modifier";
      }else{
        this.agrementPrealableTitle = "Agrément préalable";
      }
    }
  }

  loadFormulaireSectionChampValeurs() {
    this._metfpetService.getFormulaireSectionChampValeurs(MetfpetServiceAgent.DemandeDetailRequest.fromJS(
      Object.assign(
        {
          demandeId: this._model.id,
          agrementationId: this._model.agrementationId
        })
    )).subscribe((data) => {
      this.formulaireSectionChampValeurs = data;
      this.buildFormulaire();
      this._cd.markForCheck();
    });
  }

  loadPersonnelMembersOfAgrementation(agrementationId: string) {
    this._metfpetService.getPersonnelMembersOfAgrementation(agrementationId).subscribe(data => {
      this.personnelMembersOfAgrementation = data;
      this.requiredPosteListString = this.personnelMembersOfAgrementation.map(x => x.poste).join(", ");
      this._cd.markForCheck();
    });
  }

  buildFormulaire() {
    let allowEdit = false;
    const group: any = {};
    for (var section of this.formulaireSectionChampValeurs) {
      if (section.formulaireSection.employeRoleId === this.employeRoleId) {
        allowEdit = true;
      }
      for (var champ of section.formulaireSectionChampValeurs) {
        this.saveFormulaireSectionChampValeurs.push(champ);
        if(this.isCheckBox(champ) && this.isCheckBoxWithMultipleValues(champ) && champ.valeur){
          group[champ.formControlName] = new FormControl(champ.valeur.split(';'));
        }else{
          group[champ.formControlName] = new FormControl(champ.valeur || '');
        }
      }
    }
    this.form = this._formBuilder.group(group);
    this.showSaveButton = (this.allowEditForAdministrateurAndSupervisor() || allowEdit) && this.alreadySubmitted;
  }

  allowSubmit() {
    return (this.isAdministrateur || this.isAgrementSupervisor) && this.isCorrectionsNeeded;
  }
  
  allowEditForAdministrateurAndSupervisor() {
    return this.isAdministrateur || (this.isAgrementSupervisor && this.isCorrectionsNeeded);
  }

  getDemandeurRoleId(){
    this.loadingIndicator = true;
    this._metfpetService.getEmployeRoleWithDemandeurListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { 
        status : 1,
        name : 'Demandeur'
       }
    })).subscribe((res)=>{
      this.demandeurRoleId = res?.results[0]?.id
    })
    this.loadCommunicationNotes(this.demandeurRoleId)
  }

  loadCommunicationNotes(demandeurRoleId : string) {
    this._metfpetService.getAllCommunicationNoteByDemadeId(this.demandeId).subscribe(data => {
      if (this.employeRoleId && this.demandeurRoleId && !this.isAdministrateur) {
        this.communicationNoteList = data.filter(x => 
          (x.employeRoleIds.includes(this.employeRoleId) || x.employeRoleIds.includes(this.demandeurRoleId)) || 
          x.employeRoleIds.length === 0
        );
        this._cd.markForCheck();
      }else{
        this.communicationNoteList = data;
      }
      this.loadingIndicator = false;
    });
  }

  openReadMessageDialog(communicationNote: MetfpetServiceAgent.CommunicationNoteViewModel){
    this._dialog.open(AdminCommunicationReadonlyDialog,{
      width: '600px',
      data : {
        messageContent : communicationNote.message, 
        messageSubject : communicationNote.object,
        attachedFileLink : communicationNote.attachedFileLink,
        attachedFileName : communicationNote.attachedFileName
      }
    })
  }

  truncateMessage(message: string, length: number = 25): string {
    if (!message) {
      return '';
    }
    return message.length > length ? message.substring(0, length) + '...' : message;
  }

  loadProgrammesOfDemande() {
    this._metfpetService.getProgrammesOfDemande(this.demandeId).subscribe(data => {
      this.demandeProgrammeList = data;
      this._cd.markForCheck();
    });
  }

  loadDisciplinesOfDemande() {
    this._metfpetService.getDisciplinesOfDemande(this.demandeId).subscribe(data => {
      this.demandeDisciplineList = data;
      this._cd.markForCheck();
    });
  }

  loadAttachedDocumentSections() {
    this._metfpetService.getAttachedDocumentSections(MetfpetServiceAgent.DemandeDetailRequest.fromJS(
      Object.assign(
        {
          demandeId: this._model.id,
          agrementationId: this._model.agrementationId
        })
    )).subscribe((data) => {
      this.attachedDocumentSections = data;
      this._cd.markForCheck();
    });
  }

  loadTarification() {
    this._metfpetService.getTarificationForAgrementation(this._model.agrementationId).subscribe((data) => {
      this.tarification = data;
    });
  }

  loadDemandePersonnelList() {
    this._metfpetService.getPersonnelMembersOfDemande(this.demandeId).subscribe(data => {
      this.demandePersonnelList = data;
      this._cd.markForCheck();
    });
  }

  loadCurrentProcessStep() {
    this._metfpetService.getCurrentProcessStepOfEmployeRole(MetfpetServiceAgent.DemandeProcessStepDTO.fromJS(
      Object.assign({
        demandeId: this._model.id,
        step: this._model.currentProcessStep,
        employeRoleId: this.employeRoleId,
      })
    )).subscribe((data)=>{
      if (data) {
        this.isValidated = true;
        this.currentProcessStep = data;
        this._cd.markForCheck();
      }
    });
  }

  loadAdministratifPersonnelMembers(agrementationId: string) {
    this._metfpetService.getPersonnelMemberListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {
        'agrementationId': agrementationId,
        'personnelType': PersonnelType.Administratif
      }
    })).subscribe(data => {
      this.administratifPersonnelMemberList = data.results;
      this.requiredAdministratifPosteListString = this.administratifPersonnelMemberList.map(x => x.poste).join(", ");
      this._cd.markForCheck();
    });
  }

  loadAdministratifDemandePersonnelMembers() {
    this._metfpetService.getDemandePersonnelMemberListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {
        'demandeId': this.demandeId,
        'personnelType': PersonnelType.Administratif
      }
    })).subscribe(data => {
      this.administratifDemandePersonnelMemberList = data.results;
      this._cd.markForCheck();
    });
  }

  loadFormateurPersonnelMembers(agrementationId: string) {
    this._metfpetService.getPersonnelMemberListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {
        'agrementationId': agrementationId,
        'personnelType': PersonnelType.Formateur
      }
    })).subscribe(data => {
      this.formateurPersonnelMemberList = data.results;
      this._cd.markForCheck();
    });
  }

  loadFormateurDemandePersonnelMembers() {
    this._metfpetService.getDemandePersonnelMemberListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {
        'demandeId': this.demandeId,
        'personnelType': PersonnelType.Formateur
      }
    })).subscribe(data => {
      this.formateurDemandePersonnelMemberList = data.results;
      this._cd.markForCheck();
    });
  }

  loadNiveauAccess() {
    this._metfpetService.getNiveauAccesListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauAccesList = data.results;
      this._cd.markForCheck();
    });
  }

  loadTypeAdmissions() {
    this._metfpetService.getTypeAdmissionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.typeAdmissionList = data.results;
      this._cd.markForCheck();
    });
  }

  loadTypeProgrammes() {
    this._metfpetService.getTypeProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.typeProgrammeList = data.results;
      this._cd.markForCheck();
    });
  }

  loadTypeDisciplines() {
    this._metfpetService.getTypeDisciplineListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.typeDisciplineList = data.results;
      this._cd.markForCheck();
    });
  }

  loadNiveauEtudes() {
    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });
  }

  loadTypeDiplomes() {
    this._metfpetService.getTypeDiplomeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.typeDiplomeList = data.results;
      this._cd.markForCheck();
    });
  }

  loadLangues() {
    this._metfpetService.getLangueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.langueList = data.results;
      this._cd.markForCheck();
    });
  }

  loadDemandeFacultes() {
    this._metfpetService.getDemandeFaculteListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {
        demandeId: this.demandeId
      }
    })).subscribe(data => {
      this.demandeFaculteList = data.results;
      this._cd.markForCheck();
    });
  }

  loadDemandeProgrammeListView() {
    this._metfpetService.getProgrammesOfDemandeListPage(this.demandeId).subscribe(data => {
      this.demandeProgrammeListView = data;
      this._cd.markForCheck();
    });
  }

  loadTypeInstitutions() {
    this._metfpetService.getTypeInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.typeInstitutionList = data.results;
      this._cd.markForCheck();
    });
  }

  loadPrefectures() {
    this._metfpetService.getPrefectureListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.prefecturesList = data.results;
      this._cd.markForCheck();
    });
  }

  loadStatusEtablissements() {
    this._metfpetService.getStatusEtablissementListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.statusEtablissementList = data.results;
      this._cd.markForCheck();
    });
  }

  loadInstitutionOfDemande() {
    this._metfpetService.getInstitutionOfDemande(this.demandeId).subscribe(data => {
      if (data) {
        this.demandeInstitutionModel = data;
        this.demandeInstitutionForm.patchValue({
          name: this.demandeInstitutionModel.name,
          description: this.demandeInstitutionModel.description,
          typeInstitutionId: this.demandeInstitutionModel.typeInstitutionId,
          prefectureId: this.demandeInstitutionModel.prefectureId,
          statusEtablissementId: this.demandeInstitutionModel.statusEtablissementId,
        });
        this._cd.markForCheck();
      }
    });
  }

  canEditFormulaireSectionChamp(section: MetfpetServiceAgent.DemandeFormulaireSectionDTO): boolean {
    return (this.allowEditForAdministrateurAndSupervisor() || section.employeRoleId == this.employeRoleId) && this.alreadySubmitted;
  }

  canEditAttachedDocumentSection(section: MetfpetServiceAgent.DemandeSectionDTO): boolean {
    return (this.allowEditForAdministrateurAndSupervisor() || section.employeRoleId == this.employeRoleId) && this.alreadySubmitted;
  }

  isShortText(champ: MetfpetServiceAgent.FormulaireSectionChampValeurDTO) {
    return champ.champType === <any>ChampType.ShortText;
  }

  isLongText(champ: MetfpetServiceAgent.FormulaireSectionChampValeurDTO) {
    return champ.champType === <any>ChampType.LongText;
  }

  isCheckBox(champ: MetfpetServiceAgent.FormulaireSectionChampValeurDTO) {
    return champ.champType === <any>ChampType.CheckBox;
  }

  isRadioButton(champ: MetfpetServiceAgent.FormulaireSectionChampValeurDTO) {
    return champ.champType === <any>ChampType.RadioButton;
  }

  isDropdownList(champ: MetfpetServiceAgent.FormulaireSectionChampValeurDTO) {
    return champ.champType === <any>ChampType.DropdownList;
  }
  
  isCheckBoxWithMultipleValues(champ: MetfpetServiceAgent.FormulaireSectionChampValeurDTO){
    return champ?.possibleValeurs?.length>0;
  }

  onSave(isSubmit: boolean) {
    validateForm(this.form);
    validateForm(this.demandeInstitutionForm);
    if (this.form.valid && this.demandeInstitutionForm.valid) {
      this._model = Object.assign({}, this._model, this.form.value);
      this.demandeInstitutionModel = Object.assign({},
        this.demandeInstitutionModel,
        this.demandeInstitutionForm.value, {
          demandeId: this._model.id
        }
      );
      this._store.dispatch(showLoading());
      for (var item of this.saveFormulaireSectionChampValeurs) {
        if(this.isCheckBox(item) && this.isCheckBoxWithMultipleValues(item)){
          item.valeur = this.form.get(item.formControlName).value?.join(';');
        }else{
          item.valeur = this.form.get(item.formControlName).value;
        }
      }      
      this._metfpetService.updateDemandeDetail(MetfpetServiceAgent.DemandeSaveInfo.fromJS(Object.assign(
        {
          demande: this._model,
          demandeInstitution: this.demandeInstitutionModel,
          formulaireSectionChampValeurs: this.saveFormulaireSectionChampValeurs,
          isSubmit: isSubmit
        }))).subscribe(
          () => {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([UiPath.admin.agrementation.agrements]);
          },
          error => this._store.dispatch(showException({ error: error }))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onSubmit() {
    this.onSave(true);
  }

  onDelete() {
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention",
        message: "Voulez-vous définitivement supprimer ce demande ?",
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteDemande(this._model.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({ message: 'Le demande a été supprimée' }));
              this._router.navigate([UiPath.admin.agrementation.agrements]);
            },
            (error) => this._store.dispatch(showError({ message: error.response }))
          )
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  goBack() {
    this._location.back();
  }

  public showAttachedDocumentSectionWarning() {
    if (this.attachedDocumentSections && this.attachedDocumentSections.length > 0) {
      for (var item of this.attachedDocumentSections) {
        if (item.attachedDocumentSections) {
          for (var ad of item.attachedDocumentSections) {
            if (!ad.link) {
              return true;
            }
          }
        }
      }
      return false;
    }
    return true;
  }

  public getCss(attachedDocument: MetfpetServiceAgent.AttachedDocumentSectionDTO) {
    return attachedDocument.link ? NORMAL_CSS : ERROR_CSS;
  }

  public getSelectorId(attachedDocument: MetfpetServiceAgent.AttachedDocumentSectionDTO) {
    return 'selector' + attachedDocument.id;
  }

  private replaceAttachedDocumentSectionDTO(item: MetfpetServiceAgent.AttachedDocumentSectionListDTO, data: MetfpetServiceAgent.AttachedDocumentSectionDTO): MetfpetServiceAgent.AttachedDocumentSectionListDTO {
    item.attachedDocumentSections = item.attachedDocumentSections.map(x =>
      x.id == data.id ? MetfpetServiceAgent.AttachedDocumentSectionDTO.fromJS(data) : x);
    return item;
  }

  public onFileUpload(event: any, attachedDocument: MetfpetServiceAgent.AttachedDocumentSectionDTO) {
    if (event.target.files && event.target.files.length > 0) {
      this._store.dispatch(showLoading());

      let file: Blob = event.target.files[0];
      let uploadedFileName: string = event.target.files[0].name;
      var fileReader = new FileReader();
      fileReader.onloadend = function () {

      }
      fileReader.readAsBinaryString(file);

      let fileParameter =
      {
        data: file,
        fileName: uploadedFileName
      };

      this._metfpetService.uploadAttachedDocumentSection(fileParameter, attachedDocument.id)
        .subscribe(
          (data) => {
            this.attachedDocumentSections = this.attachedDocumentSections.map(x => this.replaceAttachedDocumentSectionDTO(x, data));
            this._cd.markForCheck();
            this._store.dispatch(showSuccess({}));
          },
          (error) => this._store.dispatch(showError({ message: error.message }))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public uploadDocument(attachedDocument: MetfpetServiceAgent.AttachedDocumentSectionDTO): void {
    document.getElementById('selector' + attachedDocument.id).click();
  }

  public openDocument(attachedDocument: MetfpetServiceAgent.AttachedDocumentSectionDTO): void {
    window.open(attachedDocument.link, "_blank");
  }

  public isValidToDownloadModel(attachedDocument: MetfpetServiceAgent.AttachedDocumentSectionDTO){
    return attachedDocument.modelLink && attachedDocument.modelLink.length > 0;
  }

  public openDocumentModel(attachedDocument: MetfpetServiceAgent.AttachedDocumentSectionDTO): void {
    window.open(attachedDocument.modelLink, "_blank");
  }
  createCommunicationNote(id: string) {
    var dialogref = this._dialog.open(AdminCommunicationNoteDialogComponent,
      {
        data: {
          id: id,
          demandeId: this.demandeId,
          employeMinisterielId: this.employeMinisterielId,
        }
      })
      .afterClosed().subscribe((saveSuccess) => {
        if (saveSuccess) {
          this.loadCommunicationNotes(this.demandeurRoleId);
        }
      })
  }

  editPersonnelDetails(row: MetfpetServiceAgent.DemandePersonnelMemberDTO){
    let isAdministratif = row.personnelType == <any>PersonnelType.Administratif;
    var dialogref = this._dialog.open(AdminDemandePersonnelMemberDialogComponent,
      {
        data: {
          id: row.id,
          demandeId: this.demandeId,
          personnelMemberList: this.personnelMembersOfAgrementation,
          programmeList: this.demandeProgrammeList,
          disciplineList: this.demandeDisciplineList,
          isAdministratif: isAdministratif,
        }
      })
      .afterClosed().subscribe((saveSuccess) => {
        if (saveSuccess) {
          this.reloadDemandePersonnelMembers(isAdministratif);
        }
      })
  }

  deletePersonnelMember(row: MetfpetServiceAgent.DemandePersonnelMemberDTO){
    let isAdministratif = row.personnelType == <any>PersonnelType.Administratif;
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention", 
        message: "Voulez-vous supprimer définitivement ce membre ?", 
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteDemandePersonnelMember(row.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({message: 'Le membre a été supprimé'}));
              this.reloadDemandePersonnelMembers(isAdministratif);
            },
            (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  createDemandePersonnelMember(isMandatoryMember: boolean, isAdministratif: boolean) {
    var dialogref = this._dialog.open(AdminDemandePersonnelMemberDialogComponent,
      {
        data: {
          demandeId: this.demandeId,
          personnelMemberList: isAdministratif ? this.administratifPersonnelMemberList : this.formateurPersonnelMemberList,
          programmeList: this.demandeProgrammeList,
          disciplineList: this.demandeDisciplineList,
          isMandatoryMember: isMandatoryMember,
          isAdministratif: isAdministratif,
        }
      })
      .afterClosed().subscribe((saveSuccess) => {
        if (saveSuccess) {
          this.reloadDemandePersonnelMembers(isAdministratif);
        }
      })
  }

  reloadDemandePersonnelMembers(isAdministratif: boolean) {
    if (isAdministratif) {
      this.loadAdministratifDemandePersonnelMembers();
    }
    else {
      this.loadFormateurDemandePersonnelMembers();
    }
  }

  changePersonnelTab(isAdministratif: boolean) {
    this.isSelectedAdministratifPersonnelTab = isAdministratif;
  }

  validateDemandeCurrentProcessStep(status: DemandeProcessStepStatus) {
      this._store.dispatch(showLoading());
      this.currentProcessStep.status = <any>status;
      this._metfpetService.validateDemandeCurrentProcessStep(this.currentProcessStep)
      .subscribe(
        () =>  {
          this._store.dispatch(showSuccess({}));
          this.goBack();
        },
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => this._store.dispatch(hideLoading()));
  }

  onAccept() {
    this._dialogService.openInfoDialog({
      width: '600px',
      data: {
        title: this.currentProcessStep.action,
        severity: SeverityEnum.INFO, 
        closeBtnText: 'Je confirme action :' + this.currentProcessStep.action,
        message: `Veuillez confirmer votre action`,
        class: 'bg-green'
      }
    })
    .afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this.validateDemandeCurrentProcessStep(DemandeProcessStepStatus.Valide);
      }
    });
  }

  onRefuse() {
    this._dialog.open(AdminCommunicationNoteRefusDialog,{
      width: '600px',
      panelClass: 'Warn',
      data: {
        demandeId: this.demandeId,
        employeMinisterielId: this.employeMinisterielId
      }
    })
    .afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this.validateDemandeCurrentProcessStep(DemandeProcessStepStatus.Refuse);
      }
    });
  }

  onCorrect() {
    this._dialogService.openInfoDialog({
      width: '600px',
      data: {
        title: 'Corrections nécessaires',
        severity: SeverityEnum.WARN,
        closeBtnText: 'Je confirme action :'+ this.currentProcessStep.action,
        message: `Veuillez confirmer votre action`,
        class: 'bg-orange'
      }
    })
    .afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this.validateDemandeCurrentProcessStep(DemandeProcessStepStatus.CorrectionsNecessaire);
      }
    });
  }

  createDemandeProgramme() {
    var dialogref = this._dialog.open(AdminDemandeProgrammeDialogComponent,
      {
        data: {
          demandeId: this.demandeId,
          niveauAccesList: this.niveauAccesList,
          typeAdmissionList: this.typeAdmissionList,
          typeProgrammeList: this.typeProgrammeList,
          typeDisciplineList: this.typeDisciplineList,
          niveauEtudeList: this.niveauEtudeList,
          typeDiplomeList: this.typeDiplomeList,
          langueList: this.langueList,
          demandeFaculteList: this.demandeFaculteList,
          isCenter: this.agrementation.isCenter,
        }
      })
      .afterClosed().subscribe((saveSuccess) => {
        if (saveSuccess) {
          this.loadDemandeProgrammeListView();
        }
      })
  }

  editDemandeProgramme(row: MetfpetServiceAgent.DemandeProgrammeViewModel) {
    var dialogref = this._dialog.open(AdminDemandeProgrammeDialogComponent,
      {
        data: {
          id: row.id,
          demandeId: this.demandeId,
          niveauAccesList: this.niveauAccesList,
          typeAdmissionList: this.typeAdmissionList,
          typeProgrammeList: this.typeProgrammeList,
          typeDisciplineList: this.typeDisciplineList,
          niveauEtudeList: this.niveauEtudeList,
          typeDiplomeList: this.typeDiplomeList,
          langueList: this.langueList,
          demandeFaculteList: this.demandeFaculteList,
          isCenter: this.agrementation.isCenter,
        }
      })
      .afterClosed().subscribe((saveSuccess) => {
        if (saveSuccess) {
          this.loadDemandeProgrammeListView();
        }
      })
  }

  deleteDemandeProgramme(row: MetfpetServiceAgent.DemandeProgrammeViewModel){
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention", 
        message: "Voulez-vous supprimer définitivement ce membre ?", 
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.deleteDemandeProgramme(row.id)
          .subscribe(
            () => {
              this._store.dispatch(showSuccess({message: 'Le membre a été supprimé'}));
              this.loadDemandeProgrammeListView();
            },
            (error) => this._store.dispatch(showError({message: error.response}))
        )
        .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  openAgrementManagementDialog(){
    var dialogRef = this._dialog.open(AdminDemandeManagementDialog,
      {
        width: '800px',
        data: {
          status: this._model.status,
          statusName: this._model.statusName,
          demandeId: this._model.id
        }
      }).afterClosed().subscribe(confirmed => {
        if (confirmed) {
          this._metfpetService.getDemande(this.demandeId).subscribe(data => {
            this._model = data;
            this._cd.markForCheck();
          });
        }
      });;
  }

  openDemandeCertificat(demande: MetfpetServiceAgent.DemandeDTO): void {
    if (demande.certificateLink) {
      window.open(demande.certificateLink, "_blank");
    }
  }

  openPaymentReceipDailog(){
    this._dialog.open(AdminDemandePaymentReceiptComponent, {
      width: "600px",
      data: {
        status : this._model.paymentReceiptStatusName,
        demandeId : this._model.id,
        paymentReceiptStatus : this.paymentReceiptStatus,
        type: this.paymentReceiptStatus === PaymentReceiptStatus.AAjouter ? "Add" : "Update", 
      }
    }).afterClosed().subscribe((saveSuccess)=>{
      if(saveSuccess){
        this.activeButtonReceiptPayment = false;
        this._cd.markForCheck();
        this._metfpetService.getDemande(this.demandeId).subscribe(data => {
          this.hasPaymentReceipet = data.hasPaymentReceipt;
          this.paymentReceiptStatusName = data.paymentReceiptStatusName;
          this.paymentReceiptStatus = data.paymentReceiptStatus;
          this.activeButtonReceiptPayment = true;
      })
    }
    })
  }
}



