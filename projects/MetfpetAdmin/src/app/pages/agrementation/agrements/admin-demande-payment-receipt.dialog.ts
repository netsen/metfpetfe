import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { DialogService, MetfpetServiceAgent, PaymentReceiptStatus, hideLoading, showLoading, showSuccess, validateForm } from 'MetfpetLib';

@Component({
  templateUrl: './admin-demande-payment-receipt.dialog.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminDemandePaymentReceiptComponent implements OnInit {
  paymentReceipt: MetfpetServiceAgent.AgrementationPaymentReceiptDTO;
  form: FormGroup;
  loading: boolean = true;
  fileExtension: string;
  isPdfFile: boolean;
  canRemovePaymentReceipt: boolean;
  submitButtonText: string;
  paymentReceiptFile: MetfpetServiceAgent.FileParameter;
  @ViewChild('selectedFilename') selectedFilename: ElementRef;

  constructor(
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialogRef: MatDialogRef<AdminDemandePaymentReceiptComponent>,
    private _dialogService: DialogService,
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.form = this._formBuilder.group({
      amount: [null, [Validators.required, Validators.max(3000000000)]],
      file: [null, Validators.required],
    })
    this.isPdfFile = true;
    this.canRemovePaymentReceipt = false;
  }

  ngOnInit(): void {
    this.getAgrementationPaymentReceipt();
    this.data.type == "Add" ? this.submitButtonText = "Enregistrer" : this.submitButtonText = "Modifier";
  }

  getAgrementationPaymentReceipt() {
    if (this.data.paymentReceiptStatus == PaymentReceiptStatus.AAjouter) {
      this.loading = false;
      return;
    }
    this._metfpetService.getAgrementationPaymentReceiptByDemandeId(this.data.demandeId).subscribe((data) => {
      this.paymentReceipt = data;
      this.canRemovePaymentReceipt = true;
      const fileName = this.paymentReceipt.link.split('/').pop()
      this.form.patchValue({
        amount: data.amount,
      })
      this._cd.markForCheck();
      this.loading = false;
      setTimeout(()=>{
        if(this.selectedFilename){
          this.selectedFilename.nativeElement.textContent = fileName;
        }
      }, 10)
      this.isPdfFile = true;
    })
  }

  displayPaymentReceipt(){
    if (this.paymentReceipt && this.paymentReceipt.link){
      window.open(this.paymentReceipt.link, '_blank')
    }
  }

  onClose() {
    this._dialogRef.close();
  }

  onFileChange(event: any) {
    const file = event.target.files[0];
    if (file) {
      this.paymentReceiptFile = {
        data: file,
        fileName: file.name.replace(/ /g, "_")
      };
      let filename: string;
      this.fileExtension = file.name.split('.').pop();
      this.isPdfFile = this.fileExtension === 'pdf';
      this.fileExtension === 'pdf' ? filename = file.name : filename = "";
      this.selectedFilename.nativeElement.textContent = filename;
    }
  }

  onSubmit() {
    validateForm(this.form);
    if (this.form.valid) {
      if (!this.isPdfFile || !this.paymentReceiptFile) return;
      this._store.dispatch(showLoading());
      const file = this.paymentReceiptFile;
      const amount = this.form.get('amount').value.toString();
      const status = `${PaymentReceiptStatus.Enregister}`;
      if (this.data.type === "Add") {
        this._metfpetService.uploadAgrementationPaymentReceipt(file, this.data.demandeId.toString(), amount, status).subscribe((res) => {
          this._dialogRef.close({ updated: true });
          this._store.dispatch(showSuccess({ message: "Le reçu a été ajouté avec succès!" }));
          this._store.dispatch(hideLoading());
        })
      } else {
        this._metfpetService.updatePaymentReceipt(file, this.paymentReceipt.id, this.data.demandeId.toString(), amount, status).subscribe((res) => {
          this._dialogRef.close({ updated: true });
          this._store.dispatch(showSuccess({ message: "Le reçu a été modfié avec succès!" }));
          this._store.dispatch(hideLoading());
        })
      }
    }
  }

  onRemovePaymentReceipt(){
    this._dialogService.openConfirmDialog({
      width: '420px',
      data: {
        title: "Attention",
        message: "Voulez-vous définitivement supprimer ce reçu de paiment ?",
        confirmBtnText: 'Oui'
      }
    }).afterClosed().subscribe(confirmed => {
      if (confirmed) {
        this._store.dispatch(showLoading());
        this._metfpetService.removePaymentReceipt(this.data.demandeId).subscribe((res)=>{
          this._dialogRef.close({ updated: true });
          this._store.dispatch(showSuccess({ message: "Le reçu a été supprimé avec succès!" }));
          this._store.dispatch(hideLoading());
        })
      }
    });
  }

}
