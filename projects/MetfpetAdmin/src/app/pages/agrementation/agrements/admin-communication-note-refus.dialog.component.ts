import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Component, 
  ViewEncapsulation,
	Inject, OnInit
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { 
  validateForm,
  showSuccess,
  MetfpetServiceAgent,
  showLoading,
  showException,
  showError,
  hideLoading,
} from 'MetfpetLib';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './admin-communication-note-refus.dialog.component.html',
  styleUrls: ['./admin-communication-note-refus.dialog.component.css']
})
export class AdminCommunicationNoteRefusDialog {
  form: FormGroup;
  title: string;
  model: MetfpetServiceAgent.CommunicationNoteDTO; 
  employeRoles: Array<MetfpetServiceAgent.EmployeRoleDTO>;

  constructor(
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _matDialogRef: MatDialogRef<AdminCommunicationNoteRefusDialog>, 
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.form = this._formBuilder.group({
      object: ["Refus Agrément"],
      message: [null, Validators.required],
    });
    this.title = "Refuser le dossier";
  }

  ngOnInit(){
    this.loadEmploRoles();
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      if(this.employeRoles?.length === 0) return;
      this.employeRoles = this.employeRoles.filter(x => x.name !== 'Demandeur')
      this._store.dispatch(showLoading());
      this.model = Object.assign({},
        this.form.value, 
      ); 
      this.model.demandeId = this.data.demandeId;
      this.model.employeMinisterielId = this.data.employeMinisterielId;
      this.model.employeRoleIds = this.employeRoles.map(x => x.id);

      var saveCommunicationNote$ = this._metfpetService.createCommunicationNote(MetfpetServiceAgent.CommunicationNoteDTO.fromJS(this.model));

      saveCommunicationNote$.subscribe(
        () => {
            this._store.dispatch(showSuccess({}));
            this._matDialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
  this._matDialogRef.close()
  }

  loadEmploRoles(){
    this._metfpetService.getEmployeRoleWithDemandeurListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { status : 1 }
    })).subscribe((res)=>{
      this.employeRoles = res.results
    })
  }

}
