import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { AuthService, AuthorityEnum, DialogService, EmployeeAccess, MetfpetServiceAgent, showSuccess } from 'MetfpetLib';

@Component({
  selector: 'app-admin-gestion-attribution',
  templateUrl: './admin-gestion-attribution.dialog.component.html',
  styleUrls: ['./admin-gestion-attribution.dialog.component.css']
})
export class AdminGestionAttributionDialog implements OnInit {
  form: FormGroup;
  title: string;
  requiredDemandeurField: boolean;
  demande: MetfpetServiceAgent.DemandeDTO;
  gestionnaireList: Array<MetfpetServiceAgent.EmployeMinisterielRowViewModel>;
  attributionList: Array<MetfpetServiceAgent.EmployeMinisterielRowViewModel>;
  canModify:boolean;

  constructor(
    private _dialogRef: MatDialogRef<AdminGestionAttributionDialog>,
    private _formBuilder: FormBuilder,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _authService:AuthService,
    private _store: Store<any>,
    private _matDialog: MatDialog,
    private _dialog:DialogService,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: MetfpetServiceAgent.DemandeViewModel
  ) {
    this.title = "Gérer l'attribution";
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      gestionnaire: new FormControl(this.data.gestionnaireId, []),
      attribution: new FormControl(this.data.attributionId, [])
    })
    this.checkIfCanModify();
    this._metfpetService.getEmployeMinisterielListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { 'isActive': '1' }
    })).subscribe({
      next: (value) => {
        this.gestionnaireList = value.results.filter(v => v.acces == (EmployeeAccess.Administrator as any) || v.acces == (EmployeeAccess.AgrementSupervisor as any) );
        this.attributionList = value.results.filter(v => v.acces == (EmployeeAccess.AgrementSupervisor as any) || v.acces == (EmployeeAccess.AgrementAgent as any) || v.employeRoleName === 'Ministre');
      }
    })
  }
  
  public onSubmit(): void {
    this._metfpetService.addAttribution(MetfpetServiceAgent.CreateAttributionDTO.fromJS({
      id: this.data.id,
      attributionId: this.form.value.attribution,
      gestionnaireId: this.form.value.gestionnaire,
    })).subscribe({
      next: (response) => {
        this._dialogRef.close({ updated: true });
        this._store.dispatch(showSuccess({}));
      }
    })
  }

  deleteAttribution(): void {
    this._dialog.openConfirmDialog({
      data:{
        title:"Confirmation de Supression",
        message:"Êtes-vous sûr de vouloir supprimer cette attribution?"
      }
    }).afterClosed()
    .subscribe(res=>{
      if(res){
        this._metfpetService.addAttribution(MetfpetServiceAgent.CreateAttributionDTO.fromJS({
          id: this.data.id,
          attributionId: null,
          gestionnaireId: null,
        })).subscribe({
          next: (response) => {
            this._dialogRef.close({ updated: true });
            this._store.dispatch(showSuccess({message:"L'attribution a été supprimée avec succès!"}));
          }
        })
      }
    })
  }

  checkIfCanModify(){
    let userType = this._authService.getUserType();
    this.canModify = (userType == AuthorityEnum.AdministrateurMinisteriel || 
                      userType == AuthorityEnum.Administrateur ||
                      userType== AuthorityEnum.AgrementSupervisorMinisteriel);
    if(!this.canModify){
      this.form.disable();
      this.form.updateValueAndValidity();
    }
  }

  onClose() {
    this._dialogRef.close()
  }
}
