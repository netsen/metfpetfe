import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import {
  AppSettings,
  Settings,
  MetfpetServiceAgent,
  PerfectScrollService,
  AuthService,
} from 'MetfpetLib';
import { MinistreTabs, Tabs } from './admin-agrementation-tab.component';
import { AdminDemandeDialog } from './admin-demande.dialog.component';
import { AgrementationStatus } from 'projects/MetfpetLib/src/public-api';

@Component({
  templateUrl: './admin-nouvelle-demande.component.html',
  styleUrls: ['./admin-nouvelle-demande.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminNouvelleDemandeComponent implements OnInit, AfterViewInit {

  settings: Settings;
  title: string;
  navs = Tabs;
  clickedButton: string;
  clickValue: MetfpetServiceAgent.TypeDemandeViewModel;

  testDataShown: any;
  iTestData: number;
  cardNumber: number;
  limitItem: number;
  screeSize: number;
  hideLeftArrow: boolean;
  hideRightArrow: boolean;
  loading: boolean;
  emptyArray = [];
  filter = { typeDemandeName: 'Etablissements de soins' };
  searchForm: FormGroup;
  agrementationlist: Array<MetfpetServiceAgent.AgrementationViewModel>;
  typeAgrement1: Array<MetfpetServiceAgent.AgrementationViewModel>;
  typeAgrement2: Array<MetfpetServiceAgent.AgrementationViewModel>;
  typeDemandesList: Array<MetfpetServiceAgent.TypeDemandeViewModel>;
  demandeur: MetfpetServiceAgent.DemandeurProfileViewModel

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _authService:AuthService,
    private _matDialog: MatDialog,
  ) {
    this.settings = this.appSettings.settings;
    if (this.isMinistre())
    {
      this.navs = MinistreTabs;
    }
    else
    {
      this.navs = Tabs;
    }
  }

  isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }

  ngOnInit(): void {
    this.screeSize = window.innerWidth;
    this.hideLeftArrow = true;
    this.hideRightArrow = true;
    this.loading = true;
    this.iTestData = 0;
    this.cardNumber = 4;

    this._metfpetService.getTypeDemandeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {isActive:+true}
    })).subscribe((data) => {
      this.typeDemandesList = data.results.sort().reverse();
      this.isActive(this.typeDemandesList[0]);
      this.onClick(this.typeDemandesList[0]);
      this._searchAgrementationList(this.typeDemandesList[0].name);
    })
  }
  ngAfterViewInit(): void {
  }
  logEvent(event: any, isHover: boolean) {
    let className = 'typeDemandehover';
    let lisclass = event.target.classList.value;
    if (isHover) {
      if (event.target.innerText.length >= 50) {
        if (!event.target.classList.value.includes(className)) {
          event.target.setAttribute("class", lisclass + ' ' + className);
          // this._cd.markForCheck();
        }
      }
    } else {
      if (lisclass.includes(className)) {
        event.target.setAttribute("class", lisclass.replace(className, ''));
      }
    }
  }
  protected _searchAgrementationList(filter: string) {
    this._metfpetService.getAgrementationListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1,
      filters: {
        typeDemandeName: filter,
        status: AgrementationStatus.Active
      }
    })).subscribe((data) => {
      this.agrementationlist = data.results;
      if (this.agrementationlist.length > this.cardNumber) {
        this.hideRightArrow = false;
        this.hideLeftArrow = false;
      }
      this.loading = false;
      this.iTestData = 0;
      this.setShownData();
      this._cd.markForCheck();
    })
  }

  changes($event) {
    this.loading = true;
    this._searchAgrementationList($event.target.value);
  }

  onClick(model: MetfpetServiceAgent.TypeDemandeViewModel) {
    this.clickedButton = model.name
    this.clickValue = model;
  }

  isActive(model: MetfpetServiceAgent.TypeDemandeViewModel) {
    return this.clickValue === model
  }

   openDemandeDialog(agrementation: any) {
    var dialogRef = this._matDialog.open(AdminDemandeDialog,
      {
        width: '600px',
        data: {
          agrementation: agrementation,
        }
      });
  }

  // *************Caroussel algo*******************

  setShownData() {
    if (this.screeSize < 990) {
      this.cardNumber = 2
    }
    if (this.screeSize < 480) {
      this.cardNumber = 1
    }
    this.testDataShown = this.agrementationlist.slice(this.iTestData * this.cardNumber, (this.iTestData + 1) * this.cardNumber);
  }

  next() {
    if (((this.iTestData + 1) * this.cardNumber) < this.agrementationlist.length) {
      this.hideLeftArrow = false;
      this.iTestData = this.iTestData + 1;
      this.setShownData();
      this.limitItem = this.limitItem - ((this.iTestData + 1) * this.cardNumber);
      if (this.limitItem <= 0) {
        this.hideRightArrow = true;
        this.limitItem = 0
      }
    } else {
      this.hideRightArrow = true;
    }
  }
  preview() {
    if (this.iTestData != 0) {
      this.hideRightArrow = false;
      this.iTestData = this.iTestData - 1;
      this.setShownData();
      this.limitItem = this.limitItem + ((this.iTestData + 1) * this.cardNumber);
      if (this.limitItem >= this.agrementationlist.length) {
        this.hideLeftArrow = true;
      }
    } else {
      this.hideLeftArrow = true;
    }
  }
  // ****************End Caroussel algo

  // Type de demande srolling algo

  scrollLeft() {
    const container = document.querySelector('#typeDemande');
    container.scrollLeft -= 300;
  }

  scrollRight() {
    const container = document.querySelector('#typeDemande');
    container.scrollLeft += 300;
  }

}


