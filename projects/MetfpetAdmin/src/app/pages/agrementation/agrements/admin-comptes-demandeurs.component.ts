import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { startWith, debounceTime, distinctUntilChanged } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent, 
  MetfpetServiceAgent,
  PerfectScrollService,
  DemandeurStatusValues,
  showLoading,
  hideLoading,
  ExportService,
  AuthService,
} from 'MetfpetLib';
import { MinistreTabs, Tabs } from './admin-agrementation-tab.component';
import { UiPath } from '../../ui-path';

export const DEMANDEUR_EXPORT_CONFIGS = {
  identifiant: {
    header: "Numéro d’identification",
    key: "identifiant",
    width: 25,
  },
  name: {
    header: "Nom/Société",
    key: "name",
    width: 15,
  },
  firstName: {
    header: "Prénom",
    key: "firstName",
    width: 15,
  },
  email: {
    header: "Adresse courriel",
    key: "email",
    width: 45,
  },
  demandeDate: {
    header: "Date de la demande",
    key: "demandeDate",
    width: 20,
  },
  demandeurStatusName: {
    header: "Statut",
    key: "demandeurStatusName",
    width: 20,
  },
};
@Component({
  templateUrl: './admin-comptes-demandeurs.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminComptesDemandeursComponent extends BaseTableComponent {
  
  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchIdentifiant', {static: true}) searchIdentifiant: ElementRef;

  settings: Settings;
  title: string;
  navs = Tabs;
  sessionsList: Array<any>;
  statusList: Array<any>;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _exportService: ExportService,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Gestion des comptes demandeurs';
    this.statusList = DemandeurStatusValues;
    this.sort = {prop: 'identifiant', dir: 'asc'};
    if (this.isMinistre())
    {
      this.navs = MinistreTabs;
    }
    else
    {
      this.navs = Tabs;
    }
  }

  isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getSessionBACList().subscribe(data => {
      this.sessionsList = data.map(x => {return {name: x}});
      this._cd.markForCheck();
    });

    combineLatest([
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      fromEvent(
        this.searchIdentifiant.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('identifiant').value}
            })
          ),
      this.searchForm.get('email').valueChanges
        .pipe(
          startWith(this.searchForm.get('email').value)
        ),
      this.searchForm.get('periode').valueChanges
        .pipe(
          startWith(this.searchForm.get('periode').value)
        ),
      this.searchForm.get('demandeurStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('demandeurStatus').value)
        ),
    ])
    .subscribe((
      [
        eventSearchName, 
        eventFirstname,
        eventIdentifiant,
        email,
        periode,
        demandeurStatus
      ]) => {
      this.searchForm.patchValue({
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null,
        identifiant: eventIdentifiant ? eventIdentifiant['target'].value : null, 
        email,
        periode,
        demandeurStatus
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      name: null,
      firstName: null,
      identifiant: null,
      email: null,
      periode: null,
      demandeurStatus: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getDemandeurListPage(criteria);
  }

  open(selectedDossier: MetfpetServiceAgent.DemandeurViewModel) {
    this._router.navigate([`${UiPath.admin.agrementation.dossiers.dossier}/${selectedDossier.id}`], { state: { previousPage: 'Gestion des comptes demandeurs' } });
  }

  create() {
    this._router.navigate([UiPath.admin.agrementation.dossiers.add], { state: { previousPage: 'Gestion des comptes demandeurs' } });
  }

  public exportListOfDemandeur() {
    this._store.dispatch(showLoading());
    this._metfpetService.getDemandeurListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1,
      filters: this.searchForm.value,
    }))
      .subscribe(data => 
        this._exportService.exportDemandeurs(data.results, DEMANDEUR_EXPORT_CONFIGS, "Demandeur_"))
      .add(() => this._store.dispatch(hideLoading()));
  }

}
