import { 
  ChangeDetectorRef,
  ChangeDetectionStrategy, 
  Component, 
  ViewEncapsulation,
  Inject, OnInit
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { 
  validateForm,
  showSuccess,
  MetfpetServiceAgent,
  showLoading,
  showException,
  showError,
  hideLoading,
  emailValidator,
  DemandePersonnelMemberStatus,
  PersonnelType,
  SeverityEnum,
  DialogService,
} from 'MetfpetLib';
import { Observable } from 'rxjs';

export const NORMAL_CSS = 'row mb-2';
export const ERROR_CSS = 'row mb-2 warn-color';
@Component({
  templateUrl: './admin-demande-personnel-member-dialog.component.html',
  styleUrls: ['./admin-demande-detail.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminDemandePersonnelMemberDialogComponent {

  form: FormGroup;
  title: string; 
  loading: boolean;
  hasFile: boolean = false;
  isCreation: boolean;
  isMandatoryMember: boolean;
  isAdministratif: boolean;
  personnelDocumentList: Array<MetfpetServiceAgent.AttachedPersonnelDocumentDTO>;
  model: MetfpetServiceAgent.DemandePersonnelMemberDTO;
  personnelMemberList: Array<MetfpetServiceAgent.PersonnelMemberDTO>;
  programmeList: Array<MetfpetServiceAgent.DemandeProgrammeDTO>;
  disciplineList: Array<MetfpetServiceAgent.DemandeDisciplineDTO>;
  filteredDisciplineList: Array<MetfpetServiceAgent.DemandeDisciplineDTO>;
  personnelType: PersonnelType;

  constructor(
    private _cd: ChangeDetectorRef,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _dialogService: DialogService,
    private _matDialogRef: MatDialogRef<AdminDemandePersonnelMemberDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.personnelMemberList = data.personnelMemberList;
    this.programmeList = data.programmeList;
    this.disciplineList = data.disciplineList;
    this.isMandatoryMember = data.isMandatoryMember;
    this.isAdministratif = data.isAdministratif;
    this.personnelType = data.isAdministratif ? PersonnelType.Administratif : PersonnelType.Formateur;

    this.form = this._formBuilder.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'firstName': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'dateNaissance': [null, Validators.required],
      'email': [null, Validators.compose([Validators.required, emailValidator])],
      'phone': [null, Validators.compose([Validators.required, Validators.pattern("^[0-9]{9}$")])],
      'status': true,
      'adresse': null,
      'profession': null,
    });
    if (this.isMandatoryMember) {
      this.form.addControl('personnelMemberId', this._formBuilder.control(null, [Validators.required]));
    } else {
      this.form.addControl('poste', this._formBuilder.control(null, [Validators.required]));
    }

    if (!this.isAdministratif) {
      this.form.addControl('programmes', this._formBuilder.control([]));
      this.form.addControl('disciplines', this._formBuilder.control([]));
    }

    if (data.id) {
      this.isCreation = false;
      this.title = 'Editer d’un membre du personnel';
      this._metfpetService.getDemandePersonnelMember(data.id).subscribe((data) => {
        this.model = data;
        this.filteredDisciplineList = this.disciplineList.filter(x => this.model.programmes.includes(x.demandeProgrammeId));
        this.form.patchValue({
          poste: this.model.poste,
          personnelMemberId: this.model.personnelMemberId,
          name: this.model.name,
          firstName: this.model.firstName,
          dateNaissance: this.model.dateNaissance,
          email: this.model.email,
          phone: this.model.phone,
          status: this.model.status === <any>DemandePersonnelMemberStatus.Active,
          adresse: this.model.adresse,
          programmes: this.model.programmes,
          disciplines: this.model.disciplines,
          profession: this.model.profession,
        });
      });
      this.loadPersonnelDocuments(data.id);
    } else {
      this.isCreation = true;
      this.title = this.isMandatoryMember ? 'Ajouter membres du personnel obligatoire' : 'Ajouter d’autres membres du personnel';
      this.model = data;
    }
  }

  loadPersonnelDocuments(id: string) {
    this._metfpetService.getAttachedDocumentOfPersonnel(id).subscribe((data) => {
      this.personnelDocumentList = data;
      this._cd.markForCheck();
    });
  }
  public getCss(attachedDocument: MetfpetServiceAgent.AttachedPersonnelDocumentDTO) {
    return attachedDocument.link ? NORMAL_CSS : ERROR_CSS;
  }

  public getSelectorId(attachedDocument: MetfpetServiceAgent.AttachedPersonnelDocumentDTO) {
    return 'selector' + attachedDocument.id;
  }

  public onFileUpload(event: any, attachedDocument: MetfpetServiceAgent.AttachedPersonnelDocumentDTO) {
    if (event.target.files && event.target.files.length > 0) {
      this._store.dispatch(showLoading());

      let file : Blob = event.target.files[0];
      let uploadedFileName : string = event.target.files[0].name;
      var fileReader = new FileReader();
      fileReader.onloadend = function () {
        
      }
      fileReader.readAsBinaryString(file);
      
      let fileParameter = 
      {
        data : file,
        fileName : uploadedFileName
      };

      this._metfpetService.uploadAttachedPersonnelDocument(fileParameter, attachedDocument.id)
        .subscribe(
          (data) => {
            this.personnelDocumentList = this.personnelDocumentList.map(x => this.replaceAttachedPersonnelDocumentDTO(x, data));
            this._cd.markForCheck();
            this._store.dispatch(showSuccess({}));
          },
          (error) => this._store.dispatch(showError({message: error.message}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public uploadDocument(attachedDocument: MetfpetServiceAgent.AttachedPersonnelDocumentDTO): void {
    document.getElementById('selector' + attachedDocument.id).click();
  }

  public openDocument(attachedDocument: MetfpetServiceAgent.AttachedDocumentSectionDTO): void {
    window.open(attachedDocument.link, "_blank");
  }

  public isValidToDownloadModel(attachedDocument: MetfpetServiceAgent.AttachedPersonnelDocumentDTO){
    return attachedDocument.modelLink && attachedDocument.modelLink.length > 0;
  }
  
  public openDocumentModel(attachedDocument: MetfpetServiceAgent.AttachedPersonnelDocumentDTO): void {
    window.open(attachedDocument.modelLink, "_blank");
  }
  
  private replaceAttachedPersonnelDocumentDTO(item: MetfpetServiceAgent.AttachedPersonnelDocumentDTO, data: MetfpetServiceAgent.AttachedPersonnelDocumentDTO) : MetfpetServiceAgent.AttachedPersonnelDocumentDTO {
    if (item.id == data.id)
      return MetfpetServiceAgent.AttachedPersonnelDocumentDTO.fromJS(data);
    return item;
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      
      if (this.isMandatoryMember) {
        let personnelMemberId = this.form.get('personnelMemberId').value;
        this.model.poste = this.personnelMemberList.filter(x => x.id == personnelMemberId).map(x => x.poste)[0];
      }
      this.model = Object.assign({},
        this.model,
        this.form.value,
      );
      
      if (this.model.poste && this.model.poste.startsWith("Fondateur") && !this.form.get('adresse').value) {
        this._dialogService.openInfoDialog({
          data: {
            severity: SeverityEnum.ERROR, 
            message: 'Veuillez saisir l\'adresse de résidence'
          }
        });
        return;
      }
      this._store.dispatch(showLoading());
      var saveDemandePersonnelMember$: Observable<MetfpetServiceAgent.DemandePersonnelMemberDTO>;

      if (this.isCreation) {
        this.model.personnelType = <any>this.personnelType;
        saveDemandePersonnelMember$ = this._metfpetService.createDemandePersonnelMember(MetfpetServiceAgent.DemandePersonnelMemberDTO.fromJS(this.model));
      } else {
        saveDemandePersonnelMember$ = this._metfpetService.updateDemandePersonnelMember(MetfpetServiceAgent.DemandePersonnelMemberDTO.fromJS(this.model));
      }

      saveDemandePersonnelMember$.subscribe(
        () => {
            this._store.dispatch(showSuccess({}));
            this._matDialogRef.close(true);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  onClose() {
    this._matDialogRef.close();
  }

  changeProgramme(programmeIds: any) {
    this.filteredDisciplineList = this.disciplineList.filter(x => programmeIds.includes(x.demandeProgrammeId))
  }
}
