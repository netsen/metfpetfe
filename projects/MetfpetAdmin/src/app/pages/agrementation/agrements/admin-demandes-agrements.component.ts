import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef, 
  Renderer2
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { startWith, debounceTime, distinctUntilChanged } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  BaseTableComponent, 
  MetfpetServiceAgent,
  PerfectScrollService,
  DemandeStatusValues,
  showLoading,
  hideLoading,
  ExportService,
  EmployeeAccess,
  AuthService,
} from 'MetfpetLib';
import { MinistreTabs, Tabs } from './admin-agrementation-tab.component';
import { MatDialog } from '@angular/material/dialog';
import { AdminGestionAttributionDialog } from './admin-gestion-attribution.dialog.component';

export const DEMANDES_EXPORT_CONFIGS = {
  typeDemandeName: {
    header: "Catégorie",
    key: "typeDemandeName",
    width: 25,
  },
  agrementationName: {
    header: "Type de demande",
    key: "agrementationName",
    width: 25,
  },
  numero: {
    header: "Numero",
    key: "numero",
    width: 25,
  },
  demandeurName: {
    header: "Nom",
    key: "demandeurName",
    width: 25,
  },
  demandeurFirstName: {
    header: "Prénom",
    key: "demandeurFirstName",
    width: 25,
  },
  creationTime: {
    header: "Date de la demande",
    key: "creationTime",
    width: 25,
  },
  status: {
    header: "Statut",
    key: "status",
    width: 25,
  },
};



@Component({
  templateUrl: './admin-demandes-agrements.component.html',
  styleUrls:['./admin-demandes-agrements.component.css'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminDemandesAgrementsComponent extends BaseTableComponent {  
  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchNumero', {static: true}) searchNumero: ElementRef;
  @ViewChild('searchIdentifiant', {static: true}) searchIdentifiant: ElementRef;

  settings: Settings;
  title: string;
  navs = Tabs;
  sessionsList: Array<any>;
  statusList: Array<any>;
  agrementations : Array<MetfpetServiceAgent.AgrementationViewModel>;
  typeDemandes : Array<MetfpetServiceAgent.TypeDemandeViewModel>
  attributionValues = [{name:"Oui",value:true},{name:"Non",value:false}];
  superviseurList:Array<any>;
  inChargeList:Array<any>;
  myWorkingFunction = false;
  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    private _route: ActivatedRoute,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private renderer: Renderer2,
    private _dialog:MatDialog,
    private _authService:AuthService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Dossier des demandeurs';
    this.statusList = DemandeStatusValues;
    this.sort = {prop: 'demandeurName', dir: 'asc'};
    if (this.isMinistre())
    {
      this.navs = MinistreTabs;
    }
    else
    {
      this.navs = Tabs;
    }
  }

  isMinistre() {
    return this._authService.getUserType() == 'ministre';
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getSessionBACList().subscribe(data => {
      this.sessionsList = data.map(x => {return {name: x}});
      this._cd.markForCheck();
    });
    this._metfpetService.getEmployeMinisterielListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: { 'isActive': '1' }
    })).subscribe({
      next: (value) => {
        this.superviseurList = value.results
        .filter(v => v.acces == (EmployeeAccess.Administrator as any) || v.acces == (EmployeeAccess.AgrementSupervisor as any) || v.employeRoleName === 'Ministre')
        .map(v=>({id:v.id,fullName:`${v.firstName} ${v.name}`}));

        this.inChargeList = value.results
        .filter(v => v.acces == (EmployeeAccess.AgrementSupervisor as any) || v.acces == (EmployeeAccess.AgrementAgent as any) || v.employeRoleName === 'Ministre')
        .map(v=>({id:v.id,fullName:`${v.firstName} ${v.name}`}));
      }
    })

    this._metfpetService.getTypeDemandeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe((data)=>{
      this.typeDemandes = data.results;
    });

    this._metfpetService.getAgrementationListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe((data)=>{
      this.agrementations = data.results;
    });

    combineLatest([
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      fromEvent(
        this.searchNumero.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('numero').value}
            })
          ),
      fromEvent(
        this.searchIdentifiant.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('identifiant').value}
            })
          ),
      this.searchForm.get('agrementationId').valueChanges
        .pipe(
          startWith(this.searchForm.get('agrementationId').value)
        ),
      this.searchForm.get('typeDemandeId').valueChanges
        .pipe(
          startWith(this.searchForm.get('typeDemandeId').value)
        ),
      this.searchForm.get('periode').valueChanges
        .pipe(
          startWith(this.searchForm.get('periode').value)
        ),
      this.searchForm.get('demandeurStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('demandeurStatus').value)
        ),
      this.searchForm.get('attribution').valueChanges
        .pipe(
          startWith(this.searchForm.get('attribution').value)
        ),
      this.searchForm.get('inCharge').valueChanges
        .pipe(
          startWith(this.searchForm.get('inCharge').value)
        ),
      this.searchForm.get('superviseur').valueChanges
        .pipe(
          startWith(this.searchForm.get('superviseur').value)
        ),
      this.searchForm.get('attributedTo').valueChanges
        .pipe(
          startWith(this.searchForm.get('attributedTo').value)
        ),
    ])
    .subscribe((
      [
        eventSearchName, 
        eventFirstname,
        eventNumero,
        eventIdentifiant,
        agrementationId,
        typeDemandeId,
        periode,
        demandeurStatus,
        attribution,
        inCharge,
        superviseur,
        attributedTo
      ]) => {
      this.searchForm.patchValue({
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null, 
        numero: eventNumero ? eventNumero['target'].value : null,
        identifiant: eventIdentifiant ? eventIdentifiant['target'].value : null,
        agrementationId,
        typeDemandeId,
        periode,
        demandeurStatus,
        attribution,
        inCharge,
        superviseur,
        attributedTo
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      name: null,
      firstName: null,
      numero: null,
      identifiant: null,
      periode: null,
      demandeurStatus: null,
      agrementationId: null,
      typeDemandeId: null,
      expirationDateFrom: null,
      expirationDateTo: null,
      attribution:null,
      inCharge:null,
      superviseur:null,
      attributedTo:null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getDemandeListPage(criteria);
  }

  open(row: MetfpetServiceAgent.DemandeViewModel) {
    this._router.navigate([`agrements/agrements/view/${row.id}`]);
  }

  openDocument(demande: MetfpetServiceAgent.DemandeViewModel): void {
    if (demande.certificateLink) {
      window.open(demande.certificateLink, "_blank");
    }
  }

  public exportListOfDemandes() {
    this._store.dispatch(showLoading());
    this._metfpetService.getDemandeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1,
      filters: this.searchForm.value,
    }))
      .subscribe(data => 
        this._exportService.exportDemandes(data.results, DEMANDES_EXPORT_CONFIGS, "Demandes_"))
      .add(() => this._store.dispatch(hideLoading()));
  }

  /**
   * add or remove the class active on the button element
   * @param event event
   */
  toggleActive(event: any) {
    const target = event.target || event.srcElement;
    if (target.classList.contains('active')) {
      return;
    }
    const buttonLabels = document.getElementsByClassName('button_label');
    const buttonLabel = target.closest('.button_label');
    if (buttonLabel && buttonLabel.classList.contains('button_label')) {
      for (let i = 0; i < buttonLabels.length; i++) {
        this.renderer.removeClass(buttonLabels[i], 'active');
      }
      this.renderer.addClass(buttonLabel, 'active');
    }
  }

  openAttributionDialog(row:any){
    this._dialog.open(AdminGestionAttributionDialog,{
      data:row,
      minWidth:"50vw"
    }).afterClosed().subscribe(response=>{
      if(response?.updated){
        this.triggerSearch();
      }
    })
  }
  
  viewMyBasketFunction(visible:boolean){
    if(this.myWorkingFunction == visible)
      return;
    this.myWorkingFunction = visible;
    if(visible){
      this.searchForm.controls['attributedTo'].setValue(this._authService.getUserId());
      this.searchForm.controls['attributedTo'].updateValueAndValidity();
    }else{
      this.searchForm.controls['attributedTo'].setValue(null);
      this.searchForm.controls['attributedTo'].updateValueAndValidity();
    }
  }
  
}
