import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

import {
  MetfpetServiceAgent,
  validateForm,
  showException,
  showLoading,
  showSuccess,
  DemandeStatus,
  hideLoading,
} from 'MetfpetLib';
import { Store } from '@ngrx/store';
import { AdminDemandeModificationDialog } from './admin-demande-modification-dialog.component';

function hasError(c: AbstractControl): { [key: string]: boolean } | null {
  const emailControl = c.get('email');
  const identifiantControl = c.get('identifiant');

  if ((emailControl.value != null && emailControl.value != '') || (identifiantControl.value != null && identifiantControl.value != '')) {
    return null
  }
  return { 'required': true }
}

@Component({
  selector: 'app-demande-dialog',
  templateUrl: './admin-demande.dialog.component.html',
})
export class AdminDemandeDialog {
  form: FormGroup;
  title: string;
  requiredDemandeurField: boolean;
  demande: MetfpetServiceAgent.DemandeDTO;

  constructor(
    private _dialogRef: MatDialogRef<AdminDemandeDialog>,
    private _formBuilder: FormBuilder,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _store: Store<any>,
    private _matDialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.title = "Création d’une demande";
    this.requiredDemandeurField = false;
    this.form = this._formBuilder.group({
      demandeur: this._formBuilder.group({
        identifiant: [null, Validators.compose([Validators.maxLength(14), Validators.minLength(14)])],
        email: [null, Validators.email],
      }, { validators: hasError })
    })
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this.demande = MetfpetServiceAgent.DemandeDTO.fromJS(
        Object.assign(
          this.form.value,
          {
            status: DemandeStatus.AnalyseEnCours,
            currentProcessStep: 1,
            typeDemandeId: this.data.agrementation.typeDemandeId,
            agrementationId: this.data.agrementation.id,
            email: this.form.get('demandeur.email').value,
            identifiant: this.form.get('demandeur.identifiant').value,
            isModification: this.data.agrementation.isModification
          })
      );
      if (this.data.agrementation.hasAgrementPrealable) {
        this._store.dispatch(showLoading());
        this._metfpetService.getApprovedAgrementPrealable(this.demande).subscribe(
          (data) => {
            this._dialogRef.close(true);
            this._store.dispatch(hideLoading());
              this._matDialog.open(AdminDemandeModificationDialog,
                {
                  width: '550px',
                  data : {
                    agrementation: this.data.agrementation,
                    approuvedDemandeList: data,
                  }
                }).afterClosed().subscribe(agrementPrealable =>
                {
                  if (agrementPrealable) {
                    this.demande.agrementPrealable = agrementPrealable;
                    this.createDemande();
                  }
                });
          },
          error => this._store.dispatch(showException({ error: error }))
        ).add(() => {
          this._store.dispatch(hideLoading());
        });
      } 
      else 
      {
        this.createDemande();
      }
    }
  }

  private createDemande() {
    this._store.dispatch(showLoading());
    this._metfpetService.createDemande(this.demande).subscribe(
      () => {
        this._store.dispatch(showSuccess({}));
        this._dialogRef.close(true);
      },
      error => this._store.dispatch(showException({ error: error }))
    ).add(() => {
      this._store.dispatch(hideLoading());
    });
  }

  onClose() {
    this._dialogRef.close()
  }
}
