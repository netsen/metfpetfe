import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../../ui-path';
import { 
  AuthService,
} from 'MetfpetLib';

export const Tabs = [
  { 
    routerLink: '/' + UiPath.admin.agrementation.agrements.list, 
    label: 'Demandes d’agréments' 
  },
  { 
    routerLink: '/' + UiPath.admin.agrementation.agrements.demandeurs, 
    label: 'Gestion des comptes demandeurs' 
  },
  { 
    routerLink: '/' + UiPath.admin.agrementation.agrements.demande, 
    label: 'Créer une nouvelle demande' 
  }
];

export const MinistreTabs = [
  { 
    routerLink: '/' + UiPath.admin.agrementation.agrements.list, 
    label: 'Demandes d’agréments' 
  },
  { 
    routerLink: '/' + UiPath.admin.agrementation.agrements.demandeurs, 
    label: 'Gestion des comptes demandeurs' 
  }
];

@Component({
  templateUrl: './admin-agrementation-tab.component.html',
})
export class AdminAgrementationTabComponent {

  navs = Tabs;
  title: string;
  routerEventSubscription: Subscription;
  
  constructor(private router: Router, private _authService: AuthService) {
    this.title = 'Agrémentation - Demandes d’agréments';
    if (this._authService.getUserType() == 'ministre')
    {
      this.navs = MinistreTabs;
    }
    else
    {
      this.navs = Tabs;
    }
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = 'Agrémentation - ' + selectedTab.label;
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
