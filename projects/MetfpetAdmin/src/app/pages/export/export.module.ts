import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExportInscriptionComponent } from './export-inscription.component';
import { ExportInscriptionDialog } from './export-inscription.dialog';
import { SharedModule } from '../../shared/shared.module';

export const routes = [
	{
	  path: '',
	  redirectTo: 'export-inscription',
	  pathMatch: 'full'
	},
	{ 
	  path: 'export-inscription', 
	  component: ExportInscriptionComponent, 
	  data: { breadcrumb: 'Exporter décision et inscriptions' }
	},
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [
    SharedModule,
    ExportInscriptionComponent
  ],
  declarations: [
    ExportInscriptionComponent,
    ExportInscriptionDialog
  ],
  entryComponents: [
    ExportInscriptionDialog
  ]
})
export class ExportModule {

}
