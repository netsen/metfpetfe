import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ViewChild, 
  ViewEncapsulation,
  ElementRef 
} from '@angular/core';
import { 
  DialogService, 
  MetfpetServiceAgent,
  Settings,
  AppSettings,
  AdmissionDecisionValues,
  showLoading,
  hideLoading,
  ExportService,
  PerfectScrollService,
  BaseTableComponent,
} from 'MetfpetLib';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators/';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { ExportInscriptionDialog } from './export-inscription.dialog';

@Component({
  templateUrl: './export-inscription.component.html',
  styleUrls: ['./export-inscription.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExportInscriptionComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchPVNumber', {static: true}) searchPVNumber: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;

  settings: Settings;
  title: string;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  faculteList: Array<MetfpetServiceAgent.FaculteRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  sessionsBACList: Array<any>;
  optionsBACList: Array<MetfpetServiceAgent.OptionBACRowViewModel>;
  decisionList: Array<any>;
  centreExamenList: Array<MetfpetServiceAgent.CentreExamenRowViewModel>;
  lyceesList: Array<MetfpetServiceAgent.LyceeRowViewModel>;
  genreList: Array<MetfpetServiceAgent.Genre>;
  anneeList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  sessionAdmissionList: Array<MetfpetServiceAgent.SessionAdmissionRowViewModel>;
  centreConcoursList: Array<MetfpetServiceAgent.CentreConcoursRowViewModel>;
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissement>;
  
  constructor(
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    private _dialogService: DialogService,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _exportService: ExportService,
    public appSettings: AppSettings,
    protected router: Router
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Export - Résultats d’admission';
    this.decisionList = AdmissionDecisionValues;
    this.sort = {prop: 'identifiantNationalEleve', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getStatusEtablissementList().subscribe(data => {
      this.statusEtablissementList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getSessionAdmissionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.sessionAdmissionList = data.results;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getCentreConcoursListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.centreConcoursList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getGenreList().subscribe(data => {
      this.genreList = data;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getCentreExamenListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.centreExamenList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getLyceeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.lyceesList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getSessionBACList().subscribe(data => {
      this.sessionsBACList = data.map(x => {return {name: x}});
      this._cd.markForCheck();
    });

    this._metfpetService.getOptionBACListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.optionsBACList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getFaculteListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.faculteList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeList = data.results;
      this._cd.markForCheck();
    });

    this.searchForm.get('institution').valueChanges.pipe(
      startWith(this.searchForm.get('institution').value),
      tap(institution => {
        this.faculteList = [];
        this.programList = [];
        if (!this.isFirstSearch) {
          this.searchForm.get('faculte').setValue(null);
          this.searchForm.get('programme').setValue(null);
        }
        if (!institution) {
          return;
        }
        this._loadFaculte(institution);
        this._loadPrograms(institution, null);
      })
    ).subscribe();
    
    this.searchForm.get('faculte').valueChanges.pipe(
      startWith(this.searchForm.get('faculte').value),
      tap(faculte => {
        this.programList = [];
        if (!this.isFirstSearch) {
          this.searchForm.get('programme').setValue(null);
        }
        if (!faculte) {
          return;
        }
        this._loadPrograms(this.searchForm.get('institution').value, faculte);
      })
    ).subscribe();

    combineLatest([
      fromEvent(this.searchPVNumber.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('numeroPV').value}
          })
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      this.searchForm.get('sessionAdmission').valueChanges
        .pipe(
          startWith(this.searchForm.get('sessionAdmission').value)
        ),
      this.searchForm.get('centreConcours').valueChanges
        .pipe(
          startWith(this.searchForm.get('centreConcours').value)
        ),
      this.searchForm.get('institution').valueChanges
        .pipe(
          startWith(this.searchForm.get('institution').value)
        ),
      this.searchForm.get('faculte').valueChanges
        .pipe(
          startWith(this.searchForm.get('faculte').value)
        ),
      this.searchForm.get('programme').valueChanges
        .pipe(
          startWith(this.searchForm.get('programme').value)
        ),
      this.searchForm.get('decision').valueChanges
        .pipe(
          startWith(this.searchForm.get('decision').value)
        ),
      this.searchForm.get('annee').valueChanges
        .pipe(
          startWith(this.searchForm.get('annee').value)
        ),
    ])
    .subscribe((
      [
        eventSearchPVNumber, 
        eventSearchNationalStudentIdentifier,
        eventSearchName, 
        eventFirstname, 
        sessionAdmission,
        centreConcours,
        institution,
        faculte,
        programme,
        decision,
        annee
      ]) => {
      this.searchForm.patchValue({
        numeroPV: eventSearchPVNumber ? eventSearchPVNumber['target'].value : null, 
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null, 
        sessionAdmission,
        centreConcours,
        institution,
        faculte,
        programme,
        decision,
        annee
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      sessionAdmission: null,
      centreConcours: null,
      institution: null,
      faculte: null,
      programme: null,
      numeroPV: null,
      identifiantNationalEleve: null,
      name: null,
      firstName: null,
      sessionBAC : null,
      optionBACId: null,
      lyceeId: null,
      centreExamenId: null,
      genreId: '',
      decision: null,
      annee: null,
      statusEtablissement: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getExportedAdmissionInstitutionList(criteria);
  }

  private getFileName(): string {
    let fileName = 'Resultats_Admission';
    let centreConcoursId = this.searchForm.get('centreConcours').value;
    if (centreConcoursId) {
      let centreConcours = this.centreConcoursList.find(v => v.id == centreConcoursId);
      if (centreConcours) {
        fileName = fileName + '_' + centreConcours.name;
      }
    }
    let anneeId = this.searchForm.get('annee').value;
    if (anneeId) {
      let annee = this.anneeList.find(v => v.id == anneeId);
      if (annee) {
        fileName = fileName + '_' + annee.name;
      }
    }
    return fileName;
  }

  exportAdmissions() {
    this._dialogService.openDialog(ExportInscriptionDialog, {
      width: '600px',
    }).afterClosed().subscribe(fields => {
      if (fields) {
        this._store.dispatch(showLoading());
        
        this._metfpetService.getExportedAdmissionInstitutionList(MetfpetServiceAgent.PagingSearchDTO.fromJS({
          pageIndex: -1,
          filters: this.searchForm.value,
        }))
          .subscribe(data => 
            this._exportService.exportAdmissions(data.results, fields, this.getFileName()))
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }

  private _loadPrograms(institutionId: string, faculteId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {'institution': institutionId, 'faculte': faculteId}
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });
  }

  private _loadFaculte(institutionId: string) {
    this._metfpetService.getFaculteListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {'institution': institutionId}
    })).subscribe(data => {
      this.faculteList = data.results;
      this._cd.markForCheck();
    });
  }
}
