import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component ({
  templateUrl: './export-inscription.dialog.html',
})
export class ExportInscriptionDialog {

  public form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<ExportInscriptionDialog>,
    private _formBuilder: FormBuilder
  ) {
    this.form = this._formBuilder.group({
      exportExcel: true,
      exportPrenom: true,
      exportNom: true,
      exportDateNaissance: true,
      exportSexe: true,
      exportINA: true,
      exportAnnee: true,
      exportPv: true,
      exportDecision: true,
      exportProgramme: true,
      exportFaculte: true, 
      exportInstitution: true,
      exportConcoursStatutGlobal: true,
      exportConcoursNotesMoyenne: true,
    });
  }

  onConfirm(): void {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    }
  }

  onClose(): void {
    this.dialogRef.close(null);
  }
}