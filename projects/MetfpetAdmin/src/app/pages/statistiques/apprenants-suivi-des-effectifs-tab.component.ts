import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

export const Tabs = [
  {
    routerLink: '/' + UiPath.admin.statistiques.admissions, 
    label: 'Admission' 
  },
  {
    routerLink: '/' + UiPath.admin.statistiques.inscriptions, 
    label: 'Inscription et enrôlement' 
  },
];

@Component({
  templateUrl: './apprenants-suivi-des-effectifs-tab.component.html',
})
export class ApprenantsSuiviDesEffectifsTabComponent {

  navs = Tabs;
  title: string;
  routerEventSubscription: Subscription;
  
  constructor(private router: Router) {
    this.title = 'Apprenants - suivi des effectifs';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = selectedTab.label;
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
