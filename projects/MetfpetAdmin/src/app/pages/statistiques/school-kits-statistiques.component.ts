import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ElementRef, 
  ViewChild, 
  ViewEncapsulation 
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { combineLatest, fromEvent, Observable } from 'rxjs';
import { 
  AppSettings,
  Settings,
  MetfpetServiceAgent,
  PerfectScrollService,
  BaseTableComponent,
  PaiementBourseStatusValues,
  MoisAnneeValues,
  BiometricStatusValues,
  showLoading,
  hideLoading,
} from 'MetfpetLib';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators';
import { UiPath } from '../ui-path';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { MatDialog } from '@angular/material/dialog';
import { ExportBoursePaymentsDialog } from '../bourses/export-bourse-payments.dialog';

@Component({
  templateUrl: './school-kits-statistiques.component.html',
  styleUrls: ['./school-kits-statistiques.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SchoolKitsStatistiquesComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;

  legendPosition = 'below';
  colorScheme2 = {domain: ['#F1A129', '#2967A5', '#959595', '#9B4100', '#13D40A', '#E70000']};

  settings: Settings;
  currentAnneeAcademique: MetfpetServiceAgent.AnneeAcademiqueDTO;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  allProgramList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  loadingIndicator: boolean = false;
  schoolKitStatistiquesOverview: MetfpetServiceAgent.SchoolKitStatistiquesOverview;
  schoolKitStatistiquesPerNiveau: Array<MetfpetServiceAgent.StatistiquesPipeChartInfo>;
  schoolSupplyStatistiquesPerStatus: Array<MetfpetServiceAgent.StatistiquesPipeChartInfo>;
  schoolKitInfoPerInstitution: Array<MetfpetServiceAgent.InstitutionProgrammeSchoolKitInfo>;
  schoolKitInfoPerProgramme: Array<MetfpetServiceAgent.InstitutionProgrammeSchoolKitInfo>;
  schoolSupplyInfoPerProgramme: Array<MetfpetServiceAgent.ProgrammeSchoolSupplyInfo>;
  schoolSupplyInfoPerName: Array<MetfpetServiceAgent.ProgrammeSchoolSupplyInfo>;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _fb: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialog: MatDialog,
    private _route: ActivatedRoute, 
  ) {
    super(_cd, _fb, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.sort = {prop: 'etudiantINA', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();
    
    this._metfpetService.getAnneeAcademiqueAdmissionOuverte().subscribe((data : any) => {
      if (data) {
        this.currentAnneeAcademique = data;
        this.searchForm.get('anneeAcademique').setValue(this.currentAnneeAcademique.id);
        this._cd.markForCheck();
      }
    });

    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results.filter(x => x.statusEtablissementName === 'Publique');
      this._cd.markForCheck();
    });

    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.allProgramList = data.results;
      this.programList = data.results;
      this._cd.markForCheck();
    });

    this.searchForm.get('institution').valueChanges.pipe(
      startWith(this.searchForm.get('institution').value),
      tap(institutionId => {
        this.programList = this.allProgramList;
        if (!this.isFirstSearch) {
          this.searchForm.get('programme').setValue(null);
        }
        if (!institutionId) {
          return;
        }
        this._loadPrograms(institutionId);
      })
    ).subscribe();

    combineLatest([
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(this.searchFirstname.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('firstName').value}
          })
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      this.searchForm.get('anneeAcademique').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademique').value)
        ),
      this.searchForm.get('institution').valueChanges
        .pipe(
          startWith(this.searchForm.get('institution').value)
        ),
      this.searchForm.get('programme').valueChanges
        .pipe(
          startWith(this.searchForm.get('programme').value)
        ),
      this.searchForm.get('niveauEtude').valueChanges
        .pipe(
          startWith(this.searchForm.get('niveauEtude').value)
        ),
    ])
    .subscribe((
      [
        eventSearchName, 
        eventSearchFirstname,
        eventSearchNationalStudentIdentifier,
        anneeAcademique,
        institution,
        programme,
        niveauEtude,
      ]) => {
      this.searchForm.patchValue({
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventSearchFirstname ? eventSearchFirstname['target'].value : null, 
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        anneeAcademique,
        institution,
        programme,
        niveauEtude,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      name: null,
      firstName: null,
      identifiantNationalEleve: null,
      anneeAcademique: null,
      institution: null,
      programme: null,
      niveauEtude: null,
      filterByProgramName: true,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  private getSchoolKitStatistiques(criteria: any) {
    this._metfpetService.getSchoolKitStatistiquesOverview(criteria).subscribe((data : any) => {
      this.schoolKitStatistiquesOverview = data;
      this._cd.markForCheck();
    });
    this._metfpetService.getSchoolKitStatistiquesPerNiveau(criteria).subscribe((data : any) => {
      this.schoolKitStatistiquesPerNiveau = data;
      this._cd.markForCheck();
    });
    this._metfpetService.getSchoolSupplyStatistiquesPerStatus(criteria).subscribe((data : any) => {
      this.schoolSupplyStatistiquesPerStatus = data;
      this._cd.markForCheck();
    });
    this._metfpetService.getSchoolKitInfoPerInstitution(criteria).subscribe((data : any) => {
      this.schoolKitInfoPerInstitution = data;
      this._cd.markForCheck();
    });
    this._metfpetService.getSchoolKitInfoPerProgramme(criteria).subscribe((data : any) => {
      this.schoolKitInfoPerProgramme = data;
      this._cd.markForCheck();
    });
    this._metfpetService.getSchoolSupplyInfoPerProgramme(criteria).subscribe((data : any) => {
      this.schoolSupplyInfoPerProgramme = data;
      this._cd.markForCheck();
    });
    this._metfpetService.getSchoolSupplyInfoPerName(criteria).subscribe((data : any) => {
      this.schoolSupplyInfoPerName = data;
      this._cd.markForCheck();
    });
  }

  protected _search(criteria: any): Observable<any> {
    this.getSchoolKitStatistiques(criteria);
    return this._metfpetService.getSchoolKitEtudiantListPage(criteria);
  }
  
  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {institution: institutionId}
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });
  }

  getLabel(label:any) {
    let self:any = this;
    const sum = self.series.reduce((total, seriesItem) => total + (seriesItem.value || 0), 0);
    const seriesItem = self.series.find((item) => item.name === label);
    if (seriesItem) {
        if (sum === 0)
        {
          return `0%`;
        }
        const value = seriesItem.value || 0;
        const percentage = ((value / sum) * 100).toFixed(1);
        return `${percentage}%`;
    }
    return label;
  }
}