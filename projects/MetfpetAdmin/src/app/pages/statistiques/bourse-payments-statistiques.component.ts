import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ElementRef, 
  ViewChild, 
  ViewEncapsulation 
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { combineLatest, fromEvent, Observable } from 'rxjs';
import { 
  AppSettings,
  Settings,
  MetfpetServiceAgent,
  PerfectScrollService,
  BaseTableComponent,
  PaiementBourseStatusValues,
  MoisAnneeValues,
  BiometricStatusValues,
  showLoading,
  hideLoading,
} from 'MetfpetLib';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators';
import { UiPath } from '../ui-path';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { MatDialog } from '@angular/material/dialog';
import { ExportBoursePaymentsDialog } from '../bourses/export-bourse-payments.dialog';

@Component({
  templateUrl: './bourse-payments-statistiques.component.html',
  styleUrls: ['./bourse-payments-statistiques.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BoursePaymentsStatistiquesComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;

  legendPosition = 'below';
  colorScheme2 = {domain: ['#959595', '#F1A129']};

  settings: Settings;
  currentAnneeAcademique: MetfpetServiceAgent.AnneeAcademiqueDTO;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  allProgramList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  trimestreList: Array<MetfpetServiceAgent.BourseTrimestreDTO>;
  genreList: Array<MetfpetServiceAgent.Genre>;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  typeDiplomeList: Array<MetfpetServiceAgent.TypeDiplomeViewModel>;
  statusList: Array<any>;
  biometricList: Array<any>;
  moisAnneeList: Array<any>;
  loadingIndicator: boolean = false;
  paiementBourseStatistiques: MetfpetServiceAgent.PaiementBourseStatistiquesDTO;
  boursesParMontant: Array<MetfpetServiceAgent.BoursesParMontant>;
  boursesParGender:Array<{name:string,value:number}>;
  institutionPaiementBourses: Array<MetfpetServiceAgent.InstitutionPaiementBourseRowViewModel>;

  @ViewChild(DatatableComponent) 
  institutionTable: DatatableComponent;
  institutionRows = [];
  institutionPageOffset: number = 0;
  institutionTotal: number = 0;
  institutionPagedRows = [];

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _fb: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _dialog: MatDialog,
    private _route: ActivatedRoute, 
  ) {
    super(_cd, _fb, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.statusList = PaiementBourseStatusValues;
    this.moisAnneeList = MoisAnneeValues;
    this.biometricList = BiometricStatusValues;
    this.sort = {prop: 'etudiantINA', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();
    
    this._metfpetService.getBourseTrimestreList().subscribe(data => {
      this.trimestreList = data.results;
    });

    this._metfpetService.getGenreList().subscribe(data => {
      this.genreList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueAdmissionOuverte().subscribe((data : any) => {
      if (data) {
        this.currentAnneeAcademique = data;
        this._cd.markForCheck();
      }
    });

    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getTypeDiplomeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.typeDiplomeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results.filter(x => x.statusEtablissementName === 'Publique');
      this._cd.markForCheck();
    });

    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.allProgramList = data.results;
      this.programList = data.results;
      this._cd.markForCheck();
    });

    this.searchForm.get('institutionId').valueChanges.pipe(
      startWith(this.searchForm.get('institutionId').value),
      tap(institutionId => {
        this.programList = this.allProgramList;
        if (!this.isFirstSearch) {
          this.searchForm.get('programmeId').setValue(null);
        }
        if (!institutionId) {
          return;
        }
        this._loadPrograms(institutionId);
      })
    ).subscribe();

    combineLatest([
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(this.searchFirstname.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('firstName').value}
          })
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      this.searchForm.get('status').valueChanges
        .pipe(
          startWith(this.searchForm.get('status').value)
        ),
      this.searchForm.get('anneeAcademiqueId').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademiqueId').value)
        ),
      this.searchForm.get('moisAnnee').valueChanges
        .pipe(
          startWith(this.searchForm.get('moisAnnee').value)
        ),
      this.searchForm.get('prefectureId').valueChanges
        .pipe(
          startWith(this.searchForm.get('prefectureId').value)
        ),
      this.searchForm.get('institutionId').valueChanges
        .pipe(
          startWith(this.searchForm.get('institutionId').value)
        ),
      this.searchForm.get('programmeId').valueChanges
        .pipe(
          startWith(this.searchForm.get('programmeId').value)
        ),
      this.searchForm.get('biometricStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('biometricStatus').value)
        ),
      this.searchForm.get('genre').valueChanges
        .pipe(
          startWith(this.searchForm.get('genre').value)
        ),
      this.searchForm.get('niveauEtude').valueChanges
        .pipe(
          startWith(this.searchForm.get('niveauEtude').value)
        ),
      this.searchForm.get('typeDiplome').valueChanges
        .pipe(
          startWith(this.searchForm.get('typeDiplome').value)
        ),
      this.searchForm.get('bourseTrimestreId').valueChanges
        .pipe(
          startWith(this.searchForm.get('bourseTrimestreId').value)
        ),
      this.searchForm.get('residencePrefecture').valueChanges
        .pipe(
          startWith(this.searchForm.get('residencePrefecture').value)
        )
    ])
    .subscribe((
      [
        eventSearchName, 
        eventSearchFirstname,
        eventSearchNationalStudentIdentifier,
        status,
        anneeAcademiqueId,
        moisAnnee,
        prefectureId,
        institutionId,
        programmeId,
        biometricStatus,
        genre,
        niveauEtude,
        typeDiplome,
        bourseTrimestreId,
        residencePrefecture,
      ]) => {
      this.searchForm.patchValue({
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventSearchFirstname ? eventSearchFirstname['target'].value : null, 
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        status,
        anneeAcademiqueId,
        moisAnnee,
        prefectureId,
        institutionId,
        programmeId,
        biometricStatus,
        genre,
        niveauEtude,
        typeDiplome,
        bourseTrimestreId,
        residencePrefecture,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      status: null,
      name: null,
      firstName: null,
      identifiantNationalEleve: null,
      anneeAcademiqueId: null,
      institutionId: null,
      programmeId: null,
      moisAnnee: null,
      prefectureId: null,
      biometricStatus: null,
      from: null,
      to: null,
      genre: null,
      niveauEtude: null,
      typeDiplome: null,
      bourseTrimestreId: null,
      filterByProgramName: true,
      residencePrefecture: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  private getPaiementBourseStatistiques(criteria: any) {
    this._metfpetService.getPaiementBourseStatistiques(criteria).subscribe((data : any) => {
      this.paiementBourseStatistiques = data;
      this.boursesParMontant = this.paiementBourseStatistiques.boursesParMontant;
      this.buildBoursesParGender();
      this._cd.markForCheck();
    });
  }

  private getInstitutionPaiementBourses(pageOffset: number) {
    this._store.dispatch(showLoading());
    this._metfpetService.getInstitutionPaiementBourseListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: pageOffset,
      pageSize: this.pageSize,
      filters: this.searchForm.value,
   })).subscribe(data => {
      this.institutionPaiementBourses = data.results;
      this.institutionRows = data.results;
      this.institutionPageOffset = pageOffset;
      this.institutionTotal = data.totalCount;
      this.institutionPagedRows = this.institutionRows;
      this._cd.markForCheck();
    })
    .add(() => this._store.dispatch(hideLoading()));
  }

  protected _search(criteria: any): Observable<any> {
    this.getPaiementBourseStatistiques(criteria);
    this.getInstitutionPaiementBourses(0);
    return this._metfpetService.getPaiementBourseListPage(criteria);
  }

  buildBoursesParGender() {
    this.boursesParGender = [];
    this.boursesParGender.push({
      name: "Homme",
      value: this.paiementBourseStatistiques.numberOfBoursesHomme
    });
    this.boursesParGender.push({
      name: "Femme",
      value: this.paiementBourseStatistiques.numberOfBoursesFemme
    });
  }

  public exportPayments() {
    this._dialog.open(ExportBoursePaymentsDialog , {
      width: '600px',
      data: {
        filters: this.searchForm.value
      }
    });
  }
  
  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {institution: institutionId}
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });
  }

  getLabel(label:any) {
    let self:any = this;
    const sum = self.series.reduce((total, seriesItem) => total + (seriesItem.value || 0), 0);
    const seriesItem = self.series.find((item) => item.name === label);
    if (seriesItem) {
        const value = seriesItem.value || 0;
        const percentage = ((value / sum) * 100).toFixed(1);
        return `${percentage}%`;
    }
    return label;
  }

  onChangeInstitutionPageNgxTable(event) {
    this.getInstitutionPaiementBourses(event.offset);
  }

  onChangeInstitutionPageMaterial(event) {
    this.institutionTable.onFooterPage({'page': event.pageIndex});
    this.getInstitutionPaiementBourses(event.pageIndex);
  }
}