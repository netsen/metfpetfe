import { ChangeDetectorRef, Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import {
  showSuccess,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  showException,
  ExportService,
} from 'MetfpetLib';
import { UserIdleService } from 'angular-user-idle';

@Component({
  templateUrl: './export-admission-apprenants.dialog.html',
})
export class ExportAdmissionApprenantsDialog {

  form: FormGroup;
  processing: boolean;
  processed: number;
  totalToprocess: number;
  showWarningMessage: boolean;
  filters: any;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<ExportAdmissionApprenantsDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    protected _cdRef: ChangeDetectorRef,
    private _userIdleService: UserIdleService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.filters = data.filters;
    this.processing = false;
    this.form = this._formBuilder.group({
    });
  }

  onConfirm() {
    this._store.dispatch(showLoading());
    this._metfpetService.getExportedAdmissionEtudiantIdList(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1,
      filters: this.filters,
    }))
    .subscribe(
      (ids) => {
        this.process(ids);
      },
      error => this._store.dispatch(showException({error: error}))
    )
    .add(() => this._store.dispatch(hideLoading()));
  }

  async process(ids: Array<string>) {
    this.processing = true;
    this._userIdleService.stopWatching();
    this._store.dispatch(showLoading());
    this.showWarningMessage = true;
    this.processed = 0;
    this.totalToprocess = ids.length;
    this._cdRef.markForCheck();

    let exportedAdmissions = new Array<MetfpetServiceAgent.AdmissionInstitutionRowViewModel>();
    for (let id of ids) {

      this.processed++;
      this._cdRef.markForCheck(); 

      try {
        let data = await this._metfpetService.getExportedAdmissionEtudiants(MetfpetServiceAgent.PagingSearchDTO.fromJS({
          pageIndex: -1,
          filters: {
            ...this.filters,
            etudiant: id
          },
        })).toPromise();
        if (data) {
          for (let item of data)
            {
              exportedAdmissions.push(item);
            }
        }
      } catch(error) {
      }
    }
    this._exportService.exportAdmissionApprenants(exportedAdmissions);

    this.processing = false;
    this.showWarningMessage = false;
    this._userIdleService.startWatching();
    this._store.dispatch(hideLoading());
    this._dialogRef.close();
  }

  onClose() {
    this._dialogRef.close();
  }
}