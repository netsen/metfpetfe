import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { BoursePaymentsStatistiquesComponent } from './bourse-payments-statistiques.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ApprenantPaymentsStatistiquesComponent } from './apprenant-payments-statistiques.component';
import { AdmissionStatistiquesComponent } from './admission-statistiques.component';
import { ApprenantsSuiviDesEffectifsTabComponent } from './apprenants-suivi-des-effectifs-tab.component';
import { InscriptionStatistiquesComponent } from './inscription-statistiques.component';
import { ExportAdmissionApprenantsDialog } from './export-admission-apprenants.dialog';
import { SchoolKitsStatistiquesComponent } from './school-kits-statistiques.component';

export const routes = [
  {
    path: 'bourses', 
    component: BoursePaymentsStatistiquesComponent, 
    pathMatch: 'full'
  },
  {
    path: 'paiements', 
    component: ApprenantPaymentsStatistiquesComponent, 
    pathMatch: 'full'
  },
  {
    path: 'admissions', 
    component: AdmissionStatistiquesComponent, 
    pathMatch: 'full'
  },
  {
    path: 'inscriptions', 
    component: InscriptionStatistiquesComponent, 
    pathMatch: 'full'
  },
  {
    path: 'school-kits', 
    component: SchoolKitsStatistiquesComponent, 
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxChartsModule,
  ],
  declarations: [
    BoursePaymentsStatistiquesComponent,
    ApprenantPaymentsStatistiquesComponent,
    AdmissionStatistiquesComponent,
    ApprenantsSuiviDesEffectifsTabComponent,
    InscriptionStatistiquesComponent,
    ExportAdmissionApprenantsDialog,
    SchoolKitsStatistiquesComponent,
  ]
})
export class AdminStatistiquesModule {}
