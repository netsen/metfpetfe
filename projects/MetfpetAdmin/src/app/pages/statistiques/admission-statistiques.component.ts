import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef,
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators/';
import { Store } from '@ngrx/store';
import { 
  AppSettings,
  Settings,
  AdmissionInstitutionStatusValues,
  AdmissionDecisionValues,
  BaseTableComponent,
  PerfectScrollService,
  DialogService,
  ExportService,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  addPostOptions,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { Guid } from 'guid-typescript';
import { Tabs } from './apprenants-suivi-des-effectifs-tab.component';
import { ExportAdmissionApprenantsDialog } from './export-admission-apprenants.dialog';
import { MatDialog } from '@angular/material/dialog';

@Component({
  templateUrl: './admission-statistiques.component.html',
  styleUrls: ['./admission-statistiques.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdmissionStatistiquesComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchPVNumber', {static: true}) searchPVNumber: ElementRef;
  @ViewChild('searchPVNumberEtudiant', {static: true}) searchPVNumberEtudiant: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;

  legendPosition = 'below';
  colorSchemePerGenre = {domain: ['#253E4C', '#FF710A', '#23D5AB', '#EE7752', '#23A6D5', '#E73C7E', '#8FB0C3', '#1C7FF3', '#9B4100', '#550909']};

  settings: Settings;
  title: string;
  navs = Tabs;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  typeProgrammeList: Array<MetfpetServiceAgent.TypeProgramme>;
  sessionsList: Array<any>;
  optionsList: Array<MetfpetServiceAgent.OptionRowViewModel>;
  statusList: Array<any>;
  decisionList: Array<any>;
  centreExamenList: Array<MetfpetServiceAgent.CentreExamenRowViewModel>;
  lyceesList: Array<MetfpetServiceAgent.LyceeRowViewModel>;
  genreList: Array<MetfpetServiceAgent.Genre>;
  anneeList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  centreConcoursList: Array<MetfpetServiceAgent.CentreConcoursRowViewModel>; 
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissement>;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  admissionStatistiquesOverview: MetfpetServiceAgent.StatistiquesOverview;
  admissionStatistiquesPerGenre: Array<MetfpetServiceAgent.StatistiquesPipeChartInfo>;
  admissionStatistiquesPerRegion: Array<MetfpetServiceAgent.StatistiquesPipeChartInfo>;
  admissionStatistiquesPerApprenantStatus: Array<MetfpetServiceAgent.StatistiquesPipeChartInfo>;
  admissionStatistiquesPerStatusEtablissement: Array<MetfpetServiceAgent.StatistiquesPipeChartInfo>;
  admissionStatistiquesPerStatus: Array<MetfpetServiceAgent.StatistiquesPipeChartInfo>;
  admissionStatistiquesPerPaymentStatus: Array<MetfpetServiceAgent.StatistiquesPipeChartInfo>;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialog: MatDialog,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Apprenants - suivi des effectifs';
    this.statusList = AdmissionInstitutionStatusValues;
    this.decisionList = AdmissionDecisionValues;
    this.sort = {prop: 'identifiantNationalEleve', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cd.markForCheck(); 
    });

    this._metfpetService.getTypeProgrammeList().subscribe(data => {
      this.typeProgrammeList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getGenreList().subscribe(data => {
      this.genreList = data;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getStatusEtablissementList().subscribe(data => {
      this.statusEtablissementList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getCentreConcoursListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.centreConcoursList = data.results;
      var allCentreConcours = new MetfpetServiceAgent.CentreConcoursRowViewModel();
      allCentreConcours.id = Guid.EMPTY;
      allCentreConcours.name = 'Tous les centres de concours';
      this.centreConcoursList.push(allCentreConcours);
      this._cd.markForCheck();
    });

    this._metfpetService.getCentreExamenListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.centreExamenList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getLyceeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.lyceesList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getSessionBACList().subscribe(data => {
      this.sessionsList = data.map(x => {return {name: x}});
      this._cd.markForCheck();
    });

    this._metfpetService.getOptionList().subscribe(data => {
      this.optionsList = addPostOptions(data);
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeList = data.results;
      this._cd.markForCheck();
    });

    this.searchForm.get('institution').valueChanges.pipe(
      startWith(this.searchForm.get('institution').value),
      tap(institution => {
        this.programList = [];
        if (!this.isFirstSearch) {
          this.searchForm.get('programme').setValue(null);
        }
        if (!institution) {
          return;
        }
        this._loadPrograms(institution);
      })
    ).subscribe();
    
    combineLatest([
      fromEvent(this.searchPVNumber.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('numeroPV').value}
          })
        ),
      fromEvent(this.searchPVNumberEtudiant.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('numeroPVEtudiant').value}
          })
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      this.searchForm.get('institution').valueChanges
        .pipe(
          startWith(this.searchForm.get('institution').value)
        ),
      this.searchForm.get('programme').valueChanges
        .pipe(
          startWith(this.searchForm.get('programme').value)
        ),
      this.searchForm.get('status').valueChanges
        .pipe(
          startWith(this.searchForm.get('status').value)
        ),
      this.searchForm.get('decision').valueChanges
        .pipe(
          startWith(this.searchForm.get('decision').value)
        ),
      this.searchForm.get('annee').valueChanges
        .pipe(
          startWith(this.searchForm.get('annee').value)
        ),
      this.searchForm.get('centreConcours').valueChanges
        .pipe(
          startWith(this.searchForm.get('centreConcours').value)
        ),
      this.searchForm.get('residencePrefecture').valueChanges
        .pipe(
          startWith(this.searchForm.get('residencePrefecture').value)
        ),
      this.searchForm.get('typeProgramme').valueChanges
        .pipe(
          startWith(this.searchForm.get('typeProgramme').value)
        ),
    ])
    .subscribe((
      [
        eventSearchPVNumber,
        eventSearchPVNumberEtudiant,
        eventSearchNationalStudentIdentifier,
        eventSearchName, 
        eventFirstname,
        institution,
        programme,
        status,
        decision,
        annee,
        centreConcours,
        residencePrefecture,
        typeProgramme,
      ]) => {
      this.searchForm.patchValue({
        numeroPV: eventSearchPVNumber ? eventSearchPVNumber['target'].value : null, 
        numeroPVEtudiant: eventSearchPVNumberEtudiant ? eventSearchPVNumberEtudiant['target'].value : null, 
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null, 
        institution,
        programme,
        status,
        decision,
        annee,
        centreConcours,
        residencePrefecture,
        typeProgramme,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      status: null,
      institution: null,
      programme: null,
      numeroPV: null,
      numeroPVEtudiant: null,
      identifiantNationalEleve: null,
      name: null,
      firstName: null,
      session: null,
      optionId: null,
      lyceeId: null,
      centreExamenId: null,
      genreId: '',
      decision: null,
      annee: null,
      getByChoix: false,
      centreConcours: null,
      statusEtablissement: null,
      createFrom: null,
      createTo: null,
      residencePrefecture: null,
      typeProgramme: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  private getAdmissionStatistiquesOverview(criteria: any) {
    this._store.dispatch(showLoading());
    this._metfpetService.getAdmissionStatistiquesOverview(criteria).subscribe((data : any) => {
      this.admissionStatistiquesOverview = data;
      this._cd.markForCheck();
    })
    .add(() => this._store.dispatch(hideLoading()));
  }

  private getAdmissionStatistiquesPerGenre(criteria: any) {
    this._store.dispatch(showLoading());
    this._metfpetService.getAdmissionStatistiquesPerGenre(criteria).subscribe((data : any) => {
      this.admissionStatistiquesPerGenre = data;
      this._cd.markForCheck();
    })
    .add(() => this._store.dispatch(hideLoading()));
  }

  private getAdmissionStatistiquesPerRegion(criteria: any) {
    this._store.dispatch(showLoading());
    this._metfpetService.getAdmissionStatistiquesPerRegion(criteria).subscribe((data : any) => {
      this.admissionStatistiquesPerRegion = data;
      this._cd.markForCheck();
    })
    .add(() => this._store.dispatch(hideLoading()));
  }

  private getAdmissionStatistiquesPerApprenantStatus(criteria: any) {
    this._store.dispatch(showLoading());
    this._metfpetService.getAdmissionStatistiquesPerApprenantStatus(criteria).subscribe((data : any) => {
      this.admissionStatistiquesPerApprenantStatus = data;
      this._cd.markForCheck();
    })
    .add(() => this._store.dispatch(hideLoading()));
  }

  private getAdmissionStatistiquesPerStatusEtablissement(criteria: any) {
    this._store.dispatch(showLoading());
    this._metfpetService.getAdmissionStatistiquesPerStatusEtablissement(criteria).subscribe((data : any) => {
      this.admissionStatistiquesPerStatusEtablissement = data;
      this._cd.markForCheck();
    })
    .add(() => this._store.dispatch(hideLoading()));
  }

  private getAdmissionStatistiquesPerStatus(criteria: any) {
    this._store.dispatch(showLoading());
    this._metfpetService.getAdmissionStatistiquesPerStatus(criteria).subscribe((data : any) => {
      this.admissionStatistiquesPerStatus = data;
      this._cd.markForCheck();
    })
    .add(() => this._store.dispatch(hideLoading()));
  }

  private getAdmissionStatistiquesPerPaymentStatus(criteria: any) {
    this._store.dispatch(showLoading());
    this._metfpetService.getAdmissionStatistiquesPerPaymentStatus(criteria).subscribe((data : any) => {
      this.admissionStatistiquesPerPaymentStatus = data;
      this._cd.markForCheck();
    })
    .add(() => this._store.dispatch(hideLoading()));
  }

  getLabel(label:any) {
    let self:any = this;
    const seriesItem = self.series.find((item) => item.name === label);
    if (seriesItem) {
        return `${seriesItem.value}`;
    }
    return label;
  }

  protected _search(criteria: any): Observable<any> {
    this.getAdmissionStatistiquesOverview(criteria);
    this.getAdmissionStatistiquesPerRegion(criteria);
    this.getAdmissionStatistiquesPerApprenantStatus(criteria);
    this.getAdmissionStatistiquesPerStatusEtablissement(criteria);
    this.getAdmissionStatistiquesPerStatus(criteria);
    this.getAdmissionStatistiquesPerPaymentStatus(criteria);
    this.getAdmissionStatistiquesPerGenre(criteria);
    return this._metfpetService.getAdmissionInstitutionListPage(criteria);
  }

  open(selectedAdmissionInstitution: MetfpetServiceAgent.AdmissionInstitutionRowViewModel) {
    this._router.navigate([`${UiPath.admin.students.dossier}/${selectedAdmissionInstitution.etudiantId}`], { state: { previousPage: 'Admission' } });
  }

  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {'institution': institutionId}
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });
  }

  exportAdmissionApprenants() {
    this._dialog.open(ExportAdmissionApprenantsDialog, {
      width: '650px',
      data: {
        filters: this.searchForm.value
      }
    });
  }
}
