import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ElementRef, 
  ViewChild, 
  ViewEncapsulation 
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { combineLatest, fromEvent, Observable } from 'rxjs';
import { 
  AppSettings,
  Settings,
  MetfpetServiceAgent,
  PerfectScrollService,
  BaseTableComponent,
  showLoading,
  hideLoading,
  PaymentStatusValues,
  PaymentServiceAgent,
} from 'MetfpetLib';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators';

@Component({
  templateUrl: './apprenant-payments-statistiques.component.html',
  styleUrls: ['./apprenant-payments-statistiques.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApprenantPaymentsStatistiquesComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;

  legendPosition = 'below';
  colorSchemePerStatusEtablissement = {domain: ['#959595', '#F1A129']};
  colorSchemePerStatus = {domain: ['#FF710A', '#35373B', '#253E4C', '#868E8D']};
  colorSchemePerTypeDiplome = {domain: ['#F1A129', '#35373B', '#9B4100', '#550909', '#868E8D']};

  settings: Settings;
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissement>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  allProgramList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  typeDiplomeList: Array<MetfpetServiceAgent.TypeDiplomeViewModel>;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  statusList: Array<any>;
  paymentTypes: Array<any>;
  loadingIndicator: boolean = false;
  paiementOverviewStatistiques: PaymentServiceAgent.PaiementOverviewStatistiquesDTO;
  paiementPipeChartInfoPerStatusEtablissement:Array<PaymentServiceAgent.PaiementStatistiquesPipeChartInfo>;
  paiementPipeChartInfoPerStatus:Array<PaymentServiceAgent.PaiementStatistiquesPipeChartInfo>;
  paiementPipeChartInfoPerTypeDiplome:Array<PaymentServiceAgent.PaiementStatistiquesPipeChartInfo>;
  paiementStatistiquesSummary: PaymentServiceAgent.PaiementStatistiquesSummary;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _fb: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
    private _paymentService: PaymentServiceAgent.HttpService,
    private _route: ActivatedRoute, 
  ) {
    super(_cd, _fb, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.statusList = PaymentStatusValues;
    this.paymentTypes = [
      {value : 'Admission', name : 'Admission'},
      {value : 'ImpressionDocument', name : 'Documentation'},
      {value : 'Inscription', name : 'Inscription/réinscription'},
    ];
    this.sort = {prop: 'etudiantINA', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();
    
    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cd.markForCheck(); 
    });

    this._paymentService.getPaiementStatistiquesSummary().subscribe(data => {
      this.paiementStatistiquesSummary = data; 
      this._cd.markForCheck();
    });

    this._metfpetService.getStatusEtablissementList().subscribe(data => {
      this.statusEtablissementList = data; 
      this._cd.markForCheck();
    });

    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getTypeDiplomeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.typeDiplomeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results.filter(x => x.statusEtablissementName === 'Publique');
      this._cd.markForCheck();
    });

    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.allProgramList = data.results;
      this.programList = data.results;
      this._cd.markForCheck();
    });

    this.searchForm.get('institutionId').valueChanges.pipe(
      startWith(this.searchForm.get('institutionId').value),
      tap(institutionId => {
        this.programList = this.allProgramList;
        if (!this.isFirstSearch) {
          this.searchForm.get('programmeId').setValue(null);
        }
        if (!institutionId) {
          return;
        }
        this._loadPrograms(institutionId);
      })
    ).subscribe();

    combineLatest([
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(this.searchFirstname.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('firstName').value}
          })
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      this.searchForm.get('status').valueChanges
        .pipe(
          startWith(this.searchForm.get('status').value)
        ),
      this.searchForm.get('anneeAcademiqueId').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademiqueId').value)
        ),
      this.searchForm.get('institutionId').valueChanges
        .pipe(
          startWith(this.searchForm.get('institutionId').value)
        ),
      this.searchForm.get('programmeId').valueChanges
        .pipe(
          startWith(this.searchForm.get('programmeId').value)
        ),
      this.searchForm.get('niveauEtude').valueChanges
        .pipe(
          startWith(this.searchForm.get('niveauEtude').value)
        ),
      this.searchForm.get('typeDiplome').valueChanges
        .pipe(
          startWith(this.searchForm.get('typeDiplome').value)
        ),
      this.searchForm.get('statusEtablissementId').valueChanges
        .pipe(
          startWith(this.searchForm.get('statusEtablissementId').value)
        ),
      this.searchForm.get('paymentType').valueChanges
        .pipe(
          startWith(this.searchForm.get('paymentType').value)
        ),
      this.searchForm.get('residencePrefecture').valueChanges
        .pipe(
          startWith(this.searchForm.get('residencePrefecture').value)
        ),
    ])
    .subscribe((
      [
        eventSearchName, 
        eventSearchFirstname,
        eventSearchNationalStudentIdentifier,
        status,
        anneeAcademiqueId,
        institutionId,
        programmeId,
        niveauEtude,
        typeDiplome,
        statusEtablissementId,
        paymentType,
        residencePrefecture,
      ]) => {
      this.searchForm.patchValue({
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventSearchFirstname ? eventSearchFirstname['target'].value : null, 
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        status,
        anneeAcademiqueId,
        institutionId,
        programmeId,
        niveauEtude,
        typeDiplome,
        statusEtablissementId,
        paymentType,
        residencePrefecture,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      status: null,
      name: null,
      firstName: null,
      identifiantNationalEleve: null,
      anneeAcademiqueId: null,
      institutionId: null,
      programmeId: null,
      from: null,
      to: null,
      niveauEtude: null,
      typeDiplome: null,
      statusEtablissementId: null,
      paymentType: 'Inscription',
      filterByProgramName: true,
      residencePrefecture: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  private getPaiementOverviewStatistiques(criteria: any) {
    this._store.dispatch(showLoading());
    this._paymentService.getPaiementOverviewStatistiques(criteria).subscribe((data : any) => {
      this.paiementOverviewStatistiques = data;
      this._cd.markForCheck();
    })
    .add(() => this._store.dispatch(hideLoading()));
  }

  private getPaiementPipeChartInfoPerStatusEtablissement(criteria: any) {
    this._store.dispatch(showLoading());
    this._paymentService.getPaiementPipeChartInfoPerStatusEtablissement(criteria).subscribe((data : any) => {
      this.paiementPipeChartInfoPerStatusEtablissement = data;
      this._cd.markForCheck();
    })
    .add(() => this._store.dispatch(hideLoading()));
  }

  private getPaiementPipeChartInfoPerStatus(criteria: any) {
    this._store.dispatch(showLoading());
    this._paymentService.getPaiementPipeChartInfoPerStatus(criteria).subscribe((data : any) => {
      this.paiementPipeChartInfoPerStatus = data;
      this._cd.markForCheck();
    })
    .add(() => this._store.dispatch(hideLoading()));
  }

  private getPaiementPipeChartInfoPerTypeDiplome(criteria: any) {
    this._store.dispatch(showLoading());
    this._paymentService.getPaiementPipeChartInfoPerTypeDiplome(criteria).subscribe((data : any) => {
      this.paiementPipeChartInfoPerTypeDiplome = data;
      this._cd.markForCheck();
    })
    .add(() => this._store.dispatch(hideLoading()));
  }

  protected _search(criteria: any): Observable<any> {
    this.getPaiementOverviewStatistiques(criteria);
    this.getPaiementPipeChartInfoPerStatusEtablissement(criteria);
    this.getPaiementPipeChartInfoPerStatus(criteria);
    this.getPaiementPipeChartInfoPerTypeDiplome(criteria);
    return this._paymentService.getPaiementStatistiquesListPage(criteria);
  }

  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {institution: institutionId}
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });
  }

  getLabelPerStatusEtablissement(label:any) {
    let self:any = this;
    const seriesItem = self.series.find((item) => item.name === label);
    if (seriesItem) {
        return `${seriesItem.value}`;
    }
    return label;
  }

  getLabelPerStatus(label:any) {
    let self:any = this;
    const seriesItem = self.series.find((item) => item.name === label);
    if (seriesItem) {
        return `${seriesItem.value} GNF`;
    }
    return label;
  }

  getLabelPerTypeDiplome(label:any) {
    let self:any = this;
    const seriesItem = self.series.find((item) => item.name === label);
    if (seriesItem) {
        return `${seriesItem.value}`;
    }
    return label;
  }
}