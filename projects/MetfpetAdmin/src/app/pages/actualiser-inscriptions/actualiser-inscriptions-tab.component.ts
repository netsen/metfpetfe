import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './actualiser-inscriptions-tab.component.html',
})
export class ActualiserInscriptionsTabComponent {

  navs = [
    {
      routerLink: '/' + UiPath.admin.import.actualiserInscription, 
      label: 'Actualiser les inscriptions des apprenants' 
    },
  ];

  public title: string;
  routerEventSubscription: Subscription;

  constructor(protected router: Router) {
    this.title = 'Actualiser les inscriptions des apprenants';
  }

  ngOnInit(): void {
    this.routerEventSubscription =  this.router.events.subscribe((res) => {
      let selectedTab = this.navs.find(tab => this.router.url.endsWith(tab.routerLink));
      if (selectedTab) {
       this.title = 'Actualiser les inscriptions des apprenants';
      }
     });
  }
    
  ngOnDestroy(): void {
    if (this.routerEventSubscription) {
      this.routerEventSubscription.unsubscribe();
    }
  }
}
