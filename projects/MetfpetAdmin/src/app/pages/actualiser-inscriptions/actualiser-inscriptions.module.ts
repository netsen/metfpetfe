import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ActualiserInscriptionsDialog } from './actualiser-inscriptions.dialog';
import { ActualiserInscriptionsTabComponent } from './actualiser-inscriptions-tab.component';
import { ActualiserInscriptionsComponent } from './actualiser-inscriptions.component';

export const routes = [
  {
    path: '', 
    component: ActualiserInscriptionsTabComponent,
    children: 
      [
        {
          path: '',
          redirectTo: 'inscriptions',
          pathMatch: 'full'
        },
        { 
          path: 'inscriptions', 
          component: ActualiserInscriptionsComponent, 
          data: { breadcrumb: 'Actualiser les inscriptions des apprenants' }
        },
      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  exports: [
    SharedModule,
    ActualiserInscriptionsComponent,
  ],
  declarations: [
    ActualiserInscriptionsTabComponent,
    ActualiserInscriptionsComponent,
    ActualiserInscriptionsDialog,
  ], 
  entryComponents: [
    ActualiserInscriptionsDialog
  ]
})
export class ActualiserInscriptionsModule {

}
