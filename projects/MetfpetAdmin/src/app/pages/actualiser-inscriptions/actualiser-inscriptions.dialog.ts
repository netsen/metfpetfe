import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './actualiser-inscriptions.dialog.html',
})
export class ActualiserInscriptionsDialog {

  form: FormGroup;
  fileMissing: boolean;

  constructor(
    public dialogRef: MatDialogRef<ActualiserInscriptionsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, 
    public fb: FormBuilder, 
  ) {
    
      this.form = this.fb.group({});
  }

  onFileChange(event) {
    this.data.file = event.target.files[0];
    this.fileMissing = false;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onConfirm(): void {
    if (!this.data.file) {
      this.fileMissing = true;
      return;
    }
    if (this.form.valid) {
      this.dialogRef.close(this.data);
    }
  }
}