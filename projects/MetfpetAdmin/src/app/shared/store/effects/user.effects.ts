import { Injectable } from '@angular/core';
import { tap, take } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { 
  editProfile, 
  modifyPassword,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  DialogService, 
  IdentityServiceAgent,
  ModifyPasswordDialog,
  selectLoggedInPerson,
  showException,
} from 'MetfpetLib';
import { AdminProfileDialog } from '../../../pages/profile';

@Injectable()
export class UserEffects {

  constructor(
    private _actions$: Actions,
    private _store: Store<any>,
    private _dialogService: DialogService,
    private _identityService: IdentityServiceAgent.HttpService
  ) {

  }

  editProfile$ = createEffect(() =>
    this._actions$.pipe(
      ofType(editProfile),
      tap(() => this._dialogService.openDialog(AdminProfileDialog, {width: '640px'}))
    ), { dispatch: false }
  );

  modifyPassword$ = createEffect(() =>
    this._actions$.pipe(
      ofType(modifyPassword),
      tap(() => {
        this._dialogService.openDialog(
          ModifyPasswordDialog,
          {
            width: '580px',
            data: { title: 'Modification de mot de passe' }
          }
        ).afterClosed().subscribe(result => {
          if (result) {
            this._store.pipe(select(selectLoggedInPerson), take(1)).subscribe(
              person =>  {
                result.identifiant = person.identifiant;
                this._store.dispatch(showLoading());

                this._identityService.updatePasswordMinisteriel(IdentityServiceAgent.UpdatePasswordInfo.fromJS(
                  result)
                )
                .subscribe(
                  () => this._store.dispatch(showSuccess({})),
                  error => this._store.dispatch(showException({error: error}))
                )
                .add(() => this._store.dispatch(hideLoading()));
              }
            );
          }
        });
      })
    ), { dispatch: false }
  );

}