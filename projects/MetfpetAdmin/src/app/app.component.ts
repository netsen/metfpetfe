import { Component } from '@angular/core';
import { AppSettings, Settings } from 'MetfpetLib';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styles: []
})
export class AppComponent {
  
  public title = 'ParcoursPro Admin';
  public settings: Settings;

  constructor(public appSettings: AppSettings) {
    this.settings = this.appSettings.settings;
  }
}
