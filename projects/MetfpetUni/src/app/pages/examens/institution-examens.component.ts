import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent } from 'rxjs';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { debounceTime, distinctUntilChanged, startWith, take, tap } from 'rxjs/operators/';
import {
  AppSettings,
  Settings,
  BaseTableComponent,
  PerfectScrollService,
  MetfpetServiceAgent,
  BiometricStatusValues,
  FinalExamenStatusGlobalValues,
  selectLoggedInPerson,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { Tabs } from './institution-examens-tab.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  templateUrl: './institution-examens.component.html',
  styleUrls: ['./institution-examens.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstitutionExamensComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;
  @ViewChild('searchPV', {static: true}) searchPV: ElementRef;
  @ViewChild('searchMoyenneTotalMin', {static: true}) searchMoyenneTotalMin: ElementRef;
  @ViewChild('searchMoyenneTotalMax', {static: true}) searchMoyenneTotalMax: ElementRef;

  settings: Settings;
  title: string;
  navs = Tabs;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  centreList: Array<any>;
  statusGlobalList: Array<any>;
  biometricList: Array<any>;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    private _dialog: MatDialog,
    protected _perfectScrollService: PerfectScrollService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Examens de sortie - Session en cours';
    this.statusGlobalList = FinalExamenStatusGlobalValues;
    this.biometricList = BiometricStatusValues;
    this.centreList = [];
    this.sort = {prop: 'identifiantNationalEleve', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this._store.pipe(select(selectLoggedInPerson), take(1)).subscribe(
      person =>  {
        this.searchForm.get('institution').setValue(person.institutionId);

        this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
           pageIndex: -1, filters: {institution: person.institutionId}
        })).subscribe(data => {
          this.programList = data.results;
          this._cd.markForCheck();
        });
      }
    );

    combineLatest([
      this.searchForm.get('statusGlobal').valueChanges
        .pipe(
          startWith(this.searchForm.get('statusGlobal').value)
        ),
      this.searchForm.get('programme').valueChanges
        .pipe(
          startWith(this.searchForm.get('programme').value)
        ),
      this.searchForm.get('anneeAcademique').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademique').value)
        ),
      this.searchForm.get('niveauEtude').valueChanges
        .pipe(
          startWith(this.searchForm.get('niveauEtude').value)
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      fromEvent(
        this.searchPV.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('numeroPV').value}
            })
          ),
      fromEvent(
        this.searchMoyenneTotalMin.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('moyenneTotalMin').value}
            })
          ),
      fromEvent(
        this.searchMoyenneTotalMax.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('moyenneTotalMax').value}
            })
          ),
      this.searchForm.get('finalExamenCentre').valueChanges
        .pipe(
          startWith(this.searchForm.get('finalExamenCentre').value)
        ),
      this.searchForm.get('biometricStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('biometricStatus').value)
        ),
    ])
    .subscribe((
      [
        statusGlobal,
        programme,
        anneeAcademique,
        niveauEtude,
        eventSearchNationalStudentIdentifier,
        eventSearchName, 
        eventFirstname,
        eventSearchPV,
        eventSearchMoyenneTotalMin,
        eventSearchMoyenneTotalMax,
        finalExamenCentre,
        biometricStatus,
      ]) => {
      this.searchForm.patchValue({
        statusGlobal,
        programme,
        anneeAcademique,
        niveauEtude,
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null,
        numeroPV: eventSearchPV ? eventSearchPV['target'].value : null, 
        moyenneTotalMin: eventSearchMoyenneTotalMin ? eventSearchMoyenneTotalMin['target'].value : null, 
        moyenneTotalMax: eventSearchMoyenneTotalMax ? eventSearchMoyenneTotalMax['target'].value : null, 
        finalExamenCentre,
        biometricStatus,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      identifiantNationalEleve: null,
      institution: null,
      programme: null,
      anneeAcademique: null,
      niveauEtude: null,
      name: null,
      firstName: null,
      numeroPV: null,
      biometricStatus: null,
      finalExamenCentre: null,
      moyenneTotalMin: null,
      moyenneTotalMax: null,
      statusGlobal: null,
      isCurrentSession: true
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getFinalExamenEtudiantListPage(criteria);
  }
  
  open(row: MetfpetServiceAgent.FinalExamenEtudiantViewModel) {
    this._router.navigate([`${UiPath.institution.students.dossier}/${row.etudiantId}`], { state: { previousPage: 'Cursus' } });
  }
}