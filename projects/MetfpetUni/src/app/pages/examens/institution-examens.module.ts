import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { InstitutionExamensComponent } from './institution-examens.component';
import { InstitutionExamensTabComponent } from './institution-examens-tab.component';
import { InstitutionPastExamensComponent } from './institution-past-examens.component';

export const routes = [
  { 
    path: '', 
    redirectTo: 'currentSession',
    pathMatch: 'full' 
  },
  { 
    path: 'currentSession', 
    component: InstitutionExamensComponent, 
    data: { breadcrumb: 'Session en cours' },
    pathMatch: 'full' 
  },
  { 
    path: 'pastSession', 
    component: InstitutionPastExamensComponent, 
    data: { breadcrumb: 'Session passées' },
    pathMatch: 'full' 
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    InstitutionExamensComponent,
    InstitutionExamensTabComponent,
    InstitutionPastExamensComponent
  ]
})
export class InstitutionExamensModule {}
