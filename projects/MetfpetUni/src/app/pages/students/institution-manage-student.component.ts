import { 
  ChangeDetectionStrategy, 
  Component, 
  OnInit, 
  ViewEncapsulation 
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AppSettings,
  Settings,
  MetfpetServiceAgent,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './institution-manage-student.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstitutionManageStudentComponent implements OnInit {

  settings: Settings;
  title: string;
  student: MetfpetServiceAgent.EtudiantProfileViewModel;
  studentId: string;
  loadingIndicator: boolean;

  constructor(
    public appSettings: AppSettings, 
    private _router: Router,
    private _route: ActivatedRoute
  ) {
    this.settings = this.appSettings.settings; 
  }

  ngOnInit() {
    this._route.params.subscribe(params => this.studentId = params['id']);
  }

  onLoadedStudent(student: MetfpetServiceAgent.EtudiantProfileViewModel) {
    this.student = student;
    this.title = this.student.firstName + ' ' + this.student.name;
  }

  onSavedStudent(student: MetfpetServiceAgent.EtudiantProfileViewModel) {
    this.student = student;
    this._router.navigate([UiPath.institution.students.list]);
  }
}