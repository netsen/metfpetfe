import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { InstitutionStudentsComponent } from './institution-students.component';
import { InstitutionManageStudentComponent } from './institution-manage-student.component';
import { ExportStudentDialog } from './export-student.dialog';

export const routes = [
  { 
    path: '', 
    component: InstitutionStudentsComponent, 
    pathMatch: 'full'
  },
  { 
    path: 'dossier/:id', 
    component: InstitutionManageStudentComponent, 
    data: { breadcrumb: 'Dossier' }
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    ExportStudentDialog,
    InstitutionStudentsComponent,
    InstitutionManageStudentComponent,
  ],
  entryComponents: [
    ExportStudentDialog,
  ]
})
export class InstitutionStudentsModule {

}
