import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef,
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith, take } from 'rxjs/operators/';
import { Store, select } from '@ngrx/store';
import { 
  AppSettings,
  Settings,
  AuthorityEnum,
  StudentStatusValues,
  BaseTableComponent,
  PerfectScrollService,
  AuthService,
  DialogService,
  ExportService,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  selectLoggedInPerson,
  addPostOptions,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { ExportStudentDialog } from './export-student.dialog';

@Component({
  templateUrl: './institution-students.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstitutionStudentsComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchPVNumber', {static: true}) searchPVNumber: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;

  settings: Settings;
  title: string;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  sessionsList: Array<any>;
  optionsList: Array<MetfpetServiceAgent.OptionRowViewModel>;
  lyceesList: Array<MetfpetServiceAgent.LyceeRowViewModel>;
  statusList: Array<any>;
  genreList: Array<MetfpetServiceAgent.Genre>;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _authService: AuthService,
    private _dialogService: DialogService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Dossier Apprenant';
    this.statusList = StudentStatusValues;
    this.sort = {prop: 'identifiantNationalEleve', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();
    this._store.pipe(select(selectLoggedInPerson), take(1)).subscribe(
      person =>  {
        this.searchForm.get('institutionId').setValue(person.institutionId);

        this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
           pageIndex: -1, filters: {institution: person.institutionId}
        })).subscribe(data => {
          this.programList = data.results;
          this._cd.markForCheck();
        });
      }
    );

    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getGenreList().subscribe(data => {
      this.genreList = data;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getSessionBACList().subscribe(data => {
      this.sessionsList = data.map(x => {return {name: x}});
      this._cd.markForCheck();
    });

    this._metfpetService.getOptionList().subscribe(data => {
      this.optionsList = addPostOptions(data);
      this._cd.markForCheck();
    });

    this._metfpetService.getLyceeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.lyceesList = data.results;
      this._cd.markForCheck();
    });

    combineLatest([
      fromEvent(this.searchPVNumber.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('numeroPV').value}
          })
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      this.searchForm.get('optionId').valueChanges
        .pipe(
          startWith(this.searchForm.get('optionId').value)
        ),
      this.searchForm.get('session').valueChanges
        .pipe(
          startWith(this.searchForm.get('session').value)
        ),
      this.searchForm.get('etudiantStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('etudiantStatus').value)
        ),
      this.searchForm.get('programmeId').valueChanges
        .pipe(
          startWith(this.searchForm.get('programmeId').value)
        ),
    ])
    .subscribe((
      [
        eventSearchPVNumber,
        eventSearchNationalStudentIdentifier,
        eventSearchName, 
        eventFirstname,
        optionId,
        session,
        etudiantStatus,
        programmeId,
      ]) => {
      this.searchForm.patchValue({
        numeroPV: eventSearchPVNumber ? eventSearchPVNumber['target'].value : null, 
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null, 
        optionId,
        session,
        etudiantStatus,
        programmeId,
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      etudiantStatus: null,
      institutionId: null,
      programmeId: null,
      numeroPV: null,
      identifiantNationalEleve: null,
      name: null,
      firstName: null,
      session : null,
      optionId: null,
      lyceeId: null,
      centreExamenId: null,
      genreId: '',
      anneeAcademique: null,
      niveauEtude: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getEtudiantListPage(criteria);
  }

  open(selectedStudent: MetfpetServiceAgent.EtudiantRowViewModel) {
    this._router.navigate([`${UiPath.institution.students.dossier}/${selectedStudent.id}`]);
  }
  
  getOption(row: MetfpetServiceAgent.EtudiantRowViewModel): any {
    if (row.optionBACName) {
      return row.optionBACName;
    }
    if (row.optionBEPCName) {
      return row.optionBEPCName;
    }
    if (row.optionTerminaleName) {
      return row.optionTerminaleName;
    }
    if (row.optionPostPrimaireName) {
      return row.optionPostPrimaireName;
    }
    if (row.optionPostSecondaireName) {
      return row.optionPostSecondaireName;
    }
    return row.optionAutreName;
  }

  getSession(row: MetfpetServiceAgent.EtudiantRowViewModel): any {
    if (row.sessionBAC) {
      return row.sessionBAC;
    }
    if (row.sessionBEPC) {
      return row.sessionBEPC;
    }
    return row.sessionTerminale;
  }
  
  public isAllowed() {
    return this._authService.getUserType() == 'superviseurUniversitaire' ;
  }

  exportStudents() {
    this._dialogService.openDialog(ExportStudentDialog, {
      width: '600px',
    }).afterClosed().subscribe(fields => {
      if (fields) {
        this._store.dispatch(showLoading());
        
        this._metfpetService.getEtudiantListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
          pageIndex: -1,
          filters: this.searchForm.value,
        }))
          .subscribe(data => 
            this._exportService.exportStudentsOfCurrentInstitutionToExcel(data.results, fields))
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }
}
