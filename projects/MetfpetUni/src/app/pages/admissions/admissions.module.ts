import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AdmissionListComponent } from './admission-list.component';
import { AdmissionListExportDialog } from './admission-list-export.dialog';

export const routes = [
  { 
    path: '', 
    component: AdmissionListComponent, 
    pathMatch: 'full' 
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    AdmissionListComponent,
    AdmissionListExportDialog,
  ],
  entryComponents: [
    AdmissionListExportDialog,
  ]
})
export class AdmissionsModule {}
