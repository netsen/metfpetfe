import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef,
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith, take, tap } from 'rxjs/operators/';
import { select, Store } from '@ngrx/store';
import { 
  AppSettings,
  Settings,
  AdmissionInstitutionStatusValues,
  AdmissionDecisionValues,
  BaseTableComponent,
  PerfectScrollService,
  DialogService,
  ExportService,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  addPostOptions,
  selectLoggedInPerson,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { AdmissionListExportDialog } from './admission-list-export.dialog';
import { Guid } from 'guid-typescript';

@Component({
  templateUrl: './admission-list.component.html',
  styleUrls: ['./admission-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdmissionListComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchPVNumber', {static: true}) searchPVNumber: ElementRef;
  @ViewChild('searchPVNumberEtudiant', {static: true}) searchPVNumberEtudiant: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;

  settings: Settings;
  title: string;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  sessionsList: Array<any>;
  optionsList: Array<MetfpetServiceAgent.OptionRowViewModel>;
  statusList: Array<any>;
  decisionList: Array<any>;
  centreExamenList: Array<MetfpetServiceAgent.CentreExamenRowViewModel>;
  lyceesList: Array<MetfpetServiceAgent.LyceeRowViewModel>;
  genreList: Array<MetfpetServiceAgent.Genre>;
  anneeList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  centreConcoursList: Array<MetfpetServiceAgent.CentreConcoursRowViewModel>; 
  statusEtablissementList: Array<MetfpetServiceAgent.StatusEtablissement>;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Admission - Gestion des participants à la session d’admission';
    this.statusList = AdmissionInstitutionStatusValues;
    this.decisionList = AdmissionDecisionValues;
    this.sort = {prop: 'identifiantNationalEleve', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cd.markForCheck(); 
    });

    this._metfpetService.getGenreList().subscribe(data => {
      this.genreList = data;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getStatusEtablissementList().subscribe(data => {
      this.statusEtablissementList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getCentreConcoursListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.centreConcoursList = data.results;
      var allCentreConcours = new MetfpetServiceAgent.CentreConcoursRowViewModel();
      allCentreConcours.id = Guid.EMPTY;
      allCentreConcours.name = 'Tous les centres de concours';
      this.centreConcoursList.push(allCentreConcours);
      this._cd.markForCheck();
    });

    this._metfpetService.getCentreExamenListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.centreExamenList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getLyceeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}, sort: {nameKey:'Name'}
    })).subscribe(data => {
      this.lyceesList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getSessionBACList().subscribe(data => {
      this.sessionsList = data.map(x => {return {name: x}});
      this._cd.markForCheck();
    });

    this._metfpetService.getOptionList().subscribe(data => {
      this.optionsList = addPostOptions(data);
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeList = data.results;
      this._cd.markForCheck();
    });

    this._store.pipe(select(selectLoggedInPerson), take(1)).subscribe(
      person =>  {
        this.searchForm.get('institution').setValue(person.institutionId);

        this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
           pageIndex: -1, filters: {institution: person.institutionId}
        })).subscribe(data => {
          this.programList = data.results;
          this._cd.markForCheck();
        });
      }
    );
    
    combineLatest([
      fromEvent(this.searchPVNumber.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('numeroPV').value}
          })
        ),
      fromEvent(this.searchPVNumberEtudiant.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('numeroPVEtudiant').value}
          })
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      this.searchForm.get('programme').valueChanges
        .pipe(
          startWith(this.searchForm.get('programme').value)
        ),
      this.searchForm.get('status').valueChanges
        .pipe(
          startWith(this.searchForm.get('status').value)
        ),
      this.searchForm.get('decision').valueChanges
        .pipe(
          startWith(this.searchForm.get('decision').value)
        ),
      this.searchForm.get('annee').valueChanges
        .pipe(
          startWith(this.searchForm.get('annee').value)
        ),
      this.searchForm.get('centreConcours').valueChanges
        .pipe(
          startWith(this.searchForm.get('centreConcours').value)
        ),
      this.searchForm.get('residencePrefecture').valueChanges
        .pipe(
          startWith(this.searchForm.get('residencePrefecture').value)
        ),
    ])
    .subscribe((
      [
        eventSearchPVNumber,
        eventSearchPVNumberEtudiant,
        eventSearchNationalStudentIdentifier,
        eventSearchName, 
        eventFirstname,
        programme,
        status,
        decision,
        annee,
        centreConcours,
        residencePrefecture
      ]) => {
      this.searchForm.patchValue({
        numeroPV: eventSearchPVNumber ? eventSearchPVNumber['target'].value : null, 
        numeroPVEtudiant: eventSearchPVNumberEtudiant ? eventSearchPVNumberEtudiant['target'].value : null, 
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null, 
        programme,
        status,
        decision,
        annee,
        centreConcours,
        residencePrefecture
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      status: null,
      institution: null,
      programme: null,
      numeroPV: null,
      numeroPVEtudiant: null,
      identifiantNationalEleve: null,
      name: null,
      firstName: null,
      session: null,
      optionId: null,
      lyceeId: null,
      centreExamenId: null,
      genreId: '',
      decision: null,
      annee: null,
      getByChoix: false,
      centreConcours: null,
      statusEtablissement: null,
      residencePrefecture: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getAdmissionInstitutionListPage(criteria);
  }

  open(selectedAdmissionInstitution: MetfpetServiceAgent.AdmissionInstitutionRowViewModel) {
    this._router.navigate([`${UiPath.institution.students.dossier}/${selectedAdmissionInstitution.etudiantId}`], { state: { previousPage: 'Admission' } });
  }

  private getFileName(fileName: string): string {
    let centreConcoursId = this.searchForm.get('centreConcours').value;
    if (centreConcoursId) {
      let centreConcours = this.centreConcoursList.find(v => v.id == centreConcoursId);
      if (centreConcours) {
        fileName = fileName + '_' + centreConcours.name;
      }
    }
    let anneeId = this.searchForm.get('annee').value;
    if (anneeId) {
      let annee = this.anneeList.find(v => v.id == anneeId);
      if (annee) {
        fileName = fileName + '_' + annee.name;
      }
    }
    return fileName;
  }

  exportChoices() {
    this.searchForm.patchValue({getByChoix: true});
    this._dialogService.openDialog(AdmissionListExportDialog, {
      width: '600px',
      data: {
        filters: this.searchForm.value,
        getByChoix: true,
        fileName: this.getFileName("Choix")
      }
    });
  }

  exportAdmissions() {
    this.searchForm.patchValue({getByChoix: false});
    this._dialogService.openDialog(AdmissionListExportDialog, {
      width: '600px',
      data: {
        filters: this.searchForm.value,
        getByChoix: false,
        fileName: this.getFileName("Admission")
      }
    });
  }
}
