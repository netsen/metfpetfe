import { Component, Inject } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component ({
  templateUrl: './export-inscription.dialog.html',
})
export class ExportInscriptionDialog {

  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<ExportInscriptionDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _formBuilder: FormBuilder
  ) {
    this.form = this._formBuilder.group({
      exportEtudiantNom: true,
      exportMatricule: true,
      exportPeriode: true,
      exportProgramme: true, 
      exportNiveau: true,
      exportType: true,
      exportFormation: true,
      exportStatut: true,
      exportIna: true,
      exportNomPere: true,
      exportNomMere: true,
      exportPhone: true,
      exportEmail: true,
      exportGenre: true,
    });
  }

  onConfirm(): void {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    }
  }

  onClose(): void {
    this.dialogRef.close(null);
  }

}