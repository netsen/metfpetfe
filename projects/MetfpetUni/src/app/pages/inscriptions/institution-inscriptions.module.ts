import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InstitutionInscriptionsComponent } from './institution-inscriptions.component';
import { SharedModule } from '../../shared/shared.module';
import { ExportInscriptionDialog } from './export-inscription.dialog';

export const routes = [
  { 
    path: '', 
    component: InstitutionInscriptionsComponent, 
    pathMatch: 'full' 
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    InstitutionInscriptionsComponent,
    ExportInscriptionDialog
  ],
  entryComponents: [
    ExportInscriptionDialog,
  ]
})
export class InstitutionInscriptionsModule {}