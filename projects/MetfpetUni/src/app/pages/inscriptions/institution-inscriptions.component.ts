import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ElementRef, 
  ViewChild, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent } from 'rxjs';
import { combineLatest } from 'rxjs/observable/combineLatest';
import { debounceTime, distinctUntilChanged, startWith, take } from 'rxjs/operators/';
import { Store, select } from '@ngrx/store';
import {
  AppSettings,
  Settings, 
  InscriptionProgrammeStatusValues,
  BaseTableComponent,
  PerfectScrollService,
  MetfpetServiceAgent,
  selectLoggedInPerson,
  showLoading,
  hideLoading,
  AuthService,
  DialogService,
  ExportService,
  BiometricStatusValues,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';
import { ExportInscriptionDialog } from './export-inscription.dialog';

@Component({
  templateUrl: './institution-inscriptions.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstitutionInscriptionsComponent extends BaseTableComponent {

  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;
  @ViewChild('searchMatricule', {static: true}) searchMatricule: ElementRef;

  settings: Settings;
  title: string;
  niveauEtudeList: Array<MetfpetServiceAgent.NiveauEtudeRowViewModel>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  typeDiplomeList: Array<MetfpetServiceAgent.TypeDiplomeViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  statusList: Array<any>;
  biometricList: Array<any>;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _fb: FormBuilder,
    protected _router: Router, 
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _authService: AuthService,
    private _dialogService: DialogService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _fb, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = '(Ré)-Inscription';
    this.statusList = InscriptionProgrammeStatusValues;
    this.biometricList = BiometricStatusValues;
    this.sort = {prop: 'studentName', dir: 'desc'};
  }

  ngOnInit() {
    super.ngOnInit();
    this._metfpetService.getNiveauEtudeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.niveauEtudeList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getTypeDiplomeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1, filters: {}
   })).subscribe(data =>{
      this.typeDiplomeList = data.results;
      this._cd.markForCheck();
   })

    this._store.pipe(select(selectLoggedInPerson), take(1)).subscribe(
      person =>  {
        this.searchForm.get('institution').setValue(person.institutionId);

        this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
           pageIndex: -1, filters: {institution: person.institutionId}
        })).subscribe(data => {
          this.programList = data.results;
          this._cd.markForCheck();
        });
      }
    );

    combineLatest([
      this.searchForm.get('biometricStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('biometricStatus').value)
        ),
      this.searchForm.get('status').valueChanges
        .pipe(
          startWith(this.searchForm.get('status').value)
        ),
      this.searchForm.get('programme').valueChanges
        .pipe(
          startWith(this.searchForm.get('programme').value)
        ),
      this.searchForm.get('anneeAcademique').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademique').value)
        ),
      this.searchForm.get('niveauEtude').valueChanges
        .pipe(
          startWith(this.searchForm.get('niveauEtude').value)
        ),
        this.searchForm.get('typeDiplome').valueChanges
        .pipe(
          startWith(this.searchForm.get('typeDiplome').value)
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchMatricule.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('matricule').value}
          })
        ),
    ])
    .subscribe((
      [
        biometricStatus,
        status,
        programme,
        anneeAcademique,
        niveauEtude,
        typeDiplome,
        eventSearchNationalStudentIdentifier,
        eventsearchMatricule,  
      ]) => {
      this.searchForm.patchValue({
        biometricStatus,
        status,
        programme,
        anneeAcademique,
        niveauEtude,
        typeDiplome,
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        matricule: eventsearchMatricule ? eventsearchMatricule['target'].value : null, 
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      status: null,
      identifiantNationalEleve: null,
      matricule: null,
      programme: null,
      anneeAcademique: null,
      niveauEtude: null,
      institution: null,
      typeDiplome: null,
      biometricStatus: null,
    });
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getInscriptionProgrammeListPage(criteria);
  }
  
  open(row: MetfpetServiceAgent.InscriptionProgrammeRowViewModel) {
    this._router.navigate([`${UiPath.institution.students.dossier}/${row.etudiantId}`]);
  }

  public isAllowed() {
    return this._authService.getUserType() == 'superviseurUniversitaire' ;
  }

  exportInscriptions() {
    this._dialogService.openDialog(ExportInscriptionDialog, {
      width: '600px',
    }).afterClosed().subscribe(fields => {
      if (fields) {
        this._store.dispatch(showLoading());
        
        this._metfpetService.getInscriptionProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
          pageIndex: -1,
          filters: this.searchForm.value,
        }))
          .subscribe(data => 
            this._exportService.exportInscriptionProgrammes(data.results, fields))
          .add(() => this._store.dispatch(hideLoading()));
      }
    });
  }
}
