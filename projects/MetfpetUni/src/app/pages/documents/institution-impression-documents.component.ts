import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef,
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { debounceTime, distinctUntilChanged, startWith, tap, take } from 'rxjs/operators/';
import { select, Store } from '@ngrx/store';
import { 
  AppSettings,
  Settings,
  AuthorityEnum,
  StudentStatusValues,
  BaseTableComponent,
  PerfectScrollService,
  AuthService,
  DialogService,
  ExportService,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  ImpressionDocumentStatus,
  ImpressionDocumentPaymentDialog,
  ImpressionDocumentConfirmationCodeDialog,
  selectLoggedInPerson,
  ImpressionDocumentPreviewDialog
} from 'MetfpetLib';

@Component({
  templateUrl: './institution-impression-documents.component.html',
  styleUrls: ['./institution-impression-documents.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstitutionImpressionDocumentsComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchMatricule', {static: true}) searchMatricule: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;

  settings: Settings;
  title: string;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  statusList: Array<any>;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  genreList: Array<MetfpetServiceAgent.Genre>;
  documentList: Array<MetfpetServiceAgent.DocumentViewModel>;

  constructor(
    public appSettings: AppSettings, 
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _authService: AuthService,
    private _dialogService: DialogService,
    private _exportService: ExportService,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Documents officiels';
    this.statusList = StudentStatusValues;
    this.sort = {prop: 'identifiantNationalEleve', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();

    this._metfpetService.getGenreList().subscribe(data => {
      this.genreList = data;
      this._cd.markForCheck();
    });
    
    this._metfpetService.getDocumentListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {'isActive':'1'}
    })).subscribe(data => {
      this.documentList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    this._store.pipe(select(selectLoggedInPerson), take(1)).subscribe(
      person =>  {
        this.searchForm.get('institution').setValue(person.institutionId);

        this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
           pageIndex: -1, filters: {institution: person.institutionId}
        })).subscribe(data => {
          this.programList = data.results;
          this._cd.markForCheck();
        });
      }
    );

    combineLatest([
      fromEvent(this.searchMatricule.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('matricule').value}
          })
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      this.searchForm.get('etudiantStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('etudiantStatus').value)
        ),
      this.searchForm.get('programme').valueChanges
        .pipe(
          startWith(this.searchForm.get('programme').value)
        ),
      this.searchForm.get('anneeAcademique').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademique').value)
        ),
      this.searchForm.get('documentId').valueChanges
        .pipe(
          startWith(this.searchForm.get('documentId').value)
        ),
    ])
    .subscribe((
      [
        eventsearchMatricule, 
        eventSearchNationalStudentIdentifier,
        eventSearchName, 
        eventFirstname,
        etudiantStatus,
        programme,
        anneeAcademique,
        documentId
      ]) => {
      this.searchForm.patchValue({
        matricule: eventsearchMatricule ? eventsearchMatricule['target'].value : null, 
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null, 
        etudiantStatus,
        programme,
        anneeAcademique,
        documentId
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      etudiantStatus: null,
      institution: null,
      programme: null,
      identifiantNationalEleve: null,
      name: null,
      firstName: null,
      anneeAcademique : null,
      matricule: null,
      documentId: null,
      genreId: '',
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getDocumentInfoListPage(criteria);
  }

  open(row: MetfpetServiceAgent.DocumentInfo) {
    if (row.impressionDocumentStatus === <any>ImpressionDocumentStatus.APayer) {
      this._dialogService.openDialog(
        ImpressionDocumentPaymentDialog,
        {
          width: '680px',
          data: {
            documentInfo: row,
            documentName: row.impressionDocumentName,
            documentMontant: row.montant,
            impressionDocumentId: row.impressionDocumentId
          }
        }
      ).afterClosed().subscribe(paymentResult => {
        if (paymentResult) {
          this._dialogService.openDialog(
            ImpressionDocumentConfirmationCodeDialog,
            {
              width: '680px',
              data: {
                documentInfo: row,
                documentName: row.impressionDocumentName,
                documentMontant: row.montant,
                paymentId: paymentResult.payment.id
              }
            }
          ).afterClosed().subscribe(() => {
            this._dialogService.openDialog(
              ImpressionDocumentPreviewDialog,
              {
                width: '1300px',
                data: {
                  documentInfo: row,
                  impressionDocumentId: row.impressionDocumentId,
                  diffusion: row.diffusion
                }
              }
            ).afterClosed().subscribe(() => {
              this.triggerSearch();
            });
          });
        }
      });
    } else if (row.impressionDocumentStatus === <any>ImpressionDocumentStatus.Payee || row.impressionDocumentStatus === <any>ImpressionDocumentStatus.Delivree) {
      this._dialogService.openDialog(
        ImpressionDocumentPreviewDialog,
        {
          width: '1300px',
          data: {
            documentInfo: row,
            impressionDocumentId: row.impressionDocumentId,
            diffusion: row.diffusion
          }
        }
      ).afterClosed().subscribe(() => {
        this.triggerSearch();
      });
    }
  }
}
