import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { InstitutionImpressionDocumentsComponent } from './institution-impression-documents.component';

export const routes = [
  { 
    path: '', 
    component: InstitutionImpressionDocumentsComponent, 
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    InstitutionImpressionDocumentsComponent,
  ],
  entryComponents: [
  ]
})
export class InstitutionDocumentsModule {

}
