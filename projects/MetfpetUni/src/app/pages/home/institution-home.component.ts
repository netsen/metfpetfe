import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppSettings, AuthService, Settings } from 'MetfpetLib';
import { UiPath } from '../ui-path';

@Component({
  templateUrl:'./institution-home.component.html',
})
export class InstitutionHomeComponent {

  public settings: Settings;
  public title: string;

  constructor(
    public appSettings: AppSettings, 
    private _router: Router,
    private _authService: AuthService,
  ) {
    this.settings = this.appSettings.settings;
    this.title = "Accueil";
  }

  ngAfterViewInit(){
    setTimeout(() => this.settings.loadingSpinner = false);
  }

  goToStudentDossiers() {
    this._router.navigate([UiPath.institution.students.list]);
  }
  
  goToInscriptions() {
    this._router.navigate([UiPath.institution.inscriptions.list]);
  }

  isUniversitaireUser() {
    return this._authService.getUserType() == 'superviseurUniversitaire'
    || this._authService.getUserType() == 'agentUniversitaire';
  }
}
