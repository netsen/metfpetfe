import { Component, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Store } from "@ngrx/store";
import { validateForm, MetfpetServiceAgent } from "MetfpetLib";

const EMPHASIS_CURRENT_STEP_CSS = "text-bold";
const BIOMETRICS_CSS_INIT = "payment-biometrics-circle initial";
const BIOMETRICS_CSS_PASSED = "payment-biometrics-circle passed";
const BIOMETRICS_CSS_FAILED = "payment-biometrics-circle error";

const CREATE_BOURSES_PAYMENT_STEP_1 = "Identification";
const CREATE_BOURSES_PAYMENT_STEP_2 = "Authentification";
const CREATE_BOURSES_PAYMENT_STEP_3 = "Paiement du montant";

const BIOMETRICS_AUTH_STATUS_INIT = "initial";
const BIOMETRICS_AUTH_STATUS_PASSED = "passed";
const BIOMETRICS_AUTH_STATUS_FAILED = "error";

const PAYMENT_STATUS_INIT = "initial";
const PAYMENT_STATUS_SUCCEEDED = "success";
const PAYMENT_STATUS_FAILED = "failed";

@Component({
  templateUrl: "./create-bourses-payment-manually.dialog.html",
  styleUrls: ["./create-bourses-payment-manually.dialog.scss"],
})
export class CreateBoursesPaymentManuallyDialog {
  currentStepName: string;
  currentBiometricsStatus: string;
  currentPaymentStatus: string;

  curInaNumber: string;
  curAnneeAcademiqueId: string;
  paymentBoursesId: string;

  totalPaymentAmount: number;
  monthAndSessionText: string;
  paymentAvailability: string;
  isManualMode: boolean;
  hasChanges: boolean;
  keepDlgOpenedToPayAnotherOne: boolean;

  biometricStatus: string;

  title: string;
  form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private _dialogRef: MatDialogRef<CreateBoursesPaymentManuallyDialog>,
    private _formBuilder: FormBuilder,
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.title = "Paiement des bourses";
    this.initFirstStep();
    this.parseInputData(data);
  }

  initFirstStep() {
    this.currentStepName = CREATE_BOURSES_PAYMENT_STEP_1;
    this.currentBiometricsStatus = BIOMETRICS_AUTH_STATUS_INIT;
    this.currentPaymentStatus = PAYMENT_STATUS_INIT;

    this.curInaNumber = "";
    this.monthAndSessionText = "";
    this.totalPaymentAmount = 0;

    this.form = this._formBuilder.group({
      inaNumber: null,
    });
  }

  private parseInputData(data: any){
    this.isManualMode = true;
    if (data.paymentBoursesData) {
      this.curInaNumber = data.paymentBoursesData.etudiantINA;
      this.paymentBoursesId = data.paymentBoursesData.id;
      this.totalPaymentAmount = data.paymentBoursesData.montant;
      this.monthAndSessionText = data.paymentBoursesData.moisAnneeName + "; " + data.paymentBoursesData.etudiantBourseSessionName;
      this.biometricStatus = data.paymentBoursesData.biometricStatus;
      this.isManualMode = false;

      //automatically go to next step
      if (this.biometricStatus === 'Non biomtérisé') 
      {
        this.biometricsAuthFailed();
      }
      else
      {
        this.identifyInaSucceeded();
      }
    } else {
      if (data.anneeAcademiqueId)
        this.curAnneeAcademiqueId = data.anneeAcademiqueId;
    }
  }

  getCurrentStepCss(step: string) {
    if (this.currentStepName === step) return EMPHASIS_CURRENT_STEP_CSS;
    return "";
  }

  getBiometricCircleCss() {
    if (this.currentBiometricsStatus === BIOMETRICS_AUTH_STATUS_INIT) {
      return BIOMETRICS_CSS_INIT;
    } else if (this.currentBiometricsStatus === BIOMETRICS_AUTH_STATUS_PASSED) {
      return BIOMETRICS_CSS_PASSED;
    } else if (this.currentBiometricsStatus === BIOMETRICS_AUTH_STATUS_FAILED) {
      return BIOMETRICS_CSS_FAILED;
    }
  }

  goToNextStep() {
    this.identifyIna();
  }

  private identifyIna() {
    let inaNo = this.form.controls.inaNumber.value;
    this._metfpetService
      .getEtudiantPaiementBourseInfo(
        MetfpetServiceAgent.EtudiantPaiementBourseRequest.fromJS({
          identifiantNationalEleve: inaNo,
          anneeAcademiqueId: this.curAnneeAcademiqueId,
        })
      )
      .subscribe((data) => {
        this.totalPaymentAmount = data.totalAmount;
        this.monthAndSessionText = data.paymentContent;
        this.biometricStatus = data.biometricStatus;
        this.curInaNumber = inaNo;

        if (this.biometricStatus === 'Non biomtérisé') 
        {
          this.biometricsAuthFailed();
        }
        else if (this.totalPaymentAmount > 0) 
        {
          this.identifyInaSucceeded();
        }
        else 
        {
          this.identifyInaFailed();
        }
      }, (error) => {
        this.identifyInaFailed();
      });
  }

  private getSelectedPaymentBourses() {
    this.biometricsAuth();
  }

  private identifyInaSucceeded() {
    this.currentStepName = CREATE_BOURSES_PAYMENT_STEP_2;
    this.currentBiometricsStatus = BIOMETRICS_AUTH_STATUS_INIT;
    this.currentPaymentStatus = PAYMENT_STATUS_INIT;

    setTimeout(() => {
      this.getSelectedPaymentBourses();
    }, 200);
  }

  private identifyInaFailed() {}

  biometricsAuth() {
    //TODO: implement this later
    //Currently we bypass the biometrics auth
    setTimeout(() => {
      this.biometricsAuthSucceeded();
    }, 500);
  }

  private biometricsAuthSucceeded() {
    this.currentStepName = CREATE_BOURSES_PAYMENT_STEP_3;
    this.currentBiometricsStatus = BIOMETRICS_AUTH_STATUS_PASSED;
    this.currentPaymentStatus = PAYMENT_STATUS_INIT;
  }

  private biometricsAuthFailed() {
    this.currentStepName = CREATE_BOURSES_PAYMENT_STEP_2;
    this.currentBiometricsStatus = BIOMETRICS_AUTH_STATUS_FAILED;
    this.currentPaymentStatus = PAYMENT_STATUS_INIT;
  }

  private paymentProcessingSucceeded() {
    this.currentStepName = CREATE_BOURSES_PAYMENT_STEP_3;
    this.currentPaymentStatus = PAYMENT_STATUS_SUCCEEDED;

    if (this.keepDlgOpenedToPayAnotherOne == false)
      this.onClose();
    else {
      this.title = "Paiement des bourses";
      this.initFirstStep();  
    }
  }

  private paymentProcessingFailed() {
    this.currentStepName = CREATE_BOURSES_PAYMENT_STEP_3;
    this.currentPaymentStatus = PAYMENT_STATUS_FAILED;
  }

  private executeStudentPaymentBourse() {
    this._metfpetService
      .executeEtudiantPaiementBourse(
        MetfpetServiceAgent.EtudiantPaiementBourseRequest.fromJS({
          identifiantNationalEleve: this.curInaNumber,
          anneeAcademiqueId: this.curAnneeAcademiqueId,
        })
      )
      .subscribe((data) => {
        if (data && data.length > 0) {
          this.monthAndSessionText =
            data.map((x) => x.moisAnneeName).join(", ") +
            "; " +
            data[0].etudiantBourseSessionName;
          this.hasChanges = true;
          this.paymentProcessingSucceeded();
        }
      }, (error) => {
        this.paymentProcessingFailed();
      });
  }

  private executePaymentBourses(paymentBourseId: string) {
    this._metfpetService
      .executePaiementBourse(
        MetfpetServiceAgent.PaiementBourseRequest.fromJS({
          paiementBourseId: this.paymentBoursesId,
        })
      )
      .subscribe((data) => {
        if (data) {
          this.monthAndSessionText =
            data.moisAnneeName + "; " + data.etudiantBourseSessionName;
          this.totalPaymentAmount = data.montant;
          this.biometricStatus = data.biometricStatus;
          this.hasChanges = true;
          this.paymentProcessingSucceeded();
        }
      }, (error) => {
        this.paymentProcessingFailed();
      });
  }

  private doPayment(){
    if (this.isManualMode === true) this.executeStudentPaymentBourse();
    else this.executePaymentBourses(this.paymentBoursesId);
  }

  confirmPayment() {
    this.keepDlgOpenedToPayAnotherOne = false;
    this.doPayment();
  }

  confirmPaymentAndCreateNew() {
    this.keepDlgOpenedToPayAnotherOne = true;
    this.doPayment();
  }

  onConfirm() {
    validateForm(this.form);
    if (this.form.valid) {
    }
  }

  onClose() {
    this._dialogRef.close(this.hasChanges);
  }
}
