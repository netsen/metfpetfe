import { 
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, 
  ElementRef, 
  ViewChild, 
  ViewEncapsulation 
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { combineLatest, fromEvent, Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { 
  AppSettings,
  Settings,
  MetfpetServiceAgent,
  PerfectScrollService,
  BaseTableComponent,
  DialogService,
  ExportService,
  PaymentServiceAgent,
  PaiementBourseStatusValues,
  MoisAnneeValues,
  showLoading,
  hideLoading,
  CreateBoursesPaymentDialog,
  CreateBoursesTrimestrePaymentDialog,
  AuthService,
} from 'MetfpetLib';
import { debounceTime, distinctUntilChanged, startWith, tap } from 'rxjs/operators';
import { UiPath } from '../ui-path';
import { CreateBoursesPaymentManuallyDialog } from './create-bourses-payment-manually.dialog';

@Component({
  templateUrl: './bourse-payments.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BoursePaymentsComponent extends BaseTableComponent {

  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchNationalStudentIdentifier', {static: true}) searchNationalStudentIdentifier: ElementRef;

  settings: Settings;
  currentAnneeAcademique: MetfpetServiceAgent.AnneeAcademiqueDTO;
  anneeAcademiqueList: Array<MetfpetServiceAgent.AnneeAcademiqueRowViewModel>;
  institutionList: Array<MetfpetServiceAgent.InstitutionRowViewModel>;
  programList: Array<MetfpetServiceAgent.ProgrammeRowViewModel>;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  statusList: Array<any>;
  moisAnneeList: Array<any>;
  loadingIndicator: boolean = false;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _fb: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _dialogService: DialogService,
    private _dialog: MatDialog,
    private _exportService: ExportService,
    private _authService: AuthService,
    private _metfpetService: MetfpetServiceAgent.HttpService,
  ) {
    super(_cd, _fb, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.statusList = PaiementBourseStatusValues;
    this.moisAnneeList = MoisAnneeValues;
    this.sort = {prop: 'etudiantINA', dir: 'asc'};
  }

  ngOnInit() {
    super.ngOnInit();
    
    this._metfpetService.getAnneeAcademiqueAdmissionOuverte().subscribe((data : any) => {
      if (data) {
        this.currentAnneeAcademique = data;
        this._cd.markForCheck();
      }
    });

    this._metfpetService.getAnneeAcademiqueListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
        pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.anneeAcademiqueList = data.results;
      this._cd.markForCheck();
    });

    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cd.markForCheck();
    });

    this._metfpetService.getInstitutionListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {}
    })).subscribe(data => {
      this.institutionList = data.results;
      this._cd.markForCheck();
    });

    this.searchForm.get('institutionId').valueChanges.pipe(
      startWith(this.searchForm.get('institutionId').value),
      tap(institutionId => {
        this.programList = [];
        if (!this.isFirstSearch) {
          this.searchForm.get('programmeId').setValue(null);
        }
        if (!institutionId) {
          return;
        }
        this._loadPrograms(institutionId);
      })
    ).subscribe();

    combineLatest([
      fromEvent(this.searchName.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('name').value}
          })
        ),
      fromEvent(this.searchFirstname.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('firstName').value}
          })
        ),
      fromEvent(this.searchNationalStudentIdentifier.nativeElement,'keyup')
        .pipe(
          debounceTime(800), 
          distinctUntilChanged(), 
          startWith({
            target: {value: this.searchForm.get('identifiantNationalEleve').value}
          })
        ),
      this.searchForm.get('status').valueChanges
        .pipe(
          startWith(this.searchForm.get('status').value)
        ),
      this.searchForm.get('anneeAcademiqueId').valueChanges
        .pipe(
          startWith(this.searchForm.get('anneeAcademiqueId').value)
        ),
      this.searchForm.get('moisAnnee').valueChanges
        .pipe(
          startWith(this.searchForm.get('moisAnnee').value)
        ),
      this.searchForm.get('prefectureId').valueChanges
        .pipe(
          startWith(this.searchForm.get('prefectureId').value)
        ),
      this.searchForm.get('institutionId').valueChanges
        .pipe(
          startWith(this.searchForm.get('institutionId').value)
        ),
      this.searchForm.get('programmeId').valueChanges
        .pipe(
          startWith(this.searchForm.get('programmeId').value)
        ),
    ])
    .subscribe((
      [
        eventSearchName, 
        eventSearchFirstname,
        eventSearchNationalStudentIdentifier,
        status,
        anneeAcademiqueId,
        moisAnnee,
        prefectureId,
        institutionId,
        programmeId
      ]) => {
      this.searchForm.patchValue({
        name: eventSearchName ? eventSearchName['target'].value : null, 
        firstName: eventSearchFirstname ? eventSearchFirstname['target'].value : null, 
        identifiantNationalEleve: eventSearchNationalStudentIdentifier ? eventSearchNationalStudentIdentifier['target'].value : null,
        status,
        anneeAcademiqueId,
        moisAnnee,
        prefectureId,
        institutionId,
        programmeId
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.defaultSearchFormValue = {
      status: null,
      name: null,
      firstName: null,
      identifiantNationalEleve: null,
      anneeAcademiqueId: null,
      institutionId: null,
      programmeId: null,
      moisAnnee: null,
      prefectureId: null,
    };

    this.searchForm = this._formBuilder.group(this.defaultSearchFormValue);
  }

  protected _search(criteria: any): Observable<any> {
    return this._metfpetService.getPaiementBourseListPage(criteria);
  }

  public payBourseManual() {
    this._dialog.open(CreateBoursesPaymentManuallyDialog , {
      width: '800px',
      data: {
        anneeAcademiqueId : this.currentAnneeAcademique.id
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  public payBourseTrimestre() {
    this._dialog.open(CreateBoursesTrimestrePaymentDialog , {
      width: '800px',
      data: {
        anneeAcademiqueId : this.currentAnneeAcademique.id
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }
  
  public payBourse(row: MetfpetServiceAgent.PaiementBourseRowViewModel) {
    this._dialog.open(CreateBoursesPaymentDialog , {
      width: '800px',
      data: {
        paymentBoursesData: row,
      }
    }).afterClosed().subscribe(saveSuccess => {
      if (saveSuccess) {
        this.triggerSearch();
      }
    });
  }

  public editBoursePayment(row: MetfpetServiceAgent.PaiementBourseRowViewModel) {
    this._router.navigate([`${UiPath.institution.students.dossier}/${row.etudiantId}`], { state: { previousPage: 'Paiement des bourses' } });
  }

  public exportPayments() {
    this._store.dispatch(showLoading());
    this._metfpetService.getPaiementBourseListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
      pageIndex: -1,
      filters: this.searchForm.value,
    }))
      .subscribe(data => 
        this._exportService.exportBoursePayments(data.results))
      .add(() => this._store.dispatch(hideLoading()));
  }
  
  private _loadPrograms(institutionId: string) {
    this._metfpetService.getProgrammeListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS({
       pageIndex: -1, filters: {institution: institutionId}
    })).subscribe(data => {
      this.programList = data.results;
      this._cd.markForCheck();
    });
  }

  public isAllowed() {
    return this._authService.getUserType() == 'superviseurUniversitaire' ;
  }
}