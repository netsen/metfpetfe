import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { BoursePaymentsComponent } from './bourse-payments.component';
import { CreateBoursesPaymentManuallyDialog } from './create-bourses-payment-manually.dialog';

export const routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  },
  { 
    path: 'list',
    component: BoursePaymentsComponent, 
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [  
    BoursePaymentsComponent,
    CreateBoursesPaymentManuallyDialog
  ]
})
export class InstitutionBoursesModule {}
