import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { InstitutionUsersComponent } from './institution-users.component';
import { InstitutionUserComponent } from './institution-user.component';

export const routes = [
  { path: '', component: InstitutionUsersComponent, pathMatch: 'full' },
  { path: 'new', component: InstitutionUserComponent, data: { breadcrumb: 'Nouveau' }  },
  { path: 'view/:id', component: InstitutionUserComponent, data: { breadcrumb: 'Consultation' }  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
	  ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    InstitutionUsersComponent,
	  InstitutionUserComponent,
	]
})
export class InstitutionUsersModule {}
