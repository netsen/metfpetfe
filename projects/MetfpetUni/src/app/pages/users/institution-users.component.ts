import { 
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component, 
  ViewChild, 
  ViewEncapsulation, 
  ElementRef 
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { FormBuilder } from '@angular/forms';
import { Observable, fromEvent, combineLatest } from 'rxjs';
import { startWith, take, debounceTime, distinctUntilChanged } from 'rxjs/operators/';
import { 
  AppSettings,
  Settings,
  InstitutionEmployeeAccessMap,
  BaseTableComponent, 
  MetfpetServiceAgent,
  PerfectScrollService,
  selectLoggedInPerson,
  InstitutionEmployeeAccessValues,
  BiometricStatusValues,
} from 'MetfpetLib';

@Component({
  templateUrl: './institution-users.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstitutionUsersComponent extends BaseTableComponent {

  @ViewChild('searchIdentifiant', {static: true}) searchIdentifiant: ElementRef;
  @ViewChild('searchName', {static: true}) searchName: ElementRef;
  @ViewChild('searchFirstname', {static: true}) searchFirstname: ElementRef;
  @ViewChild('searchMatricule', {static: true}) searchMatricule: ElementRef;

  settings: Settings;
  title: string;
  accessMap = InstitutionEmployeeAccessMap;
  accessList = InstitutionEmployeeAccessValues;
  biometricList = BiometricStatusValues;

  constructor(
    public appSettings: AppSettings,
    protected _cd: ChangeDetectorRef,
    protected _formBuilder: FormBuilder,
    protected _router: Router,
    protected _store: Store<any>,
    protected _perfectScrollService: PerfectScrollService,
    private _route: ActivatedRoute, 
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    super(_cd, _formBuilder, _router, _store, _perfectScrollService);
    this.settings = this.appSettings.settings;
    this.title = 'Personnels';
    this.sort = {prop: 'name', dir: 'desc'};
  }

  ngOnInit() {
    super.ngOnInit();
    this._store.pipe(select(selectLoggedInPerson), take(1)).subscribe(
      person =>  {
        this.searchForm.get('institutionId').setValue(person.institutionId);
      }
    );
  }

  ngAfterViewInit() {
    combineLatest([
      fromEvent(
        this.searchIdentifiant.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('identifiant').value}
            })
          ),
      fromEvent(
        this.searchName.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('name').value}
            })
          ),
      fromEvent(
        this.searchFirstname.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('firstName').value}
            })
          ),
      fromEvent(
        this.searchMatricule.nativeElement,'keyup')
          .pipe(
            debounceTime(800), 
            distinctUntilChanged(), 
            startWith({
              target: {value: this.searchForm.get('matricule').value}
            })
          ),
      this.searchForm.get('access').valueChanges
        .pipe(
          startWith(this.searchForm.get('access').value)
        ),
      this.searchForm.get('biometricStatus').valueChanges
        .pipe(
          startWith(this.searchForm.get('biometricStatus').value)
        ),
      this.searchForm.get('isActive').valueChanges.pipe(startWith(this.searchForm.get('isActive').value)),
    ])
    .subscribe(([
      eventIdentifiant,
      eventName, 
      eventFirstname,
      eventMatricule,
      access,
      biometricStatus,
      isActive
    ]) => {
      this.searchForm.patchValue({
        identifiant: eventIdentifiant ? eventIdentifiant['target'].value : null, 
        name: eventName ? eventName['target'].value : null, 
        firstName: eventFirstname ? eventFirstname['target'].value : null, 
        matricule: eventMatricule ? eventMatricule['target'].value : null, 
        access,
        biometricStatus,
        isActive
      }, {emitEvent: false});
      this.triggerSearch();
    });
  }

  protected _createSearchForm() {
    this.searchForm = this._formBuilder.group({
      identifiant: null,
      name: null,
      firstName: null,
      matricule: null,
      access: null,
      biometricStatus: null,
      isActive: true,
      institutionId: null,
    });
  }

  protected _search(criteria: any): Observable<any> {
    // to convert True/false value to number 1/0
    criteria.filters.isActive = +criteria.filters.isActive;
    return this._metfpetService.getEmployeUniversitaireListPage(MetfpetServiceAgent.PagingSearchDTO.fromJS(criteria));
  }

  create() {
    this._router.navigate([`new`], {relativeTo: this._route});
  }

  open(selectedRow: MetfpetServiceAgent.EmployeUniversitaireRowViewModel) {
    this._router.navigate([`view/${selectedRow.id}`], {relativeTo: this._route});
  }
}
