import {
  ChangeDetectionStrategy, 
  ChangeDetectorRef, 
  Component,
} from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { 
  AppSettings,
  Settings,
  InstitutionEmployeeAccessValues,
  emailValidator,
  validateForm,
  showSuccess,
  showError,
  showLoading,
  hideLoading,
  MetfpetServiceAgent,
  selectLoggedInPerson,
  showException,
} from 'MetfpetLib';
import { UiPath } from '../ui-path';

@Component({
  templateUrl: './institution-user.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstitutionUserComponent {

  form: FormGroup;
  settings: Settings;
  title: string;
  isCreation: boolean = true;
  accessList = InstitutionEmployeeAccessValues;
  prefectureList: Array<MetfpetServiceAgent.Prefecture>;
  public _model: MetfpetServiceAgent.EmployeUniversitaireProfileViewModel;

  constructor(
    public appSettings: AppSettings,
    private _cd: ChangeDetectorRef,
    private _fb: FormBuilder,
    private _location: Location,
    private _router: Router,
    private _route: ActivatedRoute, 
    private _store: Store<any>,
    private _metfpetService: MetfpetServiceAgent.HttpService
  ) {
    this.settings = this.appSettings.settings;
    this.form = this._fb.group({
      'name': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'firstName': [null, Validators.compose([Validators.required, Validators.minLength(2)])],
      'email': [null, Validators.compose([Validators.required, emailValidator])],
      'phone': [null, Validators.compose([Validators.pattern("^[0-9]{9}$")])],
      'identifiant': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
      'matricule': [null, Validators.compose([Validators.minLength(3)])],
      'categorie': [null, Validators.compose([Validators.minLength(3)])],
      'service': [null, Validators.compose([Validators.minLength(3)])],
      'fonction': [null, Validators.compose([Validators.minLength(3)])],
      'hierarchie': [null, Validators.compose([Validators.minLength(3)])],
	    'diplomePlusEleve': [null, Validators.compose([Validators.minLength(3)])],
      'specialite': [null, Validators.compose([Validators.minLength(3)])],
      'typeEcole': [null, Validators.compose([Validators.minLength(3)])],
      'acces': [null, Validators.required],
      'isActive': [null],
      'institutionId': [null, Validators.required],
      'prefectureId': [null, Validators.compose([Validators.minLength(3)])]
    });
  }

  ngOnInit() {
    this._store.pipe(select(selectLoggedInPerson), take(1)).subscribe(
      person =>  {
        this.form.controls['institutionId'].setValue(person.institutionId);
      }
    );

    this._route.params.subscribe(params => {
      let id = params['id'];
      this.isCreation = !id;

      if (!this.isCreation) {
        this._metfpetService.getEmployeUniversitaireProfile(id).subscribe(data => {
          this._model = data;
          this.title = this._model.firstName + ' ' + this._model.name;
          this.form.controls['name'].setValue(this._model.name);
          this.form.controls['firstName'].setValue(this._model.firstName);
          this.form.controls['email'].setValue(this._model.email);
          this.form.controls['phone'].setValue(this._model.phone);
          this.form.controls['identifiant'].setValue(this._model.identifiant);
          this.form.controls['matricule'].setValue(this._model.matricule);
          this.form.controls['categorie'].setValue(this._model.categorie);
          this.form.controls['service'].setValue(this._model.service);
          this.form.controls['fonction'].setValue(this._model.fonction);
          this.form.controls['hierarchie'].setValue(this._model.hierarchie);
          this.form.controls['diplomePlusEleve'].setValue(this._model.diplomePlusEleve);
          this.form.controls['specialite'].setValue(this._model.specialite);
          this.form.controls['typeEcole'].setValue(this._model.typeEcole);
          this.form.controls['acces'].setValue(this._model.acces);
          this.form.controls['isActive'].setValue(this._model.isActive);
          this.form.controls['institutionId'].setValue(this._model.institutionId);
          this.form.controls['prefectureId'].setValue(this._model.prefectureId);
          if(!data.photoPath){
            this._model.photoPath = "/assets/img/users/default-user.jpg";
          }
          this._cd.markForCheck();
        });
      }
    });
  }

  ngAfterViewInit() {
    this._metfpetService.getPrefectureList().subscribe(data => {
      this.prefectureList = data;
      this._cd.markForCheck();
    });
  }

  public onSubmit(): void {
    validateForm(this.form);
    if (this.form.valid) {
      this._store.dispatch(showLoading());
      var saveUser$: Observable<any>;

      if (this.isCreation) {
        saveUser$= this._metfpetService.registerEmployeUniversitaire(MetfpetServiceAgent.RegisterEmployeUniversitaireViewModel.fromJS(
          Object.assign({}, this._model, this.form.value)
        ));
      } else {
        saveUser$ = this._metfpetService.updateEmployeUniversitaireProfile(MetfpetServiceAgent.EmployeUniversitaireProfileViewModel.fromJS(
          Object.assign({}, this._model, this.form.value)
        ));
      }

      saveUser$
        .subscribe(
          () =>  {
            this._store.dispatch(showSuccess({}));
            this._router.navigate([UiPath.institution.users.list]);
          },
          error => this._store.dispatch(showException({error: error}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public onFileUpload(event) {
    if (event.target.files && event.target.files.length > 0) {
      this._store.dispatch(showLoading());

      let file : Blob = event.target.files[0];
      var fileReader = new FileReader();
      fileReader.onloadend = function () {
        
      }
      fileReader.readAsBinaryString(file);
      
      let fileParameter = 
      {
        data : file,
        fileName : event.target.fileName
      };
        
      this._metfpetService.uploadEmployeUniversitairePhoto(fileParameter, this._model.id)
        .subscribe(
          (result) => {
            this._model.photoPath = result;
            this._cd.markForCheck();
            this._store.dispatch(showSuccess({}));
          },
          (error) => this._store.dispatch(showError({message: error.message}))
        )
        .add(() => this._store.dispatch(hideLoading()));
    }
  }

  public openFile(){
    document.getElementById('photoSelector').click();
  }

  goBack() {
    this._location.back();
  }
}