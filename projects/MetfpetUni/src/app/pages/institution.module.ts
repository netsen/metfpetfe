import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthRoleGuardService, AuthorityEnum, PagesComponent, PublicLayoutComponent } from 'MetfpetLib';
import { SharedModule } from '../shared/shared.module';
import { InstitutionHomeComponent } from './home/institution-home.component';
import { InstitutionProfileDialog } from './profile';

export const routes = [
  { 
    path: 'login', 
    component: PublicLayoutComponent,
    loadChildren: () => import('./login/institution-login.module').then(m => m.InstitutionLoginModule),
  },
  { 
    path: 'retrieve-password', 
    component: PublicLayoutComponent,
    loadChildren: () => import('./retrieve-password/institution-retrieve-password.module').then(m => m.InstitutionRetrievePasswordModule), 
  },
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: '',
        component: InstitutionHomeComponent,
        data: { breadcrumb: 'Accueil', expectedRoles: [ AuthorityEnum.SuperviseurUniversitaire, AuthorityEnum.AgentUniversitaire ]  },
        canActivate: [ AuthRoleGuardService ]
      },
      { 
        path: 'admissions', 
        loadChildren: () => import('./admissions/admissions.module').then(m => m.AdmissionsModule),
        data: { breadcrumb: 'Admissions', expectedRoles: [ AuthorityEnum.SuperviseurUniversitaire, AuthorityEnum.AgentUniversitaire ] },
        canActivate: [ AuthRoleGuardService ]
      },
      { 
        path: 'inscriptions', 
        loadChildren: () => import('./inscriptions/institution-inscriptions.module').then(m => m.InstitutionInscriptionsModule),
        data: { breadcrumb: 'Inscriptions', expectedRoles: [ AuthorityEnum.SuperviseurUniversitaire, AuthorityEnum.AgentUniversitaire ] },
        canActivate: [ AuthRoleGuardService ]
      },
      { 
        path: 'users', 
        loadChildren: () => import('./users/institution-users.module').then(m => m.InstitutionUsersModule),
        data: { breadcrumb: 'Personnels', expectedRoles: [ AuthorityEnum.SuperviseurUniversitaire ] }, 
        canActivate: [ AuthRoleGuardService ] 
      },
      { 
        path: 'students', 
        loadChildren: () => import('./students/institution-students.module').then(m => m.InstitutionStudentsModule),
        data: { breadcrumb: 'Apprenants', expectedRoles: [ AuthorityEnum.SuperviseurUniversitaire, AuthorityEnum.AgentUniversitaire ] },
        canActivate: [ AuthRoleGuardService ]
      },
      { 
        path: 'documents', 
        loadChildren: () => import('./documents/institution-documents.module').then(m => m.InstitutionDocumentsModule),
        data: { breadcrumb: 'Documents', expectedRoles: [ AuthorityEnum.SuperviseurUniversitaire, AuthorityEnum.AgentUniversitaire ] },
        canActivate: [ AuthRoleGuardService ]
      },
      { 
        path: 'bourses', 
        loadChildren: () => import('./bourses/institution-bourses.module').then(m => m.InstitutionBoursesModule),
        data: { breadcrumb: 'Bourses', expectedRoles: [ AuthorityEnum.SuperviseurUniversitaire ] },
        canActivate: [ AuthRoleGuardService ]
      },
      { 
        path: 'examens', 
        loadChildren: () => import('./examens/institution-examens.module').then(m => m.InstitutionExamensModule),
        data: { breadcrumb: 'Examens', expectedRoles: [ AuthorityEnum.SuperviseurUniversitaire, AuthorityEnum.AgentUniversitaire ] },
        canActivate: [ AuthRoleGuardService ]
      },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule, 
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    InstitutionProfileDialog,
    InstitutionHomeComponent
  ],
  entryComponents: [
    InstitutionProfileDialog
  ]
})
export class InstitutionModule { }