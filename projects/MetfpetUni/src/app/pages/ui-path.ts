export const UiPath = {
  error            : 'error',
  login            : 'login',
  institution: {
    login          : 'login',
    inscriptions: {
      list         : 'inscriptions',
    },
    users: {
      list         : 'users',
      add          : 'users/new',
      view         : 'users/view',
    },
    students: {
      list         : 'students',
      dossier      : 'students/dossier',
    },
    documents: {
      list         : 'documents',
    },
    bourses: {
      list         : 'bourses/list',
    },
    examens      : {
      currentSession : 'examens/currentSession',
      pastSession  : 'examens/pastSession',
   },
  }
};