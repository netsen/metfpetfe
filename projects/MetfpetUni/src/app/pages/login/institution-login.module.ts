import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { InstitutionLoginComponent } from './institution-login.component';

export const routes = [
  { path: '', component: InstitutionLoginComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule, 
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    InstitutionLoginComponent
  ]

})
export class InstitutionLoginModule { }