import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { 
  AppSettings,
  Settings,
  LoginTypeEnum,
  AuthService,
  showError,
  showException,
} from 'MetfpetLib';
import { ReCaptchaV3Service } from 'ng-recaptcha';

@Component({
  templateUrl: './institution-login.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstitutionLoginComponent {

  settings: Settings;
  loading: boolean;
  private _recaptchaSubscription: Subscription;

  constructor(
    public appSettings: AppSettings, 
    private _cd: ChangeDetectorRef,
    private _router: Router,
    private _store: Store<any>,
    private _authService: AuthService,
    private _recaptchaV3Service: ReCaptchaV3Service
  ) {
  }

  ngOnDestroy() {
    if (this._recaptchaSubscription) {
      this._recaptchaSubscription.unsubscribe();
    }
  }

  public onSubmit(event): void {
    this.loading = true;
    if (this._recaptchaSubscription) {
      this._recaptchaSubscription.unsubscribe();
    }

    this._recaptchaSubscription = this._recaptchaV3Service.execute('login')
      .pipe(
        switchMap(token => 
          this._authService.login(event.identifier, event.password, LoginTypeEnum.Institution)
        )
      )
      .subscribe(
        () =>  this._router.navigate(['/']),
        error => this._store.dispatch(showException({error: error}))
      )
      .add(() => {
        this.loading = false;
        this._cd.markForCheck();
      });
  }
}