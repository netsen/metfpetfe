import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { InstitutionRetrievePasswordComponent } from './institution-retrieve-password.component';

export const routes = [
  { path: '', component: InstitutionRetrievePasswordComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule, 
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [
    InstitutionRetrievePasswordComponent
  ]
})
export class InstitutionRetrievePasswordModule { }