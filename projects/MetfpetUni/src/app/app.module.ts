import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule, ErrorHandler, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OverlayContainer } from '@angular/cdk/overlay';
import { CustomOverlayContainer } from 'MetfpetLib';
import { DatePipe } from '@angular/common'

import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  wheelPropagation: true,
  suppressScrollX: true
};
import { ServiceWorkerModule } from '@angular/service-worker'; 

import { SharedModule } from './shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NgxPermissionsModule } from 'ngx-permissions';
import { routing } from './app.routing';
import { AppComponent } from './app.component';

import { APP_CONFIG, APP_DI_CONFIG } from './app.config';
import { UserIdleModule } from 'angular-user-idle';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr, 'fr');
import { environment } from '../environments/environment';
import { rootReducer, metaReducers } from './shared/store/reducers/';
import { UserEffects } from './shared/store/effects/';
import { SvgIconsModule } from '@ngneat/svg-icon';
import {
  AppSettings,
  AuthenticationInterceptor,
  GlobalErrorsHandler,
  CommunicationServiceAgent, 
  IdentityServiceAgent, 
  PaymentServiceAgent,
  MetfpetServiceAgent
} from 'MetfpetLib';

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    SharedModule,
    UserIdleModule.forRoot({idle: 600, timeout: 1, ping: 120}),
    routing,
    StoreModule.forRoot(rootReducer, { metaReducers }),
    EffectsModule.forRoot([
      UserEffects,
    ]),
    SvgIconsModule.forRoot({
      icons: [],
    }),
    NgxPermissionsModule.forRoot(),
  ],
  declarations: [
    AppComponent,
  ],
  providers: [
    AppSettings,
    DatePipe,
    CommunicationServiceAgent.HttpService,
    IdentityServiceAgent.HttpService,
    PaymentServiceAgent.HttpService,
    MetfpetServiceAgent.HttpService,
    { provide: APP_CONFIG, useValue: APP_DI_CONFIG },
    { provide: ErrorHandler, useClass: GlobalErrorsHandler },
    { provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptor, multi: true },
    { provide: CommunicationServiceAgent.COMMUNICATION_BASE_URL, useValue: environment.apiEndPoint },
    { provide: IdentityServiceAgent.IDENTITY_BASE_URL, useValue: environment.apiEndPoint },
    { provide: PaymentServiceAgent.PAYMENT_BASE_URL, useValue: environment.apiEndPoint },
    { provide: MetfpetServiceAgent.METFPET_BASE_URL, useValue: environment.apiEndPoint },
    { provide: LOCALE_ID, useValue: 'fr-FR' },
    { provide: OverlayContainer, useClass: CustomOverlayContainer },
    { provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG },
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
