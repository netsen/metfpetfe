## Proposed changes

Summary of the changes

## Checklist

- [ ] Implementation 
- [ ] Unit tests
- [ ] System Tests
- [ ] Documentation
- [ ] *Ready to Merge?*

## UI changes

Add relevant UI images

## Further comments

Add more Comments if needed

## ###### REVIEW LIKE AP PRO
## Author - Checklist
- [ ] La tâche est basée sur la bonne branche 
- [ ] La branche est à jour avec 'master/dev' 
- [ ] Pas de fichiers oubliés  
- [ ] Les changements plus complexes sont anotés 
- [ ] Les messages de commit sont "A1" 
- [ ] Tous les tests passent 

## Reviewer - Checklist
- [ ] **portée (scope)** : répond au besoin, pas de changement "tan tqu'à y être" 
- [ ] **implémentation** : lisible, facile à comprendre, noms bien choisis, bonne architecture
- [ ] **tests**: il ya des tests pour couvrir les changements (unitaires, intégration, autres) 
- [ ] **validations** : les entrées sont validées, tailles max des champs, les types, requis/optionnel, etc 
- [ ] **traductions**: les textes sont extraits dans les fichiers de traduction 
- [ ] **migrations**: il ya des migrations pour les changements de BD ou l'ajout de données recquises 
- [ ] **sécurité** : pas de failles au niveau BD (SQLi), frontend(XSS), formulaires (CSRF, _mass-assignment_)