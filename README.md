# How to deploy new version of Metfpet

## Prerequisites

Create a database back-up.

Increment the version of release (format: x.y.z ==> e.g. 1.2.0) and add this info in the footer of Angular page and in `config.php`.

Create tags for front-end and back-end prior to deployment.

Need the permission to access via VPN, RDP to Metfpet machines.

## Front-end (Angular)

Go to folder: `metfpetfrontend`

Verify the configuration in `environment.dev.ts`, `environment.prod.ts` is up to date with the target environment.

There are 3 applications MetfpetAdmin, MetfpetStudent, and MetfpetUni. 

### Development
Open different command window to build projects:
```
ng build MetfpetLib --watch
ng build MetfpetAdmin --port=4201
ng build MetfpetUni --port=4202
ng build MetfpetStudent --port=4203
```

### Staging
```
ng build MetfpetAdmin --configuration=dev
```
### Production 
```
ng build MetfpetAdmin --prod
```

Copy the generated `dist` folder onto IIS server

## Back-end (Back-end)

Copy all php files in folders  `<projects>\metfpetbackend\MetfpetPHP\Modele`, `Connexion`, `Gestionnaire`, `Services`, `Helpers` onto IIS server.

Verify the new required parameters exist in `config.php`

## Add new SVG icons
To add a new icon SVG, we need to do:
1. Add the new icon in **projects/MetfpetLib/src/lib/svg**
2. Run: **npm run generate-icons**
3. Then, register new icon in the **MetfpetLibModule**

```
SvgIconsModule.forChild(
  [appCalendarIcon]
)
```
4. Use the icon in the component
```
<svg-icon [key]="menu.icon" fontSize="36px"></svg-icon>

```

